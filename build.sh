#!/usr/bin//env bash

build_type="Debug"
build_type_3party="Release"
script=$(readlink -f $0)
src_root_path=`dirname $script`
build_root_path="${src_root_path}/../build/cilab-gimiax-${build_type}"
bin_root_path="${src_root_path}/../build/bin"
executable_root_path="${bin_root_path}/bin"
third_party_build_path="${src_root_path}/../build/cilab-gimiax-3party-${build_type_3party}"
third_party_src_path="${src_root_path}/../cilab-gimiax-3party"

# getting number of cores
n_cores=$(grep -c ^processor /proc/cpuinfo)
n_cores=$(( $n_cores * 2 ))

[ -d "${build_root_path}" ] || mkdir -p "${build_root_path}"
[ -d "${bin_root_path}" ] || mkdir -p "${bin_root_path}"
[ -d "${executable_root_path}" ] || mkdir -p "${executable_root_path}"

cd "${build_root_path}" && \
    cmake -DCMAKE_BUILD_TYPE=${build_type} ${src_root_path} \
          -DBUILD_TESTING=OFF \
          -DGIMIAS_THIRD_PARTY_SCR_PATH=$third_party_src_path \
          -DGIMIAS_THIRD_PARTY_BINARIES_PATH=$third_party_build_path \
          -DGIMIAS_SRC_PATH="${src_root_path}/src" \
          -DGIMIAS_ACTIVATE_EXTENSIONS=OFF \
          -DGIMIAS_BINARY_PATH=$bin_root_path \
          -DGIMIAS_EXECUTABLE_PATH=$executable_root_path \
          "${src_root_path}/src/cmake/executable/Gimias" && \
    echo "compilation process is running in background!!!" && \
    echo " just check in source dir compile-out.log" && \
    ( make -j $n_cores &> "${src_root_path}/compile-out.log"; ) &
