/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "meNGSmoothFilter.h"

meNGSmoothFilter::meNGSmoothFilter()
{
	m_param = new Ng_Meshing_Parameters();
}


meNGSmoothFilter::~meNGSmoothFilter() 
{
}

void meNGSmoothFilter::SetParams( meMeshSmoothingParamsPtr params )
{
// 	m_param.maxh = 3;
// 	m_param.fineness =  0.2;
// 	m_param.segmentsperedge =  0.2;
}

void meNGSmoothFilter::Update()
{
	try
	{
		m_param = new Ng_Meshing_Parameters();
		if (Ng_GetNP(GetInput()->GetNGMesh( )) ==  0)
		{
			GenerateSurfaceMesh( m_param );
		}
		else
		{
			Ng_STL_MakeEdges(GetInput()->GetGeom( ), GetInput()->GetNGMesh( ), m_param );
		}

		m_param->optimize2d = "m";
		Ng_STL_OptimizeSurfaceMesh( GetInput()->GetGeom( ), GetInput()->GetNGMesh( ), m_param );
	}
	catch (...)
	{
		throw;
	}
	
}
