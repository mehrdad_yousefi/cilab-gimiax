/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "meNGRefineFilter.h"

meNGRefineFilter::meNGRefineFilter()
{

}


meNGRefineFilter::~meNGRefineFilter() 
{
}

void meNGRefineFilter::Update()
{
	try
	{
		Ng_Meshing_Parameters* mp = new Ng_Meshing_Parameters();

		if (Ng_GetNP(GetInput()->GetNGMesh( )) ==  0)
		{
			GenerateSurfaceMesh( mp );
			Ng_STL_OptimizeSurfaceMesh( GetInput()->GetGeom( ), GetInput()->GetNGMesh( ), mp );
		}
		else
		{
			Ng_STL_MakeEdges(GetInput()->GetGeom( ), GetInput()->GetNGMesh( ), mp );
		}

		Ng_MeshRefinement( GetInput()->GetGeom( ), GetInput()->GetNGMesh( ) );
	}
	catch (...)
	{
		throw;
	}
	
}