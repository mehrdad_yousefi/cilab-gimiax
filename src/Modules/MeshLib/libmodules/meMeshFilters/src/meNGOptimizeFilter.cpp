/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "meNGOptimizeFilter.h"

meNGOptimizeFilter::meNGOptimizeFilter()
{
	m_param = meMeshParamsOptimizePtr( new meMeshParamsOptimize );
}


meNGOptimizeFilter::~meNGOptimizeFilter() 
{
}

void meNGOptimizeFilter::SetParams( meMeshParamsOptimizePtr params )
{
	m_param = params;
}

void meNGOptimizeFilter::Update()
{
	try
	{
		Ng_Meshing_Parameters*  netgenParam = new Ng_Meshing_Parameters(); 
		netgenParam->maxh = m_param->m_maxh;
		netgenParam->curvaturesafety = m_param->m_curvaturesafety;
		netgenParam->segmentsperedge = m_param->m_segmentsperedge;
		//netgenParam->optsteps2d = 3;
		
		// if the input is only the geometry then generate the mesh before using it
		if (Ng_GetNP(GetInput()->GetNGMesh( )) ==  0)
		{
			GenerateSurfaceMesh( netgenParam );
		}
		else
		{
			Ng_STL_MakeEdges(GetInput()->GetGeom( ),GetInput()->GetNGMesh( ),netgenParam);
		}

	
		netgenParam->optimize2d = "cmSmSm";
		Ng_STL_OptimizeSurfaceMesh( GetInput()->GetGeom( ), GetInput()->GetNGMesh( ), netgenParam );
		Ng_MeshRefinement( GetInput()->GetGeom( ), GetInput()->GetNGMesh( ) );
		netgenParam->optimize2d = "m";
		Ng_STL_OptimizeSurfaceMesh( GetInput()->GetGeom( ), GetInput()->GetNGMesh( ), netgenParam );

		std::cout<< "optimization done!"<<std::endl;


	}
	catch (...)
	{
		throw;
	}
	
}

meMeshParamsOptimizePtr meNGOptimizeFilter::GetParams()
{
	return m_param;
}
