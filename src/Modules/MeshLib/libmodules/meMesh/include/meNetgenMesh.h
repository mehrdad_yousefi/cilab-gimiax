/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _meNetgenMesh_H
#define _meNetgenMesh_H

//#include "nglib.h"
#include "nglib_gimias.h"
using namespace nglib;
/**
Initialize Netgen and de initialize it
\author Xavi Planes
\date 03 nov 2009
\ingroup MeshLib
*/
class NetgenInitialization
{
public:
	//!
	NetgenInitialization( );

	//!
	~NetgenInitialization( );
};


/**
The input geometry Ng_STL_Geometry is used as a geometry to perform all 
operations.

The output mesh Ng_Mesh is the result of the filtering.

\author Xavi Planes
\date 03 nov 2009
\ingroup MeshLib
*/
class meNetgenMesh : public blLightObject
{
public:
	typedef meNetgenMesh Self;
	typedef blSmartPointer<Self> Pointer;
	blNewMacro(Self);

	//!
	Ng_STL_Geometry * GetGeom() const;
	void SetGeom(Ng_STL_Geometry * val);
	//! copy the geometry instead of taking the reference(used in setInput of meNGBaseFilter)
	void CopyGeom(Ng_STL_Geometry* val);
	//!
	Ng_Mesh * GetNGMesh() const;
	void SetNGMesh(Ng_Mesh * val);
	//! copy the mesh instead of taking the reference(used in setInput of meNGBaseFilter)
	void CopyMesh( Ng_Mesh* val );
protected:
	//!
	meNetgenMesh( );

	//!
	~meNetgenMesh( );

protected:
	//! Input geometry (from vtkPolyData for example)
	Ng_STL_Geometry *m_pGeom;
	//! Output mesh (result of filtering)
	Ng_Mesh *m_pNGMesh;
	//!
	static NetgenInitialization m_NetgenInitialization;
};



#endif // _meNetgenMesh_H

