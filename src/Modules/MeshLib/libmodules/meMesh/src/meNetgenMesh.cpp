/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "meNetgenMesh.h"
#include "nglib.h"


NetgenInitialization meNetgenMesh::m_NetgenInitialization;

NetgenInitialization::NetgenInitialization()
{
	Ng_Init( );
}

NetgenInitialization::~NetgenInitialization()
{
	// It crashes
	//Ng_Exit( );
}

meNetgenMesh::meNetgenMesh()
{
	m_pGeom = Ng_STL_NewGeometry( );
	m_pNGMesh = Ng_NewMesh( );
}

meNetgenMesh::~meNetgenMesh()
{
	Ng_STL_DeleteGeometry( m_pGeom );
	m_pGeom = NULL;
	Ng_DeleteMesh( m_pNGMesh );
	m_pNGMesh = 0;
}

Ng_STL_Geometry * meNetgenMesh::GetGeom() const
{
	return m_pGeom;
}

void meNetgenMesh::SetGeom( Ng_STL_Geometry * val )
{
	m_pGeom = val;
}

Ng_Mesh * meNetgenMesh::GetNGMesh() const
{
	return m_pNGMesh;
}

void meNetgenMesh::SetNGMesh( Ng_Mesh * val )
{
	m_pNGMesh = val;
}

void meNetgenMesh::CopyGeom(Ng_STL_Geometry* val)
{
	// The Geometry only contains information about triangles, we have to 
	// perform a Generate Surface to be able to use the functionalities of Netgen

	double p1[3];
	double p2[3];
	double p3[3];
	int temp[3];
	//double nv[3];

	for (	unsigned i = 1; 
	i <  Ng_STL_GetNumTriangles(  val )+1; 
	i++ )
	{
		Ng_STL_GetTriangle (  val, i , temp);
		Ng_STL_GetPoint(val,temp[0],p1);
		Ng_STL_GetPoint(val,temp[1],p2);
		Ng_STL_GetPoint(val,temp[2],p3);
		Ng_STL_AddTriangle(m_pGeom,p1,p2,p3);
	}

	Ng_STL_InitSTLGeometry(m_pGeom);

}

void meNetgenMesh::CopyMesh( Ng_Mesh* val )
{
	// The mesh contains the informations about the points the surface elements (triangles)
	// and the elements ( tetras) for the moment this last information is not included in 
	// this implementation 

	if(Ng_GetNP( val ) == 0)
		return;

	double tempPoint[3];
	// Add the points
	for (	unsigned iNumPoint = 1; 
	iNumPoint < Ng_GetNP( val )+1 ; 
	iNumPoint++ )
	{
		Ng_GetPoint( val, iNumPoint , tempPoint );
		Ng_AddPoint(m_pNGMesh,tempPoint);
	}

	int tempSurfEl[3];
	// Add the surface elements
	for (	unsigned i = 1; 
	i < Ng_GetNSE( val )+1 ; 
	i++ )
	{
		Ng_GetSurfaceElement ( val, i , tempSurfEl);
		Ng_AddSurfaceElement(m_pNGMesh,NG_TRIG,tempSurfEl);
	}

}

