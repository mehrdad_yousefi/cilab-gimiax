/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "wxMitkRenderingManagerFactory.h"
#include "wxMitkRenderingManager.h"

using namespace mitk;

//!
wxMitkRenderingManagerFactory::wxMitkRenderingManagerFactory()
{
  mitk::RenderingManager::SetFactory( this );

}

//!
wxMitkRenderingManagerFactory::~wxMitkRenderingManagerFactory()
{
}

//!
mitk::RenderingManager::Pointer wxMitkRenderingManagerFactory::CreateRenderingManager() const
{
  wxMitkRenderingManager::Pointer specificSmartPtr = wxMitkRenderingManager::New();
  mitk::RenderingManager::Pointer smartPtr = specificSmartPtr.GetPointer();
  return smartPtr;
}

