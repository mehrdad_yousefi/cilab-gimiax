/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "wxMitkVTKRenderWindowInteractor.h"
#include <vtkCommand.h>
#include <vtkObjectFactory.h>
//#include <mitkVtkRenderWindow.h>
#include <mitkRenderingManager.h>

using namespace mitk;

vtkStandardNewMacro(wxMitkVTKRenderWindowInteractor);

//!
wxMitkVTKRenderWindowInteractor::wxMitkVTKRenderWindowInteractor(void) : 
	 vtkRenderWindowInteractor()
{
}

//!
wxMitkVTKRenderWindowInteractor::~wxMitkVTKRenderWindowInteractor(void)
{
}
