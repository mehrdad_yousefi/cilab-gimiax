/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "wxMitkRenderingManager.h"
#include "wxMitkRenderingRequestEvent.h"
#include "wxMitkAbortEventFilter.h"
#include "wxID.h"

using namespace mitk;

BEGIN_EVENT_TABLE(wxMitkRenderingManager, wxEvtHandler)
END_EVENT_TABLE()

//!
wxMitkRenderingManager::wxMitkRenderingManager() : wxEvtHandler(), RenderingManager()
{
}

//!
wxMitkRenderingManager::~wxMitkRenderingManager()
{
}

void mitk::wxMitkRenderingManager::GenerateRenderingRequestEvent()
{
	wxMitkRenderingRequestEvent renderingEvent;
  wxPostEvent(this, renderingEvent);
}

bool mitk::wxMitkRenderingManager::ProcessEvent(wxEvent& event)
{
	if ( event.GetEventType() == wxEVT_RENDERING_REQUEST )
	{
		// Directly process all pending rendering requests
		this->UpdateCallback();
		return true;
	}
	else
	{
		return wxEvtHandler::ProcessEvent( event );
	}

	return false;
}

void mitk::wxMitkRenderingManager::DoFinishAbortRendering()
{
	wxMitkAbortEventFilter::GetInstance()->IssueQueuedEvents();
}

void mitk::wxMitkRenderingManager::DoMonitorRendering()
{
	wxMitkAbortEventFilter::GetInstance()->ProcessEvents();
}
