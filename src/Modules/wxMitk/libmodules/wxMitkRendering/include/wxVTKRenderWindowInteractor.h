/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#ifndef _wxVTKRenderWindowInteractor_h_
#define _wxVTKRenderWindowInteractor_h_

#include "wxMitkRenderingWin32Header.h"

// vtk includes
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"


class WXMITKRENDERING_EXPORT wxVTKRenderWindowInteractor : public wxEvtHandler, public vtkRenderWindowInteractor
{
	DECLARE_DYNAMIC_CLASS(wxVTKRenderWindowInteractor)

  public:
    static wxVTKRenderWindowInteractor * New();
	vtkTypeRevisionMacro(wxVTKRenderWindowInteractor,vtkRenderWindowInteractor);

	  //destructor
    ~wxVTKRenderWindowInteractor();

    // vtkRenderWindowInteractor overrides
    void Initialize();
    void Start();
    void UpdateSize(int x, int y);
    int CreateTimer(int timertype);
    int DestroyTimer();
    void TerminateApp();

    // event handlers
    void OnTimer(wxTimerEvent &event);

  private:
    wxTimer timer;
    
  private:

	//constructors
	wxVTKRenderWindowInteractor();

    wxDECLARE_EVENT_TABLE();
};

#endif //_wxVTKRenderWindowInteractor_h_
