/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _wxMitkVTKRenderWindowInteractor_h
#define _wxMitkVTKRenderWindowInteractor_h

#include "wxMitkRenderingWin32Header.h"
#include <vtkRenderWindowInteractor.h>

namespace mitk
{

/*
\ingroup wxMitkRendering
\author Juan Antonio Moya
\date 11 Sep 2007
*/
class WXMITKRENDERING_EXPORT wxMitkVTKRenderWindowInteractor 
	: public vtkRenderWindowInteractor
{

public:
	static wxMitkVTKRenderWindowInteractor* New();
	vtkTypeMacro(wxMitkVTKRenderWindowInteractor,vtkRenderWindowInteractor);
	wxMitkVTKRenderWindowInteractor(void);
	virtual ~wxMitkVTKRenderWindowInteractor(void);

protected:
	// unimplemented copy constructor or operator
	wxMitkVTKRenderWindowInteractor(const wxMitkVTKRenderWindowInteractor&);
	void operator=(const wxMitkVTKRenderWindowInteractor&);

};

} // namespace mitk

#endif //_wxMitkVTKRenderWindowInteractor_h_
