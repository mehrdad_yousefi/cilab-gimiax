/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef wxMitkImageSettingsWidget_H
#define wxMitkImageSettingsWidget_H

#include "wxMitkWidgetsWin32Header.h"
#include <mitkProperties.h>
#include <wx/panel.h>

#include "mitkLookupTableProperty.h"
#include "wxMitkColorSelectorControl.h"

class wxCheckBox;
class wxCommandEvent;
class wxComboBox;
class wxSlider;
class wxNotebook;

namespace mitk
{
class DataTreeNode;
class wxMitkRenderWindow;
class wxMitkLookupTableWidget;

/*
Edit image properties like opacity or LookupTable

\ingroup wxMitkWidgets
\author Xavi Planes
\date Jan 2013
*/
class WXMITKWIDGETS_EXPORT wxMitkImageSettingsWidget : public wxPanel
{
public:

	wxMitkImageSettingsWidget(
		wxWindow* parent, 
		wxWindowID id, 
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = 0, 
		const wxString& name = wxT("wxMitkImageSettingsWidget"));
	virtual ~wxMitkImageSettingsWidget(void);

	//!
	void SetDataTreeNode(mitk::DataTreeNode* node);

	/** 
	\brief Set/Get render window to show the legend. 
	\note This functionality should be inside MITK
	*/
	void SetMitkRenderWindow( mitk::wxMitkRenderWindow* renderWindow );

	mitk::wxMitkRenderWindow* GetMitkRenderWindow( ) const;

	//!
	void UpdateWidgets( );

	//!
	void CleanWidgets( );

private:
	void do_layout( );

	//! disable/enable checkboxes and sliders related to volume rendering only
	void OnIntensitySliderChanged( wxScrollEvent &event);
	void OnTextureInterpolation( wxCommandEvent& event );
	void OnResliceInterpolation( wxCommandEvent& event );
	void OnStereo( wxCommandEvent& event );
	void OnStereoType( wxCommandEvent& event );

	void SetStereo(bool onOff);
	int GetSelectedStereoType();

    wxDECLARE_EVENT_TABLE();

private:
	//!
	mitk::wxMitkRenderWindow* m_renderWindow;
	//!
	mitk::DataTreeNode* m_node;

	//TODO: could be made static to save same memory..
	std::vector<std::pair<std::string, int> > m_mapStereoTypes;

	//! Image settings
	wxStaticBox* sizer_1_staticbox;
	mitk::wxMitkLookupTableWidget *m_LookupTableWidget;
	wxSlider* m_OpacitySlider;
	wxCheckBox* m_chkTextureInterpolation;
	wxComboBox* m_cmbResliceInterpolation;
	wxCheckBox* m_chkStereo;
	wxComboBox* m_cmbStereoType;
};
 
} // namespace mitk


#endif // wxMitkImageSettingsWidget_H
