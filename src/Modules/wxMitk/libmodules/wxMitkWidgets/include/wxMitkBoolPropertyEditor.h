/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef WXMITK_BOOLPROPERTYEDITOR_H_INCLUDED
#define WXMITK_BOOLPROPERTYEDITOR_H_INCLUDED

#include <wxMitkBoolPropertyView.h>

/// @ingroup wxMitkWidgets
class WXMITKWIDGETS_EXPORT wxMitkBoolPropertyEditor 
	: public wxMitkBoolPropertyView
{
  public:
    
    wxMitkBoolPropertyEditor( 
		const mitk::BoolProperty*, 
		wxWindow* parent, 
		const char* name = 0 );
    virtual ~wxMitkBoolPropertyEditor();
      
  protected:
    virtual void PropertyRemoved();
	void onToggle(wxCommandEvent &event); // wxGlade: <event_handler>

    wxDECLARE_EVENT_TABLE();
};

#endif

