/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef wxMitkCustomTransferFunctionSelector_H
#define wxMitkCustomTransferFunctionSelector_H

#include "wxMitkWidgetsWin32Header.h"

namespace mitk
{
/*
The wxMitkCustomTransferFunctionSelector is responsible for providing 
custom transferfunctions to the user, so he can quickly select the one he 
likes. 

\ingroup wxMitkWidgets
\author Juan Antonio Moya
\date 14 Dic 2007
*/
class WXMITKWIDGETS_EXPORT wxMitkCustomTransferFunctionSelector 
	: public wxChoice
{
public:

	wxMitkCustomTransferFunctionSelector(
		wxWindow *parent, 
		wxWindowID id, 
		const wxPoint& pos, 
		const wxSize& size, 
		long style = 0, 
		const wxValidator& validator = wxDefaultValidator, 
		const wxString& name = wxT("wxMitkCustomTransferFunctionSelector"));
	virtual ~wxMitkCustomTransferFunctionSelector(void);

};
 
} // namespace mitk


#endif // wxMitkCustomTransferFunctionSelector_H
