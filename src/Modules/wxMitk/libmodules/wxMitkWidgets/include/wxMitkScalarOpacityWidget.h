/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef wxMitkScalarOpacityWidget_H
#define wxMitkScalarOpacityWidget_H

#include "wxMitkWidgetsWin32Header.h"
#include "wx/panel.h"
#include <mitkHistogramGenerator.h>

class vtkPiecewiseFunction;
class wxStaticText;

namespace mitk
{
class wxMitkScalarOpacityControl;
class wxMitkMouseOverHistogramEvent;

/*
The wxMitkScalarOpacityWidget is a custom widget for editing transfer 
functions for volume rendering.

\ingroup wxMitkWidgets
\author Juan Antonio Moya
\date 11 Dec 2007
*/
class WXMITKWIDGETS_EXPORT wxMitkScalarOpacityWidget : public wxPanel
{
public:

	wxMitkScalarOpacityWidget(
		wxWindow* parent, 
		wxWindowID id, 
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = 0, 
		const wxString& name = wxT("wxMitkScalarOpacityWidget"));
	virtual ~wxMitkScalarOpacityWidget(void);

	void SetPiecewiseFunction(vtkPiecewiseFunction* func);
	vtkPiecewiseFunction* GetPiecewiseFunction(void) const;

	void SetHistogram(const mitk::HistogramGenerator::HistogramType* histogram);
	void SetHistogramColor(const wxColour& colour);
	
	const wxColour& GetHistogramColor(void) const;

	void SetInteractiveApply(bool enable);
	void ResetWidget(void);

private:

	void OnMousePositionChangedOverHistogram(wxMitkMouseOverHistogramEvent& event);

	wxMitkScalarOpacityControl* opacityFunctionControl;

	wxStaticText* lblRange;
	wxStaticText* lblScalar;
	
    wxDECLARE_EVENT_TABLE();
};
 
} // namespace mitk


#endif // wxMitkScalarOpacityWidget_H
