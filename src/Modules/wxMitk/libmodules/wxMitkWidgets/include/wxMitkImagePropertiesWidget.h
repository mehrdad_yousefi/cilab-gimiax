/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef wxMitkImagePropertiesWidget_H
#define wxMitkImagePropertiesWidget_H

#include "wxMitkWidgetsWin32Header.h"
#include <mitkProperties.h>
#include <wx/panel.h>

class wxCheckBox;
class wxCommandEvent;
class wxComboBox;
class wxSlider;
class wxNotebook;

namespace mitk
{
class DataTreeNode;
class wxMitkTransferFunctionWidget;
class wxMitkMaterialSettingsPanel;
class wxMitkImageSettingsWidget;
class wxMitkRenderWindow;
class wxMitkWidgetUpdateEvent;

/*
Edit volume image properties: general settings, volume rendering and material settings

\ingroup wxMitkWidgets
\author Xavi Planes
\date Jan 2013
*/
class WXMITKWIDGETS_EXPORT wxMitkImagePropertiesWidget : public wxPanel
{
public:

	wxMitkImagePropertiesWidget(
		wxWindow* parent, 
		wxWindowID id, 
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = 0, 
		const wxString& name = wxT("wxMitkImagePropertiesWidget"));
	virtual ~wxMitkImagePropertiesWidget(void);

	void SetDataTreeNode(mitk::DataTreeNode* node);

	/** 
	\brief Set/Get render window to show the legend. 
	\note This functionality should be inside MITK
	*/
	void SetMitkRenderWindow( mitk::wxMitkRenderWindow* renderWindow );

	mitk::wxMitkRenderWindow* GetMitkRenderWindow( ) const;

	/**
	\brief Disable the volume rendering option. This is used for images than do not have volume rendering properties, like a ROI.
	\param disable The value to set the rendering to.
	*/
	void DisableVolumeRendering(bool disable);

	//!
	void UpdateAll();

	//!
	void SetVolumeRenderingCheck(bool enableVR, bool enableXR, int vrThreshold = -1, int presetMode = -1);

	//!
	void SetPresetsFolder( const std::string &folder );
	std::string GetPresetsFolder( ) const;

	//!
	wxMitkTransferFunctionWidget* GetTransferFunctionWidget( );

private:
	void do_layout( );

	//!
	void ResetWidget(void);

	//!
	void UpdateWidgets( );

	//!
	void UpdateData( );

	//!
	void CleanWidgets( );

    wxDECLARE_EVENT_TABLE();

private:

	wxNotebook* m_ImageNotebook;
	wxMitkTransferFunctionWidget* m_TransferFunctionWidget;
	wxMitkImageSettingsWidget* m_ImageSettingsWidget;

	mitk::wxMitkRenderWindow* m_renderWindow;
	mitk::DataTreeNode* m_node;

	//!
	std::string m_PresetsFolder;
};
 
} // namespace mitk


#endif // wxMitkImagePropertiesWidget_H
