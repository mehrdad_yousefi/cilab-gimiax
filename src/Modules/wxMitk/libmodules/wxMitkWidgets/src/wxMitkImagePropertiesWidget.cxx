/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"

#include <wx/wxprec.h>

#ifndef WX_PRECOMP
       #include <wx/wx.h>
#endif

#include <wx/notebook.h>
#include <wx/wupdlock.h>

#include "wxMitkImagePropertiesWidget.h"
#include "wxMitkTransferFunctionWidget.h"
#include "wxMitkImageSettingsWidget.h"
#include "wxMitkSurfaceLightingControl.h"

#include <mitkDataTreeNode.h>
#include <mitkRenderingManager.h>

#include "wxID.h"

#include "blMITKUtils.h"


using namespace mitk;

#define wxID_ImageNotebook wxID("ID_ImageNotebook")


// Declare events to process
BEGIN_EVENT_TABLE(wxMitkImagePropertiesWidget, wxPanel)
END_EVENT_TABLE()


//!
wxMitkImagePropertiesWidget::wxMitkImagePropertiesWidget(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name) 
: wxPanel(parent, id, pos, size, style, name)
{
	m_renderWindow = NULL;
	m_node = NULL;

	SetId(wxID_ANY);
	m_ImageNotebook = new wxNotebook(this, wxID_ImageNotebook, wxDefaultPosition, wxDefaultSize, wxNB_TOP);
	m_ImageSettingsWidget = new wxMitkImageSettingsWidget(m_ImageNotebook, wxID_ANY);
	m_TransferFunctionWidget = new wxMitkTransferFunctionWidget(m_ImageNotebook, wxID_ANY);


	//////////////////////////////////////////////////////////////////////////////
	// Set default state
	CleanWidgets();

	do_layout( );
}

//!
wxMitkImagePropertiesWidget::~wxMitkImagePropertiesWidget(void)
{
}

void wxMitkImagePropertiesWidget::do_layout( )
{

	//////////////////////////////////////////////////////////////////////////////
	// Layout the canvases
	wxBoxSizer* layout = new wxBoxSizer(wxVERTICAL);
	m_ImageNotebook->AddPage(m_ImageSettingsWidget,wxT("Image Settings"));
	m_ImageNotebook->AddPage(m_TransferFunctionWidget,wxT("Volume Rendering"));

	layout->Add(m_ImageNotebook,1, wxEXPAND, 10);
	SetSizer(layout);
	layout->Fit(this);
	Layout();
}


//!
void wxMitkImagePropertiesWidget::ResetWidget(void)
{
	SetDataTreeNode( NULL );
}

void wxMitkImagePropertiesWidget::SetDataTreeNode(mitk::DataTreeNode* node)
{
	if ( node == NULL && m_node == NULL )
		return;

	m_node = node;
	m_ImageSettingsWidget->SetDataTreeNode( node );
	m_TransferFunctionWidget->SetDataTreeNode( node );

	UpdateWidgets();
}

void wxMitkImagePropertiesWidget::DisableVolumeRendering(bool disable)
{
	m_TransferFunctionWidget->DisableVolumeRendering(disable);
}

void mitk::wxMitkImagePropertiesWidget::SetMitkRenderWindow( mitk::wxMitkRenderWindow* renderWindow )
{
	m_renderWindow = renderWindow;
	m_ImageSettingsWidget->SetMitkRenderWindow( renderWindow );
	m_TransferFunctionWidget->SetMitkRenderWindow( renderWindow );
}

mitk::wxMitkRenderWindow* mitk::wxMitkImagePropertiesWidget::GetMitkRenderWindow( ) const
{
	return m_renderWindow;
}

void mitk::wxMitkImagePropertiesWidget::UpdateWidgets()
{
	m_ImageSettingsWidget->UpdateWidgets( );
	m_TransferFunctionWidget->UpdateWidgets( );
}

void mitk::wxMitkImagePropertiesWidget::UpdateData()
{
	m_TransferFunctionWidget->UpdateData( );
}

void mitk::wxMitkImagePropertiesWidget::UpdateAll()
{
	Update();
	UpdateData( );
	UpdateWidgets( );
}

void mitk::wxMitkImagePropertiesWidget::CleanWidgets()
{
	m_ImageSettingsWidget->CleanWidgets( );
	m_TransferFunctionWidget->CleanWidgets( );
}

void mitk::wxMitkImagePropertiesWidget::SetPresetsFolder( const std::string &folder )
{
	m_PresetsFolder = folder;
	m_TransferFunctionWidget->SetPresetsFolder( m_PresetsFolder + "/TransferFunctions/" );
}

std::string mitk::wxMitkImagePropertiesWidget::GetPresetsFolder( ) const
{
	return m_PresetsFolder;
}

wxMitkTransferFunctionWidget* mitk::wxMitkImagePropertiesWidget::GetTransferFunctionWidget( )
{
	return m_TransferFunctionWidget;
}
