/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef WXMITKVIEWCONFIGURATION_H
#define WXMITKVIEWCONFIGURATION_H

#include "wxMitkViewConfigurationUI.h"
#include "wxMitkWidgetsWin32Header.h"


namespace mitk
{
class wxMitkSelectableGLWidget;

/**
\author Xavi Planes
\ingroup wxMitkWidgets
\date 10 Jan 2009
*/
class WXMITKWIDGETS_EXPORT wxMitkViewConfiguration : public wxMitkViewConfigurationUI {
public:
	//!
    wxMitkViewConfiguration(wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

	//!
	~wxMitkViewConfiguration( );

	//! Set view to manage
	void SetView( mitk::wxMitkSelectableGLWidget* view );
	mitk::wxMitkSelectableGLWidget* GetView( );

	//!
	mitk::SliceNavigationController::ViewDirection GetDirection( );

	//!
	bool GetFlip( );

	//!
	mitk::ScalarType GetAngleOfRotation( );


private:
	//!
	void UpdateWidget( );

    virtual void OnDirection(wxCommandEvent &event);
    virtual void OnFlip(wxCommandEvent &event);
    virtual void OnRotate(wxCommandEvent &event);
	virtual void OnApplyParameters(wxCommandEvent &event);
	virtual void OnAdvanced(wxCommandEvent &event);

	//!
	void UpdatePlaneParameters( );
protected:

	//!
	mitk::wxMitkSelectableGLWidget* m_View;

	//!
	unsigned long m_ObserverTag;

	//!
	unsigned long m_ObserverTagStepper;

};

} // namespace mitk

#endif // WXMITKVIEWCONFIGURATION_H
