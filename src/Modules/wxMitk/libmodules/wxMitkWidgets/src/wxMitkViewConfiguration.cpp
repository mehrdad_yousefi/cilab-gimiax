/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "wxMitkViewConfiguration.h"
#include "wxMitkSelectableGLWidget.h"
#include "wx/wupdlock.h"

mitk::wxMitkViewConfiguration::wxMitkViewConfiguration(wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    wxMitkViewConfigurationUI(parent, id, pos, size, wxTAB_TRAVERSAL)
{
	m_ObserverTag = 0;
	m_View = 0;

	m_btnApply->GetContainingSizer()->Show( m_chkAdvanced->GetValue() );
	GetSizer()->Layout();
}

mitk::wxMitkViewConfiguration::~wxMitkViewConfiguration()
{
	// Don't remove observer because m_View is destroyed 
}

void mitk::wxMitkViewConfiguration::OnDirection(wxCommandEvent &event)
{
	if ( m_View )
	{
		mitk::SliceNavigationController::Pointer navigationController;
		navigationController = m_View->GetSliceNavigationController( );
		navigationController->SetViewDirection( this->GetDirection( ) );
		navigationController->Update( );
	}

	event.Skip();
}


void mitk::wxMitkViewConfiguration::OnFlip(wxCommandEvent &event)
{
	if ( m_View )
	{
		mitk::SliceNavigationController::Pointer navigationController;
		navigationController = m_View->GetSliceNavigationController( );
		navigationController->SetFrontSide( this->GetFlip() );
		navigationController->Update( );
	}

	event.Skip();
}


void mitk::wxMitkViewConfiguration::OnRotate(wxCommandEvent &event)
{
	if ( m_View )
	{
		// When SetRotated( ) is called, the widget will be updated
		// so we need to store the local value
		mitk::ScalarType angle = this->GetAngleOfRotation();
		mitk::SliceNavigationController::Pointer navigationController;
		navigationController = m_View->GetSliceNavigationController( );
		navigationController->SetRotated( angle != 0 );
		navigationController->SetAngleOfRotation( angle );
		navigationController->Update( );
	}

	event.Skip();
}

void mitk::wxMitkViewConfiguration::SetView( mitk::wxMitkSelectableGLWidget* view )
{
	if ( m_View )
	{
		m_View->GetSliceNavigationController()->RemoveObserver( m_ObserverTag );
		m_View->GetSliceNavigationController()->GetSlice()->RemoveObserver( m_ObserverTagStepper );
	}

	m_View = view;

	if ( m_View )
	{
		itk::SimpleMemberCommand<wxMitkViewConfiguration>::Pointer command;
		command = itk::SimpleMemberCommand<wxMitkViewConfiguration>::New();
		command->SetCallbackFunction(this, &wxMitkViewConfiguration::UpdateWidget);
		m_ObserverTag = m_View->GetSliceNavigationController()->AddObserver( 
			itk::ModifiedEvent( ), command );

		command = itk::SimpleMemberCommand<wxMitkViewConfiguration>::New();
		command->SetCallbackFunction(this, &wxMitkViewConfiguration::UpdatePlaneParameters);
		m_ObserverTagStepper = m_View->GetSliceNavigationController()->GetSlice()->AddObserver( 
			itk::ModifiedEvent( ), command );

		UpdateWidget( );
	}
}

void mitk::wxMitkViewConfiguration::UpdateWidget( )
{
	if ( !m_View )
	{
		return;
	}

	mitk::SliceNavigationController::Pointer navigationController;
	navigationController = m_View->GetSliceNavigationController( );

	mitk::SliceNavigationController::ViewDirection direction;
	direction = navigationController->GetViewDirection(  );
	m_cmbDirection->SetSelection( direction );

	m_chkFlip->SetValue( navigationController->GetFrontSide( ) );

	int iAngle = 0;
	if ( navigationController->GetRotated( ) )
	{
		if ( navigationController->GetAngleOfRotation( ) == 0 )
		{
			iAngle = 0;
		}
		else if ( navigationController->GetAngleOfRotation( ) == 90 )
		{
			iAngle = 1;
		}
		else if ( navigationController->GetAngleOfRotation( ) == 180 )
		{
			iAngle = 2;
		}
		else if ( navigationController->GetAngleOfRotation( ) == 270 )
		{
			iAngle = 3;
		}
	}

	m_rdbxRotate->SetSelection( iAngle );


	UpdatePlaneParameters();
}

bool mitk::wxMitkViewConfiguration::GetFlip()
{
	return m_chkFlip->GetValue( );
}

mitk::ScalarType mitk::wxMitkViewConfiguration::GetAngleOfRotation()
{
	switch ( m_rdbxRotate->GetSelection( ) )
	{
	case 0:return 0;break;
	case 1:return 90;break;
	case 2:return 180;break;
	case 3:return 270;break;
	}

	return 0;
}

mitk::SliceNavigationController::ViewDirection mitk::wxMitkViewConfiguration::GetDirection()
{
	mitk::SliceNavigationController::ViewDirection direction;
	direction = mitk::SliceNavigationController::ViewDirection( m_cmbDirection->GetSelection( ) );
	return direction;
}

mitk::wxMitkSelectableGLWidget* mitk::wxMitkViewConfiguration::GetView()
{
	return m_View;
}

void mitk::wxMitkViewConfiguration::OnApplyParameters( wxCommandEvent &event )
{
	const mitk::PlaneGeometry* planeGeometry = m_View->GetGeometry2D();
	if ( planeGeometry )
	{
		VnlVector rightVector( 3 );
		double tempVal;
		m_txtRightVector0->GetValue().ToDouble( &tempVal );
		rightVector[ 0 ] = tempVal;
		m_txtRightVector1->GetValue().ToDouble( &tempVal );
		rightVector[ 1 ] = tempVal;
		m_txtRightVector2->GetValue().ToDouble( &tempVal );
		rightVector[ 2 ] = tempVal;

		VnlVector downVector( 3 );
		m_txtDownVector0->GetValue().ToDouble( &tempVal );
		downVector[ 0 ] = tempVal;
		m_txtDownVector1->GetValue().ToDouble( &tempVal );
		downVector[ 1 ] = tempVal;
		m_txtDownVector2->GetValue().ToDouble( &tempVal );
		downVector[ 2 ] = tempVal;

		Point3D origin;
		m_txtOrigin0->GetValue().ToDouble( &tempVal );
		origin[ 0 ] = tempVal;
		m_txtOrigin1->GetValue().ToDouble( &tempVal );
		origin[ 1 ] = tempVal;
		m_txtOrigin2->GetValue().ToDouble( &tempVal );
		origin[ 2 ] = tempVal;

		VnlVector normal  = vnl_cross_3d(rightVector, downVector); 
		normal.normalize();

		mitk::Vector3D mitkNormal;
		mitkNormal.Set_vnl_vector( normal );
		m_View->GetSliceNavigationController( )->ReorientSlices( origin, mitkNormal );

		// Is not possible to update the plane geometry because it will break the navigator
		//planeGeometry->SetOrigin( origin );
		//planeGeometry->InitializeStandardPlane( rightVector, downVector );

		// Refresh real values
		UpdatePlaneParameters( );
	}
}

void mitk::wxMitkViewConfiguration::UpdatePlaneParameters()
{
	const mitk::PlaneGeometry* planeGeometry = m_View->GetGeometry2D();
	if ( planeGeometry )
	{
		m_txtOrigin0->SetValue( wxString::Format( "%.2f", planeGeometry->GetOrigin()[ 0 ] ) );
		m_txtOrigin1->SetValue( wxString::Format( "%.2f", planeGeometry->GetOrigin()[ 1 ] ) );
		m_txtOrigin2->SetValue( wxString::Format( "%.2f", planeGeometry->GetOrigin()[ 2 ] ) );

		const AffineTransform3D* transform = planeGeometry->GetIndexToWorldTransform( );
		VnlVector rightVector = transform->GetMatrix( ).GetVnlMatrix().get_column( 0 );
		VnlVector downVector = transform->GetMatrix( ).GetVnlMatrix().get_column( 1 );

		m_txtDownVector0->SetValue( wxString::Format( "%.2f", downVector[ 0 ] ) );
		m_txtDownVector1->SetValue( wxString::Format( "%.2f", downVector[ 1 ] ) );
		m_txtDownVector2->SetValue( wxString::Format( "%.2f", downVector[ 2 ] ) );

		m_txtRightVector0->SetValue( wxString::Format( "%.2f", rightVector[ 0 ] ) );
		m_txtRightVector1->SetValue( wxString::Format( "%.2f", rightVector[ 1 ] ) );
		m_txtRightVector2->SetValue( wxString::Format( "%.2f", rightVector[ 2 ] ) );
	}

}

void mitk::wxMitkViewConfiguration::OnAdvanced( wxCommandEvent &event )
{
	wxWindowUpdateLocker lock( GetParent() );

	m_btnApply->GetContainingSizer()->Show( m_chkAdvanced->GetValue() );

	// Force re layout the window to parent and parents of it
	wxSizeEvent sizeEvent(GetParent()->GetSize(), GetParent()->GetId());
	sizeEvent.SetEventObject(GetParent());
	GetParent()->GetEventHandler()->ProcessEvent(sizeEvent);
}
