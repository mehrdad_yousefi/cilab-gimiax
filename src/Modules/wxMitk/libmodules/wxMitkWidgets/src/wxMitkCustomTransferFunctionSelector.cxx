/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
       #include <wx/wx.h>
#endif

#include "wxMitkCustomTransferFunctionSelector.h"

using namespace mitk;


//!
wxMitkCustomTransferFunctionSelector::wxMitkCustomTransferFunctionSelector(wxWindow *parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator, const wxString& name)
: wxChoice(parent, id, pos, size, 0, NULL, style, validator, name)
{
}

//!
wxMitkCustomTransferFunctionSelector::~wxMitkCustomTransferFunctionSelector(void)
{
}

