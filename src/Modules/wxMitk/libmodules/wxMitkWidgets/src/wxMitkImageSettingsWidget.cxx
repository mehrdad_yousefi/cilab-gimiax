/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"

#include <wx/wxprec.h>

#ifndef WX_PRECOMP
       #include <wx/wx.h>
#endif

#include <wx/wupdlock.h>

#include "wxMitkImageSettingsWidget.h"
#include "wxID.h"
#include "wxMitkLookupTableWidget.h"
#include "mitkVtkResliceInterpolationProperty.h"
#include "wxMitkRenderWindow.h"

using namespace mitk;

#define wxID_sliderOpacity wxID("ID_sliderOpacity")
#define wxID_chkTextureInterpolation wxID( "wxID_chkTextureInterpolation" )
#define wxID_cmbResliceInterpolation wxID( "wxID_cmbResliceInterpolation" )
#define wxID_chkStereo wxID("wxID_chkStereo")
#define wxID_cmbStereoType wxID( "wxID_cmbStereoType" )
#define wxID_LookupTable wxID("wxID_LookupTable")

// Declare events to process
BEGIN_EVENT_TABLE(wxMitkImageSettingsWidget, wxPanel)
	EVT_COMMAND_SCROLL      (wxID_sliderOpacity, wxMitkImageSettingsWidget::OnIntensitySliderChanged)
	EVT_CHECKBOX	(wxID_chkTextureInterpolation, wxMitkImageSettingsWidget::OnTextureInterpolation)
	EVT_COMBOBOX	(wxID_cmbResliceInterpolation,wxMitkImageSettingsWidget::OnResliceInterpolation )
	EVT_CHECKBOX	(wxID_chkStereo,wxMitkImageSettingsWidget::OnStereo)
	EVT_COMBOBOX	(wxID_cmbStereoType,wxMitkImageSettingsWidget::OnStereoType )
END_EVENT_TABLE()


#define HIGHRESAMPLING 0.5
#define FASTRESAMPLING 1

//!
wxMitkImageSettingsWidget::wxMitkImageSettingsWidget(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name) 
: wxPanel(parent, id, pos, size, style, name)
{
	m_renderWindow = NULL;
	m_node = NULL;


	m_LookupTableWidget = new wxMitkLookupTableWidget(this, wxID_LookupTable);
	sizer_1_staticbox = new wxStaticBox(this, -1, wxT("Opacity"));
	m_OpacitySlider = new wxSlider(this, wxID_sliderOpacity, 100, 0, 100, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL | wxSL_LABELS);
	m_chkTextureInterpolation = new wxCheckBox(
		this, 
		wxID_chkTextureInterpolation, 
		wxT("Texture interpolation"));
	m_cmbResliceInterpolation = new wxComboBox(
		this, 
		wxID_cmbResliceInterpolation, 
		wxT(""), 
		wxDefaultPosition, 
		wxDefaultSize, 
		0, 
		(const wxString*) NULL, 
		wxCB_DROPDOWN|wxCB_SIMPLE|wxCB_READONLY);
	m_cmbResliceInterpolation->Append( "Nearest" );
	m_cmbResliceInterpolation->Append( "Linear" );
	m_cmbResliceInterpolation->Append( "Reserved" );
	m_cmbResliceInterpolation->Append( "Cubic" );

	m_chkStereo = new wxCheckBox(
		this, 
		wxID_chkStereo, 
		wxT("Stereo Visualization"));

	m_cmbStereoType = new wxComboBox(
		this, 
		wxID_cmbStereoType, 
		wxT(""), 
		wxDefaultPosition, 
		wxDefaultSize,
		0,
		(const wxString*) NULL, 
		wxCB_DROPDOWN|wxCB_SIMPLE|wxCB_READONLY);

	m_mapStereoTypes.clear();
	m_mapStereoTypes.push_back(pair<std::string,int>("Crystal Eye",VTK_STEREO_CRYSTAL_EYES));
	m_mapStereoTypes.push_back(pair<std::string,int>("Red Blue",VTK_STEREO_RED_BLUE));
	m_mapStereoTypes.push_back(pair<std::string,int>("Interlaced",VTK_STEREO_INTERLACED));
	m_mapStereoTypes.push_back(pair<std::string,int>("Left only",VTK_STEREO_LEFT));
	m_mapStereoTypes.push_back(pair<std::string,int>("Right only",VTK_STEREO_RIGHT));
	m_mapStereoTypes.push_back(pair<std::string,int>("Dresden",VTK_STEREO_DRESDEN));
	m_mapStereoTypes.push_back(pair<std::string,int>("Anaglyph",VTK_STEREO_ANAGLYPH));
	m_mapStereoTypes.push_back(pair<std::string,int>("Check Board",VTK_STEREO_CHECKERBOARD));

	for(int i=0; i<m_mapStereoTypes.size(); i++)
		m_cmbStereoType->Append(m_mapStereoTypes.at(i).first);

	m_cmbStereoType->SetSelection(m_cmbStereoType->GetCount()-2); //Anaglyph

	// Set default state
	CleanWidgets();

	do_layout( );
}

//!
wxMitkImageSettingsWidget::~wxMitkImageSettingsWidget(void)
{
}

void wxMitkImageSettingsWidget::do_layout( )
{
	//////////////////////////////////////////////////////////////////////////////
	// Image settings
	wxBoxSizer* imageSettingSizer = new wxBoxSizer(wxVERTICAL);
	imageSettingSizer->Add( m_LookupTableWidget, 0 );
	imageSettingSizer->AddSpacer(16);
	wxStaticBoxSizer* sizer_1 = new wxStaticBoxSizer(sizer_1_staticbox, wxVERTICAL);
	sizer_1->Add(m_OpacitySlider, 0, wxEXPAND, 0);
	imageSettingSizer->Add(sizer_1,0, wxEXPAND, 0);
	imageSettingSizer->Add( m_chkTextureInterpolation, 0, wxEXPAND | wxALL, 5 );
	imageSettingSizer->Add( m_cmbResliceInterpolation, 0, wxEXPAND | wxALL, 5 );
	m_cmbResliceInterpolation->SetInitialSize( m_cmbResliceInterpolation->GetSize() ); // due to bug in wxWidgets 3.0.2, set the initial size or dropbox will not display correctly. 
	imageSettingSizer->AddSpacer(5);
	imageSettingSizer->Add( m_chkStereo, 0, wxEXPAND | wxALL, 5);
	imageSettingSizer->Add( m_cmbStereoType, 0, wxEXPAND | wxALL, 5);
	m_cmbStereoType->SetInitialSize( m_cmbStereoType->GetSize() ); // due to bug in wxWidgets 3.0.2, set the initial size or dropbox will not display correctly
	imageSettingSizer->AddStretchSpacer(1);
	SetSizer(imageSettingSizer);
	imageSettingSizer->Fit(this);
	Layout();
	
}

void wxMitkImageSettingsWidget::SetDataTreeNode(mitk::DataTreeNode* node)
{
	m_node = node;

	m_LookupTableWidget->SetDataTreeNode( node );

	UpdateWidgets();
}

//!
void wxMitkImageSettingsWidget::OnIntensitySliderChanged(wxScrollEvent &event)
{
	float sliderValue = static_cast<float>(m_OpacitySlider->GetValue());
	m_node->SetOpacity(sliderValue/100);
	mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}

void mitk::wxMitkImageSettingsWidget::SetMitkRenderWindow( mitk::wxMitkRenderWindow* renderWindow )
{
	m_renderWindow = renderWindow;
	m_LookupTableWidget->SetMitkRenderWindow( renderWindow );
}

mitk::wxMitkRenderWindow* mitk::wxMitkImageSettingsWidget::GetMitkRenderWindow( ) const
{
	return m_renderWindow;
}

void mitk::wxMitkImageSettingsWidget::UpdateWidgets()
{
	if ( m_node == NULL )
	{
		CleanWidgets( );
		return;
	}

	// Update the value of the slider
	//float sliderValue = static_cast<float>(m_OpacitySlider->GetValue());
	float opacity;
	m_node->GetOpacity(opacity, NULL);
	m_OpacitySlider->SetValue(opacity*100);

	// Update texture interpolation
	bool textureInterpolation = false;
	m_node->GetBoolProperty( "texture interpolation", textureInterpolation );
	m_chkTextureInterpolation->SetValue( textureInterpolation );

	VtkResliceInterpolationProperty *resliceInterpolationProperty;
	m_node->GetProperty( resliceInterpolationProperty, "reslice interpolation" );
	int interpolationMode = VTK_RESLICE_NEAREST;
	if ( resliceInterpolationProperty != NULL )
	{
		interpolationMode = resliceInterpolationProperty->GetInterpolation();
	}
	m_cmbResliceInterpolation->Select( interpolationMode );
}

void mitk::wxMitkImageSettingsWidget::CleanWidgets()
{
	m_chkStereo->SetValue(false);
	m_LookupTableWidget->ResetWidget();
}
void mitk::wxMitkImageSettingsWidget::OnTextureInterpolation( wxCommandEvent& event )
{
	m_node->SetProperty("texture interpolation", mitk::BoolProperty::New(event.GetInt( ) ));
	mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}

int mitk::wxMitkImageSettingsWidget::GetSelectedStereoType()
{
	int type = -1;
	int currSel = m_cmbStereoType->GetCurrentSelection();
	if((currSel>=0) && (currSel<m_mapStereoTypes.size()))
		type = m_mapStereoTypes.at(currSel).second;
	return type;
}


void mitk::wxMitkImageSettingsWidget::SetStereo(bool onOff)
{
	if((m_renderWindow==NULL))
		return;

	vtkSmartPointer<vtkRenderer> vtkrenderer;
	vtkrenderer = mitk::BaseRenderer::GetInstance(m_renderWindow->GetRenderWindow())->GetVtkRenderer();
	if(vtkrenderer!=NULL)
	{
		int type = GetSelectedStereoType();
		if(type>0)
		{
			std::cout<<"***Setting the stereo mode to "<<type<<std::endl;
			vtkrenderer->GetRenderWindow()->SetStereoType(type);
		}
		else
			return;
		vtkrenderer->GetRenderWindow()->SetStereoRender(onOff);
		vtkrenderer->GetRenderWindow()->Render();
	}
}
void mitk::wxMitkImageSettingsWidget::OnStereo( wxCommandEvent& event )
{
	SetStereo(m_chkStereo->GetValue());
}

void mitk::wxMitkImageSettingsWidget::OnStereoType( wxCommandEvent& event )
{
	SetStereo(m_chkStereo->GetValue());
}


void mitk::wxMitkImageSettingsWidget::OnResliceInterpolation( wxCommandEvent& event )
{
	m_node->SetProperty("reslice interpolation", 
			mitk::VtkResliceInterpolationProperty::New( event.GetInt( ) ) );
	mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}
