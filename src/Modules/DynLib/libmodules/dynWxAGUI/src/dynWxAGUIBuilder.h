/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _dynWxAGUIBuilder_H
#define _dynWxAGUIBuilder_H

#include "DynWxAGUILibWin32Header.h"
#include "ModuleDescription.h"
#include "dynWxGUIUpdater.h"
#include "dynBasePanel.h"
#include "dynWxControlFactory.h"

class wxPanel;
class wxWindow;
class wxSizer;

/**
Automated GUI Builder for wxWidgets

\author Xavi Planes
\date 15 July 2010
\ingroup DynLib
*/
class DYNWXAGUILIB_EXPORT dynWxAGUIBuilder
{
public:
	//!
	dynWxAGUIBuilder( );

	//!
	virtual ~dynWxAGUIBuilder( );

	//!
	ModuleDescription *GetModule() const;
	void SetModule(ModuleDescription *val);

	//!
	wxWindow* GetParentWindow() const;
	void SetParentWindow(wxWindow* val);

	//!
	void SetBuildHelp( bool val );
	
	//!
	void SetBuildParameterGroups( bool val );

	//!
	void SetBuildApplyButton( bool val );

	//!
	void SetBuildAdvancedOptions( bool val );

	//!
	void Update( );

	//!
	dynBasePanel* GetPanel() const;

	//!
	void SetControlFactory(dynWxControlFactoryBase::Pointer val);

	//!
	void RemoveEmptyGroups( );

protected:

	//!
	void AddHelp( );

	//!
	void AddParameterGroups( );

	//!
	void AddApplyButton( );

	/** A checkbox is added only when AlternativeType is "CommandLineModule"
	"Run as executable"
	*/
	void AddAdvancedOptions( );

	//!
	void AddParameterGroup( const ModuleParameterGroup &parameterGroup );

	//!
	void AddParameter( 
		wxWindow* parent, 
		const ModuleParameter& moduleParameter );

private:

	//!
	bool m_BuildHelp;
	
	//!
	bool m_BuildParameterGroups;

	//!
	bool m_BuildApplyButton;

	//!
	bool m_BuildAdvancedOptions;

	//!
	ModuleDescription *m_Module;

	//!
	dynBasePanel* m_Panel;

	//!
	wxWindow* m_ParentWindow;

	//!
	dynWxControlFactoryBase::Pointer m_ControlFactory;
};



#endif // _dynWxAGUIBuilder_H

