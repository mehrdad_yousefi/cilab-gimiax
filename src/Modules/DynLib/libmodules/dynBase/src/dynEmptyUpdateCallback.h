/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _dynEmptyUpdateCallback_H
#define _dynEmptyUpdateCallback_H

#include "DynLibWin32Header.h"
#include "dynUpdateCallback.h"
#include "blLightObject.h"

/**
Update callback interface for module execution

\author Xavi Planes
\date Jan 2011
\ingroup DynLib
*/
class DYNLIB_EXPORT dynEmptyUpdateCallback 
	: public dynUpdateCallback
{
public:
	typedef dynEmptyUpdateCallback Self;
	typedef blSmartPointer<Self> Pointer;
	itkFactorylessNewMacro( Self );

	//! Empty function
	virtual void Modified( );

	//! Empty function
	virtual bool GetDetachProcessing( );

protected:

	//!
	dynEmptyUpdateCallback( );

	//!
	virtual ~dynEmptyUpdateCallback( );

private:
};



#endif // _dynEmptyUpdateCallback_H

