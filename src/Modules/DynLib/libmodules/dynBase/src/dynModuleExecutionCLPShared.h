/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _dynModuleExecutionCLPShared_H
#define _dynModuleExecutionCLPShared_H

#include "DynLibWin32Header.h"
#include "dynModuleExecutionCLPBase.h"
#include "ModuleDescription.h"
#include <list>
#include <map>
#include <itksys/Process.h>
#include "itksys/DynamicLoader.hxx"

/**
Dynamic ModuleDescription execution

Executes a ModuleDescription located in a dynamic library

\author Xavi Planes
\date 14 July 2010
\ingroup DynLib
*/
class DYNLIB_EXPORT dynModuleExecutionCLPShared : public dynModuleExecutionCLPBase
{
public:
	typedef dynModuleExecutionCLPShared Self;
	typedef blSmartPointer<Self> Pointer;
	blNewMacro(Self);
	defineModuleFactory( dynModuleExecutionCLPShared );
	virtual dynModuleExecutionImpl::Pointer CreateAnother(void) const
	{
		dynModuleExecutionImpl::Pointer smartPtr;
		smartPtr = New().GetPointer();
		return smartPtr;
	}

	//!
	static void ProgressCallback( void* callbackData );

protected:
	//!
	dynModuleExecutionCLPShared( );

	//!
	virtual ~dynModuleExecutionCLPShared( );

	//!
	void BuildCommandLineLocation( );

	//!
	void RunFilter( );

private:

	//! Redirect cout and cerr
	bool m_RedirectModuleStreams;

	//! Library handle
	itksys::DynamicLoader::LibraryHandle m_LibraryHandle;
};



#endif // _dynModuleExecutionCLPShared_H

