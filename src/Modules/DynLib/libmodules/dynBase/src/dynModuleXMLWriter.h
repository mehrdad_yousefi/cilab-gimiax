/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _dynModuleXMLWriter_H
#define _dynModuleXMLWriter_H

#include "DynLibWin32Header.h"
#include "ModuleDescription.h"
#include "blLightObject.h"
#include <list>
#include <map>
#include "dynUpdateCallback.h"
#include "CILabExceptionMacros.h"

/**
ModuleDescription XML writer

\author Xavi Planes
\date Jan 2012
\ingroup DynLib
*/
class DYNLIB_EXPORT dynModuleXMLWriter : public blLightObject
{

public:
	typedef dynModuleXMLWriter Self;
	typedef blSmartPointer<Self> Pointer;
	blNewMacro(Self);
	cilabDeclareExceptionMacro(Exception, std::exception)

	//!
	ModuleDescription *GetModule() const;
	void SetModule(ModuleDescription *val);

	//!
	std::string GetFileName( ) const;
	void SetFileName( const std::string &val );

	//!
	void Update( );

protected:

	//!
	dynModuleXMLWriter( );

	//!
	virtual ~dynModuleXMLWriter( );

private:
	//!
	ModuleDescription *m_Module;

	//!
	std::string m_FileName;
};



#endif // _dynModuleXMLWriter_H

