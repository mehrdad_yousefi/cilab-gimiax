/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "dynModuleExecution.h"
#include "dynModuleExecutionCLPShared.h"
#include <sstream>
#include <stdio.h>
#include <time.h>

#include <itksys/RegularExpression.hxx>
#include "itkExceptionObject.h"
#if defined(__APPLE__) && (MAC_OS_X_VERSION_MAX_ALLOWED >= 1030)
	#include "/usr/include/dlfcn.h"
#endif

typedef int (*ModuleEntryPoint)(int argc, char* argv[]);

// Use global variables to store original cout and cerr stream buffers
std::streambuf* origcoutrdbuf = NULL;
std::streambuf* origcerrrdbuf = NULL;


dynModuleExecutionCLPShared::dynModuleExecutionCLPShared( )
{
	m_RedirectModuleStreams = true;
	m_LibraryHandle = NULL;
}

dynModuleExecutionCLPShared::~dynModuleExecutionCLPShared()
{
	// Unload all DLLs before closing application. 

	// VTK CLP
	// Matching Cubes CLP uses VTK. If it's executed, it will keep a reference to vtkCommon.dll. 
	// vtkCommon will not be unloaded by MITK plugin, It will be unloaded 
	// by the main application after unloading all DLLs. However, vtkFilter.dll registers an instance
	// in vtkCommonInformationKeyManager::Register() and this static variable will 
	// acess incorrect memory adress when destroying it. The CLP DLL needs to be unloaded
	// before VTK is unloaded

	// ITK CLP
	// When itk CLP is executed, and when writing an image, ImageIOFactory::RegisterBuiltInFactories() 
	// is called in the memory space of the CLP DLL (because itkIO is static) and the static variable 
	// firstTime is false, but when ObjectFactoryBase::RegisterFactory() is called, the static variable 
	// ObjectFactoryBase::m_RegisteredFactories is global and
	// new objects allocated in the memory space of the DLL are registered to itkCommon.dll. 
	// To fix this error, when calling RegisterFactory(), checks if the factory is already registered
	if ( m_LibraryHandle )
	{
		itksys::DynamicLoader::CloseLibrary( m_LibraryHandle );
	}
}

void dynModuleExecutionCLPShared::BuildCommandLineLocation()
{
	m_CommandLineAsString.clear();

	if (GetModule( )->GetLocation() != std::string("") && 
		GetModule( )->GetLocation() != GetModule( )->GetTarget())
	{
		m_CommandLineAsString.push_back(GetModule( )->GetLocation());
	}

	// Add a command line flag for the process information structure
	m_CommandLineAsString.push_back( "--processinformationaddress" );

	char tname[256];
	sprintf(tname, "%p", GetModule()->GetProcessInformation());
	m_CommandLineAsString.push_back( tname );

}

void dynModuleExecutionCLPShared::RunFilter()
{
	int returnValue = 0;

#if defined(__APPLE__) && (MAC_OS_X_VERSION_MAX_ALLOWED >= 1030)
	// Mac OS X defaults to RTLD_GLOBAL and there is no way to
	// override in itksys. So make the direct call to dlopen().
	m_LibraryHandle = dlopen(GetModule()->GetLocation().c_str(), RTLD_LAZY | RTLD_LOCAL);
#else
	
	m_LibraryHandle = itksys::DynamicLoader::OpenLibrary(GetModule()->GetLocation().c_str());
#endif
	if ( !m_LibraryHandle )
	{
		return;
	}

	// Initialize global variables pointing to initial standard stream buffers
	if ( origcoutrdbuf == NULL && origcerrrdbuf == NULL )
	{
		origcoutrdbuf = std::cout.rdbuf();
		origcerrrdbuf = std::cerr.rdbuf();
	}

	// If streams should be redirected, but the original streams have been changed
	// throw an exception
	if ( this->m_RedirectModuleStreams && ( 
		  origcoutrdbuf != std::cout.rdbuf() ||
		  origcerrrdbuf != std::cerr.rdbuf( ) ) )
	{
		throw dynModuleExecution::Exception( "Cannot redirect standard streams" );
	}

	std::ostringstream coutstringstream;
	std::ostringstream cerrstringstream;

	try
	{
		if (this->m_RedirectModuleStreams )
		{
			// redirect the streams
			std::cout.rdbuf( coutstringstream.rdbuf() );
			std::cerr.rdbuf( cerrstringstream.rdbuf() );
		}

		ModuleEntryPoint entryPoint = NULL;
		entryPoint = (ModuleEntryPoint)itksys::DynamicLoader::GetSymbolAddress(m_LibraryHandle, "ModuleEntryPoint");

		GetModule()->GetProcessInformation()->SetProgressCallback( 
			dynModuleExecutionCLPShared::ProgressCallback, GetUpdateCallback() );

		// run the module
		if ( entryPoint != NULL ) {
			returnValue = (*entryPoint)(m_CommandLineAsString.size(), m_Command);
		}

		if (this->m_RedirectModuleStreams )
		{
			// reset the streams
			std::cout.rdbuf( origcoutrdbuf );
			std::cerr.rdbuf( origcerrrdbuf );
		}

		// report the output
		if (coutstringstream.str().size() > 0)
		{
			std::string tmp(" standard output:\n\n");
			tmp = GetModule()->GetTitle()+tmp;
			// Print to cout
			std::cout << tmp + coutstringstream.str();
			GetUpdateCallback()->AddInformationMessage( tmp + coutstringstream.str() );
			GetUpdateCallback()->Modified( );
		}
		
		if (cerrstringstream.str().size() > 0)
		{
			std::string tmp(" standard error:\n\n");
			tmp = GetModule()->GetTitle() + tmp + cerrstringstream.str();
			throw dynModuleExecution::Exception( tmp.c_str() );
		}
	}
	catch (itk::ExceptionObject& exc)
	{
		std::cout.rdbuf( origcoutrdbuf );
		std::cerr.rdbuf( origcerrrdbuf );

		std::stringstream information;
		if ( GetModule()->GetProcessInformation()->Abort )
		{
			information << GetModule()->GetTitle() << " cancelled.";
			throw dynModuleExecution::Exception( information.str().c_str() );
		}
		else
		{
			information << GetModule()->GetTitle()
				<< " terminated with an exception: " << exc;
			throw dynModuleExecution::Exception( information.str().c_str() );
		}
	}
	catch (std::exception& exc)
	{
		std::cout.rdbuf( origcoutrdbuf );
		std::cerr.rdbuf( origcerrrdbuf );

		std::stringstream information;
		information << GetModule()->GetTitle()
			<< " terminated with an exception: " << exc.what();
		throw dynModuleExecution::Exception( information.str().c_str() );
	}
	catch (...)
	{
		std::cout.rdbuf( origcoutrdbuf );
		std::cerr.rdbuf( origcerrrdbuf );

		std::stringstream information;
		information << GetModule()->GetTitle()
			<< " terminated with an unknown exception." << std::endl;

		throw dynModuleExecution::Exception( information.str().c_str() );
	}

	// Check the return status of the module
	if (returnValue)
	{
		std::cout.rdbuf( origcoutrdbuf );
		std::cerr.rdbuf( origcerrrdbuf );

		std::stringstream information;
		information << GetModule()->GetTitle()
			<< " returned " << returnValue << " which probably indicates an error." << std::endl;

		throw dynModuleExecution::Exception( information.str().c_str() );
	}

	// Is not possible to close the library because it creates a DataEntityIO instance
	//	itksys::DynamicLoader::CloseLibrary( lib );
}

void dynModuleExecutionCLPShared::ProgressCallback( void* callbackData )
{
	dynUpdateCallback* update = static_cast<dynUpdateCallback*> ( callbackData );
	update->Modified();
}

