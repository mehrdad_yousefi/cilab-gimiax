/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _dynModuleHelper_H
#define _dynModuleHelper_H

#include "DynLibWin32Header.h"
#include "ModuleDescription.h"

/**
Helper functions to search parameters

\author Xavi Planes
\date 05 Jan 2011
\ingroup DynLib
*/
class DYNLIB_EXPORT dynModuleHelper
{
public:

	//! channel == "input" and tag is not file or directory
	static long GetNumberOfInputs( ModuleDescription* module );

	//! channel == "output" and tag is not file or directory
	static long GetNumberOfOutputs( ModuleDescription* module );

	//! 
	static ModuleParameter *GetInput( ModuleDescription* module, size_t count );

	//! 
	static ModuleParameter *GetOutput( ModuleDescription* module, size_t count );

	//! Find a parameter using channel value and count
	static ModuleParameter *FindParameter( 
		ModuleDescription* module,
		std::string channel = "",
		int count = 0,
		std::string name = "",
		const std::string &tag ="" );

	//!
	static void* StringToPointer( const std::string name );

	//!
	static std::string PointerToString( void* ptr);

	//!
	static void RemoveParameter( ModuleDescription* module, ModuleParameter* param );

	//!
	static void RemoveParameter( 
		ModuleDescription* module, 
		const std::string &groupName,
		const std::string &parameterName );

	//!
	static void RemoveParameterGroup( 
		ModuleDescription* module, 
		const std::string &groupName );

	//!
	static bool FindParameterGroup( 
		ModuleDescription* module, 
		const std::string &groupName );

protected:

	//!
	dynModuleHelper( );

	//!
	virtual ~dynModuleHelper( );

private:
};



#endif // _dynModuleHelper_H

