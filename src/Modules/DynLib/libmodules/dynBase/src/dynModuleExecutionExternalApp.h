/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _dynModuleExecutionExternalApp_H
#define _dynModuleExecutionExternalApp_H

#include "DynLibWin32Header.h"
#include "dynModuleExecutionCLPExe.h"

/**
Dynamic ModuleDescription execution

Executes a ModuleDescription located in a dynamic library

\author Xavi Planes
\date 14 July 2010
\ingroup DynLib
*/
class DYNLIB_EXPORT dynModuleExecutionExternalApp : public dynModuleExecutionCLPExe
{
public:
	typedef dynModuleExecutionExternalApp Self;
	typedef blSmartPointer<Self> Pointer;
	blNewMacro(Self);
	defineModuleFactory( dynModuleExecutionExternalApp );
	virtual dynModuleExecutionImpl::Pointer CreateAnother(void) const
	{
		dynModuleExecutionImpl::Pointer smartPtr;
		smartPtr = New().GetPointer();
		return smartPtr;
	}

protected:
	//!
	dynModuleExecutionExternalApp( );

	//!
	virtual ~dynModuleExecutionExternalApp( );

	//!
	virtual void BuildCommandLineLocation( );

	//!
	virtual void RunFilter( );

	//!
	virtual void AddCommandLineReturnParameters( );

	//! Set a parameter value to command line string
	virtual void SetParameterValue( ModuleParameter *param );

private:
};



#endif // _dynModuleExecutionExternalApp_H

