/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _dynModuleExecution_H
#define _dynModuleExecution_H

#include "DynLibWin32Header.h"
#include "ModuleDescription.h"
#include "dynModuleExecutionImpl.h"
#include "blLightObject.h"
#include <list>
#include <map>
#include "dynUpdateCallback.h"
#include "CILabExceptionMacros.h"

/**
Dynamic ModuleDescription execution

Executes a ModuleDescription located in:
- Dynamic library
- Command line plugin

Also provides some helper functions to manage in/out of ModuleDescription that
should be moved to ModuleDescription

\author Xavi Planes
\date 14 July 2010
\ingroup DynLib
*/
class DYNLIB_EXPORT dynModuleExecution : public blLightObject
{

	typedef std::map<std::string, dynModuleExecutionImpl::Factory::Pointer> FactoryMap;

public:
	typedef dynModuleExecution Self;
	typedef blSmartPointer<Self> Pointer;
	blNewMacro(Self);
	cilabDeclareExceptionMacro(Exception, std::exception)

	//!
	static void RegisterImpl( const std::string &name, dynModuleExecutionImpl::Factory::Pointer );

	//!
	static void UnRegisterImpl( const std::string &name );

	//! Unregister impl for a plugin
	static void UnRegisterPluginImpl( const std::string &pluginName );

	//!
	ModuleDescription *GetModule() const;
	void SetModule(ModuleDescription *val);

	//!
	void Update( );

	//!
	std::string GetModuleType() const;

	//!
	static std::string GetModuleType( ModuleDescription *val );

	//!
	std::string GetWorkingDirectory() const;
	void SetWorkingDirectory(std::string val);

	//!
	std::string GetUseCaseDirectory() const;
	void SetUseCaseDirectory(std::string val);

	//!
	std::string GetForceExecutionMode() const;
	void SetForceExecutionMode(const std::string &val);

	//!
	void SetSaveScript( bool val );
	bool GetSaveScript( );

	//!
	void Clean( );

	//!
	void Copy( dynModuleExecution::Pointer );

	//!
	dynUpdateCallback::Pointer GetUpdateCallback() const;
	void SetUpdateCallback(dynUpdateCallback::Pointer val);

	/** Check if this processor will be executed locally or remotely.
	This will be used to use the Default execution queue or the Remote
	execution queue.
	*/
	virtual bool IsExecutedLocally( );

protected:

	//!
	dynModuleExecution( );

	//!
	virtual ~dynModuleExecution( );

private:
	//!
	ModuleDescription *m_Module;

	//! m_Module has been allocated internally
	bool m_ModuleOwned;

	//!
	dynModuleExecutionImpl::Pointer m_Impl;

	//!
	std::string m_ModuleType;

	//! Where to execute the applications to use the needed DLLs
	std::string m_WorkingDirectory;

	//! Folder where data has been stored, like ".../usecase_BDFGHJ"
	std::string m_UseCaseDirectory;

	//!
	bool m_SaveScript;

	//! Forces execution of this kind of mode if possible
	std::string m_ForceExecutionMode;

	//!
	static FactoryMap m_Factories;
	
	//!
	dynUpdateCallback::Pointer m_UpdateCallback;
};



#endif // _dynModuleExecution_H

