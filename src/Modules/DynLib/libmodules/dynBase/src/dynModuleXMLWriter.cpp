/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "dynModuleXMLWriter.h"
#include "tinyxml.h"

#include <fstream>
#include "blTextUtils.h"

dynModuleXMLWriter::dynModuleXMLWriter( )
{
}

dynModuleXMLWriter::~dynModuleXMLWriter()
{
}

ModuleDescription* dynModuleXMLWriter::GetModule() const
{
	return m_Module;
}

void dynModuleXMLWriter::SetModule( ModuleDescription *val )
{
	m_Module = val;
}

void AddChildTextElement( TiXmlElement * parentElm, const std::string &name, const std::string &value )
{
	if ( !value.empty( ) )
	{
		TiXmlElement * elem = new TiXmlElement( name );
		elem->LinkEndChild( new TiXmlText( value ) );
		parentElm->LinkEndChild( elem );
	}
}

void AddAttribute( TiXmlElement * elem, const std::string &name, const std::string &value )
{
	if ( !value.empty( ) )
	{
		elem->SetAttribute(name, value );
	}
}

void dynModuleXMLWriter::Update()
{
	TiXmlDocument doc;
	TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "utf-8", "" );
	doc.LinkEndChild( decl );

	TiXmlElement * execElm = new TiXmlElement( "executable" );
	doc.LinkEndChild( execElm );

	AddChildTextElement( execElm, "category", m_Module->GetCategory( ) );
	AddChildTextElement( execElm, "title", m_Module->GetTitle( ) );
	AddChildTextElement( execElm, "description", m_Module->GetDescription( ) );
	AddChildTextElement( execElm, "version", m_Module->GetVersion( ) );
	AddChildTextElement( execElm, "documentation-url", m_Module->GetDocumentationURL( ) );
	AddChildTextElement( execElm, "license", m_Module->GetLicense( ) );
	AddChildTextElement( execElm, "acknowledgements", m_Module->GetAcknowledgements( ) );
	AddChildTextElement( execElm, "contributor", m_Module->GetContributor( ) );
	AddChildTextElement( execElm, "executableApp", m_Module->GetExecutableApp( ) );
	AddChildTextElement( execElm, "workingDirectory", m_Module->GetWorkingDirectory( ) );

	  // iterate over each parameter group
	std::vector<ModuleParameterGroup>::iterator pgIt;
	for ( pgIt = m_Module->GetParameterGroups().begin();
		pgIt != m_Module->GetParameterGroups().end(); ++pgIt)
	{
		TiXmlElement* paramGroupElem = new TiXmlElement( "parameters" );
		execElm->LinkEndChild( paramGroupElem );

		if ( pgIt->GetAdvanced() == "true" ) AddAttribute( paramGroupElem, "advanced", pgIt->GetAdvanced() );
		AddChildTextElement( paramGroupElem, "label",  pgIt->GetLabel( ) );
		AddChildTextElement( paramGroupElem, "description", pgIt->GetDescription( ) );

		std::vector<ModuleParameter>::const_iterator pBeginIt = pgIt->GetParameters().begin();
		std::vector<ModuleParameter>::const_iterator pEndIt = pgIt->GetParameters().end();
		for (std::vector<ModuleParameter>::const_iterator pIt = pBeginIt; pIt != pEndIt; ++pIt)
		{
			TiXmlElement* paramElem = new TiXmlElement( pIt->GetTag() );
			paramGroupElem->LinkEndChild( paramElem );

			AddAttribute( paramElem, "fileExtensions", pIt->GetFileExtensionsAsString( ) );
			AddAttribute( paramElem, "reference", pIt->GetReference( ) );
			if ( pIt->GetHidden( ) == "true" ) AddAttribute( paramElem, "hidden", pIt->GetHidden( ) );
			if ( pIt->GetMultiple( ) == "true" ) AddAttribute( paramElem, "multiple", pIt->GetMultiple( ) );
			if ( pIt->GetAggregate( ) == "true" ) AddAttribute( paramElem, "aggregate", pIt->GetAggregate( ) );
			AddAttribute( paramElem, "coordinateSystem", pIt->GetCoordinateSystem( ) );
			AddChildTextElement( paramElem, "name", pIt->GetName( ) );
			AddChildTextElement( paramElem, "longflag", pIt->GetLongFlag( ) );
			AddChildTextElement( paramElem, "label", pIt->GetLabel( ) );
			AddChildTextElement( paramElem, "description", pIt->GetDescription( ) );
			AddChildTextElement( paramElem, "function", pIt->GetFunction( ) );
			AddChildTextElement( paramElem, "datatype", pIt->GetDataType( ) );
			AddChildTextElement( paramElem, "accessmode", pIt->GetAccessMode( ) );
			AddChildTextElement( paramElem, "channel", pIt->GetChannel( ) );
			AddChildTextElement( paramElem, "index", pIt->GetIndex( ) );
			AddChildTextElement( paramElem, "default", pIt->GetDefault( ) );
			AddChildTextElement( paramElem, "flag", pIt->GetFlag( ) );
			AddChildTextElement( paramElem, "format", pIt->GetFormat( ) );

			if ( !pIt->GetConstraints( ).empty() )
			{
				TiXmlElement* constraintsElem = new TiXmlElement( "constraints" );
				paramElem->LinkEndChild( constraintsElem );

				AddChildTextElement( paramElem, "minimum", pIt->GetMinimum( ) );
				AddChildTextElement( paramElem, "maximum", pIt->GetMaximum( ) );
				AddChildTextElement( paramElem, "step", pIt->GetStep( ) );
			}
		}
	}

	if ( !doc.SaveFile( m_FileName ) )
	{
		std::stringstream sstream;
		sstream << "Cannot save file: " << m_FileName;
		throw Exception( sstream.str( ).c_str( ) );
	}

	// Apply patch to replace Windows to Linux end of lines
	std::ifstream iFile( m_FileName.c_str(), std::ios::binary | std::ios::in );
	std::stringstream buffer;
	buffer << iFile.rdbuf();
	std::string contents(buffer.str());
	blTextUtils::StrSub( contents, "\r\n", "\r" );
	iFile.close( );

	std::ofstream oFile( m_FileName.c_str() );
	oFile << contents;
	oFile.close( );
}

std::string dynModuleXMLWriter::GetFileName() const
{
	return m_FileName;
}

void dynModuleXMLWriter::SetFileName( const std::string &val )
{
	m_FileName = val;
}

