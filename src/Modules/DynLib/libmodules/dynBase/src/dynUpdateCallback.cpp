/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "dynUpdateCallback.h"

dynUpdateCallback::dynUpdateCallback( )
{
}

dynUpdateCallback::~dynUpdateCallback()
{
}


std::string dynUpdateCallback::GetInformationMessage() const
{
	return m_InformationMessage;
}

void dynUpdateCallback::SetInformationMessage( std::string val )
{
	m_InformationMessage = val;
}

void dynUpdateCallback::AddInformationMessage( std::string val )
{
	m_InformationMessage += val;
}
