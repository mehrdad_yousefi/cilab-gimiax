/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _dynModuleExecutionImpl_H
#define _dynModuleExecutionImpl_H

#include "DynLibWin32Header.h"
#include "ModuleDescription.h"
#include "blLightObject.h"
#include <list>
#include <map>
#include "dynUpdateCallback.h"
#include "blTagMap.h"

/**
Dynamic ModuleDescription execution implementation base class

Update function is used to execute it.

\author Xavi Planes
\date 14 July 2010
\ingroup DynLib
*/
class DYNLIB_EXPORT dynModuleExecutionImpl : public blLightObject
{
public:
	typedef dynModuleExecutionImpl Self;
	typedef blSmartPointer<Self> Pointer;
	virtual Pointer CreateAnother(void) const = 0;

	/**
	Factory for dynModuleExecutionImpl
	*/
	class Factory : public blLightObject {
	public:
		typedef Factory Self;
		typedef blSmartPointer<Self> Pointer;
		
		//! Create a new dynModuleExecutionImpl instance
		virtual dynModuleExecutionImpl::Pointer NewModuleExecution( ) = 0;

		//!
		blTagMap::Pointer GetProperties( )
		{
			return m_Properties;
		}

	protected:
		Factory( )
		{
			m_Properties = blTagMap::New( );
		}

		blTagMap::Pointer m_Properties;
	};

	//!
	ModuleDescription *GetModule() const;
	void SetModule(ModuleDescription *val);

	//!
	virtual void Update( ) = 0;

	//!
	std::string GetWorkingDirectory() const;
	void SetWorkingDirectory(std::string val);

	//!
	std::string GetUseCaseDirectory() const;
	void SetUseCaseDirectory(std::string val);

	//!
	void SetSaveScript( bool val );
	bool GetSaveScript() const;

	//!
	dynUpdateCallback::Pointer GetUpdateCallback() const;
	void SetUpdateCallback(dynUpdateCallback::Pointer val);

	//! Redefined
	virtual bool IsExecutedLocally( );

protected:
	//!
	dynModuleExecutionImpl( );

	//!
	virtual ~dynModuleExecutionImpl( );

	//! Set all parameters to m_Filter
	void SetAllParameters( );

	//! Get all parameters from m_Filter
	void GetAllParameters( );

	//! Set a parameter value to m_Filter
	virtual void SetParameterValue( ModuleParameter *param ) = 0;

	//! Get a parameter value from m_Filter
	virtual void GetParameterValue( ModuleParameter *param ) = 0;

protected:
	//!
	ModuleDescription *m_Module;

	//! Working Directory when calling a process
	std::string m_WorkingDirectory;

	//! Folder where data has been stored, like ".../usecase_BDFGHJ"
	std::string m_UseCaseDirectory;

	//!
	std::string m_ModuleType;

	//!
	bool m_SaveScript;

	//!
	dynUpdateCallback::Pointer m_UpdateCallback;
};

#define defineModuleFactory( ClassName ) \
	class Factory : public dynModuleExecutionImpl::Factory \
	{ \
	public: \
		typedef Factory Self; \
		typedef blSmartPointer<Self> Pointer; \
		static dynModuleExecutionImpl::Factory::Pointer New(void) \
		{ \
			Pointer smartPtr; \
			Self* rawPtr = new Self; \
			smartPtr = rawPtr; \
			rawPtr->UnRegister(); \
			return smartPtr.GetPointer( ); \
		} \
		virtual dynModuleExecutionImpl::Pointer NewModuleExecution( ) \
		{ \
			ClassName::Pointer p = ClassName::New( ); \
			return p.GetPointer(); \
		} \
	};


#define defineModuleFactory1param( ClassName, Param1 ) \
	class Factory : public dynModuleExecutionImpl::Factory \
	{ \
	public: \
		typedef Factory Self; \
		typedef blSmartPointer<Self> Pointer; \
		static dynModuleExecutionImpl::Factory::Pointer New(Param1 p1) \
		{ \
			Pointer smartPtr; \
			Self* rawPtr = new Self( p1 ); \
			smartPtr = rawPtr; \
			rawPtr->UnRegister(); \
			return smartPtr.GetPointer( ); \
		} \
		virtual dynModuleExecutionImpl::Pointer NewModuleExecution( ) \
		{ \
			ClassName::Pointer p = ClassName::New( m_P1 ); \
			return p.GetPointer(); \
		} \
	private: \
		Factory( Param1 p1 ) \
		{ \
			m_P1 = p1; \
		} \
		Param1 m_P1; \
	};

#endif // _dynModuleExecutionImpl_H

