/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "dynModuleExecution.h"
#include "dynModuleExecutionDLL.h"
#include "dynModuleExecutionCLPShared.h"
#include "dynModuleExecutionCLPExe.h"
#include "dynModuleExecutionExternalApp.h"
#include "dynEmptyUpdateCallback.h"
#include "ModuleDescriptionUtilities.h"
#include <sstream>
#include <stdio.h>

dynModuleExecution::FactoryMap dynModuleExecution::m_Factories;

dynModuleExecution::dynModuleExecution( )
{
	m_ForceExecutionMode = "UnknownModule";
	m_SaveScript = false;
	m_Module = new ModuleDescription( );
	m_ModuleOwned = true;
	m_UpdateCallback = dynEmptyUpdateCallback::New( );

	RegisterImpl( "CommandLineModule", dynModuleExecutionCLPExe::Factory::New( ) );
	RegisterImpl( "SharedObjectModule", dynModuleExecutionCLPShared::Factory::New( ) );
	RegisterImpl( "RawDynLibModule", dynModuleExecutionDLL::Factory::New( ) );
	RegisterImpl( "ExternalApp", dynModuleExecutionExternalApp::Factory::New( ) );
	
}

dynModuleExecution::~dynModuleExecution()
{
	if ( m_ModuleOwned )
	{
		delete m_Module;
		m_Module = NULL;
	}
}

ModuleDescription* dynModuleExecution::GetModule() const
{
	return m_Module;
}

void dynModuleExecution::SetModule( ModuleDescription *val )
{
	if ( m_ModuleOwned )
	{
		delete m_Module;
		m_Module = NULL;
	}
	m_ModuleOwned = false;

	m_Module = val;
	m_ModuleType = GetModuleType( val );
}

void dynModuleExecution::Update()
{

	FactoryMap::iterator it = m_Factories.find( GetModuleType() );
	if ( it == m_Factories.end() )
	{
		return;
	}

	m_Impl = it->second->NewModuleExecution( );
	m_Impl->SetSaveScript(m_SaveScript);
	m_Impl->SetWorkingDirectory( GetWorkingDirectory() );
	m_Impl->SetUseCaseDirectory( GetUseCaseDirectory() );
	m_Impl->SetModule( m_Module );
	m_Impl->SetUpdateCallback( m_UpdateCallback );
	try {
		m_Impl->Update();
	} catch (std::exception& ex) {
		throw dynModuleExecution::Exception(ex.what());
	} catch (...) {
		throw dynModuleExecution::Exception("Unknown exception caught while executing Update().");
	}
}


std::string dynModuleExecution::GetModuleType() const
{
	if ( GetForceExecutionMode( ) != "UnknownModule" )
	{
		return GetForceExecutionMode();
	}

	return m_ModuleType;
}

std::string dynModuleExecution::GetModuleType( ModuleDescription *module )
{
	std::string moduleType;

	if ( module->GetType() != "UnknownModule" )
	{
		moduleType = module->GetType();
	}
	else if ( module->GetAlternativeType() != "UnknownModule" )
	{
		moduleType = module->GetAlternativeType();
	}
	else
	{
		moduleType = "UnknownModule";
	}

	return moduleType;
}

std::string dynModuleExecution::GetWorkingDirectory() const
{
	return m_WorkingDirectory;
}

void dynModuleExecution::SetWorkingDirectory( std::string val )
{
	m_WorkingDirectory = val;
}

std::string dynModuleExecution::GetForceExecutionMode() const
{
	return m_ForceExecutionMode;
}

void dynModuleExecution::SetForceExecutionMode( const std::string &val )
{
	m_ForceExecutionMode = val;
}

void dynModuleExecution::SetSaveScript( bool val )
{
	m_SaveScript = val;
}

bool dynModuleExecution::GetSaveScript()
{
	return m_SaveScript;
}

void dynModuleExecution::Clean()
{
	m_Impl = NULL;
}

void dynModuleExecution::RegisterImpl( 
	const std::string &name, 
	dynModuleExecutionImpl::Factory::Pointer factory )
{
	FactoryMap::iterator it = m_Factories.find( name );
	if ( it == m_Factories.end() )
	{
		m_Factories[ name ] = factory;
	}
}

void dynModuleExecution::UnRegisterImpl( const std::string &name )
{
	FactoryMap::iterator it = m_Factories.find( name );
	if ( it == m_Factories.end() )
	{
		return;
	}

	m_Factories.erase( it );
}

void dynModuleExecution::UnRegisterPluginImpl( const std::string &pluginName )
{
	// Get list of factories to unregister
	std::list<std::string> factoryList;
	for ( FactoryMap::iterator it = m_Factories.begin( ) ; it != m_Factories.end( ) ; it++ )
	{
		blTag::Pointer tagPluginName = it->second->GetProperties( )->GetTag( "PluginName" );
		if ( tagPluginName.IsNull( ) )
			continue;

		if ( tagPluginName->GetValueAsString( ) == pluginName )
		{
			factoryList.push_back( it->first );
		}
	}

	// Unregister all factories
	std::list<std::string>::iterator factoryListIterator;
	for ( factoryListIterator = factoryList.begin( ) ; factoryListIterator != factoryList.end( ) ; factoryListIterator++ )
	{
		UnRegisterImpl( *factoryListIterator );
	}
}

void dynModuleExecution::Copy( Pointer instance )
{
	*m_Module = *instance->GetModule();
	m_ModuleType = instance->m_ModuleType;
	m_WorkingDirectory = instance->m_WorkingDirectory;
	m_UseCaseDirectory = instance->m_UseCaseDirectory;
	m_SaveScript = instance->m_SaveScript;
	m_ForceExecutionMode = instance->m_ForceExecutionMode;
}

dynUpdateCallback::Pointer dynModuleExecution::GetUpdateCallback() const
{
	return m_UpdateCallback;
}

void dynModuleExecution::SetUpdateCallback( dynUpdateCallback::Pointer val )
{
	m_UpdateCallback = val;
}

std::string dynModuleExecution::GetUseCaseDirectory() const
{
	return m_UseCaseDirectory;
}

void dynModuleExecution::SetUseCaseDirectory( std::string val )
{
	m_UseCaseDirectory = val;
}

bool dynModuleExecution::IsExecutedLocally()
{
	FactoryMap::iterator it = m_Factories.find( GetModuleType() );
	if ( it == m_Factories.end() )
	{
		return true;
	}

	m_Impl = it->second->NewModuleExecution( );
	bool localExecutron = m_Impl->IsExecutedLocally();
	Clean( );
	return localExecutron;
}