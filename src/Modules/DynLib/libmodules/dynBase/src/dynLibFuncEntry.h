/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _dynLibFuncEntry_H
#define _dynLibFuncEntry_H

#include <map>

#ifdef _WIN32
  typedef FARPROC FunctionPointer;
#else
  typedef void *FunctionPointer;
#endif


struct SINGLE_FUNCT_ENTRY
{
	std::string csOrdinal;
	std::string csHint;
	std::string csFunction;
	std::string csSymbol;
	std::string csEntryPoint;
	std::string csIndex;
	FunctionPointer dwData;
	bool m_bIsInvalidValid;
};

typedef std::vector<SINGLE_FUNCT_ENTRY> SingleMapEntryArray;


#endif // _dynLibFuncEntry_H

