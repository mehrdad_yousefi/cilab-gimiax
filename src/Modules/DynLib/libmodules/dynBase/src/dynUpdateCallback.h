/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _dynUpdateCallback_H
#define _dynUpdateCallback_H

#include "DynLibWin32Header.h"
#include "itkLightObject.h"

/**
Update callback interface for module execution. Notifies changes in 
ProcessInformation of ModuleDescription, like progress

\author Xavi Planes
\date Jan 2011
\ingroup DynLib
*/
class DYNLIB_EXPORT dynUpdateCallback : public itk::LightObject
{
public:
	typedef dynUpdateCallback        Self;
	typedef itk::LightObject Superclass;
	typedef itk::SmartPointer<Self> Pointer;
	typedef itk::SmartPointer<const Self>  ConstPointer;

	//! Empty function
	virtual void Modified( ) = 0;

	//! If true, detach process
	virtual bool GetDetachProcessing( ) = 0;

	//!
	std::string GetInformationMessage() const;
	void SetInformationMessage(std::string val);
	void AddInformationMessage(std::string val);

protected:

	//!
	dynUpdateCallback( );

	//!
	virtual ~dynUpdateCallback( );

private:
	//!
	std::string m_InformationMessage;
};



#endif // _dynUpdateCallback_H

