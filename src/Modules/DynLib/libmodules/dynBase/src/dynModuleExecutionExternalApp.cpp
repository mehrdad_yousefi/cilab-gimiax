/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "dynModuleExecution.h"
#include "dynModuleExecutionExternalApp.h"
#include <sstream>
#include <stdio.h>
#include <time.h>

#include <itksys/RegularExpression.hxx>
#include "itksys/DynamicLoader.hxx"
#include "itkExceptionObject.h"

#include "blTextUtils.h"

/**
 * Remove surrounding whitespace from a std::string.
 * @param s The string to be modified.
 * @param t The set of characters to delete from each end
 * of the string.
 * @return The same string passed in as a parameter reference.
 */
std::string& trim(std::string& s, const char* t = " \t\n\r\f\v")
{
	s.erase(0, s.find_first_not_of(t));
	s.erase(s.find_last_not_of(t) + 1);
	return s;
}

dynModuleExecutionExternalApp::dynModuleExecutionExternalApp( )
{
}

dynModuleExecutionExternalApp::~dynModuleExecutionExternalApp()
{
}

void dynModuleExecutionExternalApp::BuildCommandLineLocation()
{
	m_CommandLineAsString.clear();
	if ( GetModule()->GetType() == "ExternalApp" )
	{
		m_CommandLineAsString.push_back( GetModule( )->GetExecutableApp() );
	}
}

void dynModuleExecutionExternalApp::RunFilter()
{
	m_Process = itksysProcess_New();
	itksysProcess_SetCommand(m_Process, m_Command);
	if ( !GetModule( )->GetWorkingDirectory( ).empty() )
	{
		itksysProcess_SetWorkingDirectory( m_Process, GetModule( )->GetWorkingDirectory( ).c_str( ) );
	}
	else if ( !GetWorkingDirectory( ).empty() )
	{
		itksysProcess_SetWorkingDirectory( m_Process, GetWorkingDirectory( ).c_str( ) );
	}
	itksysProcess_SetOption(m_Process, itksysProcess_Option_Detach, 0);
	itksysProcess_SetOption(m_Process, itksysProcess_Option_HideWindow, 1);
	itksysProcess_Execute(m_Process);
}

void dynModuleExecutionExternalApp::SetParameterValue( ModuleParameter *param )
{
	// <prefix><flag><separator><value> -> Example: -sigma=1.0 -> -$(name)=$(value)
	// -$(name)=$(value)
	// -$(flag)=$(value)
	// -$(longflag)=$(value)
	std::string format = param->GetFormat( );
	if ( format.empty( ) )
	{
		// Use the base class for standard parameters
		dynModuleExecutionCLPExe::SetParameterValue( param );
	}
	else
	{
		blTextUtils::StrSub( format, "$(name)", param->GetName( ) );
		blTextUtils::StrSub( format, "$(flag)", param->GetFlag( ) );
		blTextUtils::StrSub( format, "$(longflag)", param->GetLongFlag( ) );

		// Find $(value) and separate it from the prefix
		std::string prefix = format;
		blTextUtils::StrSub( prefix, "$(value)", "" );
		trim( prefix );

		// Boolean is only used when the value is true
		if (param->GetTag() == "boolean" )
		{
			if ( param->GetDefault() == "true" )
			{
				m_CommandLineAsString.push_back( prefix );
			}
		}
		else
		{
			// We need to push the prefix and the value as separate command arguments
			m_CommandLineAsString.push_back( prefix );
			m_CommandLineAsString.push_back( param->GetDefault() );
		}
	}
}


void dynModuleExecutionExternalApp::AddCommandLineReturnParameters ( )
{
}

