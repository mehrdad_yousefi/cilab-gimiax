/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "dynVtkFilter.h"
#include "dynLib.h"
#include "dynVtkFilterIFace.h"
#include "dynCalls.h"

#include <stdexcept>
#include <sstream>


dynVtkFilter::dynVtkFilter(  )
{
}

dynVtkFilter::~dynVtkFilter()
{
	if ( GetInstance( ) )
	{
		Delete();
	}
}

void dynVtkFilter::Delete()
{
	CALL_MEMBER_FN(GetInstance( ),m_FuncNameDestructor,exEmpty);
}

void dynVtkFilter::NewInstance()
{
	m_FilterIFace = CALL_FN( m_FuncNameConstructor, exNew );
}

dynFilterIFace* dynVtkFilter::GetInstance()
{
	return m_FilterIFace;
}

dynFilterFunction dynVtkFilter::GetFunctionSetInput()
{
	dynFilterFunction func;
	func.SetName( "SetInput" );
	func.AddParameter( "int" );
	func.OpenParenthesis();
	return func;
}

dynFilterFunction dynVtkFilter::GetFunctionGetOutput()
{
	dynFilterFunction func;
	func.SetName( "GetOutput" );
	func.AddParameter( "int" );
	func.OpenParenthesis();
	return func;
}
