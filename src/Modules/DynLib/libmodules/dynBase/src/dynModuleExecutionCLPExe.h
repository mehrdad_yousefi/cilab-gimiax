/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _dynModuleExecutionCLPExe_H
#define _dynModuleExecutionCLPExe_H

#include "DynLibWin32Header.h"
#include "dynModuleExecutionCLPBase.h"
#include "ModuleDescription.h"
#include <list>
#include <map>
#include <itksys/Process.h>
#include "itkSimpleFastMutexLock.h"

/**
Dynamic ModuleDescription execution

Executes a ModuleDescription located in a dynamic library

\author Xavi Planes
\date 14 July 2010
\ingroup DynLib
*/
class DYNLIB_EXPORT dynModuleExecutionCLPExe : public dynModuleExecutionCLPBase
{
public:
	typedef dynModuleExecutionCLPExe Self;
	typedef blSmartPointer<Self> Pointer;
	blNewMacro(Self);
	defineModuleFactory( dynModuleExecutionCLPExe );
	virtual dynModuleExecutionImpl::Pointer CreateAnother(void) const
	{
		dynModuleExecutionImpl::Pointer smartPtr;
		smartPtr = New().GetPointer();
		return smartPtr;
	}

protected:
	//!
	dynModuleExecutionCLPExe( );

	//!
	virtual ~dynModuleExecutionCLPExe( );

	//!
	virtual void BuildCommandLineLocation( );

	//!
	virtual void RunFilter( );

	//! 
	virtual void WaitForData( );

	/** Remove the embedded XML from the stdout stream. 
	Calls FilterOutput( ) and thorws Exception if error
	*/
	virtual void ProcessOutputInformation( );

	//! Update progress and other information from m_stdoutbuffer
	virtual void UpdateProgress( );

	//! Filter Standard output/error when the process has finished executing
	virtual void FilterOutput( );

protected:

	//!
	itksysProcess *m_Process;

	/** Mutex lock to protect FilterOutput() access to a single thread */
	static itk::SimpleFastMutexLock m_FilterOutputLock;
};



#endif // _dynModuleExecutionCLPExe_H

