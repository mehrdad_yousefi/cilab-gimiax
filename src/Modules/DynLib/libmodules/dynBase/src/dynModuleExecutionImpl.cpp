/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "dynModuleExecutionImpl.h"
#include <sstream>
#include <stdio.h>

dynModuleExecutionImpl::dynModuleExecutionImpl( )
{
	m_SaveScript = false;
}

dynModuleExecutionImpl::~dynModuleExecutionImpl()
{
}

ModuleDescription* dynModuleExecutionImpl::GetModule() const
{
	return m_Module;
}

void dynModuleExecutionImpl::SetModule( ModuleDescription *val )
{
	m_Module = val;
}

void dynModuleExecutionImpl::Update()
{

}


void dynModuleExecutionImpl::SetAllParameters()
{
	std::vector<ModuleParameterGroup> parameterGroups = m_Module->GetParameterGroups();
	std::vector<ModuleParameterGroup>::iterator itGroup;
	itGroup = m_Module->GetParameterGroups().begin();
	while (itGroup != m_Module->GetParameterGroups().end())
	{
		std::vector<ModuleParameter>::iterator itParam;
		itParam = itGroup->GetParameters().begin();
		while (itParam != itGroup->GetParameters().end())
		{
			SetParameterValue( &(*itParam) );

			++itParam;
		}

		++itGroup;
	}
}

void dynModuleExecutionImpl::GetAllParameters()
{
	std::vector<ModuleParameterGroup>::iterator itGroup;
	itGroup = m_Module->GetParameterGroups().begin();
	while (itGroup != m_Module->GetParameterGroups().end())
	{
		std::vector<ModuleParameter>::iterator itParam;
		itParam = itGroup->GetParameters().begin();
		while (itParam != itGroup->GetParameters().end())
		{
			GetParameterValue( &(*itParam) );
			++itParam;
		}

		++itGroup;
	}
}

std::string dynModuleExecutionImpl::GetWorkingDirectory() const
{
	return m_WorkingDirectory;
}

void dynModuleExecutionImpl::SetWorkingDirectory( std::string val )
{
	m_WorkingDirectory = val;
}

void dynModuleExecutionImpl::SetSaveScript( bool val )
{
	m_SaveScript = val;
}

bool dynModuleExecutionImpl::GetSaveScript() const
{
	return m_SaveScript;
}

void dynModuleExecutionImpl::SetUpdateCallback( dynUpdateCallback::Pointer val )
{
	m_UpdateCallback = val;
}

dynUpdateCallback::Pointer dynModuleExecutionImpl::GetUpdateCallback() const
{
	return m_UpdateCallback;
}

std::string dynModuleExecutionImpl::GetUseCaseDirectory() const
{
	return m_UseCaseDirectory;
}

void dynModuleExecutionImpl::SetUseCaseDirectory( std::string val )
{
	m_UseCaseDirectory = val;
}

bool dynModuleExecutionImpl::IsExecutedLocally()
{
	return true;
}
