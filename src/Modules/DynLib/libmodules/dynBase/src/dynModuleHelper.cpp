/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "dynModuleHelper.h"
#include "ModuleDescriptionUtilities.h"
#include <sstream>
#include <stdio.h>

dynModuleHelper::dynModuleHelper( )
{
}

dynModuleHelper::~dynModuleHelper()
{
}

ModuleParameter *dynModuleHelper::FindParameter( 
	ModuleDescription* module,
	std::string channel /*= "" */,
	int count /*= 0*/,
	std::string name /*= ""*/,
	const std::string &tag /*=""*/ )
{

	return findModuleParameter( module, channel, count, name, tag );
}

long dynModuleHelper::GetNumberOfInputs( ModuleDescription* module )
{
	int count = 0;
	int inputs = 0;
	ModuleParameter* param;
	while ( param = FindParameter( module, "input", count ) )
	{
		if ( param->GetTag() != "file" && param->GetTag() != "directory" )
		{
			inputs++;
		}

		count++;
	}
	return inputs;
}

long dynModuleHelper::GetNumberOfOutputs( ModuleDescription* module )
{
	int count = 0;
	int outputs = 0;
	ModuleParameter* param;
	while ( param = FindParameter( module, "output", count ) )
	{
		if ( param->GetTag() != "file" && param->GetTag() != "directory" )
		{
			outputs++;
		}

		count++;
	}
	return outputs;
}

void* dynModuleHelper::StringToPointer( const std::string name )
{
	void* ptr = NULL;
	sscanf( name.c_str(), "%p", &ptr );
	return ptr;
}

std::string dynModuleHelper::PointerToString( void* ptr )
{
	char buffer[128];
	sprintf( buffer, "%p", ptr );
	return buffer;
}

ModuleParameter * dynModuleHelper::GetInput( ModuleDescription* module, size_t inputNum )
{
	int count = 0;
	int inputs = 0;
	ModuleParameter* param;
	while ( param = FindParameter( module, "input", count ) )
	{
		if ( param->GetTag() != "file" && param->GetTag() != "directory" )
		{
			if ( inputs == inputNum )
			{
				return param;
			}

			inputs++;
		}

		count++;
	}

	return 0;
}

ModuleParameter * dynModuleHelper::GetOutput( ModuleDescription* module, size_t outputNum )
{
	int count = 0;
	int outputs = 0;
	ModuleParameter* param;
	while ( param = FindParameter( module, "output", count ) )
	{
		if ( param->GetTag() != "file" && param->GetTag() != "directory" )
		{
			if ( outputs == outputNum )
			{
				return param;
			}

			outputs++;
		}

		count++;
	}

	return 0;
}

void dynModuleHelper::RemoveParameter( ModuleDescription* module, ModuleParameter* param )
{
	std::vector<ModuleParameterGroup> parameterGroups = module->GetParameterGroups();
	std::vector<ModuleParameterGroup>::iterator it;
	std::vector<ModuleParameterGroup>::iterator itGroup;
	itGroup = module->GetParameterGroups().begin();
	while (itGroup != module->GetParameterGroups().end())
	{
		std::vector<ModuleParameter>& parameters = itGroup->GetParameters( );

		std::vector<ModuleParameter>::iterator it;
		for (it = parameters.begin() ; it != parameters.end() ; it++ )
		{
			if ( &(*it) == param )
			{
				parameters.erase( it );
				break;
			}
		}
		itGroup++;
	}
}

void dynModuleHelper::RemoveParameter( 
	ModuleDescription* module, 
	const std::string &groupName,
	const std::string &parameterName)
{
	// Get selected group
	bool found = false;
	std::vector<ModuleParameterGroup>::iterator pgIt = module->GetParameterGroups().begin();
	while ( !found && pgIt != module->GetParameterGroups().end() )
	{
		if ( pgIt->GetLabel( ) == groupName )
		{
			// Erase single parameter
			std::vector<ModuleParameter>& parameters = pgIt->GetParameters( );
			std::vector<ModuleParameter>::iterator it = parameters.begin() ;
			while ( !found && it != parameters.end() )
			{
				if ( it->GetName( ) == parameterName )
				{
					found = true;
				}
				else
				{
					it++;
				}
			}

			if ( found )
			{
				parameters.erase( it );
			}

		}
		else
		{
			++pgIt;
		}
	}
}


void dynModuleHelper::RemoveParameterGroup( 
	ModuleDescription* module, 
	const std::string &groupName)
{
	std::vector<ModuleParameterGroup> &parameterGroups = module->GetParameterGroups( );
	bool found = false;

	std::vector<ModuleParameterGroup>::iterator pgIt = parameterGroups.begin();
	while( !found && pgIt != parameterGroups.end() )
	{
		if ( pgIt->GetLabel( ) == groupName )
		{
			found = true;
		}
		else
		{
			++pgIt;
		}
	}

	if ( found )
	{
		parameterGroups.erase( pgIt );
	}

}

bool dynModuleHelper::FindParameterGroup( 
	ModuleDescription* module, 
	const std::string &groupName)
{
	std::vector<ModuleParameterGroup> &parameterGroups = module->GetParameterGroups( );
	bool found = false;

	std::vector<ModuleParameterGroup>::iterator pgIt = parameterGroups.begin();
	while( !found && pgIt != parameterGroups.end() )
	{
		if ( pgIt->GetLabel( ) == groupName )
		{
			found = true;
		}
		else
		{
			++pgIt;
		}
	}

	return found;
}



