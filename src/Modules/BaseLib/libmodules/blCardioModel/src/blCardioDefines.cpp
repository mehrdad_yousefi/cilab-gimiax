/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "blCardioDefines.h"


namespace Cardio
{
	const char* VECTOR_NAME_SUBPART_ID = "subpartID";
	const char* VECTOR_NAME_REGION_ID = "regionID";
}

