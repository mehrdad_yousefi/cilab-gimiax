/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _blConfigFileReader_h
#define _blConfigFileReader_h

#include <map>
#include <string>
#include <vector>

//#define DEBUG_MESSAGES_blConfigFileReader

const int CONFIG_FILE_MAX_LINE_LENGTH = 255;
const char CONFIG_FILE_DELIMITERS[] = " \t";


/**
 * \brief This class reads values from a given file.
 * \ingroup blUtilities
 *
 *	Each line of the config file will contain the name of a parameter 
 *	followed by the values associated to it.
 *
 *	paramName value1 value2 ...
 *
 *	params and values are delimited by blanks
 */
class blConfigFileReader
{

public:

	/**\brief type of vector of strings*/
	typedef std::vector<std::string> StringVectorType;

	/**\brief type of vector of int*/
	typedef std::vector<int> IntVectorType;

	/**\brief type of vector of double*/
	typedef std::vector<double> DoubleVectorType;


	/**\brief Constructor. */
	blConfigFileReader();

	/**\brief Destructor. */
	~blConfigFileReader();

	// fsukno 20040415
	/**\brief Last error code generated*/
	int GetLastError ()
	{
		return this->lastErrorCode;
	};

	/**\brief Reset last error code to zero*/
	void ResetLastError ()
	{
		this->lastErrorCode = 0;
	}

	/**\brief Returns the filename */
	const char * GetFilename()
	{		
		return this->filename;
	};

	/**
	 * \brief Returns the value of a param for a given parameter.
     * If parameter does not exist, a empty string is returned
     * \param paramName Name of the parameter.
     * \param defaultValue The default value.
	 * \param bRemoveLastCRCharacter Search the last CR character "\r"
	 * and if it's found, erase it from the string.
     * \return the string corresponding to this parameter.
	 */
	std::string GetValue(
						const char * paramName, 
						std::string defaultValue = "",
						bool bRemoveLastCRCharacter = false );

    /**
    * Returns the value (bool) for a given parameter.
    * If parameter does not exist, returns false.
    * \param paramName Name of the parameter.
    * \param defaultValue The default value.
    * \return The boolean value corresponding to this parameter.
    */
	bool GetValueAsBool(const char * paramName, bool defaultValue = false);

    /**
    * Returns the value (int) for a given parameter.
    * If parameter does not exist, returns 0.
    * \param paramName Name of the parameter.
    * \param defaultValue The default value.
    * \return The int value corresponding to this parameter.
    */
	int GetValueAsInt(const char * paramName, int defaultValue = 0);

    /**
    * Returns the value (double) for a given parameter.
    * If parameter does not exist, returns 0.
    * \param paramName Name of the parameter.
    * \param defaultValue The default value.
    * \return The double value corresponding to this parameter.
    */
	double GetValueAsDouble(const char * paramName, double defaultValue = 0);

    /**
    * Returns a vector of values for a given parameter.
    * If parameter does not exist, an empty vector is returned.
    * \param paramName Name of the parameter.
    * \return Vector of strings corresponding to this parameter.
    */
	StringVectorType GetVectorOfValues(const char * paramName);

    /**
    * Returns a vector of values (int) for a given parameter.
    * If parameter does not exist, an empty vector is returned.
    * \param paramName Name of the parameter.
    * \return Vector of int values corresponding to this parameter.
    */
	IntVectorType GetVectorOfIntValues(const char * paramName);

    /**
    * Returns a vector of values (double) for a given parameter.
    * If parameter does not exist, an empty vector is returned.
    * \param paramName Name of the parameter.
    * \return Vector of double values corresponding to this parameter.
    */
	DoubleVectorType GetVectorOfDoubleValues(const char * paramName);

	/**\brief Sets the file to be read. */
	void SetFilename(const char * filename);

	/**\brief Reads from the specified file and extract parameters and its values. */
	void Update();

	

private:
	char * filename;
	int lastErrorCode;  
	// Last error inficator: 
	//	0:	No errors
	//	-1:	Could not open file

	typedef std::map<std::string, StringVectorType> MapType;

	typedef MapType::iterator MapIteratorType;

	MapType paramsMap;
	
};

#endif
