/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _blTagMap_H
#define _blTagMap_H

#include "blLightObject.h"
#include "blTag.h"

#include "CILabExceptionMacros.h"

#include <boost/signals.hpp>
#include <boost/bind.hpp>

#include <string>
#include <map>

/** 
\brief Map of tags using blTag::GetName( ) as ID

\author Xavi Planes
\date 28 July 2009
\ingroup blUtilities
*/
class blTagMap : public blLightObject
{
public:
	typedef blTagMap Self;
	typedef blSmartPointer<Self> Pointer;
	blNewMacro(Self );
	cilabDeclareExceptionMacro(Exception, std::exception);
	typedef std::map<std::string, blTag::Pointer> TagMapType;
	typedef std::list<std::string> InsertionOrderType;
	typedef boost::signal2<void, blTagMap*, const std::string&> TagChangedSignal;
	typedef TagMapType::iterator Iterator;
	typedef InsertionOrderType::const_iterator ListIterator;

public:
	/** Add the tag. If it already exists, overwrite it
	If tag is found, triggers m_OnChangedTagSignal
	\return true if tag exists
	*/
	bool AddTag( blTag::Pointer tag );

	/** Add the tag. If it already exists, overwrite it
	\return true if tag exists
	*/
	bool AddTag( const std::string &name, boost::any value );

	//! Remove a tag
	bool RemoveTag( blTag::Pointer tag );

	//! Return a tag the first tag searching it by name
	blTag::Pointer GetTag( const std::string &name );

	//! Return the first tag searching it by name
	blTag::Pointer FindTagByName( const std::string &name, bool recursive = true );

	//! Returns the number of rows of the map
	size_t GetLength();

	//! Add all tags
	void AddTags( blTagMap::Pointer tagMap );

	//! Add all tags
	void AddTagsAsString( blTagMap::Pointer tagMap );

	//! returns the start iterator of the map
	Iterator GetIteratorBegin();

	//! returns the end iterator of the map
	Iterator GetIteratorEnd();

	//! returns the iterator value
	blTag::Pointer GetTag( const Iterator &iterator );

	//! returns the iterator key
	std::string GetKey( const Iterator &iterator );

	//! returns the start iterator of the insertion order list
	ListIterator ListBegin();

	//! returns the end iterator of the insertion order list
	ListIterator ListEnd();

	//! returns the iterator value 
	blTag::Pointer GetTag( const ListIterator &iterator );

	//! returns the iterator key
	std::string GetKey( const ListIterator &iterator );

	/**
	Connects an observer to be notified when a tag changes the value
	using AddTag( ). 

	\param observer is the class instance of the observer
	\param slotFunction is the Slot member of the observer class, following 
		the specified signature 
	\return the boost connection, but you might not need it
	*/
	template <class T> 
	boost::signals::connection AddObserverOnChangedTag(
		T* observer, 
		void (T::*slotFunction)(blTagMap*,const std::string&))
	{
		return this->m_OnChangedTagSignal.connect(boost::bind<void>(slotFunction, observer, _1, _2));
	};

	//!  Send a signal to notify that the tag has changed
	void NotifyObserversOnChangedTag(blTagMap* tagMap, const std::string& name );

	//!
	void RemoveAll( );

	/**
	Returns true if all tags of this instance can be found in the source
	parameter and the value is the same
	*/
	bool Compare( blTagMap::Pointer source );

	/** If tag is not found, return NULL
	\note You cannot use this function for data types like std::string that
	cannot be NULL
	*/
	template<class T>
	T GetTagValue( const std::string &name )
	{
		blTag::Pointer tag = GetTag( name );
		if ( tag.IsNull() )
		{
			return NULL;
		}

		T val = NULL;
		tag->GetValue( val );
		return val;
	}

protected:
	//!
	blTagMap( );

	//!
	virtual ~blTagMap();

	//! Return the first tag searching it by name
	static blTag::Pointer FindTagByNameRecursive( const std::string &name, blTagMap::Pointer map );

private:

	//!
	TagMapType m_TagMap;
	//! Insertion order list
	InsertionOrderType m_InsertionOrderList;
	// 
	TagChangedSignal m_OnChangedTagSignal;
};


#endif // _blTagMap_H

