/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef BLPROGRAMOPTIONS_H
#define BLPROGRAMOPTIONS_H

#include <boost/program_options.hpp>
#include <vector>

struct blCommandLine
{
	boost::program_options::variables_map options; 
	std::vector<std::string> args;
	bool parsedOkay;
	std::string parseMessage;
};

void blParseOptions(
	int argc, 
	char** argv, 
	const boost::program_options::options_description& desc, 
	blCommandLine& commandLine,
	bool canThrow = true
);

#endif //BLPROGRAMOPTIONS_H
