/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "blTagMap.h"


blTagMap::blTagMap( )
{
}

blTagMap::~blTagMap(void)
{
}

bool blTagMap::AddTag( blTag::Pointer tag )
{
	bool found = false;

	blTag::Pointer foundTag = FindTagByName( tag->GetName( ), false );
	if (foundTag.IsNotNull())
	{
		foundTag->SetValue( tag->GetValue() );

		// Send a signal to notify that the tag has changed
		m_OnChangedTagSignal( const_cast<blTagMap*>(this), tag->GetName() );

		found = true;
	}
	else
	{
		m_TagMap[ tag->GetName( ) ] = tag;
		m_InsertionOrderList.push_back( tag->GetName( ) );
	}
	
	return found;
}

bool blTagMap::AddTag( const std::string &name, boost::any value )
{
	return AddTag( blTag::New( name, value ) );
}	

blTag::Pointer blTagMap::GetTag( const std::string &name )
{
	return FindTagByName( name, false );
}

blTag::Pointer 
blTagMap::FindTagByName( const std::string &name, bool recursive /*= true*/ )
{
	blTag::Pointer foundTag;

	if ( recursive )
	{
		foundTag = FindTagByNameRecursive( name, this );
	}
	else
	{
		TagMapType::iterator it = m_TagMap.find( name );
		if ( it != m_TagMap.end() )
		{
			foundTag = it->second;
		}
	}

	return foundTag;
}

blTag::Pointer blTagMap::FindTagByNameRecursive( const std::string &name, Pointer tagMap )
{
	TagMapType::iterator it = tagMap->GetIteratorBegin();
	blTag::Pointer tag;
	while( tag.IsNull() && it != tagMap->GetIteratorEnd() )
	{
		if ( it->first == name )
		{
			tag = it->second;
		}
		
		// If it's a complex tag, call AddTags again
		if ( tag.IsNull( ) && it->second->GetValue().type() == typeid( blTagMap::Pointer ) )
		{
			tag = FindTagByNameRecursive( name, it->second->GetValueCasted<blTagMap::Pointer>( ) );
		}

		it++;
	}

	return tag;
}

void blTagMap::NotifyObserversOnChangedTag(blTagMap* tagMap, const std::string& name )
{
	// Send a signal to notify that the tag has changed
	m_OnChangedTagSignal( tagMap, name );
}

size_t blTagMap::GetLength()
{
	return m_TagMap.size();
}

blTagMap::Iterator blTagMap::GetIteratorBegin()
{
	return m_TagMap.begin();
}

blTagMap::Iterator blTagMap::GetIteratorEnd()
{
	return m_TagMap.end();
}

void blTagMap::RemoveAll()
{
	m_TagMap.clear();
	m_InsertionOrderList.clear( );
}

bool blTagMap::RemoveTag( blTag::Pointer tag )
{
	TagMapType::iterator it = m_TagMap.find( tag->GetName( ) );
	if ( it != m_TagMap.end() )
	{
		m_TagMap.erase( it );
		m_InsertionOrderList.remove( tag->GetName( ) );
		return true;
	}

	return false;
}

void blTagMap::AddTags( blTagMap::Pointer tagMap )
{
	ListIterator it;
	for ( it = tagMap->ListBegin() ; it != tagMap->ListEnd() ; it++ )
	{
		blTag::Pointer tag = tagMap->GetTag( it );
		// If it's a complex tag, call AddTags again
		if ( tag->GetValue().type() == typeid( blTagMap::Pointer ) )
		{
			// If this has this tag, reuse the pointer, else create a new one
			blTagMap::Pointer dstSubTagMap;
			dstSubTagMap = GetTagValue<blTagMap::Pointer>( tag->GetName( ) );
			if ( dstSubTagMap.IsNull() )
			{
				dstSubTagMap = blTagMap::New( );
			}

			// Get source tag map
			blTagMap::Pointer srcSubTagMap;
			tag->GetValue( srcSubTagMap );

			// Call AddTags( ) again
			dstSubTagMap->AddTags( srcSubTagMap );
			AddTag( tag->GetName( ), dstSubTagMap );
		}
		else
		{
			AddTag( tag->GetName( ), tag->GetValue() );
		}
	}

}

void blTagMap::AddTagsAsString( blTagMap::Pointer tagMap )
{
	TagMapType::iterator it;
	for ( it = tagMap->GetIteratorBegin() ; it != tagMap->GetIteratorEnd() ; it++ )
	{
		// If it's a complex tag, call AddTags again
		if ( it->second->GetValue().type() == typeid( blTagMap::Pointer ) )
		{
			// If this has this tag, reuse the pointer, else create a new one
			blTagMap::Pointer dstSubTagMap;
			dstSubTagMap = GetTagValue<blTagMap::Pointer>( it->second->GetName( ) );
			if ( dstSubTagMap.IsNull() )
			{
				dstSubTagMap = blTagMap::New( );
			}

			// Get source tag map
			blTagMap::Pointer srcSubTagMap;
			it->second->GetValue( srcSubTagMap );

			// Call AddTags( ) again
			dstSubTagMap->AddTagsAsString( srcSubTagMap );
			AddTag( it->second->GetName( ), dstSubTagMap );
		}
		else
		{
			blTag::Pointer tag = FindTagByName( it->second->GetName( ) );
			if ( tag.IsNull() )
			{
				tag = blTag::New( it->second->GetName( ), NULL );
			}
			tag->SetValueAsString( it->second->GetTypeName(), it->second->GetValueAsString() );
			AddTag( tag );
		}
	}

}

bool blTagMap::Compare(blTagMap::Pointer source)
{
	TagMapType::iterator it;
	for ( it = GetIteratorBegin() ; it != GetIteratorEnd() ; it++ )
	{
		blTag::Pointer tag = source->GetTag( it->second->GetName( ) );
		if ( tag.IsNull() )
		{
			return false;
		}

		if ( !it->second->Compare( tag ) )
		{
			return false;
		}
	}

	return true;
}

blTag::Pointer blTagMap::GetTag( const Iterator &iterator )
{
	return iterator->second;
}

std::string blTagMap::GetKey( const Iterator &iterator )
{
	return iterator->first;
}

blTagMap::ListIterator blTagMap::ListBegin()
{
	return m_InsertionOrderList.begin( );
}

blTagMap::ListIterator blTagMap::ListEnd()
{
	return m_InsertionOrderList.end( );
}

blTag::Pointer blTagMap::GetTag( const ListIterator &iterator )
{
	return GetTag( *iterator );
}

std::string blTagMap::GetKey( const ListIterator &iterator )
{
	return *iterator;
}
