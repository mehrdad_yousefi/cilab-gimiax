/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
//This class defines tokenizer for strings.

#ifndef __blXMLWriter__TXX
#define __blXMLWriter__TXX

#include "blXMLWriter.h"

//--------------------------------------------------
template <typename T>
blXMLWriter<T>::blXMLWriter()
//--------------------------------------------------
{
	this->indentLevel = 0;
	this->indentSize = 2;

	this->doublePrecision = 6;	// by default
}

//--------------------------------------------------
template <typename T>
blXMLWriter<T>::~blXMLWriter()
//--------------------------------------------------
{
}

//--------------------------------------------------
template <typename T>
void blXMLWriter<T>::WriteStartElementIndent(const char * const tag, 
											 std::ofstream & output)
//--------------------------------------------------
{
	this->WriteIndentation(output);
	Superclass::WriteStartElement(tag, output);
	this->indentLevel++;
}

//--------------------------------------------------
template <typename T>
void blXMLWriter<T>::WriteStartElementIndent(const std::string & tag, 
											 std::ofstream & output)
//--------------------------------------------------
{
	this->WriteStartElementIndent(tag.c_str(), output);
}

//--------------------------------------------------
template <typename T>
void blXMLWriter<T>::WriteEndElementIndent(const char * const tag, 
										   std::ofstream & output)
//--------------------------------------------------
{
	this->indentLevel--;
	this->WriteIndentation(output);
	Superclass::WriteEndElement(tag, output);
}

//--------------------------------------------------
template <typename T>
void blXMLWriter<T>::WriteEndElementIndent(const std::string & tag, 
										   std::ofstream & output)
//--------------------------------------------------
{
	this->WriteEndElementIndent(tag.c_str(), output);
}

//--------------------------------------------------
template <typename T>
void blXMLWriter<T>::WriteIndentation(std::ofstream & outStream)
//--------------------------------------------------
{
	size_t i;
	for (i = 0; i < this->indentSize * this->indentLevel; i++)
	{
		outStream << " ";
	}
}

//--------------------------------------------------
template <typename T>
void blXMLWriter<T>::WriteVector(const vnl_vector<double> & vector, 
											std::ofstream & outStream,
											bool indentation)
//--------------------------------------------------
{
	this->WriteVector( vector.begin(), vector.size() ,outStream, indentation);
}

//--------------------------------------------------
template <typename T>
void blXMLWriter<T>::WriteVector(const std::vector<double> & vector, 
											std::ofstream & outStream,
											bool indentation)
//--------------------------------------------------
{
  this->WriteVector(&*vector.begin(), vector.size(), outStream, indentation); //Ir Fix this properly
}

//--------------------------------------------------
template <typename T>
void blXMLWriter<T>::WriteVector(const double array[], unsigned int arraySize, 
											std::ofstream & outStream,
											bool indentation)
//--------------------------------------------------
{
	// save previous precision
	unsigned int precision = outStream.precision();
	outStream.precision(this->doublePrecision);

	// scientific format "e-10"
	outStream.setf(std::ios::scientific);

	
	int colsCounter = 0;

	const double * doublePt;

	unsigned i;
	for (i = 0, doublePt = array; i < arraySize; i++, doublePt++)
	{
		// indentation
		if (colsCounter == 0)
		{
			if (indentation)
			{
				this->WriteIndentation(outStream);
			}
		}

		outStream << (double) *doublePt << " " ;
		colsCounter++;

		// new line
		if (colsCounter == NUMBER_OF_COLS_PER_LINE)
		{
			outStream << std::endl;
			colsCounter = 0;
		}
	}

	if (colsCounter != 0)
	{
		outStream << std::endl;
	}	

	
	// restore precision
	outStream.precision(precision);	
}

#endif






