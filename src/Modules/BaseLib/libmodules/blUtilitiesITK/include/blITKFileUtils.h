/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _blITKFileUtils_h
#define _blITKFileUtils_h

/**
* \brief File utilities
* \ingroup blUtilitiesITK
* \date Apr 2012
* \author Xavi Planes
*/
namespace blITKFileUtils
{

/**
\brief If input filename exists in disk return the input filename, otherwise check the 
if 3D+T version exists:
image.vtk -> image00.vtk, image01.vtk, ...

\param [in] filename Input filename
\param [out] fileNames Generated 3D+T filenames
*/
void CheckDataFilenames( const std::string &filename, std::vector<std::string> &fileNames );

/** Generate 3D+T data filenames based on single filename and number of timepoints
\param [in] filename Input filename
\param [in] size Input filename
\param [out] fileNames Generated filenames
\note If size is 1, generated filename will be the same as input filename
*/
void GenerateDataFilenames( const std::string &filename, const int size, std::vector<std::string> &fileNames );

};

#endif // _blITKFileUtils_h
