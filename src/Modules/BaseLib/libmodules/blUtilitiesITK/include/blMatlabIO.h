/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef __blMatlabIO_h
#define __blMatlabIO_h

#include "blVector.h"
#include "blMatrix.h"


/// \ingroup blUtilitiesITK
void ToMatlab( const double* vector, int length, const char* filename, const char* var_name );

/// \ingroup blUtilitiesITK
void ToMatlab( const double* matrix, int rows, int cols, const char* filename, const char* var_name );

/// \note be careful since these reading functions can handle only simple
///  structure, look at the file created by the above writing functions

/// \ingroup blUtilitiesITK
void FromMatlab( blVector<double>::VectorType* vector, const char* filename );

/// \ingroup blUtilitiesITK
void FromMatlab( vnl_matrix<double>* matrix, const char* filename );

#endif
