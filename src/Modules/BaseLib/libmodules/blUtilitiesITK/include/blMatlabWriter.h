/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _blMatlabWriter_h
#define _blMatlabWriter_h

#include <fstream>				// for blMat5FileWriter class
#include "blLightObject.h"		// for SmartPointers in blMat5FileWriter
#include "blMacro.h"			// for SmartPointers in blMat5FileWriter
#include "blSmartPointer.h"		// for SmartPointers in blMat5FileWriter

#include <vnl/vnl_matlab_filewrite.h>
#include "BaseLibWin32Header.h"

typedef vnl_matlab_filewrite vnlMatlabWriter;
typedef vnl_matrix<double> vnlMatrixDouble;
typedef vnl_vector<double> vnlVectorDouble;



/**
 * \brief This implements output to Matlab binary files
 * \ingroup blUtilities
 *
 *  Implements Open, Write and Close
 *
 * based on vnl_matlab_write.h
 */
class BASELIB_EXPORT blMatlabWriter 
{
public:
	static vnlMatlabWriter *openMatlabFile(const char *filename);

	static int writeToMatlabFile(vnlMatlabWriter * myWriter, 
									vnlVectorDouble theVector, 
									const char *name);
	
	static int writeToMatlabFile(vnlMatlabWriter * myWriter, 
									vnlMatrixDouble theMatrix, 
									const char *name);

	static int closeMatlabFile(vnlMatlabWriter *myWriter);
};

// #endif
// #ifndef __mat5filewriter_h
// #define __mat5filewriter_h
// #include <fstream.h>

// #define DEBUG_MATLAB5_NWRITES
#define SHORT_MATLAB5_NWRITES
#define ENABLE_MAT5WRITER_EXAMPLES

// fsukno 20040421
/**
 *	This class writes multidimensional arrays to Matlab files
 *      according to Version 5 MAT-File specifications
 */
// ___________________________________________________
// ---------------------------------------------------
class blMat5FileWriter:public blLightObject
// ___________________________________________________
// ---------------------------------------------------
{
        public:

		/* Smart pointers typedef for this class*/
		typedef blMat5FileWriter Self;
		typedef blSmartPointer<Self> Pointer;

		/* \brief Smart poitner class constructor*/
		static Pointer New( const char *fileName )  
		{ 
			Pointer smartPtr; 
			Self* rawPtr = new Self( fileName ); 
			smartPtr = rawPtr; 
			rawPtr->UnRegister(); 
			return smartPtr; 
		};

      
        /** \brief Returns current file status (opened = true) */
        bool IsOpened ()
        {
			return this->fileStream.is_open();
        };

		/** \brief Returns TRUE if there is an array currently opened
		 * This means that no other array can be opened for this file,
		 * since current one is not completely filled yet
		*/
		bool IsArrayOpened ()
		{
			return this->arrayOpened;
		};

        /**
         * Creates a new array for output. Only one array can be used at
         * a time since no buffer stores the data in memory (elemenst are
         * written to file as soon as they are available)
         * \return  0    on success,
         *         -1:   if already working with an array
         */
        int CreateDoubleArray (const int numberOfDimensions,
                const long *dimensionSizes, const char *name);

        /**
         * This function fills a multidimensional array at once
         * The method used here is not the most efficient, since
         * data in memory will not be sorted the same way it is
         * in a MAT file (some kind of transposition, see
         * AppendToArray (const double *data) for details)
         *
         * \param *data a pointer to double multidimensional array
         * \param numberOfDimensions
         * \param dimensionSizes
         * \param name
         * \return 0 on success, -1 if another array is already opened
         */
        int StoreArray (const int numberOfDimensions,
                const long *dimensionSizes, const char *name,
                const double *data);

        /**
         * Adds one vector to the currently opened array
         * It's important to understand that Matlab orders the elements
         * in a MAT file from the last to the first dimension. That is,
         * it we have an array of dimensions (a, b, c), then we will first
         * fill the elements in (:, 1, 1), then (:, 2, 1), then, (:, 3, 1)
         * and so on. Or, the same, we start with (1, 1, 1), (2, 1, 1),
         * (3, 1, 1), ...
         * The elements must be passed to this function in an ordered way
         * since there's no memory buffer but, instead, they're directly
         * wrote to the MAT file
         *
         * WARINING: This function stands for a double array with the right
         * number of elements on it, and will not check for it.
         * You can get the current expected size with GetNextVectorSize()
         *
         * \param *data A pointer to a double array with the data to be written
         * \return FALSE if no array is opened (nothing written), TRUE otherwise
         *
         */
        bool AppendVectorToArray (const double *data);

        /**
         * This is the single value version of AppendToArray
         * \param data is a single element to add to the array
         * (see AppendToArray (const double *data) for
         * important information).
         */
        bool AppendToArray (const double data);

        /**
         * This function fills a multidimensional array at once
         * using a vnl_matrix as input
         */
		int StoreVnl (vnl_matrix<double> *data, const char *name);

// ---------------------------------------------------
        protected:
// ---------------------------------------------------
        /**
         * Constructor: A filename must be specified since there's no
         * known use for the object but writing a MAT file
         */
        blMat5FileWriter ( const char *fileName );

        /** \brief Descrutcor */
        virtual ~blMat5FileWriter ();


// ---------------------------------------------------
        private:
// ---------------------------------------------------
		/** \brief Copy constructor*/
		blMat5FileWriter (const blMat5FileWriter&); //purposely not implemented
		/** \brief = constructor*/
		void operator = (const blMat5FileWriter&);  //purposely not implemented			

		// Methods
        // ---------------------------------------------------
        
        /**
         * Writes File Header according to MAT-File Format Version 5
         */
        void WriteHeader();

        /**
         * This function creates the array support variables and stores
         * the memory they need
         */
        void CreateArrayVariables (const long *dimensionSizes);

        /**
         * To free up some memory reserved when creating the temporary
         * variables needed to wort with an array
         */
        void FreeArrayVariables();

        /**
         * Updates the number of writes counter
         * This counter initializes to zero when a new array is created and
         * srotes the number of elements written to each array dimension
         * When the array is filled with the number of elements it was
         * defined, this function CLOSES the array, which means that you
         * can start writing another one. The file, however, is not closed
         *
         * \param numberOfElements The number of elements added to the array
         */
        void UpdateWritten ( const long numberOfElements );

        // Members
        // ---------------------------------------------------
		std::ofstream fileStream;    // Output file handler

        bool arrayOpened;       // Flag for opened array
        long nextVectorSize;    // The next data vector size to append an array
        long currentNDims;      // The number of dimensions of the currently opened array
        long *currentDims;      // The dimensions of the currently opened array
        long *nWritten;         // The number of writes counter

        char *fName;            // The current file name
        char *empty8;           // 8 zero-characters to pad file
};

namespace blMat5FileTypes
{
	/**\brief Features IDs */
	enum MatFile_DataType
        {
                miINT8 = 1,     miUINT8,        miINT16,
                miUINT16,       miINT32,        miUINT32,
                miSINGLE,       miDOUBLE = 9,   miINT64 = 12,
                miUINT64,       miMATRIX
        };
}

#ifdef ENABLE_MAT5WRITER_EXAMPLES
	namespace blClassesTests
	{
		int blMat5FileWriter_Example ();
	}
#endif // ENABLE_MAT5WRITER_EXAMPLES

#endif // _blMatlabWriter_h

