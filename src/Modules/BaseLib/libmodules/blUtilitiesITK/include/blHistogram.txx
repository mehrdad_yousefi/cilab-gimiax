/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include <math.h>

template <typename T>
bool blHistogram<T>::GetHistogram (int nDimension,
								   vnl_matrix<T> data,
								   vnl_vector<int> nBins,
								   vnl_matrix<double> & count, 
								   std::vector< vnl_vector<double> > & binCenters,
								   char N)
{

	if (!this->SetNumberOfDimensions(nDimension))
		return false;
	if (!this->SetData (data))
		return false;
	if (!this->SetDefaultMaxMin ())
		return false;
	if (!this->SetNumberOfBins(nBins))
		return false;
	if (!this->GenerateBinCenters())
		return false;
	if (!this->Update())
		return false;

	if ((N == 'N') || (N == 'n'))
		count = this->normCount;
	else if ((N == 'C') || (N == 'c'))
		count = this->cdf;
	else
		count = this->count;

	binCenters = this->binCenters;

	return true;
}

template <typename T>
bool blHistogram<T>::GetHistogram (int nDimension,
								   vnl_matrix<T> data,
								   vnl_vector<int> nBins,
								   vnl_vector<T> maxVal,
								   vnl_vector<T> minVal,
								   vnl_matrix<double> & count, 
								   std::vector< vnl_vector<double> > & binCenters,
								   char N)
{
	if (!this->SetNumberOfDimensions(nDimension))
		return false;
	if (!this->SetData (data))
		return false;
	if (!this->SetMaxMin (maxVal, minVal))
		return false;
	if (!this->SetNumberOfBins(nBins))
		return false;
	if (!this->GenerateBinCenters())
		return false;
	if (!this->Update())
		return false;

	if ((N == 'N') || (N == 'n'))
		count = this->normCount;
	else if ((N == 'C') || (N == 'c'))
		count = this->cdf;
	else
		count = this->count;

	binCenters = this->binCenters;

	return true;
}

template <typename T>
bool blHistogram<T>::GetHistogram (int nDimension,
							       vnl_matrix<T> data,
								   std::vector< vnl_vector<double> > binCenters,
								   vnl_matrix<double> & count,
								   char N)
{
	if (!this->SetNumberOfDimensions(nDimension))
		return false;
	if (!this->SetData (data))
		return false;
	if (!this->SetBinCenters(binCenters))
		return false;
	if (!this->Update())
		return false;

	if ((N == 'N') || (N == 'n'))
		count = this->normCount;
	else if ((N == 'C') || (N == 'c'))
		count = this->cdf;
	else
		count = this->count;

	return true;
}

template <typename T>
bool blHistogram<T>::SetNumberOfDimensions(int nDimension)
{

	// Sanity checks
	// To check if the number of dimensions is less 
	// than zero or greater than 2
	if ((nDimension <= 0) || (nDimension > 2))
	{
	#ifdef ENABLE_DEBUG_MESSAGES_bl_HISTOGRAM
		std::cout << "Debug: blHistogram->SetNumberOfDimensions: "
			 << "This class can handle only 1D and 2D histograms, "
			 << "Check input for nDimension" << std::endl;
	#endif
	return false;
	}

	this->nDimension = nDimension;
	return true;
}

template <typename T>
bool blHistogram<T>::SetData (vnl_matrix<T> data)
{
	// To check the dimensions of data
	if (this->nDimension != data.cols())
	{
	#ifdef ENABLE_DEBUG_MESSAGES_bl_HISTOGRAM
		std::cout << "Debug: blHistogram->SetData: "
			 << "Error in the input data " << std::endl;
		std::cout << "Debug: blHistogram->SetData: "
			 << "Hint: Arrange data in columns. " <<std::endl;
		std::cout << "Debug: blHistogram->SetData: "
			 << "The number of dimensions of the "
			 << "histogram is equal to the number of "
			 << "colums in data matrix" << std::endl;
	#endif
	return false;
	}

	this->data = data;
	return true;
}

template <typename T>
bool blHistogram<T>::SetMaxMin (vnl_vector<T> maxVal, vnl_vector<T> minVal)
{
	//Sanity checks
	// To compare the size of maxVal with the dimension of histogram
	if (maxVal.size() != this->nDimension)
	{
	#ifdef ENABLE_DEBUG_MESSAGES_bl_HISTOGRAM
		std::cout << "Debug: blHistogram->SetMaxMin: "
			 << "The number of elements in maxVal "
			 << "must be equal to dimension of histogram" << std::endl;
	#endif
	return false;
	}

	// To compare the size of minVal with the dimension of histogram
	if (minVal.size() != this->nDimension)
	{
	#ifdef ENABLE_DEBUG_MESSAGES_bl_HISTOGRAM
		std::cout << "Debug: blHistogram->SetMaxMin: "
			 << "The number of elements in minVal "
			 << "must be equal to dimension of histogram" << std::endl;
	#endif
	return false;
	}

	// To check if max value is greater than min value
	for (unsigned int iter = 0; iter < this->nDimension; iter++)
	{
		if (maxVal[iter] <= minVal[iter])
		{
			#ifdef ENABLE_DEBUG_MESSAGES_bl_HISTOGRAM
			std::cout << "Debug: blHistogram->SetMaxMin: "
				 << "Maximum value must be greater than minimum value."
				 << std::endl;
			#endif
			return false;
		}
	}


	this->maxVal.clear();
	this->minVal.clear();

	this->maxVal = maxVal;
	this->minVal = minVal;

	return true;
}

template <typename T>
bool blHistogram<T>::SetDefaultMaxMin ()
{
	vnl_vector<T> temp;
	vnl_vector<T> maxVal(this->nDimension);
	vnl_vector<T> minVal(this->nDimension);

	for (unsigned int iter = 0; iter < this->nDimension; iter++)
	{
		temp = data.get_column(iter);
		maxVal[iter] = temp.max_value();
		minVal[iter] = temp.min_value();
	}

	this->maxVal = maxVal;
	this->minVal = minVal;

	return true;
}

template <typename T>
bool blHistogram<T>::GenerateBinCenters ()
{
	this->binCenters.clear();

	// Calculate the binCenters
	double step = ((double)maxVal[0] - (double)minVal[0])/(double)nBins[0];

	// Define vector to hold binCenters of one dimension
	vnl_vector<double> temp(nBins[0]); 
	for (int iter = 0; iter < nBins[0]; iter++)
	{
		// step/2 is added to find center of bin
		temp[iter] = ((double)minVal[0] + iter * step) + step/2;
	}
	this->binCenters.push_back(temp);

	if (this->nDimension == 2)
	{
		step = ((double)maxVal[1] - (double)minVal[1])/(double)nBins[1];
		// Define vector to hold binCenters of one dimension
		vnl_vector<double> temp(nBins[1]); 
		for (int iter = 0; iter < nBins[1]; iter++)
		{
			// step/2 is added to find center of bin
			temp[iter] = ((double)minVal[1] + iter * step) + step/2;
		}
		this->binCenters.push_back(temp);
	}

	return true;
}

template <typename T>
bool blHistogram<T>::SetNumberOfBins(vnl_vector<int> nBins)
{
	// Sanity checks
	// To check if the number of elements in vnl_vector is 2
	if (nBins.size() != 2)
	{
	#ifdef ENABLE_DEBUG_MESSAGES_bl_HISTOGRAM
		std::cout << "Debug: blHistogram->SetNumberOfBins: "
			 << "The number of elements in the vnl_vector "
			 << "has to be two." << std::endl;
	#endif
	return false;
	}

	
	// To check if number of bins is less than zero
	if ((nBins[0] <= 0) || (nBins[1] <= 0))
	{
	#ifdef ENABLE_DEBUG_MESSAGES_bl_HISTOGRAM
		std::cout << "Debug: blHistogram->SetNumberOfBins: "
			 << "The elements in the vnl_vector "
			 << "has to be greater than zero." << std::endl;
	#endif
	return false;
	}


	// To check if the number of bins conforms to 
	// the dimension of the histogram desired.
	if ((this->nDimension == 1) & (nBins[1] != 1))
	{
	#ifdef ENABLE_DEBUG_MESSAGES_bl_HISTOGRAM
		std::cout << "Debug: blHistogram->SetNumberOfBins: "
			 << "Error in number of bins, "
			 << "input value has to be equal to 1" << std::endl;
	#endif
	return false;
	}

	this->nBins.clear();

	this->nBins = nBins;

	
	
	return true;
}

template <typename T>
bool blHistogram<T>::SetBinCenters(std::vector< vnl_vector<double> > binCenters)
{
	// Sanity checks
	// To check the dimensions of binCenters
	if (this->nDimension != binCenters.size())
	{
	#ifdef ENABLE_DEBUG_MESSAGES_bl_HISTOGRAM

		std::cout << "Debug: blHistogram->SetBinCenters: "
			 << "Check the dimensions of binCenters "
			 << std::endl;

		std::cout << "Debug: blHistogram->SetBinCenters: "
			 << "Hint: Arrange binCenters as vnl_vectors "
			 << "and push them into a vector" << std::endl;

		std::cout << "Debug: blHistogram->SetBinCenters: "
			 << "Hint: The number of elements in binCenters must be "
			 << "equal to the dimension of histogram (nDimension)" << std::endl;
	#endif
	return false;
	}


	this->binCenters = binCenters;

	// Now, set the values of number of bins.
	this->nBins.set_size(2);
	this->nBins[0] = binCenters[0].size();
	this->nBins[1] = 1;

	// Incase nDimension is 2
	if (this->nDimension == 2)
	{
		this->nBins[1] = binCenters[1].size();
	}

	// Setting max, min values
	this->maxVal.set_size(this->nDimension);
	this->minVal.set_size(this->nDimension);

	for (unsigned int iter = 0; iter < this->nDimension; iter++)
	{
		double step = binCenters[iter][1] - binCenters[iter][0];
		this->maxVal[iter] = binCenters[iter].max_value() + step/2;
		this->minVal[iter] = binCenters[iter].min_value() - step/2; 
	}

	return true;
}

template <typename T>
bool blHistogram<T>::Update()
{
	// Set default values to histogram matrix
	this->count.set_size(nBins[0], nBins[1]);
	this->count.fill(0);

	// To determine step sizes of histogram in each dimension
	vnl_vector<double> step(2);
	step[0] = this->binCenters[0][1] - this->binCenters[0][0];

	if (this->nDimension == 2)
		step[1] = this->binCenters[1][1] - this->binCenters[1][0];
	else
		step[1] = 1;

	for (unsigned int iter1 = 0; iter1 < this->data.rows(); iter1++)
	{
		int coord [] = {0,0};
		for (unsigned int iter2 = 0; iter2 < this->nDimension; iter2++)
		{
// TODO::COMMENTED BY XAVI
// 			coord[iter2] = (this->data[iter1][iter2] - this->minVal[iter2]) / step[iter2];
//////////////////
//TODO::MODIFIED BY XAVI
 			coord[iter2] = static_cast<int>(floor((this->data[iter1][iter2] - this->minVal[iter2]) / step[iter2] ));
///////////////
			// Sanity checks
			// To check if coordinates lie within limits
			// If not, they are set to corresponding extremes
			if (coord[iter2] < 0)
				coord[iter2] = 0;
			if (coord[iter2] >= this->nBins[iter2])
				coord[iter2] = this->nBins[iter2]-1;
		}

		count[coord [0]][coord [1]]++;

		
	}

	this->normCount = this->count / this->count.max_value();

	GenerateCDF();

	return true;
}

template <typename T>
bool blHistogram<T>::GenerateCDF()
{
	// Initially equating cdf to pdf
	this->cdf = this->normCount / this->normCount.array_one_norm();

	// Sum row wise
	for (unsigned int iter1 = 1; iter1 < this->cdf.rows(); iter1++)
	{
		// Adding previous row with present row
		this->cdf.set_row(iter1, (this->cdf.get_row(iter1)
								 + this->cdf.get_row(iter1-1)));
	}

	// Sum column wise
	for (unsigned int iter2 = 1; iter2 < this->cdf.cols(); iter2++)
	{
		// Adding previous row with present row
		this->cdf.set_column(iter2, (this->cdf.get_column(iter2)
								  + this->cdf.get_column(iter2-1)));
	}

	return true;
}

