/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include <math.h>
#include <blHistogram.h>


#ifdef ENABLE_bl_HISTOGRAM_EXAMPLES
	namespace blClassesTests
	{
		int testHistogram()
		{

			// -----------------------------------------------------
			// Example 1
			// (1) Tests GetHistogram() function
			// (2) Data is double, and hence blHistogram<double>
			// (3) Please change the value of param N to obtain one of these
			//			- Histogram counts, N = arbitrary char, except N & C
			//			- Normalized histogram, N = n
			//			- CDF, N = 'c'
			//			- (If no value is assigned explicitly, N = 'a')
			// -----------------------------------------------------
			std::cout << std::endl << "Example 1" << std::endl;

			// Inputs;
			int nDimension = 1;
			
			// Data should have 1 coulmn, since nDimension == 1
			vnl_matrix<double> data(500,1);
			// Filling up data
			for (int iter = 0; iter < 500; iter++)
				data[iter][0] = iter;

			// Specifying the number of bins
			vnl_vector<int> nBins(2);
			// Lets say, we need 10 bins
			nBins[0] = 10;
			nBins[1] = 1; // Default: will make much more sense for 2D histograms

			// Now define containers to hold binCenters and histogram counts
			vnl_matrix<double> count;
			std::vector< vnl_vector<double> > binCenters;

			// Lets invoke a smart pointer to blHistogram Class
			// The type is double because the data is double
			blHistogram<double>::Pointer myHist = blHistogram<double>::New();

			myHist->GetHistogram(nDimension, data, nBins, count, binCenters);

			displayMatrices(count, "Histogram counts");
			displayVectors(binCenters[0], "Bin Centers");

			/**
			 * Matlab code for this example:
			 * [N,X] = hist([0:499], 10); disp(N'); disp(X');
			 */
			
			// The meat of this example is that we supplied ndimensions, data, nBins,
			// and obtained count and binCenters.

			// -----------------------------------------------------
			// Example 2
			// (1) Tests GetHistogram() function
			// (2) Data is double, and hence blHistogram<double>
			// (3) MinMax values for the histogram are set
			// (4) Please change the value of param N to obtain one of these
			//			- Histogram counts, N = arbitrary char, except N & C
			//			- Normalized histogram, N = n
			//			- CDF, N = 'c'
			//			- (If no value is assigned explicitly, N = 'a')
			// -----------------------------------------------------
			std::cout << std::endl << "Example 2" << std::endl;


			// The size of minVal and maxVal is one, cbl, we are dealing
			// with 1D histograms
			// Its of type double because the data is double.

			vnl_vector<double> maxVal(1);
			vnl_vector<double> minVal(1);

			// Set values
			maxVal[0] = 500;
			minVal[0] = -100;

			myHist->GetHistogram(nDimension, data, nBins, maxVal,
								 minVal, count, binCenters);

			displayMatrices(count, "Histogram counts");
			displayVectors(binCenters[0], "Bin Centers");


			// -----------------------------------------------------
			// Example 3
			// (1) Tests GetHistogram() function
			// (2) Data is double, and hence blHistogram<double>
			// (3) BinCenters are provided as input
			// -----------------------------------------------------
			std::cout << "Example 3" << std::endl;

			myHist->GetHistogram(nDimension, data, binCenters, count);

			displayMatrices(count, "Histogram counts");
			displayVectors(binCenters[0], "Bin Centers");


			// -----------------------------------------------------
			// Example 4
			// (1) Tests GetHistogram() function for 2D histogram
			// (2) Data is double, and hence blHistogram<double>
			// (3) Please change the value of param N to obtain one of these
			//			- Histogram counts, N = arbitrary char, except N & C
			//			- Normalized histogram, N = n
			//			- CDF, N = 'c'
			//			- (If no value is assigned explicitly, N = 'a')
			// -----------------------------------------------------
			std::cout << "Example 4" << std::endl;

			// Inputs;
			nDimension = 2;
			
			// Data should have 2 coulmns, since nDimension == 2
			data.set_size(500,2);
			// Filling up data
			for (int iter1 = 0; iter1 < 500; iter1++)
			{
				for (int iter2 = 0; iter2 < 2; iter2++)
				{
					data[iter1][iter2] = iter1;
				}
			}
				
			// Lets say, we need 10 and 20 bins for the two dimensions
			nBins[0] = 10; 
			nBins[1] = 5;

			myHist->GetHistogram(nDimension, data, nBins, count, binCenters);

			displayMatrices(count, "Histogram counts");
			displayVectors(binCenters[0], "Bin Centers for dimension1");
			displayVectors(binCenters[1], "Bin Centers for dimension2");


			// -----------------------------------------------------
			// Example 5
			// (1) Tests GetHistogram() function for 2D histogram
			// (2) Data is double, and hence blHistogram<double>
			// (3) MinMax values for the histogram are set
			// (4) Please change the value of param N to obtain one of these
			//			- Histogram counts, N = arbitrary char, except N & C
			//			- Normalized histogram, N = n
			//			- CDF, N = 'c'
			//			- (If no value is assigned explicitly, N = 'a')
			// -----------------------------------------------------
			std::cout << "Example 5" << std::endl;

			maxVal.set_size(2);
			minVal.set_size(2);

			// Set values
			maxVal[0] = 500;
			minVal[0] = -100;
			maxVal[1] = 500;
			minVal[1] = -100;

			myHist->GetHistogram(nDimension, data, nBins, maxVal,
								 minVal, count, binCenters);

			displayMatrices(count, "Histogram counts");
			displayVectors(binCenters[0], "Bin Centers for dimension1");
			displayVectors(binCenters[1], "Bin Centers for dimension2");


			// -----------------------------------------------------
			// Example 6
			// (1) Tests GetHistogram() function for 2D histogram
			// (2) Data is double, and hence blHistogram<double>
			// (3) BinCenters generated in Example 5 are provided as input
			// (4) Please change the value of param N to obtain one of these
			//			- Histogram counts, N = arbitrary char, except N & C
			//			- Normalized histogram, N = n
			//			- CDF, N = 'c'
			//			- (If no value is assigned explicitly, N = 'a')
			// -----------------------------------------------------
			std::cout << "Example 6" << std::endl;

			myHist->GetHistogram(nDimension, data, binCenters, count);

			displayMatrices(count, "Histogram counts");
			displayVectors(binCenters[0], "Bin Centers");
			displayVectors(binCenters[1], "Bin Centers");



			return 0;
		}

		void displayMatrices(vnl_matrix<double> test, const char* name)
		{
			std::cout << std::endl << name << ":" << std::endl;
			for (unsigned int iter1 = 0; iter1 < test.rows(); iter1++)
			{
				for (unsigned int iter2 = 0; iter2 < test.cols(); iter2 ++)
				{
					std::cout << test[iter1][iter2] << " ";
				}
				std::cout << std::endl;
			}
			std::cout << std::endl;
		}

		void displayVectors(vnl_vector<double> test, const char* name)
		{
			std::cout << std::endl << name << ":" << std::endl;
			for (unsigned int iter1 = 0; iter1 < test.size(); iter1++)
			{
				std::cout << test[iter1] << std::endl;
			}
			std::cout << std::endl;
		}
	}
#endif // ENABLE_bl_HISTOGRAM_EXAMPLES


