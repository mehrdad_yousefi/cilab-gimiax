/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "blITKFileUtils.h"
#include <string>
#include <iostream>
#include <iomanip>

void blITKFileUtils::CheckDataFilenames( const std::string &filename, std::vector<std::string> &inputFileNames )
{
	bool fileExists = true;
	int count = 0;
	
	fileExists = itksys::SystemTools::FileExists( filename.c_str() );
	if ( fileExists )
	{
		inputFileNames.push_back( filename );
	}
	else
	{

		// Try to read file using filename00.vtk, filename01.vtk, ...
		fileExists = true;
		while ( fileExists )
		{

			std::string path = itksys::SystemTools::GetFilenamePath( filename.c_str( ) );;
			std::string fileName = itksys::SystemTools::GetFilenameWithoutExtension( filename.c_str( ) );
			std::string ext = itksys::SystemTools::GetFilenameExtension( filename.c_str( ) );
			std::stringstream sstream;
			sstream << path << "/" << fileName << std::setfill('0') << std::setw(2) << count << ext;
			fileExists = itksys::SystemTools::FileExists( sstream.str().c_str() );
			if( fileExists )
			{
				inputFileNames.push_back(sstream.str().c_str());
			}
			count++;
		}
	}

}


void blITKFileUtils::GenerateDataFilenames( 
	const std::string &filename, const int size, std::vector<std::string> &outputFileNames )
{
	if ( size == 1 )
	{
		outputFileNames.push_back( filename );
	}
	else
	{

		unsigned int shapeIndex;
		for (shapeIndex = 0; shapeIndex<size; shapeIndex++)
		{
			std::string path = itksys::SystemTools::GetFilenamePath( filename.c_str( ) );;
			std::string fileName = itksys::SystemTools::GetFilenameWithoutExtension( filename.c_str( ) );
			std::string ext = itksys::SystemTools::GetFilenameExtension( filename.c_str( ) );
			std::stringstream sstream;
			sstream << path << "/" << fileName << std::setfill('0') << std::setw(2) << shapeIndex << ext;

			outputFileNames.push_back( sstream.str() );
		}
	}

}

