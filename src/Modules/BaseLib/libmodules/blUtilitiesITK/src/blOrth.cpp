/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "blOrth.h"

void TestIfCompiles()
{
	vnl_matrix<double> m;
	vnl_vector<double> v;
	blOrth::GramSchmidt(&m, true);
	blOrth::GramSchmidtModified(&m, true, &v);
	blOrth::OrthonormalizeViaSVD(&m);
	blOrth::RemoveZeroRows(&m, true, NULL);
}
