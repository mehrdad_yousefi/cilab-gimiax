/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef __blAlignment_h
#define __blAlignment_h

#include <vnl/vnl_matrix.h>
#include <vnl/vnl_vector.h>

//#define DEBUG_MESSAGES_ALIGNMENT

/**
 * \brief class with static methods for aligment
 * \ingroup blPDAlignment
 *
 *	This class defines static methods to be used in alignment of shapes.
 */
class blAlignment
{
public:
    /**
     *	This method aligns a shape with a reference shape and returns
     *	the new coordinates for the aligned shape and parameters that defines
     *	the transform.
     *
     *	\param shape Point coordinates of shape to be aligned.
     *	\param refShape	Points coordinates of reference shape.
     *	\param outputShape New points coordinates aligned with reference shape.
     *	\param numberOfPoints Number of points.
     *	\param a Element of the transform matrix T.
     *	\param b Element of the transform matrix T.
     *	\param x_t Translation on x-axis.
     *	\param y_t Translation on y-axis.
     */
	static void AlignShape2D(const vnl_vector<double> & shape, 
									const vnl_vector<double> & refShape,
									vnl_vector<double> & outputShape,
									unsigned int numberOfPoints,
									double & a, double & b, 
									double & x_t, double & y_t);

    /**
     *	This method aligns a shape with a reference shape and returns
     *	the matrix that defines	the transformation.
     *
     *	\param shape Point coordinates of shape to be aligned.
     *	\param refShape	Points coordinates of reference shape.
     *	\param outputShape New points coordinates aligned with reference shape.
     *	\param numberOfPoints Number of points.
     *	\param transformMatrix Homogeneous transformation matrix [3,3].
     */
	static void AlignShape2D(const vnl_vector<double> & shape, 
									const vnl_vector<double> & refShape,
									vnl_vector<double> & outputShape,
									unsigned int numberOfPoints,
									vnl_matrix<double> & transformMatrix);

    /** 
     *	This method aligns a given shape with a reference shape.
     *	It is supposed that both shapes are centered on the origin.
     *	
     *	This returns a and b parameters, which define the transform matrix
     *
     *	[(a -b) (b a)]
     *
     *	where s (scaling) = sqrt (a^2 + b^2) and 
     *	RHO (rotation) = arctg(b/a)
     *
     *	\param shape Point coordinates of shape to be aligned.
     *	\param refShape	Points coordinates of reference shape.
     *	\param outputShape New points coordinates aligned with reference shape.
     *	\param homography
     *	
     */
	static void AlignShape2D_Projective(const vnl_vector<double> & shape, 
								const vnl_vector<double> & refShape,
								vnl_vector<double> & outputShape,
								vnl_matrix<double> & homography);

	/**\brief Synthesizes homography from parameters (without the angle specific to affine transform
	 * which allows skewing shapes):
	 * params = [ scale_x, scale_y, angle, translation_x, translation_y, v1, v2 ]
	 */
	static vnl_matrix<double> SynthesizeNonSkewHomography( const vnl_vector<double> params );

	//transforms by any transformation T that needs homogeneous representation
	static void TransformPointsHomog( vnl_vector<double>& pts, const vnl_matrix<double> T);

	static void AlignShape2D(const vnl_vector<double> & shape, 
								const vnl_vector<double> & refShape,
								vnl_vector<double> & outputShape,
								unsigned int numberOfPoints,
								double & a, double & b);
	


    /** 
     *	Aligns a 3D shape to a reference shape and returns the aligned shape and 
     *	the matrix that represents the similarity transformation.
     *
     *	\param shape Shape to be aligned (xyzxyz...).
     *	\param refShape Reference shape (xyzxyz...).
     *	\param outputShape Aligned shape (xyzxyz...).
     *	\param numberOfPoints Number of points in the shapes.
     *	\param matrix Similarity 3D transform matrix.
     *  \param doOnlyRigidAlign
     */
	static void AlignShape3D(const vnl_vector<double> & shape,
								const vnl_vector<double> & refShape,
								vnl_vector<double> & outputShape,
								unsigned int numberOfPoints,
								vnl_matrix<double> & matrix,
								bool doOnlyRigidAlign = false);

	static void AlignShape3D(const double shape[],
								const double refShape[],
								double outputShape[],
								unsigned int numberOfPoints,
								double transformMatrix[4][4],
								bool doOnlyRigidAlign = false);


    /**
     *	This method calculates new coordinates to center a given shape on a
     *	specified 2D point.
     *
     *	\param x Coordinate x of point the shape will be centred on.
     *	\param y Coordinate y of point the shape will be centred on.
     *	\param shape Points of the shape.
     *	\param outputShape New point coordinates.
     *	\param numberOfPoints Number of points in the shapes.
     *	\param dX Translation on x-axis.
     *	\param dY Translation on y-axis.
     */
	static void CenterShape2DTo(double x, double y, 
								const vnl_vector<double> & shape,
								vnl_vector<double> & outputShape,
								unsigned int numberOfPoints,
								double & dX, double & dY);

    /**
     *	This method calculates new coordinates to center a given shape on a
     *	specified 2D point.
     *
     *	\param x Coordinate x of point the shape will be centred on.
     *	\param y Coordinate y of point the shape will be centred on.
     *	\param z Coordinate z of point the shape will be centred on.
     *	\param shape Points of the shape.
     *	\param outputShape New point coordinates.
     *	\param numberOfPoints Number of points in the shapes.
     *	\param dX Translation on x-axis.
     *	\param dY Translation on y-axis.
     *	\param dZ Translation on z-axis.
     */
	static void CenterShape3DTo(double x, double y, double z,
								const vnl_vector<double> & shape,
								vnl_vector<double> & outputShape,
								unsigned int numberOfPoints,
								double & dX, double & dY, double & dZ);
	

    /**
     *	This method calculates new coordinates to center a given shape on a
     *	specified 2D point.
     *
     *	\param center Coordinate of the point the shape will be centred on.
     *	\param shape Points of the shape.
     *	\param outputShape new point coordinates.
     *	\param numberOfPoints Number of points in the shapes.
     *	\param numberOfDimensions Number of dimensions in the shapes.
     *	\param translation Translation.
     */
	static void CenterShapeTo(const double center[], 
									const double shape[],	
									double outputShape[],
									unsigned int numberOfPoints,
									unsigned int numberOfDimensions,
									double translation[]);


    /**
     *	This method calculates the centroid of a set of points
     *
     *	\param pointCoordinates Coordinates of the points : xy[z]xy[z]...
     *	\param numberOfDimensions Number of dimensions of points.
     *	\param numberOfPoints Number of points.
     *	\param centroid Coordinates of the centroid.
     */
	static void Centroid(const double pointCoordinates[],
									unsigned int numberOfDimensions,
									unsigned int numberOfPoints,
									double centroid[]);


    /**
     *	This method calculates the centroid of a set of points
     *
     *	\param pointCoordinates Coordinates of the points : xy[z]xy[z]...
     *	\param numberOfDimensions Number of dimensions of points.
     *	\param numberOfPoints Number of points.
     *	\param centroid Coordinates of the centroid.
     */
	static void Centroid(const vnl_vector<double> & pointCoordinates,
							unsigned int numberOfDimensions,
							unsigned int numberOfPoints,
							vnl_vector<double> & centroid);

	static void Perpendiculars(const double x[3], double y[3], 
												double z[3], double theta);

	/** 
	 * \brief This method calculates the bounding box of a set of points
	 *  minX, maxX, minY, maxY, ....
     *	\param pointCoordinates Coordinates of the points : xy[z]xy[z]...
     *	\param numberOfDimensions Number of dimensions of points.
     *	\param numberOfPoints Number of points.
     *	\param boundingBox Coordinates of the bounding Box.
     */
	static void ComputeBoundingBox(
						const vnl_vector<double> & pointCoordinates,
						unsigned int numberOfDimensions,
						unsigned int numberOfPoints,
						vnl_vector<double> & boundingBox );

	/**
	 * \brief This method calculates new coordinates to move a given 
	 *	shape on a specified 2D point, with origin in the upper-left corner
	 *  of the bounding-box
     *
     *	\param x Coordinate x of point the shape will be centered on.
     *	\param y Coordinate y of point the shape will be centered on.
     *	\param shape Points of the shape.
     *	\param outputShape New point coordinates.
     *	\param numberOfPoints Number of points.
     *	\param dX Translation on x-axis.
     *	\param dY Translation on y-axis.
     */
	static void MoveShape2DTo(double x, double y, 
						const vnl_vector<double> & shape,
						vnl_vector<double> & outputShape,
						unsigned int numberOfPoints,
						double & dX, double & dY);

	/// Add (dx, DY) to all the shape points 
	static void TranslateShape2DTo(double dX, double dY, 
						const vnl_vector<double> & shape,
						vnl_vector<double> & outputShape,
						unsigned int numberOfPoints);

protected:

	//% Argument:
	//%   pts -  3xN array of 2D homogeneous coordinates (3rd coordinate must be 1)
	//%
	//% Returns:
	//%   normalized_pts -  3xN array of transformed 2D homogeneous coordinates.  The
	//%             scaling parameter is normalised to 1 unless the point is at
	//%             infinity. 
	//%   T      -  The 3x3 transformation matrix, newpts = T*pts
	static void Normalize2Dpts(const vnl_matrix<double>& pts, vnl_matrix<double>& normalized_pts, vnl_matrix<double>& T, vnl_matrix<double>& T_inv);


};

#endif
