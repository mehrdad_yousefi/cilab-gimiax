/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "blProcrustesAlignmentFilter.h"
#include "blProcrustesAlignmentFilter.txx"

BASELIB_EXPORT template class blProcrustesAlignmentFilter<baselib::VnlMatrixType>;
BASELIB_EXPORT template class blProcrustesAlignmentFilter<baselib::UblasMatrixType>;
BASELIB_EXPORT template class blProcrustesAlignmentFilter<baselib::UblasDiskMatrixType>;
