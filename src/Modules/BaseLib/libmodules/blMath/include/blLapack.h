/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

/**
* C++ wrapping of the CLapack functions.
*/

#ifndef BLLAPACK_H
#define BLLAPACK_H

#include "BaseLibWin32Header.h"

// namespace
namespace math
{
namespace lapack
{

/**
* Computes the solution to a real system of linear equations
*     A * X = B,
*  where A is an N-by-N matrix and X and B are N-by-NRHS matrices.
* Calls the 'dgesv_' LAPACK routine (see documentation with implementation).
* @param _N The order of the matrix A.
* @param _nrhs The number of columns of the matrix B.
* @param A The N-by-N matrix A. On exit, the factors L and U from the factorization A = P*L*U;
* @param _lda The leading dimension of the array A.
* @param _ipiv The pivot indices that define the permutation matrix P.
* @param b The N-by-NRHS matrix of right hand side matrix B.
*        On exit, if INFO = 0, the N-by-NRHS solution matrix X.
* @param _ldb The leading dimension of the array B.
* @param _info Output information.
*/
BASELIB_EXPORT void solveLinearSystem(int _N, int _nrhs, double *A, int _lda, int *_ipiv, double *b, int _ldb, int &_info);

/**
* Computes for an N-by-N real nonsymmetric matrix A, the
*  eigenvalues and, optionally, the left and/or right eigenvectors.
* Calls the 'dgeev_' LAPACK routine (see documentation with implementation).
* @param _n The order of the matrix A.
* @param A The N-by-N matrix A. On exit, A has been overwritten.
* @param _info Output information.
*/
BASELIB_EXPORT void invertMatrix(int _n, double *A, int &_info);

} // namespace lapack
} // namespace math

#endif // BLLAPACK_H
