#include "blLapack.h"

// clapack 'C' functions
extern "C" {
#include "f2c.h"
#include "clapack.h"
}

// namespace
namespace math
{
namespace lapack
{

void solveLinearSystem(int _N, int _nrhs, double *A, int _lda, int *_ipiv, double *b, int _ldb, int &_info)
{
    integer N = static_cast<integer>( _N );
    integer nrhs = static_cast<integer>( _nrhs );
    integer lda = static_cast<integer>( _lda );
    integer ldb = static_cast<integer>( _ldb );
    integer info = static_cast<integer>( _info );

    integer* myIpiv = new integer[N];

    if (myIpiv != NULL) {
        for (int i=0; i<N; i++)
        {
            myIpiv[i] = static_cast<integer>( _ipiv[i] );
        }

        // Taken from the fortran file dgesv.f (http://www.netlib.org/lapack/double/dgesv.f).
        //
        // The LU decomposition with partial pivoting and row interchanges is
        // used to factor A as
        //    A = P * L * U,
        // where P is a permutation matrix, L is unit lower triangular, and U is
        // upper triangular.  The factored form of A is then used to solve the
        // system of equations A * X = B.
        // 
        // Arguments
        // =========
        // 
        // N       (input) INTEGER
        //         The number of linear equations, i.e., the order of the
        //         matrix A.  N >= 0.
        // 
        // NRHS    (input) INTEGER
        //          The number of right hand sides, i.e., the number of columns
        //          of the matrix B.  NRHS >= 0.
        // 
        // A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
        //          On entry, the N-by-N coefficient matrix A.
        //          On exit, the factors L and U from the factorization
        //          A = P*L*U; the unit diagonal elements of L are not stored.
        // 
        // LDA     (input) INTEGER
        //          The leading dimension of the array A.  LDA >= max(1,N).
        // 
        // IPIV    (output) INTEGER array, dimension (N)
        //          The pivot indices that define the permutation matrix P;
        //          row i of the matrix was interchanged with row IPIV(i).
        // 
        // B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
        //          On entry, the N-by-NRHS matrix of right hand side matrix B.
        //          On exit, if INFO = 0, the N-by-NRHS solution matrix X.
        // 
        // LDB     (input) INTEGER
        //          The leading dimension of the array B.  LDB >= max(1,N).
        // 
        // INFO    (output) INTEGER
        //          = 0:  successful exit
        //          < 0:  if INFO = -i, the i-th argument had an illegal value
        //          > 0:  if INFO = i, U(i,i) is exactly zero.  The factorization
        //                has been completed, but the factor U is exactly
        //                singular, so the solution could not be computed.
        dgesv_(&N, &nrhs, A, &lda, myIpiv, b, &ldb, &info);

        for (int i=0; i<N; i++)
        {
            _ipiv[i] = static_cast<int>( myIpiv[i] );
        }

        delete[] myIpiv;

        _info = static_cast<int>( info );
    }
}

void invertMatrix(int _n, double *A, int &_info)
{
    integer n = static_cast<integer>( _n );
    integer info = static_cast<integer>( _info );
    integer lwork = n*n; // dimension of the array WORK

    integer* ipiv = new integer[n]; // n+1?
    double* work = new double[lwork];

    if ((ipiv != NULL) && (work != NULL)) {
        // Taken from the fortran file dgesv.f (http://www.netlib.org/lapack/double/dgetrf.f).
        //
        //  DGETRF computes an LU factorization of a general M-by-N matrix A
        //  using partial pivoting with row interchanges.
        //  
        //  The factorization has the form
        //     A = P * L * U
        //  where P is a permutation matrix, L is lower triangular with unit
        //  diagonal elements (lower trapezoidal if m > n), and U is upper
        //  triangular (upper trapezoidal if m < n).
        //  
        //  This is the right-looking Level 3 BLAS version of the algorithm.
        //  
        //  Arguments
        //  =========
        //  
        //  M       (input) INTEGER
        //          The number of rows of the matrix A.  M >= 0.
        //  
        //  N       (input) INTEGER
        //          The number of columns of the matrix A.  N >= 0.
        //  
        //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
        //          On entry, the M-by-N matrix to be factored.
        //          On exit, the factors L and U from the factorization
        //          A = P*L*U; the unit diagonal elements of L are not stored.
        //  
        //  LDA     (input) INTEGER
        //          The leading dimension of the array A.  LDA >= max(1,M).
        //  
        //  IPIV    (output) INTEGER array, dimension (min(M,N))
        //         The pivot indices; for 1 <= i <= min(M,N), row i of the
        //         matrix was interchanged with row IPIV(i).
        //  
        //  INFO    (output) INTEGER
        //         = 0:  successful exit
        //         < 0:  if INFO = -i, the i-th argument had an illegal value
        //         > 0:  if INFO = i, U(i,i) is exactly zero. The factorization
        //               has been completed, but the factor U is exactly
        //               singular, and division by zero will occur if it is used
        //               to solve a system of equations.
        dgetrf_(&n,&n,A,&n,ipiv,&info);

        // Taken from the fortran file dgesv.f (http://www.netlib.org/lapack/double/dgetri.f).
        //
        // DGETRI computes the inverse of a matrix using the LU factorization
        // computed by DGETRF.
        // 
        // This method inverts U and then computes inv(A) by solving the system
        // inv(A)*L = inv(U) for inv(A).
        // 
        // Arguments
        // =========
        // 
        // N       (input) INTEGER
        //         The order of the matrix A.  N >= 0.
        // 
        // A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
        //         On entry, the factors L and U from the factorization
        //         A = P*L*U as computed by DGETRF.
        //         On exit, if INFO = 0, the inverse of the original matrix A.
        // 
        // LDA     (input) INTEGER
        //         The leading dimension of the array A.  LDA >= max(1,N).
        // 
        // IPIV    (input) INTEGER array, dimension (N)
        //         The pivot indices from DGETRF; for 1<=i<=N, row i of the
        //         matrix was interchanged with row IPIV(i).
        // 
        // WORK    (workspace/output) DOUBLE PRECISION array, dimension (MAX(1,LWORK))
        //         On exit, if INFO=0, then WORK(1) returns the optimal LWORK.
        // 
        // LWORK   (input) INTEGER
        //         The dimension of the array WORK.  LWORK >= max(1,N).
        //         For optimal performance LWORK >= N*NB, where NB is
        //         the optimal blocksize returned by ILAENV.
        // 
        //         If LWORK = -1, then a workspace query is assumed; the routine
        //         only calculates the optimal size of the WORK array, returns
        //         this value as the first entry of the WORK array, and no error
        //         message related to LWORK is issued by XERBLA.
        // 
        // INFO    (output) INTEGER
        //         = 0:  successful exit
        //         < 0:  if INFO = -i, the i-th argument had an illegal value
        //         > 0:  if INFO = i, U(i,i) is exactly zero; the matrix is
        //             singular and its inverse could not be computed.
        dgetri_(&n,A,&n,ipiv,work,&lwork,&info);

        delete[] work;
        delete[] ipiv;

        _info = static_cast<int>( info );
    }
}

} // namespace lapack
} // namespace math
