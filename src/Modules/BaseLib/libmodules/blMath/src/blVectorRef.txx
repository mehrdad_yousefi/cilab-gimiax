/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef __blVectorRef_txx
#define __blVectorRef_txx

#include "blVectorRef.h"

template <class TElement>
blVectorRef<TElement>::blVectorRef() 
{
}

template <class TElement>
blVectorRef<TElement>::~blVectorRef() 
{
	this->data = 0;
}

/** \brief Creates vector containing n elements.
* Elements are not initialized.
*/
template <class TElement>
blVectorRef<TElement>::blVectorRef(unsigned size, TElement *space) : Superclass()
{
    this->data = space;
    this->num_elmts = size;

#if VCL_HAS_SLICED_DESTRUCTOR_BUG
    vnl_vector_own_data = 0;
#endif
}

#endif
