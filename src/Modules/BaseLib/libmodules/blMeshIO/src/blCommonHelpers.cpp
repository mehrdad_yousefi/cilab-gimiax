// Copyright 2008 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


// own header-file
#include "blCommonHelpers.h"

// BaseLib-includes
#include "blMeshTypes.h"

// includes from other Toolkit-Libs
// ...none at the moment...

// Third Party Library includes
#include "boost/cstdint.hpp" // workaround because of missing "stdint.h" (C++ include) in some compilers

// C/C++ includes
#include <sstream>
#include <string>
#include <list>
#include <sys/stat.h>
#include "BaseLibTetgenWin32Header.h"

namespace base {
namespace meshio {

template<typename T>
std::string stringify(const std::list<T>& x)
{
	std::ostringstream o;
	o << "[ ";
	
	// first entry without preceded comma
    typename std::list<T>::const_iterator it = x.begin();
	if (it != x.end()) {
    	o << *it;
    	*it++;
    }
	
	// all other entries with preceded comma
	while (it != x.end()) {
    	o << ", " << *it;
    	it++;
    }
    
	o << " ]";
	return o.str();
}

template<typename T>
std::string stringify( T x)
{
	std::ostringstream o;
	o << x;
	return o.str();
}

BASELIBTETGEN_EXPORT template std::string stringify(char);
BASELIBTETGEN_EXPORT template std::string stringify(short);
BASELIBTETGEN_EXPORT template std::string stringify(int);
BASELIBTETGEN_EXPORT template std::string stringify(unsigned int);
BASELIBTETGEN_EXPORT template std::string stringify(boost::int64_t);
BASELIBTETGEN_EXPORT template std::string stringify(boost::uint64_t);
BASELIBTETGEN_EXPORT template std::string stringify(float);
BASELIBTETGEN_EXPORT template std::string stringify(double);
BASELIBTETGEN_EXPORT template std::string stringify(dVector3D);

BASELIBTETGEN_EXPORT template std::string stringify(const std::list<std::string>&);

} // meshio
} // base



