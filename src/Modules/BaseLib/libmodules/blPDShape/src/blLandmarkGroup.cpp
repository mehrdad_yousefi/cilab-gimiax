/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#include "blLandmarkGroup.h"

blLandmarkGroup::blLandmarkGroup(unsigned int groupID, 
								 unsigned int numberOfLandmarks,
								 unsigned int * landmarkIds)
{

	this->groupID = groupID;
	this->numberLandmarks = numberOfLandmarks;
	this->landmarks = landmarkIds;
}

bool blLandmarkGroup::LandmarkInGroup(unsigned int landmarkID)
{
	// linear search

	unsigned int i;
	for (i = 0; i < this->numberLandmarks; i++) 
	{
		if (landmarks[i] == landmarkID)
		{
			return true;
		}		
	}
	return false;
}

