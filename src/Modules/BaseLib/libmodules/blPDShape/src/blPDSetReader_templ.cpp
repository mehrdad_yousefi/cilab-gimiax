/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

/*
brief   : This class allow to create a new PDSet from a file that 
*/

#include "blPDSetReader_templ.h"
#include "blPDSetReader_templ.txx"

BASELIB_EXPORT template class blPDSetReader_templ<baselib::VnlMatrixType>;
BASELIB_EXPORT template class blPDSetReader_templ<baselib::UblasMatrixType>;
BASELIB_EXPORT template class blPDSetReader_templ<baselib::UblasDiskMatrixType>;
