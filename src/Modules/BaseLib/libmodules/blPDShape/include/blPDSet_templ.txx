/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "blPDSet_templ.h"
#include "blLinearAlgebraOperations.h"

//--------------------------------------------------
template< class ArgMatrixType >
blPDSet_templ<ArgMatrixType>::blPDSet_templ() : shapes(NULL)
//--------------------------------------------------
{
	this->numberDimensions = 0;
	this->numberLandmarks = 0;
	this->numberShapes = 0;	
}

//--------------------------------------------------
template< class ArgMatrixType >
blPDSet_templ<ArgMatrixType>::~blPDSet_templ()
//--------------------------------------------------
{
	delete [] this->shapes;
}

//--------------------------------------------------
template< class ArgMatrixType >
void blPDSet_templ<ArgMatrixType>::SetParameters(unsigned int numberOfShapes, 
							blPDShapeInterface::Pointer shapes[])
//--------------------------------------------------
{
	bool newSet = ( numberOfShapes != this->numberShapes ) || 
						( this->shapes == NULL );

	this->numberDimensions = 0;
	this->numberLandmarks = 0;
	this->numberShapes = numberOfShapes;
	
	//copy shapes
	if ( newSet )
	{
		if ( this->shapes != NULL ) 
		{
			delete[] this->shapes;
		}

		this->shapes = new blPDShapeInterface::Pointer [numberOfShapes];
	}

	for (unsigned int i=0; i<numberOfShapes; i++)
	{
		this->shapes[i] = shapes[i];
	}
	
	if (numberOfShapes > 0)
	{
		this->numberDimensions = this->shapes[0]->GetDimension();
		this->numberLandmarks = this->shapes[0]->GetNumberOfLandmarks();
	}
	

#ifdef DEBUG_MSG_CONSTRUCTOR
	std::cout << "New blPDSet_templ with ";
	std::cout << this->numberShapes << " shapes" << std::endl;
#endif

}

//--------------------------------------------------
template< class ArgMatrixType >
void blPDSet_templ<ArgMatrixType>::SetParameters(
	MatrixPointer m_ptr)
//--------------------------------------------------
{

	//copy shapes
	
	if (this->numberLandmarks)
	{
		this->thePoints = m_ptr;
		this->numberShapes = bllao::NrRows ( *this->thePoints );
	}

    	

#ifdef DEBUG_MSG_CONSTRUCTOR
	std::cout << "New blPDSet_templ with ";
	std::cout << this->numberShapes << " shapes" << std::endl;
#endif

}
	 
//--------------------------------------------------
template< class ArgMatrixType >
void blPDSet_templ<ArgMatrixType>::GetPoints(double ** points)
//--------------------------------------------------
{	
	unsigned int i;
	for (i = 0; i < this->numberShapes; i++)
	{	
		this->shapes[i]->GetPoints(points[i]);
	}		
}	

//--------------------------------------------------
template< class ArgMatrixType >
void blPDSet_templ<ArgMatrixType>::GetPoints(blLandmarkGroup * landmarkGroup, double ** points)
//--------------------------------------------------
{	
	unsigned int i;
	for (i = 0; i < this->numberShapes; i++)
	{
		this->shapes[i]->GetPoints(landmarkGroup, points[i]);
	}	
}	

//--------------------------------------------------
template< class ArgMatrixType >
void blPDSet_templ<ArgMatrixType>::GetPoints(vnl_matrix<double> & points)
//--------------------------------------------------
{	
	// number of cols 
//TODO::UNUSED VARIABLE
// 	int numCols = this->numberLandmarks * this->numberDimensions;
////////////
		
	// copy the points coordinates in the matrix
	
	unsigned int i;
	for (i = 0; i < numberShapes; i++)
	{			
		this->shapes[i]->GetPoints(points[i]);
	}	
}			


