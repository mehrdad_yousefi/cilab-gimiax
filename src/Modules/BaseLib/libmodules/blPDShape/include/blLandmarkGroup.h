/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/



#ifndef __blLandmarkGroup_h
#define __blLandmarkGroup_h

/**
 * \brief This class defines a group of landmarks
 * \ingroup blPDShape
 *
 *	A Landmark Group is a container class that gives information about the
 *	landmarks in a landmark group.
 */
class blLandmarkGroup 
{

    public:
        /** 
         * This constructor creates a new landmark group
         *
         * \param groupID identifier for the landmark group
         * \param numberOfLandmarks number of landmarks in this group
         * \param landmarkIds array with the landmark identifiers in this group
         */
		blLandmarkGroup (unsigned int groupID, unsigned int numberOfLandmarks, 
							unsigned int * landmarkIds);

		/** \brief get the landmark group identifier */
		unsigned int GetGroupID() const
			{ return this->groupID; };

		/** \brief return a pointer to the landmark ids */
		const unsigned int * GetLandmarks() const
			{ return this->landmarks; };

		/** \brief return the number of landmarks */
		unsigned int GetNumberOfLandmarks() const
			{ return this->numberLandmarks; };		

        /** 
         *	This method returns true if the landmark whose landmark id is specified
         *	belongs to this group.
         *
         *	\param landmarkID identifier of the specified landmark
         */
		bool LandmarkInGroup(unsigned int landmarkID);

    private:
        unsigned int groupID;			//!< group identifier 
        unsigned int numberLandmarks;   //!< number of landmarks in this group
		unsigned int * landmarks;		//!< landmarks (id) in this group

};

#endif
