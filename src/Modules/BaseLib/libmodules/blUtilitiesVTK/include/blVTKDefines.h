/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _blVTKDefines_h
#define _blVTKDefines_h

#include <vector>
class vtkPolyData;
class vtkPlane;

namespace blVTKDefines {

	/**
	* \brief vector of shapes
	* \ingroup blUtilitiesVTK
	*/
	typedef std::vector<vtkPolyData*> ShapeVectorType;

	/**
	* \brief vector of planes
	* \ingroup blUtilitiesVTK
	*/
	typedef std::vector<vtkPlane*> PlaneVectorType;

} // end namespace blVTKDefines

#endif // _blVTKDefines_h
