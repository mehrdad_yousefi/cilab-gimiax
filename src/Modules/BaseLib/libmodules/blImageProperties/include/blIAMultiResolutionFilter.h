/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef __blIAMultiResolutionFilter_h
#define __blIAMultiResolutionFilter_h

#include <itkRecursiveMultiResolutionPyramidImageFilter.h>

//--------------------------------
// own dependencies

#include "blLightObject.h"
#include "blSmartPointer.h"
#include "blMacro.h"



/**
 * \brief Wraps the itk::RecursiveMultiResolutionPyramidImageFilter
 * \ingroup blImageProperties
 *
 *	Takes a blImage as Input and returns a Pyramid of blImages
 *	If blImage is multispectral probably we'll need to get a different
 *	pyramid for each image.
 */
template <class InputImageType, class OutputImageType>
class blIAMultiResolutionFilter: 
		public itk::RecursiveMultiResolutionPyramidImageFilter<
											InputImageType, OutputImageType>
{
public:

	typedef blIAMultiResolutionFilter Self;
	typedef blSmartPointer<Self> Pointer;

	blNewMacro(Self);

protected:
	/** \brief Default Constructor
	 *
	 */
	blIAMultiResolutionFilter();
	
	/** \brief Default Destructor
	 *
	 */
	virtual ~blIAMultiResolutionFilter();

private:
	blIAMultiResolutionFilter(const Self&); //purposely not implemented
	void operator=(const Self&); //purposely not implemented


};

#include "blIAMultiResolutionFilter.txx"
#endif
