/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#ifndef __blIARescalerFilter_txx
#define __blIARescalerFilter_txx

#include "blIARescalerFilter.h"


//--------------------------------------------------
template <class inputImage, class outputImage>
blIARescalerFilter<inputImage, outputImage>::blIARescalerFilter()
//--------------------------------------------------
{
};



//--------------------------------------------------	
template <class inputImage, class outputImage>
blIARescalerFilter<inputImage, outputImage>::~blIARescalerFilter()
//--------------------------------------------------
{
};




#endif
