/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

//MultiResolution filter.

#ifndef __blIAMultiResolutionFilter_txx
#define __blIAMultiResolutionFilter_txx

#include "blIAMultiResolutionFilter.h"


//--------------------------------------------------
template <class inputImage, class outputImage>
blIAMultiResolutionFilter<inputImage, outputImage>::blIAMultiResolutionFilter()
//--------------------------------------------------
{
};



//--------------------------------------------------	
template <class inputImage, class outputImage>
blIAMultiResolutionFilter<inputImage, outputImage>::~blIAMultiResolutionFilter()
//--------------------------------------------------
{
};




#endif
