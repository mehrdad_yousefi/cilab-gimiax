/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


//Implementation file for the Independent Component Analysis.

#ifndef __blIAGradientComputation_txx
#define __blIAGradientComputation_txx


#include "blIAGradientComputation.h"

/**
 *	Constructor
 */
template <typename InputImageType, typename OutputImageType>
blIAGradientComputation<InputImageType, OutputImageType>::blIAGradientComputation() 
{
};



/**
 *	Destructor
 */
template <typename InputImageType, typename OutputImageType>
blIAGradientComputation<InputImageType, OutputImageType>::~blIAGradientComputation() 
{
};

#endif
