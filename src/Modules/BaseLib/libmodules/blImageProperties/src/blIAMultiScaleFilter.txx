/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

//Implementation file for the Independent Component Analysis.

#ifndef __blIAMultiScaleFilter_txx
#define __blIAMultiScaleFilter_txx

#include "blIAMultiScaleFilter.h"

/** \brief Default Constructor
 *
 */
template <class inputImage, class outputImage>
blIAMultiScaleFilter<inputImage, outputImage>::blIAMultiScaleFilter() 
{
};


	
/** \brief Default Destructor
 *
 */
template <class inputImage, class outputImage>
blIAMultiScaleFilter<inputImage, outputImage>::~blIAMultiScaleFilter() 
{
};


#endif;
