/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
//base class for images in SMoLib

#ifndef _blImageWriter_txx
#define _blImageWriter_txx

#include "blImageWriter.h"



/**
 * Constructor
 */
//--------------------------------------------------
template <class TInputImage>
blImageWriter<TInputImage>::blImageWriter()
//--------------------------------------------------
{ 
}



/**
 *	Destructor
 */
//--------------------------------------------------
template <class TInputImage>
blImageWriter<TInputImage>::~blImageWriter() 
//--------------------------------------------------
{ 
}


#endif


