/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

//Reader for blImage class

#ifndef _blImageReader_txx
#define _blImageReader_txx

#include "blImageReader.h"




/**
 * Constructor
 */
//--------------------------------------------------
template <class TOutputImage>
blImageReader<TOutputImage>::blImageReader()
//--------------------------------------------------
{ 
}



/**
 *	Destructor
 */
//--------------------------------------------------
template <class TOutputImage>
blImageReader<TOutputImage>::~blImageReader()
//--------------------------------------------------
{ 
}



#endif


