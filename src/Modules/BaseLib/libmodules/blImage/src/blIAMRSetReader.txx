/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


//Constructs set of images and allows reading at different resoutions

#ifndef _blIAMRSetReader_txx
#define _blIAMRSetReader_txx

#include "blIAMRSetReader.h"


/** 
 *	This method loads the path to the images
 *
 */
//--------------------------------------------------
template <class TImage>
blIAMRSetReader<TImage>::blIAMRSetReader():
	_resolution(1)
//--------------------------------------------------
{

}



/** 
 *	The Destructor.
 */
//--------------------------------------------------
template <class TImage>
blIAMRSetReader<TImage>::~blIAMRSetReader()
//--------------------------------------------------
{

}



#endif
