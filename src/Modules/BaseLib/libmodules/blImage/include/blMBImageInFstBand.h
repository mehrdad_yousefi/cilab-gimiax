/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _blMBImageInFstBand_h
#define _blMBImageInFstBand_h


//#include <blImage.h>
#include <blMultiBandImage.h>

#include "blLightObject.h"
#include "blSmartPointer.h"
#include "blMacro.h"

#include <vector>


/**
 * \brief Multi-Band image, original image is loaded into the first band
 * \ingroup blImage 
 */
template <class TImage, unsigned int NBands>
class blMBImageInFstBand: public blMultiBandImage<TImage, NBands>
{
	public:
		
		typedef blMBImageInFstBand Self;
		typedef blSmartPointer<Self>  Pointer;
		typedef blSmartPointer<const Self>  ConstPointer;

		/** \brief Static constructor */
		blNewMacro(Self);

		virtual void LoadImageFromFilename( const char* filename );

		virtual void SaveImage( const char* filename );

	protected:
		blMBImageInFstBand();
		virtual ~blMBImageInFstBand();

	private:
		blMBImageInFstBand(const Self&); //purposedly not implemented (see itk::Image)
		void operator= (const Self&); //purposedly not implemented (see itk::Image)


};


#include "blMBImageInFstBand.txx"

#endif
