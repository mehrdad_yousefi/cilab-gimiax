/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#ifndef _blIASampler_txx
#define _blIASampler_txx

//#include "blIASampler.h"

/** \brief Default Constructor
 *
 */
template <class TImage>
blIASampler<TImage>::blIASampler()
{
}

/** \brief Default Constructor
 *
 */
template <class TImage>
blIASampler<TImage>::~blIASampler()
{

}




/** \brief Set Normal direction
 *  
 */
//----------------------------------------------------
template <class TImage>
void blIASampler<TImage>::SetNormal(PointType point)
//----------------------------------------------------
{	
	this->xNormal = point[0];
	this->yNormal = point[1];
}

	

/** \brief Set Point to sample around
 *  
 */
//----------------------------------------------------
template <class TImage>
void blIASampler<TImage>::SetPoint(PointType point)
//----------------------------------------------------
{
	this->xPoint = point[0];
	this->yPoint = point[1];
}

#endif
