/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

//Implementation file for the masked sampler.

#ifndef __blIAMaskedSampler_txx
#define __blIAMaskedSampler_txx

//#include "blIAMaskedSampler.h"


template <class TImage, class TPixel, unsigned int VImageDimension>
blIAMaskedSampler<TImage, TPixel, VImageDimension>::blIAMaskedSampler():
	_nonMaskedPixelValue(1)
{
};

template <class TImage, class TPixel, unsigned int VImageDimension>
blIAMaskedSampler<TImage, TPixel, VImageDimension>::~blIAMaskedSampler() 
{
};




#endif
