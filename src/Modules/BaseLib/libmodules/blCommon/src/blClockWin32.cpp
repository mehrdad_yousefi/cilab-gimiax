/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "blClockWin32.h"

/**
\brief   
\ingroup blCommon
\author  
\date    
*/

#ifdef WIN32

blClockWin32 :: blClockWin32(){
	if ( QueryPerformanceFrequency(&m_FreqCPU) == 0 )
		MessageBox(NULL, L"H.P.T. Not detected \n", L"Gimias Warning", MB_OK);
}

blClockWin32 :: ~blClockWin32(){}

void blClockWin32::Start()	{
	QueryPerformanceCounter(&m_tics1);
}

double blClockWin32::Finish()	{
	QueryPerformanceCounter(&m_tics2);
	return  Temps( );
}

double blClockWin32::Temps(){
	m_Temps = (double) (m_tics2.QuadPart - m_tics1.QuadPart ) / m_FreqCPU.QuadPart;
	return m_Temps;
}
#endif // _WIN32
