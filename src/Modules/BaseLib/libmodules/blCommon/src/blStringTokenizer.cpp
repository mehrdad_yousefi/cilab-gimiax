/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include <cstring>
#include "blStringTokenizer.h"

blStringTokenizer::blStringTokenizer():	
	numberTokens(0), nextToken(0), delim(" \t\n\r\f")
{
}

blStringTokenizer::blStringTokenizer(const char * str):
	str(str),
	delim(" \t\n\r\f") 
{
	//this->delim = " \t\n\r\f";
	//this->str = str;
	this->numberTokens = 0;
	this->nextToken = 0;
	this->GetTokens();
}

blStringTokenizer::blStringTokenizer(const char * str, 
									 const char * delim):
	str(str),
	delim(delim) 
{
	//this->delim = delim;
	//this->str = str;
	this->numberTokens = 0;
	this->nextToken = 0;
	this->GetTokens();
}

blStringTokenizer::~blStringTokenizer()
{
}

void blStringTokenizer::GetTokens() 
{

	// initialize member vars.
	this->nextToken = 0;
	this->numberTokens = 0;
	this->tokens.clear();	// remove previous tokens

	// copy input string into buffer
	char * buffer = new char[this->str.length() + 1];
  // FIX mkarmona use smartpointers
  if (buffer != NULL) {
    strcpy(buffer, this->str.c_str());

    // call strtok with buffer
    char * result = 0;
    result = strtok(buffer, this->delim.c_str());	

    while (result)
    {
        this->tokens.push_back(result);
        this->numberTokens++;

        result = strtok(0, this->delim.c_str());
    }

    delete[] buffer;
  }
}

const char * blStringTokenizer::NextToken() 
{ 
	std::string & strRef = this->tokens[this->nextToken++];
	return strRef.c_str();
}

void blStringTokenizer::Tokenize(const char * str)
{
	this->str = str;
	this->GetTokens();
}

void blStringTokenizer::Tokenize(const char * str, unsigned int length)
{
	this->str.assign(str, length);
	this->GetTokens();
}
