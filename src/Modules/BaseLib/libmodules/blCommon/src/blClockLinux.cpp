/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "blClockLinux.h"

/**
\brief   
\ingroup blCommon
\author  
\date    
*/
blClockLinux :: blClockLinux(){
	m_ClockStart = 0;
	m_ClockStop = 0;
}

blClockLinux :: ~blClockLinux(){}

void blClockLinux::Start()	{
	m_ClockStart = clock( );
}

double blClockLinux::Finish()	{
	m_ClockStop = clock( );
	return  Temps( );
}

double blClockLinux::Temps(){
	m_Temps = (double) (m_ClockStop - m_ClockStart ) / CLOCKS_PER_SEC;
	return m_Temps;
}
