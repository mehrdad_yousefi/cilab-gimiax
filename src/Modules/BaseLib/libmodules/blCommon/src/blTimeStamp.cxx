/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "blTimeStamp.h"

blTimeStamp* blTimeStamp::New()
{
	return new Self;
}

void blTimeStamp::Modified()
{
  static unsigned long blTimeStampTime = 0;

  m_ModifiedTime = ++blTimeStampTime;

}


