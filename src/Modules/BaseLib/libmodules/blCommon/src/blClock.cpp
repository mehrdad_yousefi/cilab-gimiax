/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "blClock.h"
#include "blClockWin32.h"
#include "blClockLinux.h"

/**
\brief   
\ingroup blCommon
\author  
\date    
*/
blClock :: blClock(){

#ifdef WIN32
	m_blClock = new blClockWin32;
#else 
	m_blClock = new blClockLinux;
#endif
}

blClock :: ~blClock(){
	delete m_blClock;
	m_blClock = NULL;
}

void blClock::Start()	{
	m_blClock->Start();
}

double blClock::Finish()	{
	return  m_blClock->Finish();
}

double blClock::Temps(){
	return m_blClock->Temps();
}
