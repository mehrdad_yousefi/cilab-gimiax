/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "blObject.h"

unsigned long
blObject::GetMTime() const
{
	return m_MTime.GetMTime();
}


/**
* Make sure this object's modified time is greater than all others.
*/
void
blObject::Modified() const
{
	m_MTime.Modified();
}

blObject::blObject()
{
	Modified( );
}

blObject::~blObject()
{

}
