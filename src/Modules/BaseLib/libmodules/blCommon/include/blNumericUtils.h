/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef __blNumericUtils_h
#define __blNumericUtils_h

/**
\brief Compare two float numbers with precision epsilon (for example 0.0001)
\ingroup blCommon
*/
#define FLOAT_EQ(x,v,epsilon) \
	( ( (v - epsilon) < x ) && (x < ( v + epsilon) ) )

#define FLOAT_EQ_INCLUSIVE(x,v,epsilon) \
	( ( (v - epsilon) <= x ) && (x <= ( v + epsilon) ) )

namespace blNumericUtils
{

	/**
	\ingroup blCommon
	\author Xavi Planes
	\date 15-09-09
	*/
	long round(double a);

}


#endif // __blNumericUtils_h
