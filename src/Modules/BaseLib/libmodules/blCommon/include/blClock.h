/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _CLOCK
#define _CLOCK

class blClockImpl;

/** 
* \brief High precision clock
* \ingroup blCommon
*/
class blClock{
public:
	blClock();
	~blClock();
	/// Store the current time 
	void Start();
	/// Store the current time and call Temps()
	double Finish();
	/// Compute the time and return m_Temps
	double Temps();
protected:
	blClockImpl		*m_blClock;
};

#endif
