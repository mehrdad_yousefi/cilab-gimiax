/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef __blStringTokenizer_H
#define __blStringTokenizer_H

#include <iostream>
#include <string>
#include <vector>

/**	
* \brief This class defines a tokenizer, which can break a 
*	string into tokens
* \ingroup blCommon
*
*	This class has some delimiters by default (" \n\t\r\f"), but it is 
*	possible to specify any other set of delimiters.
\author  Sergio Andres Ruiz
\date    2007/04/19
*/

class blStringTokenizer 
{

public:
	/** \brief Empty Constructor. The tokenizer uses the default delimiter set, which is " \t\n\r\f". */
	blStringTokenizer();
    /**
     *	Constrcutor.
     *  The tokenizer uses the default delimiter set, which is " \t\n\r\f"
     *	It generates tokens for the given string.
     *
     *	\param str the specified string
     */
	blStringTokenizer(const char * str);
    /**
     * Constrcutor.
     * The characters in the delim argument are the delimiters 
     * for separating tokens.
     * It generates tokens for the given string.
     *
     * \param str the specified string
     * \param delim a set of delimiters
     */
	blStringTokenizer(const char * str, const char * delim); 		
	/** \brief Destructor */
	~blStringTokenizer();
	/** \brief return the number of tokens */
	int CountTokens() const
	{ return this->numberTokens; };
	/** \brief return true if there are more tokens */
	bool HasMoreTokens() const
	{ 	return ( this->nextToken < this->numberTokens ); };
    /** 
     *	This method returns the next token as a C string
     *
     *	\return a const pointer to next token
     */
	const char * NextToken();
	/** \brief sets the string to be tokenized */
	void SetDelimiters(const char * delim)
	{
		this->delim = delim;
	};
    /**
     *	This method sets a new string to be tokenized and 
     *	generates tokens.
     *
     *	\param str input string
     */
	void Tokenize(const char * str);
    /**
     *	This method sets a new string to be tokenized and 
     *	generates tokens.
     *
     *	\param str input string
     *	\param length number of chars to be considered
     */
	void Tokenize(const char * str, unsigned int length);

private:
	unsigned int numberTokens;
	unsigned int nextToken;
	std::string str;
	std::string delim;
	std::vector<std::string> tokens;	//!< tokens in current string
    /**
     *	This method goes through the string and get the tokens
     */
	void GetTokens();
};

#endif
