/**
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef BLLOGGER_H
#define BLLOGGER_H

// log4cplus
#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

/**
* Class used to initialise the log4cplus.
* Uses a text based configuration from the "resource/logging.conf" file.
* \ingroup blCommon
*/
class blLogger
{

public:

    /**
    * Get a log4cplus::Logger object.
    * @param name The name of the logger (can be used in the logging configuration to filter messages).
    */
    static log4cplus::Logger getInstance(const char* name);

private:

    /**
    * Private constructor, don't create instances of this class.
    */
    blLogger() {}
    
}; // class blLogger

#endif // BLLOGGER_H

