/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef blConsole_H
#define blConsole_H

#include <iostream>
#include <limits>

/** 
\brief Console window utility functions
\ingroup blCommon
\date 16/04/2009
\author Xavi Planes
*/
namespace blConsole{

	//! Show the message "Press ENTER to continue..."
	void pause();

} // namespace blConsole

#endif //blConsole_H
