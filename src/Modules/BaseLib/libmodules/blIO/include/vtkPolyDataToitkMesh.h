/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef __vtkPolyDataToitkMesh_h__
#define __vtkPolyDataToitkMesh_h__

#include "BaseLibVTKWin32Header.h"
#include "vtkPoints.h"
#include "vtkCellArray.h"
#include "vtkPolyData.h"
#include "itkDefaultDynamicMeshTraits.h"
#include "itkMesh.h"
#include "itkTriangleCell.h"


/** 
\class vtkPolyDataToitkMesh
\ingroup blIO
*/
class BASELIBVTK_EXPORT vtkPolyDataToitkMesh
{

 public:

  vtkPolyDataToitkMesh( void );
  virtual ~vtkPolyDataToitkMesh( void );

  typedef itk::DefaultDynamicMeshTraits<double, 3, 3,double,double> TriangleMeshTraits;
  typedef itk::Mesh<double,3, TriangleMeshTraits> TriangleMeshType;

  /**
  The SetInput method provides pointer to the vtkPolyData
  */
  void SetInput( vtkPolyData * polydata);
  TriangleMeshType * GetOutput();
  void ConvertvtkToitk();

  TriangleMeshType::Pointer  m_itkMesh;

  vtkPolyData           * m_PolyData;

  
};

#endif
