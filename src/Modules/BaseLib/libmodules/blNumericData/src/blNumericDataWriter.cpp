/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "blNumericDataWriter.h"
#include "blTextUtils.h"

#include <fstream>

blNumericDataWriter::blNumericDataWriter()
{
}

blNumericDataWriter::~blNumericDataWriter()
{
}

void blNumericDataWriter::InternalUpdate()
{
	// save the file as CSV
	std::ofstream numericDataFile;
	numericDataFile.open ( m_Filename.c_str() );

	if (!numericDataFile.good())
	{
		std::stringstream sstream;
		sstream << "Cannot open file for writing: " << m_Filename;
		throw std::runtime_error( sstream.str( ).c_str( ) );
	}

	// Write tag names and retrieve tag values
	std::list< std::list<std::string> > values;
	values.resize( m_Data->GetLength( ) );
	std::list< std::list<std::string> >::iterator itValue = values.begin( );
	for ( blTagMap::ListIterator it = m_Data->ListBegin() ; it != m_Data->ListEnd() ; it++ )
	{
		blTag::Pointer currentTag = m_Data->GetTag(it);
		numericDataFile<< currentTag->GetName() << ";";

		blTextUtils::ParseLine( currentTag->GetValueAsString( ), ';', (*itValue) );
		itValue++;
	} 
	numericDataFile << std::endl;

	// Write tag values
	while ( !values.empty( ) && !(*values.begin( )).empty( ) )
	{
		for ( itValue = values.begin( ) ; itValue != values.end( ) ; itValue++ )
		{
			// Parse value
			std::list<std::string> &words = *itValue;
			numericDataFile << *words.begin( ) << ";";
			words.pop_front( );
		} 
		numericDataFile << std::endl;
	}

	// Two end of lines
	numericDataFile << std::endl << std::endl;
	numericDataFile.close();
}


