/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "blXMLTagMapWriter.h"
#include "tinyxml.h"

blXMLTagMapWriter::blXMLTagMapWriter()
{
	m_Version = 0;
}

blXMLTagMapWriter::~blXMLTagMapWriter()
{
}

void blXMLTagMapWriter::InternalUpdate()
{
	std::ostringstream strVersion;
	strVersion << m_Version;
	TiXmlDocument doc;
	TiXmlDeclaration * decl = new TiXmlDeclaration( strVersion.str().c_str(), "", "" );
	doc.LinkEndChild( decl );

	SaveData( &doc, m_Data, "tagmap" );

	doc.SaveFile( m_Filename );
}

void blXMLTagMapWriter::SaveData( 
	TiXmlNode* node, blTagMap::Pointer tagMapInstance, const std::string &nodeName )
{
	TiXmlElement * tagMapElm = new TiXmlElement( nodeName );
	tagMapElm->SetAttribute("size", int( tagMapInstance->GetLength( ) ) );
	node->LinkEndChild( tagMapElm );

	blTagMap::ListIterator it;
	for ( it = tagMapInstance->ListBegin() ; it != tagMapInstance->ListEnd() ; it++ )
	{
		blTag::Pointer tag = tagMapInstance->GetTag( it );
		TiXmlElement *tagElm = new TiXmlElement( "tag" );
		tagElm->SetAttribute( "name", tag->GetName() );
		tagElm->SetAttribute( "type", tag->GetTypeName() );
		tagElm->InsertEndChild(TiXmlText( tag->GetValueAsString() ));
		//tagElm->SetValue( tag->GetValueAsString() );
		tagMapElm->LinkEndChild( tagElm );

		if ( tag->GetValue().type() == typeid( blTagMap::Pointer ) )
		{
			blTagMap::Pointer tagMapProperty;
			tag->GetValue<blTagMap::Pointer>( tagMapProperty );
			SaveData( tagElm, tagMapProperty, "tagmap" );
		}
	}
}

