/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "blBaseTagMapIO.h"


blBaseTagMapIO::blBaseTagMapIO() 
{
	m_Version = 0;
}

blBaseTagMapIO::~blBaseTagMapIO()
{
}

void blBaseTagMapIO::SetFilename( const char* filename )
{
	m_Filename = filename;
}

void blBaseTagMapIO::SetInput(blTagMap::Pointer tagMap )
{
	m_Data = tagMap;
}

void blBaseTagMapIO::Update()
{
	InternalUpdate();
}

blTagMap::Pointer blBaseTagMapIO::GetOutput()
{
	return m_Data;
}

