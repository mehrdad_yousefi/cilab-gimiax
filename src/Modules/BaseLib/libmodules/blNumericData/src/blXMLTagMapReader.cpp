/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "blXMLTagMapReader.h"
#include "tinyxml.h"

blXMLTagMapReader::blXMLTagMapReader() 
{
}

blXMLTagMapReader::~blXMLTagMapReader()
{
}

void blXMLTagMapReader::InternalUpdate()
{
	TiXmlDocument doc( m_Filename );
	if (!doc.LoadFile()) return;

	m_Data = blTagMap::New( );
	LoadData( &doc, m_Data, "tagmap" );
}

void blXMLTagMapReader::LoadData( 
	TiXmlNode* node, blTagMap::Pointer tagMapInstance, const std::string &nodeName )
{
	if ( !node->FirstChild( nodeName ) )
	{
		throw std::runtime_error( "File format is not TAGMAP" );
	}

	TiXmlElement* mainElem;
	mainElem = node->FirstChild( nodeName )->ToElement();
	if ( !mainElem ) return;

    // tagmap with 0 children -> Nothing to read
    if ( mainElem->NoChildren( ) )
    {
            return;
    }

	if ( !mainElem->FirstChild( "tag" ) )
	{
		throw std::runtime_error( "File format is not TAGMAP" );
	}
	TiXmlElement* childElem = mainElem->FirstChild( "tag" )->ToElement();
	while( childElem )
	{
		std::string name = childElem->Attribute("name");
		std::string valueTypeName = childElem->Attribute("type");
		std::string valueAsString;
		if ( childElem->GetText( ) != NULL )
		{
			valueAsString = childElem->GetText( );
		}
		
		blTag::Pointer tag = blTag::New( name, NULL );
		tag->SetValueAsString( valueTypeName, valueAsString );

		if ( valueTypeName == blTag::GetTypeName( typeid( blTagMap::Pointer ) ) )
		{
			blTagMap::Pointer tagMapProperty = blTagMap::New();
			LoadData( childElem, tagMapProperty, "tagmap" );
			tag->SetValue( tagMapProperty );
		}

		tagMapInstance->AddTag( tag );

		TiXmlNode* nextNode = mainElem->IterateChildren( "tag", childElem);
		childElem = nextNode ? nextNode->ToElement() : NULL;
	}
}
