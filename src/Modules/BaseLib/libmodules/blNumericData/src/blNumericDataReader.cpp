/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "blNumericDataReader.h"
#include "blTextUtils.h"


#include <fstream>
#include <stdlib.h>


blNumericDataReader::blNumericDataReader() 
{
}

blNumericDataReader::~blNumericDataReader()
{
}

void blNumericDataReader::InternalUpdate()
{
	// save the file as CSV
	std::ifstream numericDataFile;
	numericDataFile.open ( m_Filename.c_str() );
	if (!numericDataFile.good())
	{
		m_Data = NULL;
		return;
	}

	m_Data = blTagMap::New( );

	// Read a first line: labels
	std::string line;
	std::getline(numericDataFile,line);
	
	// Parse line
	std::list<std::string> words;
	blTextUtils::ParseLine( line, ';', words );

	// Add tags
	std::list<std::string>::iterator itWord;
	for ( itWord = words.begin( ) ; itWord != words.end( ) ; itWord++ )
	{
		m_Data->AddTag( *itWord, std::string( ) );
	}

	// Read rest of values
	std::list<std::string> values;
	values.resize( m_Data->GetLength( ) );
	while ( numericDataFile.good( ) )
	{
		// Read a firt line 
		std::getline(numericDataFile,line);

		// Parse line
		std::list<std::string> words;
		blTextUtils::ParseLine( line, ';', words );

		// Add values
		std::list<std::string>::iterator itWord = words.begin( );
		std::list<std::string>::iterator itValue = values.begin( );
		while ( itWord != words.end( ) && itValue != values.end( ) )
		{
			if ( !itValue->empty( ) ) (*itValue) += ";";
			(*itValue) += *itWord;
			
			itWord++;
			itValue++;
		}
	}

	// Push values
	blTagMap::ListIterator itTag = m_Data->ListBegin( );
	std::list<std::string>::iterator itValue = values.begin( );
	while ( itTag != m_Data->ListEnd( ) && itValue != values.end( ) )
	{
		m_Data->GetTag( itTag )->SetValue( *itValue );
			
		itValue++;
		itTag++;
	}


	numericDataFile.close();
}
