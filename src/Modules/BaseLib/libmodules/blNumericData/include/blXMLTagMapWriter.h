/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef blXMLTagMapWriter_H
#define blXMLTagMapWriter_H

#include "blBaseTagMapIO.h"
#include "blMacro.h"
#include "blTagMap.h"

class TiXmlNode;

/**
XML Writer for blTagMap

Please see blXMLTagMapReader for the format of the XML file.

\author Xavi Planes
\date Oct 2010
\ingroup blNumericData
*/
class blXMLTagMapWriter : public blBaseTagMapIO 
{
public:
	typedef blXMLTagMapWriter Self;
	typedef blSmartPointer<Self> Pointer;
	blNewMacro(Self);
	cilabDeclareExceptionMacro(Exception, std::exception);
	
public:

	//!
	static void SaveData( 
		TiXmlNode* node, 
		blTagMap::Pointer tagMapInstance, 
		const std::string &nodeName );

protected:
	blXMLTagMapWriter( );

	~blXMLTagMapWriter( );

	//!
	void InternalUpdate( );
private:

};

#endif //blSignalWriter_H
