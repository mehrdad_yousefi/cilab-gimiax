/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef blNumericDataWriter_H
#define blNumericDataWriter_H

#include "blBaseTagMapIO.h"
#include "blMacro.h"
#include "blTagMap.h"

#include <vector>
#include "CILabExceptionMacros.h"

/**
blNumericData writer for all file formats

Output CSV files will have the following structure:
key1;key2;key3
val11;val12;val13
val21;val22;val23
val31;val32;val33

blTagMap:
tag1: (key1, val11, val21, val31)
tag2: (key2, val12, val22, val32)
tag2: (key3, val13, val23, val33)


\author Martin Bianculli
\date Oct 2009
\ingroup blNumbericData
*/
class blNumericDataWriter : public blBaseTagMapIO 
{
public:
	typedef blNumericDataWriter Self;
	typedef blSmartPointer<Self> Pointer;
	blNewMacro(Self);
	cilabDeclareExceptionMacro(Exception, std::exception);
	
protected:
	blNumericDataWriter( );

	~blNumericDataWriter( );

	//!
	void InternalUpdate( );

private:
};

#endif //blSignalWriter_H
