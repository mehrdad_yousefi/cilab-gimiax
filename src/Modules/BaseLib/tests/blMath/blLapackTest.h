#ifndef BLLAPACKTEST_H
#define BLLAPACKTEST_H

// Copyright 2011 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "cxxtest/TestSuite.h"
#include "blLapack.h"

/**
* \ingroup baselibtests
*/
class blLapackTest : public CxxTest::TestSuite
{
public:
   
    /**
    * Test the solveLinearSystem function.
    */
    void TestSolveLinearSystem()
    {
        // input
        const unsigned int N = getN();
        const unsigned int matDim = N*N;
        std::vector<double> va = getA(); // create the var to keep it alive
        double* A = &(va.at(0));
        const int nrhs = N;
        const int lda = N;
        // b is identity to find the inverse of A
        std::vector<double> vb = getId(); // create the var to keep it alive
        double* b = &(vb.at(0));
        int* ipiv = new int[N];
        const int ldb = N;
        int info;
        
        // solve the system
        math::lapack::solveLinearSystem(N, nrhs, A, lda, ipiv, b, ldb, info);
        
        // check result (b now contains the result)
        std::vector<double> invA = getInvA();
        for( unsigned int i=0; i < matDim; ++i)
        {
            TSM_ASSERT_DELTA("The invert is not the expected one.", b[i], invA[i], 1e-9 );
        }

        // clean up
        delete ipiv;
    }

   /**
    * Test the invertMatrix function.
    */
    void TestInvertMatrix()
    {
        // input
        const unsigned int N = getN();
        const unsigned int matDim = N*N;
        std::vector<double> va = getA(); // create the var to keep it alive
        double* A = &(va.at(0));
        int info;
        
        // solve the system
        math::lapack::invertMatrix(N, A, info);
        
        // check result (b now contains the result)
        std::vector<double> invA = getInvA();
        for( unsigned int i=0; i < matDim; ++i)
        {
            TSM_ASSERT_DELTA("The invert is not the expected one.", A[i], invA[i], 1e-9 );
        }
    }

private:

    unsigned int getN() const { return 4; }
    
    std::vector<double> getA() const
    {
        std::vector<double> matrix( getN()*getN(), 0 );
        matrix[0] =  1; matrix[1] =  2; matrix[2] =  3; matrix[3] =  1;
        matrix[4] =  5; matrix[5] =  2; matrix[6] =  7; matrix[7] =  2;
        matrix[8] =  9; matrix[9] =  4; matrix[10] = 3; matrix[11] = 3;
        matrix[12] = 0; matrix[13] = 0; matrix[14] = 0; matrix[15] = 1;
        return matrix;
    }

    std::vector<double> getInvA() const
    {
        std::vector<double> matrix( getN()*getN(), 0 );
        matrix[0] =  -0.275; matrix[1] =  0.075; matrix[2] =   0.1; matrix[3] =  -0.175;
        matrix[4] =     0.6; matrix[5] =   -0.3; matrix[6] =   0.1; matrix[7] =    -0.3;
        matrix[8] =   0.025; matrix[9] =  0.175; matrix[10] = -0.1; matrix[11] = -0.075;
        matrix[12] =      0; matrix[13] =     0; matrix[14] =    0; matrix[15] =      1;
        return matrix;
    }

    std::vector<double> getId() const
    {
        std::vector<double> matrix( getN()*getN(), 0 );
        for( unsigned int i=0; i < getN(); ++i)
        {
            matrix[ i + getN() * i ] = 1;
        }
        return matrix;
    }
    
};

#endif //BLLAPACKTEST_H

