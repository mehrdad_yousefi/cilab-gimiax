/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield (TUoS), Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/

#ifndef BLLOGGERTEST_H
#define BLLOGGERTEST_H

#include <cppunit/extensions/HelperMacros.h>

/**
* \ingroup baselibtests
*/
class blLoggerTest : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE( blLoggerTest ); 
	CPPUNIT_TEST( TestLog );
	CPPUNIT_TEST_SUITE_END();

public:
   
	void setUp();
	void tearDown();

  /**
   * Test the loading of a vtk data.
   */
   void TestLog();
	
};

#endif // BLLOGGERTEST_H

