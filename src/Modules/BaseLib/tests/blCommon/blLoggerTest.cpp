/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University of Sheffield (TUoS), Sheffield, UK. All rights reserved.
* See license.txt file for details.
*/


#include <cppunit/config/SourcePrefix.h>
#include "blLoggerTest.h"

#include <blLogger.h>


void blLoggerTest::setUp()
{
}

void blLoggerTest::tearDown()
{
}

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( blLoggerTest );

void blLoggerTest::TestLog()
{
    log4cplus::Logger logger = blLogger::getInstance("baselib");
    
    LOG4CPLUS_TRACE_METHOD(logger, LOG4CPLUS_STRING_TO_TSTRING("blLoggerTest::TestLog()"));
    LOG4CPLUS_TRACE(logger, "This is a TRACE message.");
    LOG4CPLUS_DEBUG(logger, "This is a DEBUG message.");
    LOG4CPLUS_INFO(logger, "This is a INFO message.");
    LOG4CPLUS_WARN(logger, "This is a WARN message.");
    LOG4CPLUS_ERROR(logger, "This is a ERROR message.");
    LOG4CPLUS_FATAL(logger, "This is a FATAL message.");
}
