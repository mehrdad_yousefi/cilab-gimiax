/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef BLVTKMARSHALLERTEST_H
#define BLVTKMARSHALLERTEST_H

#include "CILabNamespaceMacros.h"
#include "cxxtest/TestSuite.h"

/**
* \ingroup baselibtests
*/
class blVTKMarshallerTest : public CxxTest::TestSuite
{
public:
	void TestWithPolyData();
	void TestWithImage();
};

#endif //BLVTKMARSHALLERTEST_H
