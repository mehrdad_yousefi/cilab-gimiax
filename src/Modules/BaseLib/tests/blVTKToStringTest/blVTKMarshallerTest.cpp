/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "blVTKMarshallerTest.h"
#include "blImageUtils.h"
#include "blTestUtils.h"
#include "blVTKMarshaller.h"
#include <CISTIBToolkit.h>
#include <vtkPolyData.h>
#include <blShapeUtils.h>

namespace Private
{

}

void blVTKMarshallerTest::TestWithPolyData()
{
    // Read a poly-data
	base::TestHelper helper(
		CISTIB_TOOLKIT_FOLDER,
		CISTIB_TOOLKIT_BUILD_FOLDER,
		"BaseLib", "TestWithPolyData");
	std::string filename = helper.GetInputFolder() + "/BaseLib/blShapeUtils/aneu_cut.vtk";
	vtkPolyData* polyData = blShapeUtils::ShapeUtils::LoadShapeFromFile(filename.c_str());

	// Convert the poly-data into a package that can be sent across a dll boundary
	blVTKHelperTools::VTKMarshaller converterAtClientSide;
	blVTKHelperTools::VTKPackage package = converterAtClientSide.ConvertIntoPackageAndDelete(polyData);

	// Convert the package into a poly data polyData
	blVTKHelperTools::VTKUnMarshaller converterAtServerSide;
	vtkPolyData* receivedObject = converterAtServerSide.ConvertToPolyData(package);
	
	// Client can now discard the package (if not, package is discarded automatically when converterAtClientSide is destructed)
	converterAtClientSide.Discard(package);

	// Write the received poly data
	blShapeUtils::ShapeUtils::SaveShapeToFile(receivedObject, helper.GetOutputFilename().c_str());
}

void blVTKMarshallerTest::TestWithImage()
{
	// Causes a segfault under linux...
	// Problem with the downcasted pointer?
	
	/*
	// Read an image
	base::TestHelper helper(
		CISTIB_TOOLKIT_FOLDER,
		CISTIB_TOOLKIT_BUILD_FOLDER,
		"BaseLib", "TestWithImage");
	const std::string filename = helper.GetInputFolder() + "/BaseLib/input-CT.vtk";
	vtkSmartPointer<vtkImageData> image = blImageUtils::LoadImageFromFileAsVTK(filename.c_str());

	// Convert the image into a package that can be sent across a dll boundary
    vtkStructuredPoints* p = vtkStructuredPoints::SafeDownCast( image );
    if( !p )
    {
        TS_FAIL("Down cast did not work." );
    }
	blVTKHelperTools::VTKMarshaller converterAtClientSide;
    blVTKHelperTools::VTKPackage package = converterAtClientSide.ConvertIntoPackageAndDelete(p);

	// Convert the package into an image
	blVTKHelperTools::VTKUnMarshaller converterAtServerSide;
	vtkStructuredPoints* receivedObject = converterAtServerSide.ConvertToStructuredPoints(package);

	// Client can now discard the package (if not, package is discarded automatically when converterAtClientSide is destructed)
	converterAtClientSide.Discard(package);

	// Write the received image
	blImageUtils::SaveImageToFile(receivedObject, helper.GetOutputFilename().c_str(), NULL);
	*/
}
