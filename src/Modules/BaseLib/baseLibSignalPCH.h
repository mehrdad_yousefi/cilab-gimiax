/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _baseLibSignalPCH_H
#define _baseLibSignalPCH_H

#include "CILabExceptionMacros.h"
#include "blLightObject.h"
#include "blMacro.h"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <stdlib.h>
#include <string>
#include <vector>

#endif // _baseLibSignalPCH_H

