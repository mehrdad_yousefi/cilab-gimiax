/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "blIDSFileReader.h"
#include "blITKImageUtils.h"

#include <stdio.h>

/**
The following example illustrates 
*/
int main( int argc, char* argv[] )
{

	if( argc < 2 )
	{
		std::cerr << "Usage: " << std::endl;
		std::cerr << argv[0] << " inputFilename  outputFilename " 
			<< std::endl;
		return EXIT_FAILURE;
	}

	try
	{

		//base::UnsignedShortVolumeType::Pointer imageOutput;
		blIDSFileReader::Pointer reader = blIDSFileReader::New();
		reader->SetFileName( argv[1] );
		reader->Update();
		
		
		blITKImageUtils::SaveImageToFile<base::UnsignedShortVolumeType>( 
			reader->GetOutput(),
			argv[2]); 
	
	}
	catch (...)
	{
		std::cout << "Conversion Failed..." << std::endl;
	}

	return 0;
}




