/*=========================================================================

  Module    : Baselib
  File      : cgns2vtk.cxx
  Copyright : (C)opyright (Pompeu Fabra University) 2005++
              See COPYRIGHT statement in top level directory.
  Authors   : Jose M. Pozo
  Modified  : $Author: jpozo $
  Purpose   : 
  Date      : 2010/10/04
  Version   : 
  Changes   : $Locker:  $
              $Log: cgns2vtk.cxx,v $

=========================================================================*/

#include <string>
#include <vector>
#include <utility>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkSmartPointer.h>

#include <blCgnsFileReader.h>

using namespace std;

void Usage(void);

//----------------------------------------------------------------------------
int main(int argc, char **argv)
{
    // Check command line
    if (argc < 3) Usage();
    
    // Parse source and target images
    string input_name = argv[1];
    argc--;
    argv++;
    string output_name = argv[1];
    argc--;
    argv++;
    string variableName = argv[1];
    argc--;
    argv++;
    
    bool bAll = strcmp( variableName.c_str(), "-all" ) == 0,
        bCompress = false, bBinary = false;
    if(argc > 1 && strcmp(argv[1], "-compress")==0 ) bCompress = true;
    if(argc > 2 && strcmp(argv[2], "-binary")==0 ) bBinary = true;
    
    vector<string> inputNameVector( 1, input_name );

    blCgnsFileReader::Pointer reader = blCgnsFileReader::New();
    reader->SetFilenames(inputNameVector);
    reader->setCompress(bCompress);
    if(bAll)
    {
        reader->ReadAll();
    }
    else
    {
        reader->ReadVariables();
        vector<string> variables(1,variableName);
        reader->SetVectorsListPoint(variables);
    }

    reader->Update();

    vtkSmartPointer<vtkUnstructuredGrid> output = reader->GetOutput()[0];
    
    vtkSmartPointer<vtkUnstructuredGridWriter> writer = vtkSmartPointer<vtkUnstructuredGridWriter>::New();
    writer->SetFileName(output_name.c_str());
    writer->SetInput( output );
    if(bBinary) writer->SetFileTypeToBinary();
    writer->Update();
}

void Usage()
{
    cerr << "CGNS to VTK file converter." << endl;
    exit(1);
}
