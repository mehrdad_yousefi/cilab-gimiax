/*=========================================================================

Program:   Insight Segmentation & Registration Toolkit
Module:    $RCSfile: ApproximateCentroidalVoronoiDiagram.cxx,v $
Language:  C++
Date:      $Date: 2008/07/10 13:42:57 $
Version:   $Revision: 1.3 $

Copyright (c) Insight Software Consortium. All rights reserved.
See ITKCopyright.txt or http://www.itk.org/HTML/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include <map>
#include <queue>
#include <set>
#include <string.h>
#include <vector>

#include "vtkAppendPolyData.h"
#include "vtkCellData.h"
#include "vtkDataSetSurfaceFilter.h"
#include "vtkDoubleArray.h"
#include "vtkIdList.h"
#include "vtkIntArray.h"
#include "vtkPointData.h"
#include "vtkPoints.h"
#include "vtkPointSet.h"
#include "vtkPolyData.h"
#include "vtkPolyDataConnectivityFilter.h"
#include "vtkPolyDataNormals.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkSmartPointer.h"
#include "vtkThreshold.h"
#include "vtkUnstructuredGrid.h"
#include "vtkWarpVector.h"

void usage()
{
	std::cerr << std::endl;
	std::cerr << "Usage: " << "bl_ApproximateCentroidVoronoiDiagram.exe" << std::endl;
	std::cerr << "inputMesh" << std::endl;
	std::cerr << "outputMesh" << std::endl;
	std::cerr << "numberOfRegions" << std::endl;
	exit (1);
}

// Compute the surface area of a triangle defined by vertices v0, v1 and
// v2 (indices into m's vertex list)
double area( vtkPolyData * m,
			 unsigned int v0,
			 unsigned int v1,
			 unsigned int v2 )
{
	double pt0[3], pt1[3], pt2[3], e1[3], e2[3], a(0);
	m->GetPoint( v0, pt0 );
	m->GetPoint( v1, pt1 );
	m->GetPoint( v2, pt2 );
	for( unsigned int i = 0; i < 3; ++i )
	{
		e1[i] = pt0[i] - pt1[i];
		e2[i] = pt0[i] - pt2[i];
	}
	for( unsigned int i = 0; i < 3; ++i )
	{
		// Scalar square of cross product of e1 and e2 written out in full
		a += e1[(i+1)%3]*e1[(i+1)%3]*e2[(i+2)%3]*e2[(i+2)%3] +
			 e1[(i+2)%3]*e1[(i+2)%3]*e2[(i+1)%3]*e2[(i+1)%3] -
			 2*e1[(i+1)%3]*e1[(i+2)%3]*e2[(i+1)%3]*e2[(i+2)%3];
	}
	a = sqrt( a ) * .5;
	return a;
}


int main( int argc, char *argv[] )
{

	typedef std::map<int,int> mapType;
	typedef std::set<int> setType;
	typedef std::string stringType;
	typedef std::queue<int> queueType;
	typedef std::vector<double> listTypeD;
	typedef std::vector<int> listTypeI;
	typedef std::vector<bool> listTypeB;
	typedef std::vector< std::vector<double> > coordListType;
	typedef std::vector< std::vector<int> > listOfListsType;

	if (argc < 4)
	{
		std::cerr << std::endl << "Missing parameters " << std::endl << std::endl;
		usage();
	}

	std::cout << std::endl;
	std::cout << "bl_ApproximateCentroidalVoronoiDiagram.exe:" << std::endl
		<< "Compute an Approximate Centroidal Voronoi Diagram of the input mesh." << std::endl;
	std::cout << std::endl;

	const char* inputMeshFileName  = NULL;
	const char* outputMeshFileName = NULL;
	int numberOfRegions, numberOfFragments;

	inputMeshFileName     = argv[1];   argc--; argv++;
	outputMeshFileName    = argv[1];   argc--; argv++;
	numberOfRegions = atoi( argv[1] ); argc--; argv++;

	vtkSmartPointer<vtkAppendPolyData>             append  = vtkSmartPointer<vtkAppendPolyData            >::New();
	vtkSmartPointer<vtkDataSetSurfaceFilter>       surfer  = vtkSmartPointer<vtkDataSetSurfaceFilter      >::New();
	vtkSmartPointer<vtkIdList>                     cl      = vtkSmartPointer<vtkIdList                    >::New();
	vtkSmartPointer<vtkIdList>                     pl      = vtkSmartPointer<vtkIdList                    >::New();
	vtkSmartPointer<vtkIdList>                     cpl     = vtkSmartPointer<vtkIdList                    >::New();
	vtkSmartPointer<vtkIdList>                     cpl0    = vtkSmartPointer<vtkIdList                    >::New();
	vtkSmartPointer<vtkIdList>                     cpl1    = vtkSmartPointer<vtkIdList                    >::New();
	vtkSmartPointer<vtkIdList>                     cpl2    = vtkSmartPointer<vtkIdList                    >::New();
	vtkSmartPointer<vtkIntArray>                   rid     = vtkSmartPointer<vtkIntArray                  >::New();
	vtkSmartPointer<vtkPoints>                     outp    = vtkSmartPointer<vtkPoints                    >::New();
	vtkSmartPointer<vtkPolyData>                   finalm  = vtkSmartPointer<vtkPolyData                  >::New();
	vtkSmartPointer<vtkPolyData>                   inm     = vtkSmartPointer<vtkPolyData                  >::New();
	vtkSmartPointer<vtkPolyData>                   outm    = vtkSmartPointer<vtkPolyData                  >::New();
	vtkSmartPointer<vtkPolyData>                   tempm   = vtkSmartPointer<vtkPolyData                  >::New();
	vtkSmartPointer<vtkPolyDataConnectivityFilter> conn    = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
	vtkSmartPointer<vtkPolyDataNormals>            normal  = vtkSmartPointer<vtkPolyDataNormals           >::New();
	vtkSmartPointer<vtkPolyDataReader>             read    = vtkSmartPointer<vtkPolyDataReader            >::New();
	vtkSmartPointer<vtkPolyDataWriter>             writ    = vtkSmartPointer<vtkPolyDataWriter            >::New();
	vtkSmartPointer<vtkThreshold>                  thresh  = vtkSmartPointer<vtkThreshold                 >::New();
	vtkSmartPointer<vtkWarpVector>                 warpN   = vtkSmartPointer<vtkWarpVector                >::New();
	vtkSmartPointer<vtkWarpVector>                 warpP   = vtkSmartPointer<vtkWarpVector                >::New();

	std::string outputCentroidsFileName( outputMeshFileName );
	outputCentroidsFileName = outputCentroidsFileName.insert( outputCentroidsFileName.size()-4, "_centroids" );

	// Load mesh
	std::cout << "Loading mesh... ";
	read->SetFileName( inputMeshFileName );
	read->Update();
	inm->DeepCopy( read->GetOutput() );
	std::cout << "Done!" << std::endl;
	std::cout << std::endl;

	std::cout << "Mesh information: " << std::endl;
	std::cout << "- number of vertices     : " << inm->GetNumberOfPoints() << std::endl;
	std::cout << "- number of faces        : " << inm->GetNumberOfPolys() << std::endl;
	std::cout << "- number of vertex arrays: " << inm->GetPointData()->GetNumberOfArrays() << std::endl;
	std::cout << "- number of face arrays  : " << inm->GetCellData()->GetNumberOfArrays() << std::endl;
	std::cout << std::endl;

	numberOfFragments = inm->GetNumberOfPolys();

	listTypeD rho, srho;
	listTypeI reg, fn;
	listTypeB inq;
	coordListType gam, sgam;
	listOfListsType el, fl;
	queueType queue;

	rho.assign( inm->GetNumberOfPolys(), 0 );
	gam.resize( inm->GetNumberOfPolys() );
	reg.assign( inm->GetNumberOfPolys(), -1 );
	fn.assign(  numberOfRegions, 1 );
	srho.assign( numberOfRegions, 0 );
	sgam.resize( numberOfRegions );

	el.resize( inm->GetNumberOfPolys()*3 );
	fl.resize( inm->GetNumberOfPolys() );

	// Build the edge list
	// Compute the rho array (face area)
	// Compute the gamma array (face centroids)
	inm->BuildCells();
	inm->BuildLinks();
	pl->SetNumberOfIds( 3 );
	int ctr = 0;
	for( int i = 0; i < inm->GetNumberOfPolys(); ++i )
	{
		inm->GetCellPoints( i, pl );
		for( int j = 0; j < 3; ++j ) {
			inm->GetCellEdgeNeighbors( i, pl->GetId( j ), pl->GetId( (j+1) % 3 ), cl );
			if( cl->GetNumberOfIds() == 0 )
			{
				el[ctr].resize( 4 );
				el[ctr][0] = std::min<int>( pl->GetId( j ), pl->GetId( (j+1) % 3 ) );
				el[ctr][1] = std::max<int>( pl->GetId( j ), pl->GetId( (j+1) % 3 ) );
				el[ctr][2] = i;
				el[ctr][3] = -1;
				fl[i].push_back( ctr );
				++ctr;
			} else if( cl->GetId( 0 ) > i )
			{
				el[ctr].resize( 4 );
				el[ctr][0] = std::min<int>( pl->GetId( j ), pl->GetId( (j+1) % 3 ) );
				el[ctr][1] = std::max<int>( pl->GetId( j ), pl->GetId( (j+1) % 3 ) );
				el[ctr][2] = i;
				el[ctr][3] = cl->GetId( 0 );
				fl[i].push_back( ctr );
				fl[cl->GetId(0)].push_back( ctr );
				++ctr;
			}
		}

		double pt0[3], pt1[3], pt2[3], ptc[3], e0[3], e1[3];
		inm->GetPoint( pl->GetId( 0 ), pt0 );
		inm->GetPoint( pl->GetId( 1 ), pt1 );
		inm->GetPoint( pl->GetId( 2 ), pt2 );

		for( int j = 0; j < 3; ++j )
		{
			e0[j] = pt1[j] - pt0[j];
			e1[j] = pt2[j] - pt0[j];
			ptc[j] = (pt0[j] + pt1[j] + pt2[j]) / 3.0;
		}

		rho[i]  = e0[1]*e0[1]*e1[2]*e1[2] + e0[2]*e0[2]*e1[1]*e1[1] - 2*e0[1]*e0[2]*e1[1]*e1[2]
		        + e0[2]*e0[2]*e1[0]*e1[0] + e0[0]*e0[0]*e1[2]*e1[2] - 2*e0[0]*e0[2]*e1[0]*e1[2]
		        + e0[0]*e0[0]*e1[1]*e1[1] + e0[1]*e0[1]*e1[0]*e1[0] - 2*e0[0]*e0[1]*e1[0]*e1[1];
		rho[i]  = sqrt( rho[i] ) / 2.0;

		gam[i].resize( 3 );
		for( int j = 0; j < 3; ++j )
		{
			gam[i][j] = rho[i] * ptc[j];
		}
	}
	el.resize( ctr );
	std::cout <<std::endl << "Number of edges: " << ctr << std::endl << std::endl;

	inq.resize( ctr );
	for( unsigned int i = 0; i < inq.size(); ++i )
	{
		inq[i] = false;
	}

	int numberOfEdges = ctr;
	int modnum = pow( (double)10.0, (int)floor( log10( (double)numberOfEdges ) ) - 2 );

	for( int i = 0; i < numberOfRegions; ++i )
	{
		sgam[i].resize(3);
		reg[i*(inm->GetNumberOfPolys()/numberOfRegions)] = i;
		srho[i] = rho[i*(inm->GetNumberOfPolys()/numberOfRegions)];
		for( int j = 0; j < 3; ++j )
		{
			sgam[i][j] = gam[i*(inm->GetNumberOfPolys()/numberOfRegions)][j];
		}
		for( unsigned int j = 0; j < 3; ++j )
		{
			if( !inq[fl[i*(inm->GetNumberOfPolys()/numberOfRegions)][j]] )
			{
				queue.push(fl[i*(inm->GetNumberOfPolys()/numberOfRegions)][j]);
				inq[fl[i*(inm->GetNumberOfPolys()/numberOfRegions)][j]] = true;
			}
		}
	}

	bool notConverged(true);
	unsigned int numberOfFaceAssignments(1), n(1), sinceLastFlip(1);
	int edgeind, edge[4], numUnassigned(inm->GetNumberOfPolys()-numberOfRegions);
	double sumLeftBottomK, sumLeftBottomS0, sumLeftBottomS1;
	double sumLeftTopK[3] = {0,0,0}, sumLeftTopS0[3] = {0,0,0}, sumLeftTopS1[3] = {0,0,0};
	double sumRightBottomK, sumRightBottomS0, sumRightBottomS1;
	double sumRightTopK[3] = {0,0,0}, sumRightTopS0[3] = {0,0,0}, sumRightTopS1[3] = {0,0,0};
	double l2Keep, l2Swap0, l2Swap1;
	while( notConverged )
	{
		edgeind = queue.front();
		queue.pop();
		inq[edgeind] = false;
		for( int i = 0; i < 4; ++i )
		{
			edge[i] = el[edgeind][i];
		}
		if( edge[3] != -1 ) // A test to avoid edges with only one face adjoining.
		{
			if( reg[edge[2]] == -1 && reg[edge[3]] == -1 )
			{
			} else if( reg[edge[2]] == -1 )
				// This edge has only the second face assigned a label. This is copied to
				// the first face.
			{
				++numberOfFaceAssignments;
				--numUnassigned;
				if( numUnassigned == 0 )
				{
					std::cout << "No more unassigned faces" << std::endl << std::endl;
				}
				srho[reg[edge[3]]] += rho[edge[2]];
				for( int j = 0; j < 3; ++j )
				{
					sgam[reg[edge[3]]][j] += gam[edge[2]][j];
				}
				fn[reg[edge[3]]] = fn[reg[edge[3]]] + 1;
				for( unsigned int i = 0; i < fl[edge[2]].size(); ++i )
				{
					if( fl[edge[2]][i] != edgeind )
					{
						if( !inq[fl[edge[2]][i]] )
						{
							queue.push( fl[edge[2]][i] );
							inq[fl[edge[2]][i]] = true;
						}
					}
				}
				reg[edge[2]] = reg[edge[3]];
				sinceLastFlip = 0;
			} else if( reg[edge[3]] == -1 )
				// This edge has only the first face assigned a label. This is copied to
				// the second face.
			{
				++numberOfFaceAssignments;
				--numUnassigned;
				if( numUnassigned == 0 )
				{
					std::cout << "No more unassigned faces" << std::endl << std::endl;
				}
				srho[reg[edge[2]]] += rho[edge[3]];
				for( int j = 0; j < 3; ++j )
				{
					sgam[reg[edge[2]]][j] += gam[edge[3]][j];
				}
				fn[reg[edge[2]]] = fn[reg[edge[2]]] + 1;
				for( unsigned int i = 0; i < fl[edge[3]].size(); ++i )
				{
					if( fl[edge[3]][i] != edgeind )
					{
						if( !inq[fl[edge[3]][i]] )
						{
							queue.push( fl[edge[3]][i] );
							inq[fl[edge[3]][i]] = true;
						}
					}
				}
				reg[edge[3]] = reg[edge[2]];
				sinceLastFlip = 0;
			} else if( reg[edge[2]] != reg[edge[3]] )
				// Both faces have different labels.
			{
				int f0 = fn[reg[edge[2]]];
				int f1 = fn[reg[edge[3]]];

				if( f0 == 1 && f1 == 1 )
					// Both faces' labels are unique; no swap is considered, and the l2
					// values are set to ensure this
				{
					l2Keep = -.1;
					l2Swap0 = 0.;
					l2Swap1 = 0.;
					if( !inq[edgeind] )
					{
						queue.push( edgeind );
						inq[edgeind] = true;
					}
					++sinceLastFlip;
				} else if( f0 == 1 )
					// The label of the first of the faces is only assigned to this face.
					// Only one type of swap is considered: to assign the label of the
					// first face to the second face (l2Swap0 = 0).
				{
					for( int j = 0; j < 3; ++j )
					{
						sumLeftTopK[j] = sgam[reg[edge[2]]][j];
						sumRightTopK[j] = sgam[reg[edge[3]]][j];
						sumLeftTopS1[j] = sumLeftTopK[j] + gam[edge[3]][j];
						sumRightTopS1[j] = sumRightTopK[j] - gam[edge[3]][j];
					}
					sumLeftBottomK = srho[reg[edge[2]]];
					sumRightBottomK = srho[reg[edge[3]]];
					sumLeftBottomS1 = sumLeftBottomK + rho[edge[3]];
					sumRightBottomS1 = sumRightBottomK - rho[edge[3]];

					// Compute (10) for either case
					l2Keep = -1 * (sumLeftTopK[0]*sumLeftTopK[0] + sumLeftTopK[1]*sumLeftTopK[1] + sumLeftTopK[2]*sumLeftTopK[2])/sumLeftBottomK - (sumRightTopK[0]*sumRightTopK[0] + sumRightTopK[1]*sumRightTopK[1] + sumRightTopK[2]*sumRightTopK[2])/sumRightBottomK;
					l2Swap0 = 0;
					l2Swap1 = -1 * (sumLeftTopS1[0]*sumLeftTopS1[0] + sumLeftTopS1[1]*sumLeftTopS1[1] + sumLeftTopS1[2]*sumLeftTopS1[2])/sumLeftBottomS1 - (sumRightTopS1[0]*sumRightTopS1[0] + sumRightTopS1[1]*sumRightTopS1[1] + sumRightTopS1[2]*sumRightTopS1[2])/sumRightBottomS1;
				} else if( f1 == 1 )
					// The label of the second of the faces is only assigned to this face.
					// Only one type of swap is considered: to assign the label of the
					// second face to the first face (l2Swap1 = 0).
				{
					for( int j = 0; j < 3; ++j )
					{
						sumLeftTopK[j] = sgam[reg[edge[2]]][j];
						sumRightTopK[j] = sgam[reg[edge[3]]][j];
						sumLeftTopS0[j] = sumLeftTopK[j] - gam[edge[2]][j];
						sumRightTopS0[j] = sumRightTopK[j] + gam[edge[2]][j];
					}
					sumLeftBottomK = srho[reg[edge[2]]];
					sumRightBottomK = srho[reg[edge[3]]];
					sumLeftBottomS0 = sumLeftBottomK - rho[edge[2]];
					sumRightBottomS0 = sumRightBottomK + rho[edge[2]];

					// Compute (10) for either case
					l2Keep = -1 * (sumLeftTopK[0]*sumLeftTopK[0] + sumLeftTopK[1]*sumLeftTopK[1] + sumLeftTopK[2]*sumLeftTopK[2])/sumLeftBottomK - (sumRightTopK[0]*sumRightTopK[0] + sumRightTopK[1]*sumRightTopK[1] + sumRightTopK[2]*sumRightTopK[2])/sumRightBottomK;
					l2Swap0 = -1 * (sumLeftTopS0[0]*sumLeftTopS0[0] + sumLeftTopS0[1]*sumLeftTopS0[1] + sumLeftTopS0[2]*sumLeftTopS0[2])/sumLeftBottomS0 - (sumRightTopS0[0]*sumRightTopS0[0] + sumRightTopS0[1]*sumRightTopS0[1] + sumRightTopS0[2]*sumRightTopS0[2])/sumRightBottomS0;
					l2Swap1 = 0;
				} else
					// Both types of swap are possible, so both are considered.
				{
					for( int j = 0; j < 3; ++j )
					{
						sumLeftTopK[j] = sgam[reg[edge[2]]][j];
						sumRightTopK[j] = sgam[reg[edge[3]]][j];
						sumLeftTopS0[j] = sumLeftTopK[j] - gam[edge[2]][j];
						sumRightTopS0[j] = sumRightTopK[j] + gam[edge[2]][j];
						sumLeftTopS1[j] = sumLeftTopK[j] + gam[edge[3]][j];
						sumRightTopS1[j] = sumRightTopK[j] - gam[edge[3]][j];
					}

					sumLeftBottomK = srho[reg[edge[2]]];
					sumRightBottomK = srho[reg[edge[3]]];
					sumLeftBottomS0 = sumLeftBottomK - rho[edge[2]];
					sumRightBottomS0 = sumRightBottomK + rho[edge[2]];
					sumLeftBottomS1 = sumLeftBottomK + rho[edge[3]];
					sumRightBottomS1 = sumRightBottomK - rho[edge[3]];

					// Compute (10) for each case
					l2Keep = -1 * (sumLeftTopK[0]*sumLeftTopK[0] + sumLeftTopK[1]*sumLeftTopK[1] + sumLeftTopK[2]*sumLeftTopK[2])/sumLeftBottomK - (sumRightTopK[0]*sumRightTopK[0] + sumRightTopK[1]*sumRightTopK[1] + sumRightTopK[2]*sumRightTopK[2])/sumRightBottomK;
					l2Swap0 = -1 * (sumLeftTopS0[0]*sumLeftTopS0[0] + sumLeftTopS0[1]*sumLeftTopS0[1] + sumLeftTopS0[2]*sumLeftTopS0[2])/sumLeftBottomS0 - (sumRightTopS0[0]*sumRightTopS0[0] + sumRightTopS0[1]*sumRightTopS0[1] + sumRightTopS0[2]*sumRightTopS0[2])/sumRightBottomS0;
					l2Swap1 = -1 * (sumLeftTopS1[0]*sumLeftTopS1[0] + sumLeftTopS1[1]*sumLeftTopS1[1] + sumLeftTopS1[2]*sumLeftTopS1[2])/sumLeftBottomS1 - (sumRightTopS1[0]*sumRightTopS1[0] + sumRightTopS1[1]*sumRightTopS1[1] + sumRightTopS1[2]*sumRightTopS1[2])/sumRightBottomS1;
				}
				// Apply the swap if necessary
				if( l2Swap0 < l2Keep && l2Swap0 < l2Swap1 )
				{
					++numberOfFaceAssignments;
					for( int j = 0; j < 3; ++j )
					{
						sgam[reg[edge[2]]][j] = sgam[reg[edge[2]]][j] - gam[edge[2]][j];
						sgam[reg[edge[3]]][j] = sgam[reg[edge[3]]][j] + gam[edge[2]][j];
					}
					srho[reg[edge[2]]] = srho[reg[edge[2]]] - rho[edge[2]];
					srho[reg[edge[3]]] = srho[reg[edge[3]]] + rho[edge[2]];

					fn[reg[edge[2]]] = fn[reg[edge[2]]] - 1;
					fn[reg[edge[3]]] = fn[reg[edge[3]]] + 1;
					for( unsigned int i = 0; i < fl[edge[2]].size(); ++i )
					{
						if( fl[edge[2]][i] != edgeind )
						{
							if( !inq[fl[edge[2]][i]] )
							{
								queue.push( fl[edge[2]][i] );
								inq[fl[edge[2]][i]] = true;
							}
						}
					}
					reg[edge[2]] = reg[edge[3]];
					sinceLastFlip = 0;
				} else if( l2Swap1 < l2Keep && l2Swap1 < l2Swap0 )
				{
					++numberOfFaceAssignments;
					for( int j = 0; j < 3; ++j )
					{
						sgam[reg[edge[2]]][j] = sgam[reg[edge[2]]][j] + gam[edge[3]][j];
						sgam[reg[edge[3]]][j] = sgam[reg[edge[3]]][j] - gam[edge[3]][j];
					}
					srho[reg[edge[2]]] = srho[reg[edge[2]]] + rho[edge[3]];
					srho[reg[edge[3]]] = srho[reg[edge[3]]] - rho[edge[3]];

					fn[reg[edge[2]]] = fn[reg[edge[2]]] + 1;
					fn[reg[edge[3]]] = fn[reg[edge[3]]] - 1;
					for( unsigned int i = 0; i < fl[edge[3]].size(); ++i )
					{
						if( fl[edge[3]][i] != edgeind )
						{
							if( !inq[fl[edge[3]][i]] )
							{
								queue.push( fl[edge[3]][i] );
								inq[fl[edge[3]][i]] = true;
							}
						}
					}
					reg[edge[3]] = reg[edge[2]];
					sinceLastFlip = 0;
				} else
				{
					if( !inq[edgeind] )
					{
						queue.push( edgeind );
						inq[edgeind] = true;
					}
					++sinceLastFlip;
				}
				if( sinceLastFlip > queue.size() )
					// Check whether all regions are connected; if not, the largest
					// component of each should be kept and the rest re-assigned to -1.
				{
					std::cout << "Cleaning up the queue...";
					for( unsigned int j = 0; j < sinceLastFlip-1; ++j )
					{
						edgeind = queue.front();
						queue.pop();
						inq[edgeind] = false;
						if( el[edgeind][3] != -1 )
						{
							if( reg[el[edgeind][2]] != reg[el[edgeind][3]] )
							{
								if( !inq[edgeind] )
								{
									queue.push(edgeind);
									inq[edgeind] = true;
								}
							}
						}
					}
					std::cout << " done!" << std::endl;
					listTypeI conn;
					conn.assign( reg.size(), -1 );
					int numberOfConnectedComponents(0);
					for( unsigned int j = 0; j < el.size(); ++j )
					{
						if( el[j][3] != -1 )
						{
							if( conn[el[j][2]] == -1 && conn[el[j][3]] != -1 )
							{
								if( reg[el[j][3]] == reg[el[j][2]] )
								{
									conn[el[j][2]] = conn[el[j][3]];
								}
								else
								{
									conn[el[j][2]] = numberOfConnectedComponents;
									++numberOfConnectedComponents;
								}
							}
							else if( conn[el[j][2]] != -1 && conn[el[j][3]] == -1 )
							{
								if( reg[el[j][3]] == reg[el[j][2]] )
								{
									conn[el[j][3]] = conn[el[j][2]];
								}
								else
								{
									conn[el[j][3]] = numberOfConnectedComponents;
									++numberOfConnectedComponents;
								}
							} else if( conn[el[j][2]] == -1 && conn[el[j][3]] == -1 )
							{
								if( reg[el[j][3]] == reg[el[j][2]] )
								{
									conn[el[j][2]] = numberOfConnectedComponents;
									conn[el[j][3]] = numberOfConnectedComponents;
									++numberOfConnectedComponents;
								}
								else
								{
									conn[el[j][2]] = numberOfConnectedComponents;
									++numberOfConnectedComponents;
									conn[el[j][3]] = numberOfConnectedComponents;
									++numberOfConnectedComponents;
								}
							}
						}
						else
						{
							if( conn[el[j][2]] == -1 )
							{
								conn[el[j][2]] = numberOfConnectedComponents;
								++numberOfConnectedComponents;
							}
						}
					}
					// Neighboring faces with the same label are assigned the same
					// connected region label; how many such labels exist is recorded
					bool hasChanged = true;
					while( hasChanged )
					{
						hasChanged = false;
						for( unsigned int j = 0; j < el.size(); ++j )
						{
							if( el[j][3] != -1 )
							{
								if( reg[el[j][2]] == reg[el[j][3]] && conn[el[j][2]] != conn[el[j][3]] )
								{
									int mn = std::min<int>( conn[el[j][2]], conn[el[j][3]] );
									conn[el[j][2]] = mn;
									conn[el[j][3]] = mn;
									hasChanged = true;
								}
							}
						}
					}
					setType connectedComponentLabels;
					for( unsigned j = 0; j < conn.size(); ++j )
					{
						connectedComponentLabels.insert( conn[j] );
					}
					numberOfConnectedComponents = connectedComponentLabels.size();
					if( numberOfConnectedComponents == numberOfRegions )
						// If we have as many connected region labels as region labels, we're
						// done
					{
						notConverged = false;
						std::cout << "Connectivity check passed: job completed" << std::endl;
						std::cout << "Face reassignments: " << numberOfFaceAssignments << std::endl;
						std::cout << "Queue length: " << queue.size() << std::endl << std::endl;
					} else
						// We have disconnected regions; we find them and unassign the
						// smaller pieces
					{
						if( numberOfConnectedComponents - numberOfRegions < 0 )
						{
							rid->SetNumberOfComponents( 1 );
							rid->SetNumberOfTuples( inm->GetNumberOfPolys() );
							rid->SetName( "VoronoiRegionID" );
							for( unsigned int i = 0; i < reg.size(); ++i )
							{
								rid->SetValue( i, reg[i] );
							}
							inm->GetCellData()->AddArray( rid );

							// Save new mesh with region IDs
							std::cout << "Saving weird situation... ";
							writ->SetInput( inm );
							writ->SetFileName( outputMeshFileName );
							writ->Update();
							std::cout << "Done!" << std::endl << std::endl;
							exit( 1 );
						}
						if( numberOfConnectedComponents - numberOfRegions < numberOfFragments )
						{
							numberOfFragments = numberOfConnectedComponents - numberOfRegions;
							std::cout << std::endl;
							std::cout << "Connectivity check failed: removing " << numberOfConnectedComponents - numberOfRegions << " fragments" << std::endl;
							std::cout << "Face reassignments: " << numberOfFaceAssignments << std::endl;
							std::cout << "Queue length: " << queue.size() << std::endl << std::endl;
							std::cout << std::endl;
							sinceLastFlip = 0;
							mapType connectedComponentToRegionIdMap;
							listOfListsType regionIdToConnectedComponentsMap;
							regionIdToConnectedComponentsMap.resize( numberOfRegions );

							// For each connected region label, we record which label it
							// belongs to; the map data structure should take care of the
							// uniqueness of the key/value pairs
							for( unsigned int j = 0; j < conn.size(); ++j )
							{
								connectedComponentToRegionIdMap[conn[j]] = reg[j];
							}

							// Now we reverse the mapping; we record for each label which
							// connected region labels are associated with it
							for( mapType::iterator it = connectedComponentToRegionIdMap.begin(); it != connectedComponentToRegionIdMap.end(); ++it )
							{
								regionIdToConnectedComponentsMap[(*it).second].push_back( (*it).first );
							}

							for( unsigned int j = 0; j < regionIdToConnectedComponentsMap.size(); ++j )
							{
								if( regionIdToConnectedComponentsMap[j].size() > 1 )
									// If the region is not connected, we compute the sizes
									// of each connected region and compare on-the-fly
								{
									double maxSize(0.0);
									int maxInd;
									for( unsigned int k = 0; k < regionIdToConnectedComponentsMap[j].size(); ++k )
									{
										double regionSize(0.0);
										for( unsigned int l = 0; l < conn.size(); ++l )
										{
											if( conn[l] == regionIdToConnectedComponentsMap[j][k] )
											{
												regionSize += rho[l];
											}
										}
										if( regionSize > maxSize )
										{
											maxSize = regionSize;
											maxInd = k;
										}
									}
									for( unsigned int k = 0; k < reg.size(); ++k )
										// All of the sub-regions that are not connected are let go of.
									{
										if( reg[k] == j && conn[k] != regionIdToConnectedComponentsMap[j][maxInd] )
										{
											for( unsigned int l = 0; l < 3; ++l )
											{
												sgam[j][l] -= gam[k][l];
											}
											srho[j] -= rho[k];
											fn[j] -= 1;
											reg[k] = -1;
											++numUnassigned;
										}
									}
								}
							}
						}
						else
						{
							notConverged = false;
							std::cout << "Connectivity check failed; fragmentation cannot be fixed" << std::endl;
							std::cout << "Face reassignments: " << numberOfFaceAssignments << std::endl;
							std::cout << "Queue length: " << queue.size() << std::endl << std::endl;

						}
					}
				}
			}
		}
	}

	// Put the region IDs into the mesh as cell data
	rid->SetNumberOfComponents( 1 );
	rid->SetNumberOfTuples( inm->GetNumberOfPolys() );
	rid->SetName( "VoronoiRegionID" );
	for( unsigned int i = 0; i < reg.size(); ++i )
	{
		rid->SetValue( i, reg[i] );
	}
	unsigned int arrayId = inm->GetCellData()->AddArray( rid );

	// Save new mesh with region IDs
	std::cout << "Saving mesh with CVD... ";
	writ->SetInput( inm );
	writ->SetFileName( outputMeshFileName );
	writ->Update();
	std::cout << "Done!" << std::endl << std::endl;

	// Compute centroid locations
	outp->SetNumberOfPoints( numberOfRegions );
	outp->SetDataTypeToDouble();
	for( unsigned int i = 0; i < sgam.size(); ++i )
	{
		outp->InsertNextPoint( sgam[i][0]/srho[i], sgam[i][1]/srho[i], sgam[i][2]/srho[i] );
	}
	outm->SetPoints( outp );

	// Find vertices in the original mesh that have three (or four) different adjoining
	// region IDs, and construct a face (or two) from the associated centroids
	outm->Allocate();
	setType neighboringRegions;
	for( int i = 0; i < inm->GetNumberOfPoints(); ++i )
	{
		neighboringRegions.clear();
		inm->GetPointCells( i, cl );
		for( int j = 0; j < cl->GetNumberOfIds(); ++j )
		{
			neighboringRegions.insert( reg[cl->GetId( j )] );
		}
		if( neighboringRegions.size() == 3 )
		{
			cpl->Reset();
			for( setType::iterator it = neighboringRegions.begin(); it != neighboringRegions.end(); ++it )
			{
				cpl->InsertNextId( *it );
			}
			outm->InsertNextCell( VTK_TRIANGLE, cpl );
		}
		else if( neighboringRegions.size() > 3 )
		{
			int s = neighboringRegions.size();
			if( s > 5 ) {
				++i; --i;
			}
			listOfListsType adjacencyMatrix;
			mapType vertexLabels, vertexLabelsReverse;
			adjacencyMatrix.resize( neighboringRegions.size() );
			//vertexLabels.resize( neighboringRegions.size() );
			for( unsigned int j = 0; j < neighboringRegions.size(); ++j )
			{
				adjacencyMatrix[j].assign( neighboringRegions.size(), 0 );
			}

			unsigned int index(0);
			for( setType::iterator it = neighboringRegions.begin(); it != neighboringRegions.end(); ++it )
			{
				vertexLabels[*it] = index;
				vertexLabelsReverse[index] = *it;
				++index;
			}

			for( int j = 0; j < cl->GetNumberOfIds(); ++j )
			{
				inm->GetCellPoints( cl->GetId(j), pl );
				for( unsigned int k = 0; k < 3; ++k )
				{
					if( pl->GetId(k) == i || pl->GetId((k+1)%3) == i )
					{
						inm->GetCellEdgeNeighbors( cl->GetId(j), pl->GetId( k ), pl->GetId( (k+1) % 3 ), cpl );
						if( cpl->GetNumberOfIds() > 0 )
						{
							if( reg[cpl->GetId(0)] != reg[cl->GetId(j)] )
							{
								adjacencyMatrix[vertexLabels[reg[cpl->GetId(0)]]][vertexLabels[reg[cl->GetId(j)]]] = 1;
								//adjacencyMatrix[vertexLabels[reg[cl->GetId(0)]]][vertexLabels[reg[cpl->GetId(j)]]] = 1;
							}
						}
					}
				}
			}

			if( neighboringRegions.size() == 4 )
			{
				cpl0->Reset();
				cpl1->Reset();
				// Analyze the adjacency matrix:
				// 1. Find a pair that is not connected
				// 2. Place this edge in either of the faces to be constructed
				// 3. Add a remaining vertex to each of the faces
				for( unsigned int j = 1; j < adjacencyMatrix.size(); ++j )
				{
					if( adjacencyMatrix[0][j] == 0 )
					{
						cpl0->InsertNextId( vertexLabelsReverse[0] );
						cpl0->InsertNextId( vertexLabelsReverse[j] );
						cpl1->InsertNextId( vertexLabelsReverse[0] );
						cpl1->InsertNextId( vertexLabelsReverse[j] );
						switch( j ) {
					case 1:
						cpl0->InsertNextId( vertexLabelsReverse[2] );
						cpl1->InsertNextId( vertexLabelsReverse[3] );
						break;
					case 2:
						cpl0->InsertNextId( vertexLabelsReverse[1] );
						cpl1->InsertNextId( vertexLabelsReverse[3] );
						break;
					case 3:
						cpl0->InsertNextId( vertexLabelsReverse[1] );
						cpl1->InsertNextId( vertexLabelsReverse[2] );
						break;
						}
						break;
					}
				}
				outm->InsertNextCell( VTK_TRIANGLE, cpl0 );
				outm->InsertNextCell( VTK_TRIANGLE, cpl1 );
			}
			else if( neighboringRegions.size() == 5 )
			{
				// Analyze the adjacency matrix:
				// 1. Grab a vertex. Insert in the first face
				// 2. Find its neighbors. Insert in both the first and second face
				// 3. Add one of the neighbors to the third face
				// 4. Find a non-neighbor of this face and add it to the second and third face
				// 5. Find the remaining vertex and add it to the third face
				cpl0->Reset();
				cpl1->Reset();
				cpl2->Reset();

				cpl0->InsertNextId( vertexLabelsReverse[0] );
				unsigned int neighbor0(0), neighbor1(0);
				bool hasFoundOneNeighbor(false), hasFoundOneNonNeighbor(false);
				for( unsigned int j = 1; j < adjacencyMatrix.size(); ++j )
				{
					if( adjacencyMatrix[0][j] == 1 )
					{
						cpl0->InsertNextId( vertexLabelsReverse[j] );
						cpl1->InsertNextId( vertexLabelsReverse[j] );
						if( !hasFoundOneNeighbor )
						{
							neighbor0 = j;
							cpl2->InsertNextId( vertexLabelsReverse[j] );
							hasFoundOneNeighbor = true;
						}
						else
						{
							neighbor1 = j;
						}
					}
				}
				for( unsigned int j = 1; j < adjacencyMatrix.size(); ++j )
				{
					if( adjacencyMatrix[neighbor0][j] == 0 && j != neighbor0 && j != neighbor1 )
					{
						cpl1->InsertNextId( vertexLabelsReverse[j] );
						cpl2->InsertNextId( vertexLabelsReverse[j] );
					}
					else if( adjacencyMatrix[neighbor0][j] == 1 )
					{
						cpl2->InsertNextId( vertexLabelsReverse[j] );
					}
				}
				outm->InsertNextCell( VTK_TRIANGLE, cpl0 );
				outm->InsertNextCell( VTK_TRIANGLE, cpl1 );
				outm->InsertNextCell( VTK_TRIANGLE, cpl2 );
			}
		}
	}

	// Transfer the partID vector from the original mesh to the coarsened mesh
	// by area majority
	vtkSmartPointer<vtkIntArray> pid      = vtkSmartPointer<vtkIntArray>::New();
	vtkSmartPointer<vtkDoubleArray> parea = vtkSmartPointer<vtkDoubleArray>::New();
	double * range = inm->GetCellData()->GetArray("part")->GetRange();
	parea->SetNumberOfComponents( (int)(range[1]+1) );
	parea->SetNumberOfTuples( outm->GetNumberOfPoints() );
	pid->SetNumberOfComponents(1);
	pid->SetNumberOfTuples( outm->GetNumberOfPoints() );
	pid->SetName( "part" );

	for( unsigned int i = 0; i <= range[1]; ++i )
	{
		parea->FillComponent( i, 0.0 );
	}

	for( unsigned int i = 0; i < inm->GetNumberOfCells(); ++i )
	{
		unsigned int partid = inm->GetCellData()->GetArray("part")->GetComponent( i, 0 );
		unsigned int pointid = rid->GetComponent( i, 0 );
		inm->GetCellPoints( i, pl );
		double facearea = area( inm, pl->GetId(0), pl->GetId(1), pl->GetId(2) );
		parea->SetComponent( pointid, partid, parea->GetComponent( pointid, partid ) + facearea );
	}

	for( unsigned int i = 0; i < outm->GetNumberOfPoints(); ++i )
	{
		double maxarea = 0;
		int maxareaind = -1;
		for( unsigned int j = 0; j <= range[1]; ++j )
		{
			if( parea->GetComponent( i, j ) > maxarea )
			{
				maxarea = parea->GetComponent( i, j );
				maxareaind = j;
			}
		}
		pid->SetComponent( i, 0, maxareaind );
	}
	outm->GetPointData()->AddArray( pid );

	//double negBounds[6], posBounds[6], negBBVolume, posBBVolume;

	//conn->SetInput( outm );
	//conn->ColorRegionsOn();
	//conn->SetExtractionModeToAllRegions();

	//conn->Update();
	//conn->GetOutput()->GetPointData()->SetActiveScalars( "RegionId" );
	//for( int i = 0; i < conn->GetNumberOfExtractedRegions(); ++i )
	//{
	//	thresh->SetInputConnection( conn->GetOutputPort() );
	//	thresh->SetInputArrayToProcess( 0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, "RegionId" );
	//	thresh->ThresholdBetween( i, i );

	//	surfer->SetInputConnection( thresh->GetOutputPort() );
	//	surfer->Update();

	//	tempm->DeepCopy( surfer->GetOutput() );

	//	normal->SetInput( tempm );
	//	normal->ComputePointNormalsOn();
	//	normal->ConsistencyOn();
	//	normal->FlipNormalsOff();
	//	normal->NonManifoldTraversalOff();
	//	normal->SplittingOff();

	//	normal->GetOutput()->GetPointData()->SetActiveVectors( "Normals" );

	//	warpN->SetInputConnection( normal->GetOutputPort() );
	//	warpN->SetInputArrayToProcess( 0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, "Normals" );
	//	warpN->SetScaleFactor( -1 );

	//	warpP->SetInputConnection( normal->GetOutputPort() );
	//	warpP->SetInputArrayToProcess( 0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, "Normals" );
	//	warpP->SetScaleFactor( 1 );

	//	warpN->Update();
	//	warpN->GetOutput()->GetBounds( negBounds );
	//	negBBVolume = (negBounds[1]-negBounds[0]) * (negBounds[3]-negBounds[2]) * (negBounds[5]-negBounds[4]);

	//	warpP->Update();
	//	warpP->GetOutput()->GetBounds( posBounds );
	//	posBBVolume = (posBounds[1]-posBounds[0]) * (posBounds[3]-posBounds[2]) * (posBounds[5]-posBounds[4]);

	//	if( negBBVolume > posBBVolume )
	//	{
	//		normal->FlipNormalsOn();
	//		normal->Update();
	//	}

	//	if( i == 0 )
	//	{
	//		finalm->DeepCopy( normal->GetOutput() );
	//	}
	//	else
	//	{
	//		append->AddInput( finalm );
	//		append->AddInput( normal->GetOutput() );
	//		append->Update();
	//		finalm->DeepCopy( append->GetOutput() );
	//	}
	//	std::cout << std::endl;
	//}

	// Remove unnecessary baggage (cell and vertex labels)
	//unsigned int nPdArrays = finalm->GetPointData()->GetNumberOfArrays();
	//for( unsigned int i = 0; i < nPdArrays; ++i )
	//	finalm->GetPointData()->RemoveArray( finalm->GetPointData()->GetArrayName( 0 ) );

	//unsigned int nCdArrays = finalm->GetCellData()->GetNumberOfArrays();
	//for( unsigned int i = 0; i < nCdArrays; ++i )
	//	finalm->GetCellData()->RemoveArray( finalm->GetCellData()->GetArrayName( 0 ) );

	std::cout << "Saving centroids... ";
	writ->SetInput( outm );
	writ->SetFileName( outputCentroidsFileName.c_str() );
	writ->Update();
	std::cout << "Done!" << std::endl << std::endl;

	return EXIT_SUCCESS;

}
