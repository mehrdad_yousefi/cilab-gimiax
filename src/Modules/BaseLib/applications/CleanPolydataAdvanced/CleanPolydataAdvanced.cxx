#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#ifdef __BORLANDC__
#define ITK_LEAN_AND_MEAN
#endif

#include "vtkPolyData.h"
#include "vtkCleanPolyData.h"
#include "blShapeUtils.h"
// here go your #includes

#include "itkPluginUtilities.h"

#include "CleanPolydataAdvancedCLP.h"

// Use an anonymous namespace to keep class types and function names
// from colliding when module is used as shared object module.  Every
// thing should be in an anonymous namespace except for the module
// entry point, e.g. main()
//
namespace {
} // end of anonymous namespace


int main( int argc, char * argv[] )
{
  PARSE_ARGS;

  vtkSmartPointer< vtkPolyData > poly = vtkSmartPointer< vtkPolyData >::Take( 
	  blShapeUtils::ShapeUtils::LoadShapeFromFile(inputGeometry.c_str()) );

  vtkSmartPointer<vtkCleanPolyData> clean = vtkSmartPointer<vtkCleanPolyData>::New();
  clean->SetInput(poly);
  clean->Update();

  vtkSmartPointer< vtkPolyData > polyOut = vtkSmartPointer< vtkPolyData > ::New();
  
  if (removeLinesAndExtraVertices)
  {
	  polyOut->SetPoints(clean->GetOutput()->GetPoints());
	  polyOut->SetPolys(clean->GetOutput()->GetPolys());
  }
  else
	  polyOut->DeepCopy(clean->GetOutput());

  blShapeUtils::ShapeUtils::SaveShapeToFile(polyOut,outputGeometry.c_str());

  return EXIT_SUCCESS;
}
