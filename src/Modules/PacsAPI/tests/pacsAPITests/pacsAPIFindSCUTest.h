// Copyright 2011 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#ifndef PACSAPIFINDSCUTEST_H
#define PACSAPIFINDSCUTEST_H

#include <cxxtest/TestSuite.h>

/**
* Tests fo the PacsAPI FindSCU methods.
* Equivalent to a dcmtk/dcmnet findscu:
*  findscu -aet PacsTest -aec SERVERAE --patient -v 10.80.4.213 104 query.dcm 
* With query.dcm created using dcmtk/dcmdata: dump2dcm query.txt query.dcm
* and query.txt:
# query patient names and IDs
(0008,0052) CS [PATIENT]     # QueryRetrieveLevel
(0010,0010) PN []            # PatientName
(0010,0020) LO []            # PatientID
*/
class PacsAPIFindSCUTest : public CxxTest::TestSuite
{
public:

    //! Queries a PACS server and checks if the retrieved patient names is the one given.
    void TestQuery();

}; // class PacsAPIFindSCUTest

#endif // PACSAPIFINDSCUTEST_H

