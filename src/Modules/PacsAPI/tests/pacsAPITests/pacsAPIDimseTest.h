// Copyright 2011 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#ifndef PACSAPIDIMSETEST_H
#define PACSAPIDIMSETEST_H

// cxxtest
#include <cxxtest/TestSuite.h>

/**
* Tests fo the PacsAPI Dimse methods.
* Equivalent to a dcmtk echoscu:
*  echoscu 10.80.4.213 104 -aet PacsTest -aec SERVERAE -v
*/
class PacsAPIDimseTest : public CxxTest::TestSuite
{
public:

    //! Tests the association to a given PACS server.
    void TestAssociation();

}; // class PacsAPIDimseTest

#endif // PACSAPIDIMSETEST_H

