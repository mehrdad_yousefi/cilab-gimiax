// Copyright 2011 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#ifndef PACSCONFIG_H
#define PACSCONFIG_H

#include <string>

namespace pacs
{
namespace test
{

/**
* Holder for PACS test server details.
* Configuration of the test PACS server.
* To access it, use http://pacs.gimias.org:81/ImageServer/
* (user: 'admin', pass: 'gimias2012')
*/
class Config
{
public:
    
	/**
	* Constructor.
	* About receiveOnPort: some linux leave port<1024 only for root,
	* The test PACS server (ClearCanvas) allows connection from any AE title
	* but it does not seem to understand the receive port
	* (it saves a device with port 104). The device config needs to be edited
	* so that the port number is 1024.
	*/
	Config()
	: m_serverAddress("176.34.102.200"),
	m_calledAETitle("SERVERAE"),
	m_serverPort(4006),
	m_receiveOnPort(12350)
    {
    }
    
    //! Get the ServerAddress.
	std::string getServerAddress() const { return m_serverAddress; }
    //! Get the CalledAETitle.
	std::string getCalledAETitle() const { return m_calledAETitle; }
    //! Get the ServerPort.
	unsigned long getServerPort() const { return m_serverPort; }
    //! Get the ReceiveOnPorts.
	unsigned long getReceiveOnPort() const { return m_receiveOnPort; }

    /**
    * Get a machine specific calling AE title.
    * Needs to be called once connections have been established (under windows).
    */
    std::string getCallingAETitle() const
    {
        char hostName[255];
        gethostname(hostName, 255);
        std::stringstream ss;
        ss << hostName;
        return ss.str().substr(0,15); // max 16char
    }
    
private:

    std::string m_serverAddress;
    std::string m_calledAETitle;
    unsigned long m_serverPort;
    unsigned long m_receiveOnPort; 

}; // class Config

} // namepsace test
} // namepsace pacs

#endif // PACSCONFIG_H

