// Copyright 2011 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

// std
#include <vector>
// boost
#include <boost/filesystem.hpp>
// dcmapi
#include <dcmDataSet.h>
#include <dcmDataSetReader.h>
// pacs
#include <pacsAPIDimse.h>
#include <pacsAPIMoveSCU.h>
// local
#include "pacsAPIMoveSCUTest.h"
#include "pacsConfig.h"

void PacsAPIMoveSCUTest::TestMove()
{
    // PACS details
    const pacs::test::Config testServerConfig;

    // association
    PACS::Association association;
    association.initAssociation(); //16KB max PDU length
    association.setApplicationCtx(UID_StandardApplicationContext);
    
    // Set default transferSyntaxe only (DICOM suggested default)
    const char *transferSyntaxes[1]={ UID_LittleEndianImplicitTransferSyntax };  //just the default transfer syntax

    // C-Find service for Patient Root Query/Retrive Information Model
    association.addPresentationCtx(UID_FINDPatientRootQueryRetrieveInformationModel,
        transferSyntaxes, 1);

    // C-Move service for Patient Root Query/Retrive Information Model
    association.addPresentationCtx(UID_MOVEPatientRootQueryRetrieveInformationModel,
        transferSyntaxes, 1);

    // Do the network connection and associate
    TSM_ASSERT_EQUALS("Connection to PACS failed.", association.networkConnect( 
        testServerConfig.getServerAddress().c_str(), 
        testServerConfig.getServerPort(), 30,
        NET_ACCEPTORREQUESTOR, 
        testServerConfig.getReceiveOnPort(), OFFalse), OFTrue);

    // AE titles 
    // needs to be called after the network connect for the 
    // CallingAETitle to have the hostname included
    association.setAETitles( 
        testServerConfig.getCallingAETitle().c_str(),
        testServerConfig.getCalledAETitle().c_str());  

    // Negotiate the association with the PACS
    TSM_ASSERT_EQUALS("Association to PACS failed.", association.associate(), OFTrue);

    // retrieve patient with id=123565
    const unsigned int nQueryStrings = 1;
    const char *queryString[]={"0010,0020=123567"};

    // setup move SCU
    PACS::MoveSCU moveSCU;
    moveSCU.setQueryRetriveLevel("PATIENT");

    // storage folder
    const std::string storageFolderName = "./pacstest/"; // compulsory end '/'
    moveSCU.setOutputStorageFolder(storageFolderName.c_str());
    
    // create the folder
    const boost::filesystem::path storageFolder( storageFolderName );
    boost::filesystem::create_directory( storageFolder );

    // do the move
    TSM_ASSERT_EQUALS("PACS query failed.", moveSCU.move( 
        &association, queryString, nQueryStrings, 
        testServerConfig.getReceiveOnPort()), OFTrue);
    
    // Close the association with the PACS
    TSM_ASSERT_EQUALS("Closing the association to PACS failed.", association.close(OFFalse), OFTrue);

    // check dicom data

    // Data set
    dcmAPI::DataSet::Pointer dcmData = dcmAPI::DataSet::New();
                
    // scan the current working folder for dicom files
    dcmAPI::DataSetReader::Pointer reader = dcmAPI::DataSetReader::New( );
    reader->SetDataSet( dcmData );
    try
    {
        reader->ReadDirectory( storageFolderName ); 
    }
    catch( const dcmAPI::DataSetReader::DcmDataSetReaderException& exception )
    {
        std::stringstream ss;
        ss << "Exception caught while reading dicom files: ";
        ss << exception.what();
        TS_FAIL( ss.str().c_str() );
    }

    // get patient
    const dcmAPI::Patient::Pointer patient = dcmData->GetPatient( dcmData->GetPatientIds()->at(0) );
    
    // check tag values
    std::string tagStr;
    tagStr = patient->GetTagAsText(dcmAPI::tags::PatientId); // PatientRealID is not stored...
    TSM_ASSERT_EQUALS( "Unexpected PatientId tag value.", tagStr, "1" );
    tagStr = patient->GetTagAsText(dcmAPI::tags::PatientName);
    TSM_ASSERT_EQUALS( "Unexpected PatientName tag value.", tagStr, "^Test" );
    tagStr = patient->GetTagAsText(dcmAPI::tags::PatientDate);
    TSM_ASSERT_EQUALS( "Unexpected PatientDate tag value.", tagStr, "20120701" );
    // patient sex is not stored...
    tagStr = patient->GetTagAsText(dcmAPI::tags::PatientSex);
    TSM_ASSERT_EQUALS( "Unexpected PatientSex tag value.", tagStr, "F" );

    // delete files and folder
    boost::filesystem::remove_all( storageFolder );
}

