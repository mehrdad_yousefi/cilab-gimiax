// Copyright 2011 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

// pacs
#include <pacsAPIDimse.h>
// local
#include "pacsAPIDimseTest.h"
#include "pacsConfig.h"

void PacsAPIDimseTest::TestAssociation()
{
    // PACS details
    const pacs::test::Config testServerConfig;

    // PACS association
    PACS::Association association;
    association.initAssociation(); //16KB max PDU length
    association.setApplicationCtx(UID_StandardApplicationContext);
    
    // Set some used transferSyntaxes;
    const char *transferSyntaxes[9]={
        UID_LittleEndianImplicitTransferSyntax, //the DICOM suggested default
        UID_LittleEndianExplicitTransferSyntax,
        UID_BigEndianExplicitTransferSyntax,
        UID_JPEGProcess1TransferSyntax,
        UID_JPEGProcess2_4TransferSyntax,
        UID_JPEGProcess3_5TransferSyntax,
        UID_JPEGProcess6_8TransferSyntax,
        UID_JPEGLSLosslessTransferSyntax,
        UID_JPEGLSLossyTransferSyntax};  

    // C-Find service for Patient Root Query/Retrive Information Model
    association.addPresentationCtx(UID_FINDPatientRootQueryRetrieveInformationModel,
        transferSyntaxes, 9);

    // C-Move service for Patient Root Query/Retrive Information Model
    association.addPresentationCtx(UID_MOVEPatientRootQueryRetrieveInformationModel,
        transferSyntaxes, 9);

    // Try to connect to the network
    TSM_ASSERT_EQUALS("Connection to PACS failed.", association.networkConnect( 
        testServerConfig.getServerAddress().c_str(), 
        testServerConfig.getServerPort(), 30,
        NET_ACCEPTORREQUESTOR, 
        testServerConfig.getReceiveOnPort(), OFFalse), OFTrue);

    // AE titles 
    // needs to be called after the network connect for the 
    // CallingAETitle to have the hostname included
    association.setAETitles( 
        testServerConfig.getCallingAETitle().c_str(),
        testServerConfig.getCalledAETitle().c_str());  
    
    // Negotiate the association with the PACS
    TSM_ASSERT_EQUALS("Association to PACS failed.", association.associate(), OFTrue);
    
    // Close the association with the PACS
    TSM_ASSERT_EQUALS("Closing the association to PACS failed.", association.close(OFFalse), OFTrue);
}

