// Copyright 2011 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

// std
#include <vector>
// dcmapi
#include <dcmTypes.h>
#include <dcmPACSQueryFileReader.h>
// pacs
#include <pacsAPIDimse.h>
#include <pacsAPIFindSCU.h>
// local
#include "pacsAPIFindSCUTest.h"
#include "pacsConfig.h"

void PacsAPIFindSCUTest::TestQuery()
{
    // PACS details
    const pacs::test::Config testServerConfig;

    // Do a simple association
    PACS::Association association;
    association.initAssociation(); 
    association.setApplicationCtx(UID_StandardApplicationContext);
    
    // Set default transferSyntaxe only (DICOM suggested default)
    const char *transferSyntaxes[1]={ UID_LittleEndianImplicitTransferSyntax };
    
    // C-Find service for Patient Root Query/Retrive Information Model
    association.addPresentationCtx(UID_FINDPatientRootQueryRetrieveInformationModel,
        transferSyntaxes,1);

    // Do the network connection and associate
    TSM_ASSERT_EQUALS("Connection to PACS failed.", association.networkConnect( 
        testServerConfig.getServerAddress().c_str(),
        testServerConfig.getServerPort(), 30,
        NET_ACCEPTORREQUESTOR, 
        testServerConfig.getReceiveOnPort(), OFFalse), OFTrue);

    // AE titles 
    // needs to be called after the network connect for the 
    // CallingAETitle to have the hostname included
    association.setAETitles( 
        testServerConfig.getCallingAETitle().c_str(),
        testServerConfig.getCalledAETitle().c_str());  

    // Negotiate the association with the PACS
    TSM_ASSERT_EQUALS("Association to PACS failed.", association.associate(), OFTrue);

    // Set the query in patient Level
    PACS::FindSCU findSCU;
    findSCU.setQueryRetriveLevel("PATIENT");
    
    // query tags
    std::vector< dcmAPI::Tag > queryTags;
    queryTags.push_back( dcmAPI::tags::PatientRealID );
    queryTags.push_back( dcmAPI::tags::PatientName );
    queryTags.push_back( dcmAPI::tags::PatientDate );
    queryTags.push_back( dcmAPI::tags::PatientSex );

    // query string
    const char** findString = new const char *[queryTags.size()];
    for( unsigned int i = 0; i < queryTags.size(); ++i )
    {
        findString[ i ] = queryTags[i].GetString()->c_str();
    }

    // output file
    const std::string outputFilename = "res_findscu.txt";
    findSCU.setOutTxtFile(outputFilename.c_str());
    
    // query
    TSM_ASSERT_EQUALS( "PACS query failed.", findSCU.query(&association, findString, queryTags.size()), OFTrue);

    // Close the association with the PACS
    TSM_ASSERT_EQUALS("Closing the association to PACS failed.", association.close(OFFalse), OFTrue);

    // dicom data set
    dcmAPI::DataSet::Pointer dataset = dcmAPI::DataSet::New();

    // result file reader
    dcmAPI::PACSQueryFileReader::Pointer reader = dcmAPI::PACSQueryFileReader::New();
    reader->SetDataSet( dataset );
    reader->SetPath( outputFilename );
    reader->SetQuery( queryTags );
    
    // read the file
    try
    {
        reader->Update();
    }
    catch(...)
    {
        TS_FAIL("Exception caught while reading file.");
    }

    // check patient with id 123456
    bool foundPatient = false;
    for( unsigned int i = 0; i < dataset->GetPatientIds()->size(); ++ i )
    {
        const dcmAPI::Patient::Pointer patient = dataset->GetPatient( dataset->GetPatientIds()->at(i) );
        // check tag values
        std::string tagStr;
        tagStr = patient->GetTagAsText(dcmAPI::tags::PatientRealID);
        if( tagStr == "=123567" )
        {
            foundPatient = true;
            tagStr = patient->GetTagAsText(dcmAPI::tags::PatientName);
            TSM_ASSERT_EQUALS( "Unexpected PatientName tag value.", tagStr, "=^Test" );
            tagStr = patient->GetTagAsText(dcmAPI::tags::PatientDate);
            TSM_ASSERT_EQUALS( "Unexpected PatientDate tag value.", tagStr, "=20120701" );
            tagStr = patient->GetTagAsText(dcmAPI::tags::PatientSex);
            // Patient sex is not stored by the dcmAPI?
            TSM_ASSERT_EQUALS( "Unexpected PatientSex tag value.", tagStr, "" );
        }
    }
    TSM_ASSERT("Patient with id 123456 not found.", foundPatient);

    // delete result file
    if( remove( outputFilename.c_str() ) != 0 )
    {
        perror( "Error deleting file." );
        TSM_ASSERT( "Error deleting file.", false );
    }

    // clean up
    delete[] findString;
}

