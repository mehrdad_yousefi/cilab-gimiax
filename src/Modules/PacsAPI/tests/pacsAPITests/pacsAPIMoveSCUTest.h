// Copyright 2011 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#ifndef PACSAPIMOVESCUTEST_H
#define PACSAPIMOVESCUTEST_H

#include <cxxtest/TestSuite.h>

/**
* Tests fo the PacsAPI MoveSCU methods.
* Equivalent to a dcmtk/dcmnet movescu:
*  movescu -aet PacsTest -aec SERVERAE --patient --port 1024 -v 10.80.4.213 104 query.dcm  
* This will send data on port 104, you need to have a storescp active on this port.
* To do this use the dcmtk/dcmnet storescp: storescp 1024
* With query.dcm created using dcmtk/dcmdata: dump2dcm query.txt query.dcm
* and query.txt:
# query patient names and IDs
(0008,0052) CS [PATIENT]     # QueryRetrieveLevel
(0010,0020) LO [123565]      # PatientID
* The data will be saved in the current folder.
*
* WARNING: your firewall needs to allow the test to download the data.
*/
class PacsAPIMoveSCUTest : public CxxTest::TestSuite
{
public:

    //! Move something from the PACS.
    void TestMove();

}; // class PacsAPIMoveSCUTest

#endif // PACSAPIMOVESCUTEST_H

