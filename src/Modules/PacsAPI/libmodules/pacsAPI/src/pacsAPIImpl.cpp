/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

	//---------------------------------------------------
	// HEADERS
	//---------------------------------------------------

	// SMoAPI
	#include "pacsAPIImpl.h"

	#include <fstream>
	#include <stdexcept>

	#include "boost/filesystem.hpp"


	//---------------------------------------------------
	// DLL ENTRY POINT
	// GLOBAL FUNCTION FOR RETURNING THE POINTER OF PACS::PacsAPI
	//---------------------------------------------------
	PACS::PacsAPI* PACS::CreateObjectOfPacsAPI()
	{
		return new PACS::PacsAPIImpl();
	}

	void PACS::DestroyObjectOfPacsAPI( PACS::PacsAPI* pPointer )
	{
		if ( pPointer != 0 )
		{
			delete pPointer;
		}
	}

	//---------------------------------------------------
	//  END
	//---------------------------------------------------


	std::map<PACS::QueryRetriveLevel, std::string> PACS::PacsAPIImpl::m_QueryRetriveLevelTable;

	//---------------------------------------------------
	// OPERATIONS
	//---------------------------------------------------


PACS::PacsAPIImpl::PacsAPIImpl()
{
	m_QueryRetriveLevelTable[ PATIENT].assign("PATIENT");
	m_QueryRetriveLevelTable[ STUDY ].assign("STUDY");
	m_QueryRetriveLevelTable[ SERIES ].assign("SERIES");
	m_QueryRetriveLevelTable[ IMAGE ].assign("IMAGE");

	m_QueryResultsFilename = "PACSQuery.txt";
	m_PreviousPort = 0;
}

PACS::PacsAPIImpl::~PacsAPIImpl()
{
	if ( m_PreviousPort != 0 )
	{
		m_Association.close(OFFalse);
	}
}

void PACS::PacsAPIImpl::Connect( bool send )
{
	OFBool result;

	// If port is the same, reuse association
	unsigned long currentPort = send? m_NetworkParams.clientPortSend : m_NetworkParams.clientPortRetrieve;
	if ( currentPort == m_PreviousPort )
	{
		return;
	}

	// Close previous association
	if ( m_PreviousPort != 0 )
	{
		m_Association.close(OFFalse);
	}

	m_Association.initAssociation(); //16KB max PDU length
	m_Association.setApplicationCtx(UID_StandardApplicationContext);

	const char *transferSyntaxes[1]={UID_LittleEndianImplicitTransferSyntax};  //just the default transfer syntax

	//This association proposes a C-Find service for Patient Root Query/Retrive Information Model
	m_Association.addPresentationCtx(
		UID_FINDPatientRootQueryRetrieveInformationModel,
		transferSyntaxes,
		1);

	//This association proposes a C-Move service for Patient Root Query/Retrive Information Model
	m_Association.addPresentationCtx(
		UID_MOVEPatientRootQueryRetrieveInformationModel,
		transferSyntaxes,
		1);

	if(m_AssociationParams.extraPresentationCtx.length()>0)
	{
		//This association proposes the service in the added sopclass
		m_Association.addPresentationCtx(
			(char *)m_AssociationParams.extraPresentationCtx.c_str(),
			transferSyntaxes,
			1);
	}

	//Add presentation context for all standard store SOP uids supported:
	for(int i=0;i<SUPPORTEDSTOREUID;i++)
		m_Association.addPresentationCtx((char *)StoreClassUID[i],transferSyntaxes,1);
	
	m_Association.setAETitles(m_NetworkParams.CallingAETitle.c_str(),
		m_NetworkParams.CalledAETitle.c_str());  //AE titles!
	result = m_Association.networkConnect(
		m_NetworkParams.serverIP.c_str(), 
		m_NetworkParams.serverPort, 
		m_NetworkParams.timeout,
		NET_ACCEPTORREQUESTOR, 
		currentPort, 
		OFFalse);

	if(result != OFTrue)
	{
		throw std::runtime_error( "Network connection to PACS server failed" );
	}

	// Associate
	result = m_Association.associate();
	if( result == OFFalse)
	{
		throw std::runtime_error( "Network connection to PACS server failed" );
	}

	m_PreviousPort = currentPort;
}

void PACS::PacsAPIImpl::SetNetworkParams( NetworkParams params )
{
	m_NetworkParams = params;
}

void PACS::PacsAPIImpl::Query()
{
	Connect( false );

	DoQuery();
}

void PACS::PacsAPIImpl::Move()
{
	Connect( false );

	DoMove();
}

void PACS::PacsAPIImpl::Store()
{

	Connect( true );

	DoStore();
}

void PACS::PacsAPIImpl::SetQueryParams( QueryParams params )
{
	m_QueryParams = params;
}

std::string PACS::PacsAPIImpl::GetQueryResultsFileName()
{
	return m_QueryResultsFilename;
}

void PACS::PacsAPIImpl::SetMoveParams( MoveParams params )
{
	m_MoveParams = params;
}

void PACS::PacsAPIImpl::DoQuery()
{
	OFBool result;
	std::string fullQueryResultsFilename = GetFullQueryResultsFileName( );

	// Clean file
	std::ofstream queryFile;
	queryFile.open ( fullQueryResultsFilename.c_str() );
	queryFile.close();

	std::string queryLevel = m_QueryRetriveLevelTable[ m_QueryParams.retrieveLevel ];
	m_Find.setQueryRetriveLevel( queryLevel.c_str() );
	m_Find.setOutTxtFile( fullQueryResultsFilename.c_str() );

	// Convert query to const char*
	std::vector<const char*> query;
	BuildQuery( query );

	// Do query
	result = m_Find.query( &m_Association, &query[ 0 ], (unsigned int)( query.size() ) );
	if( result == OFFalse)
	{
		throw std::runtime_error( "Query failed" );
	}
}

void PACS::PacsAPIImpl::DoMove()
{
	OFBool result;

	// Create results folder
	std::stringstream filenameStream;
	clock_t time = clock();
	filenameStream 
		<< m_MoveParams.outputStorageFolder 
		<< "/" << "PACS" << time << "/";
	m_MoveResultsFolder = filenameStream.str();

	try {
		boost::filesystem::create_directory ( m_MoveResultsFolder );
	}
	catch (... ) {
		std::ostringstream strError;
		strError 
			<< "Directory creation failed: "
			<< m_MoveResultsFolder
			<< std::endl;
		throw std::runtime_error( strError.str( ) );
	} 

	std::string queryLevel = m_QueryRetriveLevelTable[ m_QueryParams.retrieveLevel ];
	m_Move.setQueryRetriveLevel( queryLevel.c_str() );
	m_Move.setOutputStorageFolder( m_MoveResultsFolder.c_str() );

	// Convert query to const char*
	std::vector<const char*> query;
	BuildQuery( query );

	// Do query
	result = m_Move.move( &m_Association, &query[ 0 ], (unsigned int)( query.size() ), m_MoveParams.retriveport );
	if( result == OFFalse)
	{
		throw std::runtime_error( "Move failed" );
	}
}

void PACS::PacsAPIImpl::SetStoreParams( StoreParams  params)
{
	m_StoreParams = params;
}

void PACS::PacsAPIImpl::SetAssociationParams( AssociationParams  params)
{
	m_AssociationParams = params;
}

void PACS::PacsAPIImpl::DoStore()
{
	OFBool result;

	// Do store
	for(std::vector<std::string>::iterator it=m_StoreParams.fullfilenames.begin(); 
		it != m_StoreParams.fullfilenames.end(); ++it)
	{
		std::string fullfilename=(*it);

		result = m_Store.store(&m_Association, fullfilename.c_str() );
		if( result == OFFalse)
		{
			throw std::runtime_error( "Store failed" );
		}
	}

}

PACS::QueryParams PACS::PacsAPIImpl::GetQueryParams()
{
	return m_QueryParams;
}

std::string PACS::PacsAPIImpl::GetMoveResultsFolder()
{
	return m_MoveResultsFolder;
}

void PACS::PacsAPIImpl::BuildQuery( std::vector<const char*> &query )
{
	for ( unsigned i = 0 ; i < m_QueryParams.query.size() ; i++ )
	{

		//m_QueryParams.query[ i ].GetString( '=' );  //fill the m_TagInTextFormat
		query.push_back(m_QueryParams.query[ i ].GetString('=')->c_str( ) );
	}

	if ( !m_QueryParams.birthDateFrom.m_value.empty() )
	{
		query.push_back( m_QueryParams.birthDateFrom.GetString( '>' )->c_str( ) );
	}

	if ( !m_QueryParams.birthDateTo.m_value.empty() )
	{
		query.push_back( m_QueryParams.birthDateTo.GetString( '<' )->c_str( ) );
	}
}

std::string PACS::PacsAPIImpl::GetWorkingDirectory() const
{
	return m_WorkingDirectory;
}

void PACS::PacsAPIImpl::SetWorkingDirectory( const std::string &val )
{
	m_WorkingDirectory = val;
}

std::string PACS::PacsAPIImpl::GetFullQueryResultsFileName()
{
	std::string fullQueryResultsFilename;

	if (m_WorkingDirectory.empty()) {
		fullQueryResultsFilename = m_QueryResultsFilename;
	} else {
		fullQueryResultsFilename = m_WorkingDirectory + "/" + m_QueryResultsFilename;
	}

	return fullQueryResultsFilename;
}

//int main()
//{
//	PACS::PacsAPIImpl impl;
//	PACS::StoreParams sParams;
//	PACS::NetworkParams nParams;
//	PACS::AssociationParams aParams;
//
//	nParams.CalledAETitle.assign("ANYSCP");
//	nParams.clientPort=1235;
//	//nParams.CallingAETitle.assign("pacsAPILocal");
//	nParams.CallingAETitle.assign("g127.0.0.1");
//	nParams.serverIP="127.0.0.1";
//	nParams.serverPort=1234;
//	
//	impl.SetNetworkParams(nParams);
//	sParams.fullfilenames.push_back("c:/Code/fundus/fundus images/image1.dcm");
//	sParams.fullfilenames.push_back("c:/Code/fundus/fundus images/image2.dcm");
//	impl.SetStoreParams(sParams);
//
//	//aParams.extraPresentationCtx=UID_MRImageStorage;
//	//impl.SetAssociationParams(aParams);
//
//	impl.Store();
//}
