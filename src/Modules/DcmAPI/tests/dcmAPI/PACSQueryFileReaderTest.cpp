/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#include "PACSQueryFileReaderTest.h"

// std
#include <fstream>
// DcmAPI
#include <dcmPACSQueryFileReader.h>
// Predefined paths
#include <CISTIBToolkit.h>

void PACSQueryFileReaderTest::TestReadFile()
{
	// data path
	const std::string filePath = std::string(CISTIB_TOOLKIT_FOLDER) + "/Data/Tests/DcmAPI/pacs/res_findscu.txt";
	// Check if the file exists
	std::ifstream ifs( filePath.c_str() );
	TSM_ASSERT( "The input file does not exist.", !ifs.fail() );
	ifs.close();

    // data set to fill in
    dcmAPI::DataSet::Pointer dataset = dcmAPI::DataSet::New();

    // file reader
    dcmAPI::PACSQueryFileReader::Pointer reader = dcmAPI::PACSQueryFileReader::New();
	reader->SetDataSet( dataset );
	reader->SetPath( filePath );
    
    // query tags
    std::vector< dcmAPI::Tag > queryTags;
    queryTags.push_back( dcmAPI::tags::PatientRealID );
	queryTags.push_back( dcmAPI::tags::PatientName );
	queryTags.push_back( dcmAPI::tags::PatientDate );
	queryTags.push_back( dcmAPI::tags::PatientSex );
    reader->SetQuery(queryTags);
    
    // read the file
    try
    {
        reader->Update();
    }
    catch(...)
    {
        TS_FAIL("Exception caught while reading file.");
    }

	// get patient
	const dcmAPI::Patient::Pointer patient = dataset->GetPatient( dataset->GetPatientIds()->at(0) );
	
    // check tag values
    std::string tagStr;
    tagStr = patient->GetTagAsText(dcmAPI::tags::PatientRealID);
	TSM_ASSERT_EQUALS( "Unexpected tag value.", tagStr, "=123565" );
	tagStr = patient->GetTagAsText(dcmAPI::tags::PatientName);
	TSM_ASSERT_EQUALS( "Unexpected tag value.", tagStr, "=LastName^GivenName^MiddleName^Miss" );
	tagStr = patient->GetTagAsText(dcmAPI::tags::PatientDate);
	TSM_ASSERT_EQUALS( "Unexpected tag value.", tagStr, "=19830101" );
	// patient sex is not stored...
    tagStr = patient->GetTagAsText(dcmAPI::tags::PatientSex);
	TSM_ASSERT_EQUALS( "Unexpected tag value.", tagStr, "" );
}
