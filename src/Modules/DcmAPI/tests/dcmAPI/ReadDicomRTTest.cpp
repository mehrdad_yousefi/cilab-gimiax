/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#include "ReadDicomRTTest.h"

// VTK
#include "vtkSmartPointer.h"
#include "vtkImageClip.h"
// DcmAPI
#include "dcmDataSetReader.h"
#include "dcmMultiSliceReader.h"
#include "dcmStandardImageReader.h"
#include <dcmImageUtilities.h>

// Predefined paths
#include "CISTIBToolkit.h"
// baselib
#include "blVTKHelperTools.h"

void ReadDicomRTTest::TestReadDicomRT()
{
	// data path
	const std::string path = std::string(CISTIB_TOOLKIT_FOLDER) + "/Data/Tests/DcmAPI/dicomrt";
	
	// read the structure of the file
	dcmAPI::DataSetReader::Pointer dataSetReader = dcmAPI::DataSetReader::New( );
    dcmAPI::StandardImageReader::Pointer standardReaderPtr( new dcmAPI::StandardImageReader() );
    dataSetReader->AddReader(standardReaderPtr);
	try
	{
        dataSetReader->ReadDirectory( path );
    }
    catch ( const std::exception& e )
    {
        TSM_ASSERT( e.what(), false );
    }
	
	// check number of patients, study, series

    dcmAPI::Patient::Pointer patient;
    dcmAPI::PatientIdVectorPtr patientIdVector;
    dcmAPI::Study::Pointer study;
    dcmAPI::StudyIdVectorPtr studiesIdVector;
    dcmAPI::Series::Pointer series;
    dcmAPI::SeriesIdVectorPtr seriesIdVector;
    dcmAPI::TimePoint::Pointer timePoint;
    dcmAPI::SliceIdVectorPtr sliceIdVector;
    dcmAPI::Slice::Pointer slice;
    std::string filePath;

    // file name list
    std::vector< std::vector< std::string > > dcmFileNames;

    // Patients in the Dataset
    patientIdVector = dataSetReader->GetDataSet()->GetPatientIds();
    TSM_ASSERT_EQUALS( "There should be only one patient.", patientIdVector->size(), 1 );
    for( unsigned int i=0; i < patientIdVector->size(); ++i )
    {
        patient = dataSetReader->GetDataSet()->GetPatient(patientIdVector->at(i));
        
        // Studies of the patient
        studiesIdVector = patient->StudyIds();
        TSM_ASSERT_EQUALS( "There should be only one study.", studiesIdVector->size(), 1 );
        for( unsigned int j=0; j < studiesIdVector->size(); ++j )
        {
            study = patient->Study(studiesIdVector->at(j));
            
            // Series of the study
            seriesIdVector = study->SeriesIds();
            TSM_ASSERT_EQUALS( "There should be 2 series.", seriesIdVector->size(), 2 );
             // Series of the study
            for( unsigned int k=0; k < seriesIdVector->size(); ++k )
            {
                series = study->Series(seriesIdVector->at(k));
           
                const dcmAPI::TimePointIdVectorPtr timePointIdVector = series->TimePointIds();
                for( unsigned int l=0; l < timePointIdVector->size(); ++l )
                {
	                timePoint = series->TimePoint(timePointIdVector->at(l));
	                // sub level list
	                std::vector< std::string > list;
                    // Slice
                    sliceIdVector = timePoint->SliceIds();
                    for( unsigned int m=0; m < sliceIdVector->size(); ++m )
                    {
                        slice = timePoint->Slice(sliceIdVector->at(m));
                        filePath = slice->GetTagAsText(dcmAPI::tags::SliceFilePath);
                        // only add if not allready present (could be multislice single file)
                        if( ( list.size() == 0 || find( list.begin(), list.end(), filePath ) == list.end() ) )
                        {
                            list.push_back( filePath );
                        }
                    }
	                // add a level to the file list
	                dcmFileNames.push_back(list);
                }
            }
        }
    }

    // two lists: one with multiple files, one with the rtdose
    TS_ASSERT_EQUALS(dcmFileNames.size(), 2);
    if( dcmFileNames.size() == 0 ) return; // exit if no files
    
    // multiple files
    // for windows, the svn backup files (in '.svn') are also counted, filter them out
    for( unsigned int i = 0; i < dcmFileNames[0].size(); ++i )
    {
        if( dcmFileNames[0][i].find("svn") != std::string::npos )
        {
            dcmFileNames[0].erase( dcmFileNames[0].begin()+i );
        }
    }
    TS_ASSERT( (dcmFileNames[0].size() == 10) );
    if( dcmFileNames[0].size() == 0 ) return; // exit if no files
    
    // rtdose
    // for windows, only the one from the '.svn' folder is present
    TS_ASSERT_EQUALS(dcmFileNames[1].size(), 1);
    if( dcmFileNames[1].size() == 0 ) return; // exit if no files
    const bool found = ( dcmFileNames[1][0].find("rtdose.dcm") != std::string::npos );
    TSM_ASSERT("Wrong rtdose filename.", found);

    // read multislice file 
	std::vector< vtkSmartPointer<vtkImageData> > vtkVolumesVector;
	dcmAPI::MultiSliceReader::Pointer reader = dcmAPI::MultiSliceReader::New();
    reader->AddReader(standardReaderPtr);
	reader->SetPath( dcmFileNames[1][0] ); // should be first serie
	reader->SetDataSet( dataSetReader->GetDataSet() );
	try
	{
	    reader->Update( );
    }
    catch ( const std::exception& e )
    {
        TSM_ASSERT( e.what(), false );
    }
	vtkVolumesVector = reader->GetOutput( );
	
	// data to compare in test
	const int numberOfRowsInSlice = 129; 
	const int numberOfColsInSlice = 194;
	const int numberOfFrames = 98;
	const double xPixelSpacing = 1.0;
	const double yPixelSpacing = 1.0;
	const double zPixelSpacing = 3.0;
	const unsigned int numberOfVolumes = 1;

	// compare number of slices
	TS_ASSERT_EQUALS(vtkVolumesVector.size(), numberOfVolumes);

	vtkSmartPointer<vtkImageData> volume = vtkSmartPointer<vtkImageData>::New();
	volume = vtkVolumesVector.at(0);
	
	// compare dimensions
	int dim[3];
	volume->GetDimensions(dim);
	TS_ASSERT_EQUALS(dim[0], numberOfColsInSlice);
	TS_ASSERT_EQUALS(dim[1], numberOfRowsInSlice);
	TS_ASSERT_EQUALS(dim[2], numberOfFrames);
	// compare spacings
	double spacing[3];
	volume->GetSpacing(spacing);
	TS_ASSERT_EQUALS(spacing[0], xPixelSpacing);
	TS_ASSERT_EQUALS(spacing[1], yPixelSpacing);
	TS_ASSERT_EQUALS(spacing[2], zPixelSpacing);

    // read mulitple files
    vtkSmartPointer<vtkImageData> vtkDicomImage = 
        dcmAPI::ImageUtilities::ReadMultiSliceVtkImageFromFiles< unsigned short, 3 >( dcmFileNames[0], true );
	
    // compare dimensions
	vtkDicomImage->GetDimensions(dim);
	TS_ASSERT_EQUALS(dim[0], 512);
	TS_ASSERT_EQUALS(dim[1], 512);
	TS_ASSERT_EQUALS(dim[2], 10);
	// compare spacings
	vtkDicomImage->GetSpacing(spacing);
    TS_ASSERT_DELTA(spacing[0], 1.07422, 1e-5);
	TS_ASSERT_DELTA(spacing[1], 1.07422, 1e-5);
	TS_ASSERT_EQUALS(spacing[2], zPixelSpacing);

}
