/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#pragma once

#include "cxxtest/TestSuite.h"
// DcmAPI
#include <dcmDataSet.h>

/**
 * \brief Tests for reading general 4D DICOM data.
 * \ingroup dcmAPI
 */
class Read4DDataTest : public CxxTest::TestSuite 
{
public:

	/**
	 * \test Test the reading of 4D DICOM data without the time tag config file.
	 */
	void TestWithoutConfigFile();

    /**
	 * \test Test the reading of 4D DICOM data with the time tag config file.
	 */
	void TestWithConfigFile();

    /**
	 * \test Test the reading of 4D DICOM data with the time tag config file and a regular expression.
	 */
	void TestWithConfigFileAndRegEx();

private:

    /**
     * Check that the DcmData is correct.
     */
    bool CheckDcmData( 
        dcmAPI::DataSet::Pointer dcmData, 
        const std::vector< std::vector< std::string > >& dcmTimePoints ) const;

}; // class Read4DDataTest
