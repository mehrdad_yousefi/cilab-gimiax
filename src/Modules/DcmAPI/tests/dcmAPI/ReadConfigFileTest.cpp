/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#include "ReadConfigFileTest.h"

// std
#include <fstream>
#include <iostream>
// DcmAPI
#include "dcmConfigFileReader.h"
// Predefined paths
#include "CISTIBToolkit.h"

void ReadConfigFileTest::TestRead()
{
	// data path
	const std::string filePath = std::string(CISTIB_TOOLKIT_FOLDER) + "/Data/Tests/DcmAPI/config/dcmConfigFile.xml";
	const std::string dicomFolder = std::string(CISTIB_TOOLKIT_FOLDER) + "/Data/Tests/DcmAPI/synthetic/colors/axial/color-a1.dcm";

	// Check if the file exists
	std::ifstream ifs( filePath.c_str() );
	TSM_ASSERT( "The input file does not exist.", !ifs.fail() );
	ifs.close();

	// read the structure of the file
	dcmAPI::ConfigFileReader::Pointer configFileReader = dcmAPI::ConfigFileReader::New();
    configFileReader->SetFileName( filePath );
	try
	{
	    configFileReader->Update();
    }
    catch ( const std::exception& e )
    {
        TSM_ASSERT( e.what(), false );
    }
	
    dcmAPI::TagId readTagId;
    dcmAPI::TagId refTagId;

    // default manufacturer: good tag
    dcmAPI::DicomTimeTagConfig config = configFileReader->GetTimeTag(dicomFolder);
    readTagId = config.getTimeTag();
	refTagId = dcmAPI::TagId(0x7005, 0x1004);
    TSM_ASSERT_EQUALS("The default time tag is not the expected one.", readTagId, refTagId);
}
