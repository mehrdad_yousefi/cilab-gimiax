/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#include "Read4DDataTest.h"

// DcmAPI
#include <dcmDataSetReader.h>
#include <dcmConfigFileReader.h>
// Predefined paths
#include <CISTIBToolkit.h>

void Read4DDataTest::TestWithoutConfigFile()
{
	try
	{
		// input folder
        const std::string dcmInputFolder = std::string(CISTIB_TOOLKIT_FOLDER) + "/Data/Tests/DcmAPI/time/mr";
        // DICOM data set
		dcmAPI::DataSet::Pointer dcmData = dcmAPI::DataSet::New();
		// scan the input folder for dicom files
		dcmAPI::DataSetReader::Pointer reader = dcmAPI::DataSetReader::New();
		reader->SetDataSet( dcmData );
        reader->ReadDirectory( dcmInputFolder ); 
        // reference folder structure
        std::vector< std::vector< std::string > > dcmRefTimePoints;
        std::vector< std::string > files0;
        files0.push_back(dcmInputFolder+"/IM-0001-0001.dcm");
        files0.push_back(dcmInputFolder+"/IM-0001-0033.dcm");
        dcmRefTimePoints.push_back( files0 );
        std::vector< std::string > files1;
        files1.push_back(dcmInputFolder+"/IM-0001-0002.dcm");
        files1.push_back(dcmInputFolder+"/IM-0001-0034.dcm");
        dcmRefTimePoints.push_back( files1 );
        // compare the reference folder structure and the one from the DICOM data
        CheckDcmData( dcmData, dcmRefTimePoints );
	}
	catch(...)
	{
		TS_FAIL("Read4DDataTest::TestWithoutConfigFile failed: thrown exception.");
	}
}

void Read4DDataTest::TestWithConfigFile()
{
	try
	{
		// input folder
        const std::string dcmInputFolder = std::string(CISTIB_TOOLKIT_FOLDER) + "/Data/Tests/DcmAPI/time/mr";
        // DICOM data set
		dcmAPI::DataSet::Pointer dcmData = dcmAPI::DataSet::New();
		// read the config file
	    const std::string configFilePath = std::string(CISTIB_TOOLKIT_FOLDER) + "/Data/Tests/DcmAPI/config/dcmConfigFile.xml";
	    std::ifstream ifs( configFilePath.c_str() );
	    TSM_ASSERT( "The input config file does not exist.", !ifs.fail() );
	    ifs.close();
	    dcmAPI::ConfigFileReader::Pointer configFileReader = dcmAPI::ConfigFileReader::New();
        configFileReader->SetFileName( configFilePath );
	    configFileReader->Update();
		dcmAPI::DicomTimeTagConfig config = configFileReader->GetTimeTag( dcmInputFolder + "/IM-0001-0001.dcm" );

        const dcmAPI::TagId timeTagId = config.getTimeTag( );
        // scan the input folder for dicom files
		dcmAPI::DataSetReader::Pointer reader = dcmAPI::DataSetReader::New();
		reader->SetDataSet( dcmData );
        reader->SetTimeTag( timeTagId );
        reader->ReadDirectory( dcmInputFolder );
        // reference folder structure
        std::vector< std::vector< std::string > > dcmRefTimePoints;
        std::vector< std::string > files0;
        files0.push_back(dcmInputFolder+"/IM-0001-0001.dcm");
        files0.push_back(dcmInputFolder+"/IM-0001-0033.dcm");
        dcmRefTimePoints.push_back( files0 );
        std::vector< std::string > files1;
        files1.push_back(dcmInputFolder+"/IM-0001-0002.dcm");
        files1.push_back(dcmInputFolder+"/IM-0001-0034.dcm");
        dcmRefTimePoints.push_back( files1 );
        // compare the reference folder structure and the one from the DICOM data
        CheckDcmData( dcmData, dcmRefTimePoints );
	}
    catch ( const std::exception& e )
    {
        TS_FAIL( e.what() );
    }
	catch(...)
	{
		TS_FAIL("Read4DDataTest::TestWithConfigFile failed: thrown exception.");
	}
}

void Read4DDataTest::TestWithConfigFileAndRegEx()
{
	try
	{
		// input folder
        const std::string dcmInputFolder = std::string(CISTIB_TOOLKIT_FOLDER) + "/Data/Tests/DcmAPI/time/ratib3";
        // DICOM data set
		dcmAPI::DataSet::Pointer dcmData = dcmAPI::DataSet::New();
		// read the config file
	    const std::string configFilePath = std::string(CISTIB_TOOLKIT_FOLDER) + "/Data/Tests/DcmAPI/config/dcmConfigFile.xml";
	    std::ifstream ifs( configFilePath.c_str() );
	    TSM_ASSERT( "The input config file does not exist.", !ifs.fail() );
	    ifs.close();
	    dcmAPI::ConfigFileReader::Pointer configFileReader = dcmAPI::ConfigFileReader::New();
        configFileReader->SetFileName( configFilePath );
	    configFileReader->Update();
		dcmAPI::DicomTimeTagConfig config = configFileReader->GetTimeTag( dcmInputFolder + "/IM-0004-0001.dcm" );
        const dcmAPI::TagId timeTagId = config.getTimeTag( );
        const std::string regex = config.getRegex();
        const bool isInFile = config.getIsInFile();

        // scan the input folder for dicom files
		dcmAPI::DataSetReader::Pointer reader = dcmAPI::DataSetReader::New();
		reader->SetDataSet( dcmData );
        reader->SetTimeTag( timeTagId );
        reader->SetTimeTagRegEx( regex );
        reader->SetTimeTagLocation(isInFile);
        reader->ReadDirectory( dcmInputFolder );
        
        // reference folder structure
        std::vector< std::vector< std::string > > dcmRefTimePoints;
        std::vector< std::string > files0;
        files0.push_back(dcmInputFolder+"/IM-0004-0003.dcm");
        files0.push_back(dcmInputFolder+"/IM-0004-0002.dcm");
        files0.push_back(dcmInputFolder+"/IM-0004-0001.dcm");
        dcmRefTimePoints.push_back( files0 );
        std::vector< std::string > files1;
        files1.push_back(dcmInputFolder+"/IM-0005-0069.dcm");
        files1.push_back(dcmInputFolder+"/IM-0005-0068.dcm");
        files1.push_back(dcmInputFolder+"/IM-0005-0067.dcm");
        dcmRefTimePoints.push_back( files1 );
        // compare the reference folder structure and the one from the DICOM data
        CheckDcmData( dcmData, dcmRefTimePoints );
	}
    catch ( const std::exception& e )
    {
        TS_FAIL( e.what() );
    }
	catch(...)
	{
		TS_FAIL("Read4DDataTest::TestWithConfigFile failed: thrown exception.");
	}
}

bool Read4DDataTest::CheckDcmData( 
    dcmAPI::DataSet::Pointer dcmData, 
    const std::vector< std::vector< std::string > >& dcmRefTimePoints ) const
{
	// Patient (only one)
	const dcmAPI::Patient::Pointer patient = dcmData->GetPatient( dcmData->GetPatientIds()->at(0) );
	// Study (only one)
	const dcmAPI::Study::Pointer study = patient->Study( patient->StudyIds()->at(0) );
	// Serie (only one)
	const dcmAPI::Series::Pointer series = study->Series( study->SeriesIds()->at(0) );
	// TimePoint
    const size_t numberOfTimePoints = series->TimePointIds()->size();
    TSM_ASSERT_EQUALS( "Not the expected number of timepoints.", dcmRefTimePoints.size(), numberOfTimePoints );
    if( numberOfTimePoints != dcmRefTimePoints.size() ) return false;
    for( unsigned int t=0; t < numberOfTimePoints; ++t )
    {
        const dcmAPI::TimePoint::Pointer timePoint = series->TimePoint( series->TimePointIds()->at(t) );
		// Slice
		const dcmAPI::SliceIdVectorPtr sliceIdVector = timePoint->SliceIds();
        const size_t numberOfSlices = sliceIdVector->size();
        TSM_ASSERT_EQUALS( "Not the expected number of files.", dcmRefTimePoints[t].size(), numberOfSlices );
        if( numberOfSlices != dcmRefTimePoints[t].size() ) return false;
        for( unsigned int i=0; i < numberOfSlices; ++i)
		{
			const dcmAPI::Slice::Pointer slice = timePoint->Slice(sliceIdVector->at(i));
			const std::string filePath = slice->GetTagAsText(dcmAPI::tags::SliceFilePath);
            TSM_ASSERT_EQUALS( "Not the expected file path in time point.", dcmRefTimePoints[t][i], filePath );
		}
    }
    return true;
}
