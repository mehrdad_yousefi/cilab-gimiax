/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#pragma once

#include "cxxtest/TestSuite.h"

/**
 * \brief Tests for reading Siemens DICOM
 * \ingroup dcmAPI
 */
class ReadSiemensTest : public CxxTest::TestSuite 
{
public:

	/**
	 * \test Test reading of Siemens NM.
	 */
	void TestReadSiemensNuclearModality();

};

