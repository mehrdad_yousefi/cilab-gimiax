/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#pragma once

#include "cxxtest/TestSuite.h"

/**
 * \brief Tests for the MultiSliceReader.
 * \ingroup dcmAPI
 */
class MultiSliceReaderTest : public CxxTest::TestSuite 
{
public:

	/**
	 * \test Test reading without a specific reader.
	 */
	void TestReadWithoutReader();

};

