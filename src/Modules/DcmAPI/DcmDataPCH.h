/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _DcmDataPCH_H
#define _DcmDataPCH_H

#include "CILabBoostMacros.h"
#include "boost/shared_ptr.hpp"

#include <boost/foreach.hpp>
#include <exception>
#include <map>
#include <string>
#include <vector>

#endif //_DcmDataPCH_H
