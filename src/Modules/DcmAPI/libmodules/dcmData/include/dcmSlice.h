/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _dcmSLICE_H
#define _dcmSLICE_H

#include "dcmAPIDataWin32Header.h"

//DcmAPI
#include "dcmTypes.h"
#include "dcmBase.h"

namespace dcmAPI
{

/** 
* \brief Class that holds the structure of a DICOM Slice.
* 
* Reminder: the DICOM data structure can be modeled as a hierarchy tree which levels are:
* DataSet > Patient > Study > Series > TimePoint > Slice
* 
* \ingroup data
* \author Pedro Omedas
* \date 15 May 2008
*/

class DCMAPIDATA_EXPORT Slice : public Base
{
public:
	//! 
	cistibBoostPointerMacro(Slice);
	//!
	Slice();
	//!
	~Slice();
	//!
};

} // namespace dcmAPI

#endif //end _dcmSLICE_H
