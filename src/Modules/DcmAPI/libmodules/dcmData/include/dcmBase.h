/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _dcmBase_H
#define _dcmBase_H

#include "dcmAPIDataWin32Header.h"
//dcmAPI
#include "dcmTypes.h"

//Common libraries
#include <string>
#include <vector>

namespace dcmAPI
{

/** 
\brief Base class for all DICOM structures that contains a list of tags.

\ingroup data
\author Pedro Omedas, Yves Martelli
\date 15 May 2008
*/

class  DCMAPIDATA_EXPORT Base
{
public:
	
    //! Pointer typedef (boost shared) and New method.
	cistibBoostPointerMacro(Base);
	
    //! Constructor.
	Base();
	
    //! Destructor.
	virtual ~Base();

    /**
    * Get the tag map.
    * @return The tag map.
    */
	TagsMap* GetTagsMap();

    /**
    * Convert the value associated with \a id to a string.
    * @param id The tag id to get. If id is not found, a TagNotFound exception is thrown.
    * @return The string representation of the tag.
    */
	std::string GetTagAsText(const TagId& id) const;

	/**
    * Convert the value associated with \a id to a double. 
    * @param id The tag id to get. If id is not found, a TagNotFound exception is thrown.
    * @return The double representation of the tag.
    */
	double GetTagAsNumber(const TagId& id) const;
	
    /**
    * Add a tag to the tags map.
    * @param id The id of the tag.
    * @param value The value of the tag.
    */
	void AddTag(const TagId& id, const std::string& value);
	
    /**
    * Add a tag to the tags map.
    * @param tag The tag to add.
    */
	void AddTag(const Tag& tag );
	
    /**
    * Get the size of the tag map.
    * @return The size of the tag map.
    */
	unsigned int GetTagsMapSize() const;

private:

	//! Copy constructor. Purposely not implemented.
	Base( const Base& rhs );
	//! Assignement operator. Purposely not implemented.
	void operator=( const Base& rhs );

protected:

    //! Map to store tags. 
    TagsMap m_tagsMap;

}; // class Base

} // namespace dcmAPI

#endif //_dcmBase_H
