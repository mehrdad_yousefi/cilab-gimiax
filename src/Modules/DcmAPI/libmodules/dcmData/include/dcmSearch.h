/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _dcmSearch_H
#define _dcmSearch_H

#include "dcmAPIDataWin32Header.h"

//Common libraries
#include <string>
#include <vector>
#include <map>

//DcmAPI
#include "dcmDataSet.h"

namespace dcmAPI
{

/** 
\brief Search DICOM data or tags in a dcmDataSet
\ingroup dcmapi
\author Xavi Planes
\date 31 March 2010
*/

class DCMAPIDATA_EXPORT Search
{
public:
	//! Check that ImageOrientationPatient tag is present in all slices
	static bool CheckOrientationTag( DataSet::Pointer dataSet );

private:
};

} // namespace dcmAPI

#endif // End _dcmSearch_H
