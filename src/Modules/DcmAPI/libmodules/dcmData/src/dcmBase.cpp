/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "dcmBase.h"
using namespace dcmAPI;

Base::Base()
{
	m_tagsMap = TagsMap();
}

Base::~Base()
{
}

void Base::AddTag(const TagId& id, const std::string& value)
{
	m_tagsMap[ id ] = value;
}

void Base::AddTag(const Tag& tag )
{
	m_tagsMap[ tag.m_tagId ] = tag.m_value;
}

std::string Base::GetTagAsText(const TagId& id) const
{
	std::string tagAsText = "";
	TagsMap::const_iterator iter = m_tagsMap.find(id);
	if( iter != m_tagsMap.end() )
	{
		tagAsText = iter->second;
	}
	return tagAsText;
}

double Base::GetTagAsNumber(const TagId& id) const
{
	return atof( GetTagAsText( id ).c_str() );
}

TagsMap* Base::GetTagsMap()
{
	return &m_tagsMap;
}
