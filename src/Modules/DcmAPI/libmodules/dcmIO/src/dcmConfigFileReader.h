/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef DCMCONFIGFILEREADER_H
#define DCMCONFIGFILEREADER_H

// DCMAPI_EXPORT
#include "dcmAPIIOWin32Header.h"

// baseLib
#include <blTagMap.h>
#include <blLightObject.h>

// dcmAPI
#include "dcmTypes.h"
#include "dcmFile.h"

namespace dcmAPI
{

class DCMAPIIO_EXPORT DicomTimeTagConfig
{
public:
    //! constrcutor
    DicomTimeTagConfig( const dcmAPI::TagId& timeTag, const std::string& regex = "", bool isInFile = false, bool mixSeries = false)
        : m_timeTag( timeTag ), m_regex( regex ), m_isInFile( isInFile ), m_mixSeries( mixSeries ) {}
    //! Get the time tag.
    dcmAPI::TagId getTimeTag() const { return m_timeTag; }
    //! Get the regular expression.
    std::string getRegex() const { return m_regex; }
    //! Get the is in file flag.
    bool getIsInFile() const { return m_isInFile; }
    //! Get mix series flag
    bool getMixSeries() const { return m_mixSeries; }
private:
    dcmAPI::TagId m_timeTag;
    std::string m_regex;
    bool m_isInFile;
	bool m_mixSeries;
};    
    
/** 
* Reader for a dcmAPI config file.
* 
* Mainly used to read the first DICOM file and check all tags to automatically set the 
* tag for time that allows to read correctly the series
*
* \ingroup io
* \author Xavi Planes, Yves Martelli
* \date Mar 2011
*/
class DCMAPIIO_EXPORT ConfigFileReader : public blLightObject
{

public:

    typedef ConfigFileReader Self;
    typedef blSmartPointer<Self> Pointer;
    
    //! New method.
    blNewMacro(Self);

    //! Set the config file name.
    void SetFileName( const std::string& m_configFileName );
    
    //! Get the config file name.
    std::string GetFileName() const;
    
    //! Update method: read the config file.
    void Update();

    /**
    * Get the time tag configuration.
    * @param dicomFileName The DICOM file where to look for the time tag.
    * @return The associated DICOM time tag configuration (tag+regex+...).
    */
    DicomTimeTagConfig GetTimeTag(const std::string& dicomFolder) const;

protected:

    //! Constructor: protect from instantiation.
    ConfigFileReader();

    //! Destructor: protect from instantiation.
    ~ConfigFileReader();
    
	//!
	GdcmFilePtr FindFirstDicomFile( const std::string& filePath ) const;

private:

    //! Copy constructor: purposely not implemented.
    ConfigFileReader( const Self& filter);

    //! Assignement operator: purposely not implemented.
    void operator=( const Self& filter);

    //! Config file name;
    std::string m_configFileName;
    
    //! Tag map from the config file.
    blTagMap::Pointer m_tagMap;

}; // class ConfigFileReader

} // namespace dcmAPI

#endif //DCMCONFIGFILEREADER_H
