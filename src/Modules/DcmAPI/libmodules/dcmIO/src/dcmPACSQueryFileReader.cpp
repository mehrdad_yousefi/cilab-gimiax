/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "dcmPACSQueryFileReader.h"

#include "dcmTypes.h"

#include <boost/algorithm/string.hpp>

using namespace dcmAPI;

PACSQueryFileReader::PACSQueryFileReader()
{
	m_DataSet = dcmAPI::DataSet::New( );
}

PACSQueryFileReader::~PACSQueryFileReader()
{
}

void PACSQueryFileReader::Update()
{
	// Open query file
	std::ifstream queryFile;
	queryFile.open ( m_Path.c_str() );
	if ( queryFile.bad() )
	{
		return;
	}

	// For each patient -> Add to the vector
	std::string line;

	bool fileisOk = true;
	while ( fileisOk )
	{
		// Collect all TAGS for each query result
		dcmAPI::TagsMap tagMap;
		unsigned i = 0;
		while ( fileisOk && i < m_Query.size() )
		{
			fileisOk = std::getline(queryFile,line);
			if ( fileisOk )
			{
				boost::trim(line);
				if (!line.empty()) {
					dcmAPI::Tag dcmTAG( line );
					tagMap[ dcmTAG.m_tagId ] = dcmTAG.m_value;
					i++;
				}
			}
		}


		if ( fileisOk )
		{

			// Find if the patient already exists
			bool bNewPatient=false;
			dcmAPI::Patient::Pointer patient;
			patient = m_DataSet->GetPatient( tagMap[ dcmAPI::tags::PatientRealID ] );
			
			if(patient==NULL)
			{
				patient = dcmAPI::Patient::New();
				patient->AddTag( dcmAPI::Tag( dcmAPI::tags::PatientId,tagMap[ dcmAPI::tags::PatientRealID ]));
				patient->AddTag( dcmAPI::Tag( dcmAPI::tags::PatientRealID, tagMap[ dcmAPI::tags::PatientRealID ] ) );
				patient->AddTag( dcmAPI::Tag( dcmAPI::tags::PatientName, tagMap[ dcmAPI::tags::PatientName ] ) );
				patient->AddTag( dcmAPI::Tag( dcmAPI::tags::PatientDate, tagMap[ dcmAPI::tags::PatientDate ] ) );
				bNewPatient=true;
			}
			
			if ( tagMap[ dcmAPI::tags::PatientRealID ].empty() )
			{
				continue;
			}


			// Fill study data
			dcmAPI::Study::Pointer study;
			if ( !tagMap[ dcmAPI::tags::StudyInstanceUID ].empty() )
			{
				study = patient->Study( tagMap[ dcmAPI::tags::StudyInstanceUID ] );
				if(study==NULL)
				{
					study=dcmAPI::Study::New();
					study->AddTag( dcmAPI::Tag( dcmAPI::tags::StudyInstanceUID, tagMap[ dcmAPI::tags::StudyInstanceUID ] ) );
					study->AddTag( dcmAPI::Tag( dcmAPI::tags::StudyRealID, tagMap[ dcmAPI::tags::StudyRealID ] ) );
					study->AddTag( dcmAPI::Tag( dcmAPI::tags::StudyId, tagMap[ dcmAPI::tags::StudyInstanceUID ] ) );
					study->AddTag( dcmAPI::Tag( dcmAPI::tags::StudyDate, tagMap[ dcmAPI::tags::StudyDate ] ) );
					study->AddTag( dcmAPI::Tag( dcmAPI::tags::StudyDescription, tagMap[ dcmAPI::tags::StudyDescription ] ) );
					patient->AddStudy( study );
				}
			}

			// Fill series data
			if ( !tagMap[ dcmAPI::tags::SeriesInstanceUID ].empty())
			{
				//handle the case in which the studyinstanceuid is not present, but a series has been retrieved anyway:
				//this could happen if the query level is series and no studyinstanceuid have been specified (but only
				//patient id (note: on some server this kind of query should be forbidden since the studyinstanceuid is
				//specifically required in the case of sublevel queries - series is a sublevel respect to study!)
				//In this case create an empty study first...of course the study information has no sense in this case!
				if(study==NULL)
				{
					study=dcmAPI::Study::New();
					study->AddTag( dcmAPI::Tag( dcmAPI::tags::StudyInstanceUID, tagMap[ dcmAPI::tags::StudyInstanceUID ] ) );
					study->AddTag( dcmAPI::Tag( dcmAPI::tags::StudyRealID, tagMap[ dcmAPI::tags::StudyRealID ] ) );
					study->AddTag( dcmAPI::Tag( dcmAPI::tags::StudyId, tagMap[ dcmAPI::tags::StudyInstanceUID ] ) );
					study->AddTag( dcmAPI::Tag( dcmAPI::tags::StudyDate, tagMap[ dcmAPI::tags::StudyDate ] ) );
					study->AddTag( dcmAPI::Tag( dcmAPI::tags::StudyDescription, tagMap[ dcmAPI::tags::StudyDescription ] ) );
					patient->AddStudy( study );
				}


				dcmAPI::Series::Pointer series = study->Series( tagMap[ dcmAPI::tags::SeriesInstanceUID ] );
				if(series==NULL)
				{
					series=dcmAPI::Series::New();
					series->AddTag( dcmAPI::Tag( dcmAPI::tags::SeriesInstanceUID, tagMap[ dcmAPI::tags::SeriesInstanceUID ] ) );
					series->AddTag( dcmAPI::Tag( dcmAPI::tags::SeriesId, tagMap[ dcmAPI::tags::SeriesInstanceUID ] ) );
					series->AddTag( dcmAPI::Tag( dcmAPI::tags::SeriesNumber, tagMap[ dcmAPI::tags::SeriesNumber ] ) );
					series->AddTag( dcmAPI::Tag( dcmAPI::tags::SeriesDescription, tagMap[ dcmAPI::tags::SeriesDescription ] ) );
					series->AddTag( dcmAPI::Tag( dcmAPI::tags::Modality, tagMap[ dcmAPI::tags::Modality ] ) );
					study->AddSeries( series );
				}
			}

			if(bNewPatient)
				m_DataSet->AddPatient( patient );
		}
	}

	// Close file
	queryFile.close();
}

dcmAPI::DataSet::Pointer PACSQueryFileReader::GetDataSet() const
{
	return m_DataSet;
}

void PACSQueryFileReader::SetDataSet( dcmAPI::DataSet::Pointer val )
{
	m_DataSet = val;
}

std::string dcmAPI::PACSQueryFileReader::GetPath() const
{
	return m_Path;
}

void dcmAPI::PACSQueryFileReader::SetPath( std::string val )
{
	m_Path = val;
}

std::vector< dcmAPI::Tag > dcmAPI::PACSQueryFileReader::GetQuery() const
{
	return m_Query;
}

void dcmAPI::PACSQueryFileReader::SetQuery( 
	const std::vector< dcmAPI::Tag > &val )
{
	m_Query = val;
}
