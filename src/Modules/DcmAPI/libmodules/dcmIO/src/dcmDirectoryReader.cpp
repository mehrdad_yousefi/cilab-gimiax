/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "dcmDirectoryReader.h"
#include "dcmImageUtilities.h"
#include "dcmIOUtils.h"
#include "dcmConfigFileReader.h"

// BaseLib
#include "blTextUtils.h"

//gdcm
#include "gdcmUtil.h"
#include "gdcmDicomDir.h"
#include "gdcmDicomDirPatient.h"
#include "gdcmDicomDirStudy.h"
#include "gdcmDicomDirSerie.h"
#include "gdcmDicomDirImage.h"

//itk
#include <itksys/RegularExpression.hxx>
#include "itkExtractImageFilter.h"
#include "itkGDCMImageIO.h"

using namespace dcmAPI;

DirectoryReader::DirectoryReader()
{
	m_DataSet = DataSet::New();
	m_logger = blLogger::getInstance("dcmapi.DirectoryReader");
	
	m_TimeTagRegEx = "";
    m_TimeTolerance = 0;
	m_IsTimeTagInFile = false;
	m_MixSeries = false;
}

TagId DirectoryReader::GetTimeTag() const
{
    return m_TimeTag;
}

void DirectoryReader::SetTimeTag(const TagId& timeTag)
{
    m_TimeTag = timeTag;
}

float DirectoryReader::GetTimeTolerance() const
{
    return m_TimeTolerance;
}

void DirectoryReader::SetTimeTolerance(float tolerance)
{
    m_TimeTolerance = tolerance;
}

void DirectoryReader::SetTimeTagRegEx(const std::string& regex)
{
    m_TimeTagRegEx = regex;
}

void DirectoryReader::SetTimeTagLocation(bool isInFile)
{
    m_IsTimeTagInFile = isInFile;
}

void DirectoryReader::SetMixSeries(bool mixSeries)
{
    m_MixSeries = mixSeries;
}

void DirectoryReader::SetTagsConfigurationFilename( const std::string &val )
{
	m_TagsConfigurationFilename = val;
}

TagId DirectoryReader::GetFinalTimeTag(const std::string& modality)
{
    // if no time tage was set use the default one related to modality
    if(m_TimeTag.m_group==0 && m_TimeTag.m_element==0)
    {
	    m_TimeTag = IOUtils::GetTimeTagForSpecifiedModality(modality);
    }
    // use the set one
    return m_TimeTag;
}

std::string DirectoryReader::GetTimeTagValue(gdcm::DocEntry* docEntry) const
{
    std::string value;
    if(docEntry != NULL)
    {
	    GdcmValEntryPtr valEntry;
	    valEntry.reset(new gdcm::ValEntry(docEntry));
	    value = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
	    if( m_TimeTagRegEx != "" )
	    {
            LOG4CPLUS_DEBUG(m_logger, "Using a regular expression to find the time tag: " << m_TimeTagRegEx.c_str());
            // Compile the regular expression.
            itksys::RegularExpression re;
            if(!re.compile(m_TimeTagRegEx.c_str()))
            {
                throw std::runtime_error("sub-command REGEX, mode MATCHALL failed to compile regex \""+m_TimeTagRegEx+"\".");
            }
            // Only take the first match
            if(re.find(value))
            {
                std::string::size_type start = re.start();
                std::string::size_type end = re.end();
                if(end-start == 0)
                {
                    throw std::runtime_error("sub-command REGEX, mode MATCHALL regex \""+m_TimeTagRegEx+"\" matched an empty string.");
                }
                value = value.substr(start, end-start);
                LOG4CPLUS_DEBUG(m_logger, "Regex found a value: " << value.c_str());
            }
            else
            {
                LOG4CPLUS_DEBUG(m_logger, "Regex did not find a value.");
            }
	    }
    }
    else
    {
        // cannot find entry...
        LOG4CPLUS_WARN(m_logger, "No value for time tag.");
    }
    return value;
}

std::string GetFileName( const std::string &path, gdcm::DicomDirImage* gdcmImage )
{

	std::string fileName;
	GdcmValEntryPtr valEntry;
	gdcm::DocEntry* entry = NULL;
	std::string val;

	// ReferencedFileID
	entry = gdcmImage->GetDocEntry
		(
		tags::ReferencedFileID.m_group,
		tags::ReferencedFileID.m_element
		);
	if(entry != NULL)
	{
		valEntry.reset(new gdcm::ValEntry(entry));
		val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());

		//slice file name
		fileName = IOUtils::GetSliceFilePathFromReferenceFileID(val, path, false);
	}

	return fileName;
}
void DirectoryReader::Update()
{
	dcmAPI::ConfigFileReader::Pointer configFileReader;
	if( !m_TagsConfigurationFilename.empty( ) )
	{
		// read the XML config file
		configFileReader = dcmAPI::ConfigFileReader::New();
		configFileReader->SetFileName( m_TagsConfigurationFilename );
		configFileReader->Update();
	}

	DataSet::PatientsVector myPatientList;
	boost::shared_ptr< gdcm::DicomDir > dcmdir(new gdcm::DicomDir());
	dcmdir->SetDirectoryName(m_Path);
	
	if(dcmdir->Load() == true)
	{
		// fill patients list
		gdcm::DicomDirPatient* gdcmPatient(dcmdir->GetFirstPatient());
		GdcmValEntryPtr valEntry;
		gdcm::DocEntry* entry = NULL;
		std::string val;
		std::ostringstream idStr;
		int patientId = 1;
		int studyId = 1;
		int seriesId = 1;
		int sliceId = 1;
		while(gdcmPatient != NULL)
		{
			//create patient object and retrieve required tags
			Patient::Pointer patient = Patient::New();

			//patient id
			/*entry = gdcmPatient->GetDocEntry(tags::PatientID.m_group, tags::PatientID.m_element);
			valEntry.reset(new gdcm::ValEntry(entry));
			val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());*/
			idStr.str("");
			idStr<<patientId++;
			patient->AddTag(tags::PatientId, idStr.str());

			//patient name
			entry = gdcmPatient->GetDocEntry(tags::PatientName.m_group, tags::PatientName.m_element);
			valEntry.reset(new gdcm::ValEntry( entry));
			val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
			patient->AddTag(tags::PatientName, val);

			//patient birth date
			entry = gdcmPatient->GetDocEntry(tags::PatientDate.m_group, tags::PatientDate.m_element);
			valEntry.reset(new gdcm::ValEntry( entry));
			val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
			patient->AddTag(tags::PatientDate, val);

			//patient sex
			entry = gdcmPatient->GetDocEntry(tags::PatientSex.m_group, tags::PatientSex.m_element);
			valEntry.reset(new gdcm::ValEntry( entry));
			val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
			patient->AddTag(tags::PatientSex, val);

			//PatientWeight
			entry = gdcmPatient->GetDocEntry(tags::PatientWeight.m_group, tags::PatientWeight.m_element);
			if(entry != NULL)
			{
				valEntry.reset(new gdcm::ValEntry( entry));
				val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
				patient->AddTag(tags::PatientWeight, val);
			}

			// add to patient list
			myPatientList.push_back(patient);

			//fill study list
			gdcm::DicomDirStudy* gdcmStudy(gdcmPatient->GetFirstStudy());
			while(gdcmStudy != NULL)
			{	

				//create study object and retrieve required tags
				Study::Pointer study = Study::New();
				
				// Study Date
				entry = gdcmStudy->GetDocEntry(tags::StudyDate.m_group, tags::StudyDate.m_element);
				if(entry != NULL)
				{
					valEntry.reset(new gdcm::ValEntry(entry));
					val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
					study->AddTag(tags::StudyDate, val);
				}

				// Study Description
				entry = gdcmStudy->GetDocEntry(tags::StudyDescription.m_group, tags::StudyDescription.m_element);
				if(entry != NULL)
				{
					valEntry.reset(new gdcm::ValEntry(entry));
					val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
					study->AddTag(tags::StudyDescription, val);	
				}

				// Study UID (serves as ID)
				/*entry = gdcmStudy->GetDocEntry(tags::StudyInstanceUID.m_group, tags::StudyInstanceUID.m_element);
				if(entry != NULL)
				{
					valEntry.reset(new gdcm::ValEntry(entry));
					val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
					study->AddTag(tags::StudyInstanceUID, val);
				}*/

				//study id
				idStr.str("");
				idStr<<studyId++;
				study->AddTag(tags::StudyId, idStr.str());

				// add study to patient
				patient->AddStudy(study);

				int timePointId;

				//fill series list
				gdcm::DicomDirSerie* gdcmSeries(gdcmStudy->GetFirstSerie());
				while(gdcmSeries != NULL)
				{
					if( !m_TagsConfigurationFilename.empty( ) )
					{
						gdcm::DicomDirImage* gdcmImage(gdcmSeries->GetFirstImage());
						dcmAPI::DicomTimeTagConfig config = configFileReader->GetTimeTag( GetFileName( m_Path, gdcmImage ) );
						SetTimeTagRegEx( config.getRegex( ) );
						SetTimeTagLocation( config.getIsInFile( ) );
						SetMixSeries( config.getMixSeries( ) );
						m_TimeTag = config.getTimeTag( );
					}

					//create series object and retrieve required tags
					Series::Pointer series;
					if ( m_MixSeries && !study->SeriesIds( )->empty( ) )
					{
						series = study->Series( *study->SeriesIds( )->begin( ) );
					}
					else
					{
						series = Series::New();

						//add series to study
						study->AddSeries(series);

						timePointId = 1;
					}

					// Series  modality
					entry = gdcmSeries->GetDocEntry(tags::Modality.m_group, tags::Modality.m_element);
					if(entry != NULL)
					{
						valEntry.reset(new gdcm::ValEntry(entry));
						val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
						series->AddTag(tags::Modality, val);
					}
					
					//Series manufacturer
					entry = gdcmSeries->GetDocEntry(tags::Manufacturer.m_group, tags::Manufacturer.m_element);
					if(entry != NULL)
					{
						valEntry.reset(new gdcm::ValEntry(entry));
						val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
						series->AddTag(tags::Manufacturer, val);
					}

					//check if time information exists
					TimePoint::Pointer timePoint;
					std::string timeValue = "";
					bool hasTimeInformation = true;
					std::string modality = series->GetTagAsText(tags::Modality);
					TagId timeTagId = GetFinalTimeTag(modality);
					LOG4CPLUS_DEBUG(m_logger, "Time tag: " <<  "group " << timeTagId.m_group << "element " << timeTagId.m_element << "desc " << timeTagId.m_description.c_str());
					hasTimeInformation = timeTagId.IsSet();
					
					// Series UID (serves as ID)
					/*entry = gdcmSeries->GetDocEntry(tags::SeriesInstanceUID.m_group, tags::SeriesInstanceUID.m_element);
					if(entry != NULL)
					{
						valEntry.reset(new gdcm::ValEntry(entry));
						val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
						series->AddTag(tags::SeriesInstanceUID, val);
					}*/

					//series id
					idStr.str("");
					idStr<<seriesId++;
					series->AddTag(tags::SeriesId, idStr.str());
					
					// Series description
					entry = gdcmSeries->GetDocEntry(tags::SeriesDescription.m_group, tags::SeriesDescription.m_element);
					{
						valEntry.reset(new gdcm::ValEntry(entry));
						val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
						series->AddTag(tags::SeriesDescription, val);
					}
					
					//helper variables
					std::string filePath;
					std::string pixelType;
					bool pixelTypeFound = false;

					//fill timepoint list
					gdcm::DicomDirImage* gdcmImage(gdcmSeries->GetFirstImage());
					while(gdcmImage != NULL)
					{   
						//create slice object and retrieve required tags
						Slice::Pointer slice = Slice::New();
						
						// ReferencedFileID
						entry = gdcmImage->GetDocEntry
							(
							tags::ReferencedFileID.m_group,
							tags::ReferencedFileID.m_element
							);
						if(entry != NULL)
						{
							valEntry.reset(new gdcm::ValEntry(entry));
							val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());

							//slice file name
							std::string fileName = IOUtils::GetSliceFileNameFromReferenceFileID(val);
							if(fileName.length() > 0)
							{
								slice->AddTag(tags::SliceFileName, fileName);
							}

							//slice file path 
							filePath = IOUtils::GetSliceFilePathFromReferenceFileID(val, m_Path, false);
							if(filePath.length() > 0)
							{
								slice->AddTag(tags::SliceFilePath, filePath);
							}
						}

						// ImageNumber
						entry = gdcmImage->GetDocEntry(tags::ImageNumber.m_group, tags::ImageNumber.m_element);
						if(entry != NULL)
						{
							valEntry.reset(new gdcm::ValEntry(entry));
							val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
							slice->AddTag(tags::ImageNumber, val);
						}

						//slice id
						idStr.str("");
						idStr<<sliceId++;
						slice->AddTag(tags::SliceId, idStr.str());
						
						//pixel type is the same for all slices so it has to be read just 1 time
						if(!pixelTypeFound)
						{
							pixelType = ImageUtilities::ReadPixelType(filePath);
							pixelTypeFound = true;
						}
						//pixel type
						if(!pixelType.empty())
						{
							slice->AddTag(tags::PxType, pixelType);
						}
						else
							pixelTypeFound = false;

						// Image Position
						entry = gdcmImage->GetDocEntry
						(
						tags::ImagePositionPatient.m_group,
						tags::ImagePositionPatient.m_element
						);
						if(entry != NULL)
						{
							valEntry.reset(new gdcm::ValEntry(entry));
							val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
							slice->AddTag(tags::ImagePositionPatient, val);
						}

						// Image Orientation
						entry = gdcmImage->GetDocEntry
							(
							tags::ImageOrientationPatient.m_group,
							tags::ImageOrientationPatient.m_element
							);
						if(entry != NULL)
						{
							valEntry.reset(new gdcm::ValEntry(entry));
							val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
							slice->AddTag(tags::ImageOrientationPatient, val);
						}
						
						// Window width
						entry = gdcmImage->GetDocEntry(tags::WindowWidth.m_group, tags::WindowWidth.m_element);
						if(entry != NULL)
						{
							valEntry.reset(new gdcm::ValEntry(entry));
							val = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
							slice->AddTag(tags::WindowWidth, val);
						}

						//Window center
						entry = gdcmImage->GetDocEntry(tags::WindowCenter.m_group, tags::WindowCenter.m_element);
						if(entry != NULL)
						{
							slice->AddTag(tags::WindowCenter, val);
						}

						//ReadTimeValue
						if(hasTimeInformation)
						{
							// read the information in a gdcm file
							if( m_IsTimeTagInFile )
							{
						    	gdcm::File gdcmFile;
	                            gdcmFile.SetFileName( filePath );
	                            gdcmFile.Load();

	                            // For Binary entries, we need to force ascii value to retrieve the contents
								entry = gdcmFile.GetDocEntry(timeTagId.m_group, timeTagId.m_element);
								if ( entry )
								{
									GdcmValEntryPtr valEntry;
									valEntry.reset( new gdcm::ValEntry( entry ) );
									valEntry->SetValue( gdcmFile.GetEntryForcedAsciiValue(timeTagId.m_group, timeTagId.m_element) );
									timeValue = GetTimeTagValue(valEntry.get());
								}
								else
								{
									// cannot find entry...
									LOG4CPLUS_WARN(m_logger, "No value for time tag.");
								}
							}
							// get the information from the image
							else
							{
	                            entry = gdcmImage->GetDocEntry(timeTagId.m_group, timeTagId.m_element);
							    timeValue = GetTimeTagValue(entry);
							}
						}
						
						//find timepoint
						timePoint = IOUtils::FindTimepoint(timeValue, series, m_TimeTolerance);
						if(timePoint.get() == NULL)
						{
							//timepoint not found, create new timepoint
							timePoint = TimePoint::New();

							//Convert the integer into a string
							idStr.str("");
							idStr<<timePointId++;
							timePoint->AddTag(tags::TimePointId, idStr.str());
							timePoint->AddTag(tags::TimeValue, timeValue);
							series->AddTimePoint(timePoint);
						}

						// add slice to timepoint
						timePoint->AddSlice(slice);

						gdcmImage = gdcmSeries->GetNextImage();
					}

					// sort slices in timepoints
					dcmAPI::TimePointIdVectorPtr timePointIdVector = series->TimePointIds();
					for(size_t i=0; i < timePointIdVector->size(); i++)
					{
						std::string timepointId = timePointIdVector->at(i);
						IOUtils::SortTimepoint(series->TimePoint(timepointId));
					}

					IOUtils::SortSeriesByTime( series );

					gdcmSeries = gdcmStudy->GetNextSerie();
				}
				gdcmStudy = gdcmPatient->GetNextStudy();
			}
			gdcmPatient = dcmdir->GetNextPatient();
		}
	}
	
	m_DataSet->SetPatientsVector( myPatientList );
}

dcmAPI::DataSet::Pointer DirectoryReader::GetDataSet() const
{
	return m_DataSet;
}

void DirectoryReader::SetDataSet( dcmAPI::DataSet::Pointer val )
{
	m_DataSet = val;
}
std::string dcmAPI::DirectoryReader::GetPath() const
{
	return m_Path;
}

void dcmAPI::DirectoryReader::SetPath( std::string val )
{
	m_Path = val;
}
