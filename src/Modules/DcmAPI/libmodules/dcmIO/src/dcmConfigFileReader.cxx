/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#include "dcmConfigFileReader.h"

// itk
#include <itksys/SystemTools.hxx>
#include <itksys/Directory.hxx>
#include <itksys/RegularExpression.hxx>
// gdcm
#include <gdcmFile.h>
// baselib
#include <blXMLTagMapReader.h>
#include <blLogger.h>

// dcmapi
#include "dcmFile.h"
#include "dcmIOUtils.h"

namespace dcmAPI
{

ConfigFileReader::ConfigFileReader()
{
}

ConfigFileReader::~ConfigFileReader()
{
}

void ConfigFileReader::SetFileName( const std::string& fileName )
{
    m_configFileName = fileName;
}

std::string ConfigFileReader::GetFileName() const
{
    return m_configFileName;
}

void ConfigFileReader::Update()
{
	// Read DICOM time tags from XML configuration
	blXMLTagMapReader::Pointer reader = blXMLTagMapReader::New( );
	reader->SetFilename( m_configFileName.c_str() );
	reader->Update();
	m_tagMap = reader->GetOutput( );
	if ( m_tagMap.IsNull() )
	{
		throw std::runtime_error("Cannot read DICOM configuration file.");
	}
}

GdcmFilePtr ConfigFileReader::FindFirstDicomFile( const std::string& filePath ) const
{
    // Find the first dicom file
	std::string firstDICOMFile;
	bool fileFound = false;
	while ( !fileFound )
	{
		// Is a file
		if  ( itksys::SystemTools::FileExists( filePath.c_str( ), true ) )
		{
			// Load using gdcm
			GdcmFilePtr gdcmFile(new gdcm::File());
			gdcmFile->SetFileName( filePath );
			if(!gdcmFile->Load())
			{
				// Cannot read file -> Return empty pointer
				return GdcmFilePtr( );
			}

			// Check Directory record sequence for DICOMDIR files
			gdcm::DocEntry* e = gdcmFile->GetDocEntry(0x0004, 0x1220);
			if ( e )
			{
				// Is a DICOMDIR -> Return emtpy time pointer
				return GdcmFilePtr( );
			}

			return gdcmFile;

		}
		// Is a directory
		else if ( itksys::SystemTools::FileExists( filePath.c_str( ), false ) )
		{
			// Load directory
			itksys::Directory dir;
			dir.Load( filePath.c_str() );

			// Iterate over all files
			int pos = 0;
			while ( !fileFound && pos < dir.GetNumberOfFiles( ) )
			{
				// First two files are "." + ".."
				if ( std::string( dir.GetFile( pos ) ) != "." && 
					 std::string( dir.GetFile( pos ) ) != ".." )
				{
					std::string currentFile = filePath + "/" + dir.GetFile( pos );
					GdcmFilePtr gdcmFile = FindFirstDicomFile( currentFile );
					if ( gdcmFile )
					{
						fileFound = true;
						return gdcmFile;
					}
				}

				pos++;
			}

		}
	}

	return GdcmFilePtr( );
}

DicomTimeTagConfig ConfigFileReader::GetTimeTag( const std::string& dicomFolder ) const
{
	log4cplus::Logger logger = blLogger::getInstance("dcmapi.ConfigFileReader");

	GdcmFilePtr gdcmFile = FindFirstDicomFile( dicomFolder );
    LOG4CPLUS_INFO( logger, "Getting the time tag using DICOM file: " << gdcmFile->GetFileName().c_str());

	// Check if file exists and is a file
	if( !gdcmFile )
	{
		throw std::runtime_error("Problem while reading the provided DICOM folder.");
	}


    // Get the order: tag with the most conditions to the one with the least
    // (putting the number of conditions as first element to use std::sort directly)
    std::vector< std::pair< unsigned int, std::string > > sizeAndMapNames;
    std::pair< unsigned int, std::string > sizeAndMapName;
	blTagMap::TagMapType::iterator it;
	blTagMap::TagMapType::iterator subIt;
    blTagMap::Pointer subTagMap;
    std::string tagName;
    unsigned int numberOfConditions = 0;
	for ( it = m_tagMap->GetIteratorBegin() ; it != m_tagMap->GetIteratorEnd() ; it++ )
	{
		if ( it->second->GetValue().type() == typeid( blTagMap::Pointer ) )
		{
			subTagMap = m_tagMap->GetTagValue<blTagMap::Pointer>( it->second->GetName( ) );
	        numberOfConditions = 0;
            for ( subIt = subTagMap->GetIteratorBegin() ; subIt != subTagMap->GetIteratorEnd() ; subIt++ )
	        {
                tagName = subIt->second->GetName();
                if( tagName.substr(0,9) == "condition")
                {
                    ++numberOfConditions;
                }
            }
            sizeAndMapName = std::make_pair( numberOfConditions, it->first );
            sizeAndMapNames.push_back( sizeAndMapName );
        }
    }
    std::sort( sizeAndMapNames.begin(), sizeAndMapNames.end() );
    std::reverse( sizeAndMapNames.begin(), sizeAndMapNames.end() );

    // Go through the conditions from the most complex to the simplest
    dcmAPI::Tag tag;
    dcmAPI::Tag timeTag;
    std::string value;
    GdcmValEntryPtr valEntry;
    gdcm::DocEntry* entry;
    unsigned int numberOfMetConditions = 0;
    bool isInFile = false;
	bool mixSeries = false;
    std::string regex;
    DicomTimeTagConfig config = DicomTimeTagConfig(dcmAPI::TagId());
    for( unsigned int i=0; i<sizeAndMapNames.size(); ++i )
    {
        LOG4CPLUS_INFO( logger, "Checking config: " << std::string(sizeAndMapNames[i].second).c_str());
        subTagMap = m_tagMap->GetTagValue<blTagMap::Pointer>( sizeAndMapNames[i].second );
	    numberOfMetConditions = 0;
        for ( it = subTagMap->GetIteratorBegin() ; it != subTagMap->GetIteratorEnd() ; it++ )
	    {
            tagName = it->second->GetName();
            // check the conditions
            if( tagName.substr(0,9) == "condition" )
            {
                LOG4CPLUS_INFO( logger, "Checking condition: " << tagName.c_str() );
                // get the DICOM tag to check
                tag = dcmAPI::Tag( it->second->GetValueAsString() );
                LOG4CPLUS_INFO( logger, "config value: " << tag.m_value.c_str());
	            // find its value
	            entry = gdcmFile->GetDocEntry( tag.m_tagId.m_group, tag.m_tagId.m_element );
	            if( entry != NULL )
	            {
		            valEntry.reset( new gdcm::ValEntry(entry) );
		            value = IOUtils::CleanGdcmEntryStringValue( valEntry->GetValue() );
                    LOG4CPLUS_INFO( logger, "DICOM value: " << value.c_str());
                    // compare using lower case
                    std::transform( value.begin(), value.end(), value.begin(), tolower );
                    std::transform( tag.m_value.begin(), tag.m_value.end(), tag.m_value.begin(), tolower );

					// Compile the regular expression.
					itksys::RegularExpression re;
					if(!re.compile(tag.m_value.c_str()))
					{
						throw std::runtime_error("sub-command REGEX, mode MATCHALL failed to compile regex \""+tag.m_value+"\".");
					}

					// Only take the first match
					if(re.find(value))
					{
                        LOG4CPLUS_INFO( logger, "Condition passed.");
                        ++numberOfMetConditions;
                    }
                    else
                    {
                        LOG4CPLUS_INFO( logger, "Condition failed.");
                        break;
                    }
	            }
                else
                {
                    LOG4CPLUS_INFO( logger, "No value for DICOM tag.");
                    break;
                }
            }
            else if( tagName == "time_tag" )
            {
                timeTag = dcmAPI::Tag( it->second->GetValueAsString() );
            }
            else if( tagName == "regex" )
            {
                regex = it->second->GetValueAsString();
            }
            else if( tagName == "in_file" )
            {
                isInFile = it->second->GetValueCasted<bool>();
            }
            else if( tagName == "mix_series" )
            {
                mixSeries = it->second->GetValueCasted<bool>();
            }
        }
        // Found a pretendent!
        // (the last one should be the default with zero conditions)
        if( numberOfMetConditions == sizeAndMapNames[i].first )
        {
            config = DicomTimeTagConfig(timeTag.m_tagId, regex, isInFile, mixSeries);
            LOG4CPLUS_INFO( logger, "Using config: " << std::string(sizeAndMapNames[i].second).c_str());
            LOG4CPLUS_INFO( logger, "time_tag: " << timeTag.m_value.c_str()); // value should be its name...
            LOG4CPLUS_INFO( logger, "regex: " << regex.c_str());
            LOG4CPLUS_INFO( logger, "in_file: " << isInFile);
            LOG4CPLUS_INFO( logger, "mix_series: " << mixSeries);
            break;
        }
    }

    return config;
}    

} // namespace dcmAPI
