/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
/**
* @file sort.cpp 
* @brief Sort a folder containing DICOM data and puts all different series in specific folders.
*/
// boost
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string/trim.hpp>

// local
#include "dcmapiDicomSorter.h"

// tclap: needs to be after dcmapi, error about EOF (?)
#include <tclap/CmdLine.h>

// anonymous namespace
namespace
{

/**
* Converts a number to a string.
* @param value The number to convert.
* @return The string representation of the input.
*/
template< class T >
std::string toString( const T& value )
{
    std::ostringstream oss;
    oss << value;
    return oss.str(); 
}

/**
* Clean a string: trim, replace folder separator.
*/
std::string cleanStr( const std::string& input )
{
    return boost::algorithm::replace_all_copy( boost::algorithm::trim_copy(input), "/", "_" );
}

} // anonymous namespace

/**
* Sort DICOM data using UID.
* @param argv[1] The path to the DICOM data.
* @param argv[2] The DICOM reader.
*
* @ingroup dcmapiapps
*/
int main(int argc, char* argv[])
{
    // command line
    TCLAP::CmdLine cmd("DICOM Image sorter using UID. \
        \nSortfiles will return the following structure: \
        \nOUTPUT_PATH/ \
        \n\t|_ patientName#1 \
        \n\t\t|_ studyUID#1_Description \
        \n\t\t\t|_ seriesUID#1_Description \
        \n\t\t\t|_ seriesUID#2_Description \
        \n\t\t\t|_ ... \
        \n\t\t|_ studyUID#2_Description \
        \n\t\t\t|_ ... \
        \n\t|_ patientName#2 \
        \n\t|_ ...",
        ' ', "0.1");
    // input path argument
    TCLAP::UnlabeledValueArg<std::string> inputPathArg(
        "input_path", // name
        "The path to the DICOM data.",  // description
        true, "BAD_PATH", // required, default
        "input path", cmd); // short description, cmd to add to
    // output path argument
    TCLAP::ValueArg<std::string> outputPathArg(
        "o", "output_path", // name
        "The path to write the sorted DICOM data.",  // description
        true, "BAD_PATH", // required, default
        "output path", cmd); // short description, cmd to add to
    // time separator argument
    TCLAP::ValueArg<std::string> timeSepArg(
        "t", "time_sep", 
        "DICOM tag used for time separation (dcmapi):  'default': Trigger Time (0018,1060), 'quironct': truncated SOPInstanceUID (0008,0018), 'quironmr': Phase Number (2001,1008), 'quironmr2': Acquisition Time (0008,0032), 'auto': from a configuration file (dcmapi-config.xml).",  // description
        false, "default", // required, default
        "string", cmd); // short description, cmd to add to
    // time tolerance argument
    TCLAP::ValueArg<float> timeTolArg(
        "d", "time_tol", 
        "Tolerance (delta) on the time comparison when finding a timepoint (absolute).",  // description
        false, 0, // required, default
        "float", cmd); // short description, cmd to add to
    // create time folder flag
    TCLAP::SwitchArg createFolderSwitch("c","create_folder","Create time point folders", cmd, false);
    
    // parse command line
    cmd.parse( argc, argv );
    const std::string inputPathStr = inputPathArg.getValue();
    const std::string outputPathStr = outputPathArg.getValue();
    const std::string time_sep = timeSepArg.getValue();
    const float time_tol = timeTolArg.getValue();
    const bool createTimeFolder = createFolderSwitch.getValue();
    
    // time separator
    dcmapi::apps::DcmapiDicomSorter::ReadOption option = dcmapi::apps::DcmapiDicomSorter::DEFAULT;
    try
    {
        option = dcmapi::apps::DcmapiDicomSorter::FromString( time_sep );
    }
    catch( std::exception& exception )
    {
        std::cerr << "[ERROR] " << exception.what() << std::endl;
        return EXIT_FAILURE;
    }

    // ouptput if create folder
    if( createTimeFolder )
    {
	    std::cout << "[INFO] create time folder: " << createTimeFolder << std::endl;
    }

    // exit if input path does not exist
    const boost::filesystem::path inputPath(inputPathStr);
    if( !boost::filesystem::exists(inputPath) )
    { 
        std::cerr << "[ERROR] Input path does not exist: " << inputPathStr << std::endl;
        return EXIT_FAILURE;
    }
    // exit if input path does is not a directory
    if( !boost::filesystem::is_directory(inputPath) )
    { 
        std::cerr << "[ERROR] Input path is not a directory: " << inputPathStr << std::endl;
        return EXIT_FAILURE;
    }

    // check the output path
    const boost::filesystem::path outputPath(outputPathStr);
    if( !boost::filesystem::exists(outputPath) )
    { 
        std::cout << "[INFO] Output path does not exist, creating it (" << outputPathStr << ")" << std::endl;
        boost::filesystem::create_directory( outputPath );
    }
    // exit if output path does is not a directory
    if( !boost::filesystem::is_directory(outputPath) )
    { 
        std::cerr << "[ERROR] Output path is not a directory: " << outputPathStr << std::endl;
        return EXIT_FAILURE;
    }

    // DICOM data: representation of the data structure
    dcmAPI::DataSet::Pointer dcmData = dcmAPI::DataSet::New();
    dcmAPI::DataSetReader::Pointer reader = dcmAPI::DataSetReader::New( );
    reader->SetDataSet( dcmData );

	// Special time tag
    dcmapi::apps::DcmapiDicomSorter::SetReaderTimeTag( reader, inputPath, option, time_tol );

    std::cout << "[INFO] Reading dicom data from folder..." << std::endl;
    reader->ReadDirectory( inputPath.file_string() ); 
    
    // local vars
    std::string patientId;
    dcmAPI::Patient::Pointer patient;
    dcmAPI::PatientIdVectorPtr patientIdVector;
    std::string studyId;
    dcmAPI::Study::Pointer study;
    dcmAPI::StudyIdVectorPtr studiesIdVector;
    std::string seriesId;
    dcmAPI::Series::Pointer series;
    dcmAPI::SeriesIdVectorPtr seriesIdVector;
    std::string timePointId;
    dcmAPI::TimePoint::Pointer timePoint;
    dcmAPI::TimePointIdVectorPtr timePointIdVector;
    std::string sliceId;
    dcmAPI::Slice::Pointer slice;
    dcmAPI::SliceIdVectorPtr sliceIdVector;
    
    boost::filesystem::path patientPath;
    boost::filesystem::path studyPath;
    boost::filesystem::path seriesPath;
    boost::filesystem::path newSliceFilePath;

    std::string patientStr; 
    std::string studyStr; 
    std::string seriesStr;
    std::string timePointName;
    std::string sliceFileName;
    std::string sliceFilePath;
    std::string tmp;
    
    unsigned int count = 0;

    std::cout << "[INFO] Creating folders..." << std::endl;

    // Patients in the Dataset
    patientIdVector = dcmData->GetPatientIds();
    for(unsigned int i=0; i < patientIdVector->size(); ++i)
    {
        patientId = patientIdVector->at(i);
        patient = dcmData->GetPatient(patientId);
        
        patientStr = patient->GetTagAsText(dcmAPI::tags::PatientName);
        if( patientStr.size() == 0 ) patientStr = "patient_" + patientId;
        patientStr = cleanStr(patientStr);
        
        // handle patient folder allready exists
        if( boost::filesystem::exists(patientStr) )
        {
            patientStr += " (2)";
            while( boost::filesystem::exists(patientStr) )
            {
                const size_t index = patientStr.find_last_of("(");
                const unsigned int number = atoi( patientStr.substr(index+1, patientStr.size()-1 ).c_str() );
                patientStr = patientStr.substr(0, index+1) + toString(number+1) + ")";
            }
        }
        
        // create folder
        patientPath = outputPath / cleanStr(patientStr).c_str();
        std::cout << "[INFO] Creating patient folder: " << patientPath << std::endl;
        boost::filesystem::create_directory( patientPath );
        
        // Studies of the patient
        studiesIdVector = patient->StudyIds();
        for(unsigned int j=0; j < studiesIdVector->size(); ++j)
        {
            studyId = studiesIdVector->at(j);
            study = patient->Study(studyId);
            
            studyStr = study->GetTagAsText(dcmAPI::tags::StudyInstanceUID);
            if( studyStr.size() == 0 ) studyStr = "study_" + studyId;
            tmp = study->GetTagAsText(dcmAPI::tags::StudyDescription);
            if( tmp.size() != 0 ) studyStr += "-" + tmp;
            
            // create folder
            studyPath = patientPath / cleanStr(studyStr).c_str();
            std::cout << "[INFO] Creating study folder:   " << studyPath << std::endl;
            boost::filesystem::create_directory( studyPath );
            
            // Series of the study
            seriesIdVector = study->SeriesIds();
            for(unsigned int k=0; k < seriesIdVector->size(); ++k)
            {
                seriesId = seriesIdVector->at(k);
                series = study->Series(seriesId);
                
                seriesStr = series->GetTagAsText(dcmAPI::tags::SeriesInstanceUID);
                if( seriesStr.size() == 0 ) seriesStr = "series_" + seriesId;
                tmp = series->GetTagAsText(dcmAPI::tags::SeriesDescription);
                if( tmp.size() != 0 ) seriesStr += "-" + tmp;
                
                // create folder
                seriesPath = studyPath / cleanStr(seriesStr);
                seriesPath = studyPath;
                std::cout << "[INFO] Creating series folder:  " << seriesPath << std::endl;
                boost::filesystem::create_directory( seriesPath );
                
                // TimePoints in the series
                timePointIdVector = series->TimePointIds();
                for(unsigned int p=0; p < timePointIdVector->size(); ++p)
                {
                    timePointId = timePointIdVector->at(p);
                    timePoint = series->TimePoint(timePointId);
                    
                    std::string timepointStr = "time_" + timePointId;
                    boost::filesystem::path timepointPath = seriesPath / cleanStr(timepointStr);
                    
                    // create folder
                    if( createTimeFolder )
                    {
                        std::cout << "[INFO] Creating timepoint folder:  " << timepointPath << std::endl;
                        boost::filesystem::create_directory( timepointPath );
                    }

                    // Slices in the timepoint
                    sliceIdVector = timePoint->SliceIds();
                    for(unsigned int q=0; q < sliceIdVector->size(); ++q)
                    {
                        sliceId = sliceIdVector->at(q);
                        slice = timePoint->Slice(sliceId);
                        
                        sliceFilePath = slice->GetTagAsText(dcmAPI::tags::SliceFilePath);
                        sliceFileName = slice->GetTagAsText(dcmAPI::tags::SliceFileName);
                        
                        // copy file
                        if( createTimeFolder ) newSliceFilePath = timepointPath / sliceFileName;
                        else newSliceFilePath = seriesPath / sliceFileName;
                        boost::filesystem::copy_file( sliceFilePath, newSliceFilePath );
                        ++count;
                    }
                }
            }
        }
    }
    
    std::cout << "[INFO] Copied " << count << " file";
    if( count > 1 ) std::cout << "s";
    std::cout << "." << std::endl;

    return EXIT_SUCCESS;
}

