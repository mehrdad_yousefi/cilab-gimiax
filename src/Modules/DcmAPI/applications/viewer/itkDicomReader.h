/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
/**
* @file itkDicomReader.h 
* @brief DICOM reading methods using ITK.
*/
#ifndef ITKDICOMREADER_H
#define ITKDICOMREADER_H

// boost
#include <boost/filesystem.hpp>

// VTK
#include <vtkSmartPointer.h>
#include <vtkImageData.h>

// ITK
#include <itkImage.h>
#include <itkImageSeriesReader.h>
#include <itkGDCMImageIO.h>
#include <itkImageToVTKImageFilter.h>
#include <itkIndent.h>
#include <itkCommand.h>

// local
#include "AbstractDicomReader.h"
#include "itkDicomSorter.h"

/**
* \defgroup itkread ITK Readers
* \ingroup viewer
*/

//@{ 
namespace dcmapi
{
namespace apps
{

/**
* Command Observer.
* Derives from itk::Command and reimplements
* the Execute() method.
*/
class CommandObserver : public itk::Command 
{

public:
    //! Standard class typedefs.
    typedef CommandObserver Self;
    typedef itk::Command Superclass;
    typedef itk::SmartPointer<Self> Pointer;
    typedef itk::ProcessObject ProcessType;
    typedef const ProcessType* ProcessPointer;
    //! Method for creation through the object factory.
    itkNewMacro( Self );
    //! Execute with non const input.
    void Execute(itk::Object *caller, const itk::EventObject & event)
    {
        Execute( (const itk::Object *)caller, event);
    }
    //! Execute with const input.
    void Execute(const itk::Object * object, const itk::EventObject & event)
    {
        ProcessPointer filter = dynamic_cast< ProcessPointer >( object );
        if( typeid( event ) == typeid( itk::ProgressEvent ) )
        {
            const int nProgress = (int)(filter->GetProgress() * 100);
            std::cout << "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"; // X+3+1 chars + 1 for fun...
            std::cout << "[itk] Loading: "; // X chars
            // assure line is always of the same size
            if( nProgress < 10 ) std::cout << "  "; // 2 chars + 1 for number
            else if( nProgress < 100 ) std::cout << " "; // 1 char + 2 for number
            std::cout << nProgress << "%";
        }
        else if( typeid( event ) == typeid( itk::StartEvent ) )
        {
            std::cout << "[itk] Loading:   0%";
        }
        else if( typeid( event ) == typeid( itk::EndEvent ) )
        {
            std::cout << "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"; // X+3+1 chars + 1 for fun...
            std::cout << "[itk] Loading: 100%" << std::endl;
        }
    }
protected:
    //! Constructor: protect from instantiation.
    CommandObserver() {}
    //! Destructor: protect from instantiation.
    ~CommandObserver() {}
private:
    //! Copy constructor: purposely not implemented.
    CommandObserver( const Self& );
    //! Assignement operator: purposely not implemented.
    void operator=( const Self& );

}; // class CommandObserver

/**
* Itk DICOM Reader: DICOM reader using ITK methods.
*/
class ItkDicomReader : public AbstractDicomReader
{

public:

    /**
    * Read DICOM images from an input path (file or folder).
    * @input inputPath The input path.
    * @return A list of VTK images.
    */
    std::vector< vtkSmartPointer<vtkImageData> > Read( const boost::filesystem::path& inputPath )
    {
        // get a proper file list: only one time point
        const std::vector<std::string> paths = m_sorter.Sort( inputPath )[0];
        
        // print out file lsit
        std::cout << "[itk] files:";
        for( unsigned int i=0; i < paths.size(); ++i )
        {
            std::string path = paths[i];
            std::cout << " " << path.substr( path.find_last_of('/')+1, path.size() );
        }
        std::cout << std::endl;

        // use GDCMImageIO to obtain pizel type from the first file of the list
	    itk::GDCMImageIO::Pointer gdcmImageIO = itk::GDCMImageIO::New();
	    gdcmImageIO->SetFileName(paths[0]);
	    gdcmImageIO->ReadImageInformation();
        const itk::ImageIOBase::IOComponentType pixelType = gdcmImageIO->GetComponentType();
        
        // load the files
        vtkSmartPointer<vtkImageData> imageData;
        switch(pixelType)
        {
            case itk::ImageIOBase::DOUBLE:
                std::cout << "[itk] Data type: ImageIOBase::DOUBLE" << std::endl;
                imageData = ReadDicomFiles< double, 3 >(paths);
                break;
            case itk::ImageIOBase::FLOAT:
                std::cout << "[itk] Data type: ImageIOBase::FLOAT" << std::endl;
                imageData = ReadDicomFiles< float, 3 >(paths);
                break;
            case itk::ImageIOBase::LONG:
                std::cout << "[itk] Data type: ImageIOBase::LONG" << std::endl;
                imageData = ReadDicomFiles< long, 3 >(paths);
                break;
            case itk::ImageIOBase::ULONG:
                std::cout << "[itk] Data type: ImageIOBase::ULONG" << std::endl;
                imageData = ReadDicomFiles< unsigned long, 3 >(paths);
                break;
            case itk::ImageIOBase::INT:
                std::cout << "[itk] Data type: ImageIOBase::INT" << std::endl;
                imageData = ReadDicomFiles< int, 3 >(paths);
                break;
            case itk::ImageIOBase::UINT:
                std::cout << "[itk] Data type: ImageIOBase::UINT" << std::endl;
                imageData = ReadDicomFiles< unsigned int, 3 >(paths);
                break;
            case itk::ImageIOBase::SHORT:
                std::cout << "[itk] Data type: ImageIOBase::SHORT" << std::endl;
                imageData = ReadDicomFiles< short, 3 >(paths);
                break;
            case itk::ImageIOBase::USHORT:
                std::cout << "[itk] Data type: ImageIOBase::USHORT" << std::endl;
                imageData = ReadDicomFiles< unsigned short, 3 >(paths);
                break;
            case itk::ImageIOBase::CHAR:
                std::cout << "[itk] Data type: ImageIOBase::CHAR" << std::endl;
                imageData = ReadDicomFiles< char, 3 >(paths);
                break;
            case itk::ImageIOBase::UCHAR:
                std::cout << "[itk] Data type: ImageIOBase::UCHAR" << std::endl;
                imageData = ReadDicomFiles< unsigned char, 3 >(paths);
                break;
            default:
                throw std::runtime_error("[itk] Unkown pixel type.");
        }
        // return
        std::vector< vtkSmartPointer<vtkImageData> > data;
        data.push_back( imageData );
        return data;
    }

private:

    /**
    * Read a DICOM file/folder with a itk::ImageSeriesReader and itk::GDCMImageIO.
    * @param paths The list of DICOM files paths.
    * @return The read data.
    */
    template< class T, unsigned int N >
    vtkSmartPointer<vtkImageData> ReadDicomFiles(const std::vector<std::string>& paths)
    {
	    // tyepdef
        typedef itk::Image< T, N > ImageType;
        // reader
        typename itk::ImageSeriesReader< ImageType >::Pointer reader = 
            itk::ImageSeriesReader< ImageType >::New();
        // gdcm io
        itk::GDCMImageIO::Pointer gdcmImageIO = itk::GDCMImageIO::New();
        reader->SetImageIO( gdcmImageIO );
        // observers
        CommandObserver::Pointer observer = CommandObserver::New();
        reader->AddObserver( itk::StartEvent(), observer );
        reader->AddObserver( itk::ProgressEvent(), observer );
        reader->AddObserver( itk::EndEvent(), observer );
        // file or directory?
        if( paths.size() == 1 )
        {
            reader->SetFileName( paths[0] );
        }
        else
        {
            reader->SetFileNames( paths );
        }
        // image adaptor (itk -> vtk)
	    typename itk::ImageToVTKImageFilter< ImageType >::Pointer imageAdaptor 
	        = itk::ImageToVTKImageFilter< ImageType >::New();
        imageAdaptor->SetInput(reader->GetOutput());
        imageAdaptor->Update();
        // final image
        vtkSmartPointer<vtkImageData> imageData = vtkSmartPointer<vtkImageData>::New();
        imageData->DeepCopy( imageAdaptor->GetOutput() );
        return imageData;
    }

    // DICOM Files sorter.
    ItkDicomSorter m_sorter;

}; // class ItkDicomReader

} // namespace dcmapi
} // namespace apps

//@}

#endif // ITKDICOMREADER_H

