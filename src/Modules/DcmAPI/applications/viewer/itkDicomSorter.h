/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef ITKDICOMSORTER_H
#define ITKDICOMSORTER_H

// boost
#include <boost/filesystem.hpp>
// ITK
#include <itkGDCMSeriesFileNames.h>
// local
#include "AbstractDicomSorter.h"

// namespace
namespace dcmapi
{
namespace apps
{

/**
* Itk DICOM sorter: sort using ITK methods.
*/
class ItkDicomSorter : public AbstractDicomSorter
{

public:

    /**
    * Sort DICOM files from an input path (file or folder).
    * @input inputPath The input path.
    * @return A list of valid DICOM files ordered by timepoints.
    */
    std::vector< std::vector<std::string> > Sort(const boost::filesystem::path& inputPath )
    {
        // file list to load
        std::vector<std::string> fileList;
        // single file case
        if( boost::filesystem::is_regular_file(inputPath) )
        {
            std::cout << "[itk sort] Reading DICOM file." << std::endl;
            fileList.push_back(inputPath.file_string());
        }
        // folder case
        else if( boost::filesystem::is_directory(inputPath))
        {
            std::cout << "[itk sort] Reading DICOM folder." << std::endl;
            fileList = SortFolder(inputPath.file_string());
            std::cout << "[itk sort] " << fileList.size() << " file(s)." << std::endl;
        }
        else
        {
            std::cerr << "[itk][ERROR] Neither file, nor folder, this is a problem..." << std::endl;
        }
        // return
        std::vector< std::vector<std::string> > list;
        list.push_back( fileList );
        return list;
    }

    /**
    * Get the list of files to read from a DICOM folder.
    * @param path The path to the DICOM folder.
    * @return The file list.
    */
    std::vector<std::string> SortFolder(const boost::filesystem::path& inputPath)
    {
        itk::GDCMSeriesFileNames::Pointer sorter = itk::GDCMSeriesFileNames::New();
        sorter->SetInputDirectory( inputPath.file_string() );
        return sorter->GetInputFileNames();
    }

}; // class AbstractDicomSorter

} // namespace dcmapi
} // namespace apps

#endif // ITKDICOMSORTER_H
