/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef ABSTRACTDICOMSORTER_H
#define ABSTRACTDICOMSORTER_H

// boost
#include <boost/filesystem.hpp>

namespace dcmapi
{
namespace apps
{

/**
* Abstract DICOM sorter.
*/
class AbstractDicomSorter
{

public:

    /**
    * Sort DICOM files from an input path (file or folder).
    * @input inputPath The input path.
    * @return A list of valid DICOM files ordered by timepoints.
    */
    virtual std::vector< std::vector<std::string> > Sort( 
        const boost::filesystem::path& inputPath ) = 0;

}; // class AbstractDicomSorter

} // namespace dcmapi
} // namespace apps

#endif // ABSTRACTDICOMSORTER_H
