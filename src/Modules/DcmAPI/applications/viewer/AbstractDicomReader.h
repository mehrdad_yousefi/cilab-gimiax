/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef ABSTRACTDICOMREADER_H
#define ABSTRACTDICOMREADER_H

// boost
#include <boost/filesystem.hpp>
// VTK
#include <vtkSmartPointer.h>
#include <vtkImageData.h>
// local
#include "AbstractDicomSorter.h"

namespace dcmapi
{
namespace apps
{

/**
* Abstract DICOM reader.
*/
class AbstractDicomReader
{

public:

    /**
    * Read DICOM images from an input path (file or folder).
    * @input inputPath The input path.
    * @return A list of VTK images.
    */
    virtual std::vector< vtkSmartPointer< vtkImageData > > Read( 
        const boost::filesystem::path& inputPath ) = 0;

    /**
    * Set the DICOM files sorter.
    * @input The DICOM files sorter.
    */
    //virtual void SetDicomSorter( const AbstractDicomSorter& sorter) = 0;

}; // class AbstractDicomReader

} // namespace dcmapi
} // namespace apps

#endif // ABSTRACTDICOMREADER_H
