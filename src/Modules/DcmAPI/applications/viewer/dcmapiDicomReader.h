/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
/**
* @file dcmapiDicomReader.h
* @brief DICOM reading methods using the dcmapi.
*/
#ifndef DCMAPIDICOMREADER_H
#define DCMAPIDICOMREADER_H

// boost
#include <boost/filesystem.hpp>
// VTK
#include <vtkSmartPointer.h>
#include <vtkImageData.h>
// gdcm
#include <gdcmDicomDir.h>
#include <gdcmDicomDirPatient.h>
#include <gdcmDicomDirStudy.h>
#include <gdcmDicomDirSerie.h>
#include <gdcmValEntry.h>

// dcmapi
#include <dcmImageUtilities.h>
#include <dcmDataSetReader.h>
#include <dcmTypes.h>
#include <dcmConfigFileReader.h>
#include <dcmIOUtils.h>

// baselib
#include <blLogger.h>

// local
#include "AbstractDicomReader.h"
#include "dcmapiDicomSorter.h"

/**
* \defgroup dcmapiread DCMAPI Readers
* \ingroup viewer
*/

//@{ 
namespace dcmapi
{
namespace apps
{

/**
* dcmAPI DICOM Reader: DICOM reader using dcmAPI methods.
*/
class DcmapiDicomReader : public AbstractDicomReader
{

public:

    void SetOption( const DcmapiDicomSorter::ReadOption& option ) { m_option = option; }
    
    void SetTimeTolerance( float tolerance ) { m_timeTolerance = tolerance; }

    /**
    * Read DICOM images from an input path (file or folder).
    * @input inputPath The input path.
    * @return A list of VTK images.
    */
    std::vector< vtkSmartPointer<vtkImageData> > Read( const boost::filesystem::path& inputPath )
    {
        // get the file list
        m_sorter.SetOption( m_option );
        m_sorter.SetTimeTolerance( m_timeTolerance );
	    const std::vector< std::vector<std::string> > paths = m_sorter.Sort(inputPath);

        // use GDCMImageIO to obtain pizel type from the first file of the list
	    itk::GDCMImageIO::Pointer gdcmImageIO = itk::GDCMImageIO::New();
	    gdcmImageIO->SetFileName(paths[0][0]);
	    gdcmImageIO->ReadImageInformation();
        itk::ImageIOBase::IOComponentType pixelType = gdcmImageIO->GetComponentType();

        const bool reorientData = false;
        
        std::string protocol;
        gdcmImageIO->GetValueFromTag("0018|1030", protocol);
        const bool isTaggedMR = (protocol.find("3DTAG") != std::string::npos);

        std::vector< vtkSmartPointer<vtkImageData> > data;
    
        for( unsigned int i=0; i < paths.size(); ++i )
        {
            std::cout << "[dcmapi] Loading time point " << i+1 << "/" << paths.size() << std::endl;

            log4cplus::Logger logger = blLogger::getInstance("dcmapi.ReadDicomFiles");
            LOG4CPLUS_INFO( logger, "Time Point " << i+1 << "/" << paths.size() );
            std::string path = paths[i][0];
            LOG4CPLUS_INFO( logger, " root: " << path.substr( 0, path.find_last_of('/')) );
            for( unsigned int j=0; j < paths[i].size(); ++j )
            {
                path = paths[i][j];
                LOG4CPLUS_INFO( logger, " file: " << path.substr( path.find_last_of('/')+1, path.size()) );
            }

            vtkSmartPointer<vtkImageData> imageData;
            // load the files
            switch(pixelType)
            {
                case itk::ImageIOBase::DOUBLE:
                    std::cout << "[dcmapi] Data type: ImageIOBase::DOUBLE" << std::endl;
                    imageData = ReadDicomFiles< double, 3 >(paths[i], reorientData, isTaggedMR);
                    break;
                case itk::ImageIOBase::FLOAT:
                    std::cout << "[dcmapi] Data type: ImageIOBase::FLOAT" << std::endl;
                    imageData = ReadDicomFiles< float, 3 >(paths[i], reorientData, isTaggedMR);
                    break;
                case itk::ImageIOBase::LONG:
                    std::cout << "[dcmapi] Data type: ImageIOBase::LONG" << std::endl;
                    imageData = ReadDicomFiles< long, 3 >(paths[i], reorientData, isTaggedMR);
                    break;
                case itk::ImageIOBase::ULONG:
                    std::cout << "[dcmapi] Data type: ImageIOBase::ULONG" << std::endl;
                    imageData = ReadDicomFiles< unsigned long, 3 >(paths[i], reorientData, isTaggedMR);
                    break;
                case itk::ImageIOBase::INT:
                    std::cout << "[dcmapi] Data type: ImageIOBase::INT" << std::endl;
                    imageData = ReadDicomFiles< int, 3 >(paths[i], reorientData, isTaggedMR);
                    break;
                case itk::ImageIOBase::UINT:
                    std::cout << "[dcmapi] Data type: ImageIOBase::UINT" << std::endl;
                    imageData = ReadDicomFiles< unsigned int, 3 >(paths[i], reorientData, isTaggedMR);
                    break;
                case itk::ImageIOBase::SHORT:
                    std::cout << "[dcmapi] Data type: ImageIOBase::SHORT" << std::endl;
                    imageData = ReadDicomFiles< short, 3 >(paths[i], reorientData, isTaggedMR);
                    break;
                case itk::ImageIOBase::USHORT:
                    std::cout << "[dcmapi] Data type: ImageIOBase::USHORT" << std::endl;
                    imageData = ReadDicomFiles< unsigned short, 3 >(paths[i], reorientData, isTaggedMR);
                    break;
                case itk::ImageIOBase::CHAR:
                    std::cout << "[dcmapi] Data type: ImageIOBase::CHAR" << std::endl;
                    imageData = ReadDicomFiles< char, 3 >(paths[i], reorientData, isTaggedMR);
                    break;
                case itk::ImageIOBase::UCHAR:
                    std::cout << "[dcmapi] Data type: ImageIOBase::UCHAR" << std::endl;
                    imageData = ReadDicomFiles< unsigned char, 3 >(paths[i], reorientData, isTaggedMR);
                    break;
                default:
                    throw std::runtime_error("[dcmapi] Unkown pixel type.");
            }
            data.push_back( imageData );
        }
        // return
        return data;
    }


private:
    
    /**
    * Get the list of files to read from a DICOM folder. Simple: reads everything.
    * @param path The path to the DICOM folder.
    * @return The file list.
    */
    std::vector<std::string> GetDicomFolderFileListSimple(const std::string& path)
    {
        // create list of files
        std::vector< std::string > dcmFileNames;
        boost::filesystem::directory_iterator end_itr; // default construction yields past-the-end
        for( boost::filesystem::directory_iterator itr( path );
            itr != end_itr; ++itr )
        {
            if( is_regular_file(itr->status()) && itr->leaf() != "DICOMDIR" && itr->leaf()[0] != '.')
            {
                dcmFileNames.push_back( path + "/" + itr->leaf() );
            }
        }
        // sort the files alphabetically
        std::sort(dcmFileNames.begin(), dcmFileNames.end());
    
        // output
        std::cout << "[dcmapi] " << dcmFileNames.size() << " file(s)." << std::endl;

        // return
        return dcmFileNames;
    }

    /**
    * Utility method to print a std::map.
    * @input map The map from which to print the content.
    */
    void printMap( const std::map< dcmAPI::TagId, std::string >& map )
    {
        std::map< dcmAPI::TagId, std::string >::const_iterator it;
        for( it=map.begin(); it != map.end(); it++ )
        {
            std::cout << (*it).first << " => " << (*it).second << std::endl;
        }
    }

    /**
    * Read a list of DICOM files with a dcmapi reader.
    * @param paths The list of DICOM files paths.
    * @param reorientData Flag to reorient the data or not.
    * @param isTaggedMR Flag to load as tagged MR or not.
    * @return The read data.
    */
    template< class T, unsigned int N >
    vtkSmartPointer<vtkImageData> ReadDicomFiles(const std::vector<std::string>& paths, bool reorientData, bool isTaggedMR)
    {
        // Read using dcmAPI::ImageUtilities
        return dcmAPI::ImageUtilities::ReadMultiSliceVtkImageFromFiles< T, N >( 
            paths, reorientData, isTaggedMR );
    }

    // read option
    DcmapiDicomSorter::ReadOption m_option;
    // DICOM files sorter
    DcmapiDicomSorter m_sorter;
    // time tolerance
    float m_timeTolerance;

}; // class DcmapiDicomReader

} // namespace dcmapi
} // namespace apps

//@}

#endif // DCMAPIDICOMREADER_H

