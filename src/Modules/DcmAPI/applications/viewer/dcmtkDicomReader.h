/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
/**
* @file dcmtkDicomRead.h 
* @brief DICOM reading methods using dcmtk.
*/
#ifndef DCMTKDICOMREADER_H
#define DCMTKDICOMREADER_H

// boost
#include <boost/filesystem.hpp>

// VTK
#include <vtkSmartPointer.h>
#include <vtkImageData.h>

// itk
#include <itkGDCMSeriesFileNames.h>

// dcmtk
#include <dcmtk/dcmimgle/dcmimage.h>
#include <dcmtk/dcmdata/dcfilefo.h>
#include <dcmtk/dcmdata/dcdeftag.h>

// local
#include "AbstractDicomReader.h"
#include "itkDicomSorter.h"

/**
* \defgroup dcmtkread dcmtk Readers
* \ingroup viewer
*/

//@{ 
namespace dcmapi
{
namespace apps
{

/**
* dcmtk DICOM Reader: DICOM reader using dcmtk methods.
*/
class DcmtkDicomReader : public AbstractDicomReader
{

public:

    /**
    * Read DICOM images from an input path (file or folder).
    * @input inputPath The input path.
    * @return A list of VTK images.
    */
    std::vector< vtkSmartPointer<vtkImageData> > Read( const boost::filesystem::path& inputPath )
    {
        // get the file list: only one timepoint
	    const std::vector<std::string> paths = m_sorter.Sort(inputPath)[0];

        // load first slice to get image info
        DcmFileFormat fileformat;
        OFCondition status = fileformat.loadFile(paths[0].c_str());
        if(!status.good())
        {
            std::cerr << "[dcmtk][ERROR] Error reading image:" << status.text() << std::endl;
            throw std::runtime_error("[dcmtk][ERROR] Error reading image.");
        }
    
        double spacing[3];
        fileformat.getDataset()->findAndGetFloat64(DCM_PixelSpacing, spacing[0], 0);
        fileformat.getDataset()->findAndGetFloat64(DCM_PixelSpacing, spacing[1], 1);
        spacing[2] = 1;

        // load first image
        DicomImage* dcmtkImage = new DicomImage( paths[0].c_str() );
        // check image
        if( dcmtkImage == NULL )
        {
            throw std::runtime_error("[dcmtk][ERROR] Null pointer returned.");
        }
        EI_Status imageStatus = dcmtkImage->getStatus();
        if( imageStatus != EIS_Normal )
        {
            std::cerr << "[dcmtk][ERROR] Error reading image:" << DicomImage::getString(imageStatus) << std::endl;
            throw std::runtime_error("[dcmtk][ERROR] Error reading image.");
        }

        unsigned long size[3];
        //fileformat.getDataset()->findAndGetUint32(DCM_Rows, size[0]);
        //fileformat.getDataset()->findAndGetUint32(DCM_Columns, size[1]);
        size[0] = dcmtkImage->getWidth();
        size[1] = dcmtkImage->getHeight();
        size[2] = paths.size();
    
        // create vtk image
        vtkSmartPointer<vtkImageData> imageData = vtkSmartPointer<vtkImageData>::New();
        imageData->SetOrigin( 0, 0, 0 );
        imageData->SetSpacing( spacing );
        imageData->SetDimensions( size[0], size[1], size[2] );


        if( dcmtkImage->getInterData() != NULL )
        {
            int rep = dcmtkImage->getInterData()->getRepresentation();
            switch(rep)
            {
            case EPR_Sint8:
                std::cout << "[dcmtk] Data type: EPR_Sint8" << std::endl;
                imageData->SetScalarTypeToChar();
                break;

            case EPR_Sint16:
                std::cout << "[dcmtk] Data type: EPR_Sint16" << std::endl;
                imageData->SetScalarTypeToShort();
                break;

            case EPR_Sint32:
                std::cout << "[dcmtk] Data type: EPR_Sint32" << std::endl;
                imageData->SetScalarTypeToLong();
                break;

            case EPR_Uint8:
                std::cout << "[dcmtk] Data type: EPR_Uint8" << std::endl;
                imageData->SetScalarTypeToUnsignedChar();
                break;

            case EPR_Uint16:
                std::cout << "[dcmtk] Data type: EPR_Uint16" << std::endl;
                imageData->SetScalarTypeToUnsignedShort();
                break;

            case EPR_Uint32:
                std::cout << "[dcmtk] Data type: EPR_Uint32" << std::endl;
                imageData->SetScalarTypeToUnsignedLong();
                break;
            } 
        }
        else
        {
            imageData->SetScalarTypeToUnsignedShort();
        }
        imageData->AllocateScalars();

        // read all slices
        for( unsigned int k = 0; k < paths.size(); ++k )
        {
            // load image
            dcmtkImage = new DicomImage( paths[k].c_str() );
    
            // check image
            if( dcmtkImage == NULL )
            {
                throw std::runtime_error("[dcmtk][ERROR] Null pointer returned.");
            }
            EI_Status imageStatus = dcmtkImage->getStatus();
            if( imageStatus != EIS_Normal )
            {
                std::cerr << "[dcmtk][ERROR] Error reading image:" << DicomImage::getString(imageStatus) << std::endl;
                throw std::runtime_error("[dcmtk][ERROR] Error reading image.");
            }

            // fill in pixel data
            dcmtkImage->getOutputData(
                imageData->GetScalarPointer(0,0,k),
                dcmtkImage->getOutputDataSize() ); 

            // clean up
            delete dcmtkImage;
        }

        // add image to vector of images
        std::vector< vtkSmartPointer<vtkImageData> > data;
        //data.push_back( imageData );
        return data;
    }

private: 

    // DICOM files sorter.
    ItkDicomSorter m_sorter;

}; // class DcmtkDicomReader

} // namepsace apps
} // namepsace dcmapi

//@}

#endif // DCMTKDICOMREADER_H
