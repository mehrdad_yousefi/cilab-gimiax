/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
/**
* @file dump.cpp 
* @brief Dump DICOM file header content.
*/
// std
#include <map>
// boost
#include <boost/filesystem.hpp>
// ITK
#include <itkGDCMSeriesFileNames.h>
#include <itkGDCMImageIO.h>
#include <gdcmFile.h>
#include <gdcmDocEntry.h>
#include <gdcmValEntry.h>
#include <gdcmUtil.h>
// tclap
#include <tclap/CmdLine.h>
// local
#include "dcmapiDicomSorter.h"

// anonymous namespace
namespace
{

/**
* Split a std::string according to a delimiter.
* @param str The string to split.
* @param delim The splitting delimiter.
*/
std::vector<std::string> split(const std::string& str, const std::string& delim)
{
    size_t start_pos = 0;
    size_t match_pos;
    size_t substr_length;
    std::vector<std::string> result;

    while((match_pos = str.find(delim, start_pos)) != std::string::npos)
    {
        substr_length = match_pos - start_pos;

        if (substr_length > 0)
        {
            result.push_back(str.substr(start_pos, substr_length));
        }

        start_pos = match_pos + delim.length();
    }

    substr_length = str.length() - start_pos;

    if (substr_length > 0)
    {
        result.push_back(str.substr(start_pos, substr_length));
    }

    return result;
}

/**
* Write a std::map to a tabulation separated file.
* @input map The map to write.
* @input filename The name of the file to write to.
*/
void writeMapToFile( const std::map< std::string, std::vector<std::string> >& map, const std::string& filename )
{
    std::ofstream ofs( filename.c_str() );

    const std::string separator = "\t"; // ';' can be used in comments

    std::map< std::string, std::vector<std::string> >::const_iterator it;
    for( it = map.begin(); it != map.end(); it++ )
    {
        // key
        std::vector<std::string> splitted = split( it->first, "|" );
        for( unsigned int i = 0; i < splitted.size(); ++i )
        {
            ofs << splitted[i] << separator;
        }
        
        // value
        for( unsigned int i = 0; i < it->second.size(); ++i )
        {
            ofs << it->second[i] << separator;
        }
        ofs << std::endl;
    }

    ofs.close();
}

} // anonymous namespace

/**
* Dump DICOM file header content.
* @param argv[1] The path to the DICOM data (file or folder).
*
* @ingroup dcmapiapps
*/
int main(int argc, char* argv[])
{
    // command line
    TCLAP::CmdLine cmd("DICOM header(s) dump.", ' ', "0.1");
    // path argument
    TCLAP::UnlabeledValueArg<std::string> pathArg(
        "input_path", // name
        "The path to the DICOM data.",  // description
        true, "BAD_PATH", // required, default
        "input path", cmd); // short description, cmd to add to
    
    // parse command line
    cmd.parse( argc, argv );
    const std::string inputPathStr = pathArg.getValue();

    // check input path
    const boost::filesystem::path inputPath(inputPathStr);
    if( !boost::filesystem::exists(inputPath) )
    { 
        std::cerr << "[ERROR] Input path does not exist: " << inputPathStr << std::endl;
        return EXIT_FAILURE;
    }

    // output file
    const std::string filename = "tagmap.csv";
    const boost::filesystem::path outputPath = inputPath / filename.c_str();
    std::cout << "[INFO] dumping DICOM header content to: " << outputPath.file_string() << std::endl;

    // get the list of file
    itk::GDCMSeriesFileNames::Pointer sorter = itk::GDCMSeriesFileNames::New();
    sorter->SetInputDirectory( inputPath.file_string() );
    sorter->SetRecursive(true);
    std::vector< std::string > fileNames = sorter->GetInputFileNames();
    if( fileNames.size() == 0 )
    {
        std::cout << "[WARNING] No files found using GDCM." << std::endl;
        dcmapi::apps::DcmapiDicomSorter sorter2;
        std::vector< std::vector<std::string> > paths = sorter2.Sort(inputPath);
        for( int i=0; i < paths.size(); ++i )
        {
            for( int j=0; j < paths[i].size(); ++j )
            {
                fileNames.push_back( paths[i][j] );
            }
        }
        if( fileNames.size() == 0 )
        {
            std::cerr << "[ERROR] No files found using dcmapi." << std::endl;
            return EXIT_FAILURE;
        }
    }

    // tag map
    typedef std::map <std::string, std::vector<std::string> > MapType;
    typedef std::pair< std::string, std::vector<std::string> > MapContentPairType;
    MapType tagMap;
    MapType::iterator it;

    // file name row
    const std::vector<std::string> emptyVector;
    std::ostringstream osskeyfile;
    osskeyfile << "Root|";
    osskeyfile << inputPathStr;
    osskeyfile << "|Filename";
    const std::string mapFilenameKey = osskeyfile.str();
    tagMap.insert( MapContentPairType( mapFilenameKey, emptyVector ) );
    
    // get the tags
    itk::GDCMImageIO::Pointer gdcmImageIO = itk::GDCMImageIO::New();
    gdcm::DocEntry* docEntry;
    std::string dcmKey; // for ex: "0002|0000"
    std::string dcmName; // for ex: "Group Length"
    std::string mapKey; // for ex: "0002|0000|Group Length"
    std::string dcmValue;
    boost::filesystem::path dcmFilePath;
    std::string message;
    for( unsigned int i = 0; i < fileNames.size(); ++i )
    {
	    // progress
        std::ostringstream ossinfo;
        ossinfo << "[INFO] reading file " << i+1 << "/" << fileNames.size();
        for( unsigned int j = 0; j < ossinfo.str().size(); ++j )
            std::cout << "\b";
        std::cout << ossinfo.str();
        
        // store the file name
        dcmFilePath = boost::filesystem::path(fileNames[i]);
        tagMap.find( mapFilenameKey )->second.push_back( dcmFilePath.filename() );

        // read the dicom file
        gdcm::File gdcmFile;
	    gdcmFile.SetFileName( fileNames[i] );
	    if(!gdcmFile.Load())
	    {
            std::cerr << "[ERROR] cannot read: " << fileNames[i] << std::endl;
        }

        // store dicom header content
        docEntry = gdcmFile.GetFirstEntry();
        while( docEntry != NULL )
        {
            // get the tag value
            gdcm::ValEntry valEntry( docEntry );
            dcmValue = valEntry.GetValue();
            gdcm::Util::CreateCleanString( dcmValue );

            // create the tag map key
            std::ostringstream osskey;
            osskey << docEntry->GetKey();
            osskey << "|";
            osskey << docEntry->GetName();
            mapKey = osskey.str();

            // either add a row or add a value to a row
            it = tagMap.find( mapKey );
            if( it == tagMap.end() )
            {
                std::vector<std::string> valueVector;
                valueVector.push_back( dcmValue );
                tagMap.insert( std::pair< std::string, std::vector<std::string> >(mapKey, valueVector) );
            }
            else
            {
                it->second.push_back( dcmValue );
            }

            // who's next!
            docEntry = gdcmFile.GetNextEntry();
        }
    }

    // write to file
    writeMapToFile( tagMap, outputPath.file_string() );

    return EXIT_SUCCESS;
}
