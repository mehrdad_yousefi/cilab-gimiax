/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef DCMAPIDICOMSORTER_H
#define DCMAPIDICOMSORTER_H

// boost
#include <boost/filesystem.hpp>
// gdcm
#include <gdcmDicomDir.h>
#include <gdcmDicomDirPatient.h>
#include <gdcmDicomDirStudy.h>
#include <gdcmDicomDirSerie.h>
#include <gdcmDicomDirImage.h>
// baselib
#include <blLogger.h>
// dcmapi
#include <dcmDataSetReader.h>
#include <dcmIOUtils.h>
#include <dcmConfigFileReader.h>

// namespace
namespace dcmapi
{
namespace apps
{

/**
* dcmapi DICOM sorter: sort using dcmapi methods.
*/
class DcmapiDicomSorter
{

public:

    /**
    * Option for the dcmapi reader (mostly changes of the time separator tag).
    */
    enum ReadOption
    {
        DEFAULT,
        QUIRON_CT,
        QUIRON_MR,
        QUIRON_MR2,
        AUTO
    };

    void SetOption( const ReadOption& option ) { m_option = option; }

    void SetTimeTolerance( float tolerance ) { m_timeTolerance = tolerance; }

    /**
    * Convert a std::string representing an option to a ReadOption.
    * @input option_str The std::string representing an option.
    * @return The coresponding ReadOption.
    */
    static ReadOption FromString( const std::string& option_str )
    {
        ReadOption option = DEFAULT;
        if( option_str == "default")
        {
            option = DEFAULT;
        }
        else if( option_str == "quironct")
        {
            option = QUIRON_CT;
        }
        else if( option_str == "quironmr")
        {  
            option = QUIRON_MR;
        }
        else if( option_str == "quironmr2")
        {  
            option = QUIRON_MR2;
        }
        else if( option_str == "auto")
        {  
            option = AUTO;
        }
        else
        {
            std::ostringstream oss;
            oss << "Unknown DICOM read option: ";
            oss << option_str;
            throw std::runtime_error( oss.str() );
        }
        return option;
    }
    
    /**
    * Set the time tag for a dcmAPI::DataSetReader.
    * @param reader The reader to set.
    * @param inputPath The input path of the data.
    * @param option The time tag option.
    */
    static void SetReaderTimeTag( 
        dcmAPI::DataSetReader::Pointer reader, 
        const boost::filesystem::path& inputPath,
        const ReadOption& option,
        float timeTolerance = 0)
    {
        // Use a truncate of the SOPInstanceUID
        if( option == QUIRON_CT )
        {
            std::cout << "[dcmapi sort] Option: QUIRON_CT." << std::endl;
            reader->SetTimeTag(dcmAPI::tags::SOPInstanceUID);
            reader->SetTimeTagRegEx("[0-9]$");
        }
        // Use the phase number from the dicom file
        else if( option == QUIRON_MR )
        {
            std::cout << "[dcmapi sort] Option: QUIRON_MR." << std::endl;
            reader->SetTimeTag(dcmAPI::tags::PhaseNumberMR);
            reader->SetTimeTagLocation(true);
        }
        // Use the acquisition time from the dicom file
        else if( option == QUIRON_MR2 )
        {
            std::cout << "[dcmapi sort] Option: QUIRON_MR2." << std::endl;
            reader->SetTimeTag(dcmAPI::tags::AcquisitionTime);
            reader->SetTimeTagLocation(true);
        }
        // Use a config file
        else if( option == AUTO )
        {
            std::cout << "[dcmapi sort] Option: AUTO." << std::endl;
            if( boost::filesystem::is_directory(inputPath))
            {
                // read the configuration file
                dcmAPI::ConfigFileReader::Pointer configFileReader = dcmAPI::ConfigFileReader::New();
                configFileReader->SetFileName( "dcmapi-config.xml" );
	            try
	            {
	                configFileReader->Update();
                }
                catch ( const std::exception& e )
                {
                    std::cerr << "[dcmapi sort] Error while reading time tag configuration file." << std::endl;
                }
                // get the configuration
                dcmAPI::DicomTimeTagConfig config = configFileReader->GetTimeTag(inputPath.file_string());
                // set the time tag
                reader->SetTimeTag(config.getTimeTag());
                reader->SetTimeTagLocation(config.getIsInFile());
            }
            else
            {
                std::cerr << "[dcmapi sort] Error: only for directory." << std::endl;

            }
        }
        // good for all
        reader->SetTimeTolerance( timeTolerance );
    }
    
    /**
    * Sort DICOM files from an input path (file or folder).
    * @input inputPath The input path.
    * @return A list of valid DICOM files ordered by timepoints.
    */
    std::vector< std::vector<std::string> > Sort( const boost::filesystem::path& inputPath )
    {
        log4cplus::Logger logger = blLogger::getInstance("dcmapi.GetDicomFilesList");
        LOG4CPLUS_INFO( logger, "Input path: " << inputPath );

        // DICOM data: representation of the data structure
        dcmAPI::DataSet::Pointer dcmData = dcmAPI::DataSet::New();

        // scan the current working folder for dicom files
        dcmAPI::DataSetReader::Pointer reader = dcmAPI::DataSetReader::New( );
        reader->SetDataSet( dcmData );
    
        // Special time tag
        SetReaderTimeTag( reader, inputPath, m_option, m_timeTolerance );
    
        // file list to load
        std::vector< std::vector<std::string> > fileList;
        // single file case
        if( boost::filesystem::is_regular_file(inputPath) )
        {
            if( inputPath.file_string() == "DICOMDIR" )
            {
                std::cout << "[dcmapi sort] Reading DICOMDIR structure." << std::endl;
                // set the data set from the DICOMDIR
                reader->ReadDicomDir( inputPath.file_string() ); 
                // get the file list
                fileList = GetDataSetFileList(dcmData);
                // output
                size_t nFiles = 0;
                for( unsigned int i = 0; i < fileList.size(); ++i )
                {
                    nFiles += fileList[i].size();
                }
                std::cout << "[dcmapi sort] " << nFiles << " file(s)." << std::endl;
            }
            else
            {
                std::cout << "[dcmapi sort] Reading DICOM file structure." << std::endl;
                // set the data set from the file
                reader->ReadDcmFile( inputPath.file_string() ); 
                // get the file list
                std::vector<std::string> list;
                list.push_back(inputPath.file_string());
                fileList.push_back( list );
            }
        }
        // folder case
        else if( boost::filesystem::is_directory(inputPath))
        {
            std::cout << "[dcmapi sort] Reading DICOM folder structure." << std::endl;
            // set the data set from the directory
            reader->ReadDirectory( inputPath.file_string() ); 
            // get the file list
            fileList = GetDataSetFileList(dcmData);
            // output
            size_t nFiles = 0;
            for( unsigned int i = 0; i < fileList.size(); ++i )
            {
                nFiles += fileList[i].size();
            }
            std::cout << "[dcmapi sort] " << nFiles << " file(s)." << std::endl;
        }
        else
        {
            std::cerr << "[dcmapi sort][ERROR] Neither file, nor folder, this is a problem..." << std::endl;
        }
    
        // show the data set structure
        //ShowDataSetStructure(dcmData);
    
        // return
        return fileList;
    }

 private:
     
     /**
    * Get the list of files to read from a data set.
    * @param dataSet The data set representing the structure.
    * @return The file list.
    */
    std::vector< std::vector<std::string> > GetDataSetFileList(const dcmAPI::DataSet::Pointer& dataSet)
    {
        // file name list
        std::vector< std::vector< std::string > > dcmFileNames;
    
        // Patient: take the first one
        if( dataSet->GetPatientIds()->size() > 1 )
        {
            std::cerr << "[dcmapi sort][WARNING] More than one patient, loading the first one." << std::endl;
        }
        const dcmAPI::Patient::Pointer patient = dataSet->GetPatient( dataSet->GetPatientIds()->at(0) );
        // Study: take the first one
        if( patient->StudyIds()->size() > 1 )
        {
            std::cerr << "[dcmapi sort][WARNING] More than one study, loading the first one." << std::endl;
        }
        const dcmAPI::Study::Pointer study = patient->Study( patient->StudyIds()->at(0) );
    
        // vars
        std::string seriesId;
        dcmAPI::Series::Pointer series;
        std::string tmp;
        dcmAPI::TimePointIdVectorPtr timePointIdVector;
        size_t timePointSize;
    
        // Series of the study
        const dcmAPI::SeriesIdVectorPtr seriesIdVector = study->SeriesIds();
        const size_t seriesSize = seriesIdVector->size();
        // seriesNumber starts at 1
        unsigned int seriesNumber = 1;
        bool modeAll = false;
        if( seriesSize > 1 )
        {
            std::cout << "[dcmapi sort] Found multiple series:" << std::endl;
            for(unsigned int k=0; k < seriesSize; ++k)
            {
                seriesId = seriesIdVector->at(k);
                series = study->Series(seriesId);
            
                // tag info
                std::cout << "[dcmapi sort] [" << seriesId << "]";
                tmp = series->GetTagAsText(dcmAPI::tags::SeriesNumber);
                if( tmp.size() > 0 ) std::cout << " SeriesNumber: " << tmp;
                tmp = series->GetTagAsText(dcmAPI::tags::SeriesInstanceUID);
                if( tmp.size() > 0 ) std::cout << " SeriesInstanceUID: " << tmp;
                tmp = series->GetTagAsText(dcmAPI::tags::SeriesDescription);
                if( tmp.size() > 0 ) std::cout << " SeriesDescription: " << tmp;
            
                // size info
                timePointIdVector = series->TimePointIds();
                timePointSize = timePointIdVector->size();
            
                std::cout << " (";
                std::cout << timePointSize << " timepoint";
                if( timePointSize > 1 ) std::cout << "s";
            
                int min = 1e6;
                int max = 0;
                int sliceSize = 0;
                std::string timePointId;
                dcmAPI::TimePoint::Pointer timePoint;
                for( unsigned int i=0; i < timePointSize; ++i)
                {
                    timePointId = timePointIdVector->at(i);
                    timePoint = series->TimePoint(timePointId);
                    sliceSize = static_cast<int>( timePoint->SliceIds()->size() );
                    if( i == 0 ) 
                    {
                        max = sliceSize; 
                        min = sliceSize;
                    }
                    else if( sliceSize > max ) max = sliceSize;
                    else if( sliceSize < min ) min = sliceSize;
                }
                if( timePointSize > 1 ) std::cout << ", each";
                if( min == max )
                {
                    std::cout << " with " << min << " slice(s)";
                }
                else
                {   
                    std::cout << " with " << min << " to " << max << " slice(s)";
                }

                std::cout << ")";
                std::cout << std::endl;
            }
    
            std::cout << "[dcmapi sort] Enter the series number to load (0 for all): ";
            std::cin >> seriesNumber;
    
            if( seriesNumber > seriesSize )
            {
                throw std::runtime_error("[dcmapi sort][ERROR] Series number is not in allowed range.");
            }
            else if( seriesNumber == 0 )
            {
                modeAll = true;
            }
        }
    
        if( modeAll )
        {
            // Series of the study
            std::vector< std::vector<std::string> > subVector;
            for(unsigned int k=0; k < seriesIdVector->size(); ++k)
            {
                seriesId = seriesIdVector->at(k);
                series = study->Series(seriesId);
                subVector = GetSeriesFileList( series );
                for( unsigned int l=0; l < subVector.size(); ++l)
                {
                    dcmFileNames.push_back( subVector[l] );
                }
            }
        }
        else
        {
            // seriesNumber starts at 1
            seriesId = seriesIdVector->at(seriesNumber-1);
            series = study->Series(seriesId);
            dcmFileNames = GetSeriesFileList( series );
        }
    
        // return
        return dcmFileNames;
    }

    /**
    * Get the list of files to read from a series.
    * @param series The series representing the structure.
    * @return The file list.
    */
    std::vector< std::vector<std::string> > GetSeriesFileList(const dcmAPI::Series::Pointer& series)
    {
        // file name list
        std::vector< std::vector< std::string > > dcmFileNames;
    
        // vars
        std::string timePointId;
        dcmAPI::TimePoint::Pointer timePoint;
        dcmAPI::SliceIdVectorPtr sliceIdVector;
        std::string sliceId;
        dcmAPI::Slice::Pointer slice;
        std::string filePath;

        // TimePoints
        const dcmAPI::TimePointIdVectorPtr timePointIdVector = series->TimePointIds();
        for( unsigned int i=0; i < timePointIdVector->size(); ++i)
        {
            timePointId = timePointIdVector->at(i);
            timePoint = series->TimePoint(timePointId);
            // sub level list
            std::vector< std::string > list;
            // Slice
            sliceIdVector = timePoint->SliceIds();
            for( unsigned int j=0; j < sliceIdVector->size(); ++j)
            {
                sliceId = sliceIdVector->at(j);
                slice = timePoint->Slice(sliceId);
                filePath = slice->GetTagAsText(dcmAPI::tags::SliceFilePath);
                // only add if not allready present (could be multislice single file)
                if( list.size() == 0 || find( list.begin(), list.end(), filePath ) == list.end() )
                {
                    list.push_back( filePath );
                }
            }
            // add a level to the file list
            dcmFileNames.push_back(list);
        }
    
        // return
        return dcmFileNames;
    }

    /**
    * Print the data set structure on screen.
    * @param dataSet The data set representing the structure.
    * @return True if all went well.
    */
    bool ShowDataSetStructure(const dcmAPI::DataSet::Pointer& dataSet)
    {
        std::string patientId;
        std::string studyId;
        std::string seriesId;
        std::string timePointId;
        std::string sliceId;
        std::string text; 
    
        std::cerr << "[dcmapi sort] structure:" << std::endl;

        // Get the vector of patients Ids
        dcmAPI::PatientIdVectorPtr patientIdVector = dataSet->GetPatientIds();
        // Patients in the Dataset
        for(unsigned int i=0; i < patientIdVector->size(); ++i)
        {
            patientId = patientIdVector->at(i);
            dcmAPI::Patient::Pointer patient = dataSet->GetPatient(patientId);
            text = patient->GetTagAsText(dcmAPI::tags::PatientName);
            std::cout << "|_ patient name: " << text << std::endl;
        
            // Studies of the patient
            dcmAPI::StudyIdVectorPtr studiesIdVector = patient->StudyIds();
            for(unsigned int j=0; j < studiesIdVector->size(); ++j)
            {
                studyId = studiesIdVector->at(j);
                dcmAPI::Study::Pointer study = patient->Study(studyId);
                text = study->GetTagAsText(dcmAPI::tags::StudyInstanceUID);
                std::cout << "  |_ study UID: " << studyId << std::endl;
            
                // Series of the study
                dcmAPI::SeriesIdVectorPtr seriesIdVector = study->SeriesIds();
                for(unsigned int k=0; k < seriesIdVector->size(); ++k)
                {
                    seriesId = seriesIdVector->at(k);
                    dcmAPI::Series::Pointer series = study->Series(seriesId);
                    text = series->GetTagAsText(dcmAPI::tags::SeriesInstanceUID);
                    std::cout << "    |_  series UID: " << seriesId << std::endl;
                
                    // TimePoints in the series
                    dcmAPI::TimePointIdVectorPtr timePointIdVector = series->TimePointIds();
                    for(unsigned int p=0; p < timePointIdVector->size(); ++p)
                    {
                        timePointId = timePointIdVector->at(p);
                        dcmAPI::TimePoint::Pointer timePoint = series->TimePoint(timePointId);
                        std::cout << "      |_ time point: " << timePointId << std::endl;
                    
                        // Slices in the timepoint
                        dcmAPI::SliceIdVectorPtr sliceIdVector = timePoint->SliceIds();
                        for(unsigned int q=0; q < sliceIdVector->size(); ++q)
                        {
                            sliceId = sliceIdVector->at(q);
                            dcmAPI::Slice::Pointer slice = timePoint->Slice(sliceId);
                            text = slice->GetTagAsText(dcmAPI::tags::SliceFileName);
                            std::cout << "        |_ slice: " << text << std::endl;
                        }
                    }
                }
            }
        }
        return true;
    }

    // read option
    ReadOption m_option;
    // time tolerance
    float m_timeTolerance;


}; // class DcmapiDicomSorter

} // namespace dcmapi
} // namespace apps

#endif // DCMAPIDICOMSORTER_H
