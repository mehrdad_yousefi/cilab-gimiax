/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef GBLWXBRIDGELIB_H
#define GBLWXBRIDGELIB_H

/**
This file includes most of the headers files you need when working with the GuiBridgeLib and WxWidgets.

\author Maarten Nieber
\date 20 mar 2008
*/

#include "gblWxConnectorOfWidgetChangesToSlotFunction.h"
#include "gblWxTextCtrlValueProxy.h"
#include "gblWxRadioButtonValueProxy.h"
#include "gblWxRadioBoxValueProxy.h"
#include "gblWxToggleButtonValueProxy.h"
#include "gblWxListBoxValueProxy.h"
#include "gblWxComboBoxValueProxy.h"
#include "gblWxChoiceValueProxy.h"
#include "gblWxCheckBoxValueProxy.h"
#include "gblWxCheckableControlValueProxy.h"
#include "gblBridge.h"
#include "gblWxButtonEventProxy.h"
#include "gblWxChoiceBookValueProxy.h"
#include "gblWxValidate.h"
#include "gblWxSliderValueProxy.h"
#include "gblWxFloatSliderValueProxy.h"
#include "gblWxSliderWithTextValueProxy.h"


#endif //GBLWXBRIDGELIB_H
