/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "gblWxValidate.h"
#include "wx/wx.h"

bool gbl::wx::SetAppearance(wxWindow* widget, bool valid)
{
	widget->SetBackgroundColour(valid ? wxColour(200, 200, 200) : wxColour(255, 0, 0) );
	widget->Refresh();
	return valid;
}
