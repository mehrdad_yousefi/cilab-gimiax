/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "gblWxRadioButtonValueProxy.h"
#include "gblWxEventHandler.h"

template <>
void gbl::SetText( wxRadioButton* radioButton, const std::string& text )
{
	radioButton->SetValue( gbl::ValueConverter::TextToFlag(text) );
}

template <>
std::string gbl::GetText( wxRadioButton* radioButton )
{
	return gbl::ValueConverter::FlagToText( radioButton->GetValue() );
}
