/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "gblWxControlWithItemsValueProxy.h"
#include "boost/format.hpp"

#include <wxUnicode.h>

void gbl::wx::SetTextOfControlWithItems( wxControlWithItems* listBox, const std::string& text )
{
	size_t i = listBox->FindString(_U(text));
	if( i >= 0 && i < listBox->GetCount() )
	{
		listBox->SetSelection( i );
	}
	else
	{
		std::string message = (boost::format("Cannot find list box item %1%") % text).str();
		throw gbl::Exception(message.c_str());
	}
}

std::string gbl::wx::GetTextOfControlWithItems( wxControlWithItems* listBox )
{
	return _U(listBox->GetStringSelection());
}

double gbl::wx::GetNumberOfControlWithItems( wxControlWithItems* listBox )
{
	return listBox->GetSelection();
}

void gbl::wx::SetNumberOfControlWithItems( wxControlWithItems* listBox, int number)
{
	listBox->SetSelection(number);
}
