/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef GBLWXBUTTONEVENTPROXY_H
#define GBLWXBUTTONEVENTPROXY_H

#include "wx/button.h"
#include "gblBridge.h"

namespace gbl
{
	//! Convenience function that instantiates ButtonEventProxy.
	void AddBridgeEvent(gbl::Bridge bridge, const std::string& id, wxButton* button);
}

#endif //GBLWXBUTTONEVENTPROXY_H
