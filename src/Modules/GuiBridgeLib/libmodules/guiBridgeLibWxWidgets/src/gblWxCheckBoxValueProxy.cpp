/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "gblWxCheckBoxValueProxy.h"

template <>
void gbl::SetText( wxCheckBox* checkbox, const std::string& text )
{
	checkbox->SetValue( gbl::ValueConverter::TextToFlag(text) );
}

template <>
std::string gbl::GetText( wxCheckBox* checkbox )
{
	return gbl::ValueConverter::FlagToText( checkbox->GetValue() );
}
