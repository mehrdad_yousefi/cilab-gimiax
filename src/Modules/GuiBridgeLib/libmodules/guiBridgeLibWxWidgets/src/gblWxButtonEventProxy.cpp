/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "gblWxButtonEventProxy.h"
#include "gblWxEventProxy.h"

using namespace gbl::wx;

void gbl::AddBridgeEvent(gbl::Bridge bridge, const std::string& id, wxButton* button)
{
	new gbl::wx::EventProxy(bridge, id, button, wxEVT_COMMAND_BUTTON_CLICKED);
}
