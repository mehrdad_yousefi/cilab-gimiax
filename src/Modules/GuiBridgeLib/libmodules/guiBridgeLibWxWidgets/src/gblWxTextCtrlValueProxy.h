/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef GBLWXTEXTCTRLVALUEPROXY_H
#define GBLWXTEXTCTRLVALUEPROXY_H

#include "wx/wx.h"
#include "gblValueProxy.h"

namespace gbl
{
	//! Set the text field in a wxTextCtrl
	template <> void SetText(wxTextCtrl* _textCtrl, const std::string& _text);

	//! Set the text field in a wxTextCtrl
	template <> std::string GetText(wxTextCtrl* _textCtrl);
}

#endif //GBLWXTEXTCTRLVALUEPROXY_H
