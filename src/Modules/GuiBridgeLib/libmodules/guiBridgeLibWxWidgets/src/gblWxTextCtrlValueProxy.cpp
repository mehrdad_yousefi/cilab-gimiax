/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "gblWxTextCtrlValueProxy.h"

#include <wxUnicode.h>

template <> 
void gbl::SetText( wxTextCtrl* textCtrl, const std::string& text )
{
	wxString s(_U(text)); 
	if( textCtrl->GetValue() != s )
		textCtrl->SetValue( s );
}

template <> 
std::string gbl::GetText( wxTextCtrl* textCtrl )
{
	return _U(textCtrl->GetValue());
}
