/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "gblWxFloatSliderValueProxy.h"

#include <wxUnicode.h>

template <> 
void gbl::SetText( wxFloatSlider* slider, const std::string& text )
{
	double num = gbl::ValueConverter::TextToNumber(text);
	if( slider->GetValue() != num )
		slider->SetValue( num );
}

template <> 
std::string gbl::GetText( wxFloatSlider* slider )
{
	return gbl::ValueConverter::NumberToText(slider->GetValue());
}
