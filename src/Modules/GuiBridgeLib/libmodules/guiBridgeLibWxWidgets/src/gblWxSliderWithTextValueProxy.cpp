/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "gblWxSliderWithTextValueProxy.h"

#include <wxUnicode.h>

template <> 
void gbl::SetText( wxSliderWithTextCtrl* slider, const std::string& text )
{
	if( slider->GetValueAsString() != text )
		slider->SetValueAsString( text );
}

template <> 
std::string gbl::GetText( wxSliderWithTextCtrl* slider )
{
	return slider->GetValueAsString();
}
