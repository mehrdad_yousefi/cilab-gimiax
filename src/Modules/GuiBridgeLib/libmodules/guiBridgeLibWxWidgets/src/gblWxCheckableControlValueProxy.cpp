/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "gblWxCheckableControlValueProxy.h"

namespace gbl {

template <>
void SetText( wxCheckableControl* checkbox, const std::string& text )
{
	checkbox->SetValue( gbl::ValueConverter::TextToFlag(text) );
}

template <>
std::string GetText( wxCheckableControl* checkbox )
{
	return gbl::ValueConverter::FlagToText( checkbox->GetValue() );
}

}
