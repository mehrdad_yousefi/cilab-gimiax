/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "gblEventProxy.h"

gbl::EventProxy::EventProxy(Bridge _bridge, const std::string& _eventId)
: eventId(_eventId)
{
	this->bridge.ShallowCopyFrom(_bridge);
}

gbl::EventProxy::~EventProxy()
{

}

void gbl::EventProxy::CallEvent()
{
	this->bridge.CallEvent(this->eventId);
}
