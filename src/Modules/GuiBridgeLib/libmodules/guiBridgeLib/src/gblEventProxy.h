/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef GBLEVENTPROXY_H
#define GBLEVENTPROXY_H

#include "CILabNamespaceMacros.h"
#include "gblBridge.h"
#include <string>

CILAB_BEGIN_NAMESPACE(gbl)

/**
This is the base class for event proxies. It stores a pointer to the bridge, and the id of the bridge event that must be called.
\author Maarten Nieber
\date 2 Sep 2007
*/

class GUIBRIDGELIB_EXPORT EventProxy
{
public:
	//! Stores \a _bridge pointer and \a _eventId so that we can call this event in CallEvent.
	EventProxy(Bridge bridge, const std::string& eventId);
	//!
	virtual ~EventProxy();

	//! Executes the event in the bridge.
	void CallEvent();

private:
	//!
	Bridge bridge;
	//!
	std::string eventId;
};

CILAB_END_NAMESPACE(gbl)

#endif //GBLEVENTPROXY_H
