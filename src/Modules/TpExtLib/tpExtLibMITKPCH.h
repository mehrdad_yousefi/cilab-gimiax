/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef baseLibMITKPCH_h
#define baseLibMITKPCH_h

#include "CILabNamespaceMacros.h"
#include "boost/shared_ptr.hpp"
#include "itkPoint.h"
#include "mitkBoundingObject.h"
#include "mitkCommon.h"
#include "mitkDisplayPositionEvent.h"
#include "mitkGlobalInteraction.h"
#include "mitkInteractionConst.h"
#include "mitkInteractor.h"
#include "mitkPointOperation.h"
#include "mitkPointSet.h"
#include "mitkPointSetInteractor.h"
#include "mitkVtkPropRenderer.h"
#include <mitkAction.h>
#include <mitkBaseRenderer.h>
#include <mitkDataTreeNode.h>
#include <mitkInteractionConst.h>
#include <mitkOperationEvent.h>
#include <mitkPointOperation.h>
#include <mitkPointSet.h>
#include <mitkPositionEvent.h>
#include <mitkProperties.h>
#include <mitkRenderingManager.h>
#include <mitkStateEvent.h>
#include <mitkSurface.h>
#include <mitkUndoController.h>
#include <mitkSlicesRotator.h>
#include "TpExtLibMITKWin32Header.h"

#include <vtkPolyData.h>
#include "vtkOBBTree.h"

#endif // baseLibMITKPCH_h

