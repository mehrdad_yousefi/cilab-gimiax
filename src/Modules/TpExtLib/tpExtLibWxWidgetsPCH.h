/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef baseLibWxWidgetsPCH_h
#define baseLibWxWidgetsPCH_h

#include "TpExtLibWxWidgetsWin32Header.h"
#include <map>
#include <string>
#include "wx/wxprec.h"
#include <wx/defs.h>
#include <wx/wx.h>

#endif // baseLibWxWidgetsPCH_h

