/////////////////////////////////////////////////////////////////////////////
// Name:        webupdatedlg.cpp
// Purpose:     wxWebUpdateDlg, wxWebUpdateAdvPanel, wxWebUpdateAboutDlg
// Author:      Francesco Montorsi
// Created:     2005/06/23
// RCS-ID:      $Id: webupdatedlg.cpp,v 1.69 2005/11/07 08:18:49 frm Exp $
// Copyright:   (c) 2005 Francesco Montorsi
// Licence:     wxWidgets licence
/////////////////////////////////////////////////////////////////////////////



// For compilers that support precompilation, includes "wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

// includes
#ifndef WX_PRECOMP
    #include <wx/log.h>
    #include <wx/textctrl.h>
    #include <wx/checkbox.h>
    #include <wx/msgdlg.h>
    #include <wx/dirdlg.h>
    //#include <wx/generic/dirdlgg.h>
    #include <wx/stattext.h>
    #include <wx/gauge.h>
    #include <wx/textctrl.h>
    #include <wx/listctrl.h>
    #include <wx/progdlg.h>
    #include <wx/checkbox.h>
    #include <wx/xrc/xh_all.h>
#endif

// includes
#include "wx/webupdateaboutdlg.h"
#include "wx/webupdate.h"
#include "wx/installer.h"

#include "gimias.xpm"

// wxWidgets RTTI
IMPLEMENT_CLASS(wxWebUpdateAboutDlg, wxDialog)


BEGIN_EVENT_TABLE(wxWebUpdateAboutDlg, wxDialog)

    // link events will be added when wxWidgets will give a wxHyperlink control

END_EVENT_TABLE()



// ---------------------
// wxWEBUPDATEABOUTDLG
// ---------------------

wxWebUpdateAboutDlg::wxWebUpdateAboutDlg(wxWindow *parent)
	: wxWebUpdateAboutDlgUI(parent, wxID_ANY,"About")
{  
	// eventually set the version of the WebUpdater...
	m_lblVersion->SetLabel(wxWUAD_PREFIX + wxWebUpdateInstaller::Get()->GetVersion() + wxWUAD_POSTFIX);

	GetSizer()->SetSizeHints(this);

	m_bmpLogo->SetBitmap( wxBitmap(gimias_xpm) );
}

wxWebUpdateAboutDlg::~wxWebUpdateAboutDlg() 
{

}

