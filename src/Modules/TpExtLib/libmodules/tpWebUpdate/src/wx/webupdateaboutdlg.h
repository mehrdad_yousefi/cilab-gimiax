/////////////////////////////////////////////////////////////////////////////
// Name:        webupdatedlg.h
// Purpose:     wxWebUpdateDlg, wxWebUpdateAdvPanel, wxWebUpdateAboutDlg
// Author:      Francesco Montorsi
// Created:     2005/06/28
// RCS-ID:      $Id: webupdatedlg.h,v 1.46 2005/11/05 11:46:31 frm Exp $
// Copyright:   (c) 2005 Francesco Montorsi
// Licence:     wxWidgets licence
/////////////////////////////////////////////////////////////////////////////


#ifndef _WX_WEBUPDATEABOUTDLG_H_
#define _WX_WEBUPDATEABOUTDLG_H_

#include "wx/webupdatedef.h"
#include "wxWebUpdateAboutDlgUI.h"

//! The text prefix for the IDWUAD_VERSION.
#define wxWUAD_PREFIX					wxT("This is GIMIAS WebUpdater version ")
#define wxWUAD_POSTFIX					wxT("\nby Francesco Montorsi (c) 2005 and GIMIAS Team (2011)")


//! The about dialog shown by wxWebUpdateDlg.
class TPEXTLIBWXWEBUPDATE_EXPORT wxWebUpdateAboutDlg : public wxWebUpdateAboutDlgUI 
{
protected:

public:
	wxWebUpdateAboutDlg(wxWindow *parent);
	virtual ~wxWebUpdateAboutDlg();

private:
	DECLARE_CLASS(wxWebUpdateAboutDlg)
    wxDECLARE_EVENT_TABLE();
};

#endif // _WX_WEBUPDATEABOUTDLG_H_

