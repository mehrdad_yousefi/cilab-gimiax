/////////////////////////////////////////////////////////////////////////////
// Name:        webupdatedlg.h
// Purpose:     wxWebUpdateDlg, wxWebUpdateAdvPanel, wxWebUpdateAboutDlg
// Author:      Francesco Montorsi
// Created:     2005/06/28
// RCS-ID:      $Id: webupdatedlg.h,v 1.46 2005/11/05 11:46:31 frm Exp $
// Copyright:   (c) 2005 Francesco Montorsi
// Licence:     wxWidgets licence
/////////////////////////////////////////////////////////////////////////////


#ifndef _WX_WEBUPDATEADVPANEL_H_
#define _WX_WEBUPDATEADVPANEL_H_

#include "wxWebUpdateAdvPanelUI.h"

// wxWidgets headers
#include "wx/webupdate.h"
#include "wx/download.h"
#include "wx/webupdatectrl.h"
#include "wx/installer.h"
#include <wx/dialog.h>
#include <wx/panel.h>
#include <wx/checkbox.h>
#include <wx/textctrl.h>

// defined later
class wxStaticText;
class wxButton;
class wxTextCtrl;
class wxGauge;

//! The advanced panel of a wxWebUpdateDlg which contains all connection
//! settings options and some miscellaneous others.
class TPEXTLIBWXWEBUPDATE_EXPORT wxWebUpdateAdvPanel : public wxWebUpdateAdvPanelUI 
{
protected:		// pointers to our controls
	
#if wxUSE_HTTPENGINE
	wxProxySettings m_proxy;
	wxHTTPAuthSettings m_auth;
#endif

	//! The local XML script used to get some info.
	wxWebUpdateLocalXMLScript *m_xmlLocal;

	//!
	wxCheckBox *m_pSaveLog;

protected:

protected:		// event handlers

	void OnBrowse(wxCommandEvent &);
	void OnProxySettings(wxCommandEvent &);
	void OnAuthSettings(wxCommandEvent &);
	void OnAddUpdateSite(wxCommandEvent &);
	void OnRemoveUpdateSite(wxCommandEvent &);
	void OnRestart(wxCommandEvent &);
	void OnSaveLog(wxCommandEvent &);
	
public:

	//! Constructs a wxWebUpdateAdvPanel.
	wxWebUpdateAdvPanel(wxWindow* parent, wxWindowID id);

	virtual ~wxWebUpdateAdvPanel();


public:		// setters

 	void SetData(wxWebUpdateLocalXMLScript *script);

public:		// getters

#if wxUSE_HTTPENGINE
	//! Returns the updated proxy settings.
	wxProxySettings GetProxySettings() const
		{ return m_proxy; }

	//! Returns the updated HTTP authentication settings.
	wxHTTPAuthSettings GetHTTPAuthSettings() const
		{ return m_auth; }
#endif

	//! Returns the path chosen by the user for the downloaded file.
	//! This one is initialized to the temporary folder for the current user.
	wxString GetDownloadPath() const
		{ return m_pDownloadPathTextCtrl->GetValue(); } 

	//! Returns TRUE if the user has chosen to remove the downloaded files.
	bool RemoveFiles() const
		{ if (m_pRemoveFiles) return m_pRemoveFiles->GetValue(); return TRUE; }

	//! Returns TRUE if the user has chosen to restart the updated application
 	//! when exiting WebUpdater.
	bool Restart() const
		{ if (m_pRestart) return m_pRestart->GetValue(); return TRUE; }

	//!
	wxListBox* GetListUpdateSites() const { return m_pListUpdateSites; }
private:
	DECLARE_CLASS(wxWebUpdateAdvPanel)
    wxDECLARE_EVENT_TABLE();
};



#endif // _WX_WEBUPDATEADVPANEL_H_

