/////////////////////////////////////////////////////////////////////////////
// Name:        webupdatedlg.cpp
// Purpose:     wxWebUpdateDlg, wxWebUpdateAdvPanel, wxWebUpdateAboutDlg
// Author:      Francesco Montorsi
// Created:     2005/06/23
// RCS-ID:      $Id: webupdatedlg.cpp,v 1.69 2005/11/07 08:18:49 frm Exp $
// Copyright:   (c) 2005 Francesco Montorsi
// Licence:     wxWidgets licence
/////////////////////////////////////////////////////////////////////////////



// For compilers that support precompilation, includes "wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

// includes
#ifndef WX_PRECOMP
    #include <wx/log.h>
    #include <wx/textctrl.h>
    #include <wx/checkbox.h>
    #include <wx/msgdlg.h>
    #include <wx/dirdlg.h>
    //#include <wx/generic/dirdlgg.h>
    #include <wx/dirdlg.h>
    #include <wx/stattext.h>
    #include <wx/gauge.h>
    #include <wx/textctrl.h>
    #include <wx/listctrl.h>
    #include <wx/progdlg.h>
    #include <wx/checkbox.h>
    #include <wx/xrc/xh_all.h>
#endif

// includes
#include "wx/webupdateadvpanel.h"
#include "wx/installer.h"
#include <wx/wfstream.h>
#include <wx/xrc/xmlres.h>
#include <wx/image.h>
#include <wx/dialup.h>
#include <wx/tokenzr.h>
#include <wx/tokenzr.h>
#include <wx/settings.h>
#include "wx/wupdlock.h"


#if wxUSE_HTTPENGINE
    #include <wx/proxysettingsdlg.h>
    #include <wx/authdlg.h>
#endif


// wxWidgets RTTI
IMPLEMENT_CLASS(wxWebUpdateAdvPanel, wxPanel)

BEGIN_EVENT_TABLE(wxWebUpdateAdvPanel, wxPanel)

    // buttons
EVT_BUTTON(IDWUAP_BROWSE, wxWebUpdateAdvPanel::OnBrowse)
EVT_BUTTON(IDWUAP_PROXYSETTINGS, wxWebUpdateAdvPanel::OnProxySettings)
EVT_BUTTON(IDWUAP_AUTHSETTINGS, wxWebUpdateAdvPanel::OnAuthSettings)
EVT_BUTTON(IDWUAP_ADD_UPDATE_SITE, wxWebUpdateAdvPanel::OnAddUpdateSite)
EVT_BUTTON(IDWUAP_REMOVE_UPDATE_SITE, wxWebUpdateAdvPanel::OnRemoveUpdateSite)

// checkboxes
EVT_CHECKBOX(IDWUAP_RESTART, wxWebUpdateAdvPanel::OnRestart)
EVT_CHECKBOX(IDWUAP_SAVELOG, wxWebUpdateAdvPanel::OnSaveLog)

END_EVENT_TABLE()



// ---------------------
// wxWEBUPDATEADVPANEL
// ---------------------

//! Constructs a wxWebUpdateAdvPanel.
wxWebUpdateAdvPanel::wxWebUpdateAdvPanel(wxWindow* parent, wxWindowID id)
	: m_xmlLocal(NULL), wxWebUpdateAdvPanelUI( parent, id )
{ 

	m_pSaveLog = NULL;

#if !wxUSE_HTTPENGINE
	// if not using wxHttpEngine component, hide our ADV conn settings buttons
	m_btnProxySettings->Hide();
	m_btnAutSettings->Hide();
#endif

	if (m_pDownloadPathTextCtrl) {

		// get the temporary folder where we put by default the updates
		wxFileName str(wxFileName::CreateTempFileName(wxEmptyString, (wxFile *) NULL));
		str.SetFullName(wxEmptyString);     // remove the filename and keep only the path
		m_pDownloadPathTextCtrl->SetValue(str.GetLongPath());
	}

	// relayout
	// --------

	//GetSizer()->CalcMin();
	GetSizer()->Layout();
	GetSizer()->Fit(this);
	GetSizer()->SetSizeHints(this);

}

wxWebUpdateAdvPanel::~wxWebUpdateAdvPanel() 
{
}


void wxWebUpdateAdvPanel::SetData(wxWebUpdateLocalXMLScript *script)
{
    m_xmlLocal = script;
    if (m_pRestart) {

        // update the restart checkbox, if present
        m_pRestart->SetLabel(wxT("Restart ") + m_xmlLocal->GetAppName() +
                            wxT(" after the update is finished"));
        m_pRestart->SetValue(m_xmlLocal->IsAppToRestart());
    }

    if (m_pSaveLog) {

        // update the savelog checkbox, if present
        m_pSaveLog->SetValue(m_xmlLocal->IsLogToSave());
    }

	m_pListUpdateSites->Clear();
	for ( size_t i = 0 ; i < m_xmlLocal->GetRemoteScriptURICount( ) ; i++ )
	{
		m_pListUpdateSites->AppendString( m_xmlLocal->GetRemoteScriptURI( int( i ) ) );
	}
}

void wxWebUpdateAdvPanel::OnBrowse(wxCommandEvent &)
{
    // get the current value of the the "download path" from the textctrl
    wxString path = m_pDownloadPathTextCtrl->GetValue();
    wxDirDialog dlg(this, wxT("Choose a directory"),
                    path, wxDD_DEFAULT_STYLE | wxDD_NEW_DIR_BUTTON);
    if (dlg.ShowModal() == wxID_OK) {

        m_pDownloadPathTextCtrl->SetValue(dlg.GetPath());
        wxLogUsrMsg(wxT("wxWebUpdateDlg::OnBrowse - New output path is ") + dlg.GetPath());

        // save it also in our update installer
        wxWebUpdateInstaller::Get()->SetKeywordValue(wxT("downloaddir"), dlg.GetPath());

    } else {

        // just don't change nothing
    }
}

void wxWebUpdateAdvPanel::OnProxySettings(wxCommandEvent &)
{
#if wxUSE_HTTPENGINE
    wxProxySettingsDlg dlg(this, -1, wxT("WebUpdate proxy settings"));

    wxTopLevelWindow *tw = wxDynamicCast(GetParent(), wxTopLevelWindow);
    if (tw) dlg.SetIcon( tw->GetIcon() );

    dlg.CenterOnScreen();
    dlg.SetProxySettings(m_proxy);

    if (dlg.ShowModal() == wxID_OK)
        m_proxy = dlg.GetProxySettings();
#endif
}

void wxWebUpdateAdvPanel::OnAuthSettings(wxCommandEvent &)
{
#if wxUSE_HTTPENGINE
    wxAuthenticateDlg dlg(this, -1, wxT("WebUpdate authentication settings"));

    wxTopLevelWindow *tw = wxDynamicCast(GetParent(), wxTopLevelWindow);
    if (tw) dlg.SetIcon( tw->GetIcon() );

    dlg.CenterOnScreen();
    dlg.SetAuthSettings(m_auth);

    if (dlg.ShowModal() == wxID_OK)
        m_auth = dlg.GetAuthSettings();
#endif
}

void wxWebUpdateAdvPanel::OnRestart(wxCommandEvent &ev)
{
    // update the restart field
    m_xmlLocal->OverrideRestartFlag(ev.IsChecked());
}

void wxWebUpdateAdvPanel::OnSaveLog(wxCommandEvent &ev)
{
    // update the restart field
    m_xmlLocal->OverrideSaveLogFlag(ev.IsChecked());
}

void wxWebUpdateAdvPanel::OnAddUpdateSite( wxCommandEvent & )
{
	wxString siteURL;
	siteURL = ::wxGetTextFromUser( "Site URL", "Add Update Site", 
		"http://www.gimias.org/update/GIMIAS_1_5/UpdateServerScript.xml", this );
	m_pListUpdateSites->AppendString( siteURL );
}

void wxWebUpdateAdvPanel::OnRemoveUpdateSite( wxCommandEvent & )
{
	if ( m_pListUpdateSites->GetSelection() == -1 )
	{
		return;
	}

	m_pListUpdateSites->Delete( m_pListUpdateSites->GetSelection() );
}
