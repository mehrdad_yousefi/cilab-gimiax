// -*- C++ -*- generated by wxGlade 0.6.3 on Mon Jul 04 12:40:33 2011

#include "wxWebUpdateAboutDlgUI.h"

// begin wxGlade: ::extracode

// end wxGlade


wxWebUpdateAboutDlgUI::wxWebUpdateAboutDlgUI(wxWindow* parent, int id, const wxString& title, const wxPoint& pos, const wxSize& size, long style):
    wxDialog(parent, id, title, pos, size, wxDEFAULT_DIALOG_STYLE)
{
    // begin wxGlade: wxWebUpdateAboutDlgUI::wxWebUpdateAboutDlgUI
    sizer_15_staticbox = new wxStaticBox(this, -1, wxT("Links"));
    m_bmpLogo = new wxStaticBitmap(this, wxID_ANY, wxNullBitmap);
    m_lblVersion = new wxStaticText(this, wxID_ANY, wxT("XXXX"));
    IDWUAD_TEXT = new wxStaticText(this, wxID_ANY, wxT("WebUpdater is an open-source wxWidgets-licensed component hosted at wxCode.\nTo know more about Open Source, go at http://www.opensource.org.\n\nWebUpdater is based on wxWidgets and on the wxHTTPEngine component.\nThis program uses the Message Digest Algorithm (MD5) by RSA Data Security.\n\nWebUpdater has been extended with new features for GIMIAS Framework."));
    IDWUAD_TEXT2 = new wxStaticText(this, wxID_ANY, wxT("WebUpdater homepage:"));
    IDWUAD_WEBUPDATE_LINK = new wxStaticText(this, wxID_ANY, wxT("http://wxcode.sf.net/components/webupdate"));
    IDWUAD_TEXT3 = new wxStaticText(this, wxID_ANY, wxT("For reporting bugs about WebUpdater:"));
    IDWUAD_WEBUPDATEBUGS_LINK = new wxStaticText(this, wxID_ANY, wxT("http://sourceforge.net/projects/wxcode"));
    IDWUAD_TEXT4 = new wxStaticText(this, wxID_ANY, wxT("wxWidgets homepage:"));
    IDWUAD_WXWIDGETS_LINK = new wxStaticText(this, wxID_ANY, wxT("http://www.wxwidgets.org"));
    IDWUAD_TEXT5 = new wxStaticText(this, wxID_ANY, wxT("wxHTTPEngine homepage:"));
    IDWUAD_WXHTTPENGINE_LINK = new wxStaticText(this, wxID_ANY, wxT("http://wxcode.sf.net/components/wxhttpengine"));
    m_pCancelBtn = new wxButton(this, wxID_CANCEL, wxT("OK"));

    set_properties();
    do_layout();
    // end wxGlade
}


void wxWebUpdateAboutDlgUI::set_properties()
{
    // begin wxGlade: wxWebUpdateAboutDlgUI::set_properties
    SetTitle(wxT("About WebUpdater"));
    m_lblVersion->SetFont(wxFont(8, wxDEFAULT, wxNORMAL, wxBOLD, 0, wxT("")));
    m_pCancelBtn->SetDefault();
    // end wxGlade
}


void wxWebUpdateAboutDlgUI::do_layout()
{
    // begin wxGlade: wxWebUpdateAboutDlgUI::do_layout
    wxBoxSizer* sizer_12 = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* sizer_16 = new wxBoxSizer(wxHORIZONTAL);
    wxStaticBoxSizer* sizer_15 = new wxStaticBoxSizer(sizer_15_staticbox, wxVERTICAL);
    wxGridSizer* grid_sizer_1 = new wxGridSizer(4, 2, 0, 0);
    wxBoxSizer* sizer_13 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* sizer_14 = new wxBoxSizer(wxVERTICAL);
    sizer_13->Add(m_bmpLogo, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    sizer_14->Add(20, 20, 0, wxEXPAND, 0);
    sizer_14->Add(m_lblVersion, 1, wxEXPAND|wxALIGN_CENTER_VERTICAL, 5);
    sizer_14->Add(20, 20, 0, wxEXPAND, 0);
    sizer_14->Add(IDWUAD_TEXT, 0, 0, 5);
    sizer_13->Add(sizer_14, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    sizer_12->Add(sizer_13, 5, wxEXPAND|wxALIGN_CENTER_VERTICAL, 5);
    grid_sizer_1->Add(IDWUAD_TEXT2, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5);
    grid_sizer_1->Add(IDWUAD_WEBUPDATE_LINK, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5);
    grid_sizer_1->Add(IDWUAD_TEXT3, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5);
    grid_sizer_1->Add(IDWUAD_WEBUPDATEBUGS_LINK, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5);
    grid_sizer_1->Add(IDWUAD_TEXT4, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5);
    grid_sizer_1->Add(IDWUAD_WXWIDGETS_LINK, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5);
    grid_sizer_1->Add(IDWUAD_TEXT5, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5);
    grid_sizer_1->Add(IDWUAD_WXHTTPENGINE_LINK, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5);
    sizer_15->Add(grid_sizer_1, 1, wxEXPAND, 0);
    sizer_12->Add(sizer_15, 0, wxALL|wxEXPAND, 5);
    sizer_16->Add(20, 20, 3, wxEXPAND, 0);
    sizer_16->Add(m_pCancelBtn, 0, 0, 0);
    sizer_12->Add(sizer_16, 1, wxEXPAND, 0);
    SetSizer(sizer_12);
    sizer_12->Fit(this);
    Layout();
    // end wxGlade
}

