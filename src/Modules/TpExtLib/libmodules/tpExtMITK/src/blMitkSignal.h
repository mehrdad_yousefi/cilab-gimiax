/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _blMitkSignal_H
#define _blMitkSignal_H

#include "TpExtLibMITKWin32Header.h"
#include "mitkCommon.h"
#include "mitkBaseData.h"
#include "blSignalCollective.h"

namespace blMitk {

/**
 \brief Signal rendering data

 \ingroup blUtilitiesMITK
 \date 18 02 10
 \author Xavi Planes
 */
  class TPEXTLIBMITK_EXPORT Signal : public mitk::BaseData
  {
  protected:

  public:
    mitkClassMacro(Signal, mitk::BaseData);

    itkNewMacro(Self);

	virtual void SetSignal(blSignalCollective::Pointer signal);
	blSignalCollective::Pointer GetSignal() const;

	//!
	void SetRequestedRegionToLargestPossibleRegion();

	//!
	bool RequestedRegionIsOutsideOfTheBufferedRegion();

	//!
	virtual bool VerifyRequestedRegion();

	//!
	void SetRequestedRegion(itk::DataObject *data);

  protected:

    Signal();

    virtual ~Signal();

  protected:

    //!
	blSignalCollective::Pointer m_Signal;
  };

} // namespace mitk

#endif // _blMitkSignal_H
