/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "blMitkSurface.h"
#include "vtkPolyData.h"
#include "vtkPointData.h"

#include <cctype>

blMitk::Surface::Surface() : mitk::Surface()
{
	m_ScalarTimeSteps = false;
}

blMitk::Surface::~Surface()
{
}

void RemoveLastDigits( std::string &name )
{
	// Remove last digits
	while ( name.size() && std::isdigit( name[ name.size() - 1 ] ) )
	{
		name = name.substr( 0, name.size() - 1 );
	}
}

void blMitk::Surface::SetVtkPolyData( vtkPolyData* polydata, unsigned int t /*= 0*/ )
{
	size_t maxElements = 0;
	// Group the arrays by name, removing the last digits
	if ( polydata->GetPointData() != NULL )
	{
		// Group array names
		vtkPointData *pointData = polydata->GetPointData();
		for ( int i = 0 ; i < pointData->GetNumberOfArrays( ) ; i++ )
		{
			vtkDataArray *dataArray = pointData->GetArray( i );
			std::string arrayName( dataArray->GetName() );
			RemoveLastDigits( arrayName );
			m_NamesMap[ arrayName ].push_back( dataArray->GetName() );
		}

		// Get max elements of group
		std::string maxName;
		std::map< std::string, std::list<std::string> >::iterator it;
		for ( it = m_NamesMap.begin( ) ; it != m_NamesMap.end( ) ; it++ )
		{
			if ( it->second.size( ) > maxElements )
			{
				maxElements = it->second.size( );
				maxName = it->first;
			}
		}

		// If num > 1 -> Configure it
		if ( maxElements > 1 )
		{
			m_ScalarTimeSteps = true;
		}
		else
		{
			m_ScalarTimeSteps = false;
		}

	}

	
	if(m_ScalarTimeSteps)
	{
		// Set number of time steps to size of arrayNames
		// Set the same polyData to all time steps
		Expand( maxElements );

		for ( int i = 0 ; i < maxElements ; i++ )
		{
			Superclass::SetVtkPolyData( polydata, i );
		}

	}
	else
	{
		Superclass::SetVtkPolyData( polydata, t );
	}

}

vtkPolyData* blMitk::Surface::GetVtkPolyData( unsigned int t /*= 0*/ )
{
	vtkPolyData* polyData = Superclass::GetVtkPolyData( t );

	if ( m_ScalarTimeSteps && polyData && !m_NamesMap.empty( ) )
	{
		// Get selected group
		std::string scalarGroup;
		if ( polyData->GetPointData()->GetScalars( ) )
		{
			scalarGroup = polyData->GetPointData()->GetScalars( )->GetName( );
			RemoveLastDigits( scalarGroup );
		}
		else
		{
			scalarGroup = m_NamesMap.begin()->first;
		}

		// Set active scalars
		std::list<std::string>::iterator it = m_NamesMap[ scalarGroup ].begin( );
		std::advance(it, t);
		if ( it != m_NamesMap[ scalarGroup ].end( ) )
		{
			polyData->GetPointData()->SetActiveScalars( it->c_str() );
		}
	}

	return polyData;
}
