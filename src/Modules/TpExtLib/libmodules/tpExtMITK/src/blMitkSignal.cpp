/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "blMitkSignal.h"

blMitk::Signal::Signal() : mitk::BaseData()
{
	InitializeTimeSlicedGeometry( 1 );
}

blMitk::Signal::~Signal()
{
}

void blMitk::Signal::SetRequestedRegionToLargestPossibleRegion()
{

}

bool blMitk::Signal::RequestedRegionIsOutsideOfTheBufferedRegion()
{
	return false;
}

bool blMitk::Signal::VerifyRequestedRegion()
{
	return true;
}

void blMitk::Signal::SetRequestedRegion( itk::DataObject *data )
{

}

void blMitk::Signal::SetSignal( blSignalCollective::Pointer signal )
{
	m_Signal = signal;
}

blSignalCollective::Pointer blMitk::Signal::GetSignal() const
{
	return m_Signal;
}
