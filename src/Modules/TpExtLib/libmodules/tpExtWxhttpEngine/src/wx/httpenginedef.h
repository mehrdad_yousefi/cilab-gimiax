/////////////////////////////////////////////////////////////////////////////
// Name:        httpenginedef.h
// Purpose:     shared build defines
// Author:      Francesco Montorsi
// Created:     2005/07/26
// RCS-ID:      $Id: httpenginedef.h,v 1.1 2005/08/11 04:05:14 amandato Exp $
// Copyright:   (c) 2005 Francesco Montorsi and Angelo Mandato
// Licence:     wxWidgets licence
/////////////////////////////////////////////////////////////////////////////


#ifndef _WX_HTTPENGINE_DEFS_H_
#define _WX_HTTPENGINE_DEFS_H_

// Defines for shared builds.
// Simple reference for using these macros and for writin components
// which support shared builds:
//
// 1) use the TPEXTLIBWXHTTPENGINE_EXPORT in each class declaration:
//          class TPEXTLIBWXHTTPENGINE_EXPORT HTTPENGINEClass {   [...]   };
//
// 2) use the TPEXTLIBWXHTTPENGINE_EXPORT in the declaration of each global function:
//          TPEXTLIBWXHTTPENGINE_EXPORT int myGlobalFunc();
//
// 3) use the WXDLLIMPEXP_DATA_HTTPENGINE() in the declaration of each global
//    variable:
//          WXDLLIMPEXP_DATA_HTTPENGINE(int) myGlobalIntVar;
//
//#ifdef WXMAKINGDLL_HTTPENGINE
//    #define TPEXTLIBWXHTTPENGINE_EXPORT                  WXEXPORT
//    #define WXDLLIMPEXP_DATA_HTTPENGINE(type)       WXEXPORT type
//#elif defined(WXUSINGDLL)
//    #define TPEXTLIBWXHTTPENGINE_EXPORT                  WXIMPORT
//    #define WXDLLIMPEXP_DATA_HTTPENGINE(type)       WXIMPORT type
//#else // not making nor using DLL
//    #define TPEXTLIBWXHTTPENGINE_EXPORT
//    #define WXDLLIMPEXP_DATA_HTTPENGINE(type)	    type
//#endif
#include "tpExtLibWxhttpEngineWin32Header.h"
#define WXDLLIMPEXP_DATA_HTTPENGINE(type)	    type

#endif // _WX_HTTPENGINE_DEFS_H_

