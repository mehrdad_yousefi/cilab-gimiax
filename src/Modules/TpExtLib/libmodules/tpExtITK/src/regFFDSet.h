/*
** Setting BSpline transformation
**
*/

#ifndef   	REGFFDSET_H_
#define   	REGFFDSET_H_

#include "itkImageRegion.h"
#include "itkImage.h"
#include "itkBSplineDeformableTransform.h"


template <class ImageType, class TransformType> 
void computeBSplineGrid(typename ImageType::ConstPointer fixedImage, typename ImageType::RegionType fixedRegion, typename TransformType::SpacingType spacingOfControlPoints,
						typename TransformType::OriginType & gridOrigin,  typename TransformType::RegionType & bsplineRegion)  				
{
	// Definition of origin 
	fixedImage->TransformIndexToPhysicalPoint(fixedRegion.GetIndex(), gridOrigin);

	typedef typename TransformType::RegionType RegionType;

	typename RegionType::SizeType   gridSizeOnImage;
	typename RegionType::SizeType   gridHalfSizeOnImage;
	typename RegionType::SizeType   totalGridSize;

	typename ImageType::SizeType  fixedImageRegionSize = fixedRegion.GetSize();

	typedef typename TransformType::SpacingType SpacingType;
	SpacingType imageSpacing = fixedImage->GetSpacing();

	for(unsigned int r=0; r<fixedImage->GetImageDimension(); ++r)
	{
		gridHalfSizeOnImage[r] = static_cast<typename ImageType::SizeType::SizeValueType> ( 
			floor( (static_cast<double>(fixedImageRegionSize[r])/2)
			* imageSpacing[r] / spacingOfControlPoints[r] ) );
		gridSizeOnImage[r] = gridHalfSizeOnImage[r]*2 + 1;
		gridOrigin[r] += fixedImageRegionSize[r]*imageSpacing[r]/2 - (gridHalfSizeOnImage[r] + 2) * spacingOfControlPoints[r] ;
	}

	// Adding border control points
	typename RegionType::SizeType   gridBorderSize;
	gridBorderSize.Fill( 4 );						// 2 control points before and 2 after
	totalGridSize = gridSizeOnImage + gridBorderSize;
	bsplineRegion.SetSize( totalGridSize );
}



#endif /* !REGFFDSET_H_ */
