#ifndef   	REGIMAGECOMMON_H_
# define   	REGIMAGECOMMON_H_

#include "itkImage.h"
#include "itkNumericTraits.h"
#include "itkImageRegionIterator.h"

#include "itkGrayscaleDilateImageFilter.h"
#include "itkGrayscaleErodeImageFilter.h"
#include "itkBinaryBallStructuringElement.h" 


#include "regPostprocessing.h"

//////////////////////////////////////////////////////////////////////////

// Available functions:
// ---------------------
// CopyImage
// CropImage
// ComputeNormImage
// LocalOperationsImages
// ScalarMultiplyImage
// RemoveMaskedValuesOnImage

//////////////////////////////////////////////////////////////////////////

template <class TImageType> 
void CopyImage(	TImageType* input_Image, TImageType* output_Image  )
{
	typedef itk::ImageRegionIterator<TImageType> TIteratorType;

	TIteratorType it_input_Image(input_Image,input_Image->GetBufferedRegion());
	TIteratorType it_output_Image(output_Image,output_Image->GetBufferedRegion());
	for ( it_input_Image.GoToBegin(), it_output_Image.GoToBegin(); 
		! ( it_input_Image.IsAtEnd() || it_output_Image.IsAtEnd() ); 
		++it_input_Image, ++it_output_Image)
	{
		it_output_Image.Set(it_input_Image.Get());
	}
}

//////////////////////////////////////////////////////////////////////////

template <class ImageType> 
typename ImageType::Pointer 
CropImage(	ImageType* VF, int start[], int size[] )
{
	// CAUTION: TO BE USED CAREFULLY
	typedef typename ImageType::PixelType  PixelType;
	unsigned int ImageDimension = VF->GetImageDimension();

	//////////////////////////////////////////////////////////////////////////

	typedef typename ImageType::RegionType RegionType;
	typedef typename ImageType::PointType OriginType;
	RegionType VF_region;
	OriginType VF_origin = VF->GetOrigin();
	typename RegionType::SizeType VF_region_size = VF->GetBufferedRegion().GetSize();
	for (unsigned int dim=0;dim<ImageDimension;dim++)
	{
		VF_region_size[dim] = size[dim];
		VF_origin[dim] += start[dim] * (VF->GetSpacing()[dim]);
	}
	VF_region.SetSize(VF_region_size);

	// output image
	typename ImageType::Pointer cropped_VF = ImageType::New();
	cropped_VF->SetRegions(VF_region);
	cropped_VF->SetSpacing(VF->GetSpacing());
	cropped_VF->SetOrigin(VF_origin);
	cropped_VF->SetDirection(VF->GetDirection());
	cropped_VF->Allocate();
	cropped_VF->FillBuffer(itk::NumericTraits<PixelType>::ZeroValue());

	// iterator on output def field
	typedef itk::ImageRegionIterator<ImageType> IteratorType;
	IteratorType it_cropped_VF(cropped_VF,cropped_VF->GetBufferedRegion());
	IteratorType it_VF(VF,VF->GetBufferedRegion());

	if ((size[0]>=VF->GetBufferedRegion().GetSize()[0]))	// Image Extension
	{
		for ( it_VF.GoToBegin(); 
			! ( it_VF.IsAtEnd() ); 
			++it_VF )
		{
			typename ImageType::IndexType index_cropped_VF;
			typename ImageType::IndexType index_VF = it_VF.GetIndex();
			for (unsigned int dim=0;dim<ImageDimension;dim++)
			{
				index_cropped_VF[dim] = index_VF[dim] - start[dim];
			}
			it_cropped_VF.SetIndex(index_cropped_VF);
			it_cropped_VF.Set(it_VF.Get());
		}
	}
	else	// Image Cropping
	{
		for ( it_cropped_VF.GoToBegin(); 
			! ( it_cropped_VF.IsAtEnd() ); 
			++it_cropped_VF )
		{
			typename ImageType::IndexType index_cropped_VF = it_cropped_VF.GetIndex();
			typename ImageType::IndexType index_VF;
			for (unsigned int dim=0;dim<ImageDimension;dim++)
			{
				index_VF[dim] = index_cropped_VF[dim] + start[dim];
			}
			it_VF.SetIndex(index_VF);
			it_cropped_VF.Set(it_VF.Get());
		}
	}

	return cropped_VF;
}

//////////////////////////////////////////////////////////////////////////

template <class ImageType> 
float ComputeNormImage(	ImageType* V,
					   ImageType* V_mask)
{
	typedef typename ImageType::PixelType  PixelType;
	unsigned int ImageDimension = V->GetImageDimension();

	// iterator on output def field
	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	ImageIteratorType it_V_mask(V_mask,V_mask->GetBufferedRegion());
	ImageIteratorType it_V(V,V->GetBufferedRegion());
	// filling in the deformation field

	float normValue = 0;
	int total_mask = 0;

	for ( it_V.GoToBegin(), it_V_mask.GoToBegin();
		! ( it_V.IsAtEnd() || it_V_mask.IsAtEnd() ); 
		++it_V, ++it_V_mask)
	{

		PixelType Point_V = it_V.Get();

		if ( (it_V_mask.Get() == 1.) )
		{
			normValue += Point_V*Point_V;     
			total_mask++;
		}
	}

	normValue /= (double)total_mask;
	normValue = sqrt(normValue);

	return normValue;
}

//////////////////////////////////////////////////////////////////////////

template <class ImageType> 
int LocalOperationsImages(	ImageType* Image_1,
						  ImageType* Image_2,
						  ImageType* diffImage,
						  ImageType* Image_1_mask,
						  ImageType* Image_2_mask,
						  ImageType* diffImage_mask,
						  int operationIndex)
{
	//////////////////////////////////////////////////////////////////////////
	// Operation index:
	// 1 = addition
	// 2 = substraction
	// 3 = multiplication
	// 4 = division
	//////////////////////////////////////////////////////////////////////////

	typedef typename ImageType::PixelType  PixelType;
	unsigned int ImageDimension = Image_1->GetImageDimension();


	// iterator on output def field
	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	ImageIteratorType it_Image_1_mask(Image_1_mask,Image_1_mask->GetBufferedRegion());
	ImageIteratorType it_Image_2_mask(Image_2_mask,Image_2_mask->GetBufferedRegion());
	ImageIteratorType it_diffImage_mask(diffImage_mask,diffImage_mask->GetBufferedRegion());
	typedef itk::ImageRegionIterator<ImageType> IteratorType;
	IteratorType it_diff(diffImage,diffImage->GetBufferedRegion());
	IteratorType it_Image_1(Image_1,Image_1->GetBufferedRegion());
	IteratorType it_Image_2(Image_2,Image_2->GetBufferedRegion());

	// filling in the deformation field

	for ( it_Image_1.GoToBegin(), it_Image_2.GoToBegin(), it_diff.GoToBegin(),
		it_Image_1_mask.GoToBegin(), it_Image_2_mask.GoToBegin(), it_diffImage_mask.GoToBegin(); 
		! ( it_Image_1.IsAtEnd() || it_Image_2.IsAtEnd() || it_diff.IsAtEnd()
		|| it_Image_1_mask.IsAtEnd() || it_Image_2_mask.IsAtEnd() || it_diffImage_mask.IsAtEnd()); 
	++it_Image_1, ++it_Image_2, ++it_diff
		, ++it_Image_1_mask, ++it_Image_2_mask, ++it_diffImage_mask)
	{

		PixelType inputPoint_1 = it_Image_1.Get();
		PixelType inputPoint_2 = it_Image_2.Get();
		PixelType outputPoint;

		if ( (it_Image_1_mask.Get() == 1) &&
			(it_Image_2_mask.Get() == 1) )
		{
			if (operationIndex == 1)
			{
				outputPoint = inputPoint_1 + inputPoint_2;     
			} 
			else if(operationIndex == 2)
			{
				outputPoint = inputPoint_1 - inputPoint_2;     
			}
			else if(operationIndex == 3)
			{
				outputPoint = inputPoint_1 * inputPoint_2;     
			}
			else if(operationIndex == 4)
			{
				if (inputPoint_2 != 0)
				{
					outputPoint = PixelType( inputPoint_1 / inputPoint_2 );
				}
				else
				{
					outputPoint = itk::NumericTraits<PixelType>::NonpositiveMin();
					it_diffImage_mask.Set(0);
				}	
			}
		} 
		else
		{
			outputPoint = itk::NumericTraits<PixelType>::NonpositiveMin();
			it_diffImage_mask.Set(0);
		}

		it_diff.Set(outputPoint);
	}

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class ImageType> 
typename ImageType::Pointer 
ScalarMultiplyImage(	ImageType* inputImage , double scalar, ImageType* inputImage_mask, ImageType* outputImage_mask)
{
	typedef typename ImageType::PixelType  PixelType;
	unsigned int ImageDimension = inputImage->GetImageDimension();



	typename ImageType::Pointer scaledImage = ImageType::New();
	scaledImage->SetRegions(inputImage->GetBufferedRegion());
	scaledImage->SetSpacing(inputImage->GetSpacing());
	scaledImage->SetOrigin(inputImage->GetOrigin());
	scaledImage->SetDirection(inputImage->GetDirection());
	scaledImage->Allocate();
	scaledImage->FillBuffer(itk::NumericTraits<PixelType>::ZeroValue());

	// iterator on output def field
	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	ImageIteratorType it_inputImage_mask(inputImage_mask,inputImage_mask->GetBufferedRegion());
	ImageIteratorType it_outputImage_mask(outputImage_mask,outputImage_mask->GetBufferedRegion());
	typedef itk::ImageRegionIterator<ImageType> IteratorType;
	IteratorType it_scaled(scaledImage,scaledImage->GetBufferedRegion());
	IteratorType it_Image(inputImage,inputImage->GetBufferedRegion());

	// filling in the deformation field

	for ( it_Image.GoToBegin(), it_scaled.GoToBegin(), it_inputImage_mask.GoToBegin(), it_outputImage_mask.GoToBegin(); 
		! ( it_Image.IsAtEnd() || it_scaled.IsAtEnd() || it_inputImage_mask.IsAtEnd() || it_outputImage_mask.IsAtEnd() ); 
		++it_Image, ++it_scaled, ++it_inputImage_mask, ++it_outputImage_mask)
	{

		PixelType inputPoint = it_Image.Get();
		PixelType outputPoint;

		if (it_inputImage_mask.Get() == 1)
		{
			outputPoint = inputPoint * scalar;     
		} 
		else
		{
			outputPoint = itk::NumericTraits<PixelType>::NonpositiveMin();
			it_outputImage_mask.Set(0);
		}
		it_scaled.Set(outputPoint);
	}

	return scaledImage;
}

//////////////////////////////////////////////////////////////////////////

template <class ImageType> 
int RemoveMaskedValuesOnImage(	ImageType* inputImage, ImageType* CleanedImage, ImageType* inputImage_mask )
{
	typedef typename ImageType::PixelType  PixelType;
	unsigned int ImageDimension = inputImage->GetImageDimension();

	// iterator on output def field
	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	ImageIteratorType it_inputImage_mask(inputImage_mask,inputImage_mask->GetBufferedRegion());
	typedef itk::ImageRegionIterator<ImageType> IteratorType;
	IteratorType it_Cleaned(CleanedImage,CleanedImage->GetBufferedRegion());
	IteratorType it_Image(inputImage,inputImage->GetBufferedRegion());

	// filling in the deformation field

	for ( it_Image.GoToBegin(), it_Cleaned.GoToBegin(), it_inputImage_mask.GoToBegin(); 
		! ( it_Image.IsAtEnd() || it_Cleaned.IsAtEnd() || it_inputImage_mask.IsAtEnd() ); 
		++it_Image, ++it_Cleaned, ++it_inputImage_mask )
	{

		PixelType inputPoint = it_Image.Get();
		PixelType outputPoint;

		if (it_inputImage_mask.Get() != 1)
		{
			outputPoint = itk::NumericTraits<PixelType>::ZeroValue();
		} 
		else
		{
			outputPoint = inputPoint;     
		}

		it_Cleaned.Set(outputPoint);
	}

	return EXIT_SUCCESS;
}









#endif /* !REGIMAGECOMMON_H_ */
