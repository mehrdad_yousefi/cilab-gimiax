#ifndef   	REGPREPROCESSING_H_
# define   	REGPREPROCESSING_H_

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkCastImageFilter.h"
#include "itkSmoothingRecursiveGaussianImageFilter.h"
#include "itkThresholdImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkNumericTraits.h"
#include "itkExtractImageFilter.h"

//////////////////////////////////////////////////////////////////////////

// Available functions:
// ---------------------
// SaveImageToBMP
// Create_Mask
// Mask_Apply
// Crop_Image
// SmoothImage
// ThresholdImage

//////////////////////////////////////////////////////////////////////////

template <class ImageType> 
int SaveImageToBMP(const ImageType* inputImage, const char* inputImageFileName)
{
	typedef  unsigned char           BMPPixelType;
	typedef itk::Image< BMPPixelType,
		::itk::GetImageDimension<ImageType>::ImageDimension >  BMPImageType;
	unsigned int ImageDimension = inputImage->GetImageDimension();


	typedef itk::CastImageFilter< ImageType, BMPImageType > BMPCastFilterType;
	typename BMPCastFilterType::Pointer BMPCastfilter  = BMPCastFilterType::New();


	BMPCastfilter ->SetInput( inputImage );
	BMPCastfilter ->Update();

	std::ostringstream BMP_OsFileName;
	std::string BMP_StringFileName;
	BMP_OsFileName << inputImageFileName;
	BMP_StringFileName = BMP_OsFileName.str();
	BMP_StringFileName = BMP_StringFileName.substr(0,BMP_StringFileName.length() - 4);
	BMP_OsFileName.str("");
	BMP_OsFileName << BMP_StringFileName << ".bmp";
	BMP_StringFileName = BMP_OsFileName.str();

// 	std::cout << "Writing BMP image...   " << std::endl;

	typedef itk::ImageFileWriter< BMPImageType >  BMPWriterType;
	typename BMPWriterType::Pointer      BMPWriter =  BMPWriterType::New();
	BMPWriter->SetFileName(BMP_StringFileName);
	BMPWriter->SetInput( BMPCastfilter->GetOutput() );

	try
	{
		BMPWriter->Update();
	}
	catch( itk::ExceptionObject & err ) 
	{ 
		std::cerr << "ExceptionObject caught !" << std::endl; 
		std::cerr << err << std::endl; 
		return EXIT_FAILURE;
	} 
	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class ImageType> 
int Create_Mask(	ImageType* maskImage,
				int mask_coordinates[]  )
{
	typedef typename ImageType::PixelType  PixelType;
	unsigned int ImageDimension = maskImage->GetImageDimension();

	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;

	int point_T_int[2] = {mask_coordinates[0],mask_coordinates[1]};
	int point_L_int[2] = {mask_coordinates[2],mask_coordinates[3]};
	int point_R_int[2] = {mask_coordinates[4],mask_coordinates[5]};

	//////////////////////////////////////////////////////////////////////////

	// Computing mask region
	double point_T[2];
	double point_L[2];
	double point_R[2];
	for (int r=0;r<2;r++)
	{
		point_T[r] = (double)point_T_int[r];
		point_L[r] = (double)point_L_int[r];
		point_R[r] = (double)point_R_int[r];
	}

	double slope_L[2];
	slope_L[0] = (point_L[1] - point_T[1]) / (point_L[0] - point_T[0]);
	slope_L[1] = point_L[1] - slope_L[0] * point_L[0];

	double slope_R[2];
	slope_R[0] = (point_R[1] - point_T[1]) / (point_R[0] - point_T[0]);
	slope_R[1] = point_R[1] - slope_R[0] * point_R[0];

	double R2 = (point_R[0] - point_T[0])*(point_R[0] - point_T[0]) + (point_R[1] - point_T[1])*(point_R[1] - point_T[1]);

	//////////////////////////////////////////////////////////////////////////

	// Allocating pixel data

	ImageIteratorType it_maskImage(maskImage,maskImage->GetBufferedRegion());

	for ( it_maskImage.GoToBegin();
		! ( it_maskImage.IsAtEnd() ); 
		++it_maskImage)
	{
		typename ImageType::IndexType index_maskImage = it_maskImage.GetIndex();

		if ( (index_maskImage[1] < (slope_L[0]*index_maskImage[0] + slope_L[1])) 
			|| (index_maskImage[1] < (slope_R[0]*index_maskImage[0] + slope_R[1])) 
			|| 	( (index_maskImage[1] - point_T[1])*(index_maskImage[1] - point_T[1]) + (index_maskImage[0] - point_T[0])*(index_maskImage[0] - point_T[0]) > R2) )
		{
			it_maskImage.Set( 0 );
		}
	}

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class ImageType> 
int Mask_Apply(	std::string inputImageFileName, std::string maskImageFileName, std::string outputImageFileName )
{
	typedef typename ImageType::PixelType PixelType;

	typedef itk::ImageFileReader< ImageType >	ImageReaderType;
	typename ImageReaderType::Pointer maskImageReader	 =	ImageReaderType::New();
	typename ImageReaderType::Pointer inputImageReader	 =	ImageReaderType::New();

	inputImageReader->SetFileName(inputImageFileName);
	try{
		inputImageReader->Update();
	} catch( itk::ExceptionObject & err ) {
		std::cerr << "ExceptionObject caught !" << std::endl;
		std::cerr << err << std::endl;
		return EXIT_FAILURE;
	}
	typename ImageType::Pointer inputImage = inputImageReader->GetOutput();


	maskImageReader->SetFileName(maskImageFileName);
	try{
		maskImageReader->Update();
	} catch( itk::ExceptionObject & err ) {
		std::cerr << "ExceptionObject caught !" << std::endl;
		std::cerr << err << std::endl;
		return EXIT_FAILURE;
	}
	typename ImageType::Pointer maskImage = maskImageReader->GetOutput();


	typename ImageType::IndexType pixelIndex;
	typename ImageType::SizeType size = inputImage->GetLargestPossibleRegion().GetSize();

	typename ImageType::Pointer outputImage = ImageType::New();
	outputImage->SetRegions(inputImage->GetBufferedRegion());
	outputImage->SetSpacing(inputImage->GetSpacing());
	outputImage->SetOrigin(inputImage->GetOrigin());
	outputImage->SetDirection(inputImage->GetDirection());
	outputImage->Allocate();
	outputImage->FillBuffer(itk::NumericTraits<PixelType>::ZeroValue());

	for (int ix=0;ix<size[0];ix++)
	{
		for (int iy=0;iy<size[1];iy++)
		{
			pixelIndex[0] = ix;
			pixelIndex[1] = iy;
			if (maskImage->GetPixel(pixelIndex) == 0)
			{
				outputImage->SetPixel( pixelIndex , itk::NumericTraits<PixelType>::ZeroValue() );
			}
			else
			{
				outputImage->SetPixel( pixelIndex , inputImage->GetPixel(pixelIndex) );
			}
		}
	}

	typedef itk::ImageFileWriter< ImageType >	WriterType;
	typename WriterType::Pointer writer = WriterType::New();
	writer->SetFileName( outputImageFileName );
	writer->SetInput( outputImage );
	try{
		writer->Update();
	} catch( itk::ExceptionObject & err ) {
		std::cerr << "ExceptionObject caught !" << std::endl;
		std::cerr << err << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class ImageType> 
int Crop_Image(	std::string inputImageFileName, typename ImageType::IndexType index, typename ImageType::SizeType size, std::string outputImageFileName )
{
	typedef itk::ImageFileReader< ImageType >	ImageReaderType;
	typename ImageReaderType::Pointer inputImageReader	 =	ImageReaderType::New();

	inputImageReader->SetFileName(inputImageFileName);
	try{
		inputImageReader->Update();
	} catch( itk::ExceptionObject & err ) {
		std::cerr << "ExceptionObject caught !" << std::endl;
		std::cerr << err << std::endl;
		return EXIT_FAILURE;
	}
	typename ImageType::Pointer inputImage = inputImageReader->GetOutput();


	typename ImageType::RegionType desiredRegion;
	desiredRegion.SetSize( size );
	desiredRegion.SetIndex( index );

	typedef itk::ExtractImageFilter< ImageType, ImageType > ExtractImageFilterType;
	typename ExtractImageFilterType::Pointer extractImageFilter = ExtractImageFilterType::New();
	extractImageFilter->SetExtractionRegion( desiredRegion );
	extractImageFilter->SetInput( inputImage );

	typedef itk::ImageFileWriter< ImageType >  WriterType;
	typename WriterType::Pointer writer = WriterType::New();
	writer->SetInput( extractImageFilter->GetOutput() );  
	writer->SetFileName( outputImageFileName );
	try{
		writer->Update();
	} catch( itk::ExceptionObject & err ) {
		std::cerr << "ExceptionObject caught !" << std::endl;
		std::cerr << err << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class ImageType> 
typename ImageType::Pointer SmoothImage(const ImageType* inputImage, float sigma_smooth, bool writeOutputFileSpecified, std::string outputImageFileName = "")
{
	typedef typename ImageType::PixelType PixelType;
	typedef itk::SmoothingRecursiveGaussianImageFilter<ImageType,ImageType>   SmootherType;
	typename SmootherType::Pointer  smoother = SmootherType::New();

	smoother->SetInput(  inputImage );
	smoother->SetSigma( sigma_smooth );
	smoother->Update();

	if (writeOutputFileSpecified)
	{
		std::cout << "Writing smoothed Image..." << std::endl;
		typedef itk::ImageFileWriter< ImageType >   SmoothWriterType;
		typename SmoothWriterType::Pointer      smoothWriter =  SmoothWriterType::New();
		smoothWriter->SetFileName(  outputImageFileName );
		smoothWriter->SetInput(  smoother->GetOutput()   );
		try
		{
			smoothWriter->Update();
		}
		catch( itk::ExceptionObject & err ) 
		{ 
			std::cerr << "ExceptionObject caught !" << std::endl; 
			std::cerr << err << std::endl; 
		} 
	}

	typename ImageType::Pointer outputImage = ImageType::New();
	outputImage->SetRegions(inputImage->GetBufferedRegion());
	outputImage->SetSpacing(inputImage->GetSpacing());
	outputImage->SetOrigin(inputImage->GetOrigin());
	outputImage->SetDirection(inputImage->GetDirection());
	outputImage = smoother->GetOutput();

	return outputImage;
}

//////////////////////////////////////////////////////////////////////////

template <class ImageType> 
typename ImageType::Pointer ThresholdImage(const ImageType* inputImage, float lowThresh, bool writeOutputFileSpecified, std::string outputImageFileName = "")
{
	typedef typename ImageType::PixelType PixelType;
	typedef itk::MinimumMaximumImageCalculator<ImageType>  MinMaxImageCalculatorType;
	typename MinMaxImageCalculatorType::Pointer  minMaxCalculator  = MinMaxImageCalculatorType::New();

	typedef itk::ThresholdImageFilter<ImageType>  ThresholdImageFilterType;
	typename ThresholdImageFilterType::Pointer thresholdImageFilter = ThresholdImageFilterType::New();

	minMaxCalculator->SetImage(inputImage);
	PixelType max_pixel;
	PixelType min_pixel;
	minMaxCalculator->ComputeMaximum();
	minMaxCalculator->ComputeMinimum();
	max_pixel = minMaxCalculator->GetMaximum();
	min_pixel = minMaxCalculator->GetMinimum();

	PixelType  lowThresh_pixel  = min_pixel  + (max_pixel  - min_pixel)  / lowThresh;

	std::cout << "Image Range:   [ " << min_pixel << " , " << max_pixel << " ]" << std::endl;
	std::cout << "Setting LowThresh to:   " << lowThresh_pixel << std::endl;

	thresholdImageFilter->SetInput(inputImage);
	thresholdImageFilter->SetOutsideValue(0);
	thresholdImageFilter->ThresholdOutside(lowThresh_pixel,itk::NumericTraits<PixelType>::max());
	thresholdImageFilter->Update();

	if (writeOutputFileSpecified)
	{
		std::cout << "Writing thresholded Image..." << std::endl;
		typedef itk::ImageFileWriter< ImageType >   ThresholdWriterType;
		typename ThresholdWriterType::Pointer   thresholdWriter  =  ThresholdWriterType::New();

		thresholdWriter->SetFileName( outputImageFileName );
		thresholdWriter->SetInput( thresholdImageFilter->GetOutput()   );
		try
		{
			thresholdWriter->Update();
		}
		catch( itk::ExceptionObject & err ) 
		{ 
			std::cerr << "ExceptionObject caught !" << std::endl; 
			std::cerr << err << std::endl; 
		} 
	}

	typename ImageType::Pointer outputImage = ImageType::New();
	outputImage->SetRegions(inputImage->GetBufferedRegion());
	outputImage->SetSpacing(inputImage->GetSpacing());
	outputImage->SetOrigin(inputImage->GetOrigin());
	outputImage->SetDirection(inputImage->GetDirection());
	outputImage->Allocate();
	outputImage = thresholdImageFilter->GetOutput();

	return outputImage;
}


#endif /* !REGPREPROCESSING_H_ */
