#ifndef   	REGVECTORCOMMON_H_
# define   	REGVECTORCOMMON_H_

#include "itkImage.h"
#include "itkNumericTraits.h"
#include "itkImageRegionIterator.h"
#include "itkVectorLinearInterpolateImageFunction.h"
#include "itkLinearInterpolateImageFunction.h"

#include "itkGrayscaleDilateImageFilter.h"
#include "itkGrayscaleErodeImageFilter.h"
#include "itkBinaryBallStructuringElement.h" 


#include "regPostprocessing.h"

//////////////////////////////////////////////////////////////////////////

// Available functions:
// ---------------------
// CropVectorFieldImage
// DownSampleVectorFieldImage
// RemoveMaskedValues
// LocalOperationsVectorFields
// MultiplyVectorFieldByScalarImage
// ReSampleVectorFieldImage
// ComputeNormVectorField
// ScalarMultiplyVectorField
// ComposeVectorFields
// ComputeJacobianVectorField
// ComposeVectorFieldWithJacobianTranspose
// MultiplyJacobianByVectorField
// ComputeAbsDetJacobian

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType> 
typename VectorFieldImageType::Pointer 
CropVectorFieldImage(	VectorFieldImageType* VF, int start[], int size[] )
{
	// CAUTION: TO BE USED CAREFULLY
	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	unsigned int ImageDimension = VF->GetImageDimension();

	//////////////////////////////////////////////////////////////////////////

	typedef typename VectorFieldImageType::RegionType VectorFieldRegionType;
	typedef typename VectorFieldImageType::PointType VectorFieldOriginType;
	VectorFieldRegionType VF_region;
	VectorFieldOriginType VF_origin = VF->GetOrigin();
	typename VectorFieldRegionType::SizeType VF_region_size = VF->GetBufferedRegion().GetSize();
	for (unsigned int dim=0;dim<ImageDimension;dim++)
	{
		VF_region_size[dim] = size[dim];
		VF_origin[dim] += start[dim] * (VF->GetSpacing()[dim]);
	}
	VF_region.SetSize(VF_region_size);

	// output image
	typename VectorFieldImageType::Pointer cropped_VF = VectorFieldImageType::New();
	cropped_VF->SetRegions(VF_region);
	cropped_VF->SetSpacing(VF->GetSpacing());
	cropped_VF->SetOrigin(VF_origin);
	cropped_VF->SetDirection(VF->GetDirection());
	cropped_VF->Allocate();
	cropped_VF->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

	// iterator on output def field
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_cropped_VF(cropped_VF,cropped_VF->GetBufferedRegion());
	VectorFieldIteratorType it_VF(VF,VF->GetBufferedRegion());

	for ( it_cropped_VF.GoToBegin(); 
		! ( it_cropped_VF.IsAtEnd() ); 
		++it_cropped_VF )
	{
		typename VectorFieldImageType::IndexType index_cropped_VF = it_cropped_VF.GetIndex();
		typename VectorFieldImageType::IndexType index_VF;
		for (unsigned int dim=0;dim<ImageDimension;dim++)
		{
			index_VF[dim] = index_cropped_VF[dim] + start[dim];
		}
		it_VF.SetIndex(index_VF);
		it_cropped_VF.Set(it_VF.Get());
	}

	return cropped_VF;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType> 
int DownSampleVectorFieldImage(	VectorFieldImageType* inputVF, VectorFieldImageType* outputVF )
{
	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	unsigned int ImageDimension = inputVF->GetImageDimension();

	// CAUTION: TO BE USED CAREFULLY
	std::cout << "DS Input Vector Field..." << std::endl;

	typedef typename VectorFieldImageType::RegionType VectorFieldRegionType;
	VectorFieldRegionType VF_region;
	typename VectorFieldRegionType::SizeType VF_region_size = inputVF->GetBufferedRegion().GetSize();
	typename VectorFieldImageType::SpacingType VF_spacing = inputVF->GetSpacing();

	for (unsigned int dim=0;dim<ImageDimension;dim++)
	{
		VF_region_size[dim] = (VF_region_size[dim]+1)/2;
		VF_spacing[dim] *= 2;
	}
	VF_region.SetSize(VF_region_size);

	outputVF->SetRegions(VF_region);
	outputVF->SetSpacing(VF_spacing);
	outputVF->SetOrigin(inputVF->GetOrigin());
	outputVF->SetDirection(inputVF->GetDirection());
	outputVF->Allocate();
	outputVF->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());


	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_inputVF(inputVF,inputVF->GetBufferedRegion());
	VectorFieldIteratorType it_outputVF(outputVF,outputVF->GetBufferedRegion());
	typedef itk::Point < double,::itk::GetImageDimension<VectorFieldImageType>::ImageDimension > PointType;

	typedef itk::VectorLinearInterpolateImageFunction< VectorFieldImageType , double > InterpolatorType;
	typename InterpolatorType::Pointer interpolator_1 = InterpolatorType::New();
	interpolator_1->SetInputImage( inputVF );

	for ( it_outputVF.GoToBegin(); 
		! ( it_outputVF.IsAtEnd() ); 
		++it_outputVF )
	{
		typename VectorFieldImageType::IndexType index_outputVF = it_outputVF.GetIndex();
		PointType point_VF;
		outputVF->TransformIndexToPhysicalPoint(index_outputVF, point_VF);

		if (interpolator_1->IsInsideBuffer(point_VF))
		{
			it_outputVF.Set( interpolator_1->Evaluate(point_VF) );
		}
	}
	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType, class ImageType> 
int RemoveMaskedValues(	VectorFieldImageType* VectorFieldImage, VectorFieldImageType* CleanedVectorFieldImage, ImageType* inputImage_mask )
{
	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	unsigned int ImageDimension = VectorFieldImage->GetImageDimension();

	// iterator on output def field
	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	ImageIteratorType it_inputImage_mask(inputImage_mask,inputImage_mask->GetBufferedRegion());
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_CleanedVectorField(CleanedVectorFieldImage,CleanedVectorFieldImage->GetBufferedRegion());
	VectorFieldIteratorType it_VectorFieldImage(VectorFieldImage,VectorFieldImage->GetBufferedRegion());

	// filling in the deformation field

	for ( it_VectorFieldImage.GoToBegin(), it_CleanedVectorField.GoToBegin(), it_inputImage_mask.GoToBegin(); 
		! ( it_VectorFieldImage.IsAtEnd() || it_CleanedVectorField.IsAtEnd() || it_inputImage_mask.IsAtEnd() ); 
		++it_VectorFieldImage, ++it_CleanedVectorField, ++it_inputImage_mask )
	{

		VectorFieldPixelType inputPoint = it_VectorFieldImage.Get();
		VectorFieldPixelType outputPoint;

		for (unsigned int dim=0; dim<ImageDimension; dim++)
		{
			if (it_inputImage_mask.Get() != 1)
			{
				outputPoint[dim] = itk::NumericTraits<PixelType>::ZeroValue();
			} 
			else
			{
				outputPoint[dim] = inputPoint[dim];     
			}
		}

		it_CleanedVectorField.Set(outputPoint);
	}

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType, class ImageType> 
int LocalOperationsVectorFields(	VectorFieldImageType* VectorFieldImage_1,
								VectorFieldImageType* VectorFieldImage_2,
								VectorFieldImageType* diffVectorFieldImage,
								ImageType* VectorFieldImage_1_mask,
								ImageType* VectorFieldImage_2_mask,
								ImageType* diffVectorFieldImage_mask,
								int operationIndex)
{
	//////////////////////////////////////////////////////////////////////////
	// Operation index:
	// 1 = addition
	// 2 = substraction
	// 3 = multiplication
	// 4 = division
	//////////////////////////////////////////////////////////////////////////

	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	unsigned int ImageDimension = VectorFieldImage_1->GetImageDimension();

	// iterator on output def field
	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	ImageIteratorType it_VectorFieldImage_1_mask(VectorFieldImage_1_mask,VectorFieldImage_1_mask->GetBufferedRegion());
	ImageIteratorType it_VectorFieldImage_2_mask(VectorFieldImage_2_mask,VectorFieldImage_2_mask->GetBufferedRegion());
	ImageIteratorType it_diffVectorFieldImage_mask(diffVectorFieldImage_mask,diffVectorFieldImage_mask->GetBufferedRegion());
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_diffVectorField(diffVectorFieldImage,diffVectorFieldImage->GetBufferedRegion());
	VectorFieldIteratorType it_VectorFieldImage_1(VectorFieldImage_1,VectorFieldImage_1->GetBufferedRegion());
	VectorFieldIteratorType it_VectorFieldImage_2(VectorFieldImage_2,VectorFieldImage_2->GetBufferedRegion());

	// filling in the deformation field

	for ( it_VectorFieldImage_1.GoToBegin(), it_VectorFieldImage_2.GoToBegin(), it_diffVectorField.GoToBegin(),
		it_VectorFieldImage_1_mask.GoToBegin(), it_VectorFieldImage_2_mask.GoToBegin(), it_diffVectorFieldImage_mask.GoToBegin(); 
		! ( it_VectorFieldImage_1.IsAtEnd() || it_VectorFieldImage_2.IsAtEnd() || it_diffVectorField.IsAtEnd()
		|| it_VectorFieldImage_1_mask.IsAtEnd() || it_VectorFieldImage_2_mask.IsAtEnd() || it_diffVectorFieldImage_mask.IsAtEnd()); 
	++it_VectorFieldImage_1, ++it_VectorFieldImage_2, ++it_diffVectorField
		, ++it_VectorFieldImage_1_mask, ++it_VectorFieldImage_2_mask, ++it_diffVectorFieldImage_mask)
	{

		VectorFieldPixelType inputPoint_1 = it_VectorFieldImage_1.Get();
		VectorFieldPixelType inputPoint_2 = it_VectorFieldImage_2.Get();
		VectorFieldPixelType outputPoint;

		for (unsigned int dim=0; dim<ImageDimension; dim++)
		{
			if ( (it_VectorFieldImage_1_mask.Get() == 1) &&
				(it_VectorFieldImage_2_mask.Get() == 1) )
			{
				if (operationIndex == 1)
				{
					outputPoint[dim] = inputPoint_1[dim] + inputPoint_2[dim];     
				} 
				else if(operationIndex == 2)
				{
					outputPoint[dim] = inputPoint_1[dim] - inputPoint_2[dim];     
				}
				else if(operationIndex == 3)
				{
					outputPoint[dim] = inputPoint_1[dim] * inputPoint_2[dim];     
				}
				else if(operationIndex == 4)
				{
					if (inputPoint_2[dim] != 0)
					{
						outputPoint[dim] = inputPoint_1[dim] / inputPoint_2[dim];     
					}
					else
					{
						outputPoint[dim] = itk::NumericTraits<PixelType>::NonpositiveMin();
						it_diffVectorFieldImage_mask.Set(0);
					}	
				}
			} 
			else
			{
				outputPoint[dim] = itk::NumericTraits<PixelType>::NonpositiveMin();
				it_diffVectorFieldImage_mask.Set(0);
			}
		}

		it_diffVectorField.Set(outputPoint);
	}

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType, class ImageType> 
int MultiplyVectorFieldByScalarImage(	ImageType* F_Image,
									 VectorFieldImageType* V_Image,
									 VectorFieldImageType* FTimesV_Image,
									 ImageType* F_Image_mask,
									 ImageType* V_Image_mask,
									 ImageType* FTimesV_Image_mask)
{
	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	unsigned int ImageDimension = V_Image->GetImageDimension();


	// iterator on output def field
	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	ImageIteratorType it_F_Image_mask(F_Image_mask,F_Image_mask->GetBufferedRegion());
	ImageIteratorType it_V_Image_mask(V_Image_mask,V_Image_mask->GetBufferedRegion());
	ImageIteratorType it_FTimesV_Image_mask(FTimesV_Image_mask,FTimesV_Image_mask->GetBufferedRegion());
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_FTimesV(FTimesV_Image,FTimesV_Image->GetBufferedRegion());
	VectorFieldIteratorType it_V(V_Image,V_Image->GetBufferedRegion());

	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	ImageIteratorType it_F(F_Image,F_Image->GetBufferedRegion());

	// filling in the deformation field

	for ( it_V.GoToBegin(), it_F.GoToBegin(), it_FTimesV.GoToBegin(), it_F_Image_mask.GoToBegin(), it_V_Image_mask.GoToBegin(), it_FTimesV_Image_mask.GoToBegin(); 
		! ( it_V.IsAtEnd() || it_F.IsAtEnd() || it_FTimesV.IsAtEnd() || it_F_Image_mask.IsAtEnd() || it_V_Image_mask.IsAtEnd() || it_FTimesV_Image_mask.IsAtEnd() ); 
		++it_V, ++it_F, ++it_FTimesV, ++it_F_Image_mask, ++it_V_Image_mask, ++it_FTimesV_Image_mask)
	{

		PixelType				inputPoint_F = it_F.Get();
		VectorFieldPixelType	inputPoint_V = it_V.Get();
		VectorFieldPixelType	outputPoint;

		for (unsigned int dim=0; dim<ImageDimension; dim++)
		{
			if ( (it_F_Image_mask.Get() == 1) &&
				(it_V_Image_mask.Get() == 1) )
			{
				outputPoint[dim] = inputPoint_V[dim] * inputPoint_F;     
			} 
			else
			{
				outputPoint[dim] = itk::NumericTraits<PixelType>::NonpositiveMin();
				it_FTimesV_Image_mask.Set(0);
			}
		}

		it_FTimesV.Set(outputPoint);
	}

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType> 
int ReSampleVectorFieldImage(	VectorFieldImageType* inputVF, VectorFieldImageType* outputVF )
{
	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	unsigned int ImageDimension = inputVF->GetImageDimension();

	// CAUTION: TO BE USED CAREFULLY
	std::cout << "RS Input Vector Field..." << std::endl;

	typedef typename VectorFieldImageType::RegionType VectorFieldRegionType;
	VectorFieldRegionType VF_region;
	typename VectorFieldRegionType::SizeType VF_region_size = inputVF->GetBufferedRegion().GetSize();
	typename VectorFieldImageType::SpacingType VF_spacing = inputVF->GetSpacing();
	for (unsigned int dim=0;dim<ImageDimension;dim++)
	{
		// Output image has data points on corners
		VF_region_size[dim] = 2*VF_region_size[dim] - 1;
		VF_spacing[dim] /= 2;
	}
	VF_region.SetSize(VF_region_size);

	outputVF->SetRegions(VF_region);
	outputVF->SetSpacing(VF_spacing);
	outputVF->SetOrigin(inputVF->GetOrigin());
	outputVF->SetDirection(inputVF->GetDirection());
	outputVF->Allocate();
	outputVF->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_inputVF(inputVF,inputVF->GetBufferedRegion());
	VectorFieldIteratorType it_outputVF(outputVF,outputVF->GetBufferedRegion());
	typedef itk::Point < double,::itk::GetImageDimension<VectorFieldImageType>::ImageDimension > PointType;

	typedef itk::VectorLinearInterpolateImageFunction< VectorFieldImageType , double > InterpolatorType;
	typename InterpolatorType::Pointer interpolator_1 = InterpolatorType::New();
	interpolator_1->SetInputImage( inputVF );

	for ( it_outputVF.GoToBegin(); 
		! ( it_outputVF.IsAtEnd() ); 
		++it_outputVF )
	{
		typename VectorFieldImageType::IndexType index_outputVF = it_outputVF.GetIndex();
		PointType point_VF;
		outputVF->TransformIndexToPhysicalPoint(index_outputVF, point_VF);

		if (interpolator_1->IsInsideBuffer(point_VF))
		{
			it_outputVF.Set( interpolator_1->Evaluate(point_VF) );
		}
	}
	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType, class ImageType> 
float ComputeNormVectorField(	VectorFieldImageType* V,
							 ImageType* V_mask)
{
	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	unsigned int ImageDimension = V->GetImageDimension();

	// iterator on output def field
	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	ImageIteratorType it_V_mask(V_mask,V_mask->GetBufferedRegion());
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_V(V,V->GetBufferedRegion());
	// filling in the deformation field

	float normValue = 0;
	int total_mask = 0;

	for ( it_V.GoToBegin(), it_V_mask.GoToBegin();
		! ( it_V.IsAtEnd() || it_V_mask.IsAtEnd() ); 
		++it_V, ++it_V_mask)
	{

		VectorFieldPixelType Point_V = it_V.Get();

		if ( (it_V_mask.Get() == 1.) )
		{
			for (unsigned int dim=0; dim<ImageDimension; dim++)
			{
				normValue += Point_V[dim]*Point_V[dim];     
			} 
			total_mask++;
		}
	}

	normValue /= (double)total_mask * 2;
	normValue = sqrt(normValue);

	return normValue;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType, class ImageType> 
int ScalarMultiplyVectorField(	VectorFieldImageType* VectorFieldImage, VectorFieldImageType* scaledVectorFieldImage, double scalar, ImageType* inputImage_mask, ImageType* outputImage_mask)
{
	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	unsigned int ImageDimension = VectorFieldImage->GetImageDimension();


	// iterator on output def field
	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	ImageIteratorType it_inputImage_mask(inputImage_mask,inputImage_mask->GetBufferedRegion());
	ImageIteratorType it_outputImage_mask(outputImage_mask,outputImage_mask->GetBufferedRegion());
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_scaledVectorField(scaledVectorFieldImage,scaledVectorFieldImage->GetBufferedRegion());
	VectorFieldIteratorType it_VectorFieldImage(VectorFieldImage,VectorFieldImage->GetBufferedRegion());

	// filling in the deformation field

	for ( it_VectorFieldImage.GoToBegin(), it_scaledVectorField.GoToBegin(), it_inputImage_mask.GoToBegin(), it_outputImage_mask.GoToBegin(); 
		! ( it_VectorFieldImage.IsAtEnd() || it_scaledVectorField.IsAtEnd() || it_inputImage_mask.IsAtEnd() || it_outputImage_mask.IsAtEnd() ); 
		++it_VectorFieldImage, ++it_scaledVectorField, ++it_inputImage_mask, ++it_outputImage_mask)
	{

		VectorFieldPixelType inputPoint = it_VectorFieldImage.Get();
		VectorFieldPixelType outputPoint;

		for (unsigned int dim=0; dim<ImageDimension; dim++)
		{
			if (it_inputImage_mask.Get() == 1)
			{
				outputPoint[dim] = inputPoint[dim] * scalar;     
			} 
			else
			{
				outputPoint[dim] = itk::NumericTraits<PixelType>::NonpositiveMin();
				it_outputImage_mask.Set(0);
			}
		}
		it_scaledVectorField.Set(outputPoint);
	}

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType, class ImageType> 
int ComposeVectorFields(	VectorFieldImageType* V_1,
						VectorFieldImageType* V_2,
						VectorFieldImageType* V_out,
						ImageType* inputImage_1_mask,
						ImageType* inputImage_2_mask,
						ImageType* outputImage_mask,
						bool composeVectorFieldWithTransform = false)
{
	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	typedef itk::Point < double,
		::itk::GetImageDimension<VectorFieldImageType>::ImageDimension > PointType;
	unsigned int ImageDimension = V_1->GetImageDimension();


	// iterator on output def field
	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	ImageIteratorType it_inputImage_1_mask(inputImage_1_mask,inputImage_1_mask->GetBufferedRegion());
	ImageIteratorType it_inputImage_2_mask(inputImage_2_mask,inputImage_2_mask->GetBufferedRegion());
	ImageIteratorType it_outputImage_mask(outputImage_mask,outputImage_mask->GetBufferedRegion());
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_V_1(V_1,V_1->GetBufferedRegion());
	VectorFieldIteratorType it_V_out(V_out,V_out->GetBufferedRegion());

	typename VectorFieldImageType::IndexType index_1;
	PointType point_1, point_2, point_out;
	VectorFieldPixelType displacement_1;

	typedef itk::LinearInterpolateImageFunction< ImageType , double > InterpolatorType;
	typename InterpolatorType::Pointer interpolator_2_mask = InterpolatorType::New();
	interpolator_2_mask->SetInputImage( inputImage_2_mask );
	typedef itk::VectorLinearInterpolateImageFunction< VectorFieldImageType , double > VectorInterpolatorType;
	typename VectorInterpolatorType::Pointer interpolator_2 = VectorInterpolatorType::New();
	interpolator_2->SetInputImage( V_2 );

	typename VectorInterpolatorType::OutputType displacement_2;
	VectorFieldPixelType displacement_out;

	it_V_out.GoToBegin();


	for ( it_V_1.GoToBegin(), it_inputImage_1_mask.GoToBegin(), it_outputImage_mask.GoToBegin(); 
		! ( it_V_1.IsAtEnd() || it_inputImage_1_mask.IsAtEnd() || it_outputImage_mask.IsAtEnd()); 
		++it_V_1, ++it_inputImage_1_mask, ++it_outputImage_mask)
	{
		index_1 = it_V_1.GetIndex();

		V_1->TransformIndexToPhysicalPoint(index_1, point_1);
		if (it_inputImage_1_mask.Get() != 1)
		{
			for (unsigned int dim=0; dim<ImageDimension; dim++)
			{
				displacement_out[dim] = itk::NumericTraits<PixelType>::NonpositiveMin();
			}
			it_outputImage_mask.Set(0);
		}
		else
		{
			displacement_1 = it_V_1.Get();

			for (unsigned int dim=0; dim<ImageDimension; dim++)
			{
				point_2[dim] = point_1[dim] + (double)displacement_1[dim];     
			}

			if (interpolator_2->IsInsideBuffer(point_2))
			{
// 				if (interpolator_2_mask->Evaluate(point_2) == 1)  // gives problems due to interpolation, not exactly 1 on the mask.
				PixelType testValue = itk::NumericTraits<PixelType>::NonpositiveMin()/((PixelType)std::pow(10.,10.));
				bool validPoint = true;
				for (unsigned int dim=0; dim<ImageDimension; dim++)
				{   if (interpolator_2->Evaluate(point_2)[dim] < testValue){ validPoint = false; }   }
				
				if ( (interpolator_2_mask->Evaluate(point_2) > 0.8) && (validPoint) )
				{
					displacement_2 = interpolator_2->Evaluate(point_2);
					for (unsigned int dim=0; dim<ImageDimension; dim++)
					{
						if (composeVectorFieldWithTransform)
						{
							displacement_out[dim] = displacement_2[dim];
						}
						else
						{
							point_out[dim] = point_2[dim] + (double)displacement_2[dim];  
							displacement_out[dim] = point_out[dim] - point_1[dim];
						}
					}
				}
				else
				{
					for (unsigned int dim=0; dim<ImageDimension; dim++)
					{
						displacement_out[dim] = itk::NumericTraits<PixelType>::NonpositiveMin();
					}
					it_outputImage_mask.Set(0);
				} 
			}
			else
			{
				for (unsigned int dim=0; dim<ImageDimension; dim++)
				{
					displacement_out[dim] = itk::NumericTraits<PixelType>::NonpositiveMin();
				}
				it_outputImage_mask.Set(0);
			} 
		}
		it_V_out.SetIndex(index_1);
		it_V_out.Set(displacement_out);
	}

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType, class JacobianVectorFieldImageType, class ImageType> 
int ComputeJacobianVectorField(	VectorFieldImageType* VectorFieldImage,
							   ImageType* VectorFieldImage_mask,
							   JacobianVectorFieldImageType* Jacobian_Image,
							   ImageType* Jacobian_Image_mask)
{
	typedef typename VectorFieldImageType::PixelType			VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	typedef typename JacobianVectorFieldImageType::PixelType	JacobianVectorFieldPixelType;
	unsigned int ImageDimension = VectorFieldImage->GetImageDimension();

	typename VectorFieldImageType::SizeType	size_VectorField = VectorFieldImage->GetBufferedRegion().GetSize();

	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	ImageIteratorType it_VectorFieldImage_mask(VectorFieldImage_mask,VectorFieldImage_mask->GetBufferedRegion());
	ImageIteratorType it_Jacobian_Image_mask(Jacobian_Image_mask,Jacobian_Image_mask->GetBufferedRegion());
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_VectorFieldImage(VectorFieldImage,VectorFieldImage->GetBufferedRegion());
	typedef itk::ImageRegionIterator<JacobianVectorFieldImageType> JacobianVectorFieldIteratorType;
	JacobianVectorFieldIteratorType it_JacobianImage(Jacobian_Image,VectorFieldImage->GetLargestPossibleRegion());

	for ( it_VectorFieldImage.GoToBegin() , it_JacobianImage.GoToBegin(), it_VectorFieldImage_mask.GoToBegin(), it_Jacobian_Image_mask.GoToBegin(); 
		! ( it_VectorFieldImage.IsAtEnd() || it_JacobianImage.IsAtEnd() || it_VectorFieldImage_mask.IsAtEnd() || it_Jacobian_Image_mask.IsAtEnd() ); 
		++it_VectorFieldImage , ++it_JacobianImage, ++it_VectorFieldImage_mask, ++it_Jacobian_Image_mask)
	{
		VectorFieldPixelType Point_VectorField = it_VectorFieldImage.Get();
		JacobianVectorFieldPixelType Point_Jacobian;
		typename VectorFieldImageType::IndexType index_VectorField = it_VectorFieldImage.GetIndex();
		typename JacobianVectorFieldImageType::IndexType index_Jacobian = it_JacobianImage.GetIndex();

		std::vector <int> indexIsInBorder;
		std::vector <VectorFieldPixelType> Point_VectorField_Before;
		std::vector <VectorFieldPixelType> Point_VectorField_After;

		for (unsigned int dim=0;dim<ImageDimension;dim++)
		{
			indexIsInBorder.push_back(0);
			Point_VectorField_Before.push_back( itk::NumericTraits<VectorFieldPixelType>::ZeroValue() );
			Point_VectorField_After.push_back( itk::NumericTraits<VectorFieldPixelType>::ZeroValue() );
		}
		for (unsigned int dim=0;dim<ImageDimension;dim++)
		{
			if ( index_VectorField[dim] == 0 ){ indexIsInBorder.at(dim) = 1; }
			if ( index_VectorField[dim] == size_VectorField[dim]-1 ){ indexIsInBorder.at(dim) = 2; }
		}

		bool IsValidPointJacobian = true;
		for (unsigned int dim=0;dim<ImageDimension;dim++)
		{
			if (indexIsInBorder.at(dim)==0)
			{
				typename VectorFieldImageType::IndexType index_VectorField_Before = index_VectorField;
				index_VectorField_Before[dim] = index_VectorField[dim] - 1;
				it_VectorFieldImage.SetIndex(index_VectorField_Before);
				Point_VectorField_Before.at(dim) = it_VectorFieldImage.Get();
				it_VectorFieldImage_mask.SetIndex(index_VectorField_Before);
				if ( it_VectorFieldImage_mask.Get() != 1 )
				{ IsValidPointJacobian = false; }

				typename VectorFieldImageType::IndexType index_VectorField_After = index_VectorField;
				index_VectorField_After[dim] = index_VectorField[dim] + 1;
				it_VectorFieldImage.SetIndex(index_VectorField_After);
				Point_VectorField_After.at(dim) = it_VectorFieldImage.Get();
				it_VectorFieldImage_mask.SetIndex(index_VectorField_After);
				if ( it_VectorFieldImage_mask.Get() != 1 )
				{ IsValidPointJacobian = false; }
			}

			if (indexIsInBorder.at(dim)==1)
			{
				typename VectorFieldImageType::IndexType index_VectorField_Before = index_VectorField;
				it_VectorFieldImage.SetIndex(index_VectorField_Before);
				Point_VectorField_Before.at(dim) = it_VectorFieldImage.Get();
				it_VectorFieldImage_mask.SetIndex(index_VectorField_Before);
				if ( it_VectorFieldImage_mask.Get() != 1 )
				{ IsValidPointJacobian = false; }

				typename VectorFieldImageType::IndexType index_VectorField_After = index_VectorField;
				index_VectorField_After[dim] = index_VectorField[dim] + 1;
				it_VectorFieldImage.SetIndex(index_VectorField_After);
				Point_VectorField_After.at(dim) = it_VectorFieldImage.Get();
				it_VectorFieldImage_mask.SetIndex(index_VectorField_After);
				if ( it_VectorFieldImage_mask.Get() != 1 )
				{ IsValidPointJacobian = false; }

				for (int dim2=0;dim2<ImageDimension;dim2++)
				{
					Point_VectorField_Before.at(dim)[dim2] *= 2;
					Point_VectorField_After.at(dim)[dim2] *= 2;
				}
			}

			if (indexIsInBorder.at(dim)==2)
			{
				typename VectorFieldImageType::IndexType index_VectorField_Before = index_VectorField;
				index_VectorField_Before[dim] = index_VectorField[dim] - 1;
				it_VectorFieldImage.SetIndex(index_VectorField_Before);
				Point_VectorField_Before.at(dim) = it_VectorFieldImage.Get();
				it_VectorFieldImage_mask.SetIndex(index_VectorField_Before);
				if ( it_VectorFieldImage_mask.Get() != 1 )
				{ IsValidPointJacobian = false;	}

				typename VectorFieldImageType::IndexType index_VectorField_After = index_VectorField;
				it_VectorFieldImage.SetIndex(index_VectorField_After);
				Point_VectorField_After.at(dim) = it_VectorFieldImage.Get();
				it_VectorFieldImage_mask.SetIndex(index_VectorField_After);
				if ( it_VectorFieldImage_mask.Get() != 1 )
				{ IsValidPointJacobian = false;	}

				for (int dim2=0;dim2<ImageDimension;dim2++)
				{
					Point_VectorField_Before.at(dim)[dim2] *= 2;
					Point_VectorField_After.at(dim)[dim2] *= 2;
				}
			}

		}

		// Defining Jacobian matrix

		typename VectorFieldImageType::SpacingType spacing = VectorFieldImage->GetSpacing();
		it_Jacobian_Image_mask.SetIndex(index_Jacobian);

		if (IsValidPointJacobian)
		{
			for (unsigned int dim=0;dim<ImageDimension;dim++)
			{
				for (unsigned int dim2=0;dim2<ImageDimension;dim2++)
				{
					Point_Jacobian[dim][dim2] = (Point_VectorField_After.at(dim)[dim2] - Point_VectorField_Before.at(dim)[dim2]) / (2 * spacing[dim]);
				}
				// As V_Image is a vector field, it encodes (V(X)-Id(X)). As a result, the jacobian of V should be:
				// J( V_Image ) + J(Id) = J(V_Image) + Id(2x2)
				Point_Jacobian[dim][dim] += 1;
			}

		} 
		else
		{
			for (int dim=0;dim<ImageDimension;dim++)
			{
				for (int dim2=0;dim2<ImageDimension;dim2++)
				{
					Point_Jacobian[dim][dim2] = itk::NumericTraits<PixelType>::NonpositiveMin();
				}
			}
			it_Jacobian_Image_mask.Set(0);
		}

		it_VectorFieldImage.SetIndex(index_VectorField);

		it_JacobianImage.SetIndex(index_Jacobian);
		it_JacobianImage.Set(Point_Jacobian);

	}

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType, class JacobianVectorFieldImageType, class ImageType> 
int ComposeVectorFieldWithJacobianTranspose(	VectorFieldImageType* V_Image,
											JacobianVectorFieldImageType* J_Image,
											JacobianVectorFieldImageType* JtoV_Image,
											ImageType* V_Image_mask,
											ImageType* J_Image_mask,
											ImageType* JtoV_Image_mask)
{
	typedef typename VectorFieldImageType::PixelType			VectorFieldPixelType;
	typedef typename JacobianVectorFieldImageType::PixelType	JacobianVectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	typedef itk::Point < double,
		::itk::GetImageDimension<VectorFieldImageType>::ImageDimension > PointType;
	unsigned int ImageDimension = V_Image->GetImageDimension();

	unsigned int dim_3D = 0;
	if (ImageDimension == 3)
	{
		dim_3D = 1;
	}

	// iterator on output def field
	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	ImageIteratorType it_V_Image_mask(V_Image_mask,V_Image_mask->GetBufferedRegion());
	ImageIteratorType it_J_Image_mask(J_Image_mask,J_Image_mask->GetBufferedRegion());
	ImageIteratorType it_JtoV_Image_mask(JtoV_Image_mask,JtoV_Image_mask->GetBufferedRegion());
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_V  (V_Image,V_Image->GetBufferedRegion());
	typedef itk::ImageRegionIterator<JacobianVectorFieldImageType> JacobianVectorFieldIteratorType;
	JacobianVectorFieldIteratorType it_J(J_Image,J_Image->GetLargestPossibleRegion());
	JacobianVectorFieldIteratorType it_JtoV(JtoV_Image,JtoV_Image->GetLargestPossibleRegion());

	typename VectorFieldImageType::IndexType index_V;
	PointType point_1, point_2;
	VectorFieldPixelType displacement_V;


	// Converting Jacobian Matrix into 2 vectors for interpolation

	// First line for d(fx)/d(i)
	typename VectorFieldImageType::Pointer J_fx_Image = VectorFieldImageType::New();
	J_fx_Image->SetRegions(V_Image->GetBufferedRegion());
	J_fx_Image->SetSpacing(V_Image->GetSpacing());
	J_fx_Image->SetOrigin(V_Image->GetOrigin());
	J_fx_Image->SetDirection(V_Image->GetDirection());
	J_fx_Image->Allocate();
	J_fx_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

	// First line for d(fy)/d(i)
	typename VectorFieldImageType::Pointer J_fy_Image = VectorFieldImageType::New();
	J_fy_Image->SetRegions(V_Image->GetBufferedRegion());
	J_fy_Image->SetSpacing(V_Image->GetSpacing());
	J_fy_Image->SetOrigin(V_Image->GetOrigin());
	J_fy_Image->SetDirection(V_Image->GetDirection());
	J_fy_Image->Allocate();
	J_fy_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

	typename VectorFieldImageType::Pointer J_fz_Image = VectorFieldImageType::New();
	if (dim_3D)
	{
		J_fz_Image->SetRegions(V_Image->GetBufferedRegion());
		J_fz_Image->SetSpacing(V_Image->GetSpacing());
		J_fz_Image->SetOrigin(V_Image->GetOrigin());
		J_fz_Image->SetDirection(V_Image->GetDirection());
		J_fz_Image->Allocate();
		J_fz_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
	}

	VectorFieldIteratorType it_J_fx  (J_fx_Image,J_fx_Image->GetBufferedRegion());
	VectorFieldIteratorType it_J_fy  (J_fy_Image,J_fy_Image->GetBufferedRegion());
	VectorFieldIteratorType it_J_fz  (J_fz_Image,J_fz_Image->GetBufferedRegion());

	VectorFieldPixelType Point_J_fx, Point_J_fy, Point_J_fz;
	if (dim_3D)
	{ it_J_fz.GoToBegin(); }

	for ( it_J.GoToBegin(), it_J_fx.GoToBegin(), it_J_fy.GoToBegin(); 
		! ( it_J.IsAtEnd() || it_J_fx.IsAtEnd() || it_J_fy.IsAtEnd() ); 
		++it_J, ++it_J_fx, ++it_J_fy)
	{
		JacobianVectorFieldPixelType Point_J = it_J.Get();

		for (unsigned int dim=0;dim<ImageDimension;dim++)
		{
			Point_J_fx[dim] = Point_J[dim][0];
			Point_J_fy[dim] = Point_J[dim][1];
			if (dim_3D)
			{ Point_J_fz[dim] = Point_J[dim][2]; }
		}

		it_J_fx.Set(Point_J_fx);
		it_J_fy.Set(Point_J_fy);
		if (dim_3D)
		{ 
			it_J_fz.SetIndex(it_J.GetIndex());
			it_J_fz.Set(Point_J_fz);
		}
	}


	typedef itk::LinearInterpolateImageFunction< ImageType , double > InterpolatorType;
	typename InterpolatorType::Pointer interpolator_J_mask = InterpolatorType::New();
	interpolator_J_mask->SetInputImage( J_Image_mask );
	typedef itk::VectorLinearInterpolateImageFunction< VectorFieldImageType , double > VectorInterpolatorType;
	typename VectorInterpolatorType::Pointer interpolator_x = VectorInterpolatorType::New();
	interpolator_x->SetInputImage( J_fx_Image );
	typename VectorInterpolatorType::Pointer interpolator_y = VectorInterpolatorType::New();
	interpolator_y->SetInputImage( J_fy_Image );
	typename VectorInterpolatorType::Pointer interpolator_z = VectorInterpolatorType::New();
	if (dim_3D)
	{ interpolator_z->SetInputImage( J_fz_Image ); }

	typename VectorInterpolatorType::OutputType Point_J_fx_interp, Point_J_fy_interp, Point_J_fz_interp;

	JacobianVectorFieldPixelType point_JtoV;
	it_JtoV.GoToBegin();


	for ( it_V.GoToBegin(), it_J_fx.GoToBegin(), it_J_fy.GoToBegin(), it_JtoV.GoToBegin(), it_JtoV_Image_mask.GoToBegin(), it_V_Image_mask.GoToBegin(); 
		! ( it_V.IsAtEnd() || it_J_fx.IsAtEnd() || it_J_fy.IsAtEnd() || it_JtoV.IsAtEnd() || it_JtoV_Image_mask.IsAtEnd() || it_V_Image_mask.IsAtEnd()); 
		++it_V, ++it_J_fx, ++it_J_fy, ++it_JtoV, ++it_JtoV_Image_mask, ++it_V_Image_mask)
	{
		index_V = it_V.GetIndex();
		V_Image->TransformIndexToPhysicalPoint(index_V, point_1);
		displacement_V = it_V.Get();

		if (it_V_Image_mask.Get() == 1.)
		{
			for (unsigned int dim=0; dim<ImageDimension; dim++)
			{
				point_2[dim] = point_1[dim] + displacement_V[dim];     
			}

// 			if ( (interpolator_x->IsInsideBuffer(point_2)) && (interpolator_y->IsInsideBuffer(point_2))
// 				&& (interpolator_J_mask->Evaluate(point_2) == 1) )   // gives problems due to interpolation, not exactly 1 on the mask.

			PixelType testValue = itk::NumericTraits<PixelType>::NonpositiveMin()/((PixelType)std::pow(10.,10.));
			bool validPoint = true;
			if ( !(interpolator_x->IsInsideBuffer(point_2)) ){ validPoint = false; }
			if ( !(interpolator_y->IsInsideBuffer(point_2)) ){ validPoint = false; }
			if (dim_3D)
			{  if ( !(interpolator_z->IsInsideBuffer(point_2)) ){ validPoint = false; }  }

			if (validPoint)
			{
				for (unsigned int dim=0; dim<ImageDimension; dim++)
				{
					if (interpolator_x->Evaluate(point_2)[dim] < testValue){ validPoint = false; }
					if (interpolator_y->Evaluate(point_2)[dim] < testValue){ validPoint = false; }
					if (dim_3D)
					{  if (interpolator_z->Evaluate(point_2)[dim] < testValue){ validPoint = false; }  }
				}
			}
			
			if ( (validPoint) && (interpolator_J_mask->Evaluate(point_2) > 0.8) )
			{
				Point_J_fx_interp = interpolator_x->Evaluate(point_2);
				Point_J_fy_interp = interpolator_y->Evaluate(point_2);
				if (dim_3D)
				{ Point_J_fz_interp = interpolator_z->Evaluate(point_2); }

				// including transposition
				for (unsigned int dim=0; dim<ImageDimension; dim++)
				{
					point_JtoV[0][dim] = Point_J_fx_interp[dim];
					point_JtoV[1][dim] = Point_J_fy_interp[dim];
					if (dim_3D)
					{ point_JtoV[2][dim] = Point_J_fz_interp[dim]; }
				}
			} 
			else
			{
				for (unsigned int dim=0; dim<ImageDimension; dim++)
				{
					for (unsigned int dim2=0; dim2<ImageDimension; dim2++)
					{ point_JtoV[dim2][dim] = itk::NumericTraits<PixelType>::NonpositiveMin(); }
				}
				it_JtoV_Image_mask.Set(0);
			}
		} 
		else
		{
			for (unsigned int dim=0; dim<ImageDimension; dim++)
			{
				for (unsigned int dim2=0; dim2<ImageDimension; dim2++)
				{ point_JtoV[dim2][dim] = itk::NumericTraits<PixelType>::NonpositiveMin(); }
			}
			it_JtoV_Image_mask.Set(0);
		}
		it_JtoV.Set(point_JtoV);
	}



	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType, class JacobianVectorFieldImageType, class ImageType> 
int MultiplyJacobianByVectorField(	VectorFieldImageType* V_Image,
								  JacobianVectorFieldImageType* J_Image,
								  VectorFieldImageType* JtimesV_Image,
								  ImageType* V_Image_mask,
								  ImageType* J_Image_mask,
								  ImageType* JtimesV_Image_mask)
{
	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename JacobianVectorFieldImageType::PixelType	JacobianVectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	unsigned int ImageDimension = V_Image->GetImageDimension();


	// iterator on output def field
	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	ImageIteratorType it_V_Image_mask(V_Image_mask,V_Image_mask->GetBufferedRegion());
	ImageIteratorType it_J_Image_mask(J_Image_mask,J_Image_mask->GetBufferedRegion());
	ImageIteratorType it_JtimesV_Image_mask(JtimesV_Image_mask,JtimesV_Image_mask->GetBufferedRegion());
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_JtimesV(JtimesV_Image,JtimesV_Image->GetBufferedRegion());
	VectorFieldIteratorType it_V(V_Image,V_Image->GetBufferedRegion());
	typedef itk::ImageRegionIterator<JacobianVectorFieldImageType> JacobianVectorFieldIteratorType;
	JacobianVectorFieldIteratorType it_J(J_Image,J_Image->GetLargestPossibleRegion());

	// filling in the deformation field

	for ( it_V.GoToBegin(), it_J.GoToBegin(), it_JtimesV.GoToBegin(), it_V_Image_mask.GoToBegin(), it_J_Image_mask.GoToBegin(), it_JtimesV_Image_mask.GoToBegin(); 
		! ( it_V.IsAtEnd() || it_J.IsAtEnd() || it_JtimesV.IsAtEnd() || it_V_Image_mask.IsAtEnd() || it_J_Image_mask.IsAtEnd() || it_JtimesV_Image_mask.IsAtEnd()); 
		++it_V, ++it_J, ++it_JtimesV, ++it_V_Image_mask, ++it_J_Image_mask, ++it_JtimesV_Image_mask)
	{

		VectorFieldPixelType Point_V = it_V.Get();
		JacobianVectorFieldPixelType Point_J = it_J.Get();
		VectorFieldPixelType Point_JtimesV;

		for (unsigned int dim=0; dim<ImageDimension; dim++)
		{
			if ( (it_J_Image_mask.Get() == 1) && (it_V_Image_mask.Get() == 1) )
			{
				Point_JtimesV[dim] = 0;
				for (unsigned int dim2=0; dim2<ImageDimension; dim2++)
				{ Point_JtimesV[dim] += Point_J[dim2][dim] * Point_V[dim2]; }
				//	The following would use the jacobian transpose:
				//	Point_JtimesV[dim] = Point_J[dim][0] * Point_V[0] + Point_J[dim][1] * Point_V[1];   
			} 
			else
			{
				Point_JtimesV[dim] = itk::NumericTraits<PixelType>::NonpositiveMin();
				it_JtimesV_Image_mask.Set(0);
			}
		}
		it_JtimesV.Set(Point_JtimesV);
	}

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class ImageType, class JacobianVectorFieldImageType> 
int ComputeAbsDetJacobian( JacobianVectorFieldImageType* J_Image,
						  ImageType* absDet_J_Image,
						  ImageType* J_Image_mask,
						  ImageType* absDet_J_Image_mask)
{
	typedef typename JacobianVectorFieldImageType::PixelType	JacobianVectorFieldPixelType;
	typedef typename ImageType::PixelType PixelType;
	unsigned int ImageDimension = J_Image->GetImageDimension();

	unsigned int dim_3D = 0;
	if (ImageDimension == 3)
	{
		dim_3D = 1;
	}


	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	ImageIteratorType it_J_Image_mask(J_Image_mask,J_Image_mask->GetBufferedRegion());
	ImageIteratorType it_absDet_J_Image_mask(absDet_J_Image_mask,absDet_J_Image_mask->GetBufferedRegion());
	ImageIteratorType it_absDet_J(absDet_J_Image,absDet_J_Image->GetBufferedRegion());

	typedef itk::ImageRegionIterator<JacobianVectorFieldImageType> JacobianVectorFieldIteratorType;
	JacobianVectorFieldIteratorType it_J(J_Image,J_Image->GetLargestPossibleRegion());

	for ( it_absDet_J.GoToBegin(), it_J.GoToBegin(), it_J_Image_mask.GoToBegin(), it_absDet_J_Image_mask.GoToBegin(); 
		! ( it_absDet_J.IsAtEnd() || it_J.IsAtEnd() || it_J_Image_mask.IsAtEnd() || it_absDet_J_Image_mask.IsAtEnd()); 
		++it_absDet_J, ++it_J, ++it_J_Image_mask, ++it_absDet_J_Image_mask)
	{
		JacobianVectorFieldPixelType Point_J = it_J.Get();
		PixelType Point_absDet_J;

		if ( it_J_Image_mask.Get() == 1 )
		{
			if (dim_3D)
			{
					PixelType Point_absDet_J_0 = Point_J[1][1] * Point_J[2][2] - Point_J[1][2] * Point_J[2][1];
					PixelType Point_absDet_J_1 = Point_J[0][0] * Point_J[2][2] - Point_J[0][2] * Point_J[2][0];
					PixelType Point_absDet_J_2 = Point_J[0][1] * Point_J[1][1] - Point_J[0][1] * Point_J[1][0];
					Point_absDet_J = Point_J[0][0] * Point_absDet_J_0 - Point_J[0][1] * Point_absDet_J_1 +Point_J[0][2] * Point_absDet_J_2; 
			}
			else
			{
				Point_absDet_J = Point_J[0][0] * Point_J[1][1] - Point_J[0][1] * Point_J[1][0]; 
			}
			Point_absDet_J = std::abs(Point_absDet_J);
		} 
		else
		{
			Point_absDet_J = itk::NumericTraits<PixelType>::NonpositiveMin();
			it_absDet_J_Image_mask.Set(0);
		}
		it_absDet_J.Set(Point_absDet_J);
	}

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////












#endif /* !REGVECTORCOMMON_H_ */
