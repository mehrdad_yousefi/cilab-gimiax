#ifndef   	REGPOSTPROCESSING_H_
# define   	REGPOSTPROCESSING_H_

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkNumericTraits.h"
#include "itkResampleImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkWarpImageFilter.h"

//////////////////////////////////////////////////////////////////////////

// Available functions:
// ---------------------
// CreateGrid
// WarpImageWithVectorField

//////////////////////////////////////////////////////////////////////////


template <class GridImageType, class FixedImageType> 
typename GridImageType::Pointer CreateGrid(const FixedImageType* inputImage, int grid_spacing)
{
	typename GridImageType::Pointer gridImage = GridImageType::New();
	typename GridImageType::IndexType grid_start = inputImage->GetBufferedRegion().GetIndex();
	typename GridImageType::SizeType grid_size   = inputImage->GetBufferedRegion().GetSize();

	typename GridImageType::RegionType grid_region;
	grid_region.SetSize( grid_size );
	grid_region.SetIndex( grid_start );

	gridImage->SetRegions( grid_region );
	gridImage->Allocate();
	gridImage->FillBuffer(itk::NumericTraits<typename GridImageType::PixelType>::ZeroValue());

	int ImageDimension = inputImage->GetBufferedRegion().GetImageDimension();

	std::vector <int> spacing;
	std::vector <int> numPoints;
	for (int r=0;r<ImageDimension;r++)
	{
		spacing.push_back(grid_spacing);
		numPoints.push_back( (int)(grid_size[r] / spacing[r]) );
	}

	typename GridImageType::IndexType pixelIndex;

	for (int r=0;r<ImageDimension;r++)
	{

		for (int ir=0;ir<grid_size[r];ir++)
		{
			for (int r2=0;r2<ImageDimension;r2++)
			{

				if (r2 == r)
				{
					pixelIndex[r2] = ir;
				}
				else
				{
					for (int ir2=0;ir2<numPoints.at(r2);ir2++)
					{
						pixelIndex[r2] = ir2 * spacing.at(r2);
						gridImage->SetPixel( pixelIndex , 200 );
					}
				}

			}
		}

	}

	return gridImage;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType, class ImageType> 
typename ImageType::Pointer 
WarpImageWithVectorField(	ImageType* F_Image,
						 VectorFieldImageType* V_Image,
						 typename ImageType::PixelType paddingValue )
{
	typedef itk::WarpImageFilter< ImageType, ImageType, VectorFieldImageType  >  WarpFilterType;
	typename WarpFilterType::Pointer warpFilter = WarpFilterType::New();

// 	typedef typename ImageType::PixelType  PixelType;

	typedef itk::LinearInterpolateImageFunction< ImageType, double >  InterpolatorType;
	typename InterpolatorType::Pointer interpolator = InterpolatorType::New();

	warpFilter->SetInterpolator( interpolator );
	warpFilter->SetOutputSpacing( V_Image->GetSpacing() );
	warpFilter->SetOutputOrigin(  V_Image->GetOrigin() );
	warpFilter->SetEdgePaddingValue( paddingValue );

	warpFilter->SetDeformationField( V_Image );
	warpFilter->SetInput( F_Image );
	warpFilter->Update();

	return warpFilter->GetOutput();

}

//////////////////////////////////////////////////////////////////////////





#endif /* !REGPOSTPROCESSING_H_ */
