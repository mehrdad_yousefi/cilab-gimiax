#ifndef   	REGDATAOPERATIONS_H_
# define   	REGDATAOPERATIONS_H_

#include "itkImage.h"
#include "itkNumericTraits.h"
#include "itkImageRegionIterator.h"
#include "itkVectorLinearInterpolateImageFunction.h"
#include "itkLinearInterpolateImageFunction.h"

#include "itkGrayscaleDilateImageFilter.h"
#include "itkGrayscaleErodeImageFilter.h"
#include "itkBinaryBallStructuringElement.h" 

#include "regPostprocessing.h"
#include "regImageCommon.h"
#include "regVectorCommon.h"

//////////////////////////////////////////////////////////////////////////

// Available functions:
// ---------------------
// ExtendMaskImage_exp_decrease
// maskVF_exp_decrease
// ExtendVF_exp_decrease
// ComputeSqrtPhi
// ComputeInversePhi
// LinearInterpolateImageFromDispField
// LinearInterpolateVelocityFromDispField
// ComputeDistanceBetweenPhi
// ComputeLogPhi

//////////////////////////////////////////////////////////////////////////

template <class ImageType> 
int ExtendMaskImage_exp_decrease(	ImageType* input_maskImage, ImageType* extended_maskImage, 	unsigned int N_it, float sig_decrease, int V_radius)
{
	typedef typename ImageType::PixelType  PixelType;
	unsigned int ImageDimension = input_maskImage->GetImageDimension();

	//////////////////////////////////////////////////////////////////////////

	CopyImage <ImageType> ( input_maskImage, extended_maskImage );

	typename ImageType::Pointer extended_maskImage_mask_old = ImageType::New();
	extended_maskImage_mask_old->SetRegions(input_maskImage->GetBufferedRegion());
	extended_maskImage_mask_old->SetSpacing(input_maskImage->GetSpacing());
	extended_maskImage_mask_old->SetOrigin(input_maskImage->GetOrigin());
	extended_maskImage_mask_old->SetDirection(input_maskImage->GetDirection());
	extended_maskImage_mask_old->Allocate();
	extended_maskImage_mask_old->FillBuffer(1);
	CopyImage <ImageType> ( input_maskImage, extended_maskImage_mask_old.GetPointer() );

	typedef itk::BinaryBallStructuringElement< PixelType, ::itk::GetImageDimension<ImageType>::ImageDimension  > StructuringElementType;
	typedef itk::GrayscaleDilateImageFilter< ImageType, ImageType, StructuringElementType >  DilateFilterType;
	typedef itk::GrayscaleErodeImageFilter< ImageType, ImageType, StructuringElementType >  ErodeFilterType;
	StructuringElementType  structuringElement;
	structuringElement.SetRadius( 1 );  // 3x3 structuring element
	structuringElement.CreateStructuringElement();

	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;

	ImageIteratorType it_extended_maskImage(extended_maskImage,extended_maskImage->GetBufferedRegion());

	for (unsigned int it=0;it<N_it;it++)
	{

		// Create processing zone where data will be extended:
		// 1. new_mask = old_mask dilated by 1 pixel (struturing element 3x3)
		// 2. Processing zone = find( (new_mask - old_mask) == 1 )
		// 3. Updating mask: old_mask = new_mask

		typename DilateFilterType::Pointer dilateFilter = DilateFilterType::New();
		dilateFilter->SetKernel( structuringElement );
		dilateFilter->SetInput( extended_maskImage_mask_old.GetPointer() );
		dilateFilter->Update();
		typename ImageType::Pointer extended_maskImage_mask = dilateFilter->GetOutput();

		typename ImageType::Pointer diff_maskVF = ImageType::New();
		diff_maskVF->SetRegions(input_maskImage->GetBufferedRegion());
		diff_maskVF->SetSpacing(input_maskImage->GetSpacing());
		diff_maskVF->SetOrigin(input_maskImage->GetOrigin());
		diff_maskVF->SetDirection(input_maskImage->GetDirection());
		diff_maskVF->Allocate();
		diff_maskVF->FillBuffer(itk::NumericTraits<PixelType>::ZeroValue());
		typename ImageType::Pointer diff_maskVF_mask = ImageType::New();
		diff_maskVF_mask->SetRegions(input_maskImage->GetBufferedRegion());
		diff_maskVF_mask->SetSpacing(input_maskImage->GetSpacing());
		diff_maskVF_mask->SetOrigin(input_maskImage->GetOrigin());
		diff_maskVF_mask->SetDirection(input_maskImage->GetDirection());
		diff_maskVF_mask->Allocate();
		diff_maskVF_mask->FillBuffer(1);

		int diff_maskVF_done = LocalOperationsImages <ImageType> ( extended_maskImage_mask.GetPointer() ,
			extended_maskImage_mask_old,
			diff_maskVF.GetPointer(),
			diff_maskVF_mask.GetPointer(),
			diff_maskVF_mask.GetPointer(),
			diff_maskVF_mask.GetPointer(),
			2 );

		CopyImage <ImageType> ( extended_maskImage_mask.GetPointer(), extended_maskImage_mask_old.GetPointer() );

		//////////////////////////////////////////////////////////////////////////

		ImageIteratorType it_diff_maskVF(diff_maskVF,diff_maskVF->GetBufferedRegion());

		for ( it_diff_maskVF.GoToBegin(); 
			! ( it_diff_maskVF.IsAtEnd() ); 
			++it_diff_maskVF )
		{

			if (it_diff_maskVF.Get()!=0)
			{
				typename ImageType::IndexType index_VF = it_diff_maskVF.GetIndex();
				PixelType dist2 = (PixelType)((it+1)*(it+1));

				// Exponential decrease
				it_extended_maskImage.SetIndex(index_VF);
				PixelType exp_decrease_coef = -(dist2 / (2*(PixelType)sig_decrease*(PixelType)sig_decrease));
				it_extended_maskImage.Set(std::exp( float(exp_decrease_coef) ));

			} // 	if (it_diff_maskVF.Get()!=0)

		} // for ( it_diff_maskVF.GoToBegin() ...

	} //	for (unsigned int it=0;it<N_it;it++)

	//////////////////////////////////////////////////////////////////////////

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType, class ImageType> 
int maskVF_exp_decrease(	VectorFieldImageType* VF, ImageType* mask_VF, VectorFieldImageType* extended_VF, 	unsigned int N_it, float sig_decrease, int V_radius)
{
	typedef typename ImageType::PixelType  PixelType;
	unsigned int ImageDimension = mask_VF->GetImageDimension();
	typedef itk::Vector<PixelType, ::itk::GetImageDimension<ImageType>::ImageDimension > VectorFieldPixelType;

	//////////////////////////////////////////////////////////////////////////

	mask_VF->SetSpacing(VF->GetSpacing());
	mask_VF->SetOrigin(VF->GetOrigin());

	// output image
	typename VectorFieldImageType::Pointer extended_VF_old = VectorFieldImageType::New();
	extended_VF_old->SetRegions(VF->GetBufferedRegion());
	extended_VF_old->SetSpacing(VF->GetSpacing());
	extended_VF_old->SetOrigin(VF->GetOrigin());
	extended_VF_old->SetDirection(VF->GetDirection());
	extended_VF_old->Allocate();
	extended_VF_old->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

	// exponential mask
	typename VectorFieldImageType::Pointer exp_mask = VectorFieldImageType::New();
	exp_mask->SetRegions(extended_VF->GetBufferedRegion());
	exp_mask->SetSpacing(extended_VF->GetSpacing());
	exp_mask->SetOrigin(extended_VF->GetOrigin());
	exp_mask->SetDirection(extended_VF->GetDirection());
	exp_mask->Allocate();
	VectorFieldPixelType constant_value;
	for (unsigned int dim=0;dim<ImageDimension;dim++){ constant_value[dim] = 1; }
	exp_mask->FillBuffer(constant_value);


	typedef itk::BinaryBallStructuringElement< PixelType, ::itk::GetImageDimension<ImageType>::ImageDimension  > StructuringElementType;
	typedef itk::GrayscaleDilateImageFilter< ImageType, ImageType, StructuringElementType >  DilateFilterType;
	typedef itk::GrayscaleErodeImageFilter< ImageType, ImageType, StructuringElementType >  ErodeFilterType;
	StructuringElementType  structuringElement;
	structuringElement.SetRadius( 1 );  // 3x3 structuring element
	structuringElement.CreateStructuringElement();

	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;

	VectorFieldIteratorType it_VF(VF,VF->GetBufferedRegion());
	VectorFieldIteratorType it_extended_VF(extended_VF,extended_VF->GetBufferedRegion());
	ImageIteratorType it_mask_VF(mask_VF,mask_VF->GetBufferedRegion());
	typename ImageType::SizeType	size_dim_VF = mask_VF->GetBufferedRegion().GetSize();

	// Mask input VF
	typename VectorFieldImageType::Pointer masked_VF = VectorFieldImageType::New();
	masked_VF->SetRegions(VF->GetBufferedRegion());
	masked_VF->SetSpacing(VF->GetSpacing());
	masked_VF->SetOrigin(VF->GetOrigin());
	masked_VF->SetDirection(VF->GetDirection());
	masked_VF->Allocate();
	masked_VF->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

	typename ImageType::Pointer masked_VF_mask = ImageType::New();
	masked_VF_mask->SetRegions(VF->GetBufferedRegion());
	masked_VF_mask->SetSpacing(VF->GetSpacing());
	masked_VF_mask->SetOrigin(VF->GetOrigin());
	masked_VF_mask->SetDirection(VF->GetDirection());
	masked_VF_mask->Allocate();
	masked_VF_mask->FillBuffer(1);

	int masked_VF_done = MultiplyVectorFieldByScalarImage<VectorFieldImageType, ImageType>(	mask_VF, VF, masked_VF.GetPointer(),
		mask_VF, mask_VF, masked_VF_mask.GetPointer());
	int masked_VF_done02 = RemoveMaskedValues <VectorFieldImageType, ImageType> (	masked_VF.GetPointer(), masked_VF.GetPointer(), masked_VF_mask.GetPointer() );

	VectorFieldIteratorType it_masked_VF(masked_VF,masked_VF->GetBufferedRegion());

	if (N_it == 0)
	{
		CopyImage <VectorFieldImageType> ( masked_VF.GetPointer(), extended_VF );
	} 
	else
	{
		VectorFieldIteratorType it_extended_VF_tmp(extended_VF,extended_VF->GetBufferedRegion());
		VectorFieldIteratorType it_extended_VF_old(extended_VF_old,extended_VF_old->GetBufferedRegion());
		VectorFieldIteratorType it_exp_mask(exp_mask,exp_mask->GetBufferedRegion());

		for (unsigned int it=0;it<N_it;it++)
		{

			// extended_VF = where the data is updated
			// extended_VF_old = where the data is read

			if (it==0)
			{
				CopyImage <VectorFieldImageType> ( masked_VF.GetPointer(), extended_VF_old.GetPointer() );
				CopyImage <VectorFieldImageType> ( masked_VF.GetPointer(), extended_VF );
			} 
			else
			{
				CopyImage <VectorFieldImageType> ( extended_VF, extended_VF_old.GetPointer() );
			}

			// Create processing zone where data will be extended:
			// 1. new_mask = old_mask dilated by 1 pixel (struturing element 3x3)
			// 2. Processing zone = find( (new_mask - old_mask) == 1 )
			// 3. Updating mask: old_mask = new_mask

			typename DilateFilterType::Pointer dilateFilter = DilateFilterType::New();
			dilateFilter->SetKernel( structuringElement );
			dilateFilter->SetInput( mask_VF );
			dilateFilter->Update();
			typename ImageType::Pointer extended_maskVF = dilateFilter->GetOutput();

			typename ImageType::Pointer diff_maskVF = ImageType::New();
			diff_maskVF->SetRegions(mask_VF->GetBufferedRegion());
			diff_maskVF->SetSpacing(mask_VF->GetSpacing());
			diff_maskVF->SetOrigin(mask_VF->GetOrigin());
			diff_maskVF->SetDirection(mask_VF->GetDirection());
			diff_maskVF->Allocate();
			diff_maskVF->FillBuffer(itk::NumericTraits<PixelType>::ZeroValue());
			typename ImageType::Pointer diff_maskVF_mask = ImageType::New();
			diff_maskVF_mask->SetRegions(mask_VF->GetBufferedRegion());
			diff_maskVF_mask->SetSpacing(mask_VF->GetSpacing());
			diff_maskVF_mask->SetOrigin(mask_VF->GetOrigin());
			diff_maskVF_mask->SetDirection(mask_VF->GetDirection());
			diff_maskVF_mask->Allocate();
			diff_maskVF_mask->FillBuffer(1);

			int diff_maskVF_done = LocalOperationsImages <ImageType> ( extended_maskVF.GetPointer() ,
				mask_VF,
				diff_maskVF.GetPointer(),
				diff_maskVF_mask.GetPointer(),
				diff_maskVF_mask.GetPointer(),
				diff_maskVF_mask.GetPointer(),
				2 );

			//////////////////////////////////////////////////////////////////////////

			// Create processing zone where data will be looked at:
			// 1. new_mask = old_mask eroded by 1 pixel (struturing element 3x3)
			// 2. Processing zone = find( (new_mask - old_mask) == 1 )

			typename ErodeFilterType::Pointer erodeFilter = ErodeFilterType::New();
			erodeFilter->SetKernel( structuringElement );
			erodeFilter->SetInput( mask_VF );
			erodeFilter->Update();
			typename ImageType::Pointer eroded_maskVF = erodeFilter->GetOutput();

			typename ImageType::Pointer int_maskVF = ImageType::New();
			int_maskVF->SetRegions(mask_VF->GetBufferedRegion());
			int_maskVF->SetSpacing(mask_VF->GetSpacing());
			int_maskVF->SetOrigin(mask_VF->GetOrigin());
			int_maskVF->SetDirection(mask_VF->GetDirection());
			int_maskVF->Allocate();
			int_maskVF->FillBuffer(itk::NumericTraits<PixelType>::ZeroValue());
			typename ImageType::Pointer int_maskVF_mask = ImageType::New();
			int_maskVF_mask->SetRegions(mask_VF->GetBufferedRegion());
			int_maskVF_mask->SetSpacing(mask_VF->GetSpacing());
			int_maskVF_mask->SetOrigin(mask_VF->GetOrigin());
			int_maskVF_mask->SetDirection(mask_VF->GetDirection());
			int_maskVF_mask->Allocate();
			int_maskVF_mask->FillBuffer(1);

			int int_maskVF_done = LocalOperationsImages <ImageType> ( mask_VF ,
				eroded_maskVF.GetPointer(),
				int_maskVF.GetPointer(),
				int_maskVF_mask.GetPointer(),
				int_maskVF_mask.GetPointer(),
				int_maskVF_mask.GetPointer(),
				2 );

			//////////////////////////////////////////////////////////////////////////

			typename ErodeFilterType::Pointer erodeFilter2 = ErodeFilterType::New();
			erodeFilter2->SetKernel( structuringElement );
			erodeFilter2->SetInput( eroded_maskVF );
			erodeFilter2->Update();
			typename ImageType::Pointer eroded_maskVF2 = erodeFilter2->GetOutput();

			typename ImageType::Pointer int_maskVF2 = ImageType::New();
			int_maskVF2->SetRegions(mask_VF->GetBufferedRegion());
			int_maskVF2->SetSpacing(mask_VF->GetSpacing());
			int_maskVF2->SetOrigin(mask_VF->GetOrigin());
			int_maskVF2->SetDirection(mask_VF->GetDirection());
			int_maskVF2->Allocate();
			int_maskVF2->FillBuffer(itk::NumericTraits<PixelType>::ZeroValue());
			typename ImageType::Pointer int_maskVF2_mask = ImageType::New();
			int_maskVF2_mask->SetRegions(mask_VF->GetBufferedRegion());
			int_maskVF2_mask->SetSpacing(mask_VF->GetSpacing());
			int_maskVF2_mask->SetOrigin(mask_VF->GetOrigin());
			int_maskVF2_mask->SetDirection(mask_VF->GetDirection());
			int_maskVF2_mask->Allocate();
			int_maskVF2_mask->FillBuffer(1);

			int int_maskVF2_done = LocalOperationsImages <ImageType> ( eroded_maskVF.GetPointer() ,
				eroded_maskVF2.GetPointer(),
				int_maskVF2.GetPointer(),
				int_maskVF2_mask.GetPointer(),
				int_maskVF2_mask.GetPointer(),
				int_maskVF2_mask.GetPointer(),
				2 );


			//////////////////////////////////////////////////////////////////////////


			ImageIteratorType it_diff_maskVF(diff_maskVF,diff_maskVF->GetBufferedRegion());
			ImageIteratorType it_int_maskVF(int_maskVF,int_maskVF->GetBufferedRegion());
			ImageIteratorType it_int_maskVF2(int_maskVF2,int_maskVF2->GetBufferedRegion());

			for ( it_extended_VF_tmp.GoToBegin(), it_extended_VF_old.GoToBegin(), it_diff_maskVF.GoToBegin(); 
				! ( it_extended_VF_tmp.IsAtEnd() || it_extended_VF_old.IsAtEnd() || it_diff_maskVF.IsAtEnd() ); 
				++it_extended_VF_tmp, ++it_extended_VF_old, ++it_diff_maskVF )
			{

				if (it_diff_maskVF.Get()!=0)
				{
					typename VectorFieldImageType::IndexType index_VF = it_extended_VF_old.GetIndex();
					PixelType dist2 = (PixelType)((it+1)*(it+1));

					for (unsigned int dim=0;dim<ImageDimension;dim++)
					{
						std::vector <PixelType> pixels_list;
						std::vector <PixelType> pixels_list2;
						std::vector <PixelType> weights_list;
						std::vector <PixelType> weights_list2;
						it_mask_VF.SetIndex(index_VF);
						it_int_maskVF.SetIndex(index_VF);
						if ( (it_mask_VF.Get()!=0) && (it_int_maskVF.Get()!=0) )
						{
							pixels_list.push_back( it_extended_VF_old.Get()[dim] );
							weights_list.push_back( 1 );
						}

						// Neighbouring points

						for (int vx=-V_radius;vx<V_radius;vx++)
						{
							for (int vy=-V_radius;vy<V_radius;vy++)
							{
								typename VectorFieldImageType::IndexType index_VF_tmp = index_VF;
								index_VF_tmp[0] += vx;
								index_VF_tmp[1] += vy;

								bool valid_point = true;
								bool valid_point2 = true;
								int current_radius = vx*vx+vy*vy;
								// Check if inside circular neighbourhood
								if ( current_radius > (V_radius*V_radius) )
								{
									valid_point = false;
									valid_point2 = false;
								}
								// Check if inside image domain
								if ( (index_VF_tmp[0]<0) || (index_VF_tmp[0]>size_dim_VF[0]-1) )
								{
									valid_point = false;
									valid_point2 = false;
								}
								if ( (index_VF_tmp[1]<0) || (index_VF_tmp[1]>size_dim_VF[1]-1) )
								{
									valid_point = false;
									valid_point2 = false;
								}
								// Check if border point
								if (valid_point)
								{
									it_int_maskVF.SetIndex(index_VF_tmp);
									if ( it_int_maskVF.Get()==0 )
									{
										valid_point = false;
									}
								}
								if (valid_point2)
								{
									it_int_maskVF2.SetIndex(index_VF_tmp);
									if (it_int_maskVF2.Get()==0)
									{
										valid_point2 = false;
									}
								}

								if (valid_point)
								{
									it_extended_VF_old.SetIndex(index_VF_tmp);
									it_mask_VF.SetIndex(index_VF_tmp);
									pixels_list.push_back( it_extended_VF_old.Get()[dim] );
									// Weights for the mean over the neighbourhood (mean ponderated by a gaussian)
									PixelType exp_mean_coef = -((PixelType)current_radius / (2*(PixelType)V_radius*(PixelType)V_radius));
									weights_list.push_back( std::exp(exp_mean_coef) );
								}

								if (valid_point2)
								{
									it_extended_VF_old.SetIndex(index_VF_tmp);
									it_mask_VF.SetIndex(index_VF_tmp);
									pixels_list2.push_back( it_extended_VF_old.Get()[dim] );
									// Weights for the mean over the neighbourhood (mean ponderated by a gaussian)
									PixelType exp_mean_coef = -((PixelType)current_radius / (2*(PixelType)V_radius*(PixelType)V_radius));
									weights_list2.push_back( std::exp(exp_mean_coef) );
								}

							}
						}

						// Mean over the neighbourhood
						PixelType mean_value = itk::NumericTraits<PixelType>::ZeroValue();
						PixelType weights_sum = itk::NumericTraits<PixelType>::ZeroValue();
						for (unsigned int pixels_list_it=0;pixels_list_it<pixels_list.size();pixels_list_it++)
						{
							PixelType current_weight = weights_list.at(pixels_list_it);
							mean_value += pixels_list.at(pixels_list_it) * current_weight;
							weights_sum += current_weight;
						}
						if (pixels_list.size()>0)
						{
							mean_value /= weights_sum;
						}

						// Mean over the neighbourhood
						PixelType mean_value2 = itk::NumericTraits<PixelType>::ZeroValue();
						PixelType weights_sum2 = itk::NumericTraits<PixelType>::ZeroValue();
						for (unsigned int pixels_list_it2=0;pixels_list_it2<pixels_list2.size();pixels_list_it2++)
						{
							PixelType current_weight2 = weights_list2.at(pixels_list_it2);
							mean_value2 += pixels_list2.at(pixels_list_it2) * current_weight2;
							weights_sum2 += current_weight2;
						}
						if (pixels_list2.size()>0)
						{
							mean_value2 /= weights_sum2;
						}

						// Exponential decrease
						it_exp_mask.SetIndex(index_VF);
						PixelType exp_decrease_coef = -(dist2 / (2*(PixelType)sig_decrease*(PixelType)sig_decrease));
						VectorFieldPixelType valueToReplace_exp_mask = it_exp_mask.Get();
						valueToReplace_exp_mask[dim] = std::exp(exp_decrease_coef);
						it_exp_mask.Set(valueToReplace_exp_mask);

						PixelType diff_value = mean_value - mean_value2;
						PixelType diff_value_decreased = (mean_value + 0);
						// 					diff_value_decreased = (mean_value) * std::exp(exp_decrease_coef);

						it_extended_VF_old.SetIndex(index_VF);
						VectorFieldPixelType valueToReplace = it_extended_VF_tmp.Get();
						valueToReplace[dim] = diff_value_decreased;
						it_extended_VF_tmp.Set(valueToReplace);

					} // 	for (unsigned int dim=0;dim<ImageDimension;dim++)


				} // 	if (it_diff_maskVF.Get()!=0)

			} // for ( it_extended_VF_tmp.GoToBegin() ...

			ImageIteratorType it_extended_maskVF(extended_maskVF,extended_maskVF->GetBufferedRegion());
			for ( it_extended_maskVF.GoToBegin(), it_mask_VF.GoToBegin(); 
				! ( it_extended_maskVF.IsAtEnd() || it_mask_VF.IsAtEnd() ); 
				++it_extended_maskVF, ++it_mask_VF )
			{
				it_mask_VF.Set(it_extended_maskVF.Get());
			}


		} //	for (unsigned int it=0;it<N_it;it++)

	} // 	if (N_it == 0) ... else ...

	typename ImageType::Pointer extended_VF_oneMask = ImageType::New();
	extended_VF_oneMask->SetRegions(extended_VF->GetBufferedRegion());
	extended_VF_oneMask->SetSpacing(extended_VF->GetSpacing());
	extended_VF_oneMask->SetOrigin(extended_VF->GetOrigin());
	extended_VF_oneMask->SetDirection(extended_VF->GetDirection());
	extended_VF_oneMask->Allocate();
	extended_VF_oneMask->FillBuffer(1);


	int apply_exp_decrease = LocalOperationsVectorFields <VectorFieldImageType, ImageType> ( extended_VF, exp_mask.GetPointer(), extended_VF,
		extended_VF_oneMask.GetPointer(),
		extended_VF_oneMask.GetPointer(),
		extended_VF_oneMask.GetPointer(),
		3 );


	//////////////////////////////////////////////////////////////////////////

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType> 
typename VectorFieldImageType::Pointer 
ExtendVF_exp_decrease(	VectorFieldImageType* VF, 	unsigned int N_it, float sig_decrease, int V_radius)
{
	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	unsigned int ImageDimension = VF->GetImageDimension();

	typedef itk::Image<PixelType, ::itk::GetImageDimension<VectorFieldImageType>::ImageDimension> ImageType;

	typedef bool BoolPixelType;
	typedef itk::Image<BoolPixelType, ::itk::GetImageDimension<VectorFieldImageType>::ImageDimension> BoolImageType;

	unsigned int dim_3D = 0;
	if (ImageDimension == 3)
	{
		dim_3D = 1;
	}

	//////////////////////////////////////////////////////////////////////////
	typename ImageType::SpacingType spacing = VF->GetSpacing();

	typedef typename VectorFieldImageType::RegionType VectorFieldRegionType;
	typedef typename VectorFieldImageType::PointType VectorFieldOriginType;
	VectorFieldRegionType VF_region;
	VectorFieldOriginType VF_origin = VF->GetOrigin();
	typename VectorFieldRegionType::SizeType VF_region_size = VF->GetBufferedRegion().GetSize();
	for (unsigned int dim=0;dim<ImageDimension;dim++)
	{
		VF_region_size[dim] += 2*N_it;
		VF_origin[dim] -= N_it*spacing[dim];
	}
	VF_region.SetSize(VF_region_size);
	//////////////////////////////////////////////////////////////////////////

	// output image
	typename VectorFieldImageType::Pointer extended_VF = VectorFieldImageType::New();
	extended_VF->SetRegions(VF_region);
	extended_VF->SetSpacing(VF->GetSpacing());
	extended_VF->SetOrigin(VF_origin);
	extended_VF->SetDirection(VF->GetDirection());
	extended_VF->Allocate();
	extended_VF->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
	typename VectorFieldImageType::Pointer extended_VF_old = VectorFieldImageType::New();
	extended_VF_old->SetRegions(extended_VF->GetBufferedRegion());
	extended_VF_old->SetSpacing(extended_VF->GetSpacing());
	extended_VF_old->SetOrigin(extended_VF->GetOrigin());
	extended_VF_old->SetDirection(extended_VF->GetDirection());
	extended_VF_old->Allocate();
	extended_VF_old->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
	typename BoolImageType::Pointer mask_VF = BoolImageType::New();
	mask_VF->SetRegions(extended_VF->GetBufferedRegion());
	mask_VF->SetSpacing(extended_VF->GetSpacing());
	mask_VF->SetOrigin(extended_VF->GetOrigin());
	mask_VF->SetDirection(extended_VF->GetDirection());
	mask_VF->Allocate();
	mask_VF->FillBuffer(0.);

	// exponential mask
	typename ImageType::Pointer exp_mask = ImageType::New();
	exp_mask->SetRegions(extended_VF->GetBufferedRegion());
	exp_mask->SetSpacing(extended_VF->GetSpacing());
	exp_mask->SetOrigin(extended_VF->GetOrigin());
	exp_mask->SetDirection(extended_VF->GetDirection());
	exp_mask->Allocate();
	exp_mask->FillBuffer(1.);


	typename BoolImageType::Pointer Ones_mask = BoolImageType::New();
	Ones_mask->SetRegions(mask_VF->GetBufferedRegion());
	Ones_mask->SetSpacing(mask_VF->GetSpacing());
	Ones_mask->SetOrigin(mask_VF->GetOrigin());
	Ones_mask->SetDirection(mask_VF->GetDirection());
	Ones_mask->Allocate();
	Ones_mask->FillBuffer(1.);



	typedef itk::BinaryBallStructuringElement< BoolPixelType, ::itk::GetImageDimension<ImageType>::ImageDimension  > StructuringElementType;
	typedef itk::GrayscaleDilateImageFilter< BoolImageType, BoolImageType, StructuringElementType >  DilateFilterType;
	typedef itk::GrayscaleErodeImageFilter< BoolImageType, BoolImageType, StructuringElementType >  ErodeFilterType;
	StructuringElementType  structuringElement;
	structuringElement.SetRadius( 1 );  // 3x3 structuring element
	structuringElement.CreateStructuringElement();

	typedef itk::ImageRegionIterator<BoolImageType> BoolImageIteratorType;
	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;

	VectorFieldIteratorType it_VF(VF,VF->GetBufferedRegion());
	VectorFieldIteratorType it_extended_VF(extended_VF,extended_VF->GetBufferedRegion());
	BoolImageIteratorType it_mask_VF(mask_VF,mask_VF->GetBufferedRegion());
	typename BoolImageType::SizeType	size_dim_VF = mask_VF->GetBufferedRegion().GetSize();

	std::cout << std::endl <<"Filling new VF";
	for ( it_VF.GoToBegin(); 
		! ( it_VF.IsAtEnd() ); 
		++it_VF )
	{
		VectorFieldPixelType inputPoint = it_VF.Get();
		typename VectorFieldImageType::IndexType index_VF = it_VF.GetIndex();
		typename VectorFieldImageType::IndexType index_extended_VF;
		typename BoolImageType::IndexType index_mask_VF;
		for (unsigned int dim=0;dim<ImageDimension;dim++)
		{
			index_extended_VF[dim] = index_VF[dim] + N_it;
			index_mask_VF[dim] = index_VF[dim] + N_it;
		}
		it_extended_VF.SetIndex(index_extended_VF);
		it_extended_VF.Set(inputPoint);
		it_mask_VF.SetIndex(index_mask_VF);
		it_mask_VF.Set(1);
	}

	VectorFieldIteratorType it_extended_VF_tmp(extended_VF,extended_VF->GetBufferedRegion());
	VectorFieldIteratorType it_extended_VF_old(extended_VF_old,extended_VF_old->GetBufferedRegion());
	ImageIteratorType it_exp_mask(exp_mask,exp_mask->GetBufferedRegion());

	std::cout << std::endl << "Expanding... ";
	for (unsigned int it=0;it<N_it;it++)
	{
		std::cout << " " << it;

		// extended_VF = where the data is updated
		// extended_VF_old = where the data is read

		CopyImage <VectorFieldImageType> ( extended_VF, extended_VF_old.GetPointer() );

		// Create processing zone where data will be extended:
		// 1. new_mask = old_mask dilated by 1 pixel (struturing element 3x3)
		// 2. Processing zone = find( (new_mask - old_mask) == 1 )
		// 3. Updating mask: old_mask = new_mask

		// Mask where to update the data
		typename BoolImageType::Pointer diff_maskVF = BoolImageType::New();
		// Mask for "p"
		typename BoolImageType::Pointer int_maskVF = BoolImageType::New();
		// Mask wfor "p-1"
		typename BoolImageType::Pointer int_maskVF2 = BoolImageType::New();

		typename DilateFilterType::Pointer dilateFilter = DilateFilterType::New();
		dilateFilter->SetKernel( structuringElement );
		dilateFilter->SetInput( mask_VF );
		dilateFilter->Update();
		typename BoolImageType::Pointer extended_maskVF = dilateFilter->GetOutput();
		{
			{

				diff_maskVF->SetRegions(mask_VF->GetBufferedRegion());
				diff_maskVF->SetSpacing(mask_VF->GetSpacing());
				diff_maskVF->SetOrigin(mask_VF->GetOrigin());
				diff_maskVF->SetDirection(mask_VF->GetDirection());
				diff_maskVF->Allocate();
				diff_maskVF->FillBuffer(0);

				int diff_maskVF_done = LocalOperationsImages <BoolImageType> ( extended_maskVF.GetPointer() ,
					mask_VF.GetPointer(), diff_maskVF.GetPointer(),
					Ones_mask.GetPointer(),
					Ones_mask.GetPointer(),
					Ones_mask.GetPointer(),
					2 );
			}

			//////////////////////////////////////////////////////////////////////////

			// Create processing zone where data will be looked at:
			// 1. new_mask = old_mask eroded by 1 pixel (struturing element 3x3)
			// 2. Processing zone = find( (new_mask - old_mask) == 1 )

			typename ErodeFilterType::Pointer erodeFilter = ErodeFilterType::New();
			erodeFilter->SetKernel( structuringElement );
			erodeFilter->SetInput( mask_VF );
			erodeFilter->Update();
			typename BoolImageType::Pointer eroded_maskVF = erodeFilter->GetOutput();
			{
				int_maskVF->SetRegions(mask_VF->GetBufferedRegion());
				int_maskVF->SetSpacing(mask_VF->GetSpacing());
				int_maskVF->SetOrigin(mask_VF->GetOrigin());
				int_maskVF->SetDirection(mask_VF->GetDirection());
				int_maskVF->Allocate();
				int_maskVF->FillBuffer(0);

				int int_maskVF_done = LocalOperationsImages <BoolImageType> ( mask_VF.GetPointer() ,
					eroded_maskVF.GetPointer(),
					int_maskVF.GetPointer(),
					Ones_mask.GetPointer(),
					Ones_mask.GetPointer(),
					Ones_mask.GetPointer(),
					2 );
			}

			//////////////////////////////////////////////////////////////////////////

			{
				typename ErodeFilterType::Pointer erodeFilter2 = ErodeFilterType::New();
				erodeFilter2->SetKernel( structuringElement );
				erodeFilter2->SetInput( eroded_maskVF );
				erodeFilter2->Update();
				typename BoolImageType::Pointer eroded_maskVF2 = erodeFilter2->GetOutput();

				int_maskVF2->SetRegions(mask_VF->GetBufferedRegion());
				int_maskVF2->SetSpacing(mask_VF->GetSpacing());
				int_maskVF2->SetOrigin(mask_VF->GetOrigin());
				int_maskVF2->SetDirection(mask_VF->GetDirection());
				int_maskVF2->Allocate();
				int_maskVF2->FillBuffer(0);

				int int_maskVF2_done = LocalOperationsImages <BoolImageType> ( eroded_maskVF.GetPointer() ,
					eroded_maskVF2.GetPointer(),
					int_maskVF2.GetPointer(),
					Ones_mask.GetPointer(),
					Ones_mask.GetPointer(),
					Ones_mask.GetPointer(),
					2 );
			}


		}

		//////////////////////////////////////////////////////////////////////////


		// Compute distance between to-be-filled point and original image
		int neighborhoude = V_radius*V_radius;
		PixelType dist2 = (it+1)*(it+1);
		PixelType exp_decrease_coef = -((PixelType)dist2 / (2*(PixelType)sig_decrease*(PixelType)sig_decrease));
		exp_decrease_coef = std::exp( float( exp_decrease_coef ) );

		BoolImageIteratorType it_diff_maskVF(diff_maskVF,diff_maskVF->GetBufferedRegion());
		BoolImageIteratorType it_int_maskVF(int_maskVF,int_maskVF->GetBufferedRegion());
		BoolImageIteratorType it_int_maskVF2(int_maskVF2,int_maskVF2->GetBufferedRegion());

		for ( it_extended_VF_tmp.GoToBegin(), it_extended_VF_old.GoToBegin(), it_diff_maskVF.GoToBegin(); 
			! ( it_extended_VF_tmp.IsAtEnd() || it_extended_VF_old.IsAtEnd() || it_diff_maskVF.IsAtEnd() ); 
			++it_extended_VF_tmp, ++it_extended_VF_old, ++it_diff_maskVF )
		{

			if (it_diff_maskVF.Get()!=0)
			{
				typename VectorFieldImageType::IndexType index_VF = it_extended_VF_old.GetIndex();


				for (unsigned int dim=0;dim<ImageDimension;dim++)
				{
					std::vector <PixelType> pixels_list;
					std::vector <PixelType> weights_list;
					std::vector <PixelType> pixels_list2;
					std::vector <PixelType> weights_list2;
					it_mask_VF.SetIndex(index_VF);
					it_int_maskVF.SetIndex(index_VF);
					if ( (it_mask_VF.Get()!=0) && (it_int_maskVF.Get()!=0) )
					{
						pixels_list.push_back( it_extended_VF_old.Get()[dim] );
						weights_list.push_back( 1 );
					}

					// Neighbouring points


					for (int vx=-V_radius+1;vx<V_radius;vx++)
					{
						for (int vy=-V_radius+1;vy<V_radius;vy++)
						{
							int vz = 0;
							if (dim_3D){ vz = -V_radius+1; }

							while (vz<V_radius)
							{
								typename VectorFieldImageType::IndexType index_VF_tmp = index_VF;
								index_VF_tmp[0] += vx;
								index_VF_tmp[1] += vy;
								int current_radius = (vx*vx+vy*vy);
								if (dim_3D)
								{
									index_VF_tmp[2] += vz;
									current_radius += vz*vz;
								}

								bool valid_point = true;
								bool valid_point2 = true;

								// Check if inside circular neighbourhood

								if (current_radius > (neighborhoude) )
								{
									valid_point = false;
									valid_point2 = false;
								}
								// Check if inside image domain
								else if ( (index_VF_tmp[0]<0) || (index_VF_tmp[0]>size_dim_VF[0]-1) ||
									(index_VF_tmp[1]<0) || (index_VF_tmp[1]>size_dim_VF[1]-1) )
								{
									valid_point = false;
									valid_point2 = false;
								}
								else if ( (dim_3D) && ((index_VF_tmp[2]<0) || (index_VF_tmp[2]>size_dim_VF[2]-1)) )
								{
									valid_point = false;
									valid_point2 = false;
								}
								// Check if border point
								if (valid_point)
								{
									it_int_maskVF.SetIndex(index_VF_tmp);
									if ( it_int_maskVF.Get()==0 )
									{
										valid_point = false;
									}
								}
								if (valid_point2)
								{
									it_int_maskVF2.SetIndex(index_VF_tmp);
									if (it_int_maskVF2.Get()==0)
									{
										valid_point2 = false;
									}
								}
								if (valid_point)
								{
									it_extended_VF_old.SetIndex(index_VF_tmp);
									it_mask_VF.SetIndex(index_VF_tmp);
									pixels_list.push_back( it_extended_VF_old.Get()[dim] );
									// Weights for the mean over the neighbourhood (mean ponderated by a gaussian)
									PixelType exp_mean_coef = -((PixelType)current_radius / (2*(PixelType)neighborhoude));
									weights_list.push_back( std::exp(float( exp_mean_coef )) );
								}

								if (valid_point2)
								{
									it_extended_VF_old.SetIndex(index_VF_tmp);
									it_mask_VF.SetIndex(index_VF_tmp);
									pixels_list2.push_back( it_extended_VF_old.Get()[dim] );
									// Weights for the mean over the neighbourhood (mean ponderated by a gaussian)
									PixelType exp_mean_coef = -((PixelType)current_radius / (2*(PixelType)neighborhoude));
									weights_list2.push_back( std::exp(float( exp_mean_coef) ) );
								}

								if (dim_3D){ vz++; }
								else { vz = V_radius; }

							} // while (vz<V_radius)
						} // for (int vy=-V_radius;vy<V_radius;vy++)
					} // for (int vx=-V_radius;vx<V_radius;vx++)

					// Mean over the neighbourhood
					PixelType mean_value = itk::NumericTraits<PixelType>::ZeroValue();
					PixelType weights_sum = itk::NumericTraits<PixelType>::ZeroValue();
					for (unsigned int pixels_list_it=0;pixels_list_it<pixels_list.size();pixels_list_it++)
					{
						PixelType current_weight = weights_list.at(pixels_list_it);
						mean_value += pixels_list.at(pixels_list_it) * current_weight;
						weights_sum += current_weight;
					}
					if (pixels_list.size()>0)
					{
						mean_value /= weights_sum;
					}

					// Mean over the neighbourhood
					PixelType mean_value2 = itk::NumericTraits<PixelType>::ZeroValue();
					PixelType weights_sum2 = itk::NumericTraits<PixelType>::ZeroValue();
					for (unsigned int pixels_list_it2=0;pixels_list_it2<pixels_list2.size();pixels_list_it2++)
					{
						PixelType current_weight2 = weights_list2.at(pixels_list_it2);
						mean_value2 += pixels_list2.at(pixels_list_it2) * current_weight2;
						weights_sum2 += current_weight2;
					}
					if (pixels_list2.size()>0)
					{
						mean_value2 /= weights_sum2;
					}

					// Exponential decrease
					if (dim==0)
					{
						it_exp_mask.SetIndex(index_VF);
						it_exp_mask.Set( exp_decrease_coef );
					}

					PixelType diff_value = mean_value - mean_value2;
// 					PixelType diff_value_decreased = (mean_value + diff_value);
					PixelType diff_value_decreased = mean_value;
					// 					diff_value_decreased = (mean_value) * std::exp(exp_decrease_coef);

					it_extended_VF_old.SetIndex(index_VF);
					VectorFieldPixelType valueToReplace = it_extended_VF_tmp.Get();
					valueToReplace[dim] = diff_value_decreased;
					it_extended_VF_tmp.Set(valueToReplace);

				} // 	for (unsigned int dim=0;dim<ImageDimension;dim++)


			} // 	if (it_diff_maskVF.Get()!=0)

		} // for ( it_extended_VF_tmp.GoToBegin() ...

		CopyImage <BoolImageType> ( extended_maskVF.GetPointer(), mask_VF.GetPointer() );

	} //	for (unsigned int it=0;it<N_it;it++)

	std::cout << std::endl;


	typename ImageType::Pointer IOnes_mask = ImageType::New();
	IOnes_mask->SetRegions(mask_VF->GetBufferedRegion());
	IOnes_mask->SetSpacing(mask_VF->GetSpacing());
	IOnes_mask->SetOrigin(mask_VF->GetOrigin());
	IOnes_mask->SetDirection(mask_VF->GetDirection());
	IOnes_mask->Allocate();
	IOnes_mask->FillBuffer(1.);

	int apply_exp_decrease = MultiplyVectorFieldByScalarImage <VectorFieldImageType, ImageType> (	exp_mask.GetPointer(), extended_VF.GetPointer(), extended_VF.GetPointer(),
		IOnes_mask.GetPointer(),
		IOnes_mask.GetPointer(),
		IOnes_mask.GetPointer());


	//////////////////////////////////////////////////////////////////////////


	return extended_VF;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType, class ImageType> 
int ComputeSqrtPhi(VectorFieldImageType* Phi_Image, VectorFieldImageType* T_Image,
				   float deltaIterations_inv,
				   unsigned int maxNumberOfIterations_inv,
				   double rho_gradientDescent_inv,
				   float deltaIterations_sqrt,
				   unsigned int maxNumberOfIterations_sqrt,
				   double rho_gradientDescent_sqrt,
				   ImageType* Phi_Image_mask,
				   ImageType* T_Image_mask,
				   std::string & energyValue)
{
	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	typedef itk::Matrix<PixelType,
		::itk::GetImageDimension<VectorFieldImageType>::ImageDimension,
		::itk::GetImageDimension<VectorFieldImageType>::ImageDimension> JacobianVectorFieldPixelType;
	typedef itk::Image<JacobianVectorFieldPixelType,
		::itk::GetImageDimension<VectorFieldImageType>::ImageDimension> JacobianVectorFieldImageType;

	unsigned int ImageDimension = Phi_Image->GetImageDimension();

	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_T(T_Image,T_Image->GetBufferedRegion());
	VectorFieldIteratorType it_Phi(Phi_Image,Phi_Image->GetBufferedRegion());

	//////////////////////////////////////////////////////////////////////////

	// Initialization: T = 1/2 (Phi - Id). Given a transformation V, V-Id corresponds to its displacement field.

// 	{
// 		typedef itk::ImageFileWriter<VectorFieldImageType> VFWriterType;
// 		VFWriterType::Pointer writer_VF = VFWriterType::New();
// 		writer_VF->SetInput(Phi_Image);
// 		writer_VF->SetFileName("E:\\bk-up\\tests_Corne\\atlasStuff\\testVF_01Phi.mhd");
// 		try{writer_VF->Update();}
// 		catch ( itk::ExceptionObject & err )	{
// 			std::cerr << "ExceptionObject caught !" << std::endl;
// 			std::cerr << err << std::endl;}
// 	}


	int T_Image_initdone = ScalarMultiplyVectorField <VectorFieldImageType, ImageType> ( Phi_Image, T_Image, 0.5, Phi_Image_mask, T_Image_mask );

	//////////////////////////////////////////////////////////////////////////

	double diffMagnitude_1st = 1.;
	double diffMagnitude_scaled = deltaIterations_sqrt + 1;
	double diffMagnitude = 0.;
	unsigned int it = 0;

	// Starting iterations
	while( (diffMagnitude_scaled > deltaIterations_sqrt) && (it < maxNumberOfIterations_sqrt) )
	{
// 		if (it==0)
// 		{
// 			typedef itk::ImageFileWriter<VectorFieldImageType> VFWriterType;
// 			VFWriterType::Pointer writer_VF = VFWriterType::New();
// 			writer_VF->SetInput(T_Image);
// 			writer_VF->SetFileName("E:\\bk-up\\tests_Corne\\atlasStuff\\testVF_01T.mhd");
// 			try{writer_VF->Update();}
// 			catch ( itk::ExceptionObject & err )	{
// 				std::cerr << "ExceptionObject caught !" << std::endl;
// 				std::cerr << err << std::endl;}
// 
// 			typedef itk::ImageFileWriter<ImageType> IWriterType;
// 			IWriterType::Pointer writer_I = IWriterType::New();
// 			writer_I->SetInput(T_Image_mask);
// 			writer_I->SetFileName("E:\\bk-up\\tests_Corne\\atlasStuff\\testVF_01Tm.mhd");
// 			try{writer_I->Update();}
// 			catch ( itk::ExceptionObject & err )	{
// 				std::cerr << "ExceptionObject caught !" << std::endl;
// 				std::cerr << err << std::endl;}
// 		}

		//////////////////////////////////////////////////////////////////////////

		// Computing Jacobian D(T)

		typename JacobianVectorFieldImageType::Pointer DT_Image = JacobianVectorFieldImageType::New();
		DT_Image->SetRegions(Phi_Image->GetBufferedRegion());
		DT_Image->SetSpacing(Phi_Image->GetSpacing());
		DT_Image->SetOrigin(Phi_Image->GetOrigin());
		DT_Image->SetDirection(Phi_Image->GetDirection());
		DT_Image->Allocate();
		typename ImageType::Pointer DT_Image_mask = ImageType::New();
		DT_Image_mask->SetRegions(Phi_Image->GetBufferedRegion());
		DT_Image_mask->SetSpacing(Phi_Image->GetSpacing());
		DT_Image_mask->SetOrigin(Phi_Image->GetOrigin());
		DT_Image_mask->SetDirection(Phi_Image->GetDirection());
		DT_Image_mask->Allocate();
		DT_Image_mask->FillBuffer(1.);

		int DT_Image_computed = ComputeJacobianVectorField <VectorFieldImageType , JacobianVectorFieldImageType, ImageType> ( T_Image, T_Image_mask, DT_Image.GetPointer(), DT_Image_mask.GetPointer() );

		//////////////////////////////////////////////////////////////////////////

		// Computing (D^t(T)) o T

		typename JacobianVectorFieldImageType::Pointer DtTtoT_Image = JacobianVectorFieldImageType::New();
		DtTtoT_Image->SetRegions(Phi_Image->GetBufferedRegion());
		DtTtoT_Image->SetSpacing(Phi_Image->GetSpacing());
		DtTtoT_Image->SetOrigin(Phi_Image->GetOrigin());
		DtTtoT_Image->SetDirection(Phi_Image->GetDirection());
		DtTtoT_Image->Allocate();
		typename ImageType::Pointer DtTtoT_Image_mask = ImageType::New();
		DtTtoT_Image_mask->SetRegions(Phi_Image->GetBufferedRegion());
		DtTtoT_Image_mask->SetSpacing(Phi_Image->GetSpacing());
		DtTtoT_Image_mask->SetOrigin(Phi_Image->GetOrigin());
		DtTtoT_Image_mask->SetDirection(Phi_Image->GetDirection());
		DtTtoT_Image_mask->Allocate();
		DtTtoT_Image_mask->FillBuffer(1.);

		int DtTtoT_Image_done = ComposeVectorFieldWithJacobianTranspose <VectorFieldImageType,JacobianVectorFieldImageType,ImageType> (	T_Image, DT_Image.GetPointer(), DtTtoT_Image.GetPointer(), T_Image_mask, DT_Image_mask.GetPointer(), DtTtoT_Image_mask.GetPointer());

// 		if (it==1)
// 		{
// 			typedef itk::ImageFileWriter<JacobianVectorFieldImageType> VFWriterType;
// 			VFWriterType::Pointer writer_VF = VFWriterType::New();
// 			writer_VF->SetInput(DtTtoT_Image.GetPointer());
// 			writer_VF->SetFileName("E:\\bk-up\\tests_Corne\\atlasStuff\\testVF_01DtTtoT.mhd");
// 			try{writer_VF->Update();}
// 			catch ( itk::ExceptionObject & err )	{
// 				std::cerr << "ExceptionObject caught !" << std::endl;
// 				std::cerr << err << std::endl;}
// 
// 			typedef itk::ImageFileWriter<ImageType> IWriterType;
// 			IWriterType::Pointer writer_I = IWriterType::New();
// 			writer_I->SetInput(DtTtoT_Image_mask.GetPointer());
// 			writer_I->SetFileName("E:\\bk-up\\tests_Corne\\atlasStuff\\testVF_01DtTtoTm.mhd");
// 			try{writer_I->Update();}
// 			catch ( itk::ExceptionObject & err )	{
// 				std::cerr << "ExceptionObject caught !" << std::endl;
// 				std::cerr << err << std::endl;}
// 
// 		}


		// 		DT_Image->Delete();
		// 		DT_Image_mask->Delete();

		//////////////////////////////////////////////////////////////////////////

		// Computing ( T o T )

		typename VectorFieldImageType::Pointer ToT_Image = VectorFieldImageType::New();
		ToT_Image->SetRegions(Phi_Image->GetBufferedRegion());
		ToT_Image->SetSpacing(Phi_Image->GetSpacing());
		ToT_Image->SetOrigin(Phi_Image->GetOrigin());
		ToT_Image->SetDirection(Phi_Image->GetDirection());
		ToT_Image->Allocate();
		ToT_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
		typename ImageType::Pointer ToT_Image_mask = ImageType::New();
		ToT_Image_mask->SetRegions(Phi_Image->GetBufferedRegion());
		ToT_Image_mask->SetSpacing(Phi_Image->GetSpacing());
		ToT_Image_mask->SetOrigin(Phi_Image->GetOrigin());
		ToT_Image_mask->SetDirection(Phi_Image->GetDirection());
		ToT_Image_mask->Allocate();
		ToT_Image_mask->FillBuffer(1.);

		int ToT_Image_done = ComposeVectorFields <VectorFieldImageType, ImageType> (	T_Image, T_Image, ToT_Image.GetPointer(), T_Image_mask, T_Image_mask, ToT_Image_mask.GetPointer() );

// 		if (it==1)
// 		{
// 			typedef itk::ImageFileWriter<VectorFieldImageType> VFWriterType;
// 			VFWriterType::Pointer writer_VF = VFWriterType::New();
// 			writer_VF->SetInput(ToT_Image.GetPointer());
// 			writer_VF->SetFileName("E:\\bk-up\\tests_Corne\\atlasStuff\\testVF_01ToT.mhd");
// 			try{writer_VF->Update();}
// 			catch ( itk::ExceptionObject & err )	{
// 				std::cerr << "ExceptionObject caught !" << std::endl;
// 				std::cerr << err << std::endl;}
// 
// 			typedef itk::ImageFileWriter<ImageType> IWriterType;
// 			IWriterType::Pointer writer_I = IWriterType::New();
// 			writer_I->SetInput(ToT_Image_mask.GetPointer());
// 			writer_I->SetFileName("E:\\bk-up\\tests_Corne\\atlasStuff\\testVF_01ToTm.mhd");
// 			try{writer_I->Update();}
// 			catch ( itk::ExceptionObject & err )	{
// 				std::cerr << "ExceptionObject caught !" << std::endl;
// 				std::cerr << err << std::endl;}
// 		}

		//////////////////////////////////////////////////////////////////////////

		// Computing (T o T - Phi)

		typename VectorFieldImageType::Pointer ToTMinusPhi_Image = VectorFieldImageType::New();
		ToTMinusPhi_Image->SetRegions(Phi_Image->GetBufferedRegion());
		ToTMinusPhi_Image->SetSpacing(Phi_Image->GetSpacing());
		ToTMinusPhi_Image->SetOrigin(Phi_Image->GetOrigin());
		ToTMinusPhi_Image->SetDirection(Phi_Image->GetDirection());
		ToTMinusPhi_Image->Allocate();
		ToTMinusPhi_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
		typename ImageType::Pointer ToTMinusPhi_Image_mask = ImageType::New();
		ToTMinusPhi_Image_mask->SetRegions(Phi_Image->GetBufferedRegion());
		ToTMinusPhi_Image_mask->SetSpacing(Phi_Image->GetSpacing());
		ToTMinusPhi_Image_mask->SetOrigin(Phi_Image->GetOrigin());
		ToTMinusPhi_Image_mask->SetDirection(Phi_Image->GetDirection());
		ToTMinusPhi_Image_mask->Allocate();
		ToTMinusPhi_Image_mask->FillBuffer(1.);

		int ToTMinusPhi_Image_done = LocalOperationsVectorFields <VectorFieldImageType, ImageType> ( ToT_Image.GetPointer() ,
			Phi_Image, ToTMinusPhi_Image.GetPointer(),
			ToT_Image_mask.GetPointer(),
			Phi_Image_mask,
			ToTMinusPhi_Image_mask.GetPointer(),
			2 );

// 		if (it==1)
// 		{
// 			typedef itk::ImageFileWriter<VectorFieldImageType> VFWriterType;
// 			VFWriterType::Pointer writer_VF = VFWriterType::New();
// 			writer_VF->SetInput(ToTMinusPhi_Image.GetPointer());
// 			writer_VF->SetFileName("E:\\bk-up\\tests_Corne\\atlasStuff\\testVF_01ToTmPhi.mhd");
// 			try{writer_VF->Update();}
// 			catch ( itk::ExceptionObject & err )	{
// 				std::cerr << "ExceptionObject caught !" << std::endl;
// 				std::cerr << err << std::endl;}
// 
// 			typedef itk::ImageFileWriter<ImageType> IWriterType;
// 			IWriterType::Pointer writer_I = IWriterType::New();
// 			writer_I->SetInput(ToTMinusPhi_Image_mask.GetPointer());
// 			writer_I->SetFileName("E:\\bk-up\\tests_Corne\\atlasStuff\\testVF_01ToTmPm.mhd");
// 			try{writer_I->Update();}
// 			catch ( itk::ExceptionObject & err )	{
// 				std::cerr << "ExceptionObject caught !" << std::endl;
// 				std::cerr << err << std::endl;}
// 		}


		//////////////////////////////////////////////////////////////////////////

		double norm_ToTMinusPhi = ComputeNormVectorField <VectorFieldImageType, ImageType> (ToTMinusPhi_Image.GetPointer(), ToTMinusPhi_Image_mask.GetPointer());

		// 		ToT_Image->Delete();
		// 		ToT_Image_mask->Delete();

		//////////////////////////////////////////////////////////////////////////

		// Computing (D^t(T) o T) * ( T o T - Phi )

		typename VectorFieldImageType::Pointer DtToTtimesDiff_Image = VectorFieldImageType::New();
		DtToTtimesDiff_Image->SetRegions(Phi_Image->GetBufferedRegion());
		DtToTtimesDiff_Image->SetSpacing(Phi_Image->GetSpacing());
		DtToTtimesDiff_Image->SetOrigin(Phi_Image->GetOrigin());
		DtToTtimesDiff_Image->SetDirection(Phi_Image->GetDirection());
		DtToTtimesDiff_Image->Allocate();
		DtToTtimesDiff_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
		typename ImageType::Pointer DtToTtimesDiff_Image_mask = ImageType::New();
		DtToTtimesDiff_Image_mask->SetRegions(Phi_Image->GetBufferedRegion());
		DtToTtimesDiff_Image_mask->SetSpacing(Phi_Image->GetSpacing());
		DtToTtimesDiff_Image_mask->SetOrigin(Phi_Image->GetOrigin());
		DtToTtimesDiff_Image_mask->SetDirection(Phi_Image->GetDirection());
		DtToTtimesDiff_Image_mask->Allocate();
		DtToTtimesDiff_Image_mask->FillBuffer(1.);

		int DtToTtimesDiff_Image_done = MultiplyJacobianByVectorField <VectorFieldImageType,JacobianVectorFieldImageType,ImageType> ( ToTMinusPhi_Image.GetPointer() , DtTtoT_Image.GetPointer(), DtToTtimesDiff_Image.GetPointer(), ToTMinusPhi_Image_mask.GetPointer(), DtTtoT_Image_mask.GetPointer(), DtToTtimesDiff_Image_mask.GetPointer());

		// 		DtTtoT_Image->Delete();
		// 		DtTtoT_Image_mask->Delete();

		//////////////////////////////////////////////////////////////////////////

		typename VectorFieldImageType::Pointer GradientDescent_Image = VectorFieldImageType::New();
		GradientDescent_Image->SetRegions(Phi_Image->GetBufferedRegion());
		GradientDescent_Image->SetSpacing(Phi_Image->GetSpacing());
		GradientDescent_Image->SetOrigin(Phi_Image->GetOrigin());
		GradientDescent_Image->SetDirection(Phi_Image->GetDirection());
		GradientDescent_Image->Allocate();
		GradientDescent_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
		typename ImageType::Pointer GradientDescent_Image_mask = ImageType::New();
		GradientDescent_Image_mask->SetRegions(Phi_Image->GetBufferedRegion());
		GradientDescent_Image_mask->SetSpacing(Phi_Image->GetSpacing());
		GradientDescent_Image_mask->SetOrigin(Phi_Image->GetOrigin());
		GradientDescent_Image_mask->SetDirection(Phi_Image->GetDirection());
		GradientDescent_Image_mask->Allocate();
		GradientDescent_Image_mask->FillBuffer(1.);

		//////////////////////////////////////////////////////////////////////////

		// Compute (D^t(T) o T) * ( T o T - Phi ) - ( -( T o T - Phi ) )
		int GradientDescent_Image_done = LocalOperationsVectorFields <VectorFieldImageType, ImageType> ( DtToTtimesDiff_Image.GetPointer() , ToTMinusPhi_Image.GetPointer(), GradientDescent_Image.GetPointer(), DtToTtimesDiff_Image_mask.GetPointer(), ToTMinusPhi_Image_mask.GetPointer(), GradientDescent_Image_mask.GetPointer(), 1 );

		// 		ToTMinusPhi_Image->Delete();
		// 		ToTMinusPhi_Image_mask->Delete();
		// 		DtToTtimesDiff_Image->Delete();
		// 		DtToTtimesDiff_Image_mask->Delete();

		//////////////////////////////////////////////////////////////////////////

		// Gradient descent
		diffMagnitude = norm_ToTMinusPhi;
		if (it==0)
		{
			diffMagnitude_1st = diffMagnitude;
		}
		diffMagnitude_scaled = diffMagnitude / diffMagnitude_1st;
		std::cout << "SQRT: " << it << ": " << diffMagnitude_scaled << std::endl;
// 		std::cout << "SQRT: " << it << ": " << diffMagnitude_scaled << " ...   " << diffMagnitude << std::endl;
		std::ostringstream energyValue_Os;
		energyValue_Os << diffMagnitude_scaled;
		energyValue = energyValue_Os.str();

		int GradientDescent_Image_done02 = ScalarMultiplyVectorField <VectorFieldImageType,ImageType> (	GradientDescent_Image.GetPointer(), GradientDescent_Image.GetPointer(),	rho_gradientDescent_sqrt, GradientDescent_Image_mask.GetPointer(), GradientDescent_Image_mask.GetPointer() );

		int T_Image_done = LocalOperationsVectorFields <VectorFieldImageType, ImageType> ( T_Image , GradientDescent_Image.GetPointer(), T_Image,
			T_Image_mask, GradientDescent_Image_mask.GetPointer(), T_Image_mask, 2 );


		// 		GradientDescent_Image->Delete();
		// 		GradientDescent_Image_mask->Delete();

		it++;

	}

	return EXIT_SUCCESS;

}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType, class ImageType> 
int ComputeInversePhi(VectorFieldImageType* Phi_Image,
					  VectorFieldImageType* T_Image,
					  float deltaIterations,
					  unsigned int maxNumberOfIterations,
					  double rho_gradientDescent,
					  ImageType* Phi_Image_mask,
					  ImageType* T_Image_mask,
					  std::string & energyValue)
{
	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	unsigned int ImageDimension = Phi_Image->GetImageDimension();

	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_T(T_Image,T_Image->GetBufferedRegion());
	VectorFieldIteratorType it_Phi(Phi_Image,Phi_Image->GetBufferedRegion());

	typedef itk::Matrix<float,
		::itk::GetImageDimension<VectorFieldImageType>::ImageDimension,
		::itk::GetImageDimension<VectorFieldImageType>::ImageDimension> JacobianVectorFieldPixelType;
	typedef itk::Image<JacobianVectorFieldPixelType,
		::itk::GetImageDimension<VectorFieldImageType>::ImageDimension> JacobianVectorFieldImageType;

	//////////////////////////////////////////////////////////////////////////

	// Initialization: T = - (Phi - Id). Given a transformation V, V-Id corresponds to its displacement field.

	int T_Image_scal = ScalarMultiplyVectorField <VectorFieldImageType, ImageType> ( Phi_Image, T_Image, -1, Phi_Image_mask, T_Image_mask );

	//////////////////////////////////////////////////////////////////////////

	double diffMagnitude_1st = 1.;
	double diffMagnitude_scaled = deltaIterations + 1;
	double diffMagnitude = 0.;
	unsigned int it = 0;

	// Starting iterations
	while( (diffMagnitude_scaled > deltaIterations) && (it < maxNumberOfIterations) )
	{
		//////////////////////////////////////////////////////////////////////////

		// Computing ( Phi o T - Id )

		typename VectorFieldImageType::Pointer PhioT_Image = VectorFieldImageType::New();
		PhioT_Image->SetRegions(Phi_Image->GetBufferedRegion());
		PhioT_Image->SetSpacing(Phi_Image->GetSpacing());
		PhioT_Image->SetOrigin(Phi_Image->GetOrigin());
		PhioT_Image->SetDirection(Phi_Image->GetDirection());
		PhioT_Image->Allocate();
		PhioT_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
		typename ImageType::Pointer PhioT_Image_mask = ImageType::New();
		PhioT_Image_mask->SetRegions(Phi_Image->GetBufferedRegion());
		PhioT_Image_mask->SetSpacing(Phi_Image->GetSpacing());
		PhioT_Image_mask->SetOrigin(Phi_Image->GetOrigin());
		PhioT_Image_mask->SetDirection(Phi_Image->GetDirection());
		PhioT_Image_mask->Allocate();
		PhioT_Image_mask->FillBuffer(1);

		int PhioT_Image_done = ComposeVectorFields <VectorFieldImageType, ImageType> (	T_Image, Phi_Image, PhioT_Image.GetPointer(), T_Image_mask, Phi_Image_mask, PhioT_Image_mask.GetPointer() );

		//////////////////////////////////////////////////////////////////////////

		double sum = 0;
		ImageIteratorType it_PhioT_Image_mask(PhioT_Image_mask,PhioT_Image_mask->GetBufferedRegion());
		VectorFieldIteratorType it_PhioT(PhioT_Image,PhioT_Image->GetBufferedRegion());
		for ( it_PhioT.GoToBegin(), it_PhioT_Image_mask.GoToBegin(); 
			! ( it_PhioT.IsAtEnd() || it_PhioT_Image_mask.IsAtEnd() ); 
			++it_PhioT, ++it_PhioT_Image_mask)
		{
			VectorFieldPixelType Point_PhioT = it_PhioT.Get();

			for (unsigned int dim=0; dim<ImageDimension; dim++)
			{
				if (it_PhioT_Image_mask.Get() == 1)
				{
					sum += Point_PhioT[dim] * Point_PhioT[dim];
				}
			}
		}
		double norm_PhioT = sqrt(sum);


		//////////////////////////////////////////////////////////////////////////

		// Computing Jacobian D(Phi)

		typename JacobianVectorFieldImageType::Pointer DPhi_Image = JacobianVectorFieldImageType::New();
		DPhi_Image->SetRegions(Phi_Image->GetBufferedRegion());
		DPhi_Image->SetSpacing(Phi_Image->GetSpacing());
		DPhi_Image->SetOrigin(Phi_Image->GetOrigin());
		DPhi_Image->SetDirection(Phi_Image->GetDirection());
		DPhi_Image->Allocate();
		typename ImageType::Pointer DPhi_Image_mask = ImageType::New();
		DPhi_Image_mask->SetRegions(Phi_Image->GetBufferedRegion());
		DPhi_Image_mask->SetSpacing(Phi_Image->GetSpacing());
		DPhi_Image_mask->SetOrigin(Phi_Image->GetOrigin());
		DPhi_Image_mask->SetDirection(Phi_Image->GetDirection());
		DPhi_Image_mask->Allocate();
		DPhi_Image_mask->FillBuffer(1);

		int DPhi_Image_computed = ComputeJacobianVectorField <VectorFieldImageType , JacobianVectorFieldImageType, ImageType> ( Phi_Image, Phi_Image_mask, DPhi_Image.GetPointer(), DPhi_Image_mask.GetPointer() );


		//////////////////////////////////////////////////////////////////////////

		// Computing (D^t(Phi)) o T

		typename JacobianVectorFieldImageType::Pointer DtPhitoT_Image = JacobianVectorFieldImageType::New();
		DtPhitoT_Image->SetRegions(Phi_Image->GetBufferedRegion());
		DtPhitoT_Image->SetSpacing(Phi_Image->GetSpacing());
		DtPhitoT_Image->SetOrigin(Phi_Image->GetOrigin());
		DtPhitoT_Image->SetDirection(Phi_Image->GetDirection());
		DtPhitoT_Image->Allocate();
		typename ImageType::Pointer DtPhitoT_Image_mask = ImageType::New();
		DtPhitoT_Image_mask->SetRegions(Phi_Image->GetBufferedRegion());
		DtPhitoT_Image_mask->SetSpacing(Phi_Image->GetSpacing());
		DtPhitoT_Image_mask->SetOrigin(Phi_Image->GetOrigin());
		DtPhitoT_Image_mask->SetDirection(Phi_Image->GetDirection());
		DtPhitoT_Image_mask->Allocate();
		DtPhitoT_Image_mask->FillBuffer(1);

		int DtPhitoT_Image_done = ComposeVectorFieldWithJacobianTranspose <VectorFieldImageType,JacobianVectorFieldImageType, ImageType> (	T_Image, DPhi_Image.GetPointer(), DtPhitoT_Image.GetPointer(), T_Image_mask, DPhi_Image_mask.GetPointer(), DtPhitoT_Image_mask.GetPointer() );

		//////////////////////////////////////////////////////////////////////////

		// Computing (D^t(Phi) o T) * ( Phi o T - Id )

		typename VectorFieldImageType::Pointer DtPhioTtimesDiff_Image = VectorFieldImageType::New();
		DtPhioTtimesDiff_Image->SetRegions(Phi_Image->GetBufferedRegion());
		DtPhioTtimesDiff_Image->SetSpacing(Phi_Image->GetSpacing());
		DtPhioTtimesDiff_Image->SetOrigin(Phi_Image->GetOrigin());
		DtPhioTtimesDiff_Image->SetDirection(Phi_Image->GetDirection());
		DtPhioTtimesDiff_Image->Allocate();
		DtPhioTtimesDiff_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
		typename ImageType::Pointer DtPhioTtimesDiff_Image_mask = ImageType::New();
		DtPhioTtimesDiff_Image_mask->SetRegions(Phi_Image->GetBufferedRegion());
		DtPhioTtimesDiff_Image_mask->SetSpacing(Phi_Image->GetSpacing());
		DtPhioTtimesDiff_Image_mask->SetOrigin(Phi_Image->GetOrigin());
		DtPhioTtimesDiff_Image_mask->SetDirection(Phi_Image->GetDirection());
		DtPhioTtimesDiff_Image_mask->Allocate();
		DtPhioTtimesDiff_Image_mask->FillBuffer(1);

		int DtPhioTtimesDiff_Image_done = MultiplyJacobianByVectorField <VectorFieldImageType,JacobianVectorFieldImageType, ImageType> ( PhioT_Image.GetPointer() , DtPhitoT_Image.GetPointer(), DtPhioTtimesDiff_Image.GetPointer(), PhioT_Image_mask.GetPointer(), DtPhitoT_Image_mask.GetPointer(), DtPhioTtimesDiff_Image_mask.GetPointer() );

		//////////////////////////////////////////////////////////////////////////

		// Gradient descent
		diffMagnitude = norm_PhioT / ( Phi_Image->GetBufferedRegion().GetNumberOfPixels() );
		if (it==0)
		{
			diffMagnitude_1st = diffMagnitude;
		}
		diffMagnitude_scaled = diffMagnitude / diffMagnitude_1st;
		std::cout << it << ": " << diffMagnitude_scaled << std::endl;
		std::ostringstream energyValue_Os;
		energyValue_Os << diffMagnitude_scaled;
		energyValue = energyValue_Os.str();

		typename VectorFieldImageType::Pointer GradientDescent_Image = VectorFieldImageType::New();
		GradientDescent_Image->SetRegions(Phi_Image->GetBufferedRegion());
		GradientDescent_Image->SetSpacing(Phi_Image->GetSpacing());
		GradientDescent_Image->SetOrigin(Phi_Image->GetOrigin());
		GradientDescent_Image->SetDirection(Phi_Image->GetDirection());
		GradientDescent_Image->Allocate();
		GradientDescent_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
		typename ImageType::Pointer GradientDescent_Image_mask = ImageType::New();
		GradientDescent_Image_mask->SetRegions(Phi_Image->GetBufferedRegion());
		GradientDescent_Image_mask->SetSpacing(Phi_Image->GetSpacing());
		GradientDescent_Image_mask->SetOrigin(Phi_Image->GetOrigin());
		GradientDescent_Image_mask->SetDirection(Phi_Image->GetDirection());
		GradientDescent_Image_mask->Allocate();
		GradientDescent_Image_mask->FillBuffer(1);

		int GradientDescent_Image_done = ScalarMultiplyVectorField <VectorFieldImageType, ImageType> (	DtPhioTtimesDiff_Image.GetPointer(), GradientDescent_Image.GetPointer(),	rho_gradientDescent, DtPhioTtimesDiff_Image_mask.GetPointer(), GradientDescent_Image_mask.GetPointer() );

		int T_Image_done = LocalOperationsVectorFields<VectorFieldImageType, ImageType>( T_Image, GradientDescent_Image.GetPointer(), T_Image, T_Image_mask, GradientDescent_Image_mask, T_Image_mask, 2 );

		it++;

	}

	return EXIT_SUCCESS;

}

//////////////////////////////////////////////////////////////////////////

template <class ImageType, class VectorFieldImageType> 
typename ImageType::Pointer LinearInterpolateImageFromDispField(VectorFieldImageType* Phi_tF_to_tM, const ImageType* inputImage_tM, 
																float tF, float tM, float t,
																float delta_gradient,
																int Nmax_gradientDescent, float rho_gradientDescent, std::string & energyValue, bool useInv = true)
{
	typedef typename VectorFieldImageType::PixelType			VectorFieldPixelType;

	//////////////////////////////////////////////////////////////////////////
	typename ImageType::Pointer Phi_tF_to_tM_mask = ImageType::New();
	Phi_tF_to_tM_mask->SetRegions(Phi_tF_to_tM->GetBufferedRegion());
	Phi_tF_to_tM_mask->SetSpacing(Phi_tF_to_tM->GetSpacing());
	Phi_tF_to_tM_mask->SetOrigin(Phi_tF_to_tM->GetOrigin());
	Phi_tF_to_tM_mask->SetDirection(Phi_tF_to_tM->GetDirection());
	Phi_tF_to_tM_mask->Allocate();
	Phi_tF_to_tM_mask->FillBuffer(1);

	// Create Transform \phi( t -> tM,.) = (tM - t) / (tM - tF) * \phi(tF -> tM,.);

	// Formula also works for 1st and last points (last points = factor < 0)
	double scaleTransform_factor_t_to_tM = (tM - t) / (tM - tF);
	std::cout << "Scale Factor: " << scaleTransform_factor_t_to_tM << std::endl;

	typename VectorFieldImageType::Pointer Phi_t_to_tM_Image = VectorFieldImageType::New();
	Phi_t_to_tM_Image->SetRegions(Phi_tF_to_tM->GetBufferedRegion());
	Phi_t_to_tM_Image->SetSpacing(Phi_tF_to_tM->GetSpacing());
	Phi_t_to_tM_Image->SetOrigin(Phi_tF_to_tM->GetOrigin());
	Phi_t_to_tM_Image->SetDirection(Phi_tF_to_tM->GetDirection());
	Phi_t_to_tM_Image->Allocate();
	Phi_t_to_tM_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
	typename ImageType::Pointer Phi_t_to_tM_Image_mask = ImageType::New();
	Phi_t_to_tM_Image_mask->SetRegions(Phi_tF_to_tM->GetBufferedRegion());
	Phi_t_to_tM_Image_mask->SetSpacing(Phi_tF_to_tM->GetSpacing());
	Phi_t_to_tM_Image_mask->SetOrigin(Phi_tF_to_tM->GetOrigin());
	Phi_t_to_tM_Image_mask->SetDirection(Phi_tF_to_tM->GetDirection());
	Phi_t_to_tM_Image_mask->Allocate();
	Phi_t_to_tM_Image_mask->FillBuffer(1);

	if (useInv)
	{
		std::string energyValue01;
		int Phi_t_to_tM_Image_done = ComputeInversePhi <VectorFieldImageType, ImageType> (	Phi_tF_to_tM, Phi_t_to_tM_Image.GetPointer(), delta_gradient,
			Nmax_gradientDescent, 
			rho_gradientDescent,
			Phi_tF_to_tM_mask.GetPointer(),
			Phi_t_to_tM_Image_mask.GetPointer(),energyValue01);

		int Phi_t_to_tM_Image_done02 = RemoveMaskedValues <VectorFieldImageType, ImageType> (	Phi_t_to_tM_Image.GetPointer(), Phi_t_to_tM_Image.GetPointer(), Phi_t_to_tM_Image_mask.GetPointer() );

		int Phi_t_to_tM_Image_done03 = ScalarMultiplyVectorField <VectorFieldImageType, ImageType> ( Phi_t_to_tM_Image.GetPointer(), Phi_t_to_tM_Image.GetPointer(),
			scaleTransform_factor_t_to_tM,
			Phi_t_to_tM_Image_mask.GetPointer(),
			Phi_t_to_tM_Image_mask.GetPointer());

		std::string energyValue02;
		int Phi_t_to_tM_Image_done04 = ComputeInversePhi <VectorFieldImageType, ImageType> (	Phi_t_to_tM_Image.GetPointer(), Phi_t_to_tM_Image.GetPointer(),
			delta_gradient,
			Nmax_gradientDescent, 
			rho_gradientDescent,
			Phi_t_to_tM_Image_mask.GetPointer(),
			Phi_t_to_tM_Image_mask.GetPointer(),energyValue02);

		int Phi_t_to_tM_Image_done05 = RemoveMaskedValues <VectorFieldImageType, ImageType> (	Phi_t_to_tM_Image.GetPointer(), Phi_t_to_tM_Image.GetPointer(), Phi_t_to_tM_Image_mask.GetPointer() );


		std::ostringstream energyValue_Os;
		energyValue_Os << energyValue01 << " " << energyValue02;
		energyValue = energyValue_Os.str();
	}
	else
	{
		int Phi_t_to_tM_Image_done = ScalarMultiplyVectorField <VectorFieldImageType, ImageType> ( Phi_tF_to_tM, Phi_t_to_tM_Image.GetPointer(),
			scaleTransform_factor_t_to_tM,
			Phi_t_to_tM_Image_mask.GetPointer(),
			Phi_t_to_tM_Image_mask.GetPointer());
	}

	// Estimate Image^k(t,.) from Image^k(tM,.) using \phi( t -> tM,.) 
	typedef itk::WarpImageFilter< ImageType, ImageType, VectorFieldImageType  >  WarpFilterType;
	typename WarpFilterType::Pointer warpFilter = WarpFilterType::New();
	typedef itk::LinearInterpolateImageFunction< ImageType, double >  InterpolatorType;
	typename InterpolatorType::Pointer interpolator = InterpolatorType::New();
	warpFilter->SetInterpolator( interpolator );
	warpFilter->SetOutputSpacing( Phi_t_to_tM_Image->GetSpacing() );
	warpFilter->SetOutputOrigin(  Phi_t_to_tM_Image->GetOrigin() );
	warpFilter->SetDeformationField( Phi_t_to_tM_Image );
	// 			warpFilter->SetDefaultPixelValue( itk::NumericTraits<PixelType>::ZeroValue() );
	warpFilter->SetInput( inputImage_tM );
	warpFilter->Update();

	return warpFilter->GetOutput();
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType> 
int LinearInterpolateVelocityFromDispField(VectorFieldImageType* V_Image,
										   VectorFieldImageType* Disp_Image_ji,
										   VectorFieldImageType* Disp_Image_jiP,
										   float timeInterval, 
										   float t_k_ji, float t_k_jiP, float t_ref_i, float t_ref_iP, bool useOverLapping = true)
{
	typedef typename VectorFieldImageType::PixelType			VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	unsigned int ImageDimension = V_Image->GetImageDimension();
	typedef itk::Image<PixelType,::itk::GetImageDimension<VectorFieldImageType>::ImageDimension> ImageType;

	bool overLappingPoint = false;
	if (useOverLapping)
	{
		if ( ((t_ref_i-t_k_ji)<(t_k_jiP-t_k_ji)) && ((t_ref_iP-t_k_ji)>(t_k_jiP-t_k_ji)) )
		{
			overLappingPoint = true;
			std::cout << "OverLapping Point" << std::endl;
		}
	}
	double Factor_2 = (t_k_jiP - t_ref_i) / (t_ref_iP - t_ref_i);
	double Factor_3 = (t_ref_i+timeInterval - t_k_jiP) / (t_ref_iP - t_ref_i);
			
	CopyImage <VectorFieldImageType> ( Disp_Image_ji, V_Image );

	double scaleTransform_factor_t_to_ji = - (t_ref_i - t_k_ji) / (t_k_jiP - t_k_ji);

	typename ImageType::Pointer Ones_Image_mask = ImageType::New();
	Ones_Image_mask->SetRegions(V_Image->GetBufferedRegion());
	Ones_Image_mask->SetSpacing(V_Image->GetSpacing());
	Ones_Image_mask->SetOrigin(V_Image->GetOrigin());
	Ones_Image_mask->SetDirection(V_Image->GetDirection());
	Ones_Image_mask->Allocate();
	Ones_Image_mask->FillBuffer(1);

	typename VectorFieldImageType::Pointer Phi_t_to_ji_Image = VectorFieldImageType::New();
	Phi_t_to_ji_Image->SetRegions(V_Image->GetBufferedRegion());
	Phi_t_to_ji_Image->SetSpacing(V_Image->GetSpacing());
	Phi_t_to_ji_Image->SetOrigin(V_Image->GetOrigin());
	Phi_t_to_ji_Image->SetDirection(V_Image->GetDirection());
	Phi_t_to_ji_Image->Allocate();
	Phi_t_to_ji_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

	int Phi_t_to_ji_Image_done = ScalarMultiplyVectorField <VectorFieldImageType, ImageType> ( Disp_Image_ji, Phi_t_to_ji_Image.GetPointer(), scaleTransform_factor_t_to_ji, Ones_Image_mask.GetPointer(), Ones_Image_mask.GetPointer() );

	// Apply transform to velocity v^k to get v^k(t_ref_i,x) = v^k(t_ji,(phi^k(t_j_{i} -> t))^-1(x))

	typename VectorFieldImageType::Pointer V_t_Image = VectorFieldImageType::New();
	V_t_Image->SetRegions(V_Image->GetBufferedRegion());
	V_t_Image->SetSpacing(V_Image->GetSpacing());
	V_t_Image->SetOrigin(V_Image->GetOrigin());
	V_t_Image->SetDirection(V_Image->GetDirection());
	V_t_Image->Allocate();
	V_t_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

	// Identifying overlapping time-points

	typename VectorFieldImageType::Pointer Phi_t_to_jiP_Image = VectorFieldImageType::New();
	double scaleTransform_factor_t_to_jiP;

	if ((overLappingPoint)&&(useOverLapping))
	{
		// Compute Phi_t_to_jiP_Image
		scaleTransform_factor_t_to_jiP = (t_k_jiP - t_ref_i) / (t_k_jiP - t_k_ji);
		Phi_t_to_jiP_Image->SetRegions(V_Image->GetBufferedRegion());
		Phi_t_to_jiP_Image->SetSpacing(V_Image->GetSpacing());
		Phi_t_to_jiP_Image->SetOrigin(V_Image->GetOrigin());
		Phi_t_to_jiP_Image->SetDirection(V_Image->GetDirection());
		Phi_t_to_jiP_Image->Allocate();
		Phi_t_to_jiP_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

		int Phi_t_to_jiP_Image_done = ScalarMultiplyVectorField <VectorFieldImageType, ImageType> ( Disp_Image_ji, Phi_t_to_jiP_Image.GetPointer(), scaleTransform_factor_t_to_jiP, Ones_Image_mask.GetPointer(), Ones_Image_mask.GetPointer() );
	}

	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_V_t(V_t_Image,V_t_Image->GetBufferedRegion());
	VectorFieldIteratorType it_Phi_inv(Phi_t_to_ji_Image,Phi_t_to_ji_Image->GetBufferedRegion());
	VectorFieldIteratorType it_Phi_post(Phi_t_to_jiP_Image,Phi_t_to_jiP_Image->GetBufferedRegion());

	typedef itk::VectorLinearInterpolateImageFunction< VectorFieldImageType , PixelType > VectorInterpolatorType;
	typename VectorInterpolatorType::Pointer interpolator_1 = VectorInterpolatorType::New();
	interpolator_1->SetInputImage( V_Image );
	typename VectorInterpolatorType::Pointer interpolator_post = VectorInterpolatorType::New();
	interpolator_post->SetInputImage( Disp_Image_jiP );

	typename VectorInterpolatorType::OutputType displacement_2,displacement_3;
	VectorFieldPixelType displacement_2V;
	typedef itk::Point < PixelType, ::itk::GetImageDimension<VectorFieldImageType>::ImageDimension > PointType;
	PointType point_1, point_2, point_3;


	for ( it_V_t.GoToBegin(),it_Phi_inv.GoToBegin() ; 
		! ( it_V_t.IsAtEnd() ) || ! ( it_Phi_inv.IsAtEnd() ); 
		++it_V_t,++it_Phi_inv)
	{
		typename VectorFieldImageType::IndexType index = it_V_t.GetIndex();
		VectorFieldPixelType displacement_inv = it_Phi_inv.Get();
		VectorFieldPixelType displacement_post;

		if ((overLappingPoint)&&(useOverLapping))
		{
			displacement_post = it_Phi_post.Get();
		}

		V_t_Image->TransformIndexToPhysicalPoint(index, point_1);
		for (unsigned int dim=0;dim<ImageDimension;dim++)
		{
			point_2[dim] = point_1[dim] + displacement_inv[dim];
			if ((overLappingPoint)&&(useOverLapping))
			{
				point_3[dim] = point_1[dim] + displacement_post[dim];
			}
		}

		bool isValidPoint = true;
		if ( !(interpolator_1->IsInsideBuffer(point_2)) )
		{
			isValidPoint = false;
		}
		if ((overLappingPoint)&&(useOverLapping))
		{
			if ( !(interpolator_post->IsInsideBuffer(point_3)) )
			{
				isValidPoint = false;
			}
		}

		if (isValidPoint)
		{
			if ((overLappingPoint)&&(useOverLapping))
			{
				displacement_2 = interpolator_1->Evaluate(point_2);
				displacement_3 = interpolator_post->Evaluate(point_3);
				for (unsigned int dim=0;dim<ImageDimension;dim++)
				{
					displacement_2V[dim] = Factor_2 * displacement_2[dim] + Factor_3 * displacement_3[dim];
				}
			} 
			else
			{
				displacement_2 = interpolator_1->Evaluate(point_2);
				for (unsigned int dim=0;dim<ImageDimension;dim++)
				{
					displacement_2V[dim] = displacement_2[dim];
				}
			}
		}
		else
		{
			for (unsigned int dim=0;dim<ImageDimension;dim++)
			{
				displacement_2V[dim] = itk::NumericTraits<PixelType>::ZeroValue();
			}
		}

		it_V_t.Set(displacement_2V);
	}

	// Temporal normalization / (t_i - t_{i+1})

	double timeInterval_interval = (double)(1. / timeInterval);
	int v_Image_done = ScalarMultiplyVectorField <VectorFieldImageType, ImageType> ( V_t_Image.GetPointer(), V_t_Image.GetPointer(), timeInterval_interval, Ones_Image_mask.GetPointer(), Ones_Image_mask.GetPointer() );

	CopyImage <VectorFieldImageType> ( V_t_Image.GetPointer(), V_Image );

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType, class ImageType> 
float ComputeDistanceBetweenPhi(	VectorFieldImageType* V_1,
								VectorFieldImageType* V_2,
								ImageType* V_1_mask,
								ImageType* V_2_mask,
								float delta_gradient,
								unsigned int Nmax_gradientDescent,
								double rho_gradientDescent,
								std::string & energyValue_inv,
								unsigned int N_it_expand, double sig_decrease_expand, double V_radius_expand
								)
{
	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	unsigned int ImageDimension = V_1->GetImageDimension();

	//////////////////////////////////////////////////////////////////////////

	// 	typename VectorFieldImageType::Pointer V_1_big = VectorFieldImageType::New();
	// 	typename ImageType::Pointer V_1_big_mask = ImageType::New();
	// 	typedef itk::ImageRegionIterator<ImageType> ImageIteratorType;
	// 	ImageIteratorType it_V_1_big_mask(V_1_big_mask,V_1_big_mask->GetBufferedRegion());
	// 	ImageIteratorType it_V_1_mask(V_1_mask,V_1_mask->GetBufferedRegion());
	// 
	// 	if (N_it_expand > 0)
	// 	{
	// 		std::cout << "Expanding Input Vector Field..." << std::endl;
	// 		// Extend vector field before any computation
	// 		V_1_big = ExtendVF_exp_decrease <VectorFieldImageType> ( V_1, N_it_expand, sig_decrease_expand, V_radius_expand );
	// 
	// 		V_1_big_mask->SetRegions(V_1_big->GetBufferedRegion());
	// 		V_1_big_mask->SetSpacing(V_1_big->GetSpacing());
	// 		V_1_big_mask->SetOrigin(V_1_big->GetOrigin());
	// 		V_1_big_mask->SetDirection(V_1_big->GetDirection());
	// 		V_1_big_mask->Allocate();
	// 		V_1_big_mask->FillBuffer(1);
	// 	}
	// 	else
	// 	{
	// 		V_1_big->SetRegions(V_1->GetBufferedRegion());
	// 		V_1_big->SetSpacing(V_1->GetSpacing());
	// 		V_1_big->SetOrigin(V_1->GetOrigin());
	// 		V_1_big->SetDirection(V_1->GetDirection());
	// 		V_1_big->Allocate();
	// 
	// 		V_1_big_mask->SetRegions(V_1->GetBufferedRegion());
	// 		V_1_big_mask->SetSpacing(V_1->GetSpacing());
	// 		V_1_big_mask->SetOrigin(V_1->GetOrigin());
	// 		V_1_big_mask->SetDirection(V_1->GetDirection());
	// 		V_1_big_mask->Allocate();
	// 
	// 		CopyImage <VectorFieldImageType> ( V_1, V_1_big.GetPointer() );
	// 		CopyImage <ImageType> ( V_1_mask, V_1_big_mask.GetPointer() );
	// 	}
	// 
	// 	//////////////////////////////////////////////////////////////////////////
	// 
	// 	typename VectorFieldImageType::Pointer V_1_inv = VectorFieldImageType::New();
	// 	V_1_inv->SetRegions(V_1_big->GetBufferedRegion());
	// 	V_1_inv->SetSpacing(V_1_big->GetSpacing());
	// 	V_1_inv->SetOrigin(V_1_big->GetOrigin());
	// 	V_1_inv->SetDirection(V_1_big->GetDirection());
	// 	V_1_inv->Allocate();
	// 	typename ImageType::Pointer V_1_inv_mask = ImageType::New();
	// 	V_1_inv_mask->SetRegions(V_1_big->GetBufferedRegion());
	// 	V_1_inv_mask->SetSpacing(V_1_big->GetSpacing());
	// 	V_1_inv_mask->SetOrigin(V_1_big->GetOrigin());
	// 	V_1_inv_mask->SetDirection(V_1_big->GetDirection());
	// 	V_1_inv_mask->Allocate();
	// 	V_1_inv_mask->FillBuffer(1);
	// 
	// 	std::string energyValue;
	// 	int V_1_inv_done = ComputeInversePhi <VectorFieldImageType, ImageType> (	V_1_big.GetPointer(), V_1_inv.GetPointer(),
	// 		delta_gradient,
	// 		Nmax_gradientDescent, 
	// 		rho_gradientDescent,
	// 		V_1_big_mask.GetPointer(),
	// 		V_1_inv_mask.GetPointer(),
	// 		energyValue);
	// 
	// 	std::ostringstream energyValue_Os;
	// 	energyValue_Os << energyValue;
	// 	energyValue_inv = energyValue_Os.str();
	// 
	// 	//////////////////////////////////////////////////////////////////////////
	// 
	// 	typename VectorFieldImageType::Pointer V_1_inv_out = VectorFieldImageType::New();
	// 	if (N_it_expand > 0)
	// 	{
	// 		std::cout << "Cropping output Vector Field..." << std::endl;
	// 		int start_crop[::itk::GetImageDimension<VectorFieldImageType>::ImageDimension];
	// 		int size_cropped_VF[::itk::GetImageDimension<VectorFieldImageType>::ImageDimension];
	// 		for (unsigned int dim=0;dim<ImageDimension;dim++)
	// 		{
	// 			start_crop[dim] = V_1->GetOrigin()[dim];
	// 			size_cropped_VF[dim] = V_1->GetBufferedRegion().GetSize()[dim];
	// 		}
	// 		// Cropping extended vector field before writing output file
	// 		V_1_inv_out = CropVectorFieldImage <VectorFieldImageType> (	V_1_inv.GetPointer(), start_crop, size_cropped_VF );
	// 	}
	// 	else
	// 	{
	// 		V_1_inv_out->SetRegions(V_1->GetBufferedRegion());
	// 		V_1_inv_out->SetSpacing(V_1->GetSpacing());
	// 		V_1_inv_out->SetOrigin(V_1->GetOrigin());
	// 		V_1_inv_out->SetDirection(V_1->GetDirection());
	// 		V_1_inv_out->Allocate();
	// 
	// 		CopyImage <VectorFieldImageType> ( V_1, V_1_inv_out.GetPointer() );
	// 	}
	// 
	// 	//////////////////////////////////////////////////////////////////////////
	// 
	// 	typename VectorFieldImageType::Pointer V2oV1inv_Image = VectorFieldImageType::New();
	// 	V2oV1inv_Image->SetRegions(V_1->GetBufferedRegion());
	// 	V2oV1inv_Image->SetSpacing(V_1->GetSpacing());
	// 	V2oV1inv_Image->SetOrigin(V_1->GetOrigin());
	// 	V2oV1inv_Image->SetDirection(V_1->GetDirection());
	// 	V2oV1inv_Image->Allocate();
	// 	typename ImageType::Pointer V2oV1inv_Image_mask = ImageType::New();
	// 	V2oV1inv_Image_mask->SetRegions(V_1->GetBufferedRegion());
	// 	V2oV1inv_Image_mask->SetSpacing(V_1->GetSpacing());
	// 	V2oV1inv_Image_mask->SetOrigin(V_1->GetOrigin());
	// 	V2oV1inv_Image_mask->SetDirection(V_1->GetDirection());
	// 	V2oV1inv_Image_mask->Allocate();
	// 	V2oV1inv_Image_mask->FillBuffer(1);
	// 
	// 	int V2oV1inv_Image_done = ComposeVectorFields <VectorFieldImageType, ImageType> (	V_1_inv_out.GetPointer(), V_2, V2oV1inv_Image.GetPointer(), V_1_mask, V_2_mask, V2oV1inv_Image_mask.GetPointer() );
	// 

	typename VectorFieldImageType::Pointer V2oV1inv_over_V1_Image = VectorFieldImageType::New();
	V2oV1inv_over_V1_Image->SetRegions(V_1->GetBufferedRegion());
	V2oV1inv_over_V1_Image->SetSpacing(V_1->GetSpacing());
	V2oV1inv_over_V1_Image->SetOrigin(V_1->GetOrigin());
	V2oV1inv_over_V1_Image->SetDirection(V_1->GetDirection());
	V2oV1inv_over_V1_Image->Allocate();
	V2oV1inv_over_V1_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
	typename ImageType::Pointer V2oV1inv_over_V1_Image_mask = ImageType::New();
	V2oV1inv_over_V1_Image_mask->SetRegions(V_1->GetBufferedRegion());
	V2oV1inv_over_V1_Image_mask->SetSpacing(V_1->GetSpacing());
	V2oV1inv_over_V1_Image_mask->SetOrigin(V_1->GetOrigin());
	V2oV1inv_over_V1_Image_mask->SetDirection(V_1->GetDirection());
	V2oV1inv_over_V1_Image_mask->Allocate();
	V2oV1inv_over_V1_Image_mask->FillBuffer(1);

	// 	int V2oV1inv_over_V1_Image_done = LocalOperationsVectorFields <VectorFieldImageType, ImageType> ( V2oV1inv_Image.GetPointer(), V_1, V2oV1inv_over_V1_Image.GetPointer(),
	// 		V2oV1inv_Image_mask.GetPointer(),
	// 		V_1_mask,
	// 		V2oV1inv_over_V1_Image_mask.GetPointer(),
	// 		4 );

	//////////////////////////////////////////////////////////////////////////

	typename VectorFieldImageType::Pointer V2minusV1_Image = VectorFieldImageType::New();
	V2minusV1_Image->SetRegions(V_1->GetBufferedRegion());
	V2minusV1_Image->SetSpacing(V_1->GetSpacing());
	V2minusV1_Image->SetOrigin(V_1->GetOrigin());
	V2minusV1_Image->SetDirection(V_1->GetDirection());
	V2minusV1_Image->Allocate();
	V2minusV1_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
	typename ImageType::Pointer V2minusV1_Image_mask = ImageType::New();
	V2minusV1_Image_mask->SetRegions(V_1->GetBufferedRegion());
	V2minusV1_Image_mask->SetSpacing(V_1->GetSpacing());
	V2minusV1_Image_mask->SetOrigin(V_1->GetOrigin());
	V2minusV1_Image_mask->SetDirection(V_1->GetDirection());
	V2minusV1_Image_mask->Allocate();
	V2minusV1_Image_mask->FillBuffer(1);

	int V2minusV1_done = LocalOperationsVectorFields <VectorFieldImageType, ImageType> ( V_1, V_2, V2minusV1_Image.GetPointer(),
		V_1_mask,
		V_2_mask,
		V2minusV1_Image_mask.GetPointer(),
		2 );

	int V2minusV1overV1_done = LocalOperationsVectorFields <VectorFieldImageType, ImageType> ( V2minusV1_Image.GetPointer(), V_1, V2oV1inv_over_V1_Image.GetPointer(),
		V2minusV1_Image_mask.GetPointer(),
		V_1_mask,
		V2oV1inv_over_V1_Image_mask.GetPointer(),
		4 );

	float distancePhi = ComputeNormVectorField <VectorFieldImageType, ImageType> (V2oV1inv_over_V1_Image.GetPointer(), V2oV1inv_over_V1_Image_mask.GetPointer());

	return distancePhi;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType> 
int ComputeExpPhi(	VectorFieldImageType* V, VectorFieldImageType* out_V,
				  int N_it_expand, float sig_decrease_expand, int V_radius_expand, int N_composition )
{
	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	unsigned int ImageDimension = V->GetImageDimension();

	typedef itk::Image<PixelType, ::itk::GetImageDimension<VectorFieldImageType>::ImageDimension> ImageType;


	typename VectorFieldImageType::Pointer expanded_V = VectorFieldImageType::New();

	if (N_it_expand > 0)
	{
		std::cout << "Expanding Input Vector Field..." << std::endl;
		// Extend vector field before any computation
		expanded_V = ExtendVF_exp_decrease <VectorFieldImageType> ( V, N_it_expand, sig_decrease_expand, V_radius_expand );
	}
	else
	{
		expanded_V->SetRegions(V->GetBufferedRegion());
		expanded_V->SetSpacing(V->GetSpacing());
		expanded_V->SetOrigin(V->GetOrigin());
		expanded_V->SetDirection(V->GetDirection());
		expanded_V->Allocate();
		expanded_V->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

		CopyImage <VectorFieldImageType> ( V, expanded_V.GetPointer() );
	}

	typename ImageType::Pointer expanded_V_mask = ImageType::New();
	expanded_V_mask->SetRegions(expanded_V->GetBufferedRegion());
	expanded_V_mask->SetSpacing(expanded_V->GetSpacing());
	expanded_V_mask->SetOrigin(expanded_V->GetOrigin());
	expanded_V_mask->SetDirection(expanded_V->GetDirection());
	expanded_V_mask->Allocate();
	expanded_V_mask->FillBuffer(1);

	typename VectorFieldImageType::Pointer exp_expanded_V = VectorFieldImageType::New();
	exp_expanded_V->SetRegions(expanded_V->GetBufferedRegion());
	exp_expanded_V->SetSpacing(expanded_V->GetSpacing());
	exp_expanded_V->SetOrigin(expanded_V->GetOrigin());
	exp_expanded_V->SetDirection(expanded_V->GetDirection());
	exp_expanded_V->Allocate();
	exp_expanded_V->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
	typename ImageType::Pointer exp_expanded_V_mask = ImageType::New();
	exp_expanded_V_mask->SetRegions(expanded_V->GetBufferedRegion());
	exp_expanded_V_mask->SetSpacing(expanded_V->GetSpacing());
	exp_expanded_V_mask->SetOrigin(expanded_V->GetOrigin());
	exp_expanded_V_mask->SetDirection(expanded_V->GetDirection());
	exp_expanded_V_mask->Allocate();
	exp_expanded_V_mask->FillBuffer(1);

	// N recursive compositions = 2^N iterative compositions
	for (unsigned it=0;it<N_composition;it++)
	{
		std::cout << std::endl << "Computing Recomposed Vector Field... " << it << std::endl;

		if (it==0)
		{
			int exp_expanded_V_done = ComposeVectorFields <VectorFieldImageType, ImageType> (	expanded_V.GetPointer(), expanded_V.GetPointer(), exp_expanded_V.GetPointer(), expanded_V_mask.GetPointer(), expanded_V_mask.GetPointer(), exp_expanded_V_mask.GetPointer() );
		} 
		else
		{
			typename VectorFieldImageType::Pointer exp_expanded_V_tmp = VectorFieldImageType::New();
			exp_expanded_V_tmp->SetRegions(expanded_V->GetBufferedRegion());
			exp_expanded_V_tmp->SetSpacing(expanded_V->GetSpacing());
			exp_expanded_V_tmp->SetOrigin(expanded_V->GetOrigin());
			exp_expanded_V_tmp->SetDirection(expanded_V->GetDirection());
			exp_expanded_V_tmp->Allocate();
			exp_expanded_V_tmp->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
			typename ImageType::Pointer exp_expanded_V_tmp_mask = ImageType::New();
			exp_expanded_V_tmp_mask->SetRegions(exp_expanded_V_tmp->GetBufferedRegion());
			exp_expanded_V_tmp_mask->SetSpacing(exp_expanded_V_tmp->GetSpacing());
			exp_expanded_V_tmp_mask->SetOrigin(exp_expanded_V_tmp->GetOrigin());
			exp_expanded_V_tmp_mask->SetDirection(exp_expanded_V_tmp->GetDirection());
			exp_expanded_V_tmp_mask->Allocate();
			exp_expanded_V_tmp_mask->FillBuffer(1);

			int exp_expanded_V_tmp_done = ComposeVectorFields <VectorFieldImageType> (	exp_expanded_V.GetPointer(), exp_expanded_V.GetPointer(), exp_expanded_V_tmp.GetPointer(), exp_expanded_V_mask.GetPointer(), exp_expanded_V_mask.GetPointer(), exp_expanded_V_tmp_mask.GetPointer() );

			CopyImage <VectorFieldImageType> ( exp_expanded_V_tmp.GetPointer(), exp_expanded_V.GetPointer() );
		}
	}

	std::cout << std::endl << "Removing NonpositiveMin values..." << std::endl;
	int exp_expanded_V_done = RemoveMaskedValues <VectorFieldImageType, ImageType> (	exp_expanded_V.GetPointer(), exp_expanded_V.GetPointer(), exp_expanded_V_mask.GetPointer() );

	//////////////////////////////////////////////////////////////////////////
	typename VectorFieldImageType::Pointer exp_V = VectorFieldImageType::New();
	exp_V->SetRegions(V->GetBufferedRegion());
	exp_V->SetSpacing(V->GetSpacing());
	exp_V->SetOrigin(V->GetOrigin());
	exp_V->SetDirection(V->GetDirection());
	exp_V->Allocate();
	exp_V->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

	if (N_it_expand > 0)
	{
		std::cout << "Cropping output Recomposed Vector Field..." << std::endl;

		int start_crop[::itk::GetImageDimension<VectorFieldImageType>::ImageDimension];
		int size_cropped_VF[::itk::GetImageDimension<VectorFieldImageType>::ImageDimension];
		for (unsigned int dim=0;dim<ImageDimension;dim++)
		{
			start_crop[dim] = N_it_expand;
			size_cropped_VF[dim] = V->GetBufferedRegion().GetSize()[dim];
		}
		// Cropping extended vector field before writing output file
		exp_V = CropVectorFieldImage(	exp_expanded_V.GetPointer(), start_crop, size_cropped_VF );
	}
	else
	{
		CopyImage <VectorFieldImageType> ( exp_expanded_V.GetPointer(), exp_V.GetPointer() );
	}



	// Copy output data
	CopyImage <VectorFieldImageType> ( exp_V.GetPointer(), out_V );

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////

template <class VectorFieldImageType> 
int ComputeLogPhi(VectorFieldImageType* inputVectorFieldImage_tmp,
				  VectorFieldImageType* output_log_VectorFieldImage,
				  int N_it_expand, 	float sig_decrease_expand, int V_radius_expand,
				  int N_composition, float delta_sqrt, int Nmax_it_sqrt, float rho_sqrt,
				  float delta_inv, int Nmax_it_inv, float rho_inv,
				  bool outputLogAccFileNameSpecified,
				  std::string & energyValue_list			  )
{
	typedef typename VectorFieldImageType::PixelType  VectorFieldPixelType;
	typedef typename VectorFieldPixelType::ValueType PixelType;
	unsigned int ImageDimension = inputVectorFieldImage_tmp->GetImageDimension();

	typedef itk::Image<PixelType, ::itk::GetImageDimension<VectorFieldImageType>::ImageDimension> ImageType;
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;

	typename VectorFieldImageType::Pointer log_Image = VectorFieldImageType::New();

	std::ostringstream energyValue_list_OsName;


	if (N_it_expand > 0)
	{
		std::cout << "Expanding Input Vector Field...   sig = " << sig_decrease_expand << std::endl;
		// Extend vector field before any computation
		log_Image = ExtendVF_exp_decrease <VectorFieldImageType> ( inputVectorFieldImage_tmp, N_it_expand, sig_decrease_expand, V_radius_expand );

	}
	else
	{
		log_Image->SetRegions(inputVectorFieldImage_tmp->GetBufferedRegion());
		log_Image->SetSpacing(inputVectorFieldImage_tmp->GetSpacing());
		log_Image->SetOrigin(inputVectorFieldImage_tmp->GetOrigin());
		log_Image->SetDirection(inputVectorFieldImage_tmp->GetDirection());
		log_Image->Allocate();
		log_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

		CopyImage <VectorFieldImageType> ( inputVectorFieldImage_tmp, log_Image.GetPointer() );
	}

// 	typedef itk::ImageFileWriter<VectorFieldImageType> VFWriterType;
// 	VFWriterType::Pointer writer_VF = VFWriterType::New();
// 	writer_VF->SetInput(log_Image.GetPointer());
// 	writer_VF->SetFileName("E:\\bk-up\\tests_Corne\\atlasStuff\\testVF_01L.mhd");
// 	try{writer_VF->Update();}
// 	catch ( itk::ExceptionObject & err )	{
// 		std::cerr << "ExceptionObject caught !" << std::endl;
// 		std::cerr << err << std::endl;}


	//////////////////////////////////////////////////////////////////////////

	// Computing log = N iterative sqrt

	typename ImageType::Pointer log_Image_mask = ImageType::New();
	log_Image_mask->SetRegions(log_Image->GetBufferedRegion());
	log_Image_mask->SetSpacing(log_Image->GetSpacing());
	log_Image_mask->SetOrigin(log_Image->GetOrigin());
	log_Image_mask->SetDirection(log_Image->GetDirection());
	log_Image_mask->Allocate();
	log_Image_mask->FillBuffer(1.);

	typename VectorFieldImageType::Pointer T_Image = VectorFieldImageType::New();
	T_Image->SetRegions(log_Image->GetBufferedRegion());
	T_Image->SetSpacing(log_Image->GetSpacing());
	T_Image->SetOrigin(log_Image->GetOrigin());
	T_Image->SetDirection(log_Image->GetDirection());
	T_Image->Allocate();
	T_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
	typename ImageType::Pointer T_Image_mask = ImageType::New();
	T_Image_mask->SetRegions(log_Image->GetBufferedRegion());
	T_Image_mask->SetSpacing(log_Image->GetSpacing());
	T_Image_mask->SetOrigin(log_Image->GetOrigin());
	T_Image_mask->SetDirection(log_Image->GetDirection());
	T_Image_mask->Allocate();
	T_Image_mask->FillBuffer(1);

	for (unsigned it=0;it<N_composition;it++)
	{
		std::cout << "Computing sqrt...   it=" << it << std::endl;
		std::string energyValue;

		CopyImage <VectorFieldImageType> ( log_Image.GetPointer(), T_Image.GetPointer() );
		CopyImage <ImageType> ( log_Image_mask.GetPointer(), T_Image_mask.GetPointer() );

		int log_Image_done = ComputeSqrtPhi <VectorFieldImageType, ImageType> (	log_Image.GetPointer(), T_Image.GetPointer(),
			delta_inv, Nmax_it_inv, rho_inv,
			delta_sqrt, Nmax_it_sqrt, rho_sqrt,
			log_Image_mask.GetPointer(), T_Image_mask.GetPointer(), energyValue);

		if (outputLogAccFileNameSpecified)
		{
			energyValue_list_OsName << energyValue << " ";
		}

		CopyImage <VectorFieldImageType> ( T_Image.GetPointer(), log_Image.GetPointer() );
		CopyImage <ImageType> ( T_Image_mask.GetPointer(), log_Image_mask.GetPointer() );
	}


	std::cout << std::endl << "Removing NonpositiveMin values..." << std::endl;
	typename VectorFieldImageType::Pointer Cleaned_log_Image = VectorFieldImageType::New();
	Cleaned_log_Image->SetRegions(log_Image->GetBufferedRegion());
	Cleaned_log_Image->SetSpacing(log_Image->GetSpacing());
	Cleaned_log_Image->SetOrigin(log_Image->GetOrigin());
	Cleaned_log_Image->SetDirection(log_Image->GetDirection());
	Cleaned_log_Image->Allocate();
	Cleaned_log_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
	int Cleaned_log_Image_done = RemoveMaskedValues <VectorFieldImageType, ImageType> (	log_Image.GetPointer(), Cleaned_log_Image.GetPointer(), log_Image_mask.GetPointer() );

	// 	log_Image->Delete();
	// 	log_Image_mask->Delete();


	//////////////////////////////////////////////////////////////////////////
	typename VectorFieldImageType::Pointer Cleaned_log_Image_toWrite = VectorFieldImageType::New();
	if (N_it_expand > 0)
	{
		std::cout << "Cropping output Vector Field..." << std::endl;
		int start_crop[::itk::GetImageDimension<VectorFieldImageType>::ImageDimension];
		int size_cropped_VF[::itk::GetImageDimension<VectorFieldImageType>::ImageDimension];
		for (unsigned int dim=0;dim<ImageDimension;dim++)
		{
			start_crop[dim] = N_it_expand;
			size_cropped_VF[dim] = inputVectorFieldImage_tmp->GetBufferedRegion().GetSize()[dim];
		}
		// Cropping extended vector field before writing output file
		Cleaned_log_Image_toWrite = CropVectorFieldImage <VectorFieldImageType> (	Cleaned_log_Image.GetPointer(), start_crop, size_cropped_VF );
	}
	else
	{
		Cleaned_log_Image_toWrite->SetRegions(Cleaned_log_Image->GetBufferedRegion());
		Cleaned_log_Image_toWrite->SetSpacing(Cleaned_log_Image->GetSpacing());
		Cleaned_log_Image_toWrite->SetOrigin(Cleaned_log_Image->GetOrigin());
		Cleaned_log_Image_toWrite->SetDirection(Cleaned_log_Image->GetDirection());
		Cleaned_log_Image_toWrite->Allocate();
		Cleaned_log_Image_toWrite->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

		CopyImage <VectorFieldImageType> ( Cleaned_log_Image.GetPointer(), Cleaned_log_Image_toWrite.GetPointer() );
	}



	// 	Cleaned_log_Image->Delete();

	//////////////////////////////////////////////////////////////////////////

	// 	writer_VF->SetInput(Cleaned_log_Image_toWrite.GetPointer());
	// 	writer_VF->SetFileName("E:\\bk-up\\tests_Corne\\atlasStuff\\testVF_DS_log.mhd");
	// 	try{writer_VF->Update();}
	// 	catch ( itk::ExceptionObject & err )	{
	// 		std::cerr << "ExceptionObject caught !" << std::endl;
	// 		std::cerr << err << std::endl;}


	// Copy output data
	CopyImage <VectorFieldImageType> ( Cleaned_log_Image_toWrite.GetPointer(), output_log_VectorFieldImage );

	return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////













#endif /* !REGDATAOPERATIONS_H_ */
