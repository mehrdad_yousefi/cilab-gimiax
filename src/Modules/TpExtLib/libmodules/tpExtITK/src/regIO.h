#ifndef REGIO_H_
#define REGIO_H_

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkTransformFileReader.h"
#include "itkTransformFactory.h"

#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"

//////////////////////////////////////////////////////////////////////////

// Available functions:
// ---------------------
// ReadTransformFromFile
// WriteVectorFieldFromTransform
// getFileNameExtension
// ScalarMultiplyImage
// RemoveMaskedValuesOnImage

//////////////////////////////////////////////////////////////////////////


template <class TransformType> 
typename TransformType::Pointer 
ReadTransformFromFile(	const char* transformFileName, const char* expectedTransformType, bool showInfo = true )
{
	itk::TransformFileReader::Pointer reader;
	reader = itk::TransformFileReader::New();
	itk::TransformFactory<TransformType>::RegisterTransform();
	reader->SetFileName( transformFileName );
	try
	{
		reader->Update();
	}
	catch( itk::ExceptionObject & excp )
	{
		std::cerr << "ExceptionObject caught !" << std::endl;
		std::cerr << excp << std::endl;
		std::cerr << "[FAILED]" << std::endl;
	}

	typedef itk::TransformFileReader::TransformListType * TransformListType;
	TransformListType transforms = reader->GetTransformList();
	if (showInfo)
	{
		std::cout << "Number of transforms = " << transforms->size() << std::endl;
	}

	itk::TransformFileReader::TransformListType::const_iterator it = transforms->begin();
	typename TransformType::Pointer transform_temp = 0;
	typename TransformType::Pointer  transform = TransformType::New();

	if(!strcmp((*it)->GetNameOfClass(),expectedTransformType))
	{
		transform_temp = static_cast<TransformType*>((*it).GetPointer());
		if (showInfo)
		{
			transform_temp->Print(std::cout);
		}
		transform = transform_temp;
	}
	return transform;
}

//////////////////////////////////////////////////////////////////////////

template <class TransformType, class ImageType> 
int WriteVectorFieldFromTransform(const TransformType* transform, const ImageType* fixedImage, const char* outputDefoFieldFileName, float ext_size[])
{
	typedef typename ImageType::PixelType PixelType;
	unsigned int ImageDimension = fixedImage->GetImageDimension();

	typedef itk::Vector<PixelType,
		::itk::GetImageDimension<ImageType>::ImageDimension> VectorFieldPixelType;
	typedef itk::Image<VectorFieldPixelType,
		::itk::GetImageDimension<ImageType>::ImageDimension> VectorFieldImageType;

	typename VectorFieldImageType::Pointer defoFieldImage = VectorFieldImageType::New();
	if (ext_size==0)
	{
		defoFieldImage->SetRegions(fixedImage->GetBufferedRegion());
		defoFieldImage->SetOrigin(fixedImage->GetOrigin());
	} 
	else
	{
		//////////////////////////////////////////////////////////////////////////
		typedef typename VectorFieldImageType::RegionType VectorFieldRegionType;
		typedef typename VectorFieldImageType::PointType VectorFieldOriginType;
		VectorFieldRegionType VF_region;
		VectorFieldOriginType VF_origin = fixedImage->GetOrigin();
		typename VectorFieldRegionType::SizeType VF_region_size = fixedImage->GetBufferedRegion().GetSize();
		for (unsigned int dim=0;dim<ImageDimension;dim++)
		{
			VF_region_size[dim] += (int)(2*ext_size[dim]/fixedImage->GetSpacing()[dim]);
			VF_origin[dim] -= ext_size[dim];
		}
		VF_region.SetSize(VF_region_size);
		//////////////////////////////////////////////////////////////////////////
		defoFieldImage->SetRegions(VF_region);
		defoFieldImage->SetOrigin(VF_origin);
	}
	defoFieldImage->SetSpacing(fixedImage->GetSpacing());
	defoFieldImage->SetDirection(fixedImage->GetDirection());
	defoFieldImage->Allocate();
	defoFieldImage->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

	// iterator on output def field
	typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
	VectorFieldIteratorType it_defoField(defoFieldImage,defoFieldImage->GetBufferedRegion());

	// filling in the deformation field

	for ( it_defoField.GoToBegin(); 
		! ( it_defoField.IsAtEnd() ); 
		++it_defoField)
	{
		typename TransformType::OutputPointType outputPoint;
		typename TransformType::InputPointType currentPoint;
		VectorFieldPixelType displacement;

		typename ImageType::IndexType index = it_defoField.GetIndex();
		defoFieldImage->TransformIndexToPhysicalPoint(index, currentPoint);

		outputPoint = transform->TransformPoint(currentPoint);

		for (unsigned int dim=0; dim<ImageDimension; dim++)
		{
			displacement[dim] = outputPoint[dim] - currentPoint[dim];     
		}
		it_defoField.Set(displacement);
	}



// 	std::cout << std::endl << "Writing Deformation Field..." << std::endl;
	typedef itk::ImageFileWriter<VectorFieldImageType> VectorFieldImageWriterType;
	typename VectorFieldImageWriterType::Pointer writer_defoField = VectorFieldImageWriterType::New();

	writer_defoField->SetInput(defoFieldImage);
	writer_defoField->SetFileName(outputDefoFieldFileName);

	try
	{
		writer_defoField->Update();
	}
	catch ( itk::ExceptionObject & err )
	{
		std::cerr << "ExceptionObject caught !" << std::endl;
		std::cerr << err << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;

}

//////////////////////////////////////////////////////////////////////////

std::string getFileNameExtension(int i);

//////////////////////////////////////////////////////////////////////////



#endif /* !REGIO_H_ */
