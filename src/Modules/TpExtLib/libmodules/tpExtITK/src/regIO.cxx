#include "regIO.h"

std::string getFileNameExtension(int i)
{

	std::ostringstream extension_i_OsName;
	if ( (i >= 0) && (i < 10) )
	{
		extension_i_OsName  << "_000" << i;
	}
	else if ((i >= 10) && (i < 100))
	{
		extension_i_OsName  << "_00" << i;
	}
	else if ((i >= 100) && (i < 1000))
	{
		extension_i_OsName  << "_0" << i;
	}
	else if ((i >= 1000) && (i < 10000))
	{
		extension_i_OsName  << "_" << i;
	}
	else
	{
		std::cerr << "Too many frames inside Dicom series (> 10000 frames) " << std::endl;
	}
	std::string extension_i_StringName = extension_i_OsName.str();

	return extension_i_StringName;

}
