/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
       #include <wx/wx.h>
#endif

#include <wx/wupdlock.h>

#include "wxWidgetStackControl.h"


//!
wxWidgetStackControl::wxWidgetStackControl(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
: wxPanel(parent, id, pos, size, style, name), m_currentWidget(NULL), m_currentIndex(-1)
{
	// Layout
	SetAutoLayout(true);
	SetSizer( new wxBoxSizer(wxHORIZONTAL) );
}

//!
wxWidgetStackControl::~wxWidgetStackControl(void)
{
}

int wxWidgetStackControl::Add(wxWindow* widget)
{
	if(widget == NULL || GetIndexOfWidget( widget ) != -1 )
		return -1;

	widget->Hide();
	int index = int( m_registeredWindowList.size() );
	wxWidgetStackControl::WindowMapElement value(index, widget);
	wxWidgetStackControl::WindowMapInsertResult ret = m_registeredWindowList.insert(value);

	// If insertion could not take place, return -1 signaling an error
	if(!ret.second)
		return -1;

	widget->Hide();
	widget->Reparent(this);

	return index;

}

void wxWidgetStackControl::Remove(wxWindow* widget)
{
	bool found = false;
	wxWidgetStackControl::WindowMap::iterator it;
	for( it = m_registeredWindowList.begin();
		it != m_registeredWindowList.end(); ++it)
	{
		if((*it).second == widget)
		{
			found = true;
			break;
		}
	}

	if ( found )
	{
		// if it is the widget at top, redo the layout
		if(widget == m_currentWidget || (*it).first == m_currentIndex)
			ClearCurrentWidget();

		// Erase the widget from the list
		m_registeredWindowList.erase(it);
	}

}

void wxWidgetStackControl::Remove(int widgetIndex)
{
	wxWidgetStackControl::WindowMap::iterator it = m_registeredWindowList.find(widgetIndex);
	if(it != m_registeredWindowList.end())
	{
		// if it is the widget at top, redo the layout
		if((*it).first == m_currentIndex || (*it).second == m_currentWidget)
			ClearCurrentWidget();
		
		// Erase the widget from the list
		m_registeredWindowList.erase(it);
	}
}

void wxWidgetStackControl::Raise(wxWindow* widget)
{
	int widgetIndex, previousIndex;
	wxWindow* previousWidget;

	if(widget == NULL || (widgetIndex = GetIndexOfWidget(widget)) < 0)
		return;

	if ( widget == GetWidgetOnTop( ) )
	{
		return;
	}

	wxWindowUpdateLocker noUpdates( this );

	if(m_currentWidget != NULL)
		m_currentWidget->Hide();

	previousIndex = m_currentIndex;
	previousWidget = m_currentWidget;
	m_currentIndex = widgetIndex;
	m_currentWidget = widget;
	GetSizer()->Clear(false);
	GetSizer()->Add(widget, 1, wxEXPAND);

	// Don't call wiget->Show( ) because the scroll bar is not updated
	GetSizer()->Show( widget, true, true );

	wxSizeEvent resEvent(this->GetBestSize(), this->GetId());
	resEvent.SetEventObject(this);
	this->GetEventHandler()->ProcessEvent(resEvent);

	UpdateObserversOfWidgetStack(previousIndex, previousWidget);
}

void wxWidgetStackControl::Raise(int widgetIndex)
{
	wxWindow* widget, *previousWidget;
	int previousIndex;

	if(widgetIndex < 0 || (widget = GetWidgetByIndex(widgetIndex)) == NULL)
		return;

	if(m_currentWidget != NULL)
		m_currentWidget->Hide();

	previousIndex = m_currentIndex;
	previousWidget = m_currentWidget;
	m_currentIndex = widgetIndex;
	m_currentWidget = widget;
	GetSizer()->Clear(false);
	GetSizer()->Add(widget, 1, wxEXPAND);
	widget->Show();
	// Cast a resize event
	wxSizeEvent event(GetBestSize(), GetId());
    event.SetEventObject(this);
	GetEventHandler()->ProcessEvent(event);

	UpdateObserversOfWidgetStack(previousIndex, previousWidget);
}

void wxWidgetStackControl::Clear(void)
{
	ClearCurrentWidget();
	m_registeredWindowList.clear();
}

wxWindow* wxWidgetStackControl::GetWidgetOnTop(void) const 
{
	return m_currentWidget;
}

unsigned int wxWidgetStackControl::GetWidgetIndexOnTop(void) const
{
	return m_currentIndex;
}


//!
int wxWidgetStackControl::GetIndexOfWidget(wxWindow* widget) const 
{
	for(wxWidgetStackControl::WindowMap::const_iterator it = m_registeredWindowList.begin();
		it != m_registeredWindowList.end(); ++it)
	{
		if((*it).second == widget)
			return (*it).first;
	}
	return -1;
}


//!
wxWindow* wxWidgetStackControl::GetWidgetByIndex(int widgetIndex) const 
{
	wxWidgetStackControl::WindowMap::const_iterator it = m_registeredWindowList.find(widgetIndex);
	if(it == m_registeredWindowList.end())
		return NULL;
	return (*it).second;
}



//!
void wxWidgetStackControl::ClearCurrentWidget(void)
{
	if(m_currentWidget != NULL)
		m_currentWidget->Hide();
	m_currentIndex = -1;
	m_currentWidget = NULL;
	GetSizer( )->Clear(false);
}

void wxWidgetStackControl::UpdateObserversOfWidgetStack(int previousIndex, wxWindow* previousWidget)
{
	// Create a Color Function changed event and configure it
	wxWidgetRisenEvent event(GetId());
    event.SetEventObject(this);
	event.m_currentWidget = m_currentWidget;
	event.m_currentIndex = m_currentIndex;
	event.m_previousWidget = previousWidget;
	event.m_previousIndex = previousIndex;

	// Cast the event
	GetEventHandler()->ProcessEvent(event);
}

int wxWidgetStackControl::GetNumberOfWidgets()
{
	return int( m_registeredWindowList.size() );
}

wxWidgetStackControl::WindowMap wxWidgetStackControl::GetAllWidgets()
{
	return m_registeredWindowList;
}



//////////////////////////////////////////////////////////////////////////////////
//
// Code for the wxWidgetRisenEvent
//
//////////////////////////////////////////////////////////////////////////////////

DEFINE_EVENT_TYPE(wxEVT_WIDGET_RISEN)
IMPLEMENT_DYNAMIC_CLASS(wxWidgetRisenEvent, wxCommandEvent)

//!
wxWidgetRisenEvent::wxWidgetRisenEvent(int winid, wxEventType commandType)
: wxCommandEvent(commandType, winid)
{
}

//!
wxWidgetRisenEvent::~wxWidgetRisenEvent(void)
{
}

/**
This function is used to create a copy of the event polymorphically and
all derived classes must implement it because otherwise wxPostEvent()
for them wouldn't work (it needs to do a copy of the event)
*/
wxEvent* wxWidgetRisenEvent::Clone(void)
{
	return new wxWidgetRisenEvent(*this);
}

//!
wxWindow* wxWidgetRisenEvent::GetWidgetOnTop(void)
{
	return m_currentWidget;
}

//!
int wxWidgetRisenEvent::GetWidgetIndexOnTop(void)
{
	return m_currentIndex;
}

//!
wxWindow* wxWidgetRisenEvent::GetPreviousWidgetOnTop(void)
{
	return m_previousWidget;
}

//!
int wxWidgetRisenEvent::GetPreviousWidgetIndexOnTop(void)
{
	return m_previousIndex;
}
