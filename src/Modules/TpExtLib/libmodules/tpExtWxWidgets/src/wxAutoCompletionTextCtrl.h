/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _wxAutoCompletionTextCtrl_H
#define _wxAutoCompletionTextCtrl_H

#include "TpExtLibWxWidgetsWin32Header.h"
#include <wx/combo.h>
#include <wx/listctrl.h>

#include <vector>

class wxSearchListBoxEventHandler;

/** 
Special text control that shows a list of similar entries when the user
types text.

To add the entries you need to call Append( ).

When the users starts typing the text, it will show the pop up with the similar
entries. The user can navigate through the entries and select one pressing
return key or the mouse click.

The event wxEVT_TEXT will be sent with the string value when the user 
selects an entry of the list box or the text control is empty.

To set the value of the text control without sending the event, you can
use ChangeValue( ) function.

\ingroup TpExtLibWxWidgets
\author Xavi Planes
\date Apr 2011
*/
class TPEXTLIBWXWIDGETS_EXPORT wxAutoCompletionTextCtrl : public wxTextCtrl
{
public:

	//!
	wxAutoCompletionTextCtrl(
		wxWindow* parent, 
		wxWindowID id, 
		const wxString& value = wxEmptyString,
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = 0,
		const wxValidator& validator = wxDefaultValidator,
		const wxString& name = wxTextCtrlNameStr);

	//!
	virtual ~wxAutoCompletionTextCtrl(void);

	//! 
	void ShowPopup();

	//!
	void HidePopup();

	//! Get appended items to the internal list
	int GetStringCount( );

	//! Append items to the internal list
	void AppendString( const wxString& item );

	//! Clear al internal list of strings
	void ClearStrings( );

	//! Filter content that matches filter using lower case
	void FilterContent( const std::string &filter );

	//!
	void SetPopupMaxHeight( int height );

	// return true if the popup is currently shown
	bool IsPopupShown() const;

	//!
	void SelectListBoxItem( int item );

	//!
	wxListBox* GetListBox() const;

protected:

	//! If the text is not empty, show pop up. If it's empty, hide pop up
	void OnText( wxCommandEvent& event );

	//!
	void OnKeyDown(wxKeyEvent &event);

	//!
	void OnFocus(wxFocusEvent& event);

	//!
	void OnKillFocus( wxFocusEvent& event );

	//! 
	void OnMove(wxMoveEvent& event);

	//!
	void CreatePopup( );

	//!
	void DestroyPopup( );

    wxDECLARE_EVENT_TABLE();
protected:
	// wxPopupWindow or similar containing the window managed by the interface.
	wxWindow* m_winPopup;
	//! List box
	wxListBox* m_ListBox;
	//! List box event handler
	wxSearchListBoxEventHandler* m_ListBoxEventHandler;
	//! List of choices
	std::vector<std::string> m_StringVector;
	//!
	int m_heightPopup;
	// platform-dependant customization and other flags
	wxUint32                m_iFlags;
	//! When the list box item is selected, we should avoid showing popup again
	bool m_SelectingListBoxItem;

	/** When pop up is shown, the focus is set to the text control
	and we don't need to select all text
	*/
	bool m_ShowingPopup;
	//!
	wxString m_SelectedValue;
};


#endif // _corewxAutoCompletionTextCtrl_H
