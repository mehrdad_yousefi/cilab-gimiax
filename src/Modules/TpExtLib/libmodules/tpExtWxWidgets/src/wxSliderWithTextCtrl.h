/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef wxSliderWithTextCtrl_H
#define wxSliderWithTextCtrl_H

#include "TpExtLibWxWidgetsWin32Header.h"
#include "wxFloatSlider.h"

/**
wxSlider with wxSpinCtrl 

\author Xavi Planes
\date 20 Jan 2011
\ingroup DynLib
*/
class TPEXTLIBWXWIDGETS_EXPORT wxSliderWithTextCtrl : public wxPanel
{
public:
	//!
	wxSliderWithTextCtrl(wxWindow *parent, 
		const wxWindowID id, float value , float minValue, 
		float maxValue, float step, const wxPoint& point = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, long style = wxSL_HORIZONTAL, 
		const wxValidator& validator = wxDefaultValidator, 
		const wxString& name = "slider");

	//!
	float GetValue();

	//!
	std::string GetValueAsString();

	//!
	void SetValueAsString( const std::string &val );

	//!
	void SetValue( float value );

	//!
	wxTextCtrl* GetTextCtrl( );

	//!
	wxFloatSlider* GetSlider( );

private:
    wxDECLARE_EVENT_TABLE();

	//!
	void OnTextCtrl(wxCommandEvent& event);

	//!
	void OnSlider(wxScrollEvent& event);

private:
	//!
	wxFloatSlider* m_Slider;
	//!
	wxTextCtrl* m_TextCtrl;
	//!
	bool m_ModifingSlider;
};

#endif // wxSliderWithTextCtrl_H
