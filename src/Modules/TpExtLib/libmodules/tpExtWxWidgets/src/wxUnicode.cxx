/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "wxUnicode.h"

std::string _U(const wxString& s)
{
	return std::string( s.mb_str( wxConvUTF8 ) ); 
}

wxString _U(const std::string& s)
{
	return wxString::FromUTF8( s.c_str() ); 
}

wxString _U(const char* c)
{
	return wxString::FromUTF8( c );
}

