/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef wxCatpureWindow_H
#define wxCatpureWindow_H

#include "TpExtLibWxWidgetsWin32Header.h"
#include <string>
#include <vector>

class wxWindow;

/**
Capture a screen region defined by a window and save it to a filename

\ingroup TpExtLibWxWidgets
\author Xavi Planes
\date 16 April 2009
*/
void TPEXTLIBWXWIDGETS_EXPORT wxCaptureWindow( const wxWindow *inputWindow, const std::string filename );
/**
Capture a window using a specified width and height. 
Always create a snapshot of (width,height) and fill background with black color.
\param [in] margins specify surrounding margin in pixels to remove from capture
*/
void TPEXTLIBWXWIDGETS_EXPORT wxCaptureWindowThumbnail( 
	const wxWindow *inputWindow, const std::string filename, int width, int height, bool proportional, int margins = 0 );
void TPEXTLIBWXWIDGETS_EXPORT wxCaptureWindowsAndMerge( std::vector<wxWindow *> inputWindows, 
															const std::string filename,
															bool rescale,
															int width, 
															int height, 
															bool proportional );
#endif //wxCatpureWindow_H
