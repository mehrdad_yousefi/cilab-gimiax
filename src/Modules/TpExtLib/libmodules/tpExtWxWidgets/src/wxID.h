/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef WXID_H
#define WXID_H

#include <string>
#include "TpExtLibWxWidgetsWin32Header.h"

/**
This function converts a stringId into a number id. The mapping between stringId and number id is one-to-one.

\ingroup TpExtLibWxWidgets
\author Maarten Nieber
\date 21 may 2008
*/

int TPEXTLIBWXWIDGETS_EXPORT wxID(const std::string& stringId);

#endif //WXID_H
