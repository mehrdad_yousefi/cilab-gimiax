/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "wxEventHandlerHelper.h"
#include <list>

void wxPushBackEventHandler(wxWindow* win, wxEvtHandler *handler)
{
	// W->H1->H2->H3->W
	// handlerNext = H3, the last event handler
    wxEvtHandler *handlerNext = win->GetEventHandler();
	wxEvtHandler *handlerOld = handlerNext;
	while ( handlerNext && handlerNext != win )
	{
		handlerOld = handlerNext;
		handlerNext = handlerNext->GetNextHandler( );
	}

	// The next of the last is always the window
    handler->SetNextHandler( win );

    if ( handlerOld )
	{
		// Connect with the last
		// W->H1->H2->H3->H->W
		handler->SetPreviousHandler(handlerOld);
        handlerOld->SetNextHandler(handler);
	}
	else
	{
		// If there's are no event handlers, set it as the main one
		// W->H->W
		win->SetEventHandler(handler);
	}
    
}


void wxPopEventHandler(wxWindow* win, wxEvtHandler* handlerToRemove)
{
	if ( win == NULL )
	{
		return;
	}

	std::list<wxEvtHandler*> handlerList;
	wxEvtHandler* handler;
	bool isMainHandler = handlerToRemove == win->GetEventHandler();

	// Remove all handlers from the window chain and put them in the list
	handlerList.push_back( handler = win->PopEventHandler( ) );
	while ( handler )
	{
		if (win != win->GetEventHandler()) 
		{
			handler = win->PopEventHandler( );
			if ( handler )
			{
				handlerList.push_back( handler );
			}
		}
		else handler = NULL;
	}

	// remove the desiered handler from the list
	handlerList.remove( handlerToRemove );

	// Push back all handlers from the list
	std::list<wxEvtHandler*>::reverse_iterator it;
	for ( it = handlerList.rbegin() ; it != handlerList.rend() ; it++ )
	{
		win->PushEventHandler( *it );
	}
	
	if ( isMainHandler && !handlerList.empty() )
	{
		win->SetEventHandler( *handlerList.begin() );
	}
}

