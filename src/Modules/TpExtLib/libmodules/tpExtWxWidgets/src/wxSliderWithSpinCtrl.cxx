/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "wxSliderWithSpinCtrl.h"
#include <cmath>
#include <limits>
#include <sstream>
#include <wx/spinctrl.h>

BEGIN_EVENT_TABLE(wxSliderWithSpinCtrl, wxPanel)
  EVT_TEXT(wxID_ANY, wxSliderWithSpinCtrl::OnTextCtrl)
  EVT_COMMAND_SCROLL(wxID_ANY, wxSliderWithSpinCtrl::OnSlider)
END_EVENT_TABLE()


wxSliderWithSpinCtrl::wxSliderWithSpinCtrl( 
	wxWindow *parent, const wxWindowID id, 
	int value , int minValue, 
	int maxValue, const wxPoint& point /*= wxDefaultPosition*/, 
	const wxSize& size /*= wxDefaultSize*/, long style /*= wxSL_HORIZONTAL*/, 
	const wxValidator& validator /*= wxDefaultValidator*/, 
	const wxString& name /*= "slider"*/ ) : wxPanel( parent, id, point, size, style )
{
	wxSizer *sizer = new wxBoxSizer(wxHORIZONTAL);
	SetSizer( sizer );

	m_Slider = new wxSlider(this, wxID_ANY, value, minValue, maxValue);
	m_SpinCtrl = new wxSpinCtrl(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, minValue, maxValue, value );
	m_SpinCtrl->SetSize( 50, 20 );
	m_SpinCtrl->SetMaxSize( wxSize( 50, 20 ) );
	m_SpinCtrl->SetMinSize( wxSize( 50, 20 ) );
	GetSizer()->Add( m_Slider, wxSizerFlags().Expand().Proportion(1).Align( wxALIGN_CENTER_VERTICAL ) );
	GetSizer()->Add( m_SpinCtrl, wxSizerFlags().Align( wxALIGN_CENTER_VERTICAL ) );
}

int wxSliderWithSpinCtrl::GetValue()
{
	return m_SpinCtrl->GetValue();
}

void wxSliderWithSpinCtrl::SetValue( int value )
{
	m_Slider->SetValue( value );
	m_SpinCtrl->SetValue( value );
}

void wxSliderWithSpinCtrl::OnTextCtrl(wxCommandEvent& event)
{
	m_Slider->SetValue( m_SpinCtrl->GetValue() );
	event.Skip();
}

void wxSliderWithSpinCtrl::OnSlider( wxScrollEvent& event )
{
	m_SpinCtrl->SetValue( m_Slider->GetValue() );
	event.Skip();
}

std::string wxSliderWithSpinCtrl::GetValueAsString()
{
	return wxString::Format( "%d", m_SpinCtrl->GetValue() ).ToStdString();
}

void wxSliderWithSpinCtrl::SetValueAsString( const std::string &val )
{
	m_SpinCtrl->SetValue( val );
}

wxSpinCtrl* wxSliderWithSpinCtrl::GetSpinCtrl( )
{
	return m_SpinCtrl;
}

wxSlider* wxSliderWithSpinCtrl::GetSlider( )
{
	return m_Slider;
}

