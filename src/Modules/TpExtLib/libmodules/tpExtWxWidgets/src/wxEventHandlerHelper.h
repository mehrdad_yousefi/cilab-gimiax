/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _wxEventHandlerHelper_H
#define _wxEventHandlerHelper_H

#include "TpExtLibWxWidgetsWin32Header.h"

/**
This function pushes an event handler to the end of the stack

\ingroup TpExtLibWxWidgets
\author Xavi Planes
\date Nov 2010
*/
void TPEXTLIBWXWIDGETS_EXPORT wxPushBackEventHandler(wxWindow* win, wxEvtHandler* handler);

/**
This function removes an event handler from the stack

\ingroup TpExtLibWxWidgets
\author Xavi Planes
\date 26 August 2010
*/
void TPEXTLIBWXWIDGETS_EXPORT wxPopEventHandler(wxWindow* win, wxEvtHandler* handler);

#endif //_wxEventHandlerHelper_H
