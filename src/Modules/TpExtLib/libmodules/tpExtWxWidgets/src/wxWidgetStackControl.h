/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef wxMitkWidgetStackControl_H
#define wxMitkWidgetStackControl_H

#include "TpExtLibWxWidgetsWin32Header.h"
#include <wx/panel.h>
#include <map>

class wxBoxSizer;

// Definitions for enabling the custom wxWidgetRisenEvent event 
// in wxWidgets ///////////
DECLARE_EXPORTED_EVENT_TYPE(TPEXTLIBWXWIDGETS_EXPORT, wxEVT_WIDGET_RISEN, -1)

/**
\brief An event class responsible for signaling that the widget on top 
has changed, in a wxWidgetStackControl
\sa wxWidgetStackControl

\ingroup TpExtLibWxWidgets
\author Juan Antonio Moya
\date 14 Dec 2007
*/
class TPEXTLIBWXWIDGETS_EXPORT wxWidgetRisenEvent : public wxCommandEvent
{

friend class wxWidgetStackControl;

public:
	wxWidgetRisenEvent(
		int winid = 0, 
		wxEventType commandType = wxEVT_WIDGET_RISEN);
	virtual ~wxWidgetRisenEvent(void);
	
	// required for sending it with wxPostEvent()
    wxEvent* Clone();

	wxWindow* GetWidgetOnTop(void);
	int GetWidgetIndexOnTop(void);
	wxWindow* GetPreviousWidgetOnTop(void);
	int GetPreviousWidgetIndexOnTop(void);

private:
	wxWindow* m_currentWidget;
	wxWindow* m_previousWidget;
	int m_currentIndex;
	int m_previousIndex;

	DECLARE_DYNAMIC_CLASS_NO_ASSIGN(wxWidgetRisenEvent)
};


// Definitions for enabling the custom wxWidgetRisenEvent event in 
// wxWidgets ///////////
typedef void (wxEvtHandler::*wxWidgetRisenEventFunction)
(wxWidgetRisenEvent&);
#define wxWidgetRisenEventHandler(func) \
	(wxObjectEventFunction) (wxEventFunction) (wxCommandEventFunction) \
    wxStaticCastEvent( wxWidgetRisenEventFunction, & func )
#define EVT_WIDGET_RISEN(id, fn) \
	DECLARE_EVENT_TABLE_ENTRY(\
		wxEVT_WIDGET_RISEN, \
			id, wxID_ANY, wxWidgetRisenEventHandler(fn), NULL), 


// ------------------------------------------------------------------------
/*
The wxWidgetStackControl class provides a stack of widgets of which 
only the top widget is user-visible.

The application programmer can move any widget to the top of the stack at 
any time using Raise(), and add or remove widgets using Add() and Remove().

wxWidgetStackControl also provides the ability to manipulate widgets 
through application-specified integer IDs. You can also translate from 
widget pointers to IDs using GetIndexOfWidget() and from IDs to widget 
pointers using GetWidgetByIndex(). These numeric IDs are unique in the 
wxWidgetStackControl context, but it does not attach any additional 
meaning to them.

The default widget stack is frameless, but you can use the usual wxWindow 
Styles to add a frame.

wxWidgetStackControl emits a wxWidgetRisenEvent when the user 
changes the widget on top. You may catch this by using the EVT_WIDGET_RISEN
macro.

\ingroup TpExtLibWxWidgets
\author Juan Antonio Moya
\date 14 Dec 2007
*/
class TPEXTLIBWXWIDGETS_EXPORT wxWidgetStackControl : public wxPanel
{
protected:

public:
	typedef std::map<int, wxWindow*> WindowMap;
	typedef WindowMap::value_type WindowMapElement;
	typedef std::pair<WindowMap::iterator, bool> WindowMapInsertResult;

	wxWidgetStackControl(
		wxWindow* parent, 
		wxWindowID id, 
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = wxBORDER_NONE, 
		const wxString& name = wxT("wxWidgetStackControl"));
	virtual ~wxWidgetStackControl(void);

	/**
	\brief Adds a widget (wxWindow*) to the widget stack
	\return index assigned to the widget added. It is an unique identifier 
	for that widget that you must use in the function GetWidgetByIndex().
	*/
	int Add(wxWindow* widget);

	/**
	\brief Removes a widget (wxWindow*) from the widget stack. 
	If the widget removed was the widget on top, then you have to Raise 
	another widget, or nothing will be displayed.
	*/
	void Remove(wxWindow* widget);

	/**
	\brief Removes a widget (wxWindow*) from the widget stack, given its index. 
	If the widget removed was the widget on top, then you have to Raise 
	another widget, or nothing will be displayed.

	\note The widget is selected by its assigned index, when the Add function 
	was called for that widget.
	*/
	void Remove(int widgetIndex);

	/**
	Tells the widget stack to display that widget (wxWindow*) and hide the others. 
	Raising a widget casts a wxWidgetRisenEvent
	*/
	void Raise(wxWindow* widget);

	/**
	Tells the widget stack to display that widget (wxWindow*) and hide 
	the others. Raising a widget casts a wxWidgetRisenEvent
	\note The widget is selected by its assigned index, when the Add function 
	was called for that widget.
	*/
	void Raise(int widgetIndex);

	/**
	\brief Clears the widget stack. 
	It does not delete nor manage the memmory for the widgets (wxWindow*) 
	that had mapped. It clears also the client
	area occupied by the control, so it displays nothing but its background.
	*/
	void Clear(void);

	/**
	\brief Returns the widget currently at the top (being displayed). 
	You may call the Raise function to make a widget being displayed at the top.
	*/
	wxWindow* GetWidgetOnTop(void) const;

	/**
	\brief Returns the assigned index of the widget currently at the top 
	(being displayed). You may call the Raise function to make a widget 
	being displayed at the top.
	\note The assigned index is retrieved by the Add function was called for that widget.
	*/
	unsigned int GetWidgetIndexOnTop(void) const;

	//!
	int GetIndexOfWidget(wxWindow* widget) const;

	//!
	wxWindow* GetWidgetByIndex(int widgetIndex) const;

	//!
	int GetNumberOfWidgets( );
	
	//! Return all widgets with the identifier and the pointer to wxWindow
	WindowMap GetAllWidgets( );

protected:
	//!
	void ClearCurrentWidget(void);
	
private:
	/**
	When user changes the representation, an EVT_WIDGET_RISEN is cast.
	*/
	void UpdateObserversOfWidgetStack(
		int previousIndex, 
		wxWindow* previousWidget);

	WindowMap m_registeredWindowList;
	wxWindow* m_currentWidget;
	int m_currentIndex;
};
 

#endif // wxMitkWidgetStackControl_H
