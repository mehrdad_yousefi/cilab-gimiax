/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "wxAutoCompletionTextCtrl.h"

#include <wx/tooltip.h>
#include "wx/imaglist.h"
#include "wx/combo.h"
#include "wx/config.h"

#include <iostream>
#include <algorithm>
#include <string.h>

class wxSearchListBoxEventHandler : public wxEvtHandler
{
public:

	wxSearchListBoxEventHandler( wxAutoCompletionTextCtrl* searchCtrl )
	{
		m_SearchCtrl = searchCtrl;
	}

	//!
	void OnLeftDown( wxMouseEvent& event );

	void OnMouseMove( wxMouseEvent& event );

	wxAutoCompletionTextCtrl* m_SearchCtrl;

    wxDECLARE_EVENT_TABLE();
};

BEGIN_EVENT_TABLE(wxSearchListBoxEventHandler, wxEvtHandler)
  EVT_MOTION(wxSearchListBoxEventHandler::OnMouseMove)
  EVT_LEFT_DOWN(wxSearchListBoxEventHandler::OnLeftDown)
END_EVENT_TABLE()

void wxSearchListBoxEventHandler::OnLeftDown( wxMouseEvent& event )
{
	if ( !m_SearchCtrl->IsPopupShown() )
	{
		event.Skip();
		return;
	}

	int item = m_SearchCtrl->GetListBox()->HitTest(event.GetPosition());
	if ( item != wxNOT_FOUND )
	{	
		m_SearchCtrl->SelectListBoxItem( item );
	}

}

void wxSearchListBoxEventHandler::OnMouseMove( wxMouseEvent& event )
{
	// Move selection to cursor if it is inside the popup
	int itemHere = m_SearchCtrl->GetListBox()->HitTest( event.GetPosition() );
	if ( m_SearchCtrl->GetListBox()->GetSelection() != itemHere )
	{
		m_SearchCtrl->GetListBox()->SetSelection( itemHere );
	}

	event.Skip();
}


BEGIN_EVENT_TABLE(wxAutoCompletionTextCtrl, wxTextCtrl)
  EVT_TEXT( wxID_ANY, wxAutoCompletionTextCtrl::OnText )
  EVT_KEY_DOWN( wxAutoCompletionTextCtrl::OnKeyDown )
  EVT_SET_FOCUS(wxAutoCompletionTextCtrl::OnFocus)
  EVT_KILL_FOCUS(wxAutoCompletionTextCtrl::OnKillFocus)
  EVT_MOVE(wxAutoCompletionTextCtrl::OnMove)
END_EVENT_TABLE()

//!
wxAutoCompletionTextCtrl::wxAutoCompletionTextCtrl(
	wxWindow* parent, wxWindowID id,
	const wxString& value,
	const wxPoint& pos, const wxSize& size, 
	long style,
	const wxValidator& validator,
	const wxString& name)
: wxTextCtrl(parent, id, value, pos, size, style, validator, name)
{
	m_winPopup = NULL;
	m_ListBox = NULL;
	m_ListBoxEventHandler = NULL;
	m_maxHeight = 200;
    m_iFlags = 0;
	m_SelectingListBoxItem = false;
	m_ShowingPopup = false;
}

//!
wxAutoCompletionTextCtrl::~wxAutoCompletionTextCtrl(void)
{
}

void wxAutoCompletionTextCtrl::CreatePopup()
{
	if ( !m_winPopup )
	{
		m_winPopup = new wxDialog( this, wxID_ANY, wxEmptyString,
			wxPoint(-21,-21), wxSize(20, 20), wxNO_BORDER );

		m_ListBox = new wxListBox( m_winPopup, wxID_ANY );

		m_ListBoxEventHandler = new wxSearchListBoxEventHandler( this );
		m_ListBox->PushEventHandler( m_ListBoxEventHandler );
	}

	m_winPopup->Hide();
}

void wxAutoCompletionTextCtrl::DestroyPopup()
{
	if ( m_winPopup )
	{
		m_ListBox->RemoveEventHandler( m_ListBoxEventHandler );
		delete m_ListBoxEventHandler;
		m_ListBoxEventHandler = NULL;

		m_winPopup->Destroy();
		m_winPopup = NULL;
		m_ListBox = NULL;
	}
}

void wxAutoCompletionTextCtrl::ShowPopup()
{
	if ( m_winPopup != NULL )
	{
		return;
	}

	// Need to disable tab traversal of parent
	//
	// NB: This is to fix a bug in wxMSW. In theory it could also be fixed
	//     by, for instance, adding check to window.cpp:wxWindowMSW::MSWProcessMessage
	//     that if transient popup is open, then tab traversal is to be ignored.
	//     However, I think this code would still be needed for cases where
	//     transient popup doesn't work yet (wxWinCE?).
	wxWindow* parent = GetParent();
	int parentFlags = parent->GetWindowStyle();
	if ( parentFlags & wxTAB_TRAVERSAL )
	{
		parent->SetWindowStyle( parentFlags & ~(wxTAB_TRAVERSAL) );
		m_iFlags |= wxCC_IFLAG_PARENT_TAB_TRAVERSAL;
	}

	if ( m_winPopup == NULL )
	{
		CreatePopup();
	}

	m_winPopup->Enable();
	m_ListBox->SetSize( GetSize().GetWidth(), 200 );
	m_ListBox->Move(0,0);

	wxRect popupWinRect;
	
	wxPoint scrPos = GetParent()->ClientToScreen(GetPosition());

	popupWinRect.x = scrPos.x;
	popupWinRect.y = scrPos.y + GetSize().GetHeight();
	popupWinRect.SetWidth( GetSize( ).GetWidth() );
	popupWinRect.SetHeight( m_heightPopup );
	
	m_winPopup->SetPosition( popupWinRect.GetPosition() );
	m_winPopup->SetSize( popupWinRect.GetSize() );
	m_winPopup->Show();

	// Filter content
	FilterContent( GetValue().ToStdString() );

	// Set focus to text control and set insertion point at the end
	// so that the user can continue typing
	m_ShowingPopup = true;
	SetFocus();
	SetInsertionPointEnd();
	m_ShowingPopup = false;
}

void wxAutoCompletionTextCtrl::HidePopup()
{
	if ( m_winPopup == NULL )
	{
		return;
	}

	// Hide pop up
	m_winPopup->Disable();
	m_winPopup->Hide();

	// Return parent's tab traversal flag.
	// See ShowPopup for notes.
	if ( m_iFlags & wxCC_IFLAG_PARENT_TAB_TRAVERSAL )
	{
		wxWindow* parent = GetParent();
		parent->SetWindowStyle( parent->GetWindowStyle() | wxTAB_TRAVERSAL );
		m_iFlags &= ~(wxCC_IFLAG_PARENT_TAB_TRAVERSAL);
	}

	DestroyPopup();
}

void wxAutoCompletionTextCtrl::OnText( wxCommandEvent& event )
{
	if ( !m_SelectingListBoxItem )
	{

		// Show pop up
		if ( !GetValue().IsEmpty() && !IsPopupShown() )
		{
			ShowPopup();
		}
		else if ( GetValue().IsEmpty() && IsPopupShown() )
		{
			HidePopup();
		}
		else if ( IsPopupShown() )
		{
			// Filter pop up control
			FilterContent( GetValue().ToStdString() );
		}
	}

	event.Skip();
}

void wxAutoCompletionTextCtrl::AppendString( const wxString& item )
{
	m_StringVector.push_back( item.ToStdString() );
}

int wxAutoCompletionTextCtrl::GetStringCount()
{
	return m_StringVector.size();
}

void wxAutoCompletionTextCtrl::ClearStrings()
{
	m_StringVector.clear( );
}

void wxAutoCompletionTextCtrl::FilterContent( const std::string &filter )
{
	std::string filterLower = filter;
	std::transform( filter.begin(), filter.end(), filterLower.begin(), tolower); 
	m_ListBox->Clear();
	std::vector<std::string>::iterator it;
	for ( it = m_StringVector.begin() ; it != m_StringVector.end() ; it++ )
	{
		std::string lowerString = *it;
		std::transform(it->begin(), it->end(), lowerString.begin(), tolower); 
		if ( lowerString.find( filterLower ) != std::string::npos )
		{
			m_ListBox->Append( *it );
		}
	}
}

void wxAutoCompletionTextCtrl::OnKeyDown( wxKeyEvent &event )
{
	if ( !IsPopupShown() )
	{
		// Process event by text control
		event.Skip();
		return;
	}

	if ( ( event.GetKeyCode() == WXK_DOWN || event.GetKeyCode() == WXK_NUMPAD_DOWN ) && m_ListBox->GetCount() )
	{
		int selection = m_ListBox->GetSelection();
		selection = ( selection + 1 ) % m_ListBox->GetCount();
		m_ListBox->Select( selection );
	}
	else if ( ( event.GetKeyCode() == WXK_UP || event.GetKeyCode() == WXK_NUMPAD_UP ) && m_ListBox->GetCount() )
	{
		int selection = m_ListBox->GetSelection();
		selection = ( selection + m_ListBox->GetCount() - 1 ) % m_ListBox->GetCount();
		m_ListBox->Select( selection );
	}
	else if ( event.GetKeyCode() == WXK_RETURN || event.GetKeyCode() == WXK_NUMPAD_ENTER )
	{
		SelectListBoxItem( m_ListBox->GetSelection() );
	}
	else
	{
		// Process event by text control
		event.Skip();
	}
}

void wxAutoCompletionTextCtrl::OnFocus(wxFocusEvent& event)
{
	if ( !m_ShowingPopup )
	{
		wxMenuEvent menuEvt( wxEVT_COMMAND_MENU_SELECTED );
		menuEvt.SetId( wxID_SELECTALL );
		menuEvt.SetEventObject( this );
    wxPostEvent(this->GetEventHandler(), menuEvt);
	}
	
	event.Skip();
}

void wxAutoCompletionTextCtrl::OnKillFocus( wxFocusEvent& event )
{
	if ( FindFocus( ) != this && 
		 FindFocus( ) != m_winPopup && 
		 FindFocus( ) != m_ListBox &&
		 m_ListBox )
	{
		SelectListBoxItem( m_ListBox->FindString( m_SelectedValue ) );
	}

	event.Skip( );
}

void wxAutoCompletionTextCtrl::OnMove(wxMoveEvent& event)
{
	if ( m_ListBox )
	{
		SelectListBoxItem( m_ListBox->FindString( m_SelectedValue ) );
	}

	event.Skip( );
}

void wxAutoCompletionTextCtrl::SetPopupMaxHeight( int height )
{
	m_heightPopup = height;
}

bool wxAutoCompletionTextCtrl::IsPopupShown() const
{
	return m_winPopup != NULL;
}

void wxAutoCompletionTextCtrl::SelectListBoxItem( int item )
{
	if ( !m_ListBox )
	{
		return;
	}

	if ( item != -1 )
	{
		m_SelectedValue = m_ListBox->GetString( item );
	}

	m_SelectingListBoxItem = true;
	HidePopup( );
	SetValue( m_SelectedValue );
	SetInsertionPointEnd();
	m_SelectingListBoxItem = false;
}

wxListBox* wxAutoCompletionTextCtrl::GetListBox() const
{
	return m_ListBox;
}
