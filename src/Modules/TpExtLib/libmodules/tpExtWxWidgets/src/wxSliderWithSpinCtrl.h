/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef wxSliderWithSpinCtrl_H
#define wxSliderWithSpinCtrl_H

#include "TpExtLibWxWidgetsWin32Header.h"
#include "wx/spinctrl.h"

/**
wxSlider with wxSpinCtrl 

\author Xavi Planes
\date 20 Jan 2011
\ingroup DynLib
*/
class TPEXTLIBWXWIDGETS_EXPORT wxSliderWithSpinCtrl : public wxPanel
{
public:
	//!
	wxSliderWithSpinCtrl(wxWindow *parent, 
		const wxWindowID id, int value , int minValue, 
		int maxValue, const wxPoint& point = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, long style = wxSL_HORIZONTAL, 
		const wxValidator& validator = wxDefaultValidator, 
		const wxString& name = "slider");

	//!
	int GetValue();

	//!
	std::string GetValueAsString();

	//!
	void SetValueAsString( const std::string &val );

	//!
	void SetValue( int value );

	//!
	wxSpinCtrl* GetSpinCtrl( );

	//!
	wxSlider* GetSlider( );

private:
    wxDECLARE_EVENT_TABLE();

	//!
	void OnTextCtrl(wxCommandEvent& event);

	//!
	void OnSlider(wxScrollEvent& event);

private:
	//!
	wxSlider* m_Slider;
	//!
	wxSpinCtrl* m_SpinCtrl;
};

#endif // wxSliderWithSpinCtrl_H
