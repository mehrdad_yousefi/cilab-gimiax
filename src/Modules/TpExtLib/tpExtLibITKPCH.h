/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _tpExtLibITKPCH_H
#define _tpExtLibITKPCH_H

#include "itkArray.h"
#include "itkDataObject.h"
#include "itkFixedArray.h"
#include "itkImage.h"
#include "itkImageRegionIterator.h"
#include "itkMacro.h"
#include "itkMatrix.h"
#include "itkNumericTraits.h"
#include "itkObject.h"
#include "itkObjectFactory.h"
#include "itkPixelTraits.h"
#include "itkPoint.h"
#include "itkProcessObject.h"
#include "itkRGBPixel.h"
#include "itkSimpleDataObjectDecorator.h"
#include "itkSize.h"
#include "itkSmartPointer.h"
#include "itkSubsample.h"
#include "itkValarrayImageContainer.h"
#include "itkVariableLengthVector.h"
#include "itkVariableSizeMatrix.h"
#include "itkVector.h"
#include "vnl/vnl_vector_fixed.h"
#include <map>
#include <typeinfo>
#include <vector>
#include "TpExtLibITKWin32Header.h"

#endif // _tpExtLibITKPCH_H

