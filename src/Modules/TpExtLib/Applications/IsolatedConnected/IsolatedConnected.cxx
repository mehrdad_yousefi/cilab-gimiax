/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#ifdef __BORLANDC__
#define ITK_LEAN_AND_MEAN
#endif



#include "itkIsolatedConnectedImageFilter.h"
#include "itkImage.h"
#include "itkCastImageFilter.h"
#include "itkCurvatureFlowImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkPluginFilterWatcher.h"

#include "IsolatedConnectedCLP.h"

// Use an anonymous namespace to keep class types and function names
// from colliding when module is used as shared object module.  Every
// thing should be in an anonymous namespace except for the module
// entry point, e.g. main()
//
namespace {

} // end of anonymous namespace


int main( int argc, char *argv[] )
{
  PARSE_ARGS;

  typedef   float           InternalPixelType;
  const     unsigned int    Dimension = 3;
  typedef itk::Image< InternalPixelType, Dimension >  InternalImageType;

  typedef unsigned short OutputPixelType;
  typedef itk::Image< OutputPixelType, Dimension > OutputImageType;

  typedef itk::CastImageFilter< InternalImageType, OutputImageType >
    CastingFilterType;
  CastingFilterType::Pointer caster = CastingFilterType::New();
                        

  typedef  itk::ImageFileReader< InternalImageType > ReaderType;
  typedef  itk::ImageFileWriter<  OutputImageType  > WriterType;

  ReaderType::Pointer reader = ReaderType::New();
  WriterType::Pointer writer = WriterType::New();

  reader->SetFileName( inputVolume.c_str() );
  reader->Update();
  
  writer->SetFileName( outputVolume.c_str() );


  typedef itk::CurvatureFlowImageFilter< InternalImageType, InternalImageType >
    CurvatureFlowImageFilterType;
  CurvatureFlowImageFilterType::Pointer smoothing = 
                         CurvatureFlowImageFilterType::New();
  itk::PluginFilterWatcher watcher1(smoothing, "Smoothing",
                                    CLPProcessInformation, 0.5, 0.0);

  typedef itk::IsolatedConnectedImageFilter<InternalImageType, InternalImageType> 
    ConnectedFilterType;
  ConnectedFilterType::Pointer isolatedConnected = ConnectedFilterType::New();
  itk::PluginFilterWatcher watcher2(isolatedConnected, "Segmenting",
                                    CLPProcessInformation, 0.5, 0.5);

  smoothing->SetInput( reader->GetOutput() );
  isolatedConnected->SetInput( smoothing->GetOutput() );
  caster->SetInput( isolatedConnected->GetOutput() );
  writer->SetInput( caster->GetOutput() );
  writer->SetUseCompression(1);

  smoothing->SetNumberOfIterations( smoothingIterations );
  smoothing->SetTimeStep( timestep );

  isolatedConnected->SetUpper( upper );
  isolatedConnected->SetLower( lower );
  isolatedConnected->SetFindUpperThreshold( findUpperThreshold );
  isolatedConnected->SetIsolatedValueTolerance( isolatedValueTolerance );

  if (seed1.size() > 0)
    {
    InternalImageType::PointType lpsPoint;
    InternalImageType::IndexType index;
    for (::size_t i=0; i<seed1.size(); ++i)
      {
      // seeds come in ras, convert to lps
      lpsPoint[0] = -seed1[i][0];  
      lpsPoint[1] = -seed1[i][1];
      lpsPoint[2] = seed1[i][2];

      reader->GetOutput()->TransformPhysicalPointToIndex(lpsPoint, index);
      isolatedConnected->AddSeed1(index);

//       std::cout << "LPS: " << lpsPoint << std::endl;
//       std::cout << "IJK: " << index << std::endl;
      }
    }
  else
    {
    std::cerr << "No seeds specified." << std::endl;
    return -1;
    }

  if (seed2.size() > 0)
  {
	  InternalImageType::PointType lpsPoint;
	  InternalImageType::IndexType index;
	  for (::size_t i=0; i<seed2.size(); ++i)
	  {
		  // seeds come in ras, convert to lps
		  lpsPoint[0] = -seed2[i][0];  
		  lpsPoint[1] = -seed2[i][1];
		  lpsPoint[2] = seed2[i][2];

		  reader->GetOutput()->TransformPhysicalPointToIndex(lpsPoint, index);
		  isolatedConnected->AddSeed2(index);

		  //       std::cout << "LPS: " << lpsPoint << std::endl;
		  //       std::cout << "IJK: " << index << std::endl;
	  }
  }
  else
  {
	  std::cerr << "No seeds specified." << std::endl;
	  return -1;
  }


  try
    {
    writer->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }

  return 0;
}




