/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkPluginUtilities.h"
#include "ChainTransformsToVFCLP.h"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "regIO.h"

#include "itkTransformChain.h"
#include "itkTransform.h"
#include "itkSimilarity3DTransform.h"
#include "itkAffineTransform.h"
#include "itkBSplineDeformableTransform.h"
#include "itkVersorRigid3DTransform.h"
#include "itkEuler3DTransform.h"
#include "itkFixedCenterOfRotationAffineTransform.h"

#include "itkTransformFileReader.h"
#include "itkTransformFactory.h"

#include <fstream>
#include <string>
#include <sstream>


using namespace std;



int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	// Use double for reading input image to compute the deformation field
	typedef    double       PixelType;
	const    unsigned int    ImageDimension = 3;


	//////////////////////////////////////////////////////////////////////////

	typedef itk::Image<PixelType, ImageDimension>	ImageType;


	typedef double CoordinateRepType;
	typedef itk::Transform<CoordinateRepType,ImageDimension,ImageDimension>						TransformType;
	const unsigned int SplineOrder = 3;


	typedef itk::ImageFileReader<ImageType> FixedImageReaderType;
	ImageType::ConstPointer	inputImage;
	FixedImageReaderType::Pointer  inputImageReader  = FixedImageReaderType::New();
	inputImageReader->SetFileName	( inputImageFileName );
	try
	{
		inputImageReader->Update();
	}
	catch( itk::ExceptionObject & excp )
	{
		std::cerr << "Error while reading input image files" << std::endl;
		std::cerr << excp << std::endl;
		std::cerr << "[FAILED]" << std::endl;
		return EXIT_FAILURE;
	}
	inputImage	= inputImageReader->GetOutput();

	//////////////////////////////////////////////////////////////////////////

	typedef itk::BSplineDeformableTransform<CoordinateRepType,ImageDimension,ImageDimension>	TransformTypeN;
	typedef itk::AffineTransform<CoordinateRepType,ImageDimension>											TransformTypeA;
	typedef itk::Similarity3DTransform<CoordinateRepType>										TransformTypeS;
	typedef itk::VersorRigid3DTransform<CoordinateRepType>										TransformTypeV;
	typedef itk::Euler3DTransform<CoordinateRepType>											TransformTypeE;
	typedef itk::FixedCenterOfRotationAffineTransform<CoordinateRepType,ImageDimension>						TransformTypeFCRA;

	std::cout << "Reading Input Transform List..." << std::endl;

	typedef itk::TransformChain<double, ImageDimension> TransformChainType;
	TransformChainType::Pointer chainTransform = TransformChainType::New();
     
	itk::TransformFileReader::Pointer tr_reader;

 
	// Add a single transform file name at the beginning of the list
	if ( !singleTransformFileName.empty( ) )
	{
		transformFileName.push_back( singleTransformFileName );
	}

	for (unsigned int tr=0; tr<transformFileName.size( ); tr++)
	{
		tr_reader = itk::TransformFileReader::New();

		tr_reader->SetFileName( transformFileName[tr] );
		try
			{
			tr_reader->Update();
			}
		catch( itk::ExceptionObject & excp )
			{
			std::cerr << "Error while reading the transform file" << std::endl;
			std::cerr << excp << std::endl;
			return EXIT_FAILURE;
			}

		std::cout<<"after transform reading"<<std::endl;

		typedef itk::TransformFileReader::TransformListType * TransformListType;
		TransformListType transforms = tr_reader->GetTransformList();
		std::cout << "Number of transforms = " << transforms->size() << std::endl;
		itk::TransformFileReader::TransformListType::const_iterator it = transforms->begin();

		while (it != transforms->end())
		{ 
			if(!strcmp((*it)->GetNameOfClass(),"BSplineDeformableTransform"))
			{
				TransformTypeN::Pointer transform = static_cast<TransformTypeN*>((*it).GetPointer());
				chainTransform->PushBackTransformation(transform);
			} else if( !strcmp((*it)->GetNameOfClass(),"AffineTransform"))
			{
				TransformTypeA::Pointer transform = static_cast<TransformTypeA*>((*it).GetPointer());
				chainTransform->PushBackTransformation(transform);
			} else if( !strcmp((*it)->GetNameOfClass(),"Similarity3DTransform") )
			{
				TransformTypeS::Pointer transform = static_cast<TransformTypeS*>((*it).GetPointer());
				chainTransform->PushBackTransformation(transform);
			} else if( !strcmp((*it)->GetNameOfClass(),"VersorRigid3DTransform") )
			{
				TransformTypeV::Pointer transform = static_cast<TransformTypeV*>((*it).GetPointer());
				chainTransform->PushBackTransformation(transform);
			} else if( !strcmp((*it)->GetNameOfClass(),"Euler3DTransform") )
			{
				TransformTypeE::Pointer transform = static_cast<TransformTypeE*>((*it).GetPointer());
				chainTransform->PushBackTransformation(transform);
			} else if( !strcmp((*it)->GetNameOfClass(),"FixedCenterOfRotationAffineTransform") )
			{
				TransformTypeFCRA::Pointer transform = static_cast<TransformTypeFCRA*>((*it).GetPointer());
				chainTransform->PushBackTransformation(transform);
			}
			++it; 
		} 
	}

	float extensions_size[ 3 ] = {0,0,0};
	std::vector<float>::iterator itExt = ext_size.begin( );
	int count = 0;
	while ( count < 3 && itExt != ext_size.end( ) )
	{
		extensions_size[ count ] = *itExt;
		count++;
		itExt++;
	}
	int i = WriteVectorFieldFromTransform <TransformChainType, ImageType> ( chainTransform.GetPointer(), inputImage.GetPointer(), OutputVFFileName.c_str( ), extensions_size);

	return i;
}

