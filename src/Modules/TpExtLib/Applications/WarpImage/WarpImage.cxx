/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkPluginUtilities.h"
#include "WarpImageCLP.h"

#include <string.h>

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkNumericTraits.h"

#include "regPostprocessing.h"
#include "regDataOperations.h"
#include "regImageCommon.h"
#include "regVectorCommon.h"


namespace {

	template<class T, class V> int DoIt( int argc, char * argv[], T, V )
	{
		PARSE_ARGS;

		const    unsigned int    ImageDimension = 3;
		typedef T PixelType;
		typedef V VFPixelType;
		typedef T OutputPixelType;
		typedef itk::Image<OutputPixelType, ImageDimension> OutputImageType;

		typedef itk::Vector<VFPixelType, ImageDimension> VectorFieldPixelType;
		typedef itk::Image<PixelType, ImageDimension> ImageType;
		typedef itk::Image<VectorFieldPixelType, ImageDimension> VectorFieldImageType;

		const PixelType padValue = -2048;
		//////////////////////////////////////////////////////////////////////////

		// Reading inputs
		typedef itk::ImageFileReader< ImageType  > InputImageReaderType;
		std::cout << "Reading Input Image..." << std::endl;
		typename InputImageReaderType::Pointer  inputImageReader  = InputImageReaderType::New();
		inputImageReader->SetFileName( inputImageFileName  );
		try
		{
			inputImageReader->Update();
		}
		catch( itk::ExceptionObject & excp )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << excp << std::endl;
			std::cerr << "[FAILED]" << std::endl;
			return EXIT_FAILURE;
		}
		typename ImageType::Pointer I_Image = inputImageReader->GetOutput();


		typedef itk::ImageFileReader< VectorFieldImageType  > InputVectorFieldReaderType;
		std::cout << "Reading Input Vector Field..." << std::endl;
		typename InputVectorFieldReaderType::Pointer  inputVectorFieldReader  = InputVectorFieldReaderType::New();
		inputVectorFieldReader->SetFileName( inputVectorFieldFileName  );
		try
		{
			inputVectorFieldReader->Update();
		}
		catch( itk::ExceptionObject & excp )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << excp << std::endl;
			std::cerr << "[FAILED]" << std::endl;
			return EXIT_FAILURE;
		}
		typename VectorFieldImageType::Pointer V_Image = inputVectorFieldReader->GetOutput();


		//////////////////////////////////////////////////////////////////////////

		// Composing vector fields

		typename ImageType::Pointer Warped_Image = ImageType::New();
		Warped_Image->SetRegions(I_Image->GetBufferedRegion());
		Warped_Image->SetSpacing(I_Image->GetSpacing());
		Warped_Image->SetOrigin(I_Image->GetOrigin());
		Warped_Image->SetDirection(I_Image->GetDirection());
		Warped_Image->Allocate();
		Warped_Image->FillBuffer(padValue);//itk::NumericTraits<PixelType>::ZeroValue());

		std::cout << std::endl << "Warping image..." << std::endl;
		Warped_Image = WarpImageWithVectorField <VectorFieldImageType,ImageType> (I_Image.GetPointer(),V_Image.GetPointer(), padValue);

		typedef itk::CastImageFilter< ImageType, OutputImageType > CastFilterType;
		typename CastFilterType::Pointer Castfilter  = CastFilterType::New();
		Castfilter ->SetInput( Warped_Image );
		Castfilter ->Update();

		// Writing Image
		std::cout << std::endl << "Writing warped image..." << std::endl;

		typedef itk::ImageFileWriter<OutputImageType> ImageWriterType;
		typename ImageWriterType::Pointer writer_I = ImageWriterType::New();
		writer_I->SetInput(Castfilter->GetOutput());
		writer_I->SetFileName(outputImageFileName);

		try
		{
			writer_I->Update();
		}
		catch ( itk::ExceptionObject & err )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << err << std::endl;
			return EXIT_FAILURE;
		}

		//////////////////////////////////////////////////////////////////////////


		return EXIT_SUCCESS;

	}



	template<class T> int DoIt2( int argc, char * argv[], T )
	{
		PARSE_ARGS;

		itk::ImageIOBase::IOPixelType pixelType;
		itk::ImageIOBase::IOComponentType componentType;
		try
		{
			itk::GetImageType (inputVectorFieldFileName, pixelType, componentType);

			// This filter handles all types on input, but only produces
			// signed types
			switch (componentType)
			{
			case itk::ImageIOBase::UCHAR:
				return DoIt( argc, argv, static_cast<T>(0), static_cast<unsigned char>(0));
				break;
			case itk::ImageIOBase::CHAR:
				return DoIt( argc, argv, static_cast<T>(0), static_cast<char>(0));
				break;
			case itk::ImageIOBase::USHORT:
				return DoIt( argc, argv, static_cast<T>(0), static_cast<unsigned short>(0));
				break;
			case itk::ImageIOBase::SHORT:
				return DoIt( argc, argv, static_cast<T>(0), static_cast<short>(0));
				break;
			case itk::ImageIOBase::UINT:
				return DoIt( argc, argv, static_cast<T>(0), static_cast<unsigned int>(0));
				break;
			case itk::ImageIOBase::INT:
				return DoIt( argc, argv, static_cast<T>(0), static_cast<int>(0));
				break;
			case itk::ImageIOBase::ULONG:
				return DoIt( argc, argv, static_cast<T>(0), static_cast<unsigned long>(0));
				break;
			case itk::ImageIOBase::LONG:
				return DoIt( argc, argv, static_cast<T>(0), static_cast<long>(0));
				break;
			case itk::ImageIOBase::FLOAT:
				return DoIt( argc, argv, static_cast<T>(0), static_cast<float>(0));
				break;
			case itk::ImageIOBase::DOUBLE:
				return DoIt( argc, argv, static_cast<T>(0), static_cast<double>(0));
				break;
			case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
			default:
				std::cout << "unknown component type" << std::endl;
				break;
			}
		}

		catch( itk::ExceptionObject &excep)
		{
			std::cerr << argv[0] << ": exception caught !" << std::endl;
			std::cerr << excep << std::endl;
			return EXIT_FAILURE;
		}
		return EXIT_SUCCESS;
	}



} // end of anonymous namespace


int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	itk::ImageIOBase::IOPixelType pixelType;
	itk::ImageIOBase::IOComponentType componentType;
	try
	{
		itk::GetImageType (inputImageFileName, pixelType, componentType);

		// This filter handles all types on input, but only produces
		// signed types
		switch (componentType)
		{
		case itk::ImageIOBase::UCHAR:
			return DoIt2( argc, argv, static_cast<unsigned char>(0));
			break;
		case itk::ImageIOBase::CHAR:
			return DoIt2( argc, argv, static_cast<char>(0));
			break;
		case itk::ImageIOBase::USHORT:
			return DoIt2( argc, argv, static_cast<unsigned short>(0));
			break;
		case itk::ImageIOBase::SHORT:
			return DoIt2( argc, argv, static_cast<short>(0));
			break;
		case itk::ImageIOBase::UINT:
			return DoIt2( argc, argv, static_cast<unsigned int>(0));
			break;
		case itk::ImageIOBase::INT:
			return DoIt2( argc, argv, static_cast<int>(0));
			break;
		case itk::ImageIOBase::ULONG:
			return DoIt2( argc, argv, static_cast<unsigned long>(0));
			break;
		case itk::ImageIOBase::LONG:
			return DoIt2( argc, argv, static_cast<long>(0));
			break;
		case itk::ImageIOBase::FLOAT:
			return DoIt2( argc, argv, static_cast<float>(0));
			break;
		case itk::ImageIOBase::DOUBLE:
			return DoIt2( argc, argv, static_cast<double>(0));
			break;
		case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
		default:
			std::cout << "unknown component type" << std::endl;
			break;
		}
	}

	catch( itk::ExceptionObject &excep)
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		std::cerr << excep << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

