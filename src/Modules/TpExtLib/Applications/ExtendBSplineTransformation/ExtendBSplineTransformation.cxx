/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkPluginUtilities.h"
#include "ExtendBSplineTransformationCLP.h"
#include "itkTransform.h"
#include "itkBSplineDeformableTransform.h"

#include "itkTransformFileReader.h"
#include "itkTransformFileWriter.h"
//#include "itkTransformFactory.h"

#include <fstream>
#include <string>
#include <sstream>


using namespace std;


int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	const unsigned int ImageDimension = 3;

	typedef double CoordinateRepType;

	typedef itk::BSplineDeformableTransform<CoordinateRepType,ImageDimension,3>	TransformTypeN;

	std::cout << "Reading Input Transform..." << std::endl;

	itk::TransformFileReader::Pointer transformReader = itk::TransformFileReader::New();
    transformReader->SetFileName( inputTransformFileName );
	try
	{
		transformReader->Update();
	}
	catch( itk::ExceptionObject & excp )
	{
		std::cerr << "Error while reading the transform file" << std::endl;
		std::cerr << excp << std::endl;
		return EXIT_FAILURE;
	}

    typedef itk::TransformFileReader::TransformListType * TransformListType;
    TransformListType transforms = transformReader->GetTransformList();
    std::cout << "Number of transforms = " << transforms->size() << std::endl;
    itk::TransformFileReader::TransformListType::const_iterator it = transforms->begin();

	TransformTypeN::Pointer extendedTransform = TransformTypeN::New();

    while (it != transforms->end())
	{ 
		if(!strcmp((*it)->GetNameOfClass(),"BSplineDeformableTransform"))
		{
			TransformTypeN::Pointer transform = static_cast<TransformTypeN*>((*it).GetPointer());

			// Extend transform
			TransformTypeN::RegionType extendedRegion = transform->GetGridRegion();
			TransformTypeN::OriginType extendedOrigin = transform->GetGridOrigin();
			TransformTypeN::SpacingType extendedSpacing = transform->GetGridSpacing();
			TransformTypeN::RegionType::SizeType originalSize = extendedRegion.GetSize();
			TransformTypeN::RegionType::SizeType extendedSize = originalSize;

			for( int i = 0; i < ImageDimension; ++i ) {
				extendedSize[i] += (pf+pb);
				extendedOrigin[i] -= pf*extendedSpacing[i];
			}

			extendedRegion.SetSize( extendedSize );

			extendedTransform->SetGridRegion( extendedRegion );
			extendedTransform->SetGridOrigin( extendedOrigin );
			extendedTransform->SetGridSpacing( extendedSpacing ); // Note that this is the original spacing anyway

			TransformTypeN::ParametersType extendedParams( extendedTransform->GetNumberOfParameters() );
			TransformTypeN::ParametersType originalParams = transform->GetParameters();

			int nPDimOld = transform->GetNumberOfParametersPerDimension();
			int nPDimNew = extendedTransform->GetNumberOfParametersPerDimension();

			int indexInOldParams(0);
			int indexInNewParams(0);
			int sx = originalSize[0]; // I preferred a short name for these...
			int sy = originalSize[1];
			int sz = originalSize[2];
			// The conversion of the indexes from the old to the new transform
			// By making the outer loop the z loop and the inner loop the x loop,
			// the indexes jump as little as possible. If this has any consequences
			// for the speed of the process, I don't know. If any, it should be
			// faster than the other way around.
			extendedParams.Fill( 0.0 );
			for( int i = 0; i < ImageDimension; ++i ) {
				for( int lz = 0; lz < sz; ++lz ) {
					for( int ly = 0; ly < sy; ++ly ) {
						for( int lx = 0; lx < sx; ++lx ) {
							indexInNewParams = i*nPDimNew +
								(lz+pf)*(pf+pb+sx)*(pf+pb+sy) +
								(ly+pf)*(pf+pb+sx) +
								(lx+pf);
							indexInOldParams = i*nPDimOld +
								lz*sx*sy + ly*sx + lx;
							extendedParams.SetElement(indexInNewParams,originalParams.GetElement(indexInOldParams));
						}
					}
				}
			}
			extendedTransform->SetParametersByValue( extendedParams );

		} else {
			std::cerr << "Not a B-spline transformation" << std::endl;
			return EXIT_FAILURE;
		}
		++it; 
    } 

	itk::TransformFileWriter::Pointer transformWriter = itk::TransformFileWriter::New();
	transformWriter->SetFileName( outputTransformFileName );
	transformWriter->SetInput( extendedTransform );
	try
	{
		transformWriter->Update();
	}
	catch( itk::ExceptionObject & excp )
	{
		std::cerr << "Error while writing the transform file" << std::endl;
		std::cerr << excp << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
