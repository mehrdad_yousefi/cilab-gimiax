/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkPluginUtilities.h"

#include "itkMaskImageFilter.h"
#include "itkStripTsImageFilter.h"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkPluginFilterWatcher.h"

#include "SkullStripperCLP.h"

// Use an anonymous namespace to keep class types and function names
// from colliding when module is used as shared object module.  Every
// thing should be in an anonymous namespace except for the module
// entry point, e.g. main()
//
namespace {

	template<class T> int DoIt( int argc, char * argv[], T )
	{
		PARSE_ARGS;

		typedef    T       InputPixelType;
		typedef    T       OutputPixelType;
		
		typedef itk::Image< int,  3 >   InputImageType;
		typedef itk::Image< OutputPixelType, 3 >   OutputImageType;
		typedef itk::Image<short, 3> AtlasImageType;
		typedef itk::Image<unsigned char, 3> AtlasLabelType;

		typedef itk::StripTsImageFilter<InputImageType, AtlasImageType, AtlasLabelType> StripTsFilterType;
		typedef itk::MaskImageFilter<InputImageType, AtlasLabelType, OutputImageType> MaskFilterType;
		typedef itk::Image<short, 3> AtlasImageType;
		typedef itk::Image<unsigned char, 3> AtlasLabelType;
		
		typedef itk::ImageFileReader< InputImageType >  ReaderType;
		typedef itk::ImageFileReader<AtlasImageType> AtlasReaderType;
		typedef itk::ImageFileReader<AtlasLabelType> LabelReaderType;
		typedef itk::ImageFileWriter< OutputImageType >  WriterType;
		typedef itk::ImageFileWriter<AtlasLabelType> MaskWriterType;
		
		// Read
		typename ReaderType::Pointer reader = ReaderType::New();
		itk::PluginFilterWatcher watchReader(reader, "Read Volume 1",
											CLPProcessInformation);
		reader->SetFileName( patientImageFilename.c_str() );
		reader->Update( );
		
		typename AtlasReaderType::Pointer atlasReader = AtlasReaderType::New();
		itk::PluginFilterWatcher watchReaderAtlas(atlasReader, "Read Atlas",
											CLPProcessInformation);
		atlasReader->SetFileName( atlasImageFilename );
		atlasReader->Update();

		typename LabelReaderType::Pointer labelReader = LabelReaderType::New();
		itk::PluginFilterWatcher watchReaderLabel(labelReader, "Read Label",
											CLPProcessInformation);
		labelReader->SetFileName( atlasMaskFilename );
		labelReader->Update();
		
		// perform skull-stripping using stripTsImageFilter
		std::cout << std::endl << "Performing skull-stripping" << std::endl;
		
		// set up skull-stripping filter
		typename StripTsFilterType::Pointer stripTsFilter = StripTsFilterType::New();
		itk::PluginFilterWatcher watchStripTsFilter(stripTsFilter, "Skull-Stripping",
											CLPProcessInformation);
		stripTsFilter->SetInput( reader->GetOutput() );
		stripTsFilter->SetAtlasImage( atlasReader->GetOutput() );
		stripTsFilter->SetAtlasBrainMask( labelReader->GetOutput() );
		stripTsFilter->Update();

		// mask the patient image using the output generated from the stripTsImageFilter as mask
		typename MaskFilterType::Pointer maskFilter = MaskFilterType::New();
		maskFilter->SetInput1( reader->GetOutput() );
		maskFilter->SetInput2( stripTsFilter->GetOutput() );
		maskFilter->Update();

		// Write images to disk
		typename MaskWriterType::Pointer maskWriter = MaskWriterType::New();
		itk::PluginFilterWatcher watchMaskWriter(maskWriter,
										   "Write Mask",
										   CLPProcessInformation);
		maskWriter->SetInput( stripTsFilter->GetOutput() );
		maskWriter->SetFileName( outputMaskVolume.c_str() );
		maskWriter->UseCompressionOn();
		maskWriter->Update();
		
		typename WriterType::Pointer imageWriter = WriterType::New();
		itk::PluginFilterWatcher watchWriter(imageWriter,
										   "Write Volume",
										   CLPProcessInformation);
		imageWriter->SetInput( maskFilter->GetOutput() );
		imageWriter->SetFileName( outputVolume.c_str( ) );
		imageWriter->UseCompressionOn();
		imageWriter->Update();
		
		return EXIT_SUCCESS;

	}
	
} // end of anonymous namespace



int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	itk::ImageIOBase::IOPixelType pixelType;
	itk::ImageIOBase::IOComponentType componentType;
	try
	{
		itk::GetImageType (patientImageFilename, pixelType, componentType);

		// This filter handles all types on input, but only produces
		// signed types
		switch (componentType)
		{
		case itk::ImageIOBase::UCHAR:
			return DoIt( argc, argv, static_cast<unsigned char>(0));
			break;
		case itk::ImageIOBase::CHAR:
			return DoIt( argc, argv, static_cast<char>(0));
			break;
		case itk::ImageIOBase::USHORT:
			return DoIt( argc, argv, static_cast<unsigned short>(0));
			break;
		case itk::ImageIOBase::SHORT:
			return DoIt( argc, argv, static_cast<short>(0));
			break;
		case itk::ImageIOBase::UINT:
			return DoIt( argc, argv, static_cast<unsigned int>(0));
			break;
		case itk::ImageIOBase::INT:
			return DoIt( argc, argv, static_cast<int>(0));
			break;
		case itk::ImageIOBase::ULONG:
			return DoIt( argc, argv, static_cast<unsigned long>(0));
			break;
		case itk::ImageIOBase::LONG:
			return DoIt( argc, argv, static_cast<long>(0));
			break;
		case itk::ImageIOBase::FLOAT:
			return DoIt( argc, argv, static_cast<float>(0));
			break;
		case itk::ImageIOBase::DOUBLE:
			return DoIt( argc, argv, static_cast<double>(0));
			break;
		case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
		default:
			std::cout << "unknown component type" << std::endl;
			break;
		}
	}

	catch( itk::ExceptionObject &excep)
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		std::cerr << excep << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

