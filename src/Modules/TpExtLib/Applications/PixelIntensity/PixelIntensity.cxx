/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#ifdef __BORLANDC__
#define ITK_LEAN_AND_MEAN
#endif

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itksys/SystemTools.hxx"

// here go your #includes

#include "itkPluginUtilities.h"

#include "PixelIntensityCLP.h"

#include "blSignalCollective.h"
#include "blSignalWriter.h"

// Use an anonymous namespace to keep class types and function names
// from colliding when module is used as shared object module.  Every
// thing should be in an anonymous namespace except for the module
// entry point, e.g. main()
//
namespace {

template<class T> int DoIt( int argc, char * argv[], T )
{
	PARSE_ARGS;

	typedef    T       InputPixelType;
	typedef itk::Image< InputPixelType,  4 >   InputImageType;
	typedef itk::ImageFileReader< InputImageType >  ReaderType;


	if (point.empty() )
	{
		std::cerr << "No point defined" << std::endl;
		return EXIT_FAILURE;
	}

	// Read image
	typename ReaderType::Pointer reader = ReaderType::New();
	reader->SetFileName( inputVolume.c_str() );
	reader->Update( );
	typename InputImageType::Pointer image = reader->GetOutput( );

	// Get point
	typename InputImageType::PointType lpsPoint;
	lpsPoint[0] = point[0];
	lpsPoint[1] = point[1];
	lpsPoint[2] = point[2];

	// Initialize signal
	blSignalCollective::Pointer signalCollective = blSignalCollective::New( );
	signalCollective->SetNumberOfSignals( 1 );

	// Get values
	std::vector<double> vectorValues;
	for ( int i = 0 ; i < image->GetBufferedRegion().GetSize()[ 3 ] ; i++ )
	{
		// Transform from physical point to index
		typename InputImageType::IndexType index;
		image->TransformPhysicalPointToIndex(lpsPoint, index);
		index[ 3 ] = i;

		// Print values
		std::cout << "Index: " << index << std::endl;
		std::cout << "Pixel: " << image->GetPixel( index ) << std::endl;
		vectorValues.push_back( image->GetPixel( index ) );
	}

	blSignal::Pointer signal = blSignal::Build( "Values", "Intensity", vectorValues );
	signalCollective->SetSignal( 0, signal );

	// Write signal
	blSignalWriter::Pointer writer = blSignalWriter::New( );
	writer->SetFilename( outputSignal.c_str( ) );
	writer->SetInput( signalCollective );
	writer->Update();
 
	return EXIT_SUCCESS;
}

} // end of anonymous namespace


int main( int argc, char * argv[] )
{
  PARSE_ARGS;

  itk::ImageIOBase::IOPixelType pixelType;
  itk::ImageIOBase::IOComponentType componentType;

  try
    {

    itk::GetImageType (inputVolume.c_str(), pixelType, componentType);

    // This filter handles all types on input, but only produces
    // signed types
    switch (componentType)
      {
      case itk::ImageIOBase::UCHAR:
        return DoIt( argc, argv, static_cast<unsigned char>(0));
        break;
      case itk::ImageIOBase::CHAR:
        return DoIt( argc, argv, static_cast<char>(0));
        break;
      case itk::ImageIOBase::USHORT:
        return DoIt( argc, argv, static_cast<unsigned short>(0));
        break;
      case itk::ImageIOBase::SHORT:
        return DoIt( argc, argv, static_cast<short>(0));
        break;
      case itk::ImageIOBase::UINT:
        return DoIt( argc, argv, static_cast<unsigned int>(0));
        break;
      case itk::ImageIOBase::INT:
        return DoIt( argc, argv, static_cast<int>(0));
        break;
      case itk::ImageIOBase::ULONG:
        return DoIt( argc, argv, static_cast<unsigned long>(0));
        break;
      case itk::ImageIOBase::LONG:
        return DoIt( argc, argv, static_cast<long>(0));
        break;
      case itk::ImageIOBase::FLOAT:
        return DoIt( argc, argv, static_cast<float>(0));
        break;
      case itk::ImageIOBase::DOUBLE:
        return DoIt( argc, argv, static_cast<double>(0));
        break;
      case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
      default:
        std::cout << "unknown component type" << std::endl;
        break;
      }
    }

  catch( itk::ExceptionObject &excep)
    {
    std::cerr << argv[0] << ": exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    return EXIT_FAILURE;
    }
  return EXIT_SUCCESS;
}
