/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "vtkJPEGReader.h"
#include "vtkSmartPointer.h"
#include "vtkTexture.h"
#include "vtkImageData.h"
#include "vtkRectangularButtonSource.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkCamera.h"

#include <vector>

const std::string STR_INPUT_IMAGE = "H:/Repositories/VTKData/Data/beach.jpg";

int main( int , char *[] )
{

	vtkSmartPointer<vtkJPEGReader> r = vtkSmartPointer<vtkJPEGReader>::New();
	r->SetFileName( STR_INPUT_IMAGE.c_str() );
	r->Update();

	vtkSmartPointer<vtkTexture> t = vtkSmartPointer<vtkTexture>::New();
	t->SetInputConnection( r->GetOutputPort( ) );

	int *dims = r->GetOutput()->GetDimensions();
	int d1 = dims[0];
	int d2 = dims[1];

	// The third rectangular button
	vtkSmartPointer<vtkRectangularButtonSource> bs3 = vtkSmartPointer<vtkRectangularButtonSource>::New();
	bs3->SetWidth( 1.5 );
	bs3->SetHeight( 0.75 );
	bs3->SetDepth( 0.2 );
	bs3->TwoSidedOn();
	bs3->SetCenter( 0, 1, 0 );
	bs3->SetTextureDimensions( d1, d2 );
	
	vtkSmartPointer<vtkPolyDataMapper> b3Mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	b3Mapper->SetInputConnection( bs3->GetOutputPort() );
	vtkSmartPointer<vtkActor> b3 = vtkSmartPointer<vtkActor>::New();
	b3->SetMapper( b3Mapper );
	b3->SetTexture( t );

	// Create the RenderWindow, Renderer and Interactive Renderer
	vtkSmartPointer<vtkRenderer> ren1 = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkRenderWindow> renWin = vtkSmartPointer<vtkRenderWindow>::New();
	renWin->AddRenderer( ren1 );
	vtkSmartPointer<vtkRenderWindowInteractor> iren = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	iren->SetRenderWindow( renWin );

	ren1->AddActor( b3 );
	ren1->SetBackground( 0, 0, 0 );

	renWin->SetSize( 250, 150 );
	renWin->Render();
	ren1->GetActiveCamera()->Zoom( 1.5 );
	renWin->Render();

	iren->Start();

	return 0;
}
