/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkPluginUtilities.h"
#include "VFComposeCLP.h"

#include <string.h>

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkNumericTraits.h"

#include "regDataOperations.h"
#include "regImageCommon.h"
#include "regVectorCommon.h"




namespace {

	template<class T> int DoIt( int argc, char * argv[], T )
	{
		PARSE_ARGS;

		typedef T PixelType;
		const    unsigned int    ImageDimension = 3; 


		typedef itk::Vector<PixelType, ImageDimension> VectorFieldPixelType;
		typedef itk::Image<PixelType, ImageDimension> ImageType;
		typedef itk::Image<VectorFieldPixelType, ImageDimension> VectorFieldImageType;


		//////////////////////////////////////////////////////////////////////////

		// Reading input vector fields

		typedef itk::ImageFileReader< ImageType  > ImageReaderType;
		typedef itk::ImageFileReader< VectorFieldImageType  > InputVectorFieldReaderType;

		std::cout << "Reading Input Vector Field 01..." << std::endl;
		typename InputVectorFieldReaderType::Pointer  inputVectorField01_ImageReader  = InputVectorFieldReaderType::New();
		inputVectorField01_ImageReader->SetFileName( inputVectorField01_FileName  );
		try
		{
			inputVectorField01_ImageReader->Update();
		}
		catch( itk::ExceptionObject & excp )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << excp << std::endl;
			std::cerr << "[FAILED]" << std::endl;
			return EXIT_FAILURE;
		}
		typename VectorFieldImageType::Pointer V01_Image_tmp = inputVectorField01_ImageReader->GetOutput();

		std::cout << "Reading Input Vector Field 02..." << std::endl;
		typename InputVectorFieldReaderType::Pointer  inputVectorField02_ImageReader  = InputVectorFieldReaderType::New();
		inputVectorField02_ImageReader->SetFileName( inputVectorField02_FileName  );
		try
		{
			inputVectorField02_ImageReader->Update();
		}
		catch( itk::ExceptionObject & excp )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << excp << std::endl;
			std::cerr << "[FAILED]" << std::endl;
			return EXIT_FAILURE;
		}
		typename VectorFieldImageType::Pointer V02_Image_tmp = inputVectorField02_ImageReader->GetOutput();

		//////////////////////////////////////////////////////////////////////////

		typename VectorFieldImageType::Pointer V01_Image = VectorFieldImageType::New();
		typename VectorFieldImageType::Pointer V02_Image = VectorFieldImageType::New();

		if (N_it_expand>0)
		{
			V01_Image = ExtendVF_exp_decrease <VectorFieldImageType> ( V01_Image_tmp.GetPointer(), N_it_expand, sig_decrease_expand, V_radius_expand );
			V02_Image = ExtendVF_exp_decrease <VectorFieldImageType> ( V02_Image_tmp.GetPointer(), N_it_expand, sig_decrease_expand, V_radius_expand );
		}
		else
		{
			V01_Image->SetRegions(V01_Image_tmp->GetBufferedRegion());
			V01_Image->SetSpacing(V01_Image_tmp->GetSpacing());
			V01_Image->SetOrigin(V01_Image_tmp->GetOrigin());
			V01_Image->SetDirection(V01_Image_tmp->GetDirection());
			V01_Image->Allocate();
			V01_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());


			CopyImage <VectorFieldImageType> ( V01_Image_tmp.GetPointer(), V01_Image.GetPointer() );

			V02_Image->SetRegions(V02_Image_tmp->GetBufferedRegion());
			V02_Image->SetSpacing(V02_Image_tmp->GetSpacing());
			V02_Image->SetOrigin(V02_Image_tmp->GetOrigin());
			V02_Image->SetDirection(V02_Image_tmp->GetDirection());
			V02_Image->Allocate();
			V02_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

			CopyImage <VectorFieldImageType> ( V02_Image_tmp.GetPointer(), V02_Image.GetPointer() );
		}

		//////////////////////////////////////////////////////////////////////////

		// Composing vector fields

		typename ImageType::Pointer V02_Image_mask = ImageType::New();
		typename ImageType::Pointer V01_Image_mask = ImageType::New();

		typename VectorFieldImageType::Pointer V02oV01_Image = VectorFieldImageType::New();
		V02oV01_Image->SetRegions(V02_Image->GetBufferedRegion());
		V02oV01_Image->SetSpacing(V02_Image->GetSpacing());
		V02oV01_Image->SetOrigin(V02_Image->GetOrigin());
		V02oV01_Image->SetDirection(V02_Image->GetDirection());
		V02oV01_Image->Allocate();
		V02oV01_Image->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

		typename ImageType::Pointer V02oV01_Image_mask = ImageType::New();
		V02oV01_Image_mask->SetRegions(V02oV01_Image->GetBufferedRegion());
		V02oV01_Image_mask->SetSpacing(V02oV01_Image->GetSpacing());
		V02oV01_Image_mask->SetOrigin(V02oV01_Image->GetOrigin());
		V02oV01_Image_mask->SetDirection(V02oV01_Image->GetDirection());
		V02oV01_Image_mask->Allocate();
		V02oV01_Image_mask->FillBuffer(1);

		//////////////////////////////////////////////////////////////////////////

		if ( !VF01_MaskFileName.empty() )
		{
			typename ImageReaderType::Pointer  VF01_MaskImageReader  = ImageReaderType::New();
			VF01_MaskImageReader->SetFileName( VF01_MaskFileName  );
			try
			{
				VF01_MaskImageReader->Update();
			}
			catch( itk::ExceptionObject & excp )
			{
				std::cerr << "ExceptionObject caught !" << std::endl;
				std::cerr << excp << std::endl;
				std::cerr << "[FAILED]" << std::endl;
				return EXIT_FAILURE;
			}
			V01_Image_mask = VF01_MaskImageReader->GetOutput();
		} 
		else
		{
			V01_Image_mask->SetRegions(V01_Image->GetBufferedRegion());
			V01_Image_mask->SetSpacing(V01_Image->GetSpacing());
			V01_Image_mask->SetOrigin(V01_Image->GetOrigin());
			V01_Image_mask->SetDirection(V01_Image->GetDirection());
			V01_Image_mask->Allocate();
			V01_Image_mask->FillBuffer(1);
		}

		if ( !VF02_MaskFileName.empty() )
		{
			typename ImageReaderType::Pointer  VF02_MaskImageReader  = ImageReaderType::New();
			VF02_MaskImageReader->SetFileName( VF02_MaskFileName  );
			try
			{
				VF02_MaskImageReader->Update();
			}
			catch( itk::ExceptionObject & excp )
			{
				std::cerr << "ExceptionObject caught !" << std::endl;
				std::cerr << excp << std::endl;
				std::cerr << "[FAILED]" << std::endl;
				return EXIT_FAILURE;
			}
			V02_Image_mask = VF02_MaskImageReader->GetOutput();
		} 
		else
		{
			V02_Image_mask->SetRegions(V02_Image->GetBufferedRegion());
			V02_Image_mask->SetSpacing(V02_Image->GetSpacing());
			V02_Image_mask->SetOrigin(V02_Image->GetOrigin());
			V02_Image_mask->SetDirection(V02_Image->GetDirection());
			V02_Image_mask->Allocate();
			V02_Image_mask->FillBuffer(1);
		}

		//////////////////////////////////////////////////////////////////////////

		std::cout << std::endl << "Composing vector fields..." << std::endl;
		int V02oV01_Image_done = ComposeVectorFields <VectorFieldImageType, ImageType> (	V01_Image.GetPointer(), V02_Image.GetPointer(), V02oV01_Image.GetPointer(), V01_Image_mask.GetPointer(), V02_Image_mask.GetPointer(), V02oV01_Image_mask.GetPointer() );

		std::cout << std::endl << "Removing NonpositiveMin values..." << std::endl;
		int V02oV01_Image_done02 = RemoveMaskedValues <VectorFieldImageType, ImageType> (	V02oV01_Image.GetPointer(), V02oV01_Image.GetPointer(), V02oV01_Image_mask.GetPointer() );

		//////////////////////////////////////////////////////////////////////////

		typename VectorFieldImageType::Pointer V02oV01_Image_toWrite = VectorFieldImageType::New();

		if (N_it_expand > 0)
		{
			int start_crop[::itk::GetImageDimension<VectorFieldImageType>::ImageDimension] = {N_it_expand,N_it_expand};
			int size_cropped_VF[::itk::GetImageDimension<VectorFieldImageType>::ImageDimension];
			for (unsigned int dim=0;dim<ImageDimension;dim++)
			{
				size_cropped_VF[dim] = V01_Image_tmp->GetBufferedRegion().GetSize()[dim];
			}
			// Cropping extended vector field before writing output file
			V02oV01_Image_toWrite = CropVectorFieldImage(	V02oV01_Image.GetPointer(), start_crop, size_cropped_VF );
		}
		else
		{
			V02oV01_Image_toWrite->SetRegions(V02oV01_Image->GetBufferedRegion());
			V02oV01_Image_toWrite->SetSpacing(V02oV01_Image->GetSpacing());
			V02oV01_Image_toWrite->SetOrigin(V02oV01_Image->GetOrigin());
			V02oV01_Image_toWrite->SetDirection(V02oV01_Image->GetDirection());
			V02oV01_Image_toWrite->Allocate();
			V02oV01_Image_toWrite->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

			CopyImage <VectorFieldImageType> ( V02oV01_Image.GetPointer(), V02oV01_Image_toWrite.GetPointer() );
		}


		//////////////////////////////////////////////////////////////////////////

		// Writing composition of Vector Fields
		std::cout << std::endl << "Writing composition of vector fields..." << std::endl;

		typedef itk::ImageFileWriter<VectorFieldImageType> VectorFieldWriterType;
		typename VectorFieldWriterType::Pointer writer_V02oV01 = VectorFieldWriterType::New();
		writer_V02oV01->SetInput(V02oV01_Image_toWrite);
		writer_V02oV01->SetFileName(outputVectorFieldFileName);

		try
		{
			writer_V02oV01->Update();
		}
		catch ( itk::ExceptionObject & err )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << err << std::endl;
			return EXIT_FAILURE;
		}

		//////////////////////////////////////////////////////////////////////////

		if ( !VFOut_MaskFileName.empty( ) )
		{
			typedef itk::ImageFileWriter<ImageType> ImageWriterType;
			typename ImageWriterType::Pointer  VFOut_MaskImageWriter  = ImageWriterType::New();
			VFOut_MaskImageWriter->SetInput(V02oV01_Image_mask);
			VFOut_MaskImageWriter->SetFileName(VFOut_MaskFileName);

			try
			{
				VFOut_MaskImageWriter->Update();
			}
			catch ( itk::ExceptionObject & err )
			{
				std::cerr << "ExceptionObject caught !" << std::endl;
				std::cerr << err << std::endl;
				return EXIT_FAILURE;
			}
		}



		return EXIT_SUCCESS;

	}


} // end of anonymous namespace


int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	itk::ImageIOBase::IOPixelType pixelType;
	itk::ImageIOBase::IOComponentType componentType;
	try
	{
		itk::GetImageType (inputVectorField01_FileName, pixelType, componentType);

		// This filter handles all types on input, but only produces
		// signed types
		switch (componentType)
		{
		case itk::ImageIOBase::UCHAR:
			return DoIt( argc, argv, static_cast<unsigned char>(0));
			break;
		case itk::ImageIOBase::CHAR:
			return DoIt( argc, argv, static_cast<char>(0));
			break;
		case itk::ImageIOBase::USHORT:
			return DoIt( argc, argv, static_cast<unsigned short>(0));
			break;
		case itk::ImageIOBase::SHORT:
			return DoIt( argc, argv, static_cast<short>(0));
			break;
		case itk::ImageIOBase::UINT:
			return DoIt( argc, argv, static_cast<unsigned int>(0));
			break;
		case itk::ImageIOBase::INT:
			return DoIt( argc, argv, static_cast<int>(0));
			break;
		case itk::ImageIOBase::ULONG:
			return DoIt( argc, argv, static_cast<unsigned long>(0));
			break;
		case itk::ImageIOBase::LONG:
			return DoIt( argc, argv, static_cast<long>(0));
			break;
		case itk::ImageIOBase::FLOAT:
			return DoIt( argc, argv, static_cast<float>(0));
			break;
		case itk::ImageIOBase::DOUBLE:
			return DoIt( argc, argv, static_cast<double>(0));
			break;
		case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
		default:
			std::cout << "unknown component type" << std::endl;
			break;
		}
	}

	catch( itk::ExceptionObject &excep)
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		std::cerr << excep << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

