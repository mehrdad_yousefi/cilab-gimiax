/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkPluginUtilities.h"

//itk
#include <itkBinaryBallStructuringElement.h>
#include <itkBinaryCrossStructuringElement.h>
#include <itkBinaryDilateImageFilter.h>

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkPluginFilterWatcher.h"

#include "itkNearestNeighborInterpolateImageFunction.h"
#include "itkResampleImageFilter.h"
#include "itkIdentityTransform.h"

#include "DilateCLP.h"

#include <float.h>

// Use an anonymous namespace to keep class types and function names
// from colliding when module is used as shared object module.  Every
// thing should be in an anonymous namespace except for the module
// entry point, e.g. main()
//
namespace {

	template<class T> void FilterStructure( 
		itk::Neighborhood< T, 3 > &structure,
		std::string dimension,
		unsigned long radius)
	{
		if ( dimension == "ALL" )
		{
			return;
		}
		
		for( unsigned long i = 0; i < structure.Size(); i++ )
		{
			// Get pixel coordinates
			unsigned long x = ( i / structure.GetStride( 0 ) ) % (radius*2+1);
			unsigned long y = ( i / structure.GetStride( 1 ) ) % (radius*2+1);
			unsigned long z = ( i / structure.GetStride( 2 ) ) % (radius*2+1);
			if ( dimension == "X" && x != radius )
			{
				structure[ i ] = 0;
			}
			else if ( dimension == "Y" && y != radius )
			{
				structure[ i ] = 0;
			}
			else if ( dimension == "Z" && z != radius )
			{
				structure[ i ] = 0;
			}
		}
	}


	template<class T> int DoIt( int argc, char * argv[], T )
	{
		PARSE_ARGS;

		typedef    T       InputPixelType;
		typedef    T       OutputPixelType;
		
		typedef itk::Image< InputPixelType,  3 >   InputImageType;
		typedef itk::Image< OutputPixelType, 3 >   OutputImageType;
		typedef itk::BinaryBallStructuringElement< InputPixelType, 3 > BallType;
		typedef itk::BinaryCrossStructuringElement< InputPixelType, 3 > CrossType;
		typedef itk::BinaryDilateImageFilter< InputImageType, OutputImageType, BallType > BallDilateFilterType;
		typedef itk::BinaryDilateImageFilter< InputImageType, OutputImageType, CrossType > CrossDilateFilterType;

		typedef itk::NearestNeighborInterpolateImageFunction< InputImageType, double > NearestNeighborInterpolatorType;
		typedef itk::ResampleImageFilter< InputImageType, InputImageType > ResampleFilterType;
		typedef itk::IdentityTransform< double, 3 > TransformType;

		typedef itk::ImageFileReader< InputImageType >  ReaderType;
		typedef itk::ImageFileWriter< OutputImageType >  WriterType;
		
		// Read
		typename ReaderType::Pointer reader = ReaderType::New();
		reader->SetFileName( inputVolume.c_str() );
		reader->Update( );
		typename OutputImageType::Pointer outputImage = reader->GetOutput( );

		// Resample to isotropic voxel size
		typename NearestNeighborInterpolatorType::Pointer nearestNeighborInterpolator = NearestNeighborInterpolatorType::New();
		typename TransformType::Pointer transform = TransformType::New();
		transform->SetIdentity();
		const typename InputImageType::SpacingType& inputSpacing = outputImage->GetSpacing();
		const typename InputImageType::RegionType& inputRegion = outputImage->GetLargestPossibleRegion();
		const typename InputImageType::SizeType& inputSize = inputRegion.GetSize();
		typename InputImageType::SpacingType outputSpacing;

		if ( resample )
		{

			double minSpacing = DBL_MAX;
			for (unsigned int i = 0; i < 3; i++)
			{
				if ( inputSpacing[i] < minSpacing )
				{
					minSpacing = inputSpacing[i];
				}
			}
			for (unsigned int i = 0; i < 3; i++)
			{
				outputSpacing[i] = minSpacing;
			}
			typename InputImageType::SizeType   outputSize;
			typedef typename InputImageType::SizeType::SizeValueType SizeValueType;
			outputSize[0] = static_cast<SizeValueType>(inputSize[0] * inputSpacing[0] / outputSpacing[0] + .5);
			outputSize[1] = static_cast<SizeValueType>(inputSize[1] * inputSpacing[1] / outputSpacing[1] + .5);
			outputSize[2] = static_cast<SizeValueType>(inputSize[2] * inputSpacing[2] / outputSpacing[2] + .5);
			typename ResampleFilterType::Pointer resampler = ResampleFilterType::New();
			itk::PluginFilterWatcher watcherResample1(resampler, "Resample Volume", CLPProcessInformation, 1.0/3.0, 0.0);

			resampler->SetInput( outputImage );
			resampler->SetTransform( transform );
			resampler->SetInterpolator( nearestNeighborInterpolator );
			resampler->SetOutputOrigin ( outputImage->GetOrigin());
			resampler->SetOutputSpacing ( outputSpacing );
			resampler->SetOutputDirection ( outputImage->GetDirection());
			resampler->SetSize ( outputSize );
			resampler->Update ();
			outputImage = resampler->GetOutput( );
		}

		// Dilate
		if (structuringElement == "BALL" )
		{
			BallType ball;
			typename BallDilateFilterType::Pointer ballDilateFilter;
			ball.SetRadius( radius);
			ball.CreateStructuringElement();

			FilterStructure( ball, dimension, radius );

			ballDilateFilter = BallDilateFilterType::New();
			itk::PluginFilterWatcher watchBallDilateFilter(ballDilateFilter,
				"Ball Dilate Filter", CLPProcessInformation,
				1.0/3.0, 1.0/3.0);

			ballDilateFilter->SetKernel(ball);
			ballDilateFilter->SetInput( outputImage );
			ballDilateFilter->SetDilateValue(value);
			//ballDilateFilter->BoundaryToForegroundOn();
			//ballDilateFilter->UpdateLargestPossibleRegion();
			ballDilateFilter->Update( );
			outputImage = ballDilateFilter->GetOutput( );
		}
		else if ( structuringElement == "CROSS" )
		{      
			CrossType cross;
			typename CrossDilateFilterType::Pointer crossDilateFilter;
			cross.SetRadius(radius);
			cross.CreateStructuringElement();

			FilterStructure( cross, dimension, radius );

			crossDilateFilter = CrossDilateFilterType::New();
			itk::PluginFilterWatcher watchBallDilateFilter(crossDilateFilter,
				"Cross Dilate Filter", CLPProcessInformation,
				1.0/3.0, 1.0/3.0);
			crossDilateFilter->SetKernel(cross);
			crossDilateFilter->SetInput( outputImage );
			crossDilateFilter->SetDilateValue(value);
			//crossDilateFilter->BoundaryToForegroundOn();
			//crossDilateFilter->UpdateLargestPossibleRegion();
			crossDilateFilter->Update( );
			outputImage = crossDilateFilter->GetOutput( );
		}

		// Resample
		if ( resample )
		{
			for (unsigned int i = 0; i < 3; i++)
			{
				outputSpacing[ i ] = inputSpacing[ i ];
			}
			typename InputImageType::SizeType   outputSize;
			outputSize[0] = inputSize[0];
			outputSize[1] = inputSize[1];
			outputSize[2] = inputSize[2];

			typename ResampleFilterType::Pointer resampler = ResampleFilterType::New();
			itk::PluginFilterWatcher watcherResample2(resampler,
				"Resample Volume", CLPProcessInformation,
				1.0/3.0, 2.0/3.0);

			resampler->SetInput( outputImage );
			resampler->SetTransform( transform );
			resampler->SetInterpolator( nearestNeighborInterpolator );
			resampler->SetOutputOrigin ( outputImage->GetOrigin() );
			resampler->SetOutputSpacing ( outputSpacing );
			resampler->SetOutputDirection ( outputImage->GetDirection());
			resampler->SetSize ( outputSize );
			resampler->Update ();
			outputImage = resampler->GetOutput( );
		}

		// Write
		typename WriterType::Pointer writer = WriterType::New();
		writer->SetInput( outputImage );
		writer->SetFileName( outputVolume.c_str() );
		writer->Update();
		
		return EXIT_SUCCESS;

	}
	
} // end of anonymous namespace



int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	itk::ImageIOBase::IOPixelType pixelType;
	itk::ImageIOBase::IOComponentType componentType;
	try
	{
		itk::GetImageType (inputVolume, pixelType, componentType);

		// This filter handles all types on input, but only produces
		// signed types
		switch (componentType)
		{
		case itk::ImageIOBase::UCHAR:
			return DoIt( argc, argv, static_cast<unsigned char>(0));
			break;
		case itk::ImageIOBase::CHAR:
			return DoIt( argc, argv, static_cast<char>(0));
			break;
		case itk::ImageIOBase::USHORT:
			return DoIt( argc, argv, static_cast<unsigned short>(0));
			break;
		case itk::ImageIOBase::SHORT:
			return DoIt( argc, argv, static_cast<short>(0));
			break;
		case itk::ImageIOBase::UINT:
			return DoIt( argc, argv, static_cast<unsigned int>(0));
			break;
		case itk::ImageIOBase::INT:
			return DoIt( argc, argv, static_cast<int>(0));
			break;
		case itk::ImageIOBase::ULONG:
			return DoIt( argc, argv, static_cast<unsigned long>(0));
			break;
		case itk::ImageIOBase::LONG:
			return DoIt( argc, argv, static_cast<long>(0));
			break;
		case itk::ImageIOBase::FLOAT:
			return DoIt( argc, argv, static_cast<float>(0));
			break;
		case itk::ImageIOBase::DOUBLE:
			return DoIt( argc, argv, static_cast<double>(0));
			break;
		case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
		default:
			std::cout << "unknown component type" << std::endl;
			break;
		}
	}

	catch( itk::ExceptionObject &excep)
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		std::cerr << excep << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

