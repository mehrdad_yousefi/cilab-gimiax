// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

// For compilers that don't support precompilation, include "wx/wx.h"
#include "wx/wxprec.h"

#ifndef WX_PRECOMP
       #include "wx/wx.h"
#endif

#include "blwxTreeCtrl.h"

#include "blwxAppTreeCtrl.h"
#include "wxAutoCompletionTextCtrl.h"

IMPLEMENT_APP_CONSOLE(blwxAppTreeCtrl)


BEGIN_EVENT_TABLE(blwxAppTreeCtrl, wxApp)
END_EVENT_TABLE()

/**
 */
bool blwxAppTreeCtrl::OnInit( )
{
	wxApp::OnInit( );

	CreateMainWindow( );

	ShowMainWindow( );

	return true;
}

/**
 */
void blwxAppTreeCtrl::CreateMainWindow( )
{
	m_mainWindow = new wxFrame(NULL, wxID_ANY, wxT("blwxAppTreeCtrl test") );
}

/**
 */
void blwxAppTreeCtrl::ShowMainWindow( )
{
	// Add all windows into the main window and show it
	m_mainWindow->SetAutoLayout(true);
	wxBoxSizer* layout = new wxBoxSizer( wxVERTICAL );

	wxTreeCtrl* tree;
	tree = new blwxTreeCtrl(
		m_mainWindow, 
		wxID_ANY, 
		wxDefaultPosition, 
		wxDefaultSize, 
		wxTR_HAS_BUTTONS|wxTR_LINES_AT_ROOT|wxTR_DEFAULT_STYLE|wxSUNKEN_BORDER|wxTR_EDIT_LABELS);
	layout->Add( tree, 1, wxEXPAND );


	wxTreeItemId root = tree->AddRoot( "Root", 0 );
	wxTreeItemId child = tree->AppendItem( root, "Child1" );
	tree->SetItemImage( child, 1 );
	tree->AppendItem( child, "Child1.1", 1 );
	tree->AppendItem( root, "Child2", 1 );
	tree->AppendItem( root, "Child3", 1 );
	tree->AppendItem( root, "Child4", 0 );
	tree->Expand(root);

	// Add a mini frame to test the text control
	wxMiniFrame* miniFrame = new wxMiniFrame( m_mainWindow, wxID_ANY, "MiniFrame" );
	wxBoxSizer* container = new wxBoxSizer(wxVERTICAL);

	wxPanel* comboPanel = CreateComboPanel( m_mainWindow );
	comboPanel->Reparent( miniFrame );
	container->Add(comboPanel, 1, wxEXPAND);

	miniFrame->SetSizer( container );
	miniFrame->Show();

	m_mainWindow->SetSizer(layout);
	m_mainWindow->SetSize( 300, 200 );
	m_mainWindow->Show(true);
}

wxPanel* blwxAppTreeCtrl::CreateComboPanel( wxWindow* parent )
{
	wxPanel* comboPanel = new wxPanel( parent, wxID_ANY );
	wxBoxSizer* sizerCombo = new wxBoxSizer( wxVERTICAL );

	// Text wxSearchComboCtrl
	wxAutoCompletionTextCtrl* comboCtrl = new wxAutoCompletionTextCtrl(comboPanel,wxID_ANY,wxEmptyString);
	comboCtrl->SetPopupMaxHeight( 100 );
	sizerCombo->Add( comboCtrl, 0, wxEXPAND );
	comboCtrl->AppendString(wxT("First Item"));
	comboCtrl->AppendString(wxT("Second Item"));
	comboCtrl->AppendString(wxT("Third Item"));

	comboPanel->SetSizer( sizerCombo );
	sizerCombo->Fit(comboPanel);

	return comboPanel;
}

