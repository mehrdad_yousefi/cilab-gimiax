/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkPluginUtilities.h"

#include "itkMinimumMaximumImageCalculator.h"
#include "itkMultiplyByConstantImageFilter.h"
#include "itkAddConstantToImageFilter.h"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkPluginFilterWatcher.h"

#include "NormalizeMaximumFilterCLP.h"

// Use an anonymous namespace to keep class types and function names
// from colliding when module is used as shared object module.  Every
// thing should be in an anonymous namespace except for the module
// entry point, e.g. main()
//
namespace {

	template<class T> int DoIt( int argc, char * argv[], T )
	{
		PARSE_ARGS;

		typedef    T       InputPixelType;
		typedef    double       OutputPixelType;
		
		typedef itk::Image< InputPixelType,  3 >   InputImageType;
		typedef itk::Image< OutputPixelType, 3 >   OutputImageType;
		typedef itk::MinimumMaximumImageCalculator<InputImageType>  MinMaxFilterType;
		typedef itk::MultiplyByConstantImageFilter<InputImageType, double, OutputImageType> MultiplyImageFilterType;
		typedef itk::AddConstantToImageFilter<OutputImageType, double, OutputImageType> AddConstantToImageFilterType;
		
		typedef itk::ImageFileReader< InputImageType >  ReaderType;
		typedef itk::ImageFileWriter< OutputImageType >  WriterType;
		
		// Read
		typename ReaderType::Pointer reader = ReaderType::New();
		reader->SetFileName( inputVolume.c_str() );
		reader->Update( );
		
		// min max
		typename MinMaxFilterType::Pointer minMaxFilter = MinMaxFilterType::New();
		minMaxFilter->SetImage( reader->GetOutput() );
		minMaxFilter->Compute();
		InputPixelType max = minMaxFilter->GetMaximum();
		InputPixelType min = minMaxFilter->GetMinimum();

		typename MultiplyImageFilterType::Pointer multiplyImageFilter = MultiplyImageFilterType::New();
		itk::PluginFilterWatcher watcher1(multiplyImageFilter, "Multiply Image Filter",
			CLPProcessInformation, 1.0/2.0, 0.0/2.0);
		multiplyImageFilter->SetInput(reader->GetOutput());
		multiplyImageFilter->SetConstant( 100 / (max-min) );
		multiplyImageFilter->Update();

		typename AddConstantToImageFilterType::Pointer addConstantToImageFilter = AddConstantToImageFilterType::New();
		itk::PluginFilterWatcher watcher2(addConstantToImageFilter, "Add constant Image Filter",
			CLPProcessInformation, 1.0/2.0, 1.0/2.0);
		addConstantToImageFilter->SetInput( multiplyImageFilter->GetOutput() );
		addConstantToImageFilter->SetConstant( -min );
		addConstantToImageFilter->Update( );

		typename WriterType::Pointer writer = WriterType::New();
		writer->SetInput( addConstantToImageFilter->GetOutput() );
		writer->SetUseCompression(1);
		writer->SetFileName( outputVolume.c_str() );
		writer->Update();
		
		return EXIT_SUCCESS;

	}
	
} // end of anonymous namespace



int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	itk::ImageIOBase::IOPixelType pixelType;
	itk::ImageIOBase::IOComponentType componentType;
	try
	{
		itk::GetImageType (inputVolume, pixelType, componentType);

		// This filter handles all types on input, but only produces
		// signed types
		switch (componentType)
		{
		case itk::ImageIOBase::UCHAR:
			return DoIt( argc, argv, static_cast<unsigned char>(0));
			break;
		case itk::ImageIOBase::CHAR:
			return DoIt( argc, argv, static_cast<char>(0));
			break;
		case itk::ImageIOBase::USHORT:
			return DoIt( argc, argv, static_cast<unsigned short>(0));
			break;
		case itk::ImageIOBase::SHORT:
			return DoIt( argc, argv, static_cast<short>(0));
			break;
		case itk::ImageIOBase::UINT:
			return DoIt( argc, argv, static_cast<unsigned int>(0));
			break;
		case itk::ImageIOBase::INT:
			return DoIt( argc, argv, static_cast<int>(0));
			break;
		case itk::ImageIOBase::ULONG:
			return DoIt( argc, argv, static_cast<unsigned long>(0));
			break;
		case itk::ImageIOBase::LONG:
			return DoIt( argc, argv, static_cast<long>(0));
			break;
		case itk::ImageIOBase::FLOAT:
			return DoIt( argc, argv, static_cast<float>(0));
			break;
		case itk::ImageIOBase::DOUBLE:
			return DoIt( argc, argv, static_cast<double>(0));
			break;
		case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
		default:
			std::cout << "unknown component type" << std::endl;
			break;
		}
	}

	catch( itk::ExceptionObject &excep)
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		std::cerr << excep << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

