/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkPluginUtilities.h"
#include "VFInverseCLP.h"

#include <string.h>

#include "itkTransformFileReader.h"
#include "itkTransformFileWriter.h"
#include "itkTransformFactory.h"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkBSplineDeformableTransform.h"
#include "itkImageRandomConstIteratorWithIndex.h"
#include "itkResampleImageFilter.h"
#include "itkCastImageFilter.h"

#include "itkNumericTraits.h"

//#include "BSplineDeformableTransformOpt.h"
//#include "itkVectorGradientDeterminantImageFilter.h"
#include "itkVectorLinearInterpolateImageFunction.h"

#include "regDataOperations.h"
#include "regImageCommon.h"
#include "regVectorCommon.h"


namespace {

	template<class T> int DoIt( int argc, char * argv[], T )
	{
		PARSE_ARGS;

		typedef T PixelType;
		const    unsigned int    ImageDimension = 3; 

		//////////////////////////////////////////////////////////////////////////

		// Reading input vector field
		typedef itk::Image<PixelType, ImageDimension> ImageType;

		std::cout << "Reading Input Vector Field..." << std::endl;
		typedef itk::Vector<PixelType, ImageDimension> VectorFieldPixelType;
		typedef itk::Image<VectorFieldPixelType, ImageDimension> VectorFieldImageType;

		typedef itk::ImageFileReader< VectorFieldImageType  > InputVectorFieldReaderType;
		typename InputVectorFieldReaderType::Pointer  inputVectorFieldImageReader  = InputVectorFieldReaderType::New();
		inputVectorFieldImageReader->SetFileName( inputVectorFieldFileName  );
		try
		{
			inputVectorFieldImageReader->Update();
		}
		catch( itk::ExceptionObject & excp )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << excp << std::endl;
			std::cerr << "[FAILED]" << std::endl;
			return EXIT_FAILURE;
		}
		typename VectorFieldImageType::Pointer PhiImage_tmp = inputVectorFieldImageReader->GetOutput();

		//////////////////////////////////////////////////////////////////////////

		std::cout << std::endl << "Computing inverse vector field..." << std::endl;
		std::string energyValue;

		// Computing inverse transform of Vector Field
		typename VectorFieldImageType::Pointer InversePhiImage = VectorFieldImageType::New();

		if (DSRSSpecified)
		{
			typename VectorFieldImageType::Pointer DSPhiImage_tmp = VectorFieldImageType::New();

			int DownSample_done = DownSampleVectorFieldImage <VectorFieldImageType> (	PhiImage_tmp.GetPointer(), DSPhiImage_tmp.GetPointer() );

			unsigned int N_it_expandDS = (unsigned int)((double)N_it_expand/2);
			int V_radius_expandDS;
			if (2*(int)((double)(V_radius_expand)/2) == V_radius_expand){ V_radius_expandDS = V_radius_expand / 2; } 
			else{ V_radius_expandDS = (V_radius_expand+1) / 2; }
			if (V_radius_expandDS < 3)
			{
				V_radius_expandDS = 3;
				// Data extension with C1 constraint only works with neighborhood_radius >=3
			}

			//////////////////////////////////////////////////////////////////////////

			typename VectorFieldImageType::Pointer DSPhiImage = VectorFieldImageType::New();

			if (N_it_expand > 0)
			{
				std::cout << "Expanding Input Vector Field..." << std::endl;
				// Extend vector field before any computation
				DSPhiImage = ExtendVF_exp_decrease <VectorFieldImageType> ( DSPhiImage_tmp.GetPointer(), N_it_expandDS, sig_decrease_expand/2, V_radius_expandDS );
			}
			else
			{
				DSPhiImage->SetRegions(DSPhiImage_tmp->GetBufferedRegion());
				DSPhiImage->SetSpacing(DSPhiImage_tmp->GetSpacing());
				DSPhiImage->SetOrigin(DSPhiImage_tmp->GetOrigin());
				DSPhiImage->SetDirection(DSPhiImage_tmp->GetDirection());
				DSPhiImage->Allocate();
				DSPhiImage->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());


				CopyImage <VectorFieldImageType> ( DSPhiImage_tmp.GetPointer(), DSPhiImage.GetPointer() );
			}

			//////////////////////////////////////////////////////////////////////////

			typename VectorFieldImageType::Pointer DSInversePhiImage = VectorFieldImageType::New();
			DSInversePhiImage->SetRegions(DSPhiImage->GetBufferedRegion());
			DSInversePhiImage->SetSpacing(DSPhiImage->GetSpacing());
			DSInversePhiImage->SetOrigin(DSPhiImage->GetOrigin());
			DSInversePhiImage->SetDirection(DSPhiImage->GetDirection());
			DSInversePhiImage->Allocate();
			DSInversePhiImage->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());


			typename ImageType::Pointer DSPhiImage_mask = ImageType::New();
			DSPhiImage_mask->SetRegions(DSPhiImage->GetBufferedRegion());
			DSPhiImage_mask->SetSpacing(DSPhiImage->GetSpacing());
			DSPhiImage_mask->SetOrigin(DSPhiImage->GetOrigin());
			DSPhiImage_mask->SetDirection(DSPhiImage->GetDirection());
			DSPhiImage_mask->Allocate();
			DSPhiImage_mask->FillBuffer(1);

			typename ImageType::Pointer DSInversePhiImage_mask = ImageType::New();
			DSInversePhiImage_mask->SetRegions(DSPhiImage->GetBufferedRegion());
			DSInversePhiImage_mask->SetSpacing(DSPhiImage->GetSpacing());
			DSInversePhiImage_mask->SetOrigin(DSPhiImage->GetOrigin());
			DSInversePhiImage_mask->SetDirection(DSPhiImage->GetDirection());
			DSInversePhiImage_mask->Allocate();
			DSInversePhiImage_mask->FillBuffer(1);

			int inversePhiImage_done = ComputeInversePhi <VectorFieldImageType, ImageType> (	DSPhiImage.GetPointer(), DSInversePhiImage.GetPointer(), delta_gradient,
				Nmax_gradientDescent, 
				rho_gradientDescent,
				DSPhiImage_mask.GetPointer(),
				DSInversePhiImage_mask.GetPointer(),
				energyValue);

			std::cout << std::endl << "Removing NonpositiveMin values..." << std::endl;
			int inversePhiImage_done02 = RemoveMaskedValues <VectorFieldImageType, ImageType> (	DSInversePhiImage.GetPointer(), DSInversePhiImage.GetPointer(), DSInversePhiImage_mask.GetPointer() );

			//////////////////////////////////////////////////////////////////////////

			typename VectorFieldImageType::Pointer DSInversePhiImage_out = VectorFieldImageType::New();
			DSInversePhiImage_out->SetRegions(DSPhiImage_tmp->GetBufferedRegion());
			DSInversePhiImage_out->SetSpacing(DSPhiImage_tmp->GetSpacing());
			DSInversePhiImage_out->SetOrigin(DSPhiImage_tmp->GetOrigin());
			DSInversePhiImage_out->SetDirection(DSPhiImage_tmp->GetDirection());
			DSInversePhiImage_out->Allocate();
			DSInversePhiImage_out->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

			if (N_it_expand > 0)
			{
				int start_crop[::itk::GetImageDimension<VectorFieldImageType>::ImageDimension] = {N_it_expandDS,N_it_expandDS,N_it_expandDS};
				int size_cropped_VF[::itk::GetImageDimension<VectorFieldImageType>::ImageDimension];
				for (unsigned int dim=0;dim<ImageDimension;dim++)
				{
					size_cropped_VF[dim] = DSInversePhiImage_out->GetBufferedRegion().GetSize()[dim];
				}
				// Cropping extended vector field before writing output file
				DSInversePhiImage_out = CropVectorFieldImage(	DSInversePhiImage.GetPointer(), start_crop, size_cropped_VF );
			}
			else
			{
				CopyImage <VectorFieldImageType> ( DSInversePhiImage.GetPointer(), DSInversePhiImage_out.GetPointer() );
			}

			typename VectorFieldImageType::Pointer RSInversePhiImage = VectorFieldImageType::New();
			int ReSample_done = ReSampleVectorFieldImage <VectorFieldImageType> (	DSInversePhiImage_out.GetPointer(), RSInversePhiImage.GetPointer() );

			//////////////////////////////////////////////////////////////////////////
			// Solving RS problems (1 pixel missing or not), cf. CropVF_3D.cxx:
			typename VectorFieldImageType::Pointer ExRSInversePhiImage = VectorFieldImageType::New();
			ExRSInversePhiImage = ExtendVF_exp_decrease <VectorFieldImageType> ( RSInversePhiImage.GetPointer(), 1, sig_decrease_expand, V_radius_expand );
			// Cropping
			int start_crop[ImageDimension];
			int size_cropped_VF[ImageDimension];
			for (unsigned int dim=0;dim<ImageDimension;dim++)
			{
				start_crop[dim] = 1;
				size_cropped_VF[dim] = PhiImage_tmp->GetBufferedRegion().GetSize()[dim];
			}
			InversePhiImage = CropVectorFieldImage <VectorFieldImageType> (	ExRSInversePhiImage.GetPointer(), start_crop, size_cropped_VF );
			//////////////////////////////////////////////////////////////////////////


		} 
		else
		{
			//////////////////////////////////////////////////////////////////////////
			typename VectorFieldImageType::Pointer PhiImage = VectorFieldImageType::New();

			if (N_it_expand > 0)
			{
				std::cout << "Expanding Input Vector Field..." << std::endl;
				// Extend vector field before any computation
				PhiImage = ExtendVF_exp_decrease <VectorFieldImageType> ( PhiImage_tmp.GetPointer(), N_it_expand, sig_decrease_expand, V_radius_expand );
			}
			else
			{
				PhiImage->SetRegions(PhiImage_tmp->GetBufferedRegion());
				PhiImage->SetSpacing(PhiImage_tmp->GetSpacing());
				PhiImage->SetOrigin(PhiImage_tmp->GetOrigin());
				PhiImage->SetDirection(PhiImage_tmp->GetDirection());
				PhiImage->Allocate();
				PhiImage->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

				CopyImage <VectorFieldImageType> ( PhiImage_tmp.GetPointer(), PhiImage.GetPointer() );
			}


			typename VectorFieldImageType::Pointer InversePhiImage_big = VectorFieldImageType::New();
			InversePhiImage_big->SetRegions(PhiImage->GetBufferedRegion());
			InversePhiImage_big->SetSpacing(PhiImage->GetSpacing());
			InversePhiImage_big->SetOrigin(PhiImage->GetOrigin());
			InversePhiImage_big->SetDirection(PhiImage->GetDirection());
			InversePhiImage_big->Allocate();
			InversePhiImage_big->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

			typename ImageType::Pointer inversePhiImage_mask = ImageType::New();
			inversePhiImage_mask->SetRegions(PhiImage->GetBufferedRegion());
			inversePhiImage_mask->SetSpacing(PhiImage->GetSpacing());
			inversePhiImage_mask->SetOrigin(PhiImage->GetOrigin());
			inversePhiImage_mask->SetDirection(PhiImage->GetDirection());
			inversePhiImage_mask->Allocate();
			inversePhiImage_mask->FillBuffer(1);

			typename ImageType::Pointer PhiImage_mask = ImageType::New();
			PhiImage_mask->SetRegions(PhiImage->GetBufferedRegion());
			PhiImage_mask->SetSpacing(PhiImage->GetSpacing());
			PhiImage_mask->SetOrigin(PhiImage->GetOrigin());
			PhiImage_mask->SetDirection(PhiImage->GetDirection());
			PhiImage_mask->Allocate();
			PhiImage_mask->FillBuffer(1);

			typename ImageType::Pointer InversePhiImage_mask = ImageType::New();
			InversePhiImage_mask->SetRegions(PhiImage->GetBufferedRegion());
			InversePhiImage_mask->SetSpacing(PhiImage->GetSpacing());
			InversePhiImage_mask->SetOrigin(PhiImage->GetOrigin());
			InversePhiImage_mask->SetDirection(PhiImage->GetDirection());
			InversePhiImage_mask->Allocate();
			InversePhiImage_mask->FillBuffer(1);

			int inversePhiImage_done = ComputeInversePhi <VectorFieldImageType, ImageType> (	PhiImage.GetPointer(), InversePhiImage_big.GetPointer(), delta_gradient,
				Nmax_gradientDescent, 
				rho_gradientDescent,
				PhiImage_mask.GetPointer(),
				InversePhiImage_mask.GetPointer(),
				energyValue);

			std::cout << std::endl << "Removing NonpositiveMin values..." << std::endl;
			int inversePhiImage_done02 = RemoveMaskedValues <VectorFieldImageType, ImageType> (	InversePhiImage_big.GetPointer(), InversePhiImage_big.GetPointer(), InversePhiImage_mask.GetPointer() );

			InversePhiImage->SetRegions(PhiImage_tmp->GetBufferedRegion());
			InversePhiImage->SetSpacing(PhiImage_tmp->GetSpacing());
			InversePhiImage->SetOrigin(PhiImage_tmp->GetOrigin());
			InversePhiImage->SetDirection(PhiImage_tmp->GetDirection());
			InversePhiImage->Allocate();
			InversePhiImage->FillBuffer( itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

			if (N_it_expand > 0)
			{
				int start_crop[::itk::GetImageDimension<VectorFieldImageType>::ImageDimension];
				int size_cropped_VF[::itk::GetImageDimension<VectorFieldImageType>::ImageDimension];
				for (unsigned int dim=0;dim<ImageDimension;dim++)
				{
					start_crop[dim] = N_it_expand;
					size_cropped_VF[dim] = InversePhiImage->GetBufferedRegion().GetSize()[dim];
				}
				// Cropping extended vector field before writing output file
				InversePhiImage = CropVectorFieldImage(	InversePhiImage_big.GetPointer(), start_crop, size_cropped_VF );
			}
			else
			{
				CopyImage <VectorFieldImageType> ( InversePhiImage_big.GetPointer(), InversePhiImage.GetPointer() );
			}

		}

		//////////////////////////////////////////////////////////////////////////

		// Writing inverse transform of Vector Field
		std::cout << std::endl << "Writing inverse vector field..." << std::endl;

		typedef itk::ImageFileWriter<VectorFieldImageType> LogVectorFieldWriterType;
		typename LogVectorFieldWriterType::Pointer writer_vectorField_inv = LogVectorFieldWriterType::New();
		writer_vectorField_inv->SetInput(InversePhiImage);
		writer_vectorField_inv->SetFileName(outputInverseVectorFieldFileName);

		try
		{
			writer_vectorField_inv->Update();
		}
		catch ( itk::ExceptionObject & err )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << err << std::endl;
			return EXIT_FAILURE;
		}

		//////////////////////////////////////////////////////////////////////////


		return 0;

	}

} // end of anonymous namespace


int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	itk::ImageIOBase::IOPixelType pixelType;
	itk::ImageIOBase::IOComponentType componentType;
	try
	{
		itk::GetImageType (inputVectorFieldFileName, pixelType, componentType);

		// This filter handles all types on input, but only produces
		// signed types
		switch (componentType)
		{
		case itk::ImageIOBase::UCHAR:
			return DoIt( argc, argv, static_cast<unsigned char>(0));
			break;
		case itk::ImageIOBase::CHAR:
			return DoIt( argc, argv, static_cast<char>(0));
			break;
		case itk::ImageIOBase::USHORT:
			return DoIt( argc, argv, static_cast<unsigned short>(0));
			break;
		case itk::ImageIOBase::SHORT:
			return DoIt( argc, argv, static_cast<short>(0));
			break;
		case itk::ImageIOBase::UINT:
			return DoIt( argc, argv, static_cast<unsigned int>(0));
			break;
		case itk::ImageIOBase::INT:
			return DoIt( argc, argv, static_cast<int>(0));
			break;
		case itk::ImageIOBase::ULONG:
			return DoIt( argc, argv, static_cast<unsigned long>(0));
			break;
		case itk::ImageIOBase::LONG:
			return DoIt( argc, argv, static_cast<long>(0));
			break;
		case itk::ImageIOBase::FLOAT:
			return DoIt( argc, argv, static_cast<float>(0));
			break;
		case itk::ImageIOBase::DOUBLE:
			return DoIt( argc, argv, static_cast<double>(0));
			break;
		case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
		default:
			std::cout << "unknown component type" << std::endl;
			break;
		}
	}

	catch( itk::ExceptionObject &excep)
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		std::cerr << excep << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

