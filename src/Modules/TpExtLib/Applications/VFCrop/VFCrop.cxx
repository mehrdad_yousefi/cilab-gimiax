/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include <string.h>
#include "itkPluginUtilities.h"
#include "VFCropCLP.h"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkImageRegionIterator.h"


//////////////////////////////////////////////////////////////////////////



namespace {

	template<class T> int DoIt( int argc, char * argv[], T )
	{
		PARSE_ARGS;

		typedef T PixelType;
		const    unsigned int    ImageDimension = 3; 
		typedef itk::Vector<PixelType, ImageDimension> VFPixelType;
		typedef itk::Image<PixelType, ImageDimension> ImageType;
		typedef itk::Image<VFPixelType, ImageDimension> VFType;

		//////////////////////////////////////////////////////////////////////////

		typedef itk::ImageFileReader<VFType> VFReaderType;
		typedef itk::ImageFileReader<ImageType> ImageReaderType;
		typedef itk::ImageFileWriter<VFType> VFWriterType;
		typedef itk::ImageFileWriter<ImageType> ImageWriterType;

		typedef itk::ImageRegionIterator<VFType> VFIteratorType;

		typename ImageType::RegionType imRegion;
		typename VFType::RegionType vfRegion;
		typename VFType::IndexType imindex;
		typename VFType::IndexType vfindex;
		typename VFType::PointType physpoint;

		//////////////////////////////////////////////////////////////////////////
	
		// Reading input vector field and image
		typename VFReaderType::Pointer		vfReader	= VFReaderType::New();
		typename ImageReaderType::Pointer	imageReader	= ImageReaderType::New();
		typename VFWriterType::Pointer		vfWriter	= VFWriterType::New();
		typename ImageWriterType::Pointer	imageWriter	= ImageWriterType::New();

		typename VFType::Pointer inputVF;
		typename ImageType::Pointer inputImage;

		std::cout << "Reading input VF...";
		try
		{
			vfReader->SetFileName( inputVFFileName  );
			vfReader->Update();
			inputVF = vfReader->GetOutput();
		}
		catch( itk::ExceptionObject & excp )	
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << excp << std::endl;
			std::cerr << "[FAILED]" << std::endl;
			return EXIT_FAILURE;
		}
		std::cout << "  DONE" << std::endl;

		std::cout << "Reading input Image...";
		try
		{
			imageReader->SetFileName( inputImageFileName  );
			imageReader->Update();
			inputImage = imageReader->GetOutput();
		}
		catch( itk::ExceptionObject & excp )	
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << excp << std::endl;
			std::cerr << "[FAILED]" << std::endl;
			return EXIT_FAILURE;
		}
		std::cout << "  DONE" << std::endl;


		std::cout << "Creating output VF...";
		typename VFType::Pointer outputVF = VFType::New();
		imRegion = inputImage->GetBufferedRegion();
		vfRegion = outputVF->GetBufferedRegion();
		vfRegion.SetSize( imRegion.GetSize() );
		outputVF->SetRegions( vfRegion );
		outputVF->SetOrigin( inputImage->GetOrigin() );
		outputVF->SetSpacing( inputImage->GetSpacing() );
		outputVF->Allocate();
		outputVF->FillBuffer( itk::NumericTraits<VFPixelType>::ZeroValue() );
		std::cout << "  DONE" << std::endl;

	//	inputImage->ReleaseData();

		std::cout << "Filling output VF...";
		VFIteratorType it( outputVF, outputVF->GetBufferedRegion() );
		for ( it.GoToBegin(); !it.IsAtEnd(); ++it ) {
			imindex = it.GetIndex();
			outputVF->TransformIndexToPhysicalPoint( imindex, physpoint );
			inputVF->TransformPhysicalPointToIndex( physpoint, vfindex );
			VFPixelType vfpixel = inputVF->GetPixel( vfindex );
			outputVF->SetPixel( imindex, vfpixel );
			inputImage->SetPixel( imindex, sqrt( float( vfpixel[0]*vfpixel[0] + vfpixel[1]*vfpixel[1] + vfpixel[2]*vfpixel[2] ) ) );
		}
		std::cout << "  DONE" << std::endl;
	
		//////////////////////////////////////////////////////////////////////////

		std::cout << "Writing output VF...";
		try
		{
			vfWriter->SetInput(outputVF.GetPointer());
			vfWriter->SetFileName(outputVFFileName);
			vfWriter->Update();
		}
		catch ( itk::ExceptionObject & err )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << err << std::endl;
			return EXIT_FAILURE;
		}
		std::cout << "  DONE" << std::endl;

		std::cout << "Writing output (magnitude) Image...";
		try
		{
			imageWriter->SetInput( inputImage.GetPointer() );
			imageWriter->SetFileName( outputImageFileName );
			imageWriter->Update();
		}
		catch ( itk::ExceptionObject & err )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << err << std::endl;
			return EXIT_FAILURE;
		}
		std::cout << "  DONE" << std::endl;

		return 0;

	}

} // end of anonymous namespace


int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	itk::ImageIOBase::IOPixelType pixelType;
	itk::ImageIOBase::IOComponentType componentType;
	try
	{
		itk::GetImageType (inputVFFileName, pixelType, componentType);

		// This filter handles all types on input, but only produces
		// signed types
		switch (componentType)
		{
		case itk::ImageIOBase::UCHAR:
			return DoIt( argc, argv, static_cast<unsigned char>(0));
			break;
		case itk::ImageIOBase::CHAR:
			return DoIt( argc, argv, static_cast<char>(0));
			break;
		case itk::ImageIOBase::USHORT:
			return DoIt( argc, argv, static_cast<unsigned short>(0));
			break;
		case itk::ImageIOBase::SHORT:
			return DoIt( argc, argv, static_cast<short>(0));
			break;
		case itk::ImageIOBase::UINT:
			return DoIt( argc, argv, static_cast<unsigned int>(0));
			break;
		case itk::ImageIOBase::INT:
			return DoIt( argc, argv, static_cast<int>(0));
			break;
		case itk::ImageIOBase::ULONG:
			return DoIt( argc, argv, static_cast<unsigned long>(0));
			break;
		case itk::ImageIOBase::LONG:
			return DoIt( argc, argv, static_cast<long>(0));
			break;
		case itk::ImageIOBase::FLOAT:
			return DoIt( argc, argv, static_cast<float>(0));
			break;
		case itk::ImageIOBase::DOUBLE:
			return DoIt( argc, argv, static_cast<double>(0));
			break;
		case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
		default:
			std::cout << "unknown component type" << std::endl;
			break;
		}
	}

	catch( itk::ExceptionObject &excep)
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		std::cerr << excep << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

