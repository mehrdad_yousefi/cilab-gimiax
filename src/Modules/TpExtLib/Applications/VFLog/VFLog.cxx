/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkPluginUtilities.h"
#include "VFLogCLP.h"

#include <string.h>

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkNumericTraits.h"

#include "regDataOperations.h"
#include "regImageCommon.h"
#include "regVectorCommon.h"

//////////////////////////////////////////////////////////////////////////


namespace {

	template<class T> int DoIt( int argc, char * argv[], T )
	{
		PARSE_ARGS;

		typedef T PixelType;

		//////////////////////////////////////////////////////////////////////////

		// Reading input vector field

		std::cout << "Reading Input Vector Field..." << std::endl;
		const    unsigned int    ImageDimension = 3; 
		typedef itk::Vector<PixelType, ImageDimension> VectorFieldPixelType;
		typedef itk::Image<PixelType, ImageDimension> ImageType;
		typedef itk::Image<VectorFieldPixelType, ImageDimension> VectorFieldImageType;

		typedef itk::ImageFileReader< VectorFieldImageType  > InputDefoFieldReaderType;
		typename InputDefoFieldReaderType::Pointer inputVectorFieldImageReader = InputDefoFieldReaderType::New();
		inputVectorFieldImageReader->SetFileName( inputVectorFieldFileName  );
		try
		{
			inputVectorFieldImageReader->Update();
		}
		catch( itk::ExceptionObject & excp )	
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << excp << std::endl;
			std::cerr << "[FAILED]" << std::endl;
			return EXIT_FAILURE;
		}
		typename VectorFieldImageType::Pointer inputVectorFieldImage_tmp = inputVectorFieldImageReader->GetOutput();
		std::cout << "Reading Input Vector Field...   DONE" << std::endl;

		//////////////////////////////////////////////////////////////////////////
		std::ofstream outputDataFile;
		if ( !outputLogAccFileName.empty() )
		{
			outputDataFile.open(outputLogAccFileName.c_str(),std::ios_base::app);
			if (!outputDataFile) {
				std::cerr << "Unable to open file";
				return EXIT_FAILURE;
			}
			std::ostringstream frameNumber_OsFileName;
			std::string frameNumber_StringFileName;
			frameNumber_OsFileName << inputVectorFieldFileName;
			frameNumber_StringFileName = frameNumber_OsFileName.str();
			frameNumber_StringFileName = frameNumber_StringFileName.substr(frameNumber_StringFileName.length() - 8, 4);
			outputDataFile << frameNumber_StringFileName << " ";
		}
		//////////////////////////////////////////////////////////////////////////

		std::string energyValue;

		typename VectorFieldImageType::Pointer Cleaned_log_Image_toWrite = VectorFieldImageType::New();
		Cleaned_log_Image_toWrite->SetRegions(inputVectorFieldImage_tmp->GetBufferedRegion());
		Cleaned_log_Image_toWrite->SetSpacing(inputVectorFieldImage_tmp->GetSpacing());
		Cleaned_log_Image_toWrite->SetOrigin(inputVectorFieldImage_tmp->GetOrigin());
		Cleaned_log_Image_toWrite->SetDirection(inputVectorFieldImage_tmp->GetDirection());
		Cleaned_log_Image_toWrite->Allocate();
		Cleaned_log_Image_toWrite->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());


		std::cout << "Log computations..." << std::endl;

		if (DSRSSpecified)
		{
			typename VectorFieldImageType::Pointer DSinputVectorFieldImage_tmp = VectorFieldImageType::New();

			int DownSample_done = DownSampleVectorFieldImage <VectorFieldImageType> (	inputVectorFieldImage_tmp.GetPointer(), DSinputVectorFieldImage_tmp.GetPointer() );

			typename VectorFieldImageType::Pointer DSCleaned_log_Image_toWrite = VectorFieldImageType::New();
			DSCleaned_log_Image_toWrite->SetRegions(DSinputVectorFieldImage_tmp->GetBufferedRegion());
			DSCleaned_log_Image_toWrite->SetSpacing(DSinputVectorFieldImage_tmp->GetSpacing());
			DSCleaned_log_Image_toWrite->SetOrigin(DSinputVectorFieldImage_tmp->GetOrigin());
			DSCleaned_log_Image_toWrite->SetDirection(DSinputVectorFieldImage_tmp->GetDirection());
			DSCleaned_log_Image_toWrite->Allocate();
			DSCleaned_log_Image_toWrite->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

			int V_radius_expandDS;
			if (2*(int)((double)(V_radius_expand)/2) == V_radius_expand){ V_radius_expandDS = V_radius_expand / 2; } 
			else{ V_radius_expandDS = (V_radius_expand+1) / 2; }
			if (V_radius_expandDS < 3)
			{
				V_radius_expandDS = 3;
				// Data extension with C1 constraint only works with neighborhood_radius >=3
			}
			unsigned int N_it_expandDS = (unsigned int)((double)N_it_expand/2);

			int log_phi_computed = ComputeLogPhi( DSinputVectorFieldImage_tmp.GetPointer(),
				DSCleaned_log_Image_toWrite.GetPointer(),
				N_it_expandDS, sig_decrease_expand/2, V_radius_expandDS,
				N_composition, delta_sqrt, Nmax_it_sqrt, rho_sqrt, delta_inv, Nmax_it_inv, rho_inv, !outputLogAccFileName.empty(), energyValue );

			typename VectorFieldImageType::Pointer RSCleaned_log_Image_toWrite = VectorFieldImageType::New();
			int ReSample_done = ReSampleVectorFieldImage <VectorFieldImageType> (	DSCleaned_log_Image_toWrite.GetPointer(), RSCleaned_log_Image_toWrite.GetPointer() );

			//////////////////////////////////////////////////////////////////////////
			// Solving RS problems (1 pixel missing or not), cf. CropVF_3D.cxx:
			typename VectorFieldImageType::Pointer ExRSCleaned_log_Image_toWrite = VectorFieldImageType::New();
			ExRSCleaned_log_Image_toWrite = ExtendVF_exp_decrease <VectorFieldImageType> ( RSCleaned_log_Image_toWrite.GetPointer(), 1, sig_decrease_expand, V_radius_expand );
			// Cropping
			int start_crop[ImageDimension];
			int size_cropped_VF[ImageDimension];
			for (unsigned int dim=0;dim<ImageDimension;dim++)
			{
				start_crop[dim] = 1;
				size_cropped_VF[dim] = inputVectorFieldImage_tmp->GetBufferedRegion().GetSize()[dim];
			}
			Cleaned_log_Image_toWrite = CropVectorFieldImage <VectorFieldImageType> (	ExRSCleaned_log_Image_toWrite.GetPointer(), start_crop, size_cropped_VF );
			//////////////////////////////////////////////////////////////////////////
		}
		else
		{
			int log_phi_computed = ComputeLogPhi( inputVectorFieldImage_tmp.GetPointer(),
				Cleaned_log_Image_toWrite.GetPointer(),
				N_it_expand, sig_decrease_expand, V_radius_expand,
				N_composition, delta_sqrt, Nmax_it_sqrt, rho_sqrt, delta_inv, Nmax_it_inv, rho_inv, !outputLogAccFileName.empty(), energyValue );
		}

		if (!outputLogAccFileName.empty())
		{
			outputDataFile << energyValue;
		}

		// Writing log transform of Vector Field
		std::cout << "Writing log root " << std::endl;

		typedef itk::ImageFileWriter<VectorFieldImageType> LogVectorFieldWriterType;
		typename LogVectorFieldWriterType::Pointer writer_vectorField_log = LogVectorFieldWriterType::New();
		writer_vectorField_log->SetInput(Cleaned_log_Image_toWrite.GetPointer());
		writer_vectorField_log->SetFileName(outputLogVectorFieldFileName);

		try
		{
			writer_vectorField_log->Update();
		}
		catch ( itk::ExceptionObject & err )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << err << std::endl;
			return EXIT_FAILURE;
		}

		//////////////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////////////

		typename VectorFieldImageType::Pointer recomposedVectorFieldImage_toWrite = VectorFieldImageType::New();

		if ( !outputRecomposedVectorFieldFileName.empty() )
		{
			recomposedVectorFieldImage_toWrite->SetRegions(Cleaned_log_Image_toWrite->GetBufferedRegion());
			recomposedVectorFieldImage_toWrite->SetSpacing(Cleaned_log_Image_toWrite->GetSpacing());
			recomposedVectorFieldImage_toWrite->SetOrigin(Cleaned_log_Image_toWrite->GetOrigin());
			recomposedVectorFieldImage_toWrite->SetDirection(Cleaned_log_Image_toWrite->GetDirection());
			recomposedVectorFieldImage_toWrite->Allocate();
			recomposedVectorFieldImage_toWrite->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());


			// Computing recomposed Vector Field
			int expPhi_ok = ComputeExpPhi <VectorFieldImageType> (	Cleaned_log_Image_toWrite.GetPointer(), recomposedVectorFieldImage_toWrite.GetPointer(), N_it_expand, sig_decrease_expand, V_radius_expand, N_composition );
		}


		//////////////////////////////////////////////////////////////////////////

		typename ImageType::Pointer inputVectorFieldImage_tmp_mask = ImageType::New();
		if (!outputLogAccFileName.empty())
		{
			inputVectorFieldImage_tmp_mask->SetRegions(inputVectorFieldImage_tmp->GetBufferedRegion());
			inputVectorFieldImage_tmp_mask->SetSpacing(inputVectorFieldImage_tmp->GetSpacing());
			inputVectorFieldImage_tmp_mask->SetOrigin(inputVectorFieldImage_tmp->GetOrigin());
			inputVectorFieldImage_tmp_mask->SetDirection(inputVectorFieldImage_tmp->GetDirection());
			inputVectorFieldImage_tmp_mask->Allocate();
			inputVectorFieldImage_tmp_mask->FillBuffer(1);
		}

		if (!outputRecomposedVectorFieldFileName.empty())
		{
			typedef itk::ImageFileWriter<VectorFieldImageType> RecomposedVectorFieldWriterType;
			typename RecomposedVectorFieldWriterType::Pointer writer_vectorField_recomposed = RecomposedVectorFieldWriterType::New();
			writer_vectorField_recomposed->SetInput(recomposedVectorFieldImage_toWrite);
			writer_vectorField_recomposed->SetFileName(outputRecomposedVectorFieldFileName);

			try
			{
				writer_vectorField_recomposed->Update();
			}
			catch ( itk::ExceptionObject & err )
			{
				std::cerr << "ExceptionObject caught !" << std::endl;
				std::cerr << err << std::endl;
				return EXIT_FAILURE;
			}

			if (!outputLogAccFileName.empty())
			{
				std::string energyValue_inv;
				float recomposeAccuracy = ComputeDistanceBetweenPhi<VectorFieldImageType, ImageType>(inputVectorFieldImage_tmp.GetPointer(), recomposedVectorFieldImage_toWrite.GetPointer(),
					inputVectorFieldImage_tmp_mask.GetPointer(), inputVectorFieldImage_tmp_mask.GetPointer(),
					delta_inv, Nmax_it_inv, rho_inv,energyValue_inv,N_it_expand,sig_decrease_expand,V_radius_expand);
				outputDataFile << energyValue_inv << " " << recomposeAccuracy << " ";
			}

			//////////////////////////////////////////////////////////////////////////

		}


		return 0;

	}

} // end of anonymous namespace


int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	itk::ImageIOBase::IOPixelType pixelType;
	itk::ImageIOBase::IOComponentType componentType;
	try
	{
		itk::GetImageType (inputVectorFieldFileName, pixelType, componentType);

		// This filter handles all types on input, but only produces
		// signed types
		switch (componentType)
		{
		case itk::ImageIOBase::UCHAR:
			return DoIt( argc, argv, static_cast<unsigned char>(0));
			break;
		case itk::ImageIOBase::CHAR:
			return DoIt( argc, argv, static_cast<char>(0));
			break;
		case itk::ImageIOBase::USHORT:
			return DoIt( argc, argv, static_cast<unsigned short>(0));
			break;
		case itk::ImageIOBase::SHORT:
			return DoIt( argc, argv, static_cast<short>(0));
			break;
		case itk::ImageIOBase::UINT:
			return DoIt( argc, argv, static_cast<unsigned int>(0));
			break;
		case itk::ImageIOBase::INT:
			return DoIt( argc, argv, static_cast<int>(0));
			break;
		case itk::ImageIOBase::ULONG:
			return DoIt( argc, argv, static_cast<unsigned long>(0));
			break;
		case itk::ImageIOBase::LONG:
			return DoIt( argc, argv, static_cast<long>(0));
			break;
		case itk::ImageIOBase::FLOAT:
			return DoIt( argc, argv, static_cast<float>(0));
			break;
		case itk::ImageIOBase::DOUBLE:
			return DoIt( argc, argv, static_cast<double>(0));
			break;
		case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
		default:
			std::cout << "unknown component type" << std::endl;
			break;
		}
	}

	catch( itk::ExceptionObject &excep)
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		std::cerr << excep << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

