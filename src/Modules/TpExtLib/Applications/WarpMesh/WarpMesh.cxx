/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkPluginUtilities.h"
#include "WarpMeshCLP.h"

#include <string.h>

#include "itkAffineTransform.h"
#include "itkBSplineDeformableTransform.h"
#include "itkDefaultStaticMeshTraits.h"
#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkMesh.h"
#include "itkPoint.h"
#include "itkTransformChain.h"
#include "itkTransformFileReader.h"
#include "itkWarpMeshFilter.h"

#include "vtkCellData.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkSmartPointer.h"


int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	const    unsigned int    ImageDimension = 3;
	typedef double	                                                            CoordinateRepType;

	typedef itk::AffineTransform<CoordinateRepType, ImageDimension>            TransformTypeA;
	typedef itk::BSplineDeformableTransform<CoordinateRepType, ImageDimension> TransformTypeB;
	typedef itk::TransformFileReader                                           TransformReaderType;
	typedef TransformReaderType::TransformListType *                           TransformListType;
	typedef itk::TransformChain<CoordinateRepType, ImageDimension>             TransformChainType;

	typedef itk::DefaultStaticMeshTraits<double,3,3,double,double,double>      MeshTraitsType;
	typedef itk::Mesh<double,ImageDimension,MeshTraitsType>                    MeshType;

	vtkSmartPointer<vtkPolyData>       inMeshVTK  = vtkSmartPointer<vtkPolyData>::New();
	vtkSmartPointer<vtkPolyDataReader> meshReader = vtkSmartPointer<vtkPolyDataReader>::New();
	vtkSmartPointer<vtkPolyDataWriter> meshWriter = vtkSmartPointer<vtkPolyDataWriter>::New();

	MeshType::Pointer        inMeshITK = MeshType::New();
	MeshType::Pointer        outMeshITK;
	TransformListType        transforms;

	TransformChainType::Pointer  transformChain  = TransformChainType::New();

	// Reading inputs
	std::cout << "Reading mesh... ";
	meshReader->SetFileName( inputMeshFileName.c_str( ) );
	meshReader->Update();
	inMeshVTK->DeepCopy( meshReader->GetOutput() );
	std::cout << "Done!" << std::endl;
	
	std::cout << std::endl;

	std::cout << "Mesh information:" << std::endl;
	std::cout << "- Number of vertices     : " << inMeshVTK->GetNumberOfPoints() << std::endl;
	std::cout << "- Number of faces        : " << inMeshVTK->GetNumberOfPolys() << std::endl;
	std::cout << "- Number of vertex arrays: " << inMeshVTK->GetPointData()->GetNumberOfArrays() << std::endl;
	std::cout << "- Number of face arrays  : " << inMeshVTK->GetCellData()->GetNumberOfArrays() << std::endl;

	std::cout << std::endl;

	if ( !singleTransformFileName.empty( ) )
	{
		transformFileNames.push_back( singleTransformFileName );
	}

	for( unsigned int i = 0; i < transformFileNames.size(); ++i )
	{
		std::cout << "Reading transform file " << i << "... ";
		TransformReaderType::Pointer transformReader = TransformReaderType::New();
		transformReader->SetFileName( transformFileNames[i].c_str()  );
		try
		{
			transformReader->Update();
		}
		catch( itk::ExceptionObject & excp )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << excp << std::endl;
			std::cerr << "[FAILED]" << std::endl;
			return EXIT_FAILURE;
		}
		transforms = transformReader->GetTransformList();
		for( TransformReaderType::TransformListType::const_iterator it = transforms->begin(); it != transforms->end(); ++it )
		{ 
			if( !strcmp((*it)->GetNameOfClass(),"AffineTransform"))
			{
				std::cout << "AffineTransform...";
				TransformTypeA::Pointer transform = static_cast<TransformTypeA*>((*it).GetPointer());
				transformChain->PushBackTransformation( transform );
			}
			else if( !strcmp((*it)->GetNameOfClass(),"BSplineDeformableTransform"))
			{
				std::cout << "BSplineDeformableTransform...";
				TransformTypeB::Pointer transform = static_cast<TransformTypeB*>((*it).GetPointer());
				transformChain->PushBackTransformation( transform );
			}
			else
			{
				std::cerr << "All transforms must be either affine or b-spline; aborting." << std::endl;
				return EXIT_FAILURE;
			}
		}
		std::cout << "Done!" << std::endl;
	}

	std::cout << std::endl;

	// Transform the VTK mesh points to the ITK mesh
	double ptVTK[3];
	MeshType::PointType ptITK;
	for( int i = 0; i < inMeshVTK->GetNumberOfPoints(); ++i )
	{
		if( i % 10000 == 0 )
			std::cout << "Transforming point " << i << std::endl;

		inMeshVTK->GetPoint( i, ptVTK );
		for( unsigned int j = 0; j < ImageDimension; ++j )
		{
			ptITK[j] = ptVTK[j];
		}
		ptITK = transformChain->TransformPoint( ptITK );
		for( unsigned int j = 0; j < ImageDimension; ++j )
		{
			ptVTK[j] = ptITK[j];
		}
		inMeshVTK->GetPoints()->SetPoint( i, ptVTK );
	}

	// Write the mesh
	meshWriter->SetFileName( outputMeshFileName.c_str( ) );
	meshWriter->SetInput( inMeshVTK );
	meshWriter->Update();

	return EXIT_SUCCESS;

}

