/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#include "itkPluginUtilities.h"
#include "itkPluginFilterWatcher.h"

#include "ReadDICOMCLP.h"

#include "itkOrientedImage.h"
#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImageSeriesReader.h"

#include "itkGDCMImageIO.h"
#include "itkGDCMSeriesFileNames.h"

#include "itksys/Directory.hxx"

// Use an anonymous namespace to keep class types and function names
// from colliding when module is used as shared object module.  Every
// thing should be in an anonymous namespace except for the module
// entry point, e.g. main()
//
namespace {

	template<class T> int DoIt( int argc, char * argv[], T )
	{
		PARSE_ARGS;

		typedef    T       InputPixelType;
		typedef    T       OutputPixelType;
		const unsigned int      Dimension = 3;

		typedef itk::Image< InputPixelType,  3 >   InputImageType;
		typedef itk::Image< OutputPixelType, 3 >   OutputImageType;
		typedef itk::OrientedImage< InputPixelType, Dimension >         ImageType;
		typedef itk::ImageFileWriter< OutputImageType >  WriterType;


		// Read DICOM
		typedef itk::ImageSeriesReader< ImageType >        ReaderType;
		typename ReaderType::Pointer reader = ReaderType::New();

		typedef itk::GDCMImageIO       ImageIOType;
		typename ImageIOType::Pointer dicomIO = ImageIOType::New();
		reader->SetImageIO( dicomIO );

		typedef itk::GDCMSeriesFileNames NamesGeneratorType;
		typename NamesGeneratorType::Pointer nameGenerator = NamesGeneratorType::New();
		nameGenerator->SetUseSeriesDetails( true );
		nameGenerator->AddSeriesRestriction("0008|0021" );
		nameGenerator->SetDirectory( dicomDirectory );

		// Find Series
		std::cout << std::endl << "The directory: " << std::endl;
		std::cout << std::endl << argv[1] << std::endl << std::endl;
		std::cout << "Contains the following DICOM Series: ";
		std::cout << std::endl << std::endl;
		typedef std::vector< std::string >    SeriesIdContainer;
		const SeriesIdContainer & seriesUID = nameGenerator->GetSeriesUIDs();
		SeriesIdContainer::const_iterator seriesItr = seriesUID.begin();
		SeriesIdContainer::const_iterator seriesEnd = seriesUID.end();
		while( seriesItr != seriesEnd )
		{
			std::cout << seriesItr->c_str() << std::endl;
			seriesItr++;
		}

		std::string seriesIdentifier = seriesUID.begin()->c_str();

		// Read series
		std::cout << std::endl << std::endl;
		std::cout << "Now reading series: " << std::endl << std::endl;
		std::cout << seriesIdentifier << std::endl;
		std::cout << std::endl << std::endl;
		typedef std::vector< std::string >   FileNamesContainer;
		FileNamesContainer fileNames;
		fileNames = nameGenerator->GetFileNames( seriesIdentifier );
		reader->SetFileNames( fileNames );
		reader->Update();

		// Writer
		typename WriterType::Pointer writer = WriterType::New();
		writer->SetInput( reader->GetOutput() );
		writer->SetUseCompression(1);
		writer->SetFileName( outputVolume.c_str() );
		writer->Update();

		return EXIT_SUCCESS;

	}

} // end of anonymous namespace


int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	itk::ImageIOBase::IOComponentType componentType;
	try
	{
		itksys::Directory dir;
		dir.Load( dicomDirectory.c_str() );
		std::string firstFile;
		for (int i = 0; i < dir.GetNumberOfFiles(); i++) {
			std::string currentFile(dir.GetFile( i ));
			if (currentFile != "." && currentFile != "..") {
				firstFile = currentFile;
				break;
			}
		}
		if ( firstFile.empty() )
		{
			std::cout << "No files in directory" << dicomDirectory << std::endl;
			return EXIT_FAILURE;
		}

		//use GDCMImageIO to obtain pizel type (which also take into account Rescale Intersect and Slope)
		itk::GDCMImageIO::Pointer gdcmImageIO = itk::GDCMImageIO::New();
		// Avoid . and ..
		gdcmImageIO->SetFileName( dicomDirectory + "/" + firstFile );
		gdcmImageIO->ReadImageInformation();
		componentType = gdcmImageIO->GetComponentType();

		// This filter handles all types on input, but only produces
		// signed types
		switch (componentType)
		{
		case itk::ImageIOBase::UCHAR:
			return DoIt( argc, argv, static_cast<unsigned char>(0));
			break;
		case itk::ImageIOBase::CHAR:
			return DoIt( argc, argv, static_cast<char>(0));
			break;
		case itk::ImageIOBase::USHORT:
			return DoIt( argc, argv, static_cast<unsigned short>(0));
			break;
		case itk::ImageIOBase::SHORT:
			return DoIt( argc, argv, static_cast<short>(0));
			break;
		case itk::ImageIOBase::UINT:
			return DoIt( argc, argv, static_cast<unsigned int>(0));
			break;
		case itk::ImageIOBase::INT:
			return DoIt( argc, argv, static_cast<int>(0));
			break;
		case itk::ImageIOBase::ULONG:
			return DoIt( argc, argv, static_cast<unsigned long>(0));
			break;
		case itk::ImageIOBase::LONG:
			return DoIt( argc, argv, static_cast<long>(0));
			break;
		case itk::ImageIOBase::FLOAT:
			return DoIt( argc, argv, static_cast<float>(0));
			break;
		case itk::ImageIOBase::DOUBLE:
			return DoIt( argc, argv, static_cast<double>(0));
			break;
		case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
		default:
			std::cout << "unknown component type" << std::endl;
			break;
		}
	}

	catch( itk::ExceptionObject &excep)
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		std::cerr << excep << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
