/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "AppendBodyParts3DCLP.h"
#include "itksys/Directory.hxx"
#include "itksys/SystemTools.hxx"

#include <iostream>
#include <fstream>
#include <map>
#include "blTextUtils.h"

#include "vtkOBJReader.h"
#include "vtkSmartPointer.h"
#include "vtkPolyData.h"
#include "vtkAppendPolyData.h"
#include "vtkPolyDataWriter.h"

void SaveVTK( const std::string &filePath, vtkSmartPointer<vtkPolyData> shape )
{
	std::cout << filePath << std::endl;
	vtkSmartPointer<vtkPolyDataWriter> writer;
	writer = vtkSmartPointer<vtkPolyDataWriter>::New();
	writer->SetFileName( filePath.c_str() );
	writer->SetInput( shape );
	if ( shape->GetNumberOfPoints() > 300000 )
	{
		writer->SetFileTypeToBinary();
	}
	writer->Update();
}


int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	try
	{

		// Load parts file
		std::ifstream iFile( parts_list_e.c_str() );
		if ( !iFile.good() )
		{
			std::cerr << "Cannot open " << parts_list_e << std::endl;
			return EXIT_FAILURE;
		}

		// Parse file and fill map of part ID and organ system name
		std::map<std::string, std::string> mapParts;
		bool fileisOk = true;
		std::string line;
		while ( fileisOk )
		{
			// Parse line using TAB char
			fileisOk = std::getline(iFile,line) && fileisOk;
			if  (!fileisOk)
			{
				break;
			}
			std::list<std::string> values;
			blTextUtils::ParseLine( line, '\t', values );

			// Skip first line of captions
			if ( values.size() && *values.begin() == "\"id\"" )
			{
				continue;
			}

			int count = 0;
			std::list<std::string>::iterator it;
			for( it = values.begin() ; it != values.end() ; it++ )
			{
				if ( count == 3 )
				{
					mapParts[ *values.begin() ] = *it;
				}
				count++;
			}
		}

		// Load directory
		std::map< std::string, vtkSmartPointer<vtkPolyData> > organSystemMap;
		itksys::Directory dir;
		dir.Load( inputFolder.c_str( ) );
		std::map< std::string, int> organSystemCountMap;
		for ( unsigned long i = 0 ; i < dir.GetNumberOfFiles( ) ; i++ )
		{
			std::string filename = dir.GetFile( i );
			std::string partName = itksys::SystemTools::GetFilenameWithoutExtension( filename.c_str() );

			if ( filename == "." || filename == ".." )
			{
				continue;
			}

			if ( mapParts.find( partName ) == mapParts.end() )
			{
				std::cerr << "Cannot find organ system for " << partName << std::endl;
				continue;
			}

			std::string systemName = mapParts[partName];

			if ( systemName == "nervous system" && nervous_system == false )
			{
				continue;
			}
			if ( systemName == "sense organ system" && sense_organ_system == false )
			{
				continue;
			}
			if ( systemName == "cardiovascular system" && cardiovascular_system == false )
			{
				continue;
			}
			if ( systemName == "respiratory system" && respiratory_system == false )
			{
				continue;
			}
			if ( systemName == "alimentary system" && alimentary_system == false )
			{
				continue;
			}
			if ( systemName == "endocrine system" && endocrine_system == false )
			{
				continue;
			}
			if ( systemName == "lymphoid system" && lymphoid_system == false )
			{
				continue;
			}
			if ( systemName == "urinary system" && urinary_system == false )
			{
				continue;
			}
			if ( systemName == "genital system" && genital_system == false )
			{
				continue;
			}
			if ( systemName == "skeletal system" && skeletal_system == false )
			{
				continue;
			}
			if ( systemName == "muscular system" && muscular_system == false )
			{
				continue;
			}
			if ( systemName == "articular system" && articular_system == false )
			{
				continue;
			}
			if ( systemName == "integumentary system" && integumentary_system == false )
			{
				continue;
			}
			if ( systemName == "others" && others == false )
			{
				continue;
			}

			// Create new polydata
			if ( organSystemMap.find( systemName ) == organSystemMap.end() )
			{
				organSystemMap[ systemName ] = vtkSmartPointer<vtkPolyData>::New( );
				organSystemCountMap[ systemName ] = 0;
			}

			// Read file
			vtkSmartPointer<vtkOBJReader> reader = vtkSmartPointer<vtkOBJReader>::New();
			std::string filePath = dir.GetPath();
			filePath += "/" + filename;
			reader->SetFileName( filePath.c_str() );
			reader->Update( );
			if (!reader->GetOutput())
			{
				std::cerr << "Cannot read " << filePath << std::endl;
				continue;
			}

			vtkSmartPointer<vtkAppendPolyData> append = vtkSmartPointer<vtkAppendPolyData>::New();
			append->AddInput( organSystemMap[ systemName ] );
			append->AddInput( reader->GetOutput() );
			append->Update();
			organSystemMap[ systemName ] = append->GetOutput();

			// Split in parts otherwise there's a memory allocation problem
			if ( organSystemMap[ systemName ]->GetNumberOfPoints() > 600000 )
			{
				std::stringstream str;
				str << systemName << organSystemCountMap[ systemName ];
				std::string filePath = outputFolder + "/" + str.str() + ".vtk";
				SaveVTK( filePath, organSystemMap[ systemName ] );

				organSystemMap[ systemName ] = vtkSmartPointer<vtkPolyData>::New( );
				organSystemCountMap[ systemName ]++;
			}
		}

		// Save last part
		std::map< std::string, vtkSmartPointer<vtkPolyData> >::iterator itOrganSystemMap;
		for ( itOrganSystemMap = organSystemMap.begin() ; itOrganSystemMap != organSystemMap.end() ; itOrganSystemMap++)
		{
			std::stringstream str;
			str << itOrganSystemMap->first << organSystemCountMap[ itOrganSystemMap->first ];
			std::string filePath = outputFolder + "/" + str.str() + ".vtk";
			SaveVTK( filePath, itOrganSystemMap->second );
		}
	}
	catch( ... )
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

