/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkPluginUtilities.h"
#include "VFDownSampleCLP.h"

#include <string.h>

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkNumericTraits.h"

#include "regDataOperations.h"
#include "regVectorCommon.h"

//////////////////////////////////////////////////////////////////////////


namespace {

	template<class T> int DoIt( int argc, char * argv[], T )
	{
		PARSE_ARGS;

		typedef T PixelType;
		const    unsigned int    ImageDimension = 3; 
		typedef itk::Vector<PixelType, ImageDimension> VectorFieldPixelType;
		typedef itk::Image<PixelType, ImageDimension> ImageType;
		typedef itk::Image<VectorFieldPixelType, ImageDimension> VectorFieldImageType;

		//////////////////////////////////////////////////////////////////////////

		typedef itk::ImageFileReader< VectorFieldImageType  > InputVFReaderType;

		typename VectorFieldImageType::SizeType RefVF_size;
		if ( !RSFileName.empty( ) )
		{
			typename InputVFReaderType::Pointer RefVFImageReader = InputVFReaderType::New();
			RefVFImageReader->SetFileName( RSFileName  );
			try
			{
				RefVFImageReader->Update();
			}
			catch( itk::ExceptionObject & excp )	
			{
				std::cerr << "ExceptionObject caught !" << std::endl;
				std::cerr << excp << std::endl;
				std::cerr << "[FAILED]" << std::endl;
				return EXIT_FAILURE;
			}
			RefVF_size = (RefVFImageReader->GetOutput())->GetBufferedRegion().GetSize();
		}


		//////////////////////////////////////////////////////////////////////////
	
		// Reading input vector field
		std::cout << "Reading input VF..." << std::endl;

		typename InputVFReaderType::Pointer inputVFImageReader = InputVFReaderType::New();
		inputVFImageReader->SetFileName( inputVFFileName  );
		try
		{
			inputVFImageReader->Update();
		}
		catch( itk::ExceptionObject & excp )	
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << excp << std::endl;
			std::cerr << "[FAILED]" << std::endl;
			return EXIT_FAILURE;
		}
		typename VectorFieldImageType::Pointer inputVFImage_tmp = inputVFImageReader->GetOutput();

		typename VectorFieldImageType::Pointer outputVF = VectorFieldImageType::New();

		if ( !RSFileName.empty( ) )
		{
			std::cout << "RS VF..." << std::endl;

			typename VectorFieldImageType::Pointer RSoutputVF_tmp = VectorFieldImageType::New();
			int ReSample_done = ReSampleVectorFieldImage <VectorFieldImageType> (	inputVFImage_tmp.GetPointer(), RSoutputVF_tmp.GetPointer() );
			//////////////////////////////////////////////////////////////////////////
			// Solving RS problems (1 pixel missing or not):
			//
			// Original image (5x4):     DS image (3x2):   (x = pixel used for DS, . = non used)
			// x.x.x                     xxx
			// .....                     xxx
			// x.x.x
			// .....	
			//
			// RS image (5x3) = interpolation possible everywhere as data is defined on corners:
			// x.x.x
			// .....
			// x.x.x
			// 
			// Extension of 1 pixel (7x5) to crop later on according to original image:
			// .......
			// .x.x.x.
			// .......
			// .x.x.x.
			// .......
			//
			// Cropping according to original image (5x4): start = {1,1}   dim = {5,4}
			// x.x.x
			// .....
			// x.x.x
			// .....
			//
			//////////////////////////////////////////////////////////////////////////

			inputVFImage_tmp->ReleaseData();

			//////////////////////////////////////////////////////////////////////////

			typename VectorFieldImageType::Pointer ExRSoutputVF = VectorFieldImageType::New();
			ExRSoutputVF = ExtendVF_exp_decrease <VectorFieldImageType> ( RSoutputVF_tmp.GetPointer(), 1, sig_decrease_expand, V_radius_expand );
			// Cropping
			int start_crop[ImageDimension];
			int size_cropped_VF[ImageDimension];
			for (unsigned int dim=0;dim<ImageDimension;dim++)
			{
				start_crop[dim] = 1;
				size_cropped_VF[dim] = RefVF_size[dim];
			}
			outputVF = CropVectorFieldImage <VectorFieldImageType> (	ExRSoutputVF.GetPointer(), start_crop, size_cropped_VF );
		} 
		else
		{
			std::cout << "DS VF..." << std::endl;

			int DownSample_done = DownSampleVectorFieldImage <VectorFieldImageType> (	inputVFImage_tmp.GetPointer(), outputVF.GetPointer() );
		}

		typedef itk::ImageFileWriter<VectorFieldImageType> VectorFieldWriterType;
		typename VectorFieldWriterType::Pointer writer_DS = VectorFieldWriterType::New();
		writer_DS->SetInput(outputVF.GetPointer());
		writer_DS->SetFileName(outputVFFileName);

		try
		{
			writer_DS->Update();
		}
		catch ( itk::ExceptionObject & err )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << err << std::endl;
			return EXIT_FAILURE;
		}

		return 0;

	}



} // end of anonymous namespace


int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	itk::ImageIOBase::IOPixelType pixelType;
	itk::ImageIOBase::IOComponentType componentType;
	try
	{
		itk::GetImageType (inputVFFileName, pixelType, componentType);

		// This filter handles all types on input, but only produces
		// signed types
		switch (componentType)
		{
		case itk::ImageIOBase::UCHAR:
			return DoIt( argc, argv, static_cast<unsigned char>(0));
			break;
		case itk::ImageIOBase::CHAR:
			return DoIt( argc, argv, static_cast<char>(0));
			break;
		case itk::ImageIOBase::USHORT:
			return DoIt( argc, argv, static_cast<unsigned short>(0));
			break;
		case itk::ImageIOBase::SHORT:
			return DoIt( argc, argv, static_cast<short>(0));
			break;
		case itk::ImageIOBase::UINT:
			return DoIt( argc, argv, static_cast<unsigned int>(0));
			break;
		case itk::ImageIOBase::INT:
			return DoIt( argc, argv, static_cast<int>(0));
			break;
		case itk::ImageIOBase::ULONG:
			return DoIt( argc, argv, static_cast<unsigned long>(0));
			break;
		case itk::ImageIOBase::LONG:
			return DoIt( argc, argv, static_cast<long>(0));
			break;
		case itk::ImageIOBase::FLOAT:
			return DoIt( argc, argv, static_cast<float>(0));
			break;
		case itk::ImageIOBase::DOUBLE:
			return DoIt( argc, argv, static_cast<double>(0));
			break;
		case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
		default:
			std::cout << "unknown component type" << std::endl;
			break;
		}
	}

	catch( itk::ExceptionObject &excep)
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		std::cerr << excep << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

