/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "MarchingCubesCLP.h"
#include "vtkMarchingCubes.h"
#include "vtkSmartPointer.h"
#include "vtkStructuredPointsReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkStructuredPoints.h"

#include "itksys/SystemTools.hxx"
#include "blTagMap.h"
#include "blXMLTagMapWriter.h"

int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	try
	{

		// Read input mesh
		vtkSmartPointer<vtkStructuredPointsReader> reader;
		reader = vtkSmartPointer<vtkStructuredPointsReader>::New();
		reader->SetFileName( inputVolume.c_str( ) );
		reader->Update();

		// Apply marching cubes
		vtkSmartPointer<vtkMarchingCubes> mcubes;
		mcubes = vtkSmartPointer<vtkMarchingCubes>::New();
		mcubes->SetInput( reader->GetOutput() );
		mcubes->SetComputeScalars( ComputeScalars );
		mcubes->SetComputeGradients( ComputeGradients );
		mcubes->SetComputeNormals( ComputeNormals );
		mcubes->SetValue( 0, Value );
		mcubes->Update();

		// Write output mesh
		vtkSmartPointer<vtkPolyDataWriter> writer;
		writer = vtkSmartPointer<vtkPolyDataWriter>::New();
		writer->SetFileName( outputMesh.c_str( ) );
		writer->SetInput( mcubes->GetOutput( ) );
		writer->Write();

		// Create rendering properties
		blTagMap::Pointer metadata = blTagMap::New( );
		blTagMap::Pointer rendering = blTagMap::New( );
		rendering->AddTag( "opacity", float(0.5) );
		rendering->AddTag( "hideParent", false );
		metadata->AddTag( "Rendering", rendering );

		// Write GMI
		std::string path = itksys::SystemTools::GetFilenamePath( outputMesh );
		std::string filename = itksys::SystemTools::GetFilenameWithoutExtension( outputMesh );
		std::string extension = ".gmi";
		std::string gmiFilename = path + "/" + filename + extension;
		blXMLTagMapWriter::Pointer gmiWriter = blXMLTagMapWriter::New();
		gmiWriter->SetFilename( gmiFilename.c_str() );
		gmiWriter->SetInput( metadata );
		gmiWriter->Update( );
		
	}
	catch (vtkstd::exception& e)
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		std::cerr << e.what( ) << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
