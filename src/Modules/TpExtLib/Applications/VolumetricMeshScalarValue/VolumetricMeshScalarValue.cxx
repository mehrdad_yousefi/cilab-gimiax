/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#ifdef __BORLANDC__
#define ITK_LEAN_AND_MEAN
#endif

#include "VolumetricMeshScalarValueCLP.h"

#include "vtkUnstructuredGrid.h"
#include "vtkUnstructuredGridReader.h"
#include "vtkSmartPointer.h"
#include "vtkPointData.h"
#include "vtkCellData.h"

#include "blSignalCollective.h"
#include "blSignalWriter.h"
#include "blITKFileUtils.h"

int main( int argc, char * argv[] )
{
  PARSE_ARGS;

  try
    {
		// Check input point
		if (point.empty() )
		{
			std::cerr << "No point defined" << std::endl;
			return EXIT_FAILURE;
		}

		// Get input filenames
		std::vector<std::string> inputFileNames;
		blITKFileUtils::CheckDataFilenames( inputVolumeMesh, inputFileNames );

		// Read data
		std::vector<vtkSmartPointer<vtkUnstructuredGrid> > meshVector;
		for ( std::vector<std::string>::iterator itFileName = inputFileNames.begin( ) ; 
			  itFileName != inputFileNames.end( ) ; 
			  itFileName++ )
		{
			vtkSmartPointer<vtkUnstructuredGridReader> uReader;
			uReader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
			uReader->ReadAllScalarsOn();
			uReader->ReadAllVectorsOn();
			uReader->SetFileName( itFileName->c_str() );
			uReader->Update();
			meshVector.push_back( uReader->GetOutput() );
		}

		// Initialize signal
		blSignalCollective::Pointer signalCollective = blSignalCollective::New( );
		signalCollective->SetNumberOfSignals( 1 );

		// Get values
		std::vector<double> vectorValues;
		for ( std::vector<vtkSmartPointer<vtkUnstructuredGrid> >::iterator itMesh = meshVector.begin( ) ; 
			  itMesh != meshVector.end( ) ; itMesh++ )
		{
			// Select array
			vtkDataArray* dataArray;
			vtkIdType elemId;
			if ( dataType == "PointData" )
			{
				dataArray = (*itMesh)->GetPointData( )->GetArray( arrayName.c_str( ) );
				elemId = (*itMesh)->FindPoint( point[ 0 ], point[ 1 ], point[ 2 ] );
			}
			else if ( dataType == "CellData" )
			{
				dataArray = (*itMesh)->GetCellData( )->GetArray( arrayName.c_str( ) );
				
				double  	x[3] = { point[ 0 ], point[ 1 ], point[ 2 ] };
				double pcoords[3];
                int subId;
                double * weights;
                elemId = (*itMesh)->FindCell( x, NULL, NULL, -1, 1, subId, pcoords, weights);
			}

			if ( elemId < 0 )
			{
				std::cerr << "Cannot find point or cell at coordinates: " 
					<< point[ 0 ] << ", " << point[ 1 ] << " ," << point[ 2 ] << std::endl;
				return EXIT_FAILURE;
			}

			if ( dataArray == NULL )
			{
				std::cerr << "Cannot find array" << arrayName << std::endl;
				return EXIT_FAILURE;
			}

			double value = dataArray->GetTuple1( elemId );

			// Print values
			std::cout << "Index: " << elemId << std::endl;
			std::cout << "Value: " << value << std::endl;
			vectorValues.push_back( value );
		}

		blSignal::Pointer signal = blSignal::Build( "Values", arrayName, vectorValues );
		signalCollective->SetSignal( 0, signal );

		// Write signal
		blSignalWriter::Pointer writer = blSignalWriter::New( );
		writer->SetFilename( outputSignal.c_str( ) );
		writer->SetInput( signalCollective );
		writer->Update();
 
		return EXIT_SUCCESS;
    }
	catch (vtkstd::exception& e)
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		std::cerr << e.what( ) << std::endl;
		return EXIT_FAILURE;
	}

  return EXIT_SUCCESS;
}
