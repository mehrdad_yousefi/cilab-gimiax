/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkPluginUtilities.h"
#include "BSplinesTransformCLP.h"

#include "itkImageRegistrationMethod.h"
#include "itkMattesMutualInformationImageToImageMetric.h"
#include "itkLinearInterpolateImageFunction.h"
#include "itkImage.h"

#include "itkTimeProbesCollectorBase.h"

#include "itkBSplineDeformableTransform.h"
#include "itkLBFGSBOptimizer.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkResampleImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkSquaredDifferenceImageFilter.h"
#include <itkRegularStepGradientDescentOptimizer.h>
#include <itkTransformFileWriter.h>

#include "itkSmoothingRecursiveGaussianImageFilter.h"
#include "itkTransformFileReader.h"
#include "itkTransformFileWriter.h"
#include "itkTransformFactory.h"

#include "itkBSplineResampleImageFunction.h"
#include "itkBinaryThresholdImageFilter.h"

#include "itkNumericTraits.h"
#include <itkImageMaskSpatialObject.h>

#include "itkTransformChain.h"

#include "regFFDSplit.h"
#include "regPreprocessing.h"
#include "regPostprocessing.h"
#include "regIO.h"
#include "regFFDSet.h"

#include <vector>
using namespace std;

#include "itkCommand.h"
class CommandIterationUpdate : public itk::Command 
{
public:
	typedef  CommandIterationUpdate   Self;
	typedef  itk::Command             Superclass;
	typedef  itk::SmartPointer<Self>  Pointer;
	itkNewMacro( Self );

	typedef itk::LBFGSBOptimizer     OptimizerType;
	typedef itk::SingleValuedNonLinearOptimizer GenericOptimizerType;
	typedef   const OptimizerType*    OptimizerPointer;
	typedef   const GenericOptimizerType* GenericOptimizerPointer;

	double GetParametersBound(){
		return m_ParametersBound;
	}

	void SetParametersBound(double bound){
		m_ParametersBound = bound;
	}

	void SetFirstStepStatus(bool firstStepStatus){
		m_firstStep = firstStepStatus;
	}

	void SetOldParameters(OptimizerType::ParametersType& oldParameters){
		m_OldParameters.SetSize(oldParameters.Size());
		for (unsigned int index = 0; index<oldParameters.Size(); index++)
		{
			m_OldParameters[index] = oldParameters[index];
		}
	}

	OptimizerType::ParametersType& GetUpdateParameters() {
		return m_UpdateParameters;
	}


protected:
	std::string m_exceptionMessage;
	double m_ParametersBound;
	unsigned int m_FailingParamIndex;
	OptimizerType::ParametersType m_LastValidParameters;
	OptimizerType::ParametersType m_OldParameters;
	OptimizerType::ParametersType m_UpdateParameters;

	bool m_firstStep;
	bool m_OnMode;

	CommandIterationUpdate() {
		m_OnMode = true;
		m_ParametersBound = 0.;
		m_exceptionMessage = "Bound on parameters value has been reached.";
	}

public:


	std::string GetExceptionMessage(){
		return m_exceptionMessage;
	}

	const OptimizerType::ParametersType& GetLastValidParameters() {
		return m_LastValidParameters;
	}

	unsigned int GetFailingParamIndex(){
		return m_FailingParamIndex;
	}

	void SetOnMode(bool mode)
	{ 
		m_OnMode = mode;
	}

	bool GetOnMode()
	{
		return m_OnMode;
	}


	void Execute(itk::Object *caller, const itk::EventObject & event)
	{
		Execute( (const itk::Object *)caller, event);
	}

	void Execute(const itk::Object * object, const itk::EventObject & event)
	{
		OptimizerPointer optimizer = dynamic_cast< OptimizerPointer >( object );

		if (optimizer !=0)
		{

			if( !(itk::IterationEvent().CheckEvent( &event )) )
			{
				return;
			}

			std::cout << optimizer->GetCurrentIteration() << "   ";
			std::cout << optimizer->GetCachedValue() << "   ";
			std::cout << std::endl; 

			if (m_OnMode)
			{
				const OptimizerType::ParametersType& params = optimizer->GetCachedCurrentPosition();
				m_LastValidParameters.SetSize(params.Size());
				m_LastValidParameters.SetSize(params.Size());
				for (unsigned int index = 0; index<params.Size(); index++)				
          {				
          m_LastValidParameters[index] = params[index];				
          }
			}

			return;
		}
		else
		{
			GenericOptimizerPointer optimizerGen = dynamic_cast< GenericOptimizerPointer >( object );
			if (optimizerGen !=0)
			{
				std::cout << optimizerGen->GetCurrentPosition() << std::endl;
			}

			return;
		}

	}
};


namespace {

	template<class T> int DoIt( int argc, char * argv[], T )
	{
		PARSE_ARGS;

		const    unsigned int    ImageDimension = 3;

		typedef  T              PixelType;
		typedef  PixelType      FixedPixelType;
		typedef  PixelType		MovingPixelType;
		typedef itk::Image< FixedPixelType, ImageDimension >  FixedImageType;
		typedef itk::Image< MovingPixelType, ImageDimension >  MovingImageType;

		typename FixedImageType::RegionType fixedRegion;
		typename FixedImageType::IndexType arg_index;
		typename FixedImageType::SizeType  arg_size;


		double spacingOfControlPoints[ImageDimension];
		spacingOfControlPoints[0] = spac_CP_x;
		spacingOfControlPoints[1] = spac_CP_y;
		if (ImageDimension == 3)
		{
			spacingOfControlPoints[2] = spac_CP_z;
		}

		bool userSpecifiedImageRegion = false;
		for ( int i = 0 ; i < region.size( ) ; i++ )
		{
			userSpecifiedImageRegion = userSpecifiedImageRegion || region[ i ] != 0;
		}


		//////////////////////////////////////////////////////////////////////////

		const unsigned int SpaceDimension = ImageDimension;
		const unsigned int SplineOrder = 3;

		typedef double CoordinateRepType;
		typedef itk::BSplineDeformableTransform< CoordinateRepType, SpaceDimension, SplineOrder >     TransformType;
		typedef itk::LBFGSBOptimizer       OptimizerType;
		typedef itk::SingleValuedNonLinearOptimizer GenericOptimizerType;
		typedef itk::RegularStepGradientDescentOptimizer OptimizerGdType;

		typedef itk::MattesMutualInformationImageToImageMetric< FixedImageType, MovingImageType >    MetricType;
		typedef itk:: LinearInterpolateImageFunction< MovingImageType, double >    InterpolatorType;
		typedef itk::ImageRegistrationMethod< FixedImageType, MovingImageType >    RegistrationType;

		typename MetricType::Pointer       metric        = MetricType::New();
		GenericOptimizerType*              optimizer     = 0; 
		OptimizerType::Pointer             optimizer_lb  = OptimizerType::New();
		OptimizerGdType::Pointer           optimizer_gd  = OptimizerGdType::New();
		typename InterpolatorType::Pointer interpolator  = InterpolatorType::New();
		typename RegistrationType::Pointer registration  = RegistrationType::New();


		registration->SetMetric(        metric        );
		registration->SetInterpolator(  interpolator  );

		//////////////////////////////////////////////////////////////////////////

		typedef itk::ImageFileReader< FixedImageType  > FixedImageReaderType;
		typedef itk::ImageFileReader< MovingImageType > MovingImageReaderType;

		typename FixedImageReaderType::Pointer  fixedImageReader  = FixedImageReaderType::New();
		typename MovingImageReaderType::Pointer movingImageReader = MovingImageReaderType::New();

		fixedImageReader->SetFileName(  fixedImageFileName );
		movingImageReader->SetFileName( movingImageFileName );

		try
		{
			fixedImageReader->Update();
			movingImageReader->Update();
		}
		catch( itk::ExceptionObject & excp )
		{
			std::cerr << "Error while reading input image files" << std::endl;
			std::cerr << excp << std::endl;
			std::cerr << "[FAILED]" << std::endl;
			return EXIT_FAILURE;
		}

		typename FixedImageType::ConstPointer fixedImage = fixedImageReader->GetOutput();
		typename MovingImageType::ConstPointer movingImage = movingImageReader->GetOutput();

		typename FixedImageType::IndexType index;
		typename FixedImageType::SizeType size;

		if (!userSpecifiedImageRegion)
		{
			std::cout << " Warning: Unspecified userSpecifiedRegion - Using the whole imageRegion as userSpecifiedRegion" << std::endl;
			index = fixedImage->GetLargestPossibleRegion().GetIndex();
			size  = fixedImage->GetLargestPossibleRegion().GetSize();
		}
		else
		{
			for (unsigned int r=0; r<ImageDimension; r++)
			{
				index[r] = arg_index[r];
				size[r]  = arg_size[r];
			}
		}

		fixedRegion.SetSize(size);
		fixedRegion.SetIndex(index);

		std::cout << "index " << index[0] << " " << index[1] << " " << index[2] << " " << std::endl;
		std::cout << "size  " << size[0] << " " << size[1] << " " << size[2] << " " << std::endl;

		//////////////////////////////////////////////////////////////////////////

		if ( !outputDefoFieldFileName.empty( ) )
		{
			// Write DefoField filled with zeros before any computations (Atlas precaution)

			typedef itk::Vector<FixedPixelType, ImageDimension> VectorFieldPixelType;
			typedef itk::Image<VectorFieldPixelType, ImageDimension> VectorFieldImageType;

			typename VectorFieldImageType::Pointer defoFieldImageVoid = VectorFieldImageType::New();
			defoFieldImageVoid->SetRegions(fixedImage->GetBufferedRegion());
			defoFieldImageVoid->SetSpacing(fixedImage->GetSpacing());
			defoFieldImageVoid->SetOrigin(fixedImage->GetOrigin());
			defoFieldImageVoid->SetDirection(fixedImage->GetDirection());
			defoFieldImageVoid->Allocate();
			defoFieldImageVoid->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());

			typedef itk::ImageFileWriter<VectorFieldImageType> VectorFieldImageWriterType;
			typename VectorFieldImageWriterType::Pointer writer_defoField = VectorFieldImageWriterType::New();
			writer_defoField->SetInput(defoFieldImageVoid);
			writer_defoField->SetFileName(outputDefoFieldFileName);

			try
			{
				writer_defoField->Update();
			}
			catch ( itk::ExceptionObject & err )
			{
				std::cerr << "ExceptionObject caught !" << std::endl;
				std::cerr << err << std::endl;
				return EXIT_FAILURE;
			}
		}

		//////////////////////////////////////////////////////////////////////////

		// Saving images to BMP (2D images only)
		if ((saveToBmp) && (ImageDimension == 2))
		{
			int save_fixed  = SaveImageToBMP<FixedImageType>(fixedImage.GetPointer() , fixedImageFileName.c_str( ) );
			int save_moving = SaveImageToBMP<MovingImageType>(movingImage.GetPointer(), movingImageFileName.c_str( ) );
		}

		//////////////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////////////


		//read and set mask
		typedef itk::ImageMaskSpatialObject<SpaceDimension> MaskSpatialObjectType;
		typedef MaskSpatialObjectType::ImageType   ImageMaskType;
		typedef itk::ImageFileReader< ImageMaskType >    MaskReaderType;

		if ( !maskMetricFileName.empty( ) )
		{
			std::cout << "Opening mask for the metric..." << std::endl;
			MaskSpatialObjectType::Pointer  spatialObjectMaskMetric = MaskSpatialObjectType::New();
			MaskReaderType::Pointer  maskMetricReader = MaskReaderType::New();
			maskMetricReader->SetFileName( maskMetricFileName );
			try
			{
				maskMetricReader->Update();
			}
			catch ( itk::ExceptionObject & err )
			{
				std::cerr << "ExceptionObject caught !" << std::endl;
				std::cerr << err << std::endl;
				return EXIT_FAILURE;
			}

			ImageMaskType::Pointer maskImage_tmp = maskMetricReader->GetOutput();
			maskImage_tmp->SetSpacing(fixedImage->GetSpacing());
			maskImage_tmp->SetOrigin(fixedImage->GetOrigin());

			spatialObjectMaskMetric->SetImage( maskImage_tmp.GetPointer() );
			spatialObjectMaskMetric->Initialize();
			metric->SetFixedImageMask( spatialObjectMaskMetric );
		}

		//////////////////////////////////////////////////////////////////////////

		// Generate fixed image mask from thresh value if required
		typedef itk::ImageMaskSpatialObject<ImageDimension> FixedImageMaskType;
		typedef itk::ImageMaskSpatialObject<ImageDimension> MovingImageMaskType;
		typedef itk::BinaryThresholdImageFilter<FixedImageType,FixedImageMaskType::ImageType> FixedThresholdImageFilterType;
		typedef itk::BinaryThresholdImageFilter<MovingImageType,MovingImageMaskType::ImageType> MovingThresholdImageFilterType;

		if ( lowThreshSpecifiedFixed || highThreshSpecifiedFixed)
		{
			typename FixedThresholdImageFilterType::Pointer fixedThresholdImageFilter = FixedThresholdImageFilterType::New();

			FixedImageMaskType::Pointer mask = FixedImageMaskType::New();

			fixedThresholdImageFilter->SetInput(fixedImage);

			if ( lowThreshSpecifiedFixed && highThreshSpecifiedFixed)
			{
				fixedThresholdImageFilter->SetLowerThreshold(lowThreshFixed);
				fixedThresholdImageFilter->SetUpperThreshold(highThreshFixed);
			}
			else if ( lowThreshSpecifiedFixed )
			{
				fixedThresholdImageFilter->SetLowerThreshold(lowThreshFixed);
				fixedThresholdImageFilter->SetUpperThreshold(itk::NumericTraits<FixedPixelType>::max());
			}
			else if ( highThreshSpecifiedFixed )
			{
				fixedThresholdImageFilter->SetUpperThreshold(highThreshFixed);
				fixedThresholdImageFilter->SetLowerThreshold(itk::NumericTraits<FixedPixelType>::min());
			}
			fixedThresholdImageFilter->Update();

			mask->SetImage(fixedThresholdImageFilter->GetOutput());
			metric->SetFixedImageMask(mask);
		}

		// Generate moving image mask from thresh value if required
		if ( lowThreshSpecifiedMoving || highThreshSpecifiedMoving )
		{
			typename MovingThresholdImageFilterType::Pointer movingThresholdImageFilter = MovingThresholdImageFilterType::New();

			MovingImageMaskType::Pointer mask = MovingImageMaskType::New();

			movingThresholdImageFilter->SetInput(movingImage);

			if ( lowThreshSpecifiedMoving && highThreshSpecifiedMoving)
			{
				movingThresholdImageFilter->SetLowerThreshold(lowThreshMoving);
				movingThresholdImageFilter->SetUpperThreshold(highThreshMoving);
			}
			else if ( lowThreshSpecifiedMoving )
			{
				movingThresholdImageFilter->SetLowerThreshold(lowThreshMoving);
				movingThresholdImageFilter->SetUpperThreshold(itk::NumericTraits<MovingPixelType>::max());
			}
			else if ( highThreshSpecifiedMoving )
			{
				movingThresholdImageFilter->SetUpperThreshold(highThreshMoving);
				movingThresholdImageFilter->SetLowerThreshold(itk::NumericTraits<MovingPixelType>::min());
			}
			movingThresholdImageFilter->Update();

			mask->SetImage(movingThresholdImageFilter->GetOutput());
			metric->SetMovingImageMask(mask);
		}

		//////////////////////////////////////////////////////////////////////////


		// Smoothing images

		if (sigma_smooth != 0)
		{
			std::cout << "Smoothing images...   " << "sigma = " << sigma_smooth << std::endl;
			typename FixedImageType::Pointer smoothImage_fixed    = SmoothImage<FixedImageType>   (fixedImage.GetPointer(), sigma_smooth, false);
			typename MovingImageType::Pointer smoothImage_moving  = SmoothImage<MovingImageType>  (movingImage.GetPointer(), sigma_smooth, false);

			//registration->SetFixedImage(  smoothImage_fixed   );
			registration->SetFixedImage(  fixedImage   );
			registration->SetMovingImage(  smoothImage_moving  );
		}
		else
		{
			registration->SetFixedImage(  fixedImage   );
			registration->SetMovingImage(  movingImage  );
		}

		//////////////////////////////////////////////////////////////////////////


		// Definition of transformation	
		TransformType::Pointer  transform = TransformType::New();

		//////////////////////////////////////////////////////////////////////////

		// If a transform file name has been specified, read it
		if ( !inputTransformFileName.empty( ) )
		{
			std::cout << "Reading Input Transform..." << std::endl;
			TransformType::Pointer transform_temp = 0;
			transform_temp = ReadTransformFromFile<TransformType>(inputTransformFileName.c_str( ), "BSplineDeformableTransform");

			if (refineTransformation)
			{
				std::cout << "Using refined scale..." << std::endl;

				// grid spacing at high resolution
				TransformType::SpacingType spacingLow = transform_temp->GetGridSpacing();
				TransformType::SpacingType spacingOfControlPointsHigh;
				for ( unsigned int r=0; r<ImageDimension; r++)
				{
					spacingOfControlPointsHigh[r] = spacingLow[r] / 2.;
				}

				// Upsampling grid
				TransformType::RegionType bsplineRegionHigh;
				TransformType::OriginType gridOriginHigh;
				computeBSplineGrid<FixedImageType,TransformType>(fixedImage, fixedRegion, spacingOfControlPointsHigh, 
					gridOriginHigh, bsplineRegionHigh);

				transform = RefineFFDTransformation<TransformType,FixedImageType>(transform_temp.GetPointer(), SplineOrder,
					bsplineRegionHigh, gridOriginHigh, spacingOfControlPointsHigh);
			}
			else
			{
				transform = transform_temp;
			}

		}
		else
		{ 

			// If no transform file name is specified, we need to create the transform from scratch
			//
			// Default value if not specified by the user
			for(unsigned int r=0; r<ImageDimension; ++r)
			{
				if (spacingOfControlPoints[r] == 0)
				{
					spacingOfControlPoints[r] = fixedImage->GetSpacing()[r] * (static_cast<double>(fixedRegion.GetSize()[r]) - 1.) / (4.);
				}
			}

			TransformType::RegionType bsplineRegion;
			TransformType::OriginType gridOrigin;
			computeBSplineGrid<FixedImageType,TransformType>(fixedImage, fixedRegion, spacingOfControlPoints, gridOrigin, bsplineRegion);

			transform->SetGridSpacing( spacingOfControlPoints );	transform->SetGridOrigin( gridOrigin );	transform->SetGridRegion( bsplineRegion );
			typedef TransformType::ParametersType     ParametersType;
			const unsigned int numberOfParameters = transform->GetNumberOfParameters();
			ParametersType parameters( numberOfParameters );
			parameters.Fill( 0.0 );

			transform->SetParametersByValue( parameters );

			//////////////////////////////////////////////////////////////////////////

			// Writing output transformation filled with zeros before any computation (Atlas precaution)

			std::cout << std::endl << "Writing Transformation beforehand..." << std::endl;
			typedef itk::TransformFileWriter TransformWriterB;
			TransformWriterB::Pointer writer_transformB = TransformWriterB::New();
			writer_transformB->SetInput(transform);
			writer_transformB->SetFileName( outputTransform );
			try
			{
				writer_transformB->Update();
			}
			catch( itk::ExceptionObject & err ) 
			{ 
				std::cerr << "ExceptionObject caught !" << std::endl; 
				std::cerr << err << std::endl; 
				return -1;
			} 

		}

		//////////////////////////////////////////////////////////////////////////

		// If a bulk transform was specified, read it  

		typedef itk::TransformChain<double, itk::GetImageDimension<MovingImageType>::ImageDimension> TransformChainType;
		typename TransformChainType::Pointer chainTransform = TransformChainType::New();

		for (unsigned int tr=0; tr<bulkTransformFileName.size( ); tr++)
		{
			itk::TransformFileReader::Pointer tr_reader;
			tr_reader = itk::TransformFileReader::New();
			tr_reader->SetFileName( bulkTransformFileName[tr] );
			try
			{
				tr_reader->Update();
			}
			catch( itk::ExceptionObject & excp )
			{
				std::cerr << "Error while reading the transform file" << std::endl;
				std::cerr << excp << std::endl;
			}

			std::cout<<"after transform reading"<<std::endl;

			typedef itk::TransformFileReader::TransformListType * TransformListType;
			TransformListType transforms = tr_reader->GetTransformList();
			std::cout << "Number of transforms = " << transforms->size() << std::endl;
			itk::TransformFileReader::TransformListType::const_iterator it = transforms->begin();

			while (it != transforms->end()) 
			{ 
				TransformType::Pointer transform = static_cast<TransformType*>((*it).GetPointer());
				chainTransform->PushBackTransformation(transform);
				++it; 
			} 
		}

		transform->SetBulkTransform( chainTransform );

		// Configure registration and transformation objects
		registration->SetTransform( transform );
		std::cout << "Transformation :" << std::endl;
		transform->Print(std::cout);

		registration->SetFixedImageRegion( fixedRegion );
		registration->SetInitialTransformParameters( transform->GetParameters() );

		if (optimizerOption == "LBFGSB")
		{
			optimizer = optimizer_lb.GetPointer();


			OptimizerType::BoundSelectionType boundSelect( transform->GetNumberOfParameters() );
			OptimizerType::BoundValueType upperBound( transform->GetNumberOfParameters() );
			OptimizerType::BoundValueType lowerBound( transform->GetNumberOfParameters() );



			if(maxMovementOfCP != 0)
			{
				double minGridSpacing = std::min(spacingOfControlPoints[0],spacingOfControlPoints[1]);
				if (ImageDimension == 3)
				{
					minGridSpacing = std::min(minGridSpacing,spacingOfControlPoints[2]);
				}

				double parametersBound = minGridSpacing * maxMovementOfCP;

				boundSelect.Fill( 2 );
				upperBound.Fill( parametersBound );
				lowerBound.Fill( -parametersBound );

				optimizer_lb->SetBoundSelection( boundSelect );
				optimizer_lb->SetUpperBound( upperBound );
				optimizer_lb->SetLowerBound( lowerBound );
			}
			else
			{
				boundSelect.Fill( 0 );
				upperBound.Fill( 0.0 );
				lowerBound.Fill( 0.0 );

				optimizer_lb->SetBoundSelection( boundSelect );
				optimizer_lb->SetUpperBound( upperBound );
				optimizer_lb->SetLowerBound( lowerBound );
			}

			optimizer_lb->SetCostFunctionConvergenceFactor( 1e+7 );
			optimizer_lb->SetProjectedGradientTolerance( 1e-5 );
			optimizer_lb->SetMaximumNumberOfIterations( 500 );		// 500
			optimizer_lb->SetMaximumNumberOfEvaluations( 100 );		// 500
			optimizer_lb->SetMaximumNumberOfCorrections( 12 );

			//optimizer_lb->SetCostFunctionConvergenceFactor( 1.e7 );
			//optimizer_lb->SetProjectedGradientTolerance( 1e-6 );
			//optimizer_lb->SetMaximumNumberOfIterations( 200 );
			//optimizer_lb->SetMaximumNumberOfEvaluations( 30 );
			//optimizer_lb->SetMaximumNumberOfCorrections( 5 );

		}
		else
		{
			std::cout<<"setting optimizer to gradient descent"<<std::endl;

			optimizer = optimizer_gd.GetPointer();

			optimizer_gd->SetNumberOfIterations(500);
			optimizer_gd->SetGradientMagnitudeTolerance(1e-8);

		}

		// Pass the optimizer to the registration filter, now that it has been 
		// configured properly
		registration->SetOptimizer(     optimizer     );

		// Create the Command observer and register it with the optimizer.
		//
		if(addObserver){
			CommandIterationUpdate::Pointer observer = CommandIterationUpdate::New();
			optimizer->AddObserver( itk::IterationEvent(), observer );
		}

		metric->SetNumberOfHistogramBins( numberOfHBins );

		const unsigned int numberOfSamples = fixedRegion.GetNumberOfPixels() / frac_Samples;

		metric->SetNumberOfSpatialSamples( numberOfSamples );
		//metric->ReinitializeSeed( 76926294 );

		// Add a time probe
		itk::TimeProbesCollectorBase collector;

		std::cout << std::endl << "Starting Registration..." << std::endl;

		try
		{
			collector.Start( "Registration" );
			registration->StartRegistration();
			collector.Stop( "Registration" );
		}
		catch( itk::ExceptionObject & err )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << err << std::endl;
			return -1;
		}
		OptimizerType::ParametersType finalParameters = registration->GetLastTransformParameters();


		// Report the time taken by the registration
		collector.Report();

		transform->SetParameters( finalParameters );

		//////////////////////////////////////////////////////////////////////////

		// Writing transformation
		std::cout << std::endl << "Writing Transformation..." << std::endl;

		typedef itk::TransformFileWriter TrWriter;

		TrWriter::Pointer trwr = TrWriter::New();
		trwr->SetInput(transform);
		trwr->SetFileName( outputTransform );
		try
		{
			trwr->Update();
		}
		catch( itk::ExceptionObject & err )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << err << std::endl;
			return -1;
		}

		//////////////////////////////////////////////////////////////////////////

		// Writing output image

		typedef itk::ResampleImageFilter< MovingImageType, FixedImageType >    ResampleFilterType;

		typename ResampleFilterType::Pointer resample = ResampleFilterType::New();

		resample->SetTransform( transform );
		resample->SetInput( movingImage );

		resample->SetSize(    fixedImage->GetLargestPossibleRegion().GetSize() );
		resample->SetOutputOrigin(  fixedImage->GetOrigin() );
		resample->SetOutputSpacing( fixedImage->GetSpacing() );
		resample->SetDefaultPixelValue( paddingValue );

		if ( !outputImageFileName.empty( ) )
		{
			std::cout << std::endl << "Writing output Image..." << std::endl;

			typedef itk::ImageFileWriter< FixedImageType >  WriterType;
			typename WriterType::Pointer writer =  WriterType::New();
			writer->SetFileName( outputImageFileName );
			writer->SetInput( resample->GetOutput()   );
			try
			{
				writer->Update();
			}
			catch( itk::ExceptionObject & err )
			{
				std::cerr << "ExceptionObject caught !" << std::endl;
				std::cerr << err << std::endl;
				return -1;
			}

			if ((saveToBmp) && (ImageDimension == 2))
			{
				int save_matched  = SaveImageToBMP<MovingImageType>(resample->GetOutput() , outputImageFileName.c_str( ));
			}
		}



		//////////////////////////////////////////////////////////////////////////

		if ( !outputDefoFieldFileName.empty() )
		{
			// Writing output Deformation Field

			float ext_size[3] = {0,0,0};
			std::cout << std::endl << "Writing output Deformation Field..." << std::endl;
			if (outputDefoFieldMovingDefinitionSpecified)
			{
				WriteVectorFieldFromTransform <TransformType,FixedImageType> (transform.GetPointer(), movingImage.GetPointer(), outputDefoFieldFileName.c_str( ),ext_size);
			} 
			else
			{
				WriteVectorFieldFromTransform <TransformType,FixedImageType> (transform.GetPointer(), fixedImage.GetPointer(), outputDefoFieldFileName.c_str( ),ext_size);
			}
		}

		//////////////////////////////////////////////////////////////////////////

		return 0;

	}

} // end of anonymous namespace


int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	itk::ImageIOBase::IOPixelType pixelType;
	itk::ImageIOBase::IOComponentType componentType;
	try
	{
		itk::GetImageType (fixedImageFileName, pixelType, componentType);

		// This filter handles all types on input, but only produces
		// signed types
		switch (componentType)
		{
		case itk::ImageIOBase::UCHAR:
			return DoIt( argc, argv, static_cast<unsigned char>(0));
			break;
		case itk::ImageIOBase::CHAR:
			return DoIt( argc, argv, static_cast<char>(0));
			break;
		case itk::ImageIOBase::USHORT:
			return DoIt( argc, argv, static_cast<unsigned short>(0));
			break;
		case itk::ImageIOBase::SHORT:
			return DoIt( argc, argv, static_cast<short>(0));
			break;
		case itk::ImageIOBase::UINT:
			return DoIt( argc, argv, static_cast<unsigned int>(0));
			break;
		case itk::ImageIOBase::INT:
			return DoIt( argc, argv, static_cast<int>(0));
			break;
		case itk::ImageIOBase::ULONG:
			return DoIt( argc, argv, static_cast<unsigned long>(0));
			break;
		case itk::ImageIOBase::LONG:
			return DoIt( argc, argv, static_cast<long>(0));
			break;
		case itk::ImageIOBase::FLOAT:
			return DoIt( argc, argv, static_cast<float>(0));
			break;
		case itk::ImageIOBase::DOUBLE:
			return DoIt( argc, argv, static_cast<double>(0));
			break;
		case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
		default:
			std::cout << "unknown component type" << std::endl;
			break;
		}
	}

	catch( itk::ExceptionObject &excep)
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		std::cerr << excep << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

