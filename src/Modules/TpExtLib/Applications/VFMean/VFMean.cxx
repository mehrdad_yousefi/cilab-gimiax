/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkPluginUtilities.h"
#include "VFMeanCLP.h"

#include "itkImage.h"
#include <iostream>
#include <fstream>
#include <sstream>

#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

//  The following section of code implements a Command observer
//  used to monitor the evolution of the registration process.
//
#include "itkCommand.h"
#include "regIO.h"
#include "regDataOperations.h"
#include "regImageCommon.h"
#include "regVectorCommon.h"



namespace {

	template<class T> int DoIt( int argc, char * argv[], T )
	{
		PARSE_ARGS;

		typedef T PixelType;

		const    unsigned int    ImageDimension = 3; 
		typedef itk::Vector<PixelType, ImageDimension> VectorFieldPixelType;
		typedef itk::Image<PixelType, ImageDimension> ImageType;
		typedef itk::Image<VectorFieldPixelType, ImageDimension> VectorFieldImageType;

		//////////////////////////////////////////////////////////////////////////
		// Opening VFList file

		for (unsigned int s=0;s<inputVFList_FileName.size( );s++)
		{
			std::cout << inputVFList_FileName.at(s) << std::endl;
		}

		//////////////////////////////////////////////////////////////////////////


		// Initializing mean VF

		typename VectorFieldImageType::Pointer V_mean = VectorFieldImageType::New();
		typename ImageType::Pointer V_mean_mask = ImageType::New();

		typedef itk::ImageFileWriter<VectorFieldImageType> VectorFieldImageWriterType;
		typedef itk::ImageFileWriter<ImageType> ImageWriterType;
		typedef itk::ImageFileReader< VectorFieldImageType  > VectorFieldReaderType;

		for (unsigned int s=0;s<inputVFList_FileName.size( );s++)
		{
			std::cout << std::endl << "Reading... " << inputVFList_FileName.at(s) << std::endl;
			typename VectorFieldImageType::Pointer VFImage = VectorFieldImageType::New();

			typename VectorFieldReaderType::Pointer  inputVectorFieldImageReader  = VectorFieldReaderType::New();
			inputVectorFieldImageReader->SetFileName( inputVFList_FileName.at(s) );
			try
			{
				inputVectorFieldImageReader->Update();
			}
			catch( itk::ExceptionObject & excp )
			{
				std::cerr << "ExceptionObject caught !" << std::endl;
				std::cerr << excp << std::endl;
				std::cerr << "[FAILED]" << std::endl;
				return EXIT_FAILURE;
			}
			VFImage = inputVectorFieldImageReader->GetOutput();
			std::cout << "   DONE"<< std::endl;

			if (s == 0)
			{
				// Initialize mean image
				typedef itk::ImageRegionIterator<VectorFieldImageType> VectorFieldIteratorType;
				VectorFieldIteratorType it_V_mean(V_mean,V_mean->GetBufferedRegion());

				V_mean->SetRegions(VFImage->GetBufferedRegion());
				V_mean->SetSpacing(VFImage->GetSpacing());
				V_mean->SetOrigin(VFImage->GetOrigin());
				V_mean->SetDirection(VFImage->GetDirection());
				V_mean->Allocate();
				V_mean->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());
			
				V_mean_mask->SetRegions(V_mean->GetBufferedRegion());
				V_mean_mask->SetSpacing(V_mean->GetSpacing());
				V_mean_mask->SetOrigin(V_mean->GetOrigin());
				V_mean_mask->SetDirection(V_mean->GetDirection());
				V_mean_mask->Allocate();
				V_mean_mask->FillBuffer(1);
			}	// if (s == 0)

			//////////////////////////////////////////////////////////////////////////

	// 		if (useLogSpecified)
	// 		{
	// 			std::string energyValue_log;
	// 			VectorFieldImageType::Pointer recomposedVF_void = VectorFieldImageType::New();
	// 
	// 			// Compute log of VF before any average computation
	// 			VectorFieldImageType::Pointer logVF = VectorFieldImageType::New();
	// 			logVF->SetRegions(VFImage->GetBufferedRegion());
	// 			logVF->SetSpacing(VFImage->GetSpacing());
	// 			logVF->SetOrigin(VFImage->GetOrigin());
	// 			logVF->SetDirection(VFImage->GetDirection());
	// 			logVF->Allocate();
	// 
	// 			int log_phi_computed = ComputeLogPhi<VectorFieldImageType>( VFImage.GetPointer(),
	// 			logVF.GetPointer(),
	// 			N_it_expand, sig_decrease_expand, V_radius_expand,
	// 			N_composition, delta_sqrt, Nmax_it_sqrt, rho_sqrt, delta_inv, Nmax_it_inv, rho_inv,
	// 			false, energyValue_log );
	// 
	// 			//////////////////////////////////////////////////////////////////////////
	// 
	// 			int V_mean_done = LocalOperationsVectorFields <VectorFieldImageType, ImageType> ( logVF.GetPointer(), V_mean.GetPointer(), V_mean.GetPointer(),
	// 			V_mean_mask.GetPointer(),
	// 			V_mean_mask.GetPointer(),
	// 			V_mean_mask.GetPointer(),
	// 			1 );
	// 			
	// 		}
	// 		else
	// 		{
				std::cout << std::endl << "Updating mean... " << inputVFList_FileName.at(s) << std::endl;
				int V_mean_done = LocalOperationsVectorFields <VectorFieldImageType, ImageType> ( VFImage.GetPointer(), V_mean.GetPointer(), V_mean.GetPointer(),
					V_mean_mask.GetPointer(),
					V_mean_mask.GetPointer(),
					V_mean_mask.GetPointer(),
					1 );
				std::cout << "   DONE"<< std::endl;
	// 		}
		}	// for (unsigned int s=0;s<numVF;s++)

		int V_mean_scal = ScalarMultiplyVectorField <VectorFieldImageType,ImageType> (	V_mean.GetPointer(), V_mean.GetPointer(),	1/((double)inputVFList_FileName.size() ), V_mean_mask.GetPointer(), V_mean_mask.GetPointer() );

		//////////////////////////////////////////////////////////////////////////

	// 	if (useLogSpecified)
	// 	{
	// 		// Compute exp of mean(logs)
	// 
	// 		VectorFieldImageType::Pointer exp_mean_logVF = VectorFieldImageType::New();
	// 		exp_mean_logVF->SetRegions(V_mean->GetBufferedRegion());
	// 		exp_mean_logVF->SetSpacing(V_mean->GetSpacing());
	// 		exp_mean_logVF->SetOrigin(V_mean->GetOrigin());
	// 		exp_mean_logVF->SetDirection(V_mean->GetDirection());
	// 		exp_mean_logVF->Allocate();
	// 		int expPhi_ok = ComputeExpPhi <VectorFieldImageType> (	V_mean, exp_mean_logVF,
	// 			N_it_expand, sig_decrease_expand, V_radius_expand, N_composition );
	// 
	// 		CopyImage <VectorFieldImageType> ( exp_mean_logVF.GetPointer(), V_mean.GetPointer() );
	// 	}


		typename VectorFieldImageWriterType::Pointer writerVFmean = VectorFieldImageWriterType::New();
		writerVFmean->SetInput(V_mean.GetPointer());
		writerVFmean->SetFileName(outputVF_FileName);
		try
		{
			writerVFmean->Update();
		}
		catch ( itk::ExceptionObject & err )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << err << std::endl;
			return EXIT_FAILURE;
		}



		return EXIT_SUCCESS;

	}


} // end of anonymous namespace


int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	itk::ImageIOBase::IOPixelType pixelType;
	itk::ImageIOBase::IOComponentType componentType;
	try
	{
		if ( inputVFList_FileName.empty( ) )
		{
			return EXIT_FAILURE;
		}

		itk::GetImageType (*inputVFList_FileName.begin( ), pixelType, componentType);

		// This filter handles all types on input, but only produces
		// signed types
		switch (componentType)
		{
		case itk::ImageIOBase::UCHAR:
			return DoIt( argc, argv, static_cast<unsigned char>(0));
			break;
		case itk::ImageIOBase::CHAR:
			return DoIt( argc, argv, static_cast<char>(0));
			break;
		case itk::ImageIOBase::USHORT:
			return DoIt( argc, argv, static_cast<unsigned short>(0));
			break;
		case itk::ImageIOBase::SHORT:
			return DoIt( argc, argv, static_cast<short>(0));
			break;
		case itk::ImageIOBase::UINT:
			return DoIt( argc, argv, static_cast<unsigned int>(0));
			break;
		case itk::ImageIOBase::INT:
			return DoIt( argc, argv, static_cast<int>(0));
			break;
		case itk::ImageIOBase::ULONG:
			return DoIt( argc, argv, static_cast<unsigned long>(0));
			break;
		case itk::ImageIOBase::LONG:
			return DoIt( argc, argv, static_cast<long>(0));
			break;
		case itk::ImageIOBase::FLOAT:
			return DoIt( argc, argv, static_cast<float>(0));
			break;
		case itk::ImageIOBase::DOUBLE:
			return DoIt( argc, argv, static_cast<double>(0));
			break;
		case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
		default:
			std::cout << "unknown component type" << std::endl;
			break;
		}
	}

	catch( itk::ExceptionObject &excep)
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		std::cerr << excep << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

