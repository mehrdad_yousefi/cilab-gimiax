/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include "itkPluginUtilities.h"
#include "VFExpCLP.h"

#include <string.h>

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkNumericTraits.h"

#include "regDataOperations.h"
#include "regImageCommon.h"
#include "regVectorCommon.h"




namespace {

	template<class T> int DoIt( int argc, char * argv[], T )
	{
		PARSE_ARGS;

		typedef T PixelType;
		//////////////////////////////////////////////////////////////////////////

		// Reading input vector field

		std::cout << "Reading Input Vector Field..." << std::endl;
		const    unsigned int    ImageDimension = 3; 
		typedef itk::Vector<PixelType, ImageDimension> VectorFieldPixelType;
		typedef itk::Image<PixelType, ImageDimension> ImageType;
		typedef itk::Image<VectorFieldPixelType, ImageDimension> VectorFieldImageType;

		typedef itk::ImageFileReader< VectorFieldImageType  > InputDefoFieldReaderType;
		typename InputDefoFieldReaderType::Pointer  inputVFReader  = InputDefoFieldReaderType::New();
		inputVFReader->SetFileName( inputVFFileName  );
		try
		{
			inputVFReader->Update();
		}
		catch( itk::ExceptionObject & excp )	
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << excp << std::endl;
			std::cerr << "[FAILED]" << std::endl;
			return EXIT_FAILURE;
		}
		typename VectorFieldImageType::Pointer inputVF = inputVFReader->GetOutput();
		std::cout << "Reading Input Vector Field...   DONE" << std::endl;

	// 	VectorFieldImageType::Pointer SinputVF = VectorFieldImageType::New();
	// 	SinputVF->SetRegions(inputVF->GetBufferedRegion());
	// 	SinputVF->SetSpacing(inputVF->GetSpacing());
	// 	SinputVF->SetOrigin(inputVF->GetOrigin());
	// 	SinputVF->SetDirection(inputVF->GetDirection());
	// 	SinputVF->Allocate();
	// 	ImageType::Pointer IOnes_mask = ImageType::New();
	// 	IOnes_mask->SetRegions(inputVF->GetBufferedRegion());
	// 	IOnes_mask->SetSpacing(inputVF->GetSpacing());
	// 	IOnes_mask->SetOrigin(inputVF->GetOrigin());
	// 	IOnes_mask->SetDirection(inputVF->GetDirection());
	// 	IOnes_mask->Allocate();
	// 	IOnes_mask->FillBuffer(1);
	// 	int T_Image_initdone = ScalarMultiplyVectorField <VectorFieldImageType, ImageType> ( inputVF.GetPointer(), SinputVF.GetPointer(), 0.0625, IOnes_mask.GetPointer(), IOnes_mask.GetPointer() );
	// 
	// 	typedef itk::ImageFileWriter<VectorFieldImageType> VFWriterType;
	// 	VFWriterType::Pointer writer_VF = VFWriterType::New();
	// 	writer_VF->SetInput(SinputVF.GetPointer());
	// 	writer_VF->SetFileName("E:\\bk-up\\tests_Corne\\atlasStuff\\testVF.mhd");
	// 	try{writer_VF->Update();}
	// 	catch ( itk::ExceptionObject & err )	{
	// 		std::cerr << "ExceptionObject caught !" << std::endl;
	// 		std::cerr << err << std::endl;}


		//////////////////////////////////////////////////////////////////////////

		typename VectorFieldImageType::Pointer expVF = VectorFieldImageType::New();

		if (DSRSSpecified)
		{
			typename VectorFieldImageType::Pointer DSinputVF = VectorFieldImageType::New();
			int DownSample_done = DownSampleVectorFieldImage <VectorFieldImageType> (	inputVF.GetPointer(), DSinputVF.GetPointer() );
			//////////////////////////////////////////////////////////////////////////
			unsigned int N_it_expandDS = (unsigned int)((double)N_it_expand/2);
			int V_radius_expandDS;
			if (2*(int)((double)(V_radius_expand)/2) == V_radius_expand){ V_radius_expandDS = V_radius_expand / 2; } 
			else{ V_radius_expandDS = (V_radius_expand+1) / 2; }
			if (V_radius_expandDS < 3)
			{
				V_radius_expandDS = 3;
				// Data extension with C1 constraint only works with neighborhood_radius >=3
			}

			typename VectorFieldImageType::Pointer DSexpVF = VectorFieldImageType::New();
			DSexpVF->SetRegions(DSinputVF->GetBufferedRegion());
			DSexpVF->SetSpacing(DSinputVF->GetSpacing());
			DSexpVF->SetOrigin(DSinputVF->GetOrigin());
			DSexpVF->SetDirection(DSinputVF->GetDirection());
			DSexpVF->Allocate();
			DSexpVF->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());


			// Computing recomposed Vector Field
			int expVF_ok = ComputeExpPhi <VectorFieldImageType> (	DSinputVF.GetPointer(), DSexpVF.GetPointer(), N_it_expandDS, sig_decrease_expand/2, V_radius_expandDS, N_composition );

			typename VectorFieldImageType::Pointer RSexpVF = VectorFieldImageType::New();
			int ReSample_done = ReSampleVectorFieldImage <VectorFieldImageType> (	DSexpVF.GetPointer(), RSexpVF.GetPointer() );

			//////////////////////////////////////////////////////////////////////////
			// Solving RS problems (1 pixel missing or not), cf. CropVF_3D.cxx:
			typename VectorFieldImageType::Pointer ExRSexpVF = VectorFieldImageType::New();
			ExRSexpVF = ExtendVF_exp_decrease <VectorFieldImageType> ( RSexpVF.GetPointer(), 1, sig_decrease_expand, V_radius_expand );
			// Cropping
			int start_crop[ImageDimension];
			int size_cropped_VF[ImageDimension];
			for (unsigned int dim=0;dim<ImageDimension;dim++)
			{
				start_crop[dim] = 1;
				size_cropped_VF[dim] = inputVF->GetBufferedRegion().GetSize()[dim];
			}
			expVF = CropVectorFieldImage <VectorFieldImageType> (	ExRSexpVF.GetPointer(), start_crop, size_cropped_VF );
			//////////////////////////////////////////////////////////////////////////

		}
		else
		{
			expVF->SetRegions(inputVF->GetBufferedRegion());
			expVF->SetSpacing(inputVF->GetSpacing());
			expVF->SetOrigin(inputVF->GetOrigin());
			expVF->SetDirection(inputVF->GetDirection());
			expVF->Allocate();
			expVF->FillBuffer(itk::NumericTraits<VectorFieldPixelType>::ZeroValue());


			int expVF_ok = ComputeExpPhi <VectorFieldImageType> (	inputVF.GetPointer(), expVF.GetPointer(), N_it_expand, sig_decrease_expand, V_radius_expand, N_composition );
		}


		//////////////////////////////////////////////////////////////////////////

		typedef itk::ImageFileWriter<VectorFieldImageType> OutputVFWriterType;
		typename OutputVFWriterType::Pointer writer_expVF = OutputVFWriterType::New();
		writer_expVF->SetInput(expVF);
		writer_expVF->SetFileName(outputExpVFFileName);

		try
		{
			writer_expVF->Update();
		}
		catch ( itk::ExceptionObject & err )
		{
			std::cerr << "ExceptionObject caught !" << std::endl;
			std::cerr << err << std::endl;
			return EXIT_FAILURE;
		}


		return EXIT_SUCCESS;
	}


} // end of anonymous namespace


int main( int argc, char *argv[] )
{
	PARSE_ARGS;

	itk::ImageIOBase::IOPixelType pixelType;
	itk::ImageIOBase::IOComponentType componentType;
	try
	{
		itk::GetImageType (inputVFFileName, pixelType, componentType);

		// This filter handles all types on input, but only produces
		// signed types
		switch (componentType)
		{
		case itk::ImageIOBase::UCHAR:
			return DoIt( argc, argv, static_cast<unsigned char>(0));
			break;
		case itk::ImageIOBase::CHAR:
			return DoIt( argc, argv, static_cast<char>(0));
			break;
		case itk::ImageIOBase::USHORT:
			return DoIt( argc, argv, static_cast<unsigned short>(0));
			break;
		case itk::ImageIOBase::SHORT:
			return DoIt( argc, argv, static_cast<short>(0));
			break;
		case itk::ImageIOBase::UINT:
			return DoIt( argc, argv, static_cast<unsigned int>(0));
			break;
		case itk::ImageIOBase::INT:
			return DoIt( argc, argv, static_cast<int>(0));
			break;
		case itk::ImageIOBase::ULONG:
			return DoIt( argc, argv, static_cast<unsigned long>(0));
			break;
		case itk::ImageIOBase::LONG:
			return DoIt( argc, argv, static_cast<long>(0));
			break;
		case itk::ImageIOBase::FLOAT:
			return DoIt( argc, argv, static_cast<float>(0));
			break;
		case itk::ImageIOBase::DOUBLE:
			return DoIt( argc, argv, static_cast<double>(0));
			break;
		case itk::ImageIOBase::UNKNOWNCOMPONENTTYPE:
		default:
			std::cout << "unknown component type" << std::endl;
			break;
		}
	}

	catch( itk::ExceptionObject &excep)
	{
		std::cerr << argv[0] << ": exception caught !" << std::endl;
		std::cerr << excep << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

