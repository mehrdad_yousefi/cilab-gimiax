/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "wflWorkflow.h"


wflWorkflow::wflWorkflow()
{

}

wflWorkflow::~wflWorkflow()
{

}

std::vector<wflInputPort::Pointer> &wflWorkflow::GetInputPorts()
{
	return m_InputPorts;
}

std::vector<wflOutputPort::Pointer> &wflWorkflow::GetOutputPorts()
{
	return m_OutputPorts;
}
