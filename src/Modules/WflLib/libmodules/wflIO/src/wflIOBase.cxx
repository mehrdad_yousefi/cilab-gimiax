/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "wflIOBase.h"


wflIOBase::wflIOBase() 
{
	m_Version = 0;
}

wflIOBase::~wflIOBase()
{
}

void wflIOBase::SetFilename( const char* filename )
{
	m_Filename = filename;
}

void wflIOBase::SetInput( wflWorkflow::Pointer workflow )
{
	m_Data = workflow;
}

void wflIOBase::Update()
{
	InternalUpdate();
}

wflWorkflow::Pointer wflIOBase::GetOutput()
{
	return m_Data;
}

