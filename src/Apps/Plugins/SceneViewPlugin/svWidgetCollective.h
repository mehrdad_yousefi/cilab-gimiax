/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _svWidgetCollective_H
#define _svWidgetCollective_H

#include "coreFrontEndPlugin.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreWidgetCollective.h"

/**

\ingroup SceneViewPlugin
\author Xavi Planes
\date 12 April 2010
*/

class svWidgetCollective : public Core::WidgetCollective
{
public:
	//!
	coreDeclareSmartPointerClassMacro(
		svWidgetCollective, 
		Core::WidgetCollective );

	//!
	void Init();

private:
	svWidgetCollective( );

private:

};

#endif //_svWidgetCollective_H
