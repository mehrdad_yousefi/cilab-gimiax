/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
       #include <wx/wx.h>
#endif

#include "SceneViewPlugin.h"
#include "coreException.h"
#include "coreReportExceptionMacros.h"
#include "coreProfile.h"
#include "coreKernel.h"
#include "coreWxMitkGraphicalInterface.h"

using namespace Core::Plugins;
using namespace Core::Widgets;

// Declaration of the plugin
coreBeginDefinePluginMacro(SceneViewPlugin)
coreEndDefinePluginMacro()

/**
* Constructor for class SceneViewPlugin
*/
SceneViewPlugin::SceneViewPlugin(void) 
{
	try
	{
		m_WidgetCollective = svWidgetCollective::New();
		m_WidgetCollective->Init( );

	}
	coreCatchExceptionsReportAndNoThrowMacro(SceneViewPlugin::SceneViewPlugin)
}

/**
* Destructor for class SceneViewPlugin
*/
SceneViewPlugin::~SceneViewPlugin(void)
{
}


