/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "DicomPluginWidgetCollective.h"

#include "wxID.h"

#include "coreFrontEndPlugin.h"
#include "corePluginTabFactory.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreWxMitkCoreMainWindow.h"
#include "coreKernel.h"
#include "coreCommandPanel.h"
#include "corePluginTabFactory.h"

#include "DicomWorkingAreaPanelWidget.h"
#include "DicomConnectToPacsDialogWidget.h"
#include "DicomBrowseTagsPanelWidget.h"

DicomPlugin::WidgetCollective::WidgetCollective( ) 
{
	Core::Runtime::Kernel::GetGraphicalInterface()->CreatePluginTab( "Dicom" );

	
	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory( 
		WorkingAreaPanelWidget::Factory::NewBase( ), 
		Core::WindowConfig( ).WorkingArea()
		.Id( wxID_WorkingAreaPanelWidget ).Caption( "DICOM working area" ) );

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory( 
		ConnectToPacsDialogWidget::Factory::NewBase( ), 
		Core::WindowConfig( ).TabPage( "Dicom" ).VerticalLayout( ).Float( )
		.Id( wxID_ConnectToPacsDialogWidget ).Caption( "PACS Connect/Query/Retrieve/Send" ) );

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory( 
		BrowseTagsPanelWidget::Factory::NewBase( ), 
		Core::WindowConfig().TabPage( "Dicom" ).VerticalLayout().Show( )
		.Id( wxID_BrowseTagsPanelWidget ).Caption( "Browse DICOM Tags" ) );

    // disable unwanted functionality
	if ( GetPluginTab( ) != NULL )
	{
		GetPluginTab( )->EnableWindow( wxID_CommandPanel, false);
		GetPluginTab( )->EnableAllToolbars( false );
		GetPluginTab( )->ShowWindow( wxID_IOToolbar );
		GetPluginTab( )->ShowWindow( wxID_WorkingAreaPanelWidget );
	}
}
