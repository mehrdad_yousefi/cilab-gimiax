/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _DicomProcessorCollective_H
#define _DicomProcessorCollective_H

#include "coreSmartPointerMacros.h"
#include "coreObject.h"

namespace DicomPlugin{

/**

\ingroup DicomPlugin
\author XaviPlanes
\date 24 july 2009
*/

class ProcessorCollective : public Core::SmartPointerObject
{
public:
	//!
	coreDeclareSmartPointerClassMacro(DicomPlugin::ProcessorCollective, Core::SmartPointerObject);

private:
	//! The constructor instantiates all the processors and connects them.
	ProcessorCollective();

private:

};

} // namespace DicomPlugin

#endif //_DicomProcessorCollective_H
