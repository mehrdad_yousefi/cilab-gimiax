/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
       #include <wx/wx.h>
#endif

#include "DicomPlugin.h"
#include "coreException.h"
#include "wxID.h"
#include "corePluginTab.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreKernel.h"
#include "corePluginTabFactory.h"

// Declaration of the plugin
coreBeginDefinePluginMacro(DicomPlugin::DicomPlugin)
coreEndDefinePluginMacro()

// Constructor for class DicomPlugin
DicomPlugin::DicomPlugin::DicomPlugin() 
{
	try
	{
		m_ProcessorCollective = ProcessorCollective::New();
		m_WidgetCollective = WidgetCollective::New( );

	}
	coreCatchExceptionsReportAndNoThrowMacro(DicomPlugin::DicomPlugin)
}

// Destructor for class DicomPlugin
DicomPlugin::DicomPlugin::~DicomPlugin(void)
{
}
