/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _DicomMovieToolbar_H
#define _DicomMovieToolbar_H

#include "coreMovieToolbar.h"
#include "coreBaseWindow.h"


namespace DicomPlugin
{

/**
\brief A widget for managing DICOM 3D+t data. 
\ingroup gmWidgets
\author Xavi Planes
\date July 2012
*/
class PLUGIN_EXPORT  MovieToolbar : public Core::Widgets::MovieToolbar{

public:
	//!
	coreDefineBaseWindowFactory( DicomPlugin::MovieToolbar );


    MovieToolbar(
		wxWindow* parent, 
		int id = wxID_MovieToolbar, 
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=0);

	//! Destructor
	~MovieToolbar();

	//! Get time range
	virtual void GetTimeRange( int &min, int &max );
	void SetTimeRange( int min, int max );

	//! Get current time
	virtual int GetTime( );

	//!
	void SetSelectedSliceHolder( Core::DataHolder<int>::Pointer val );

protected:

protected:
	//!
	int m_Min;

	//!
	int m_Max;
};


} // namespace DicomPlugin

#endif // _DicomMovieToolbar_H
