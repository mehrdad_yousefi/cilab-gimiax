/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "DicomMovieToolbar.h"

DicomPlugin::MovieToolbar::MovieToolbar(wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
Core::Widgets::MovieToolbar(parent, id, pos, size, style)
{
	m_Min = 0;
	m_Max = 0;
}

DicomPlugin::MovieToolbar::~MovieToolbar()
{
}


void DicomPlugin::MovieToolbar::GetTimeRange( int &min, int &max )
{
	min = m_Min;
	max = m_Max;
}

void DicomPlugin::MovieToolbar::SetTimeRange( int min, int max )
{
	m_Min = min;
	m_Max = max;

	OnRenderingTreeChanged( );
}

int DicomPlugin::MovieToolbar::GetTime( )
{
	return m_CurrentTimeStep->GetSubject( );
}

void DicomPlugin::MovieToolbar::SetSelectedSliceHolder( Core::DataHolder<int>::Pointer val )
{
	m_CurrentTimeStep = val;
	m_CurrentTimeStep->AddObserver( 
		(Core::Widgets::MovieToolbar*) this, 
		&DicomPlugin::MovieToolbar::OnModifiedTimeStepHolder );
}
