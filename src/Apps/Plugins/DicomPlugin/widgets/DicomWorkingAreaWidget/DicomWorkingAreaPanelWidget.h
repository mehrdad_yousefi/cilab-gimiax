/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef DicomWorkingAreaPanelWidget_H
#define DicomWorkingAreaPanelWidget_H

#include "DicomWorkingAreaPanelWidgetUI.h"
#include "DicomPluginTypes.h"
#include "DicomProcessorCollective.h"

#include "blSignalCollective.h"

#include "CILabNamespaceMacros.h"
#include "coreDataHolder.h"
#include "coreDataEntity.h"
#include "coreProcessingWidget.h"

#include "dcmDataSet.h"
#include "dcmTypes.h"

#include "coreDICOMFileReader.h"

namespace DicomPlugin{

	class DicomMenuEventHandler;
	class DicomWorkingAreaAdvancedDialog;

/** 
A class that defines all the graphical interface and user interaction with working area of Dicom Plugin

\ingroup DicomPlugin
\author Jakub Lyko
\date 07 April 2008
*/
class WorkingAreaPanelWidget : 
	public DicomWorkingAreaPanelWidgetUI,
	public Core::Widgets::ProcessingWidget
{
public:
	coreDefineBaseWindowFactory( DicomPlugin::WorkingAreaPanelWidget )

	WorkingAreaPanelWidget(wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

	//!
	~WorkingAreaPanelWidget( );

	//!
	void SpaceSliderChanged();

	//!
	void TimeSliderChanged();

	//!
	void SetProcessorCollective(ProcessorCollective::Pointer val);

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

	//!
	bool Enable(bool enable = true );

	//!
	void ReadDataSet( const std::string &filename );

	//! Get the currently selected slice path.
	std::string GetSelectedSlicePath() const;  

	//!
	Core::DataHolder<int>::Pointer GetSelectedSliceHolder( );

	//!
	Core::DataEntityHolder::Pointer GetDcmImageDataHolder( );

private:

	//!
	void OnInit( );

	//! add selected tree data to GIMIAS data list
	void AddToDataList(wxCommandEvent& event);

	//! 
	void OnTreeSelChanged(wxTreeEvent& event);

	//! 
	void OnSpaceSliderChanged(wxCommandEvent& event);

	//!
	void OnTimeSliderChanged(wxCommandEvent& event);

	//!
	void OnAdvanced(wxCommandEvent &event);

	//!
	dcmAPI::DataSet::Pointer GetDataSet( );

	//! apply level window that is stored in dicom file
	void LoadDefaultLevelWindow(wxTreeItemId sliceTreeItemId);

	//!
	void SetSpaceSliderPosition(wxTreeItemId sliceTreeItemId);

	//!
	void SetTimeSliderPosition(wxTreeItemId sliceTreeItemId);

	//!
	void RenderDicomSlice(wxTreeItemId sliceTreeItemId);

	//! expand the tree to the default position, and render a slice
	void DoStartStep();

	//!
	void BuildSeriesCollectiveFromSeriesDataset();

	//!
	blSignal::Pointer CreateSignalFromEcgData(
		std::vector< float > ecgSamples,
		double ecgStartTime,
		double ecgIncrementTime,
		int scaleEcgSamplesByValue = 1
		);

	//!
	std::vector<Core::DataEntity::Pointer> BuildDataEntities( 
		wxArrayTreeItemIds treeItemIdsArray,
		bool postProcessData,
		bool forRendering );

	//! Register readers from DICOMReader of Kernel into m_Reader
	void RegisterReaders( );

	//! Called when selected slice number changes
	void OnSelectedSliceModified();

    wxDECLARE_EVENT_TABLE();

private:
	//! Holds the dicom Image that is currently selected.
	Core::DataEntityHolder::Pointer m_dcmImageDataHolder;

	//!
	Core::IO::DICOMFileReader::Pointer m_Reader;

	//! Counter to append to data entity name
	static int m_DataEntityCounter;

	//!
	DicomMenuEventHandler *m_DicomMenuEventHandler;

	//!
	DicomWorkingAreaAdvancedDialog* m_AdvancedDialog;

	//! Selected slice number (-1 if no selection)
	Core::DataHolder<int>::Pointer m_SelectedSliceHolder;
};

} // DicomPlugin

#endif //DicomWorkingAreaPanelWidget_H
