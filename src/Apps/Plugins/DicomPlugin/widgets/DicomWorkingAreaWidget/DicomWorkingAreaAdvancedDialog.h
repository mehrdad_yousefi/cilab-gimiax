/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef DicomWorkingAreaAdvancedDialog_H
#define DicomWorkingAreaAdvancedDialog_H

#include "DicomWorkingAreaAdvancedDialogUI.h"
#include "coreDICOMFileReader.h"

namespace DicomPlugin{

class WorkingAreaPanelWidget;

/** 
Advanced DICOM options

\ingroup DicomPlugin
\author Xavi Planes
\date Sept 2011
*/
class DicomWorkingAreaAdvancedDialog : public DicomWorkingAreaAdvancedDialogUI
{
public:
	DicomWorkingAreaAdvancedDialog(wxWindow* parent, int id, const wxString& title, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=wxDEFAULT_DIALOG_STYLE);

	//!
	~DicomWorkingAreaAdvancedDialog( );

	//!
	Core::IO::DICOMFileReader::Pointer GetDICOMFileReader() const;
	void SetDICOMFileReader(Core::IO::DICOMFileReader::Pointer val);

	//!
	void UpdateData( );

	//!
	void UpdateWidget( );

	//!
	void SetWorkingAreaPanelWidget( WorkingAreaPanelWidget* workingArea );

private:
	//!
	virtual void OnTimeTolerance(wxCommandEvent &event);
	//!
	virtual void OnTaggedMR(wxCommandEvent &event);
	//!
    virtual void OnScanTimeTag(wxCommandEvent &event);
	//!
    virtual void OnManualTimeTag(wxCommandEvent &event);
	//!
    virtual void OnReload(wxCommandEvent &event);
	//!
    virtual void OnModifiedTimeTag(wxCommandEvent &event);
	//!
    virtual void OnSelectedTimeTag(wxCommandEvent &event);
	//!
    virtual void OnRadioTimeTag(wxCommandEvent &event);

private:
	//!
	WorkingAreaPanelWidget* m_WorkingArea;
	//!
	Core::IO::DICOMFileReader::Pointer m_DICOMFileReader;
	//!
	std::vector<dcmAPI::TagId> m_TagsVector;
};

} // DicomPlugin

#endif //DicomWorkingAreaAdvancedDialog_H
