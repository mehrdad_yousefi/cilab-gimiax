/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
	
#include "DicomWorkingAreaAdvancedDialog.h"
#include "DicomWorkingAreaPanelWidget.h"

#include "coreDirectory.h"
#include "blTextUtils.h"

std::vector<dcmAPI::TagId> Classify( const std::string &filename );


DicomPlugin::DicomWorkingAreaAdvancedDialog::DicomWorkingAreaAdvancedDialog 
	(wxWindow* parent, int id, const wxString& title, const wxPoint& pos, const wxSize& size, long style):
	DicomWorkingAreaAdvancedDialogUI(parent, id, title, pos, size, style)
{
	m_WorkingArea = NULL;
	m_rdAutomaticTimeTag->SetValue( true );
}

DicomPlugin::DicomWorkingAreaAdvancedDialog::~DicomWorkingAreaAdvancedDialog()
{
}

Core::IO::DICOMFileReader::Pointer DicomPlugin::DicomWorkingAreaAdvancedDialog::GetDICOMFileReader() const
{
	return m_DICOMFileReader;
}

void DicomPlugin::DicomWorkingAreaAdvancedDialog::SetDICOMFileReader( Core::IO::DICOMFileReader::Pointer val )
{
	m_DICOMFileReader = val;

	UpdateWidget();
}

void DicomPlugin::DicomWorkingAreaAdvancedDialog::UpdateData()
{
	double value;
	m_txtTimeTolerance->GetValue().ToDouble( &value );
	m_DICOMFileReader->SetTimeTolerance( value );

	m_DICOMFileReader->SetExportDICOMSlices( m_chbExportRawData->GetValue() );
	m_DICOMFileReader->SetImportTaggedMR( m_chbTaggedMR->GetValue( ) );
	m_DICOMFileReader->SetReorientData( m_chbReorientData->GetValue() );

	m_DICOMFileReader->SetActiveTimeTag( dcmAPI::TagId( ) );
	if ( m_rdManualTimeTag->GetValue( ) )
	{
		dcmAPI::TagId tag;

		// Parse input string "0000|0000"
		std::list<std::string> data;
		blTextUtils::ParseLine( m_cmbManualTimeTag->GetValue().ToStdString(), '|', data );
		if ( data.size( ) == 2 )
		{
			std::list<std::string>::iterator itWord = data.begin();

			std::stringstream ss;
			ss << std::hex << itWord->c_str( );
			ss >> tag.m_group;
			itWord++;

			std::stringstream ss1;
			ss1 << std::hex << itWord->c_str( );
			ss1 >> tag.m_element;

			tag.m_group;
			tag.m_element;
			m_DICOMFileReader->SetActiveTimeTag( tag );
		}

	}
}

void DicomPlugin::DicomWorkingAreaAdvancedDialog::UpdateWidget()
{
	m_txtTimeTolerance->SetValue( wxString::Format( "%.2f", m_DICOMFileReader->GetTimeTolerance( ) ) );
	m_chbExportRawData->SetValue( m_DICOMFileReader->GetExportDICOMSlices( ) );
	m_chbTaggedMR->SetValue( m_DICOMFileReader->GetImportTaggedMR( ) );
	m_chbReorientData->SetValue( m_DICOMFileReader->GetReorientData( ) );

	m_cmbManualTimeTag->Enable( m_rdManualTimeTag->GetValue( ) );
	m_btnScan->Enable ( m_rdManualTimeTag->GetValue( ) );
}

void DicomPlugin::DicomWorkingAreaAdvancedDialog::OnTimeTolerance( wxCommandEvent &event )
{
	UpdateData();
}

void DicomPlugin::DicomWorkingAreaAdvancedDialog::OnTaggedMR( wxCommandEvent &event )
{
	UpdateData();
}

void DicomPlugin::DicomWorkingAreaAdvancedDialog::OnManualTimeTag(wxCommandEvent &event)
{
	UpdateData( );
}

void DicomPlugin::DicomWorkingAreaAdvancedDialog::OnModifiedTimeTag(wxCommandEvent &event)
{
	UpdateData( );
}

void DicomPlugin::DicomWorkingAreaAdvancedDialog::OnSelectedTimeTag(wxCommandEvent &event)
{
	UpdateData( );
}

void DicomPlugin::DicomWorkingAreaAdvancedDialog::OnRadioTimeTag(wxCommandEvent &event)
{
	UpdateData( );
	UpdateWidget( );
}

void DicomPlugin::DicomWorkingAreaAdvancedDialog::OnScanTimeTag(wxCommandEvent &event)
{
	if ( m_DICOMFileReader->GetFileNames( ).empty( ) )
	{
		return;
	}

	std::string filename = *m_DICOMFileReader->GetFileNames( ).begin( );

	m_TagsVector = Classify( filename );

	m_cmbManualTimeTag->Clear( );
	std::vector<dcmAPI::TagId>::iterator it;
	for ( it = m_TagsVector.begin( ) ; it != m_TagsVector.end( ) ; it++ )
	{
		std::stringstream stream;
		stream 
			<< hex << setw(4) << setfill('0') << it->m_group << "|"
			<< hex << setw(4) << setfill('0') << it->m_element;
		if ( !it->m_description.empty( ) )
		{
			stream << ":" << it->m_description;
		}

		std::cout << stream << std::endl;
		m_cmbManualTimeTag->Append( stream.str( ) );
	}
	
}

void DicomPlugin::DicomWorkingAreaAdvancedDialog::OnReload(wxCommandEvent &event)
{
	if ( m_DICOMFileReader->GetFileNames( ).empty( ) )
	{
		return;
	}

	UpdateData( );

	std::string filename = *m_DICOMFileReader->GetFileNames( ).begin( );

	if ( m_WorkingArea )
	{
		m_WorkingArea->ReadDataSet( filename );
	}
}

void DicomPlugin::DicomWorkingAreaAdvancedDialog::SetWorkingAreaPanelWidget( WorkingAreaPanelWidget* workingArea )
{
	m_WorkingArea = workingArea;
}

std::vector<dcmAPI::TagId> Classify( const std::string &filename )
{
	std::vector<dcmAPI::TagId> tagsVector;

	// Classifed table by <tag key, <tag value, <Image Position values> > >
	typedef std::map< std::string, std::list<std::string> > TagValueMapType;
	typedef std::map< std::string, TagValueMapType> TagKeyMapType;
	TagKeyMapType table;

	// Read all files recursivelly
	Core::IO::Directory::Pointer directory = Core::IO::Directory::New( );
	directory->SetDirNameFullPath( filename );
	directory->SetRecursive( true );
	Core::IO::DirEntryFilter::Pointer filter = Core::IO::DirEntryFilter::New( );
	filter->SetMode( Core::IO::DirEntryFilter::FilesOnly );
	directory->SetFilter( filter );
	Core::IO::FileNameList contents = directory->GetContents( );

	// Classify DICOM tags
	Core::IO::FileNameList::iterator it;
	long imageCounter = 0;
	for ( it = contents.begin( ) ; it != contents.end( ) ; it++ )
	{
		gdcm::File gdcmFile;
		gdcmFile.SetFileName( *it );
		if ( !gdcmFile.Load() )
		{
			continue;
		}

		// Get ImagePatient entry if exists
		std::string imagePosition;
		gdcm::DocEntry* entry = NULL;
		entry = gdcmFile.GetDocEntry(
			dcmAPI::tags::ImagePositionPatient.m_group, dcmAPI::tags::ImagePositionPatient.m_element);
		if(entry != NULL)
		{
			imagePosition = gdcm::ValEntry(entry).GetValue();
		}

		// Iterate over all tags
		gdcm::DocEntry* docEntry = gdcmFile.GetFirstEntry();
		while(docEntry)
		{
			std::string key = docEntry->GetKey();
			std::string value = gdcm::ValEntry(docEntry).GetValue();

			// Increment counter
			table[ key ][ value ].push_back( imagePosition );
			docEntry = gdcmFile.GetNextEntry();
		}

		imageCounter++;
	}

	// Find all candidate tags 
	for( TagKeyMapType::iterator itTable = table.begin( ) ; itTable != table.end( ) ; itTable++ )
	{
		// More than one group of values and less than number of images
		if ( itTable->second.size( ) == 1 || 
			 itTable->second.size( ) == imageCounter )
		{
			continue;
		}

		// If image position is present:
		bool checkCurrentTag = true;
		if ( table.find("0020|0032") != table.end( ) )
		{
			std::cout << itTable->first << ": ";

			// For each tag value, check that all image positions are present
			TagValueMapType::iterator itTagValue = itTable->second.begin( );
			while( itTagValue != itTable->second.end( ) )
			{
				std::cout << itTagValue->first << "(" << itTagValue->second.size( ) << ")" << ", ";

				// Get Image Position values and remove it from table
				TagValueMapType::iterator itImagePosition;
				itImagePosition = table["0020|0032"].begin( );
				while ( itImagePosition != table["0020|0032"].end( ) )
				{
					std::list<std::string>::iterator itFound;
					itFound = std::find( itTagValue->second.begin( ), itTagValue->second.end( ), itImagePosition->first );
					checkCurrentTag = checkCurrentTag && itFound != itTagValue->second.end( );
					if ( itFound != itTagValue->second.end( ) )
					{
						itTagValue->second.erase( itFound );
					}
					itImagePosition++;
				}

				checkCurrentTag = checkCurrentTag && itTagValue->second.empty( );
				itTagValue++;
			}
		}
		else
		{
			// All values have the same number of elements
			long numElements = itTable->second.begin( )->second.size( );
			TagValueMapType::iterator itTagValue = itTable->second.begin( );
			while( checkCurrentTag && itTagValue != itTable->second.end( ) )
			{
				std::cout << itTagValue->first << "(" << itTagValue->second.size( ) << ")" << ", ";

				checkCurrentTag = checkCurrentTag && numElements == itTagValue->second.size( );
				itTagValue++;
			}
		}

		std::cout << std::endl;

		if ( checkCurrentTag )
		{
			std::list<std::string> data;
			blTextUtils::ParseLine( itTable->first, '|', data );
			if ( data.size( ) != 2 )
			{
				continue;
			}

			std::list<std::string>::iterator itWord = data.begin();
			dcmAPI::TagId tag;

			std::stringstream ss;
			ss << std::hex << itWord->c_str( );
			ss >> tag.m_group;
			itWord++;

			std::stringstream ss1;
			ss1 << std::hex << itWord->c_str( );
			ss1 >> tag.m_element;

			tagsVector.push_back( tag );
		}
	}

	return tagsVector;
}

