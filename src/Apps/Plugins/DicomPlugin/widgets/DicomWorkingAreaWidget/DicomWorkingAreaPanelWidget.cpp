/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
	
#include "DicomWorkingAreaPanelWidget.h"
#include "DicomPatientItemTreeData.h"
#include "DicomStudyItemTreeData.h"
#include "DicomSeriesItemTreeData.h"
#include "DicomTimepointItemTreeData.h"
#include "DicomSliceItemTreeData.h"
#include "DicomConnectToPacsDialogWidget.h"
#include "DicomMenuEventHandler.h"
#include "DicomWorkingAreaAdvancedDialog.h"

#include "dcmFile.h"
#include "dcmDataSetReader.h"
#include "dcmMultiSliceReader.h"
#include "dcmSearch.h"

#include <boost/shared_ptr.hpp>

#include "coreVTKImageDataHolder.h"
#include "coreReportExceptionMacros.h"
#include "coreKernel.h"
#include "coreWxMitkCoreMainWindow.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreDataEntity.h"
#include "coreSettings.h"
#include "coreDataContainer.h"

#include "blMitkUnicode.h"

#include "wx/dynarray.h"
#include "wx/busyinfo.h"
#include "wx/imaglist.h"
#include "wx/dnd.h"
#include "wxID.h"
#include "wxEventHandlerHelper.h"

#include "dcmFile.h"
#include "dcmImageUtilities.h"
#include "dcmIOUtils.h"

#include "coreDataEntityReader.h"

using namespace DicomPlugin;
using namespace mitk;


// Event the widget
BEGIN_EVENT_TABLE(WorkingAreaPanelWidget, DicomWorkingAreaPanelWidgetUI)
	EVT_BUTTON(wxID_DICOM_ADD_TO_DATALIST, WorkingAreaPanelWidget::AddToDataList)
	EVT_SLIDER(wxID_DICOM_IMG_SPACE_SLIDER, WorkingAreaPanelWidget::OnSpaceSliderChanged)
	EVT_SLIDER(wxID_DICOM_IMG_TIME_SLIDER, WorkingAreaPanelWidget::OnTimeSliderChanged)
END_EVENT_TABLE()

int WorkingAreaPanelWidget::m_DataEntityCounter = 0;

// Check if orientation tag is present in the read dataset
bool CheckOrientationTag( dcmAPI::DataSet::Pointer dataSet );


#if wxUSE_DRAG_AND_DROP
/**
\brief Drop target for Data entity list (i.e. user drags a file from 
explorer unto window and adds the file to entity list)

\author Xavi Planes
\date 05 Dec 2008
\ingroup gmWidgets
*/
class DICOMDropTarget : public wxFileDropTarget
{
public:
	DICOMDropTarget(DicomMenuEventHandler *dicomMenuEventHandler) {
		m_DicomMenuEventHandler = dicomMenuEventHandler;
	}
	~DICOMDropTarget(){}
	virtual bool OnDropFiles(wxCoord WXUNUSED(x), wxCoord WXUNUSED(y),
		const wxArrayString& files)
	{

		if ( files.size() == 0 )
		{
			return false;
		}

		m_DicomMenuEventHandler->Open( _U(files[ 0 ]) );

		m_DicomMenuEventHandler->AddFileToHistory( _U(files[ 0 ]) );

		return true;
	}
private:
	//!
	DicomMenuEventHandler *m_DicomMenuEventHandler;
};
#endif


WorkingAreaPanelWidget::WorkingAreaPanelWidget( wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style)
: DicomWorkingAreaPanelWidgetUI(parent, id, pos, size, style)
{
	try
	{
		Core::IO::BaseDataEntityReader::Pointer baseReader;
		baseReader = Core::IO::DataEntityReader::GetRegisteredReader( "Core::IO::DICOMFileReader" );
		if ( baseReader.IsNotNull() )
		{
			m_Reader = dynamic_cast<Core::IO::DICOMFileReader*> (baseReader->CreateAnother( ).GetPointer( ));
		}

		m_dcmImageDataHolder = Core::DataEntityHolder::New();

		m_RenderWindow->Init(m_dcmImageDataHolder);
		m_LevelWindowWidget->SetDataStorage(m_RenderWindow->GetDataTree()->GetDataStorage());
		m_LevelWindowWidget->SetLevelWindowManager(m_RenderWindow->GetLevelWindowManager());
		m_LevelWindowWidget->Show(true);

		Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
		gIface = Core::Runtime::Kernel::GetGraphicalInterface();
		wxWindow* mainWindow = dynamic_cast<wxWindow*> ( gIface->GetMainWindow( ) );
		m_DicomMenuEventHandler = new DicomMenuEventHandler( this );
		mainWindow->PushEventHandler( m_DicomMenuEventHandler );


		#if wxUSE_DRAG_AND_DROP
			// Drag & Drop
			SetDropTarget( new DICOMDropTarget( m_DicomMenuEventHandler ) );
		#endif

		m_AdvancedDialog = NULL;

		m_SelectedSliceHolder = Core::DataHolder<int>::New( );
		m_SelectedSliceHolder->SetSubject( -1 );
		m_SelectedSliceHolder->AddObserver( this, &DicomPlugin::WorkingAreaPanelWidget::OnSelectedSliceModified );
		m_treeDicomView->SetSelectedSliceHolder( m_SelectedSliceHolder );
		m_MovieToolbar->SetSelectedSliceHolder( m_SelectedSliceHolder );
	}
	coreCatchExceptionsReportAndNoThrowMacro(WorkingAreaPanelWidget::WorkingAreaPanelWidget)
}

DicomPlugin::WorkingAreaPanelWidget::~WorkingAreaPanelWidget()
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
	gIface = Core::Runtime::Kernel::GetGraphicalInterface();

	if ( m_DicomMenuEventHandler )
	{
		wxWindow* mainWindow = dynamic_cast<wxWindow*> ( gIface->GetMainWindow( ) );
		wxPopEventHandler( mainWindow, m_DicomMenuEventHandler );
		delete m_DicomMenuEventHandler;
	}
}

void WorkingAreaPanelWidget::AddToDataList(wxCommandEvent& event)
{
	wxBusyInfo info(wxT("Generating data, please wait..."), this);
	try
	{
		wxArrayTreeItemIds treeItemIdsArray;
		if(m_treeDicomView->GetSelections(treeItemIdsArray) < 1)
			return;

		std::vector<Core::DataEntity::Pointer> dataEntities;
		dataEntities = BuildDataEntities( treeItemIdsArray, true, false );

		for ( int i = 0 ; i < dataEntities.size() ; i++ )
		{
			Core::DataEntity::Pointer dataEntity = dataEntities[ i ];
			// Add to the data entity list
			if( dataEntity.IsNotNull() )
			{
				std::ostringstream name;
				name << dataEntity->GetMetadata()->GetName() + "_" << m_DataEntityCounter++;
				dataEntity->GetMetadata()->SetName( name.str( ) );

				Core::DataContainer::Pointer data = Core::Runtime::Kernel::GetDataContainer();
				Core::DataEntityList::Pointer list = data->GetDataEntityList();

				list->Add( dataEntity );
				list->GetSelectedDataEntityHolder()->SetSubject( dataEntity );
			}

		}

		m_Reader->SetNumberOfOutputs( 0 );
	}
	coreCatchExceptionsReportAndNoThrowMacro(WorkingAreaPanelWidget::AddToDataList)
}

void WorkingAreaPanelWidget::OnSelectedSliceModified()
{
	int count = 0;
	wxTreeItemId item = m_treeDicomView->FindTreeItem( m_SelectedSliceHolder->GetSubject( ), count );
	if ( item != m_treeDicomView->GetCurrentSliceTreeItemId( ) )
	{
		RenderDicomSlice( item );
	}
}

void WorkingAreaPanelWidget::OnTreeSelChanged(wxTreeEvent& event)
{	
	try
	{
		wxArrayTreeItemIds treeItemIdsArray;
		if(m_treeDicomView->GetSelections(treeItemIdsArray) < 1)
			return;

		// get the last selected tree item
		wxTreeItemId treeItemId = treeItemIdsArray.Last();

		// If patient -> select first study
		if(dynamic_cast<PatientItemTreeData*>(m_treeDicomView->GetItemData(treeItemId)) != NULL)
		{
			if(m_treeDicomView->GetChildrenCount(treeItemId) > 0)
			{
				wxTreeItemIdValue cookie;
				treeItemId = m_treeDicomView->GetFirstChild(treeItemId, cookie);
			}
		}

		// If study -> select first timepoint
		if(dynamic_cast<StudyItemTreeData*>(m_treeDicomView->GetItemData(treeItemId)) != NULL)
		{	
			if(m_treeDicomView->GetChildrenCount(treeItemId) > 0)
			{
				wxTreeItemIdValue cookie;
				treeItemId = m_treeDicomView->GetFirstChild(treeItemId, cookie);
			}
		}
		
		// If study -> select first timepoint
		if(dynamic_cast<SeriesItemTreeData*>(m_treeDicomView->GetItemData(treeItemId)) != NULL)
		{	
			if(m_treeDicomView->GetChildrenCount(treeItemId) > 0)
			{
				wxTreeItemIdValue cookie;
				treeItemId = m_treeDicomView->GetFirstChild(treeItemId, cookie);
			}
		}

		//select the first slice in this timepoint
		if(dynamic_cast<TimepointItemTreeData*>(m_treeDicomView->GetItemData(treeItemId)) != NULL)
		{	
			if(m_treeDicomView->GetChildrenCount(treeItemId) > 0)
			{
				//select the middle slice
				wxTreeItemIdValue cookie;
				int middleSlicePos = m_treeDicomView->GetChildrenCount(treeItemId)/2;
				wxTreeItemId sliceItemId = m_treeDicomView->GetFirstChild(treeItemId, cookie);
				while(middleSlicePos > 0 && sliceItemId.IsOk())
				{
					sliceItemId = m_treeDicomView->GetNextChild(treeItemId, cookie);
					middleSlicePos--;
				}
				treeItemId = sliceItemId;
			}
		}

		//checking if we selected tree item is a slice item
		if(dynamic_cast<SliceItemTreeData*>(m_treeDicomView->GetItemData(treeItemId)) != NULL)
		{
			//render slice
			RenderDicomSlice(treeItemId);
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro(WorkingAreaPanelWidget::OnTreeSelChanged)
}

dcmAPI::DataSet::Pointer DicomPlugin::WorkingAreaPanelWidget::GetDataSet()
{
	return m_Reader->GetDataSet();
}

Core::BaseProcessor::Pointer DicomPlugin::WorkingAreaPanelWidget::GetProcessor()
{
	return Core::BaseProcessor::Pointer(m_Reader);
}

bool DicomPlugin::WorkingAreaPanelWidget::Enable( bool enable /*= true */ )
{
	m_DicomMenuEventHandler->SetEvtHandlerEnabled( enable );
	return DicomWorkingAreaPanelWidgetUI::Enable( enable );
}

void DicomPlugin::WorkingAreaPanelWidget::OnInit()
{
}

void WorkingAreaPanelWidget::SetSpaceSliderPosition(wxTreeItemId sliceTreeItemId)
{
	try
	{	
		//find out how many slices are in the current timepoint and set the range
		wxTreeItemId timepointTreeItemId = m_treeDicomView->GetItemParent(sliceTreeItemId);
		unsigned int sliceCount = m_treeDicomView->GetChildrenCount(timepointTreeItemId);
		m_dcmSpaceSlider->SetRange(0, sliceCount-1);
		
		//set proper position of the space slider
		wxTreeItemIdValue cookie;
		int pos = 0;
		wxTreeItemId treeItemId = m_treeDicomView->GetFirstChild(timepointTreeItemId, cookie);
		while(treeItemId.IsOk())
		{
			if(treeItemId == sliceTreeItemId)
			{
				m_dcmSpaceSlider->SetValue(pos);
				break;
			}
			treeItemId = m_treeDicomView->GetNextChild(timepointTreeItemId, cookie);
			pos++;
		}
		
	}
	coreCatchExceptionsReportAndNoThrowMacro(WorkingAreaPanelWidget::SetSpaceSliderPosition)
}

void WorkingAreaPanelWidget::SetTimeSliderPosition(wxTreeItemId sliceTreeItemId)
{
	try
	{	
		//find out how many timepoints are in the current series and set the range
		wxTreeItemId timepointTreeItemId = m_treeDicomView->GetItemParent(sliceTreeItemId);
		wxTreeItemId serieTreeItemId = m_treeDicomView->GetItemParent(timepointTreeItemId);
		unsigned int timepointCount = m_treeDicomView->GetChildrenCount(serieTreeItemId, false);
		m_dcmTimeSlider->SetRange(0, timepointCount-1);
		
		//set proper position of the time slider
		wxTreeItemIdValue cookie;
		int pos = 0;
		wxTreeItemId treeItemId = m_treeDicomView->GetFirstChild(serieTreeItemId, cookie);
		while(treeItemId.IsOk())
		{
			if(treeItemId == timepointTreeItemId)
			{
				m_dcmTimeSlider->SetValue(pos);
				break;
			}
			treeItemId = m_treeDicomView->GetNextChild(serieTreeItemId, cookie);
			pos++;
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro(WorkingAreaPanelWidget::SetTimeSliderPosition)
}

void WorkingAreaPanelWidget::SpaceSliderChanged()
{
	try
	{
		//get new position of the space slider
		int newSpacePos  = m_dcmSpaceSlider->GetValue();

		unsigned int sliceCount = m_treeDicomView->GetCurrentSelectedSliceCount();
		if ( sliceCount > 0 )
		{
			m_dcmSpaceSlider->SetRange(0, sliceCount-1);
		}

		bool selectionChanged;
		selectionChanged = m_treeDicomView->SelectItemByIndex( newSpacePos );
		if ( selectionChanged )
		{
			RenderDicomSlice( m_treeDicomView->GetCurrentSliceTreeItemId( ) );
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro(WorkingAreaPanelWidget::SpaceSliderChanged)
}

void WorkingAreaPanelWidget::OnSpaceSliderChanged(wxCommandEvent& event)
{
	SpaceSliderChanged();
}

void WorkingAreaPanelWidget::OnTimeSliderChanged(wxCommandEvent& event)
{
	TimeSliderChanged();
}

void WorkingAreaPanelWidget::TimeSliderChanged()
{
	try
	{
		////get new position of the time slider and current position of the space slider
		int newTimeSliderPos  = m_dcmTimeSlider->GetValue();
		int spaceSliderPos  = m_dcmSpaceSlider->GetValue();

		bool selectionChanged;
		selectionChanged = m_treeDicomView->SelectItemByIndex( newTimeSliderPos, spaceSliderPos );
		if ( selectionChanged )
		{
			RenderDicomSlice( m_treeDicomView->GetCurrentSliceTreeItemId( ) );
		}

	}
	coreCatchExceptionsReportAndNoThrowMacro(WorkingAreaPanelWidget::TimeSliderChanged)

}

void WorkingAreaPanelWidget::RenderDicomSlice(wxTreeItemId sliceTreeItemId)
{
	try
	{
		m_treeDicomView->SetCurrentSliceTreeItemId( sliceTreeItemId );

		//create data entity from selected slice
		wxArrayTreeItemIds treeItemIdsArray;
		treeItemIdsArray.Add( sliceTreeItemId );
		std::vector<Core::DataEntity::Pointer> dataEntities;
		dataEntities = BuildDataEntities( treeItemIdsArray, false, true );
		
		//set filename as a label for currently rendered slice
		SliceItemTreeData* sliceItemTreeData = (SliceItemTreeData*)m_treeDicomView->GetItemData(sliceTreeItemId);
		if(sliceItemTreeData != NULL)
		{
			m_sliceName->SetValue(_U(sliceItemTreeData->GetSlicePath()));
		}

		Core::DataEntity::Pointer dataEntity;
		if ( !dataEntities.empty() && dataEntities[ 0 ].IsNotNull( ) )
		{
			dataEntity = dataEntities[ 0 ];
		}
		
		//set time slider position
		SetTimeSliderPosition(sliceTreeItemId);

		//set space slider position
		SetSpaceSliderPosition(sliceTreeItemId);
		
		//save current level window property
		m_RenderWindow->SaveCurrentLevelWindow();

		//set slice data in DataHolder
		m_dcmImageDataHolder->SetSubject(dataEntity, true);
	}
	coreCatchExceptionsReportAndNoThrowMacro(WorkingAreaPanelWidget::RenderDicomSlice)
}

void WorkingAreaPanelWidget::LoadDefaultLevelWindow(wxTreeItemId sliceTreeItemId)
{
	try
	{
		SliceItemTreeData* sliceItemTreeData = (SliceItemTreeData*)m_treeDicomView->GetItemData(sliceTreeItemId);
		if(sliceItemTreeData == NULL)
			return;
		
		mitk::LevelWindow levelWindow;
		int center = atoi(sliceItemTreeData->GetWindowCenter().c_str());	
		int width = atoi(sliceItemTreeData->GetWindowWidth().c_str());
		if ( width == 0 )
		{
			return;
		}

		levelWindow.SetWindowBounds(center - width / 2.0, center + width / 2.0);
		levelWindow.SetRangeMinMax(center - width / 2.0, center + width / 2.0);
		levelWindow.SetDefaultRangeMinMax(center - width / 2.0, center + width / 2.0);
		levelWindow.SetDefaultLevelWindow(center, width);
		m_RenderWindow->GetLevelWindowManager()->SetLevelWindow( levelWindow );
	}
	coreCatchExceptionsReportAndNoThrowMacro(WorkingAreaPanelWidget::LoadDefaultLevelWindow)
}

void WorkingAreaPanelWidget::DoStartStep()
{
	try
	{
		wxTreeItemIdValue cookie;
		
		// expand first Patient->Study->Series
		wxTreeItemId timepointId = m_treeDicomView->GetFirstSpecifiedTreeItem( DICOMTree::TIMEPOINT, cookie, true);
		if(!timepointId.IsOk())
			return;

		// render the middle slice in the first timepoint
		int middleSlicePos = m_treeDicomView->GetChildrenCount(timepointId)/2;
		wxTreeItemId sliceId = m_treeDicomView->GetFirstChild(timepointId, cookie);
		while(middleSlicePos > 0 && sliceId.IsOk())
		{
			sliceId = m_treeDicomView->GetNextChild(timepointId, cookie);
			middleSlicePos--;
		}

		//wxTreeItemId sliceId = m_treeDicomView->GetFirstChild(timepointId, cookie);
		if(!sliceId.IsOk())
			return;

		//select a slice and render it
		m_treeDicomView->UnselectAll();
		m_treeDicomView->SelectItem(timepointId);
		//m_treeDicomView->SelectItemByIndex(sliceId);
		//LoadDefaultLevelWindow(sliceId);
		m_RenderWindow->SetUseFixedLevelWindow(false);
		RenderDicomSlice(sliceId);
		m_RenderWindow->SetUseFixedLevelWindow(true);
	}
	coreCatchExceptionsReportAndNoThrowMacro(WorkingAreaPanelWidget::DoStartStep)
}

void WorkingAreaPanelWidget::BuildSeriesCollectiveFromSeriesDataset()
{
	try
	{
		m_Reader->SetLoadFilter( Core::IO::DICOMFileReader::LOAD_FILTER_MULTISLICE );
		m_Reader->SetPostProcessData( false );
		m_Reader->Update( );

		m_Reader->SetNumberOfInputs( m_Reader->GetNumberOfOutputs( ) );
		for ( int i = 0 ; i < m_Reader->GetNumberOfOutputs( ) ; i++ )
		{
			Core::DataEntity::Pointer dataEntity = m_Reader->GetOutputDataEntity( i );
			m_Reader->SetInputDataEntity( i, dataEntity );

			// Try with signal data
			blSignalCollective::Pointer signalCollective;
			bool success = dataEntity->GetProcessingData( signalCollective );
			if ( success )
			{
				wxTreeItemIdValue cookie;
				wxTreeItemId seriesTreeId = m_treeDicomView->GetFirstSpecifiedTreeItem(DICOMTree::SERIES, cookie);

				if ( signalCollective->GetNumberOfSignals() > 0 && seriesTreeId.IsOk() )
				{
					//attach ECG signal if it was in the data
					m_treeDicomView->AppendItem( seriesTreeId, _U("ECG"), 5, 5, NULL );
				}
			}

		}
	}
	coreCatchExceptionsReportAndNoThrowMacro( WorkingAreaPanelWidget::BuildSeriesCollectiveFromSeriesDataset )
}

std::vector<Core::DataEntity::Pointer> 
DicomPlugin::WorkingAreaPanelWidget::BuildDataEntities( 
	wxArrayTreeItemIds treeItemIdsArray,
	bool postProcessData,
	bool forRendering )
{
	std::vector<Core::DataEntity::Pointer> dataEntities;

	m_Reader->ClearSelection();
	for(int i=0; i<treeItemIdsArray.GetCount(); i++)
	{
		wxTreeItemId treeItemId = treeItemIdsArray.Item(i);

		SliceItemTreeData* sliceItemTreeData;
		sliceItemTreeData = dynamic_cast<SliceItemTreeData*>(m_treeDicomView->GetItemData(treeItemId));
		if( sliceItemTreeData != NULL)
		{
			m_Reader->SetSelectedSliceID( sliceItemTreeData->GetSliceId() );

			// Get time point
			treeItemId = m_treeDicomView->GetItemParent( treeItemId );
		}

		TimepointItemTreeData* timepointItemTreeData;
		timepointItemTreeData = dynamic_cast<TimepointItemTreeData*>(m_treeDicomView->GetItemData(treeItemId));
		if( timepointItemTreeData != NULL)
		{
			m_Reader->SetSelectedTimePointID( timepointItemTreeData->GetTiempointId() );

			// Get serie
			treeItemId = m_treeDicomView->GetItemParent( treeItemId );
		}

		SeriesItemTreeData* seriesDataItemTreeData;
		seriesDataItemTreeData = dynamic_cast<SeriesItemTreeData*>(m_treeDicomView->GetItemData(treeItemId));
		if ( seriesDataItemTreeData != NULL ) 
		{
			m_Reader->SetSelectedSerieID( seriesDataItemTreeData->GetSeriesId() );

			// Get serie
			treeItemId = m_treeDicomView->GetItemParent( treeItemId );
		}

		StudyItemTreeData* studyItemTreeData;
		studyItemTreeData = dynamic_cast<StudyItemTreeData*>(m_treeDicomView->GetItemData(treeItemId));
		if ( studyItemTreeData != NULL ) 
		{
			m_Reader->SetSelectedStudyID( studyItemTreeData->GetStudyId() );
		}

	}

	m_Reader->SetLoadFilter( Core::IO::DICOMFileReader::LOAD_FILTER_ALL );
	m_Reader->SetPostProcessData( postProcessData );
	bool prevValue = m_Reader->GetExportDICOMSlices( );
	// Set it to false for rendering
	if ( forRendering )
	{
		m_Reader->SetExportDICOMSlices( false );
	}
	m_Reader->Update( );
	// Restore value
	m_Reader->SetExportDICOMSlices( prevValue );

	for ( int i = 0 ; i < m_Reader->GetNumberOfOutputs( ) ; i++ )
	{
		// Add FilePath
		if ( m_Reader->GetOutputDataEntity( i ).IsNotNull() )
		{
			m_Reader->GetOutputDataEntity( i )->GetMetadata( )->AddTag( "FilePath", m_Reader->GetFileNames( )[ 0 ] );
		}
		dataEntities.push_back( m_Reader->GetOutputDataEntity( i ) );
	}

	return dataEntities;
}

void DicomPlugin::WorkingAreaPanelWidget::ReadDataSet( const std::string &filename )
{
	try
	{
		// Register readers when all plugins are loaded
		RegisterReaders( );

		std::vector<std::string> filenames;
		filenames.push_back( filename );
		m_Reader->SetFileNames( filenames );
		m_Reader->ReadDataSet( filename );

		Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
		gIface = Core::Runtime::Kernel::GetGraphicalInterface();


		//load patients into tree
		m_treeDicomView->SetDcmData( GetDataSet() );
		m_treeDicomView->LoadPatientsIntoTree();

		// Update movie toolbar
		m_MovieToolbar->SetTimeRange( 0, m_treeDicomView->GetNumberOfSlices( ) );

		bool orientationTag = CheckOrientationTag( GetDataSet() );

		if ( orientationTag )
		{
			gIface->GetMainWindow()->ReportMessage("DICOM file imported successfully", false);
		}
		else
		{
			gIface->GetMainWindow()->ReportMessage("DICOM file imported without orientation information", true);
		}

		/*if the data is a multislice (3D or 4D) it has to be loaded to cached memory, 
		it means that single file contains 3D or 4D image and will be represented as SeriesCollective object
		*/
		// build series collective map
		if (m_Reader->GetDataSet()->GetStorageType() == dcmAPI::DataSet::MULTI_SLICE_PER_FILE )
		{
			BuildSeriesCollectiveFromSeriesDataset();
		}

		//show start preview
		DoStartStep();
	}
	coreCatchExceptionsReportAndNoThrowMacro(WorkingAreaPanelWidget::OnModifiedDataSet)
}

void DicomPlugin::WorkingAreaPanelWidget::RegisterReaders()
{
	// Register specific readers
	Core::IO::BaseDataEntityReader* baseReader;
	baseReader = Core::IO::DataEntityReader::GetRegisteredReader( "Core::IO::DICOMFileReader" );
	Core::IO::DICOMFileReader* DICOMReader = dynamic_cast<Core::IO::DICOMFileReader*>( baseReader );
	if ( DICOMReader )
	{
		std::list<dcmAPI::AbstractImageReader::Pointer> readers = DICOMReader->GetRegisteredReaders();
		m_Reader->ClearReaderList( );
		std::list<dcmAPI::AbstractImageReader::Pointer>::const_reverse_iterator rit;
		for ( rit = readers.rbegin() ; rit != readers.rend() ; rit++ )
		{
			m_Reader->RegisterReader( *rit );
		}
	}

}

void DicomPlugin::WorkingAreaPanelWidget::OnAdvanced( wxCommandEvent &event )
{
	if ( m_AdvancedDialog == NULL )
	{
		m_AdvancedDialog = new DicomWorkingAreaAdvancedDialog( this, wxID_ANY, "DICOM Advanced options");
	}

	m_AdvancedDialog->SetDICOMFileReader( m_Reader );
	m_AdvancedDialog->SetWorkingAreaPanelWidget( this );
	m_AdvancedDialog->Show( );
}

std::string WorkingAreaPanelWidget::GetSelectedSlicePath() const
{
	SliceItemTreeData* sliceData = dynamic_cast<SliceItemTreeData*> ( m_treeDicomView->GetItemData(m_treeDicomView->GetCurrentSliceTreeItemId()) );
	if ( sliceData == NULL )
	{
		return "";
	}

	return sliceData->GetSlicePath();
}

bool CheckOrientationTag( dcmAPI::DataSet::Pointer dataSet )
{

	bool orientationTag = true;
	dcmAPI::PatientIdVectorPtr patiendIdVector = dataSet->GetPatientIds();
	for(unsigned i=0; i < patiendIdVector->size(); i++)
	{
		dcmAPI::Patient::Pointer patient = dataSet->GetPatient( patiendIdVector->at(i) );
		dcmAPI::StudyIdVectorPtr studiesIdVector = patient->StudyIds();
		for(unsigned i=0; i < studiesIdVector->size(); i++)
		{
			dcmAPI::Study::Pointer study = patient->Study(studiesIdVector->at(i));
			dcmAPI::SeriesIdVectorPtr seriesIdVector = study->SeriesIds();
			for(unsigned i=0; i < seriesIdVector->size(); i++)
			{
				dcmAPI::Series::Pointer series = study->Series(seriesIdVector->at(i));
				dcmAPI::TimePointIdVectorPtr timePointIdVector = series->TimePointIds();
				for(unsigned i=0; i < timePointIdVector->size(); i++)
				{
					dcmAPI::TimePoint::Pointer timePoint = series->TimePoint(timePointIdVector->at(i));
					dcmAPI::SliceIdVectorPtr sliceIdVector = timePoint->SliceIds();
					for(unsigned i=0; i < sliceIdVector->size(); i++)
					{
						dcmAPI::Slice::Pointer slice = timePoint->Slice( sliceIdVector->at(i) );
						
						dcmAPI::TagsMap::iterator it;
						it = slice->GetTagsMap( )->find( dcmAPI::tags::ImageOrientationPatient );
						if ( it == slice->GetTagsMap( )->end() || it->second.empty( ) )
						{
							orientationTag = false;
						}
					}
				}
			}
		}
	}

	return orientationTag;
}

Core::DataHolder<int>::Pointer DicomPlugin::WorkingAreaPanelWidget::GetSelectedSliceHolder( )
{
	return m_SelectedSliceHolder;
}

Core::DataEntityHolder::Pointer DicomPlugin::WorkingAreaPanelWidget::GetDcmImageDataHolder( )
{
	return m_dcmImageDataHolder;
}

