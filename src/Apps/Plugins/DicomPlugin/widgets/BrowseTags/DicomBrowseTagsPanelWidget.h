/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef DICOMBROWSETAGSPANELWIDGET_H
#define DICOMBROWSETAGSPANELWIDGET_H

// Core
#include <coreProcessingWidget.h>

// UI
#include "DicomBrowseTagsPanelWidgetUI.h"
// dicom working area panel widget
#include "DicomWorkingAreaPanelWidget.h"

#include "coreToolbarBase.h"

// namespace
namespace DicomPlugin
{

/**
* Widget to browse DICOM tags.
* 
* TODO: gdcm does not seems to load the private tags, could be useful...
*
* \ingroup DicomPlugin
* \author Yves Martelli
* \date 16 March 2011
*/
class BrowseTagsPanelWidget : 
    public DicomBrowseTagsPanelWidgetUI,
    public Core::Widgets::ProcessingWidget 
{

public:

	//! Window factory.
	coreDefineBaseWindowFactory( DicomPlugin::BrowseTagsPanelWidget );
	
	//! Constructor.
	BrowseTagsPanelWidget(wxWindow* parent, int id = wxID_ANY,
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = wxAUI_TB_DEFAULT_STYLE);

	//! Desctructor.
	~BrowseTagsPanelWidget();

	//! Called at startup.
	void OnInit();
	
	//! Enable the widget.
	bool Enable( bool enable );

private:

	//! Update GUI from working data.
	void UpdateWidget();

    //! Called when text is entered in the serch box (from the UI).
    void OnModifiedText(wxCommandEvent &event);

    //! Called when the file path is modified.
    void OnModifiedFilePath();

	//! Compare from next slice
    void OnCompare(wxCommandEvent &event);

    //! Update the grid.
    void UpdateTagsGrid();

	//!
	void UpdateEntry( gdcm::DocEntry* docEntry, wxTreeListItem parent );

	//!
	void UpdateSeqEntry( gdcm::DocEntrySet* docEntrySet, wxTreeListItem parent );

	//!
	std::string GetEntryValue( gdcm::DocEntry* docEntry );

    wxDECLARE_EVENT_TABLE();

private:
    //! Connection to the dicom area widget to get the selected DICOM file.
    WorkingAreaPanelWidget* m_dwapWidget;
    
    //! Connector to the dicom area widget
    //! can't make it work (see cpp)
    //gbl::wx::ConnectorOfWidgetChangesToSlotFunction m_dwapWidgetObserver;

    //! Tags map: map< <tag, name>, value >
    gdcm::File m_File;

	//! Previous 
    gdcm::File m_PreviousFile;

	//!
	Core::Widgets::ToolbarBase *m_Toolbar;

	//!
	std::string m_Filename;

}; // class BrowseTagsPanelWidget

} //namespace DicomPlugin

#endif //DICOMBROWSETAGSPANELWIDGET_H
