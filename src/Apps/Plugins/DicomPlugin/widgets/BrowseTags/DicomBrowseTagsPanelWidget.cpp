/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#include "DicomBrowseTagsPanelWidget.h"

// Core
#include "coreStringHelper.h"
// gdcm
#include <gdcmFile.h>
#include <gdcmDocEntry.h>
#include <gdcmBinEntry.h>
#include <gdcmUtil.h>
#include <gdcmSeqEntry.h>
#include <gdcmSQItem.h>
// wx
#include <wx/settings.h>
#include <wx/wupdlock.h>

// widget collective (to get the dicom working area widget ID)
#include "DicomPluginWidgetCollective.h"

#include <algorithm>
#include <string>


#include "compare.xpm"

namespace DicomPlugin
{

#define wxID_BTN_COMPARE wxID("wxID_BTN_COMPARE")

BEGIN_EVENT_TABLE(BrowseTagsPanelWidget, DicomBrowseTagsPanelWidgetUI)
  EVT_TOOL(wxID_BTN_COMPARE, DicomPlugin::BrowseTagsPanelWidget::OnCompare)
END_EVENT_TABLE()

BrowseTagsPanelWidget::BrowseTagsPanelWidget(  
    wxWindow* parent, 
    int id,
    const wxPoint& pos, 
    const wxSize& size, 
    long style )
: DicomBrowseTagsPanelWidgetUI( parent, id, pos, size, style )
{
	SetName( "Dicom Browse Panel Widget" );
    m_dwapWidget = NULL;

	// Create toolbar
	m_Toolbar = new Core::Widgets::ToolbarBase( this, wxID_ANY );
	m_Toolbar->AddTool(wxID_BTN_COMPARE, _T("Show difference"),
		compare_xpm, _T("Show difference"), wxITEM_CHECK );
	m_Toolbar->Realize( );

	// Do layout
    wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
	sizer->Add(m_Toolbar, 1, wxALL|wxEXPAND, 0);
	GetSizer()->Insert(0, sizer, 0, wxALL|wxEXPAND, 5);
	GetSizer()->Layout( );

	// Add tree columns
	m_TreeListCtrl->AppendColumn("Tag", wxCOL_WIDTH_AUTOSIZE, wxALIGN_LEFT, wxCOL_RESIZABLE | wxCOL_SORTABLE);
	m_TreeListCtrl->AppendColumn("Name", wxCOL_WIDTH_AUTOSIZE, wxALIGN_LEFT, wxCOL_RESIZABLE | wxCOL_SORTABLE);
	m_TreeListCtrl->AppendColumn("Value", wxCOL_WIDTH_AUTOSIZE, wxALIGN_LEFT, wxCOL_RESIZABLE | wxCOL_SORTABLE);
}

BrowseTagsPanelWidget::~BrowseTagsPanelWidget( )
{
}

void BrowseTagsPanelWidget::OnInit()
{
	UpdateWidget();
    // access the dicom working area widget
	GetPluginTab()->GetWidget( wxID_WorkingAreaPanelWidget, m_dwapWidget );

	if ( m_dwapWidget )
	{
	    m_dwapWidget->GetDcmImageDataHolder( )->AddObserver(this, &BrowseTagsPanelWidget::OnModifiedFilePath);
	}
}

void BrowseTagsPanelWidget::UpdateWidget()
{
}
 
bool BrowseTagsPanelWidget::Enable( bool enable )
{
	// enable the UI
    const bool bReturn = DicomBrowseTagsPanelWidgetUI::Enable( enable );
	// If this panel widget is selected -> Update the widget
	if( enable )
	{
		UpdateWidget();
	}
    // return UI enable result
	return bReturn;
}

void BrowseTagsPanelWidget::OnModifiedText(wxCommandEvent &event)
{
    UpdateTagsGrid();
}

void BrowseTagsPanelWidget::OnModifiedFilePath()
{
    // get the file path
    const std::string filePath = m_dwapWidget->GetSelectedSlicePath();
	if ( m_Filename == filePath )
	{
		return;
	}

    // read the file
	m_File.SetFileName( filePath );
	if(!m_File.Load())
	{
        return;
    }

    // read the previous file
	m_PreviousFile.SetFileName( m_Filename );
	m_PreviousFile.Load( );

	m_Filename = filePath;

	// update the grid with new values
    UpdateTagsGrid();
}

void BrowseTagsPanelWidget::UpdateTagsGrid()
{
	wxWindowUpdateLocker lock( this );

	// Backup scroll position
	int scrollPosY = m_TreeListCtrl->GetScrollPos( wxVERTICAL );
    // clear the grid

	m_TreeListCtrl->DeleteAllItems();
	m_TreeListCtrl->Collapse( m_TreeListCtrl->GetRootItem( ) );

    // Fill all tags 
	UpdateSeqEntry( &m_File, m_TreeListCtrl->GetRootItem( ) );

	// Update tree control
	m_TreeListCtrl->Expand( m_TreeListCtrl->GetRootItem( ) );
	m_TreeListCtrl->SetScrollPos( wxVERTICAL, scrollPosY );
}

void BrowseTagsPanelWidget::UpdateSeqEntry( gdcm::DocEntrySet* docEntrySet, wxTreeListItem parent )
{
    // Fill all tags 
    gdcm::DocEntry* docEntry = docEntrySet->GetFirstEntry();
    while(docEntry)
    {
		UpdateEntry( docEntry, parent );

        docEntry = docEntrySet->GetNextEntry();
    }
}

void BrowseTagsPanelWidget::UpdateEntry( gdcm::DocEntry* docEntry, wxTreeListItem parent )
{
	// Get key, name
	std::string key = docEntry->GetKey();
	std::string name = docEntry->GetName();
	std::string value = GetEntryValue( docEntry );

	// Check search
    // get the text to find (use it lower case to find)
    std::string searchTxt = std::string(text_ctrl_search->GetValue().mb_str());
    searchTxt = Core::StringHelper::ToLowerCase( searchTxt );

	bool checkSearch = 
		   Core::StringHelper::ToLowerCase( key ).find(searchTxt) != std::string::npos
        || Core::StringHelper::ToLowerCase( name ).find(searchTxt) != std::string::npos
        || Core::StringHelper::ToLowerCase( value ).find(searchTxt) != std::string::npos;

	// Check compare
	bool checkCompare = true;
	if ( m_Toolbar->GetToolToggled( wxID_BTN_COMPARE ) == true )
	{
		uint16_t group = docEntry->GetDictEntry()->GetGroup();
		uint16_t elem  = docEntry->GetDictEntry()->GetElement();

		gdcm::DocEntry* docEntry = m_PreviousFile.GetDocEntry( group, elem );
		std::string previousValue = GetEntryValue( docEntry );
		checkCompare = previousValue != value;
	}

	// Add tag
    if( checkSearch && checkCompare )
    {
		wxTreeListItem treeItem = m_TreeListCtrl->AppendItem( parent, key  );
        m_TreeListCtrl->SetItemText(treeItem, 1,  name );
        m_TreeListCtrl->SetItemText(treeItem, 2,  value );

		// If seq entry -> Update it
		if ( gdcm::SeqEntry *sqi = dynamic_cast<gdcm::SeqEntry *>(docEntry) )
		{
			m_TreeListCtrl->SetItemText(treeItem, 2, "");
			for(unsigned int pd = 0; pd < sqi->GetNumberOfSQItems() ; ++pd)
			{
		        gdcm::SQItem* item = sqi->GetSQItem( pd );

				std::stringstream stream;
				stream << "Item " << pd;
				wxTreeListItem childtreeItem = m_TreeListCtrl->AppendItem( treeItem, stream.str( ).c_str( )  );

				UpdateSeqEntry( item, childtreeItem );
			}
		}
	}
}

std::string BrowseTagsPanelWidget::GetEntryValue( gdcm::DocEntry* docEntry )
{
	if ( docEntry == NULL )
	{
		return "";
	}

	// Get entry value
	uint16_t group = docEntry->GetDictEntry()->GetGroup();
	uint16_t elem  = docEntry->GetDictEntry()->GetElement();
	std::string value = gdcm::GDCM_NOTASCII;

	// This function crashes when a is NULL
	//value = file.GetEntryForcedAsciiValue(group, elem);
	if (gdcm::ValEntry *v = dynamic_cast<gdcm::ValEntry *>(docEntry))
	{
		value = v->GetValue();
	}
	else if (gdcm::BinEntry *b = dynamic_cast<gdcm::BinEntry *>(docEntry))
	{
		uint8_t *a = b->GetBinArea();
		if (!b)
		{
			value = gdcm::GDCM_NOTLOADED;
		}
		else if (!a)
		{
			value = gdcm::GDCM_NOTLOADED;
		}
		else if (gdcm::Util::IsCleanArea(a, b->GetLength()) )
		{
			value = gdcm::Util::CreateCleanString(a, b->GetLength());
		}
	}

	return value;
}

void BrowseTagsPanelWidget::OnCompare( wxCommandEvent& event )
{
	UpdateTagsGrid( );
}



} // namespace DicomPlugin
