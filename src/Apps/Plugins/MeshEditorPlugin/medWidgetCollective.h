/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _medWidgetCollective_H
#define _medWidgetCollective_H

#include "coreFrontEndPlugin.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreWidgetCollective.h"

#include "medProcessorCollective.h"




/**

\ingroup MeshEditorPlugin
\author Xavi Planes
\date 12 April 2010
*/

class medWidgetCollective : public Core::WidgetCollective
{
public:
	//!
	coreDeclareSmartPointerClassMacro(
		medWidgetCollective, 
		Core::WidgetCollective );


	

	//!
	void Init();

private:
	medWidgetCollective( );

private:

};

#endif //_medWidgetCollective_H
