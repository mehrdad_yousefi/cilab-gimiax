/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "BooleanOperationsPanelWidget.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"
// Core
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"

// Core
#include "coreDataTreeHelper.h"
#include "coreReportExceptionMacros.h"
#include "coreProcessorInputWidget.h"
#include "corePointInteractorPointSet.h"
#include "coreLandmarkSelectorWidget.h"
#include "coreDataTreeMITKHelper.h"
#include "coreBaseWindow.h"


#include "mitkProperties.h"
#include "blMitkUnicode.h"
#include "coreDataContainer.h"
#include "mitkMaterialProperty.h"
#include "coreKernel.h"
#include "corePointInteractorPointSelect.h"
#include "coreDataTreeMITKHelper.h"
#include "mitkRenderingManager.h"
#include "vtkBooleanOperationPolyDataFilter.h"


BooleanOperationsPanelWidget::BooleanOperationsPanelWidget(  wxWindow* parent, int id/*= wxID_ANY*/,
													   const wxPoint&  pos /*= wxDefaultPosition*/, 
													   const wxSize&  size /*= wxDefaultSize*/, 
													   long style/* = 0*/ )
: BooleanOperationsPanelWidgetUI(parent, id,pos,size,style)
{
	m_Processor = BooleanOperationsProcessor::New();

	SetName( "Scars3D Panel Widget" );
}

BooleanOperationsPanelWidget::~BooleanOperationsPanelWidget( )
{
	// We don't need to destroy anything because all the child windows 
	// of this wxWindow are destroyed automatically
}

void BooleanOperationsPanelWidget::OnInit( )
{
	GetInputWidget(BooleanOperationsProcessor::INPUT_SURFACE_1)->SetDefaultDataEntityFlag(false);
	GetInputWidget(BooleanOperationsProcessor::INPUT_SURFACE_1)->SetAutomaticSelection(false);
	GetInputWidget(BooleanOperationsProcessor::INPUT_SURFACE_2)->SetDefaultDataEntityFlag(false);
	GetInputWidget(BooleanOperationsProcessor::INPUT_SURFACE_2)->SetAutomaticSelection(false);




	//------------------------------------------------------
	// Observers to data
	m_Processor->GetOutputDataEntityHolder( BooleanOperationsProcessor::OUTPUT_SURFACE )->AddObserver( 
		this, 
		&BooleanOperationsPanelWidget::OnModifiedOutputDataEntity );
}

void BooleanOperationsPanelWidget::UpdateWidget()
{
	
	UpdateHelperWidget( );
}

void BooleanOperationsPanelWidget::UpdateData()
{
	// Set parameters to processor. Pending
}

void BooleanOperationsPanelWidget::OnBtnSubtract(wxCommandEvent& event)
{
	// Catch the exception from the processor and show the message box
	try
	{
		m_Processor->SetOperation(vtkBooleanOperationPolyDataFilter::SUBTRACTION);
		m_Processor->Update();
	}
	coreCatchExceptionsReportAndNoThrowMacro( "BooleanOperationsPanelWidget::OnBtnSubtract" );
}

void BooleanOperationsPanelWidget::OnBtnUnion(wxCommandEvent& event)
{
	// Catch the exception from the processor and show the message box
	try
	{
		m_Processor->SetOperation(vtkBooleanOperationPolyDataFilter::UNION);
		m_Processor->Update();
	}
	coreCatchExceptionsReportAndNoThrowMacro( "BooleanOperationsPanelWidget::OnBtnUnion" );
}

void BooleanOperationsPanelWidget::OnBtnIntersection(wxCommandEvent& event)
{
	// Catch the exception from the processor and show the message box
	try
	{
		m_Processor->SetOperation(vtkBooleanOperationPolyDataFilter::INTERSECTION);
		m_Processor->Update();
	}
	coreCatchExceptionsReportAndNoThrowMacro( "BooleanOperationsPanelWidget::OnBtnIntersection" );
}

void BooleanOperationsPanelWidget::OnModifiedOutputDataEntity()
{
	try
	{

		if (m_Processor->GetOutputDataEntity( BooleanOperationsProcessor::OUTPUT_SURFACE ).IsNotNull())
		{
			Core::DataTreeHelper::PublishOutput( 
				m_Processor->GetOutputDataEntityHolder( BooleanOperationsProcessor::OUTPUT_SURFACE ), 
				GetRenderingTree(),false, false );
			//hide the original surfaces
			Core::DataEntity::Pointer inputDataEntity;
			inputDataEntity = m_Processor->GetInputDataEntity( BooleanOperationsProcessor::INPUT_SURFACE_1 );
			GetRenderingTree()->Show(inputDataEntity,false);
			
			Core::DataEntity::Pointer inputDataEntity2;
			inputDataEntity = m_Processor->GetInputDataEntity( BooleanOperationsProcessor::INPUT_SURFACE_2 );
			GetRenderingTree()->Show(inputDataEntity,false);
		}
	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"BooleanOperationsPanelWidget::OnModifiedOutputDataEntity")

}



void BooleanOperationsPanelWidget::UpdateHelperWidget()
{
	if ( GetHelperWidget( ) == NULL )
	{
		return;
	}
	/*	GetHelperWidget( )->SetInfo( 
			Core::Widgets::HELPER_INFO_LEFT_BUTTON, 
			" info that is useful in order to use the processor" );*/

}

bool BooleanOperationsPanelWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = BooleanOperationsPanelWidgetUI::Enable( enable );

	// If this panel widget is selected -> Update the widget
	if ( enable )
	{	
		UpdateWidget();
	}
	return bReturn;
}

void BooleanOperationsPanelWidget::OnModifiedInputDataEntity()
{
	
}

Core::BaseProcessor::Pointer BooleanOperationsPanelWidget::GetProcessor()
{
	return m_Processor.GetPointer( );
}

