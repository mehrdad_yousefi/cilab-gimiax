/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _BooleanOperationsPanelWidget_H
#define _BooleanOperationsPanelWidget_H

#include "BooleanOperationsProcessor.h"
#include "BooleanOperationsPanelWidgetUI.h"

// CoreLib
#include "coreRenderingTree.h"
#include "corePointInteractorPointSelect.h"
#include "coreProcessingWidget.h"



namespace Core{ namespace Widgets {
	class UserHelper;
	class DataEntityListBrowser;
}}
	

/**


\ingroup MeshEditorPlugin
\author Valeria Barbarito
\date July 2011
*/
class BooleanOperationsPanelWidget : 
public BooleanOperationsPanelWidgetUI,
public Core::Widgets::ProcessingWidget 
{

// OPERATIONS
public:
	//!
	coreDefineBaseWindowFactory( BooleanOperationsPanelWidget );
	
	//!
	BooleanOperationsPanelWidget(wxWindow* parent, int id= wxID_ANY,
		const wxPoint&  pos = wxDefaultPosition, 
		const wxSize&  size = wxDefaultSize, 
		long style = 0);

	//!
	~BooleanOperationsPanelWidget( );

	//! Add button events to the bridge and call UpdateWidget()
	void OnInit(  );
	
	//!
	bool Enable( bool enable /*= true */ );
	
	//!
	Core::BaseProcessor::Pointer GetProcessor( );

private:
	//! Update GUI from working data
	void UpdateWidget();

	//! Update working data from GUI
	void UpdateData();

	//! Button has been pressed
	void OnBtnSubtract(wxCommandEvent& event);
	//! Button has been pressed
	void OnBtnUnion(wxCommandEvent& event);
	//! Button has been pressed
	void OnBtnIntersection(wxCommandEvent& event);

	//!
	void UpdateHelperWidget( );

	//!
	void OnModifiedInputDataEntity();

	//!
	void OnModifiedOutputDataEntity();
	//!

	
// ATTRIBUTES
private:
	//! Working data of the processor
	BooleanOperationsProcessor::Pointer m_Processor;
};


#endif //_BooleanOperationsPanelWidget_H
