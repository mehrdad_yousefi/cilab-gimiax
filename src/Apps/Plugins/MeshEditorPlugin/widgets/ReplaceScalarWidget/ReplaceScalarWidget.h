/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef ReplaceScalarWidget_H
#define ReplaceScalarWidget_H

#include <wx/wx.h>
#include <wx/image.h>

#include "corePluginMacros.h"
#include "coreProcessingWidget.h"
#include "ReplaceScalarWidgetUI.h"
#include "ReplaceScalarProcessor.h"

/**
\brief Replace scalar value on a specific scalar array
\ingroup MeshEditorPlugin
\author Xavi Planes
\date Mar 2013
*/
class PLUGIN_EXPORT ReplaceScalarWidget: 
	public ReplaceScalarWidgetUI,
	public Core::Widgets::ProcessingWidget 
{
public:

	coreDefineBaseWindowFactory( ReplaceScalarWidget )

	//!
    ReplaceScalarWidget(wxWindow* parent, 
									int id = wxID_ANY, 
									const wxPoint& pos=wxDefaultPosition, 
									const wxSize& size=wxDefaultSize, 
									long style=0);


	//!
	Core::BaseProcessor::Pointer GetProcessor( );
	//!
	void UpdateData();
	//!
	void UpdateWidget();

private:
	//!
	void OnButtonApply(wxCommandEvent &event);

	//!
	void OnModifiedInput( int val );

protected:

    wxDECLARE_EVENT_TABLE();

private:
	//!
	ReplaceScalarProcessor::Pointer m_processor;
};


#endif // ReplaceScalarWidget_H

