/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "ReplaceScalarWidget.h"

#include "vtkCellData.h"

ReplaceScalarWidget::ReplaceScalarWidget(
    wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    ReplaceScalarWidgetUI(parent, id, pos, size, style)
{
    m_ScalarArrayControl->ShowAdvancedControls( false );

    m_processor = ReplaceScalarProcessor::New();
    
    UpdateWidget( );
}


BEGIN_EVENT_TABLE(ReplaceScalarWidget, ReplaceScalarWidgetUI)
END_EVENT_TABLE();


void ReplaceScalarWidget::OnButtonApply(wxCommandEvent &event)
{
    UpdateData( );

    UpdateProcessor( false );
}

Core::BaseProcessor::Pointer ReplaceScalarWidget::GetProcessor()
{
    return m_processor.GetPointer();
}

void ReplaceScalarWidget::UpdateData()
{
    double val;
    m_txtNewValue->GetValue( ).ToDouble( &val );
    m_processor->SetScalarValue( val );

    blShapeUtils::ShapeUtils::VTKScalarType scalarName;
    scalarName = m_ScalarArrayControl->GetCurrentSelectedScalarType( );
    m_processor->SetSelectedScalarArray( scalarName );
}

void ReplaceScalarWidget::UpdateWidget()
{
    m_txtNewValue->SetValue( wxString::Format( "%2f", m_processor->GetScalarValue( ) ) );
}


void ReplaceScalarWidget::OnModifiedInput( int val )
{
    if ( val == ReplaceScalarProcessor::INPUT_SURFACE )
    {
        try
        {
            Core::vtkPolyDataPtr mesh;
            m_processor->GetProcessingData( ReplaceScalarProcessor::INPUT_SURFACE, mesh );

            std::vector<blShapeUtils::ShapeUtils::VTKScalarType> scalarnames;
            blShapeUtils::ShapeUtils::GetScalarsVector( mesh, scalarnames );

            // Update scalar arrays and select first one
            m_ScalarArrayControl->SetScalarArrayTypes( scalarnames, true );

            // Get metadata active array
            std::string arrayName;
            blTagMap::Pointer metadata = static_cast<blTagMap::Pointer>( m_processor->GetInputDataEntity( ReplaceScalarProcessor::INPUT_SURFACE )->GetMetadata( ) );
            if ( metadata.IsNotNull( ) )
            {
                blTag::Pointer tag = metadata->FindTagByName( "scalar name" );
                if ( tag.IsNotNull( ) )
                {
                    arrayName = tag->GetValueCasted<std::string>( );
                }
            }

            // Get active array
            if ( arrayName.empty( ) )
            {
                if ( mesh->GetPointData( ) && mesh->GetPointData( )->GetScalars( ) )
                {
                    arrayName = mesh->GetPointData( )->GetScalars( )->GetName( );
                }
                else if ( mesh->GetCellData( ) && mesh->GetCellData( )->GetScalars( ) )
                {
                    arrayName = mesh->GetCellData( )->GetScalars( )->GetName( );
                }
            }

            // Select default one
            if ( scalarnames.size( ) && arrayName.empty( ) )
            {
                arrayName = scalarnames[ 0 ].name;
            }

            if ( !arrayName.empty( ) )
            {
                m_ScalarArrayControl->SetSelectedArray( arrayName.c_str( ) );
            }
        }
        catch(...)
        {
            std::vector<blShapeUtils::ShapeUtils::VTKScalarType> scalarnames;
            m_ScalarArrayControl->SetScalarArrayTypes( scalarnames, true );
        }
    }
}
