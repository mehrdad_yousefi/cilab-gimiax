// -*- C++ -*- generated by wxGlade 0.6.5 (standalone edition) on Fri Aug 03 17:00:24 2012

#include "ptMeshStatisticsPanelWidgetUI.h"

// begin wxGlade: ::extracode

// end wxGlade


ptMeshStatisticsPanelWidgetUI::ptMeshStatisticsPanelWidgetUI(wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    wxPanel(parent, id, pos, size, wxTAB_TRAVERSAL)
{
    // begin wxGlade: ptMeshStatisticsPanelWidgetUI::ptMeshStatisticsPanelWidgetUI
    sizer1_staticbox = new wxStaticBox(this, -1, wxT("Mesh statistics"));
    ckb_AspectRatio = new wxCheckBox(this, wxID_ANY, wxT("Aspect Ratio"));
    ckb_MinAngle = new wxCheckBox(this, wxID_ANY, wxT("Min Angle"));
    ckb_EgdeRatio = new wxCheckBox(this, wxID_ANY, wxT("Edge Ratio"));
    m_labelNumberofBins = new wxStaticText(this, wxID_ANY, wxT("Number of bins"));
    m_textNumberofBins = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
    m_btnCompute = new wxButton(this, wxID_COMPUTE, wxT("Compute"));

    set_properties();
    do_layout();
    // end wxGlade
}


BEGIN_EVENT_TABLE(ptMeshStatisticsPanelWidgetUI, wxPanel)
    // begin wxGlade: ptMeshStatisticsPanelWidgetUI::event_table
    EVT_CHECKBOX(wxID_ANY, ptMeshStatisticsPanelWidgetUI::OnAspectRatioChecked)
    EVT_CHECKBOX(wxID_ANY, ptMeshStatisticsPanelWidgetUI::OnAspectRatioChecked)
    EVT_CHECKBOX(wxID_ANY, ptMeshStatisticsPanelWidgetUI::OnEdgeRatioChecked)
    EVT_BUTTON(wxID_COMPUTE, ptMeshStatisticsPanelWidgetUI::OnComputeBtn)
    // end wxGlade
END_EVENT_TABLE();


void ptMeshStatisticsPanelWidgetUI::OnAspectRatioChecked(wxCommandEvent &event)
{
    event.Skip();
    wxLogDebug(wxT("Event handler (ptMeshStatisticsPanelWidgetUI::OnAspectRatioChecked) not implemented yet")); //notify the user that he hasn't implemented the event handler yet
}


void ptMeshStatisticsPanelWidgetUI::OnEdgeRatioChecked(wxCommandEvent &event)
{
    event.Skip();
    wxLogDebug(wxT("Event handler (ptMeshStatisticsPanelWidgetUI::OnEdgeRatioChecked) not implemented yet")); //notify the user that he hasn't implemented the event handler yet
}


void ptMeshStatisticsPanelWidgetUI::OnComputeBtn(wxCommandEvent &event)
{
    event.Skip();
    wxLogDebug(wxT("Event handler (ptMeshStatisticsPanelWidgetUI::OnComputeBtn) not implemented yet")); //notify the user that he hasn't implemented the event handler yet
}


// wxGlade: add ptMeshStatisticsPanelWidgetUI event handlers


void ptMeshStatisticsPanelWidgetUI::set_properties()
{
    // begin wxGlade: ptMeshStatisticsPanelWidgetUI::set_properties
    // end wxGlade
}


void ptMeshStatisticsPanelWidgetUI::do_layout()
{
    // begin wxGlade: ptMeshStatisticsPanelWidgetUI::do_layout
    wxBoxSizer* GlobalSizer = new wxBoxSizer(wxVERTICAL);
    wxStaticBoxSizer* sizer1 = new wxStaticBoxSizer(sizer1_staticbox, wxVERTICAL);
    wxBoxSizer* sizer_1 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* sizer_2_copy_1 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* sizer_2_copy = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* sizer_2 = new wxBoxSizer(wxHORIZONTAL);
    sizer_2->Add(ckb_AspectRatio, 1, wxEXPAND, 0);
    sizer1->Add(sizer_2, 0, wxEXPAND, 0);
    sizer_2_copy->Add(ckb_MinAngle, 1, wxEXPAND, 0);
    sizer1->Add(sizer_2_copy, 0, wxEXPAND, 0);
    sizer_2_copy_1->Add(ckb_EgdeRatio, 1, wxEXPAND, 0);
    sizer1->Add(sizer_2_copy_1, 0, wxEXPAND, 0);
    sizer_1->Add(m_labelNumberofBins, 1, wxEXPAND, 0);
    sizer_1->Add(m_textNumberofBins, 0, 0, 5);
    sizer1->Add(sizer_1, 0, wxEXPAND, 0);
    sizer1->Add(m_btnCompute, 0, wxALL|wxALIGN_RIGHT, 5);
    GlobalSizer->Add(sizer1, 1, wxALL|wxEXPAND, 5);
    SetSizer(GlobalSizer);
    GlobalSizer->Fit(this);
    // end wxGlade
}

