// Copyright 2009 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "ptMeshStatisticsPanelWidget.h"

ptMeshStatisticsPanelWidget::ptMeshStatisticsPanelWidget(
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    ptMeshStatisticsPanelWidgetUI(parent, id, pos, size, wxTAB_TRAVERSAL)
{
	ckb_AspectRatio->SetValue( true );
	ckb_MinAngle->SetValue( true );
	ckb_EgdeRatio->SetValue( true );

	m_processor = Core::MeshStatisticsProcessor::New();

	UpdateWidget();
		
}

void ptMeshStatisticsPanelWidget::UpdateData()
{	
    m_processor->SetNumberofBins(std::atoi(m_textNumberofBins->GetValue()));

	Core::QualityStringMap::iterator it;
	Core::Quality* q;
	it = m_processor->qualityStringMap.find("Tetra Aspect Ratio");	
	q = (*it).second;
	q->m_selected = ckb_AspectRatio->GetValue();

	it = m_processor->qualityStringMap.find("Tetra Min Angle");	
	q = (*it).second;
	q->m_selected = ckb_MinAngle->GetValue();

	it = m_processor->qualityStringMap.find("Tetra Edge Ratio");	
	q = (*it).second;
	q->m_selected =ckb_EgdeRatio->GetValue();
}

void ptMeshStatisticsPanelWidget::UpdateWidget()
{
	// initialize numofbins
	m_textNumberofBins->SetValue(wxString::Format("%d",m_processor->GetNumberofBins()));
	Core::QualityStringMap::iterator it;
}

void ptMeshStatisticsPanelWidget::OnComputeBtn(wxCommandEvent &event)
{
	UpdateData();
	UpdateProcessor();
	UpdateWidget();
}
Core::BaseProcessor::Pointer ptMeshStatisticsPanelWidget::GetProcessor()
{
	return m_processor.GetPointer();
}

void ptMeshStatisticsPanelWidget::OnInit()
{
	GetProcessorOutputObserver(0)->SetHideInput(false);
	GetProcessorOutputObserver(0)->SelectDataEntity(true);
	GetProcessorOutputObserver(1)->SetHideInput(false);
	GetProcessorOutputObserver(1)->SelectDataEntity(false);
}
