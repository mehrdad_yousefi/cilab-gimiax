/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreptMeshStatisticsPanelWidget_H
#define coreptMeshStatisticsPanelWidget_H

#include <wx/wx.h>
#include <wx/image.h>

#include "corePluginMacros.h"
#include "coreProcessingWidget.h"
#include "coreMeshStatisticsProcessor.h"

#include "ptMeshStatisticsPanelWidgetUI.h"

/**
\brief Pw to compute tetra statistics
\ingroup MeshEditorPlugin
\author Chiara Riccobene
\date 13 Jan 10
*/
class PLUGIN_EXPORT ptMeshStatisticsPanelWidget: 
	public ptMeshStatisticsPanelWidgetUI,
	public Core::Widgets::ProcessingWidget 
{
public:
	coreDefineBaseWindowFactory( ptMeshStatisticsPanelWidget )

    ptMeshStatisticsPanelWidget(wxWindow* parent, 
								int id = wxID_ANY, 
								const wxPoint& pos=wxDefaultPosition, 
								const wxSize& size=wxDefaultSize, 
								long style=0);

private:

	//!
	Core::BaseProcessor::Pointer GetProcessor( );
	
	//!
	void UpdateData();
	void UpdateWidget();
	void OnInit();

protected:

    virtual void OnComputeBtn(wxCommandEvent &event);
private:
	//!
	Core::MeshStatisticsProcessor::Pointer m_processor;
}; 


#endif // ptMeshStatisticsPanelWidget_H
