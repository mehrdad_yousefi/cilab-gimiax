/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef ManualNeckCuttingPanelWidget_H
#define ManualNeckCuttingPanelWidget_H

#include "coreManualNeckCuttingProcessor.h"
#include "ManualNeckCuttingPanelWidgetUI.h"

// CoreLib
#include "coreRenderingTree.h"
#include "coreProcessingWidget.h"

// GuiBridgeLib
#include "gblWxConnectorOfWidgetChangesToSlotFunction.h"

namespace Core{ namespace Widgets {
	class UserHelper;
	class DataEntityListBrowser;
	class LandmarkSelectorWidget;
	class DataEntityListBrowser;
}}


	
/**
PanelWidget for interacting with ManualNeckCuttingProcessor.

\ingroup MeshEditorPlugin
\author Chiara Ricobenne
\date 24 oct 2008
*/
class ManualNeckCuttingPanelWidget : 
	public ManualNeckCuttingPanelWidgetUI,
	public Core::Widgets::ProcessingWidget
{

// OPERATIONS
public:
	coreDefineBaseWindowFactory( ManualNeckCuttingPanelWidget )
	//!
	ManualNeckCuttingPanelWidget(
		wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

	//!
	~ManualNeckCuttingPanelWidget( );

	//! Add button events to the bridge and call UpdateWidget()
	void OnInit();

	//!
	Core::BaseProcessor::Pointer GetProcessor( );
	
	//!
	bool Enable( bool enable /*= true */ );

private:

	//! Update GUI from working data
	void UpdateWidget();

	//! Validate GUI data
	bool Validate();

	//! Button has been pressed
	void OnBtnCut(wxCommandEvent& event);

	//!
	void OnSwapOutputMesh(wxCommandEvent &event);

	//!
    void OnFinish(wxCommandEvent &event);

	//!
	void PublishOneOutputMesh();

	//!
	void UpdateHelperWidget( );

	//!
	void OnModifiedInputDataEntity();

	//!
	void OnModifiedSelectedPoint();

	//!
	void OnModifiedOutputDataEntity();

	//!
	void SwapSacAndVessel(int sacMeshIndex, int vesselMeshIndex);

	//!
	void OnBtnUndo(wxCommandEvent& event);

	//!
	void OnStart(wxCommandEvent &event);

	//! do the processor update using the specified choice for userminor..
	void DoApply(bool useminornumberofholesguess);

	
// ATTRIBUTES
private:
	//! Working data of the processor
	Core::ManualNeckCuttingProcessor::Pointer m_Processor;

	bool m_useMinorNumberOfHoles;
};

#endif //_ManualNeckCuttingPanelWidget_H
