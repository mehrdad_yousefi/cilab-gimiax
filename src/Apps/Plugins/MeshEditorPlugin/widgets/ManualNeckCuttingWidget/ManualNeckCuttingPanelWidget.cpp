/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "ManualNeckCuttingPanelWidget.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"
// Core
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"
#include "coreDataEntityListBrowser.h"
#include "coreReportExceptionMacros.h"
#include "coreProcessorManager.h"
// Core
#include "coreProcessorInputWidget.h"
#include "coreDataTreeHelper.h"
#include "coreDataTreeMITKHelper.h"
#include "coreLandmarkSelectorWidget.h"
#include "mitkMaterialProperty.h"


ManualNeckCuttingPanelWidget::ManualNeckCuttingPanelWidget( 
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style )
: ManualNeckCuttingPanelWidgetUI(parent, id, pos, size, style)
{
	m_Processor = Core::ManualNeckCuttingProcessor::New( );
	m_useMinorNumberOfHoles = true;

	m_btnUndo->Disable();
	m_btnCut->Disable();
	m_btnSwap->Disable();
	m_btnStart->Enable();
	m_btnFinish->Disable();
}

ManualNeckCuttingPanelWidget::~ManualNeckCuttingPanelWidget( )
{
	// We don't need to destroy anything because all the child windows 
	// of this wxWindow are destroyed automatically
}

void ManualNeckCuttingPanelWidget::OnInit( )
{
	GetInputWidget( Core::ManualNeckCuttingProcessor::INPUT_POINT )->Hide();

	GetInputWidget( Core::ManualNeckCuttingProcessor::INPUT_ANEURYSM )->SetAutomaticSelection( false );
	GetInputWidget( Core::ManualNeckCuttingProcessor::INPUT_ANEURYSM)->SetDefaultDataEntityFlag( false );

	GetProcessorOutputObserver(Core::ManualNeckCuttingProcessor::LOOP_POINTS )->SetHideInput(false);
	GetProcessorOutputObserver(Core::ManualNeckCuttingProcessor::OUTPUT_MESH1 )->SetHideInput(false);
	GetProcessorOutputObserver(Core::ManualNeckCuttingProcessor::OUTPUT_MESH2 )->SetHideInput(false);


	m_Processor->GetOutputDataEntityHolder( Core::ManualNeckCuttingProcessor::OUTPUT_MESH1 )->AddObserver( 
		this, 
		&ManualNeckCuttingPanelWidget::OnModifiedOutputDataEntity );

	m_Processor->GetInputDataEntityHolder( Core::ManualNeckCuttingProcessor::INPUT_ANEURYSM )->AddObserver( 
		this, 
		&ManualNeckCuttingPanelWidget::OnModifiedInputDataEntity );

	m_Processor->GetInputDataEntityHolder( Core::ManualNeckCuttingProcessor::INPUT_POINT )->AddObserver( 
		this, 
		&ManualNeckCuttingPanelWidget::OnModifiedSelectedPoint );

	UpdateWidget();
}

void ManualNeckCuttingPanelWidget::UpdateWidget()
{
	// Check that the working data is initialized
	if( !m_Processor )
	{
		Enable(false);
		return;
	}

	// Disable automatic update of the widget to avoid recursively calls
	Validate();
}

bool ManualNeckCuttingPanelWidget::Validate()
{
	bool okay = true;

	// Validate each text control. Pending
	return okay;
}


void ManualNeckCuttingPanelWidget::OnBtnCut(wxCommandEvent& event)
{
	m_useMinorNumberOfHoles = true;
	DoApply(m_useMinorNumberOfHoles);
	m_btnUndo->Disable();
	m_btnCut->Disable();
	m_btnSwap->Enable();
	m_btnStart->Enable();
	m_btnFinish->Enable( );
}


void ManualNeckCuttingPanelWidget::DoApply(bool useminornumberofholesguess)
{
//	try
//	{
		GetProcessor( )->SetMultithreading( false );
		//Core::ProcessorManager::Pointer ProcessorManager;
		Core::Runtime::Kernel::GetProcessorManager()->Execute( GetProcessor( ) );

		m_Processor->SetUseMinorNumberOfHolesGuess(useminornumberofholesguess);
		m_Processor->Update( );


		Core::Widgets::LandmarkSelectorWidget * landmarkSelector;
		landmarkSelector = GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget >( "Landmark selector" );

		landmarkSelector->StopInteraction();

		landmarkSelector->SetDefaultAllowedInputDataTypes( );
//	}

//	coreCatchExceptionsReportAndNoThrowMacro( "ManualNeckCuttingPanelWidget::DoApply" );
}

void ManualNeckCuttingPanelWidget::PublishOneOutputMesh()
{
	if(!GetRenderingTree())
		return;

	Core::DataTreeHelper::PublishOutput(
		m_Processor->GetOutputDataEntityHolder( Core::ManualNeckCuttingProcessor::OUTPUT_MESH1 ), 
		GetRenderingTree(),
		true,
		false);
	
	mitk::DataTreeNode::Pointer nodeFather; 
	Core::CastAnyProcessingData( GetRenderingTree()->GetNode( m_Processor->GetInputDataEntity(Core::ManualNeckCuttingProcessor::INPUT_ANEURYSM) ), nodeFather );


	if(nodeFather.IsNotNull())
	{
		nodeFather->SetOpacity(0.4);
	}
	if ( !GetRenderingTree()->IsDataEntityRendered(m_Processor->GetInputDataEntity(Core::ManualNeckCuttingProcessor::INPUT_ANEURYSM)) )
	{
		GetRenderingTree()->Show(m_Processor->GetInputDataEntity(Core::ManualNeckCuttingProcessor::INPUT_ANEURYSM),true);
	}

	mitk::DataTreeNode::Pointer nodeComponent;
	Core::CastAnyProcessingData( GetRenderingTree()->GetNode( m_Processor->GetOutputDataEntity( Core::ManualNeckCuttingProcessor::OUTPUT_MESH1 ) ), nodeComponent );

	if(nodeComponent.IsNotNull())
	{
		mitk::MaterialProperty::Pointer materialProperty = mitk::MaterialProperty::New( 1.0, 0, 0, 1.0, nodeComponent );
		materialProperty->SetDiffuseColor(1, 0, 0);
		nodeComponent->ReplaceProperty("material", materialProperty);
		mitk::RenderingManager::GetInstance()->ForceImmediateUpdateAll();
	}
}

void ManualNeckCuttingPanelWidget::OnModifiedOutputDataEntity()
{
	try{

		// Add output to the data list and render it
		// After adding the output, the input will automatically be changed to
		// this one

 		Core::DataTreeHelper::PublishOutput(
 			m_Processor->GetOutputDataEntityHolder( Core::ManualNeckCuttingProcessor::LOOP_POINTS), 
 			GetRenderingTree(),
			true,
			false);

		PublishOneOutputMesh();
		
	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"CardiacInitializationPanelWidget::OnModifiedOutputDataEntity")

}

void ManualNeckCuttingPanelWidget::OnSwapOutputMesh(wxCommandEvent &event)
{
	
	if(m_Processor->GetOutputDataEntityHolder( Core::ManualNeckCuttingProcessor::OUTPUT_MESH1).IsNull() ||
		m_Processor->GetOutputDataEntityHolder( Core::ManualNeckCuttingProcessor::OUTPUT_MESH1)->GetSubject().IsNull())
	{
		return;
	}
	
	//remove the lain and points
	if(	m_Processor->GetOutputDataEntityHolder( Core::ManualNeckCuttingProcessor::LOOP_POINTS).IsNotNull() &&
		m_Processor->GetOutputDataEntityHolder( Core::ManualNeckCuttingProcessor::LOOP_POINTS)->GetSubject().IsNotNull())
		GetListBrowser()->GetDataEntityList()->Remove(
		m_Processor->GetOutputDataEntityHolder( Core::ManualNeckCuttingProcessor::LOOP_POINTS)->GetSubject()->GetId());


	//remove the output dataentity
	GetListBrowser()->GetDataEntityList()->Remove(
			m_Processor->GetOutputDataEntityHolder( Core::ManualNeckCuttingProcessor::OUTPUT_MESH1)->GetSubject()->GetId());


	m_useMinorNumberOfHoles = (!m_useMinorNumberOfHoles);
	DoApply(m_useMinorNumberOfHoles);
	PublishOneOutputMesh();
}

void ManualNeckCuttingPanelWidget::UpdateHelperWidget()
{
	if(GetHelperWidget()==NULL)
		return;

	std::string strText = "";
	if (m_Processor->GetInputDataEntity( 0 ).IsNull( ) )
	{
		strText = "Please select input data and press start neck selection";
	}
	else
	{
		strText = "Please press button Apply";
	}
	
	GetHelperWidget()->SetInfo( 
		Core::Widgets::HELPER_INFO_ONLY_TEXT, strText );
}

bool ManualNeckCuttingPanelWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = ManualNeckCuttingPanelWidgetUI::Enable( enable );

	Core::Widgets::LandmarkSelectorWidget * landmarkSelector;
	landmarkSelector = GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget >( "Landmark selector" );

	// If this panel widget is selected -> Update the widget
	if ( landmarkSelector )
	{
		if (( !enable ) && (landmarkSelector->IsSelectionEnabled()))
		{
			// Disconnect interactor
			landmarkSelector->StopInteraction();
			landmarkSelector->SetDefaultAllowedInputDataTypes( );
		}
	}
	if(enable)
	{
		m_btnUndo->Disable();
		m_btnCut->Disable();
		m_btnSwap->Disable();
		m_btnStart->Enable();
		m_btnFinish->Disable();
	}

	return bReturn;
}

void ManualNeckCuttingPanelWidget::OnModifiedInputDataEntity()
{
	m_useMinorNumberOfHoles = true;

	m_btnUndo->Disable();
	m_btnCut->Disable();
	m_btnSwap->Disable();
	m_btnFinish->Disable();
	m_btnStart->Enable();

	UpdateHelperWidget();
	
}

void ManualNeckCuttingPanelWidget::OnModifiedSelectedPoint()
{
	// Add a point to the INPUT_POINT_SET data entity
	if ( m_Processor->GetInputDataEntity( Core::ManualNeckCuttingProcessor::INPUT_POINT ).IsNull() )
		return;

	Core::vtkPolyDataPtr point;
	Core::DataEntityHelper::GetProcessingData (
		m_Processor->GetInputDataEntityHolder( Core::ManualNeckCuttingProcessor::INPUT_POINT ),
		point);

	if (point->GetNumberOfPoints() == 0 )
		return;
	int n = point->GetNumberOfPoints();

	Core::DataTreeMITKHelper::ChangeShowLabelsProperty(
		m_Processor->GetInputDataEntity( Core::ManualNeckCuttingProcessor::INPUT_POINT ),
		GetRenderingTree(),
		false);
	n = point->GetNumberOfPoints();

}


void ManualNeckCuttingPanelWidget::OnBtnUndo(wxCommandEvent& event)
{
	Core::Widgets::LandmarkSelectorWidget * landmarkSelector;
	landmarkSelector = GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget >( "Landmark selector" );

	landmarkSelector->StopInteraction();
	Core::vtkPolyDataPtr landmarks;
	vtkDataArray* points;
	Core::DataEntityHelper::GetProcessingData (
		m_Processor->GetInputDataEntityHolder( Core::ManualNeckCuttingProcessor::INPUT_POINT ),
		landmarks);

	if (landmarks->GetPoints() != NULL)
	{
		points = landmarks->GetPoints()->GetData();
		points ->RemoveLastTuple();
		m_Processor->GetInputDataEntityHolder( Core::ManualNeckCuttingProcessor::INPUT_POINT )->NotifyObservers();	
	}
	landmarkSelector->StartInteractor();
}

Core::BaseProcessor::Pointer ManualNeckCuttingPanelWidget::GetProcessor()
{
	return m_Processor.GetPointer();
}

void  ManualNeckCuttingPanelWidget::OnStart(wxCommandEvent &event)
{
	Core::Widgets::LandmarkSelectorWidget * landmarkSelector;
	landmarkSelector = GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget >( "Landmark selector" );

	if ( landmarkSelector )
	{
		// Connect interactor
		landmarkSelector->SetAllowedInputDataTypes( Core::SurfaceMeshTypeId );
		landmarkSelector->SetInteractorType(Core::PointInteractor::POINT_SET);
		landmarkSelector->SetDataName( "Manual cut landmarks" );
		landmarkSelector->SetInputDataEntity(m_Processor->GetInputDataEntity(Core::ManualNeckCuttingProcessor::INPUT_ANEURYSM));
		landmarkSelector->StartInteractor();

		UpdateWidget();

		m_btnFinish->Disable();
		m_btnUndo->Enable();
		m_btnCut->Enable();
		m_btnSwap->Disable();
		m_btnStart->Disable();
	}
}

void  ManualNeckCuttingPanelWidget::OnFinish(wxCommandEvent &event)
{
	// Clean all outputs
	Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
	Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();
	if (m_Processor->GetInputDataEntity( Core::ManualNeckCuttingProcessor::INPUT_POINT ).IsNotNull())
		list->Remove( m_Processor->GetInputDataEntity( Core::ManualNeckCuttingProcessor::INPUT_POINT ) );

	if (m_Processor->GetOutputDataEntity( Core::ManualNeckCuttingProcessor::LOOP_POINTS ).IsNotNull())
		list->Remove( m_Processor->GetOutputDataEntity( Core::ManualNeckCuttingProcessor::LOOP_POINTS ) );
	if (m_Processor->GetOutputDataEntity( Core::ManualNeckCuttingProcessor::OUTPUT_MESH2 ).IsNotNull())
		list->Remove( m_Processor->GetOutputDataEntity( Core::ManualNeckCuttingProcessor::OUTPUT_MESH2 ) );

	m_btnFinish->Disable();
}

