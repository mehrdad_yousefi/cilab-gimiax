/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef VolumeClosingPanelWidget_H
#define VolumeClosingPanelWidget_H

#include <wx/wx.h>
#include <wx/image.h>

#include "VolumeClosingPanelWidgetUI.h"
#include "corePluginMacros.h"
#include "coreProcessingWidget.h"
#include "coreVTKProcessor.h"
#include "meVTKVolumeClosingFilter.h"
#include "coreCloseHolesProcessor.h"
#include "corePointInteractorPointSelect.h"

//#define wxID_btnGlobalVolumeClosing wxID("Button GlobalVolume")
//#define wxID_btnUndo wxID("Button Undo")
//#define wxID_CHOOSEHOLE wxID("ChooseHole")
//#define wxID_COMBOMETHODS wxID("ComboMethods")
//#define wxID_COMBOSMOOTHING wxID("ComboSmoothing")
//#define wxID_CHKCLOSEALL wxID("CloseAll")
//#define wxID_CHKSMOOTHING wxID("Smoothing")
//
#define wxID_VolumeClosingWidget wxID("wxID_VolumeClosingWidget")


/**
\brief represents a close operation
\ingroup MeshEditorPlugin
\author Albert Sanchez
\date 6 Apr 11
*/


struct CloseHoleOperation
{
	typedef enum
	{
		CLOSE_SURFACE,
		SHOW_PATCHES_ONLY
	} OPERATION_TYPE;	
	
	OPERATION_TYPE type;

	//ONLY CLOSE_SURFACE
	Core::vtkPolyDataPtr prevSurface;
	
	//ONLY SHOW_PATCHES_ONLY
	std::vector<int> dataEntityIds;

};


/**
\brief Pw for extract surface processor
\ingroup MeshEditorPlugin
\author Chiara Riccobene
\author Albert Sanchez
\date 5 Nov 09
*/


class PLUGIN_EXPORT VolumeClosingPanelWidget: 
	public VolumeClosingPanelWidgetUI,
	public Core::Widgets::ProcessingWidget {
public:

	//typedef Core::VTKProcessor<meVTKVolumeClosingFilter> VolumeClosingProcessor;

	coreDefineBaseWindowFactory( VolumeClosingPanelWidget )

	//!
    VolumeClosingPanelWidget(wxWindow* parent, 
							int id = wxID_VolumeClosingWidget,	
							const wxPoint& pos=wxDefaultPosition, 
							const wxSize& size=wxDefaultSize, 
							long style=wxDEFAULT_DIALOG_STYLE);

	void OnButtonFillHoles(wxCommandEvent &event); // wxGlade: <event_handler>
	void OnChooseHoles(wxCommandEvent &event); // wxGlade: <event_handler>
	void OnComboboxChoice(wxCommandEvent &event);
	void OnFillAllHolesChecked( wxCommandEvent &event );
	void OnSmoothChecked( wxCommandEvent &event );
	void ChooseHoles();
	void DoCancel();
	
	void OnButtonUndo(wxCommandEvent &event);

	void UpdateData();
	void UpdateWidget();

	void OnModifiedInputDataEntity();
	void OnModifiedSelection();
	void OnInit();

	void ConnectInteractor();
	void DisconnectInteractor();

private:
 //   void do_layout();
	//void set_properties();
    //! Enable the widget
	bool Enable( bool enable = true );
	//!
	Core::BaseProcessor::Pointer GetProcessor( );

protected:
    // begin wxGlade: ptVolumeClosingPanelWidget::attributes
	//wxStaticBox* m_GlobalVolumeClosing_staticbox;
	//wxStaticBox* sizer_2_staticbox;
	//wxCheckBox* checkbox_closeAll;
	//wxButton* btnChoosehole;
	//wxCheckBox* checkboxPatchOnly;
	//wxStaticText* labelmethod;
	//wxComboBox* combo_box_1;
	//wxCheckBox* checkbox_1;
	//wxComboBox* combo_box_2;
	//wxTextCtrl* text_ctrl_1;
	//wxStaticText* label_1;
	//wxButton* btnGlobalVolumeClosing;
	//wxButton* btnUndo;
	//wxButton* btnStart;
    // end wxGlade

	//wxStaticBoxSizer* sizer_2;

 /*  DECLARE_EVENT_TABLE();*/

private:

	//!
	//VolumeClosingProcessor::Pointer m_processor;
	Core::CloseHolesProcessor::Pointer m_processor;

	Core::DataEntityHolder::Pointer m_HolesHolder;

	Core::PointInteractorPointSelect* m_pointSelectInteractor;

	bool m_ConnectingInteractor;

	std::deque<CloseHoleOperation> m_CloseHoleOperations;

	bool m_chooseHolesEnabled;

	bool m_fillHolesEnabled;

}; // wxGlade: end class


#endif // ptVolumeClosingPanelWidget_H
