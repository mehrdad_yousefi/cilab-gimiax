// Copyright 2009 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "VolumeClosingPanelWidget.h"
#include "coreProcessorInputWidget.h"
#include "mitkMaterialProperty.h"
#include "mitkProperties.h"
#include "coreDataTreeMITKHelper.h"



VolumeClosingPanelWidget::VolumeClosingPanelWidget(
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style)
:   VolumeClosingPanelWidgetUI(parent, id, pos, size, style)
{
    m_ConnectingInteractor = false;
	m_fillHolesEnabled = false;
	m_HolesHolder = Core::DataEntityHolder::New();

	m_processor = Core::CloseHolesProcessor::New();

	m_processor->GetInputDataEntityHolder(Core::CloseHolesProcessor::INPUT_POINT)->AddObserver( 
	   this, 
	   &VolumeClosingPanelWidget::OnModifiedSelection );

	m_chooseHolesEnabled = false;
	  
	//set_properties();
	UpdateWidget();
}

void VolumeClosingPanelWidget::UpdateData()
{
	m_processor->SetCloseAll(checkbox_closeAll->GetValue());
	m_processor->SetMethod(combo_box_1->GetSelection());
	m_processor->SetPatchOnly(checkboxPatchOnly->GetValue());

	m_processor->SetFillingType(
		checkbox_1->GetValue(), 
		combo_box_2->GetSelection() );
	
	long nStepsThinPlate;
	text_ctrl_1->GetValue().ToLong(&nStepsThinPlate);

	m_processor->SetThinPlateSteps( nStepsThinPlate );
}

void VolumeClosingPanelWidget::OnInit()
{
	//GetInputWidget(Core::CloseHolesProcessor::INPUT_SURFACE)->Hide();
	//GetInputWidget(Core::CloseHolesProcessor::INPUT_SURFACE)->SetAutomaticSelection(true);
	GetInputWidget(Core::CloseHolesProcessor::INPUT_SURFACE)->SetAutomaticSelection(false);
	GetInputWidget(Core::CloseHolesProcessor::INPUT_SURFACE)->SetDefaultDataEntityFlag(true);

	GetInputWidget(Core::CloseHolesProcessor::INPUT_POINT)->Hide();
	GetInputWidget(Core::CloseHolesProcessor::INPUT_POINT)->SetAutomaticSelection(false);
	GetInputWidget(Core::CloseHolesProcessor::INPUT_POINT)->SetDefaultDataEntityFlag(false);

	GetProcessorOutputObserver(Core::CloseHolesProcessor::OUTPUT_HOLE )->SetHideInput(false);
	GetProcessorOutputObserver( Core::CloseHolesProcessor::SURFACE_HOLES )->SetHideInput(false);
	GetProcessorOutputObserver( Core::CloseHolesProcessor::SURFACE_CLOSED )->SetHideInput( false );
}

void VolumeClosingPanelWidget::UpdateWidget()
{
	btnChoosehole->SetValue( m_chooseHolesEnabled );
	std::string label = m_chooseHolesEnabled ? "Cancel " : "Choose Holes (SHIFT + click) ";
	btnChoosehole->SetLabel(_U(label));
	
	btnChoosehole->Enable(!checkbox_closeAll->GetValue());

	sizer_2_staticbox->Show(combo_box_1->GetSelection() == 6 );
	combo_box_2->Show(combo_box_1->GetSelection() == 6 );
	label_1->Show(combo_box_1->GetSelection() == 6 );
	text_ctrl_1->Show(combo_box_1->GetSelection() == 6 );
	checkbox_1->Show(combo_box_1->GetSelection() == 6 );

	if (checkbox_closeAll->GetValue())
		btnGlobalVolumeClosing->SetLabel("Close Volume");
	else
		btnGlobalVolumeClosing->SetLabel("Fill Hole");

	if ( m_fillHolesEnabled || checkbox_closeAll->GetValue())
		btnGlobalVolumeClosing->Enable();
	else
		btnGlobalVolumeClosing->Disable();
	
	std::stringstream ss;
	ss<<"Undo (" << m_CloseHoleOperations.size() << ")";
	btnUndo->SetLabel(ss.str());
	btnUndo->Enable(m_CloseHoleOperations.size()>0);

	checkboxPatchOnly->Enable(combo_box_1->GetSelection() != 6);
	
	text_ctrl_1->Enable( combo_box_2->GetSelection() == 0 && checkbox_1->GetValue());
	combo_box_2->Enable( checkbox_1->GetValue() );

	Layout();

} 

void VolumeClosingPanelWidget::OnButtonFillHoles(wxCommandEvent &event)
{
	if  ( !checkbox_closeAll->GetValue() && m_processor->GetInputHole() == NULL ) return;

	
	Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
	Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();

	Core::DataEntity::Pointer prevSelectedDataEntity;
	prevSelectedDataEntity = m_processor->GetInputDataEntity(Core::CloseHolesProcessor::INPUT_SURFACE);

	//GetInputWidget((Core::CloseHolesProcessor::INPUT_SURFACE))->SetAutomaticSelection(true);
	//GetInputWidget(Core::CloseHolesProcessor::INPUT_SURFACE)->SetAutomaticSelection(true);
	//GetInputWidget(Core::CloseHolesProcessor::INPUT_SURFACE)->SetDefaultDataEntityFlag(true);

	UpdateData();

	// Set output data as input to reuse it
	m_processor->GetOutputDataEntityHolder( 
		Core::CloseHolesProcessor::SURFACE_CLOSED )->SetEnableNotification( false );
	m_processor->SetOutputDataEntity(
		Core::CloseHolesProcessor::SURFACE_CLOSED, 
		m_processor->GetInputDataEntity( Core::CloseHolesProcessor::INPUT_SURFACE ) );
	m_processor->GetOutputDataEntityHolder( 
		Core::CloseHolesProcessor::SURFACE_CLOSED )->SetEnableNotification( true );


	// Push current state
	if ( checkboxPatchOnly->GetValue() == false ) //no patches
	{
		CloseHoleOperation operation;
		operation.type = CloseHoleOperation::CLOSE_SURFACE;
		
		Core::vtkPolyDataPtr processingData;
		m_processor->GetInputDataEntity(
			Core::CloseHolesProcessor::INPUT_SURFACE )->GetProcessingData(processingData);
		operation.prevSurface = Core::vtkPolyDataPtr::New();
		operation.prevSurface->DeepCopy(processingData);
		m_CloseHoleOperations.push_front(operation);
	}

	UpdateProcessor( false ); //m_processor->Update();

	DisconnectInteractor( );


	if (m_processor->GetInputDataEntity( Core::CloseHolesProcessor::INPUT_POINT ).IsNotNull())
	{
		list->Remove( m_processor->GetInputDataEntity( Core::CloseHolesProcessor::INPUT_POINT ) );
	}
	if (m_processor->GetOutputDataEntity( Core::CloseHolesProcessor::SURFACE_HOLES ).IsNotNull())
	{
		list->Remove( m_processor->GetOutputDataEntity( Core::CloseHolesProcessor::SURFACE_HOLES ) );
	}

	m_chooseHolesEnabled = false;
	m_fillHolesEnabled = false;
	UpdateWidget();
}

void VolumeClosingPanelWidget::OnButtonUndo(wxCommandEvent &event)
{
	if (m_CloseHoleOperations.size() == 0) return;

	CloseHoleOperation lastOperation = m_CloseHoleOperations[0];

	m_CloseHoleOperations.pop_front();
	
	if (lastOperation.type==CloseHoleOperation::CLOSE_SURFACE)
	{
		m_processor->GetOutputDataEntityHolder( 
			Core::CloseHolesProcessor::SURFACE_CLOSED )->SetEnableNotification( false );
		m_processor->GetInputDataEntity( 0 )->SetTimeStep(lastOperation.prevSurface);
		m_processor->GetOutputDataEntityHolder( 
			Core::CloseHolesProcessor::SURFACE_CLOSED )->SetEnableNotification( true );
	}
	else if(lastOperation.type==CloseHoleOperation::SHOW_PATCHES_ONLY)
	{
		//Core::DataEntity::Pointer prevDataEntity = list->GetDataEntity(lastOperation.prevDataEntityId);

		//list->GetSelectedDataEntityHolder()->SetSubject(prevDataEntity);
		//if (GetRenderingTree()) 
		//{
		//	GetRenderingTree()->Show(prevDataEntity,true);


		//}

		//for (int i=0; i<lastOperation.dataEntityIds.size(); i++)
		//{
		//	list->Remove(lastOperation.dataEntityIds[i]);
		//}
	}

	mitk::RenderingManager::GetInstance()->RequestUpdateAll();

	UpdateWidget();
}


Core::BaseProcessor::Pointer VolumeClosingPanelWidget::GetProcessor()
{
	return m_processor.GetPointer();
}

bool VolumeClosingPanelWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = wxPanel::Enable( enable );

	try
	{
		const std::string helpStr = \
			"Volume Closing: global volume closing." \
			"\n\nUsage: select a density factor, angle, minimum edges" \
			"\nand order and then run the tool.";
		SetInfoUserHelperWidget( helpStr );
	}
	coreCatchExceptionsReportAndNoThrowMacro("VolumeClosingPanelWidget::Enable");

	return bReturn;
}

void VolumeClosingPanelWidget::DoCancel()
{
	//GetInputWidget(Core::CloseHolesProcessor::INPUT_SURFACE)->SetAutomaticSelection(false);
	//GetInputWidget(Core::CloseHolesProcessor::INPUT_SURFACE)->SetDefaultDataEntityFlag(false);
	m_chooseHolesEnabled = false;
	DisconnectInteractor( );

	Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
	Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();
	if (m_processor->GetOutputDataEntity( Core::CloseHolesProcessor::SURFACE_HOLES ).IsNotNull())
		list->Remove( m_processor->GetOutputDataEntity( Core::CloseHolesProcessor::SURFACE_HOLES ) );

	mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}

void VolumeClosingPanelWidget::OnChooseHoles( wxCommandEvent &event )
{
	if (m_processor->GetInputDataEntity( Core::CloseHolesProcessor::INPUT_SURFACE).IsNull())
		return;

	if ( btnChoosehole->GetValue() )
	{
		ChooseHoles();
	}
	else
	{
		DoCancel();	
	}
	UpdateWidget();
}


void VolumeClosingPanelWidget::ChooseHoles()
{
	//GetInputWidget(Core::CloseHolesProcessor::INPUT_SURFACE)->SetAutomaticSelection(false);
	//GetInputWidget(Core::CloseHolesProcessor::INPUT_SURFACE)->SetDefaultDataEntityFlag(false);	
	m_chooseHolesEnabled = true;
	m_processor->ComputeHoles();
	ConnectInteractor(  );

	UpdateWidget();
}

void VolumeClosingPanelWidget::OnComboboxChoice( wxCommandEvent &event )
{
	UpdateWidget();
}

void VolumeClosingPanelWidget::OnFillAllHolesChecked( wxCommandEvent &event )
{
	UpdateWidget();
}
void VolumeClosingPanelWidget::OnSmoothChecked( wxCommandEvent &event )
{
	UpdateWidget();
}

void VolumeClosingPanelWidget::ConnectInteractor()
{
	Core::DataEntity::Pointer surfaceHoles;
	surfaceHoles = m_processor->GetOutputDataEntity(Core::CloseHolesProcessor::SURFACE_HOLES);
	if(surfaceHoles.IsNull())
		return;

	m_ConnectingInteractor = true;
	Core::Widgets::LandmarkSelectorWidget* m_landmarkSelector;
	m_landmarkSelector = GetSelectionToolWidget<Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" );
	if(m_landmarkSelector)
	{
		m_landmarkSelector->SetAllowedInputDataTypes( Core::SkeletonTypeId );

		m_landmarkSelector->SetInteractorType( Core::PointInteractor::POINT_SELECT );
		m_landmarkSelector->SetDataName( "ChoosedHole" );
		m_landmarkSelector->SetInputDataEntity(surfaceHoles);
		m_landmarkSelector->StartInteractor();
		
		Core::PointInteractorPointSelect* pointSelectInteractor;
		pointSelectInteractor = static_cast<Core::PointInteractorPointSelect*> (
			m_landmarkSelector->GetPointInteractor( ).GetPointer( ));

		m_processor->SetInputDataEntity( Core::CloseHolesProcessor::INPUT_POINT,
			pointSelectInteractor->GetSelectedPointsDataEntity() );
	
		m_fillHolesEnabled = true;
		m_ConnectingInteractor = false;

		if(GetRenderingTree()) 
		{
			//GetRenderingTree()->Show(m_processor->GetInputDataEntity(Core::CloseHolesProcessor::INPUT_SURFACE),true);

			mitk::DataTreeNode::Pointer node; 
			Core::CastAnyProcessingData( 
				GetRenderingTree()->GetNode(m_processor->GetInputDataEntity (Core::CloseHolesProcessor::INPUT_SURFACE)),
				node );

			if(node.IsNotNull())
			{
				node->SetOpacity(0.5);
			}
		}

		GetMultiRenderWindow()->RequestUpdateAll();
	}

}

void VolumeClosingPanelWidget::DisconnectInteractor()
{
	Core::Widgets::LandmarkSelectorWidget* widget;
	widget = GetSelectionToolWidget<Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" );

	if ( widget == NULL )
	{
		return;
	}

	widget->StopInteraction();
	widget->SetDefaultAllowedInputDataTypes( );

	// Reset opacity to 1
	mitk::DataTreeNode::Pointer node; 
	boost::any anyData = GetRenderingTree()->GetNode(m_processor->GetInputDataEntity (Core::CloseHolesProcessor::INPUT_SURFACE));
	Core::CastAnyProcessingData( anyData, node );

	if(node.IsNotNull())
	{
		node->SetOpacity(1.0);
	}

}

void VolumeClosingPanelWidget::OnModifiedSelection()
{
	if((!IsShown()) || (!/*IsEnabled()*/IsThisEnabled()))
		return;

	if (m_ConnectingInteractor) return;

	//Core::DataTreeMITKHelper::ChangeShowLabelsProperty(
	//	m_processor->GetInputDataEntity(Core::CloseHolesProcessor::INPUT_POINT),
	//	GetRenderingTree(),
	//	false);

	if (m_processor->GetInputDataEntity(Core::CloseHolesProcessor::INPUT_POINT).IsNotNull())
	{
		m_processor->SelectHole();
	}
}
