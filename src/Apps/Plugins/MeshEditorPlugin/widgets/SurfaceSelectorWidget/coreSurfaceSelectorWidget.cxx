/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include "wx/wxprec.h"

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include "wx/string.h"
#include "wx/wupdlock.h"

#include <blMitkUnicode.h>

#include "blVTKHelperTools.h"
#include "blMITKUtils.h"

#include "coreSurfaceSelectorWidget.h"
#include "coreKernel.h"
#include "coreDataEntityHelper.h"
#include "coreMultiRenderWindow.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreInputControl.h"
#include "coreDataEntityListBrowser.h"
#include "coreReportExceptionMacros.h"
#include "coreDataTreeHelper.h"
#include "coreSurfaceInteractorTriangleSelect.h"
#include "coreSurfaceInteractorSphereSelect.h"
#include "coreSurfaceInteractorBrushSelect.h"
#include "coreDataEntityMetadata.h"
#include "coreSurfaceInteractor.h"
#include "coreProcessorInputWidget.h"
#include "coreBaseWindowFactories.h"
#include "coreBaseWindowFactorySearch.h"

#include "coreDataTreeMITKHelper.h"
#include "coreDataContainer.h"

#include "vtkStringArray.h"
#include "vtkPointData.h"
#include "vtkCellData.h"

#include "wxMitkRenderWindow.h"
#include "mitkRenderingManager.h"
#include "mitkMaterialProperty.h"

#include "ptLocalRefinerPanelWidget.h"
#include "ptTaubinSmoothSurfacePanelWidget.h"

#include <sstream>

#include "HistoryLong.xpm"

using namespace Core::Widgets;
using namespace mitk;

#define wxID_POPUP_MENU_ITEM wxID( "wxID_POPUP_MENU_ITEM" )

// Event the widget
BEGIN_EVENT_TABLE(SurfaceSelectorWidget, coreSurfaceSelectorWidgetUI)
END_EVENT_TABLE()

//!
SurfaceSelectorWidget::SurfaceSelectorWidget(
	std::list<std::string> editingToolsFactoryNames,
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style)
: coreSurfaceSelectorWidgetUI(parent, id, pos, size, style)
{
	SetName( wxT("Surface selector") );

	m_EditingToolsFactoryNames = editingToolsFactoryNames;
	
	m_BtnUndoSelectionHistory->SetBitmapDisabled( wxBitmap( HistoryLong_xpm ) );
	m_BtnUndoSelectionHistory->SetBitmapFocus( wxBitmap( HistoryLong_xpm ) );
	m_BtnUndoSelectionHistory->SetBitmapHover( wxBitmap( HistoryLong_xpm ) );
	m_BtnUndoSelectionHistory->SetBitmapLabel( wxBitmap( HistoryLong_xpm ) );
	m_BtnUndoSelectionHistory->SetBitmapSelected( wxBitmap( HistoryLong_xpm ) );

	m_BtnUndoEditHistory->SetBitmapDisabled( wxBitmap( HistoryLong_xpm ) );
	m_BtnUndoEditHistory->SetBitmapFocus( wxBitmap( HistoryLong_xpm ) );
	m_BtnUndoEditHistory->SetBitmapHover( wxBitmap( HistoryLong_xpm ) );
	m_BtnUndoEditHistory->SetBitmapLabel( wxBitmap( HistoryLong_xpm ) );
	m_BtnUndoEditHistory->SetBitmapSelected( wxBitmap( HistoryLong_xpm ) );

	m_InteractorStateHolder = InteractorStateHolderType::New();
	m_InteractorStateHolder->AddObserver(this,
		&SurfaceSelectorWidget::OnModifiedInteractorState );

	m_InteractorStateHolder->SetSubject( INTERACTOR_DISABLED );
	m_interactorType = Core::SurfaceInteractor::TRIANGLE;

	m_removeArray = true;
	m_selectedPoint = Core::DataEntityHolder::New();
	m_selectionSphereHolder = Core::DataEntityHolder::New();

	m_Processor = Core::SurfaceInteractorProcessor::New();
	m_Processor->GetOutputDataEntityHolder(0)->AddObserver( 
		this, 
		&SurfaceSelectorWidget::OnModifiedOutputDataEntity, Core::DH_SUBJECT_MODIFIED_OR_NEW_SUBJECT );



	m_Processor->GetInputDataEntityHolder(0)->AddObserver( 
		this, 
		&SurfaceSelectorWidget::OnModifiedInput );

	m_Radius = 0.5;

	SELECTION_MAX_UNDOS = 100;
	EDIT_MAX_UNDOS = 100;

	m_IgnoreModifications = false;
	m_BtnSelect->SetValue(false);

	m_FlagSelection = true;
	m_ToolWindow = NULL;

	//EnableSelection();

	UpdateWidgets();

	UpdateUndoSelectionButtonLabel( );
	UpdateUndoEditButtonLabel( );
}

//!
SurfaceSelectorWidget::~SurfaceSelectorWidget(void)
{
	StopInteraction( );
}

bool SurfaceSelectorWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = coreSurfaceSelectorWidgetUI::Enable( enable );
	if(!enable)
		EnableSelection(false);

	return bReturn;
}

void SurfaceSelectorWidget::OnInit()
{

	//GetProcessorOutputObserver(0)->SetHideInput(false);
	/*GetProcessorOutputObserver(0)->SelectDataEntity(false);*/

	// Connect remove cells radio button to event handler
	Connect(
		wxID_REMOVE_CELLS,
		wxEVT_COMMAND_RADIOBUTTON_SELECTED,
		wxCommandEventHandler(SurfaceSelectorWidget::OnEditTool )
		);

	// Find factories
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	std::list<std::string>::iterator it;
	for ( it = m_EditingToolsFactoryNames.begin( ) ; it != m_EditingToolsFactoryNames.end( ) ; it++ )
	{
		Core::WindowConfig config;
		if ( graphicalIface->GetBaseWindowFactory( )->GetWindowConfig( *it, config ) )
		{
			wxWindowID winId = wxNewId( );
			wxRadioButton* radioBtnTool;
			radioBtnTool = new wxRadioButton(this, winId, wxString(config.GetCaption( )));
			m_RadioRemoveCells->GetContainingSizer( )->Add(radioBtnTool, 0, wxALL, 2);
		
			// Connect event
			Connect(
				winId,
				wxEVT_COMMAND_RADIOBUTTON_SELECTED,
				wxCommandEventHandler(SurfaceSelectorWidget::OnEditTool )
				);
		}
	}

	// Configure output observer
	GetProcessorOutputObserver(0)->SetEnable( false );
	GetInputWidget(Core::SurfaceInteractorProcessor::INPUT_SURFACE)->SetAutomaticSelection(true);
	GetInputWidget(Core::SurfaceInteractorProcessor::INPUT_SURFACE)->SetDefaultDataEntityFlag(true);

}

void SurfaceSelectorWidget::StartInteractor()
{
	Core::DataEntity::Pointer inputDataEntity; 
	inputDataEntity = m_Processor->GetInputDataEntity(0);
	if ( inputDataEntity.IsNull( ) )
	{
		return;
	}

	// Add input data entity to the rendering tree before creating the Surfaces
	if ( !GetRenderingTree( )->IsDataEntityRendered( inputDataEntity ) )
	{
		GetRenderingTree( )->Add( inputDataEntity );
	}
	
	// Backup rendering properties
	// Avoid enabling interactor if it's already enabled
	// To avoid updating DataEntity metadata again
	if ( m_InteractorStateHolder->GetSubject( ) != INTERACTOR_ENABLED  )
	{
		GetRenderingTree( )->UpdateMetadata( inputDataEntity );
	}

	CreateInteractor();

	//// Disable scalar rendering
	//mitk::DataTreeNode::Pointer node;
	//boost::any anyData = GetRenderingTree()->GetNode( inputDataEntity );
	//Core::CastAnyProcessingData( anyData, node );
	//blMITKUtils::SetScalarMode( node, "", 2 );
}

void SurfaceSelectorWidget::StopInteraction()
{
    if (m_SurfaceInteractor.IsNotNull())
        m_SurfaceInteractor->DisconnectFromDataTreeNode();
    m_SurfaceInteractor = NULL;	// remove data entities used for selection
 	/*Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
 	Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();*/
	/*if (m_selectionSphereHolder->GetSubject().IsNotNull())
		list->Remove( m_selectionSphereHolder->GetSubject() );*/
	if (m_selectionSphereHolder->GetSubject().IsNotNull())
		m_selectionSphereHolder->SetSubject(NULL);

	if (m_Processor->GetInputDataEntity(0).IsNotNull() &&
		m_removeArray )
	{
		Core::vtkPolyDataPtr  polydataInput;
		m_Processor->GetProcessingData(0,polydataInput);
		if (polydataInput != NULL &&
			polydataInput->GetCellData()->HasArray("select"))
		{
			polydataInput->GetCellData()->RemoveArray("select");
		}

		m_Processor->GetInputDataEntity(0)->Modified( );
	}

	mitk::RenderingManager::GetInstance()->RequestUpdateAll();

	m_InteractorStateHolder->SetSubject( INTERACTOR_DISABLED );

	// Restore rendering properties
	GetRenderingTree( )->UpdateRenderingProperties( m_Processor->GetInputDataEntity( 0 ) );
}

SurfaceSelectorWidget::InteractorStateHolderType::Pointer 
SurfaceSelectorWidget::GetInteractorStateHolder() const
{
	return m_InteractorStateHolder;
}

void SurfaceSelectorWidget::UpdateWidgets()
{
	Core::DataEntity::Pointer inputDataEntity; 
	inputDataEntity = m_Processor->GetInputDataEntity(0);

	if ( inputDataEntity.IsNotNull())
	{
		radio_btn_1->SetValue( m_interactorType == Core::SurfaceInteractor::TRIANGLE );
		radio_btn_2->SetValue( m_interactorType == Core::SurfaceInteractor::SPHERE );
		radio_btn_3->SetValue( m_interactorType == Core::SurfaceInteractor::BRUSH );
	}
	
	m_txtBrushSize->Enable( radio_btn_3->GetValue( ) );
	m_spinBrushSize->Enable( radio_btn_3->GetValue( ) );

	if (m_SurfaceInteractor.IsNotNull() && m_interactorType == Core::SurfaceInteractor::BRUSH )
	{
		m_Radius = m_SurfaceInteractor->GetValue();
	}
	// Don't notify handler function
	m_txtBrushSize->ChangeValue( wxString::Format( "%.1f", m_Radius ) );
	m_spinBrushSize->SetValue( m_Radius * 10 );

	if (m_SurfaceInteractor.IsNotNull())
	{
		if ( m_FlagSelection )
		{
			m_BtnSelect->SetLabel("Stop selection");
			m_BtnUnSelect->SetValue( false );
			m_BtnUnSelect->SetLabel("UnSelect");
		}
		else
		{
			m_BtnSelect->SetLabel("Select");
			m_BtnSelect->SetValue( false );
			m_BtnUnSelect->SetLabel("Stop UnSelection");
		}
	}
	else
	{
		m_BtnSelect->SetLabel("Select");
		m_BtnSelect->SetValue( false );
		m_BtnUnSelect->SetLabel("UnSelect");
		m_BtnUnSelect->SetValue( false );
	}
}

void SurfaceSelectorWidget::UpdateData()
{
	//m_SurfaceInteractor->SetValue(radius);
}

void SurfaceSelectorWidget::OnModifiedInput()
{
	if((!/*IsEnabled()*/IsThisEnabled()) || (!IsShown()))
		return;

	try{

		if ( m_Processor->GetInputDataEntity( 0 ).IsNull( ) )
		{
			EnableSelection( false );
		}

	}
	coreCatchExceptionsLogAndNoThrowMacro( SurfaceSelectorWidget::OnModifiedInput );
}

void SurfaceSelectorWidget::SetTimeStep( int time )
{
	try
	{
		UpdateWidgets( );
	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		SurfaceSelectorWidget::OnModifiedTimeStep );
}

void SurfaceSelectorWidget::OnSelectedSurface()
{
	try
	{
		UpdateWidgets();
	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		SurfaceSelectorWidget::OnSelectedSurface );
}

void SurfaceSelectorWidget::CreateInteractor()
{
	Core::DataEntity::Pointer inputDataEntity; 
	inputDataEntity = m_Processor->GetInputDataEntity(0);
	
	m_txtBrushSize->GetValue().ToDouble( &m_Radius );
	CreateSphere(inputDataEntity);


	// Create interactor
	switch ( m_interactorType )
	{

	// Triangle
	case Core::SurfaceInteractor::TRIANGLE:
		m_SurfaceInteractor = Core::SurfaceInteractorTriangleSelect::New( 
			GetRenderingTree( ), 
			m_selectedPoint,
			m_Processor->GetInputDataEntityHolder(0) );
		break;

	// Sphere
	case Core::SurfaceInteractor::SPHERE:
		{
			Core::SurfaceInteractorSphereSelect::Pointer interactor;
			 interactor = Core::SurfaceInteractorSphereSelect::New( 
				GetRenderingTree( ), 
				m_selectedPoint,
				m_Processor->GetInputDataEntityHolder(0));
			 interactor->SetSelectionSphereHolder(m_selectionSphereHolder);
			m_SurfaceInteractor = interactor ;
		}
		break;
		// Brush
	case Core::SurfaceInteractor::BRUSH:
		{
			Core::SurfaceInteractorBrushSelect::Pointer surfaceinter= Core::SurfaceInteractorBrushSelect::New( 
				GetRenderingTree( ), 
				m_selectedPoint,
				m_Processor->GetInputDataEntityHolder(0));
			m_SurfaceInteractor  = surfaceinter;
			m_SurfaceInteractor->SetValue(m_Radius);
		}
		break;
	}


	m_SurfaceInteractor->ConnectToDataTreeNode();
	m_SurfaceInteractor->SetFlagSelection(m_FlagSelection);

	m_SurfaceInteractor->SetCallBack(this);
	m_InteractorStateHolder->SetSubject( INTERACTOR_ENABLED );

}


void SurfaceSelectorWidget::PointsModified(std::vector<std::pair<vtkIdType,int> > selected)
{
	Core::vtkPolyDataPtr  pointInput;
	m_SurfaceInteractor->GetSelectedPointsDataEntity()->GetProcessingData(pointInput);

	// If brush, only add undo when the selected points is empty
	if ( m_interactorType == Core::SurfaceInteractor::BRUSH )
	{
		// User is moving the brush
		if ( pointInput->GetNumberOfPoints( ) != 0 )
		{
			return;
		}
	}
	else
	{
		//no action
		if ( selected.size() == 0 ) return; 
	}

	SelectionAction selection_action;
	selection_action.selected = selected;

	std::stringstream stream;
	stream << selected.size( );
	switch ( m_interactorType )
	{
	case Core::SurfaceInteractor::TRIANGLE:selection_action.name = "Triangle selection (" + stream.str( ) + ")";break;
	case Core::SurfaceInteractor::SPHERE:selection_action.name = "Sphere selection (" + stream.str( ) + ")";break;
	case Core::SurfaceInteractor::BRUSH:selection_action.name = "Brush selection (" + stream.str( ) + ")";break;
	}

	if ( SELECTION_MAX_UNDOS <= m_SelectionActions.size() )
	{
		m_SelectionActions.pop_back();
	}

	m_SelectionActions.push_front(selection_action);

	UpdateUndoSelectionButtonLabel();
}



void SurfaceSelectorWidget::UpdateUndoSelectionButtonLabel()
{
	std::stringstream ss;
	ss<< "Undo (" << m_SelectionActions.size() << ")";

	m_BtnUndoSelection->SetLabel( ss.str() );
	m_BtnUndoSelection->Enable(m_SelectionActions.size()!=0);
	m_BtnUndoSelectionHistory->Enable(m_SelectionActions.size()!=0);
}

void SurfaceSelectorWidget::UpdateUndoEditButtonLabel()
{
	std::stringstream ss;
	ss<< "Undo (" << m_EditActions.size() << ")";

	m_BtnUndoEditing->SetLabel( ss.str() );
	m_BtnUndoEditing->Enable(m_EditActions.size()!=0);
	m_BtnUndoEditHistory->Enable(m_EditActions.size()!=0);
}

void SurfaceSelectorWidget::UpdateInteractorType( )
{
	if (radio_btn_1->GetValue())
		m_interactorType = Core::SurfaceInteractor::TRIANGLE;
	else if (radio_btn_2->GetValue())
		m_interactorType = Core::SurfaceInteractor::SPHERE;
	else if (radio_btn_3->GetValue())
	{
		m_interactorType = Core::SurfaceInteractor::BRUSH;
	}

	// If the input changed -> Create new interaction
	if ( m_InteractorStateHolder->GetSubject() == INTERACTOR_ENABLED )
	{
		CreateInteractor();
	}	
}

void SurfaceSelectorWidget::OnModifiedOutputDataEntity()
{

	Core::DataEntity::Pointer inputDataEntity;
	inputDataEntity = m_Processor->GetInputDataEntity( Core::SurfaceInteractorProcessor::INPUT_SURFACE );
	
	if (( inputDataEntity.IsNull()) || (m_IgnoreModifications ))
		return;
	
	// Export data if selected
	if ( m_SaveIntermediateSteps->GetValue( ) )
	{
		Core::DataEntity::Pointer outDataEntity = Core::DataEntity::New( );
		outDataEntity->Copy( inputDataEntity );
		
		std::stringstream stream;
		stream.fill( '0' );
		stream << "EditedMesh" << std::setw(2) << m_EditActions.size( );
		outDataEntity->GetMetadata()->SetName( stream.str( ) );

		// Publish output
		Core::DataEntityHolder::Pointer holder = Core::DataEntityHolder::New( );
		holder->SetSubject( outDataEntity );
		Core::DataTreeHelper::PublishOutput( holder, GetRenderingTree( ), false, false );
	}
	else
	{
		mitk::RenderingManager::GetInstance()->RequestUpdateAll();
	}
}

void SurfaceSelectorWidget::SetInputDataEntity( Core::DataEntity::Pointer val )
{
	m_Processor->GetInputDataEntityHolder(0)->SetSubject( val );
}

Core::DataEntity::Pointer SurfaceSelectorWidget::GetInputDataEntity( )
{
	return m_Processor->GetInputDataEntity(0);
}


void SurfaceSelectorWidget::SetInteractorType( 
		Core::SurfaceInteractor::INTERACTOR_TYPE interactorType )
{
	m_interactorType = interactorType;
}

Core::BaseProcessor::Pointer SurfaceSelectorWidget::GetProcessor()
{
	return m_Processor.GetPointer();
}



void SurfaceSelectorWidget::CreateSphere( 
	Core::DataEntity::Pointer dataEntity )
{

	Core::vtkPolyDataPtr pointSet = Core::vtkPolyDataPtr::New( );
	Core::DataEntity::Pointer point = m_selectedPoint->GetSubject();
	if (point.IsNull())
	{
		point = Core::DataEntity::New( Core::PointSetTypeId ); 
		point->GetMetadata()->SetName( "center point" );
		point->AddTimeStep( pointSet );
		m_selectedPoint->SetSubject( point );
	}

	if (m_interactorType == Core::SurfaceInteractor::SPHERE)
	{
		Core::DataEntity::Pointer Sphere;
		Sphere = m_selectionSphereHolder->GetSubject();
		if ( Sphere.IsNull( ) )
		{
			Sphere = Core::DataEntity::New( Core::SurfaceMeshTypeId );
			Sphere->GetMetadata()->SetName( "SelectionSphere" );
			Sphere->Resize( 1, typeid( Core::vtkPolyDataPtr ) );
			Sphere->SetFather( dataEntity );

			m_selectionSphereHolder->SetSubject( Sphere );

			// Create empty image
			Core::vtkPolyDataPtr referenceSphere; 
			Sphere->GetProcessingData( referenceSphere );
			vtkSmartPointer<vtkSphereSource> selSphere = vtkSmartPointer<vtkSphereSource>::New();
			selSphere->SetThetaResolution(50);
			selSphere->SetPhiResolution(50);
			selSphere->SetRadius(0.1);
			selSphere->Update();
			referenceSphere->DeepCopy(selSphere->GetOutput());
		}
	}
}

void SurfaceSelectorWidget::OnModifiedInteractorState()
{
	if ( m_InteractorStateHolder->GetSubject() == INTERACTOR_ENABLED)
	{
	}
	else if (m_InteractorStateHolder->GetSubject() == INTERACTOR_DISABLED)
	{
	}
}

void SurfaceSelectorWidget::PushEditAction( std::string actionName )
{
	if ( GetInputDataEntity().IsNull( ) )
	{
		return;
	}

	EditAction editAction;
	editAction.name = actionName;

	// Copy processing data
	Core::vtkPolyDataPtr processingData;
	GetInputDataEntity()->GetProcessingData(processingData);
	editAction.prevSurface = Core::vtkPolyDataPtr::New();
	editAction.prevSurface->DeepCopy(processingData);

	// Save selection actions
	editAction.selection_actions = m_SelectionActions;

	// Add to history
	m_EditActions.push_front(editAction);
	UpdateUndoEditButtonLabel();
}


void SurfaceSelectorWidget::RemoveCells( )
{
	if (m_RadioRemoveCells->GetValue() && 
		m_InteractorStateHolder->GetSubject() == INTERACTOR_ENABLED )
	{
		m_Processor->SetProcessorType(Core::SurfaceInteractorProcessor::RemoveCells);
		PushEditAction( m_RadioRemoveCells->GetLabel( ).ToStdString() );
		m_Processor->Update();
	}
}

bool SurfaceSelectorWidget::IsSelectionEnabled()
{
	return m_InteractorStateHolder->GetSubject() == INTERACTOR_ENABLED;
}


void SurfaceSelectorWidget::EnableSelection( bool enable  /*=true*/ )
{
	if(enable)
		StartInteractor();
	else
	{
		if (m_SurfaceInteractor.IsNotNull())
			StopInteraction();

		m_SelectionActions = std::deque<SelectionAction>(0);
		UpdateUndoSelectionButtonLabel();
	}

	if(enable != m_BtnSelect->GetValue())
		m_BtnSelect->SetValue(enable);
	UpdateWidgets();

}

//!
void SurfaceSelectorWidget::OnBrushSizeChanged(wxCommandEvent &event)
{
	m_txtBrushSize->GetValue().ToDouble( &m_Radius );

	if (m_SurfaceInteractor.IsNotNull() )
	{
		m_SurfaceInteractor->SetValue( m_Radius );
	}

	UpdateWidgets( );
}


//!
SelectionAction SurfaceSelectorWidget::Reverse(SelectionAction action)
{
	SelectionAction retAct;
	retAct.selected = action.selected;

	for (int i=0; i<action.selected.size(); i++)
	{
		if (action.selected[i].second == 0) retAct.selected[i].second = 20;
		else retAct.selected[i].second = 0;
	}

	return retAct;
}

//!
void SurfaceSelectorWidget::OnUndoSelection(wxCommandEvent &event)
{
	Core::vtkPolyDataPtr  polydataInput;
	GetInputDataEntity()->GetProcessingData(polydataInput);

	if (m_SelectionActions.size()==0)
	{
		//create an empty array
		vtkShortArray *selArray = vtkShortArray::New();
		selArray->SetName("select");
		selArray->SetNumberOfValues( polydataInput->GetNumberOfCells());
		for ( vtkIdType i = 0; i < polydataInput->GetNumberOfCells(); i++ )
		{		
			selArray->SetTuple1(i, 0);		
		}

		polydataInput->GetCellData()->AddArray(selArray);
	}
	else
	{	
		SelectionAction undoAction = Reverse(m_SelectionActions[0]);

		for (int i=0; i<undoAction.selected.size(); i++)
		{
			polydataInput->GetCellData()->GetArray("select")->SetTuple1(undoAction.selected[i].first,undoAction.selected[i].second);
		}

		polydataInput->GetCellData()->SetActiveScalars("select");
		CreateInteractor();

		//Core::DataTreeMITKHelper::ApplyLookupTableToMesh( 
		//	m_SurfaceInteractor->GetSelectedDataEntityNode(), blMITKUtils::LUT_SCALAR_MODE_CELL_DATA );

		
	}
	
	mitk::RenderingManager::GetInstance()->RequestUpdateAll();

	m_SelectionActions.pop_front();
	
	UpdateUndoSelectionButtonLabel();
}


//!
void SurfaceSelectorWidget::OnApplyEditing(wxCommandEvent &event)
{
	bool selectionEnabled = m_SurfaceInteractor.IsNotNull();

	m_IgnoreModifications = true;

	// Set output DataEntity the same as input DataEntity
	// to reuse it
	m_Processor->SetOutputDataEntity( 0, m_Processor->GetInputDataEntity( 0 ) );

	m_IgnoreModifications = false;

	if ( m_ToolWindow )
	{
		ProcessingWidget* processing = dynamic_cast<ProcessingWidget*> ( m_ToolWindow );
		if ( processing )
		{
			PushEditAction( processing->GetProcessor( )->GetName( ) );
			processing->UpdateData( );
			processing->GetProcessor( )->SetInputDataEntity( 0, m_Processor->GetInputDataEntity( 0 ) );
			processing->GetProcessor( )->GetOutputPort( 0 )->SetReuseOutput( true );
			processing->GetProcessor( )->SetOutputDataEntity( 0, m_Processor->GetInputDataEntity( 0 ) );
			processing->GetProcessor( )->Update();
		}
	}
	else if (m_RadioRemoveCells->GetValue())
	{
		RemoveCells();
	}

	// Clean current selection actions
	m_SelectionActions = std::deque<SelectionAction>(0);
	UpdateUndoSelectionButtonLabel();

	// Enable selection interaction
	EnableSelection( selectionEnabled );
}

//!
void SurfaceSelectorWidget::OnUndoEditing(wxCommandEvent &event)
{

	if (m_EditActions.size()>0)
	{
		bool selectionEnabled = m_SurfaceInteractor.IsNotNull();

		// Get last action
		EditAction editAction = m_EditActions[0];
		m_EditActions.pop_front();

		// Restore processing data
		m_IgnoreModifications = true;
		m_Processor->GetInputDataEntity( 0 )->SetTimeStep(editAction.prevSurface);
		m_IgnoreModifications = false;

		// Restore selection actions
		m_SelectionActions = editAction.selection_actions;
		UpdateUndoSelectionButtonLabel();
		UpdateUndoEditButtonLabel();

		// Force rendering
		mitk::RenderingManager::GetInstance()->RequestUpdateAll();

		// Enable interaction
		EnableSelection( selectionEnabled );

		//// Refresh select array
		//Core::DataTreeMITKHelper::ApplyLookupTableToMesh( 
		//	m_SurfaceInteractor->GetSelectedDataEntityNode(), blMITKUtils::LUT_SCALAR_MODE_CELL_DATA );
	}

}

void SurfaceSelectorWidget::OnBtnSelect( wxCommandEvent &event )
{
	// If user is in Unselect mode and clicked Select -> change mode to selection
	if (m_SurfaceInteractor.IsNotNull() && !m_FlagSelection )
	{
		m_FlagSelection = true;
		m_SurfaceInteractor->SetFlagSelection(m_FlagSelection);
	}
	else
	{
		// Enable selection or stop it
		m_FlagSelection = true;
		EnableSelection(event.GetInt()>0);
	}

	UpdateWidgets();
}

void SurfaceSelectorWidget::OnBtnUnSelect( wxCommandEvent &event )
{
	// If user is in Select mode and clicked UnSelect -> change mode to UnSelect
	if (m_SurfaceInteractor.IsNotNull() && m_FlagSelection )
	{
		m_FlagSelection = false;
		m_SurfaceInteractor->SetFlagSelection(m_FlagSelection);
	}
	else
	{
		// Enable unselection or stop it
		m_FlagSelection = false;
		EnableSelection(event.GetInt()>0);
	}

	UpdateWidgets();
}

void SurfaceSelectorWidget::OnBrushSizeSpin(wxSpinEvent &event)
{
	m_Radius = double( m_spinBrushSize->GetValue( ) ) * 0.1f;

	if (m_SurfaceInteractor.IsNotNull() )
	{
		m_SurfaceInteractor->SetValue( m_Radius );
	}

	UpdateWidgets( );
}

void SurfaceSelectorWidget::OnBrush(wxCommandEvent &event)
{
	UpdateInteractorType( );
	UpdateWidgets( );
}

void SurfaceSelectorWidget::OnTriangle(wxCommandEvent &event)
{
	UpdateInteractorType( );
	UpdateWidgets( );
}

void SurfaceSelectorWidget::OnSphere(wxCommandEvent &event)
{
	UpdateInteractorType( );
	UpdateWidgets( );
}

void SurfaceSelectorWidget::OnUndoSelectionHistory(wxCommandEvent &event)
{
	// Create pop up menu
	m_UndoMenu = new wxMenu( );

	// Add all items and connect handler
	std::deque<SelectionAction>::iterator it;
	int count = 0;
	for ( it = m_SelectionActions.begin() ; it != m_SelectionActions.end( ) ; it++ )
	{
		wxWindowID id = wxID_POPUP_MENU_ITEM + count;
		m_UndoMenu->Append( id, it->name );
		Connect(
			id,
			wxEVT_COMMAND_MENU_SELECTED,
			wxCommandEventHandler(SurfaceSelectorWidget::OnUndoSlectionHistoryItem )
			);

		count++;
	}

	// Show pop up menu
	wxPoint position = m_BtnUndoSelectionHistory->GetPosition();
	position.y += m_BtnUndoSelectionHistory->GetSize().GetHeight();
	PopupMenu( m_UndoMenu, position );

	delete m_UndoMenu;
}

void SurfaceSelectorWidget::OnUndoEditHistory(wxCommandEvent &event)
{
	// Create pop up menu
	m_UndoMenu = new wxMenu( );

	// Add all items and connect handler
	std::deque<EditAction>::iterator it;
	int count = 0;
	for ( it = m_EditActions.begin() ; it != m_EditActions.end( ) ; it++ )
	{
		wxWindowID id = wxID_POPUP_MENU_ITEM + count;
		m_UndoMenu->Append( id, it->name );
		Connect(
			id,
			wxEVT_COMMAND_MENU_SELECTED,
			wxCommandEventHandler(SurfaceSelectorWidget::OnUndoEditHistoryItem )
			);

		count++;
	}

	// Show pop up menu
	wxPoint position = m_BtnUndoEditHistory->GetPosition();
	position.y += m_BtnUndoSelectionHistory->GetSize().GetHeight();
	PopupMenu( m_UndoMenu, position );

	delete m_UndoMenu;
}

void SurfaceSelectorWidget::OnUndoSlectionHistoryItem( wxCommandEvent& event )
{
	// Undo until selected item
	std::deque<SelectionAction>::iterator it;
	int count = 0;
	for ( it = m_SelectionActions.begin() ; it != m_SelectionActions.end( ) ; it++ )
	{
		OnUndoSelection( event );
		if ( count == (event.GetId( ) - wxID_POPUP_MENU_ITEM) )
		{
			break;
		}

		count++;
	}
}


void SurfaceSelectorWidget::OnUndoEditHistoryItem( wxCommandEvent& event )
{
	// Undo until selected item
	std::deque<EditAction>::iterator it;
	int count = 0;
	for ( it = m_EditActions.begin() ; it != m_EditActions.end( ) ; it++ )
	{
		OnUndoEditing( event );
		if ( count == (event.GetId( ) - wxID_POPUP_MENU_ITEM) )
		{
			break;
		}

		count++;
	}
}

void SurfaceSelectorWidget::OnEditTool(wxCommandEvent &event)
{
	// Lock this window
	wxWindowUpdateLocker lock( this );

	// Remove old window
	if ( m_ToolWindow )
	{
		m_ToolWindow->Destroy( );
		m_ToolWindow = NULL;
	}

	// Find caption
	std::string caption;
	wxSizer* sizer = m_RadioRemoveCells->GetContainingSizer( );
	wxSizerItemList list = sizer->GetChildren();
	wxSizerItemList::iterator it;
	for ( it = list.begin( ) ; it != list.end( ) ; it++ )
	{
		if ( (*it)->GetWindow( )->GetId( ) == event.GetId( ) )
		{
			caption = (*it)->GetWindow( )->GetLabel( );
		}
	}

	// Find factory by caption
	std::string factoryName;
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	Core::BaseWindowFactorySearch::Pointer search;
	search = Core::BaseWindowFactorySearch::New( );
	search->SetCaption( caption );
	search->Update( );
	std::list<std::string> factories = search->GetFactoriesNames( );
	if ( factories.size( ) > 0 )
	{
		factoryName = *factories.begin( );

		// Create widget
		Core::BaseWindow* baseWindow;
		baseWindow = graphicalIface->GetBaseWindowFactory( )->CreateBaseWindow( factoryName, this );
		m_ToolWindow = dynamic_cast<wxWindow*> (baseWindow);

		// Remove apply button
		wxWindow *apply = m_ToolWindow->FindWindow( wxID_APPLY );
		if ( apply )
		{
			apply->Hide( );
		}

		if ( m_ToolWindow )
		{
			ProcessingWidget* processing = dynamic_cast<ProcessingWidget*> ( m_ToolWindow );
			if ( processing )
			{
				// Init base window
				processing->InitProcessorObservers( true );

				processing->GetProcessor( )->SetInputDataEntity( 0, m_Processor->GetInputDataEntity( 0 ) );
			}
		}

		// Add to this window 
		GetSizer( )->Add(m_ToolWindow, 0, wxALL|wxEXPAND, 5);
		GetSizer( )->Layout( );
	}

	// Send a resize event to update the size of window
	wxSizeEvent resEvent(GetBestSize(), GetId());
	resEvent.SetEventObject(this);
	GetEventHandler()->ProcessEvent(resEvent);
}


