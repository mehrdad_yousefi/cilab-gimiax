/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreSurfaceSelectorWidget_H
#define coreSurfaceSelectorWidget_H

#include "corePluginMacros.h"
#include "coreSurfaceSelectorWidgetUI.h"
#include "coreDataEntityHolder.h"
#include "coreCommonDataTypes.h"
#include "coreVTKPolyDataHolder.h"
#include "coreSurfaceInteractor.h"
#include "coreSurfaceInteractorProcessor.h"
#include "coreSelectionToolWidget.h"
#include "SurfaceSelectorCallback.h"

#include <stdio.h>
#include <time.h>

#include <deque>

#define wxID_SurfaceSelectorWidget wxID("wxID_SurfaceSelectorWidget")


class SelectionAction
{
public:
	//! Changes on selected points
	std::vector<std::pair<vtkIdType,int> > selected;
	//!
	std::string name;
};


class EditAction
{
public:
	//! Name of this action
	std::string name;
	//! Previous surface
	Core::vtkPolyDataPtr prevSurface;
	//! Selection actions
	std::deque<SelectionAction> selection_actions;
};

/** 
\brief Widget for selecting Surfaces

When the user starts interaction, a region on a surface is created.
If the user unloads the surface, the data entity is removed.
If the user stops interaction, the data entity is removed.

\ingroup MeshEditorPlugin
\author Chiara Riccobene
\date 13 April 2010
*/
class PLUGIN_EXPORT SurfaceSelectorWidget 
	: public coreSurfaceSelectorWidgetUI, 
	public SurfaceSelectorCallback,
	public Core::Widgets::ProcessingWidget
{
public:

	enum INTERACTOR_STATE
	{
		INTERACTOR_DISABLED,
		INTERACTOR_ENABLED
	};
	typedef Core::DataHolder<INTERACTOR_STATE> InteractorStateHolderType;

public:
	coreDefineBaseWindowFactory1param( SurfaceSelectorWidget, std::list<std::string> )

	SurfaceSelectorWidget(
		std::list<std::string> editingToolsFactoryNames,
		wxWindow* parent, 
		int id = wxID_SurfaceSelectorWidget, 
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=wxDEFAULT_DIALOG_STYLE);
	~SurfaceSelectorWidget(void);

	//!
	void OnInit();
	//!
	bool Enable( bool enable = true );

	//!
	void StartInteractor( );

	//!
	void StopInteraction( );

	//!
	virtual bool IsSelectionEnabled( );

	//! 
	InteractorStateHolderType::Pointer GetInteractorStateHolder() const;

	//!
	void SetInputDataEntity( Core::DataEntity::Pointer val );

	//!
	Core::DataEntity::Pointer GetInputDataEntity( );

	//!
	void SetInteractorType( 
		Core::SurfaceInteractor::INTERACTOR_TYPE interactorType );

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

	//!
	SelectionAction Reverse(SelectionAction action);

private:

	//@{ 
	/// \name Event handlers

	//! Update table of points
	void OnModifiedInput( );

	//!
	void PointsModified(std::vector<std::pair<vtkIdType,int> > selected);

	//!
	void OnModifiedInteractorState( );

	//!
	void SetTimeStep( int time );

	//!
	void UpdateInteractorType( );

	//!
	void PushEditAction( std::string actionName );

	//!
	void RemoveCells();

	//!
	void OnSelectedSurface( );

	//!
	void OnBtnSelect(wxCommandEvent &event);

	//!
	void OnBtnUnSelect(wxCommandEvent &event);

	//@}


	//!
	void UpdateWidgets( );

	//!
	void UpdateData( );

	//!
	void CreateInteractor( );

	//!
	void OnModifiedOutputDataEntity();

	//!
	void CreateSphere( 
		Core::DataEntity::Pointer dataEntity );

	//!
	void OnBrushSizeChanged(wxCommandEvent &event);
	//!
	void OnUndoSelection(wxCommandEvent &event); 
	//!
	void OnApplyEditing(wxCommandEvent &event);
	//!
	void OnUndoEditing(wxCommandEvent &event);
	//!
    void OnBrushSizeSpin(wxSpinEvent &event);
	//!
    void OnBrush(wxCommandEvent &event);
    void OnTriangle(wxCommandEvent &event);
    void OnSphere(wxCommandEvent &event);
	//!
	void OnEditTool(wxCommandEvent &event);

	//!
    void OnUndoEditHistory(wxCommandEvent &event);
	//!
    void OnUndoSelectionHistory(wxCommandEvent &event);

	//!
	void OnUndoSlectionHistoryItem( wxCommandEvent& event );

	//!
	void OnUndoEditHistoryItem( wxCommandEvent& event );

	//!
	void EnableSelection( bool enable = true );
	
	//!
	void UpdateUndoSelectionButtonLabel();

	//!
	void UpdateUndoEditButtonLabel();

    wxDECLARE_EVENT_TABLE();

private:

	//! Input data
	Core::DataEntityHolder::Pointer m_InputDataHolder;

	//! Connection to m_InputDataHolder
	boost::signals::connection m_InputDataHolderConnection;

	//! State of the interactor to update the views
	InteractorStateHolderType::Pointer m_InteractorStateHolder;

	//!
	Core::DataEntityHolder::Pointer m_InputSurfacesHolder;

	//! variable to set type of interactor
	Core::SurfaceInteractor::INTERACTOR_TYPE m_interactorType;

	Core::SurfaceInteractor::Pointer m_SurfaceInteractor;

	//! brief Selected point of the mesh. If no point is selected, it's empty.
	Core::DataEntityHolder::Pointer m_selectedPoint;
	//! brief Selected point of the mesh. If no point is selected, it's empty.
	Core::DataEntityHolder::Pointer m_selectionSphereHolder;

	bool m_removeArray;

	Core::SurfaceInteractorProcessor::Pointer m_Processor;

	double m_Radius;

	bool m_IgnoreModifications;

	//!
	std::string m_PreviousSelectedScalar;


	std::deque<SelectionAction> m_SelectionActions;
	int SELECTION_MAX_UNDOS;

	std::deque<EditAction> m_EditActions;
	int EDIT_MAX_UNDOS;

	//!
	bool m_FlagSelection;

	//!
	wxMenu *m_UndoMenu;

	//!
	wxWindow* m_ToolWindow;

	//!
	std::list<std::string> m_EditingToolsFactoryNames;
};


#endif // coreSurfaceSelectorWidget_H
