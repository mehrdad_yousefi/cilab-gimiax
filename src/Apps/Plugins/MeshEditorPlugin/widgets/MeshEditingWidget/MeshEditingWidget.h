/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _MeshEditingWidget_H
#define _MeshEditingWidget_H

#include "MeshEditingWidgetUI.h"
#include "coreProcessingWidget.h"

/**
\ingroup MeshEditorPlugin
\author Xavi Planes
\date Jul 2012
*/
class MeshEditingWidget : 
	public MeshEditingWidgetUI,
	public Core::Widgets::ProcessingWidget
{

	// OPERATIONS
public:
	coreDefineBaseWindowFactory1param( MeshEditingWidget, std::list<std::string> )

	MeshEditingWidget(
		std::list<std::string> editingToolsFactoryNames,
		wxWindow* parent, 
		int id = wxID_ANY,
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=0);

	//!
	~MeshEditingWidget( );

	//!
	void OnInit();

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

private:
    wxDECLARE_EVENT_TABLE();
	//!
	void OnEditTool(wxCommandEvent &event);

	// ATTRIBUTES
private:

	//!
	std::list<std::string> m_EditingToolsFactoryNames;

	//!
	wxWindow* m_ToolWindow;

	//! Map button ID with window factory
	std::map<wxWindowID,std::string> m_ButtonFactory;

};

#endif //_MeshEditingWidget_H
