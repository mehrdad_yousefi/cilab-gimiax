/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "MeshEditingWidget.h"

#include "wx/wupdlock.h"

#include "coreWxMitkGraphicalInterface.h"
#include "coreBaseWindowFactories.h"
#include "coreBaseWindowFactorySearch.h"

// Event the widget
BEGIN_EVENT_TABLE(MeshEditingWidget,MeshEditingWidgetUI)
END_EVENT_TABLE()


MeshEditingWidget::MeshEditingWidget(
	std::list<std::string> editingToolsFactoryNames,
	wxWindow* parent, 
	int id,
	const wxPoint& pos, 
	const wxSize& size, 
	long style) :
	MeshEditingWidgetUI(parent, id, pos, size, style)
{
	m_EditingToolsFactoryNames = editingToolsFactoryNames;
	m_ToolWindow = NULL;

	wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);

	// Find factories
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	std::list<std::string>::iterator it;
	for ( it = m_EditingToolsFactoryNames.begin( ) ; it != m_EditingToolsFactoryNames.end( ) ; it++ )
	{
		Core::WindowConfig config;
		if ( graphicalIface->GetBaseWindowFactory( )->GetWindowConfig( *it, config ) )
		{
			wxWindowID winId = wxNewId( );
			wxBitmapButton* bitmapBtn;
			bitmapBtn = new wxBitmapButton(this, winId, wxBitmap(config.GetBitmap( )));
			bitmapBtn->SetToolTip( config.GetCaption( ) );
			sizer->Add(bitmapBtn, 0, wxALL, 0);
		
			m_ButtonFactory[ winId ] = *it;

			// Connect event
			Connect(
				winId,
				wxEVT_COMMAND_BUTTON_CLICKED,
				wxCommandEventHandler(MeshEditingWidget::OnEditTool )
				);
		}
	}

	GetSizer( )->Add(sizer, 0, wxALL|wxEXPAND, 5);

	GetSizer( )->Fit(this);
}

MeshEditingWidget::~MeshEditingWidget( ) 
{
}

void MeshEditingWidget::OnInit()
{
}


void MeshEditingWidget::OnEditTool(wxCommandEvent &event)
{
	// Lock this window
	wxWindowUpdateLocker lock( this );

	// Remove old window
	if ( m_ToolWindow )
	{
		m_ToolWindow->Destroy( );
		m_ToolWindow = NULL;
	}

	// Find Factory
	std::string factoryName = m_ButtonFactory[ event.GetId( ) ];
	if ( !factoryName.empty( ) )
	{
		// Create widget
		Core::BaseWindow* baseWindow;
		Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
		graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
		baseWindow = graphicalIface->GetBaseWindowFactory( )->CreateBaseWindow( factoryName, this );
		m_ToolWindow = dynamic_cast<wxWindow*> (baseWindow);

		// Init processor observers
		baseWindow->InitProcessorObservers( true );

		// OnInit can change the processor observers
		GetPluginTab( )->InitBaseWindow( baseWindow );

		// Add to this window 
		GetSizer( )->Add(m_ToolWindow, 1, wxTOP|wxEXPAND, 5);
		GetSizer( )->Layout( );
	}

	// Send a resize event to update the size of window
	wxSizeEvent resEvent(GetBestSize(), GetId());
	resEvent.SetEventObject(this);
	GetEventHandler()->ProcessEvent(resEvent);
}

Core::BaseProcessor::Pointer MeshEditingWidget::GetProcessor( )
{
	if ( m_ToolWindow == NULL )
	{
		return NULL;
	}

	Core::BaseWindow* baseWindow = dynamic_cast<Core::BaseWindow*> ( m_ToolWindow );
	return baseWindow->GetProcessor( );
}

