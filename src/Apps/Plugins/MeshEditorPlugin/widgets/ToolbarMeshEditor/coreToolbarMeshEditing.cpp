/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreToolbarMeshEditing.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreKernel.h"
#include "coreMainMenu.h"
#include "coreBaseWindowFactories.h"

#include "SelectCellsSurfaceButton.xpm"
#include "ringcut.xpm"
#include "closeholes.xpm"
#include "extractsurface.xpm"
#include "coreSurfaceSelectorWidget.h"
#include "ptExtractMainSurfacePanelWidget.h"
#include "ptRingCutPanelWidget.h"
#include "VolumeClosingPanelWidget.h"
#include "coreProcessingToolboxWidget.h"

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(Core::Widgets::ToolbarMeshEditing, Core::Widgets::ToolbarPluginTab)
END_EVENT_TABLE()


Core::Widgets::ToolbarMeshEditing::ToolbarMeshEditing(
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
ToolbarPluginTab(parent, id, pos, size, style, name)
{
	AddWidgetToToolbar(wxID_SurfaceSelectorWidget, SURFACEWIDGETNAME, wxBitmap( selectcellssurfacebutton_xpm ));
	AddWidgetToToolbar(wxID_RingCutWidget, RINGCUTWIDGETNAME, wxBitmap( ringcut_xpm ));
	AddWidgetToToolbar(wxID_VolumeClosingWidget, HOLEFILLINGWIDGETNAME, wxBitmap( closeholes_xpm ));
	AddWidgetToToolbar(wxID_ExtractSurfaceWidget, EXTRACTSURFACEWIDGETNAME, wxBitmap( extractsurface_xpm ));

	for (int i = 0; i < NUMBER_OF_TOOLBAR_WIDGETS; i++)
	{
		AddTool(m_toolbarItems[i].id, m_toolbarItems[i].widgetName, m_toolbarItems[i].bitmap );
		Connect(
			m_toolbarItems[i].id,
			wxEVT_COMMAND_MENU_SELECTED,
			wxCommandEventHandler(ToolbarMeshEditing::OnProcessingTool)
			);

	}
	m_selectedItem = -1;
	Realize();
}
void Core::Widgets::ToolbarMeshEditing::AddWidgetToToolbar ( int id, std::string name, wxBitmap bitmap )
{
		toolbarItem tbi;
		tbi.id = id;
		tbi.widgetName = name;
		tbi.bitmap = bitmap;
		m_toolbarItems.push_back(tbi);
}

void Core::Widgets::ToolbarMeshEditing::AddWidgetTool( wxWindow* window )
{
	if ( GetPluginTab()->GetWorkingArea( window->GetId() ) )
	{
		return;
	}

	Core::BaseWindow* baseWindow = dynamic_cast<Core::BaseWindow*> ( window );
	if ( baseWindow == NULL )
	{
		return;
	}

	Core::BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	WindowConfig config;
	bool success = baseWindowFactory->GetWindowConfig( baseWindow->GetFactoryName(), config );
	if ( !success || !( config.GetCategory() == "Advanced Surface Editing" || config.GetCategory() == "Basic Surface Editing" ) )
	{
		return;
	}


	ToolbarPluginTab::AddWidgetTool( window );
}


void Core::Widgets::ToolbarMeshEditing::OnProcessingTool(wxCommandEvent& event)
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
	gIface = Core::Runtime::Kernel::GetGraphicalInterface();

	wxFrame* mainFrame = dynamic_cast<wxFrame*> ( gIface->GetMainWindow() );
	wxMenuBar* menuBar = mainFrame->GetMenuBar( );
	Core::Widgets::MainMenu* mainMenu = dynamic_cast<Core::Widgets::MainMenu*> ( menuBar );

	for (int i = 0; i < NUMBER_OF_TOOLBAR_WIDGETS; i++)
	{
		if ( m_toolbarItems[i].id == event.GetId() )
			m_selectedItem = i;
	}
	int menuId = mainMenu->FindMenuItem( "Tools", m_toolbarItems[m_selectedItem].widgetName );

	if(event.GetInt()!=0)
	{
		wxCommandEvent menuEvt(wxEVT_COMMAND_MENU_SELECTED);
		menuEvt.SetId(menuId);
    wxPostEvent(mainMenu->GetEventHandler(), menuEvt);
	}


}


void Core::Widgets::ToolbarMeshEditing::UpdateState()
{
	
	if (m_selectedItem == -1)
		return;
	UpdateToolButton(m_toolbarItems[m_selectedItem].id, m_toolbarItems[m_selectedItem].widgetName);
	ToolbarPluginTab::UpdateState();
	
}


void Core::Widgets::ToolbarMeshEditing::HideTool(int id, wxString idName)
{
	Core::Widgets::ProcessingToolboxWidget* processingToolboxWidget;
	GetPluginTab()->GetWidget( wxID_ProcessingToolboxWidget, processingToolboxWidget );
	if (processingToolboxWidget == NULL)
		return;
	wxWindow* window =  processingToolboxWidget->GetToolWindow();

	bool isShown = ((window != NULL) && (window->GetName()== idName));
	if(isShown )
	{
		processingToolboxWidget->SetToolWindow(NULL);
	}
}

		
void Core::Widgets::ToolbarMeshEditing::UpdateToolButton(int id, wxString idName)
		
{
	Core::Widgets::ProcessingToolboxWidget* processingToolboxWidget;
	GetPluginTab()->GetWidget( wxID_ProcessingToolboxWidget, processingToolboxWidget );
	if (processingToolboxWidget == NULL)
		return;
	wxWindow* window =  processingToolboxWidget->GetToolWindow();

	bool isShown = (window != NULL) && (window->GetName()== idName);

	if (isShown)
		ToggleTool (id, true);
	else
		ToggleTool (id, false);
	
	for (int i = 0; i < NUMBER_OF_TOOLBAR_WIDGETS; i++)
	{
		if ( m_toolbarItems[i].id != id )
			ToggleTool (m_toolbarItems[i].id, false);
	}

}



	
