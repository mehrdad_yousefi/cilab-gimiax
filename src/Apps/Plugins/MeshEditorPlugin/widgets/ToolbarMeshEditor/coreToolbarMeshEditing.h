/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreToolbarMeshEditing_H
#define _coreToolbarMeshEditing_H

#include "corePluginTab.h"
#include "coreToolbarPluginTab.h"

#define EXTRACTSURFACEWIDGETNAME "Extract Surfaces"
#define RINGCUTWIDGETNAME "Ring Cut"
#define HOLEFILLINGWIDGETNAME "Hole Filling"
#define SURFACEWIDGETNAME "Local Surface Editing"
#define NUMBER_OF_TOOLBAR_WIDGETS 4

namespace Core
{
	namespace Widgets
	{

		/**
		\brief Toolbar for IO
		\ingroup gmWidgets
		\author Albert Sanchez
		\date 09 March 2011
		*/
		class ToolbarMeshEditing: public ToolbarPluginTab {
		public:
			//!
			coreDefineBaseWindowFactory( ToolbarMeshEditing );

			//!
			ToolbarMeshEditing(wxWindow* parent, wxWindowID id = wxID_ANY, 
				const wxPoint& pos = wxDefaultPosition, 
				const wxSize& size = wxDefaultSize, 
				long style = wxAUI_TB_DEFAULT_STYLE, 
				const wxString& name = wxPanelNameStr);

			struct toolbarItem
			{
				int id; 
				std::string widgetName; 
				wxBitmap bitmap;
			};
			typedef std::vector<toolbarItem> toolbarItemsVector; 

		protected:
			//!
			void AddWidgetTool( wxWindow* window );

			void UpdateState();

			void OnProcessingTool(wxCommandEvent& event);

    wxDECLARE_EVENT_TABLE();
		
			
			void HideTool(int id, wxString idName);
			void AddWidgetToToolbar ( int id, std::string name, wxBitmap bitmap );
		
			
			void UpdateToolButton(int id, wxString idName);
		private:
			bool m_ignoreUpdate;
			toolbarItemsVector m_toolbarItems;
			int m_selectedItem;

		};


	} // namespace Widget
} // namespace Core

#endif // _coreToolbarMeshEditing_H
