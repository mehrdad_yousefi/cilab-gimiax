// Copyright 2009 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "ptExtractMainSurfacePanelWidget.h"
#include "coreDataTreeHelper.h"

ptExtractMainSurfacePanelWidget::ptExtractMainSurfacePanelWidget(
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    wxPanel(parent, id, pos, size, wxTAB_TRAVERSAL)
{
    btnExtractMainSurface = new wxButton(this, wxID_btnExtractMainSurface, wxT("Extract surfaces"));
	m_OnlyMainChk = new wxCheckBox(this,wxID_ANY,wxT("Only main surface"));
	m_OnlyMainChk->SetValue(true);
	m_ExtractMultipleSurfaces = Core::ExtractSurfacesProcessor::New();
	m_processor = ExtractMainSurfaceProcessor::New();
	m_processor->SetName( "ExtractSurfaceProcessor" );
	m_processor->GetOutputPort( 0 )->SetDataEntityName( "Extracted Main Surface" );

	do_layout();
}


BEGIN_EVENT_TABLE(ptExtractMainSurfacePanelWidget, wxPanel)
    // begin wxGlade: ptExtractMainSurfacePanelWidget::event_table
    EVT_BUTTON(wxID_btnExtractMainSurface, ptExtractMainSurfacePanelWidget::OnButtonExtractMainSurface)
    // end wxGlade
END_EVENT_TABLE();

void ptExtractMainSurfacePanelWidget::OnButtonExtractMainSurface(wxCommandEvent &event)
{
	if ( !m_OnlyMainChk->GetValue( ) )
	{
		if (m_processor->GetInputDataEntity(0).IsNotNull())
		{
			m_ExtractMultipleSurfaces->SetInputDataEntity( 0, m_processor->GetInputDataEntity(0));
			
			m_ExtractMultipleSurfaces->Update( );

			Core::DataTreeHelper::PublishOutput(
				m_ExtractMultipleSurfaces->GetOutputDataEntityHolder( 0 ),
				GetRenderingTree() );
		}
	}
	else
	{
		m_processor->Update( );
	}

}

void ptExtractMainSurfacePanelWidget::do_layout()
{
    // begin wxGlade: ptExtractMainSurfacePanelWidget::do_layout
    wxBoxSizer* GlobalSizer = new wxBoxSizer(wxVERTICAL);
	GlobalSizer->Add(m_OnlyMainChk, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 2);
	GlobalSizer->Add(btnExtractMainSurface, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 2);
	SetSizer(GlobalSizer);
    GlobalSizer->Fit(this);
    // end wxGlade
}


Core::BaseProcessor::Pointer ptExtractMainSurfacePanelWidget::GetProcessor()
{
	return m_processor.GetPointer();
}

bool ptExtractMainSurfacePanelWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = wxPanel::Enable( enable );

	try
	{
		const std::string helpStr = \
			"Extract Main Surface: extract the main (biggest) surface." \
			"\n\nUsage: no options, just run the tool.";
		SetInfoUserHelperWidget( helpStr );
	}
	coreCatchExceptionsReportAndNoThrowMacro("ptExtractMainSurfacePanelWidget::Enable");

	return bReturn;
}
