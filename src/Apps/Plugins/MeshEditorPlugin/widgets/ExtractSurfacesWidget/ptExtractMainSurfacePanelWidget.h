/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef ptExtractMainSurfacePanelWidget_H
#define ptExtractMainSurfacePanelWidget_H

#include <wx/wx.h>
#include <wx/image.h>

#include "corePluginMacros.h"
#include "coreProcessingWidget.h"
#include "coreVTKProcessor.h"
#include "meVTKExtractMainSurfaceFilter.h"
#include "coreExtractSurfacesProcessor.h"

#define wxID_btnExtractMainSurface wxID_HIGHEST + 1

#define wxID_ExtractSurfaceWidget wxID("wxID_ExtractSurfaceWidget")

/**
\brief Pw for extract surface processor
\ingroup MeshEditorPlugin
\author Chiara Riccobene
\date 5 Nov 09
*/

class PLUGIN_EXPORT ptExtractMainSurfacePanelWidget: 
	public wxPanel,
	public Core::Widgets::ProcessingWidget 
{
public: 
	typedef Core::VTKProcessor<meVTKExtractMainSurfaceFilter> ExtractMainSurfaceProcessor;

	coreDefineBaseWindowFactory( ptExtractMainSurfacePanelWidget )

	//!
    ptExtractMainSurfacePanelWidget(wxWindow* parent, 
								int id = wxID_ExtractSurfaceWidget, 
								const wxPoint& pos=wxDefaultPosition, 
								const wxSize& size=wxDefaultSize, 
								long style=wxDEFAULT_DIALOG_STYLE);

	virtual void OnButtonExtractMainSurface(wxCommandEvent &event); // wxGlade: <event_handler>

private:
    // begin wxGlade: ptExtractMainSurfacePanelWidget::methods
    void do_layout();
    // end wxGlade
    //! Enable the widget
	bool Enable( bool enable = true );
	//!
	Core::BaseProcessor::Pointer GetProcessor( );

protected:
    wxCheckBox* m_OnlyMainChk;
    wxButton* btnExtractMainSurface;
    
    wxDECLARE_EVENT_TABLE();
private:

	//!
	ExtractMainSurfaceProcessor::Pointer m_processor;
	Core::ExtractSurfacesProcessor::Pointer m_ExtractMultipleSurfaces;
}; // wxGlade: end class

#endif // ptExtractMainSurfacePanelWidget_H
