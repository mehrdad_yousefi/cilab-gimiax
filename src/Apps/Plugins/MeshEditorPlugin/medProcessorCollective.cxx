/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "medProcessorCollective.h"
#include "coreNetgenTimeStepImpl.h"

#include "coreWxMitkGraphicalInterface.h"

medProcessorCollective::medProcessorCollective()
{
	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
		Core::DataEntityImpl::GetNameClass( ), Core::NetgenImpl::Factory::New() );
}

medProcessorCollective::~medProcessorCollective()
{
}