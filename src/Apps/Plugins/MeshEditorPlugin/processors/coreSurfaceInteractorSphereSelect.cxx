/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreSurfaceInteractorSphereSelect.h"
#include "coreVTKPolyDataHolder.h"
#include "coreException.h"
#include "coreDataTreeMITKHelper.h"

#include "vtkPolyDataNormals.h"
#include "vtkCellData.h"
#include "vtkPolyDataWriter.h"


/**
 */
Core::SurfaceInteractorSphereSelect::SurfaceInteractorSphereSelect(
						Core::RenderingTree::Pointer renderingTree,
						Core::DataEntityHolder::Pointer selectedPoints,
						Core::DataEntityHolder::Pointer selectedData )
	:	SurfaceInteractor( renderingTree, selectedPoints, selectedData )
{
}


/**
 */
Core::SurfaceInteractorSphereSelect::~SurfaceInteractorSphereSelect( )
{
	DisconnectFromDataTreeNode();
}

void Core::SurfaceInteractorSphereSelect::CreateInteractor()
{
	vtkPolyDataPtr polyData;
	if ( GetSelectedDataEntity( ) )
	{
		GetSelectedDataEntity( )->GetProcessingData( polyData );
	}

	if ( polyData == NULL )
	{
		throw Core::Exceptions::Exception( 
			"Core::SurfaceInteractor::CreateInteractor",
			"Input mesh cannot be NULL" );
	}

	if ( m_surfaceSelectInteractor.IsNull() )
	{
		m_surfaceSelectInteractor = blMitkSurfaceSelectInteractor::New( 
			"SurfaceSelectInteractor", 
			NULL,
			polyData,
			GetSphereNode(),
			GetPointSet());
	}
}

void Core::SurfaceInteractorSphereSelect::DestroyInteractor()
{
	m_surfaceSelectInteractor = NULL ;
}

void Core::SurfaceInteractorSphereSelect::OnInteractorConnected()
{
	SurfaceInteractor::OnInteractorConnected( );
}

mitk::Interactor* Core::SurfaceInteractorSphereSelect::GetInternalInteractor()
{
	return m_surfaceSelectInteractor;
}


int Core::SurfaceInteractorSphereSelect::GetDirection( )
{
	int iDirection = -1;
	if ( m_surfaceSelectInteractor.IsNotNull() )
	{
		iDirection = m_surfaceSelectInteractor->GetDirection( );
	}
	return iDirection;
}


void Core::SurfaceInteractorSphereSelect::SetBoundingBox( double bounds[ 6 ] )
{
	if ( m_surfaceSelectInteractor.IsNotNull() )
	{
		m_surfaceSelectInteractor->SetBoundingBox( bounds );
	}
}

vtkSphere* Core::SurfaceInteractorSphereSelect::GetSelectionSphere()
{
	vtkSphere* sphere = NULL;

	if ( m_surfaceSelectInteractor.IsNotNull( ) )
	{
		sphere = m_surfaceSelectInteractor->GetImplicitSphere();		
	}
	return sphere;
}

Core::DataEntityType Core::SurfaceInteractorSphereSelect::GetInputTypes( )
{
	return Core::DataEntityType( 
		Core::SurfaceMeshTypeId | 
		Core::SkeletonTypeId );
}

int Core::SurfaceInteractorSphereSelect::GetTimeStep()
{
	return m_surfaceSelectInteractor->GetTimeStep( );
}

void Core::SurfaceInteractorSphereSelect::SetSelectionSphereHolder( 
	Core::DataEntityHolder::Pointer sphereHolder )
{
	m_selectionSphere = sphereHolder;
	GetRenderingTree()->Add(m_selectionSphere->GetSubject(),
		false,
		false);
}

mitk::DataTreeNode::Pointer Core::SurfaceInteractorSphereSelect::GetSphereNode()
{
	mitk::DataTreeNode::Pointer node;
	boost::any anyData = GetRenderingTree()->GetNode( m_selectionSphere->GetSubject( ) );
	Core::CastAnyProcessingData( anyData, node );
	return node;
}

void Core::SurfaceInteractorSphereSelect::SelectSurface()
{
	if (GetSelectionSphere() && (GetPointSet()->GetSize() == 2) &&
		GetSelectionSphere()->GetRadius()>0)
	{
		// Clean Input before extracting cells avoid misbehavior
		Core::vtkPolyDataPtr  polydataInput;
		GetSelectedDataEntity()->GetProcessingData(polydataInput);
	
		vtkSmartPointer<vtkIdList> pList= vtkSmartPointer<vtkIdList>::New();
		pList->DeepCopy( SelectCells(GetSelectionSphere() ));
	
		int selectionValue;
		if (GetFlagSelection())	selectionValue = 20;
		else selectionValue = 0;


		std::vector<std::pair<vtkIdType,int> > selected;

		// For each cell id at input
		for (int j = 0; j< pList->GetNumberOfIds(); j++)
		{
			int oldSelectionValue = polydataInput->GetCellData()->GetArray("select")->GetTuple1(pList->GetId(j));
			polydataInput->GetCellData()->GetArray("select")->SetTuple1(pList->GetId(j),selectionValue);

			if ( oldSelectionValue != selectionValue )
			{
				selected.push_back(std::make_pair(pList->GetId(j),selectionValue));
			}
		}

		if (m_CallBack)
		{
			m_CallBack->PointsModified(selected);
		}


		m_surfaceSelectInteractor->ResetSphere();

		polydataInput->GetCellData()->Modified( );
		mitk::RenderingManager::GetInstance()->RequestUpdateAll( mitk::RenderingManager::REQUEST_UPDATE_3DWINDOWS );
	}
}

void Core::SurfaceInteractorSphereSelect::DeselectSurface()
{

}
