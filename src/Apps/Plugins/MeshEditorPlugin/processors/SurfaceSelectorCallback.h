#ifndef SURFACESELECTORCALLBACK_H
#define SURFACESELECTORCALLBACK_H

/** 
\brief Surface Selector Callback
\ingroup MeshEditorPlugin
\author Albert Sanchez
\date 16 Mar 2011
*/

class SurfaceSelectorCallback
{
public:

	virtual ~SurfaceSelectorCallback( ){};
	virtual void PointsModified(std::vector<std::pair<vtkIdType,int> > selected) = 0;
};



#endif // SURFACESELECTORCALLBACK_H
