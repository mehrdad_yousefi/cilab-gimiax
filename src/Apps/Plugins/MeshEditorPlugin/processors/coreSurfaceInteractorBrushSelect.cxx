/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreSurfaceInteractorBrushSelect.h"
#include "coreVTKPolyDataHolder.h"
#include "coreException.h"
#include "vtkCellData.h"
#include "coreDataTreeMITKHelper.h"

/**
 */
Core::SurfaceInteractorBrushSelect::SurfaceInteractorBrushSelect(
						Core::RenderingTree::Pointer renderingTree,
						Core::DataEntityHolder::Pointer selectedPoints,
						Core::DataEntityHolder::Pointer selectedData )
	:SurfaceInteractor( renderingTree, selectedPoints, selectedData )
{
}


/**
 */
Core::SurfaceInteractorBrushSelect::~SurfaceInteractorBrushSelect( )
{
	DisconnectFromDataTreeNode();
}

void Core::SurfaceInteractorBrushSelect::CreateInteractor()
{
	vtkPolyDataPtr polyData;
	if ( GetSelectedDataEntity( ) )
	{
		GetSelectedDataEntity( )->GetProcessingData( polyData );
	}

	if ( polyData == NULL )
	{
		throw Core::Exceptions::Exception( 
			"Core::SurfaceInteractor::CreateInteractor",
			"Input mesh cannot be NULL" );
	}

	if ( m_surfaceSelectInteractor.IsNull() )
	{
		m_surfaceSelectInteractor = blMitkTriangleSelectInteractor::New( 
			"SurfaceSelectBrushInteractor", 
			//GetSelectedPointsNode(),
			NULL,
			polyData,
			GetPointSet());
	}
}

void Core::SurfaceInteractorBrushSelect::DestroyInteractor()
{
	m_surfaceSelectInteractor = NULL ;
}

void Core::SurfaceInteractorBrushSelect::OnInteractorConnected()
{
	SurfaceInteractor::OnInteractorConnected( );
}

mitk::Interactor* Core::SurfaceInteractorBrushSelect::GetInternalInteractor()
{
	return m_surfaceSelectInteractor;
}

Core::DataEntityType Core::SurfaceInteractorBrushSelect::GetInputTypes( )
{
	return Core::DataEntityType( 
		Core::SurfaceMeshTypeId | 
		Core::SkeletonTypeId );
}

int Core::SurfaceInteractorBrushSelect::GetTimeStep()
{
	return m_surfaceSelectInteractor->GetTimeStep( );
}

vtkSmartPointer<vtkIdList> ListsUnion(vtkSmartPointer<vtkIdList> mainList, vtkSmartPointer<vtkIdList> toInsert)
{
	vtkSmartPointer<vtkIdList> result = vtkSmartPointer<vtkIdList>::New() ;

	for (int i=0; i<toInsert->GetNumberOfIds(); i++)
	{
		vtkIdType id = toInsert->GetId(i);
		if (mainList->IsId(id) == vtkIdType(-1))
		{
			mainList->InsertNextId(id);
		}
	}

	return result;
}

void Core::SurfaceInteractorBrushSelect::SelectSurface()
{
	m_surfaceSelectInteractor->SetRadius(GetValue());
	m_surfaceSelectInteractor->GetImplicitSphere()->SetRadius(GetValue());
	vtkSmartPointer<vtkIdList> pList = vtkSmartPointer<vtkIdList>::New();
	vtkIdType currentPointId;
	vtkIdType currentCellId = -1;
	Core::vtkPolyDataPtr  polydataInput;
	GetSelectedDataEntity()->GetProcessingData(polydataInput);

	Core::vtkPolyDataPtr  pointInput;
	GetSelectedPointsDataEntity()->GetProcessingData(pointInput);

	// Call callback only when point has been removed (user released mouse button)
	if ( pointInput->GetNumberOfPoints( ) != 0 )
	{
		// Get selected point cell ID
		currentPointId = polydataInput->FindPoint(pointInput->GetPoint(0));
		polydataInput->GetPointCells(currentPointId,pList);

		for (int k =0;k<pList->GetNumberOfIds();k++)
		{
			double bounds[6];
			bounds[0]=bounds[1]=bounds[2]=bounds[3]=bounds[4]=bounds[5]=0;
			polydataInput->GetCellBounds(pList->GetId(k), bounds);
			if ( pointInput->GetPoint(0)[0]>bounds[0] && pointInput->GetPoint(0)[0]<bounds[1]&&
				pointInput->GetPoint(0)[1]>bounds[2] && pointInput->GetPoint(0)[1]<bounds[3] &&
				pointInput->GetPoint(0)[2]>bounds[4] && pointInput->GetPoint(0)[2]<bounds[5])
				currentCellId = pList->GetId(k);
		}

		if (currentCellId == -1)
			currentCellId = pList->GetId(0);

		if (currentCellId < -1)
			return;

		if ( polydataInput->GetCellData() == NULL ||
			 polydataInput->GetCellData()->GetArray("select") == NULL )
		{
			throw Core::Exceptions::Exception( 
				"SurfaceInteractorBrushSelect::SelectSurface", 
				"Cannot find select array" );
		}

		// Set selection value
		int selectionValue;
		if (GetFlagSelection())	selectionValue = 20;
		else selectionValue = 0;
	
		//Select Triangles of brush
		vtkSmartPointer<vtkIdList> cellsList = vtkSmartPointer<vtkIdList>::New() ;

		// Get list of selected cells of input surface mesh
		cellsList->DeepCopy(SelectCells(GetSelectionSphere()));
	
		//anyway: add at least the cells containing the point if the cellsList is empty!
		if(cellsList->GetNumberOfIds()<=0)
		{
			vtkSmartPointer<vtkIdList> cellsWithCurrenPoint =  vtkSmartPointer<vtkIdList>::New();
			polydataInput->GetPointCells(currentPointId, cellsWithCurrenPoint);
			for(int jj=0; jj< cellsWithCurrenPoint->GetNumberOfIds(); jj++)
			{
				cellsList->InsertNextId(cellsWithCurrenPoint->GetId(jj));
			}
		}
	
		// Refresh new selection
		for (int j = 0; j< cellsList->GetNumberOfIds(); j++)
		{
			int oldSelectionValue = polydataInput->GetCellData()->GetArray("select")->GetTuple1(cellsList->GetId(j));
			polydataInput->GetCellData()->GetArray("select")->SetTuple1(cellsList->GetId(j),selectionValue);

			if ( oldSelectionValue != selectionValue )
			{
				m_SelectedCells.push_back(std::make_pair(cellsList->GetId(j),selectionValue));
			}
		}

		polydataInput->GetCellData()->Modified( );
		mitk::RenderingManager::GetInstance()->RequestUpdateAll( mitk::RenderingManager::REQUEST_UPDATE_3DWINDOWS );
	}
	// Call callback only when point has been removed (user released mouse button)
	else if (m_CallBack && pointInput->GetNumberOfPoints( ) == 0 )
	{
		m_CallBack->PointsModified(m_SelectedCells);
		m_SelectedCells.resize( 0 );
	}

}

void Core::SurfaceInteractorBrushSelect::DeselectSurface()
{
	
}

vtkSphere* Core::SurfaceInteractorBrushSelect::GetSelectionSphere()
{
	vtkSphere* sphere = NULL;

	if ( m_surfaceSelectInteractor.IsNotNull( ) )
	{
		sphere = m_surfaceSelectInteractor->GetImplicitSphere();		
	}
	return sphere;
}

void Core::SurfaceInteractorBrushSelect::SetRadius( double val )
{
	m_surfaceSelectInteractor->SetRadius(val);
}

double Core::SurfaceInteractorBrushSelect::GetRadius()
{
	return m_surfaceSelectInteractor->GetRadius();
}