/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreExtractSurfacesProcessor_H
#define _coreExtractSurfacesProcessor_H

#include "corePluginMacros.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreBaseFilter.h"
#include "coreBaseProcessor.h"

namespace Core{

/**
Extract surfaces

\ingroup MeshEditorPlugin
\author Albert Sanchez
\date 14 feb 2011
*/

class PLUGIN_EXPORT ExtractSurfacesProcessor : public Core::BaseProcessor
{
public:
	coreDeclareSmartPointerClassMacro(ExtractSurfacesProcessor, Core::BaseProcessor);

	typedef enum
	{
		INPUT_MESH,
		NUMBER_OF_INPUTS,
	} INPUT_TYPE;

	typedef enum
	{
		OUTPUT_0,
		NUMBER_OF_OUTPUTS = 100,
	} OUTPUT_TYPE;

	//!
	void Update();
	
private:
	/**
	*/
	ExtractSurfacesProcessor( );
	
};

} // Core

#endif //_coreExtractSurfacesProcessor_H
