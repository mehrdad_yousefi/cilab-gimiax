/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "ReplaceScalarProcessor.h"

#include "vtkCellData.h"
#include "vtkAppendPolyData.h"

#include "vtkCellDataToPointData.h"

ReplaceScalarProcessor::ReplaceScalarProcessor( )
{
	SetName( "ReplaceScalarProcessor" );
	
	BaseProcessor::SetNumberOfInputs( INPUTS_NUMBER );
	GetInputPort(INPUT_SURFACE)->SetName( "Surface" );
	GetInputPort(INPUT_SURFACE)->SetDataEntityType( Core::SurfaceMeshTypeId);
	
	BaseProcessor::SetNumberOfOutputs( OUTPUTS_NUMBER );
	GetInputPort(OUTPUT_SURFACE)->SetDataEntityType( Core::SurfaceMeshTypeId);

	m_ScalarValue = 0;
	m_ScalarName.name = "";
	m_ScalarName.mode = blShapeUtils::ShapeUtils::SCALAR_ARRAY_NONE;
}

ReplaceScalarProcessor::~ReplaceScalarProcessor()
{
}

void ReplaceScalarProcessor::Update()
{
	Core::vtkPolyDataPtr inputMesh;
	GetProcessingData( INPUT_SURFACE, inputMesh );

	if ( !inputMesh->GetCellData() )
	{
		throw Core::Exceptions::Exception( "ReplaceScalarProcessor::Update", "Cannot find CellData" );
	}

	Core::vtkPolyDataPtr output = Core::vtkPolyDataPtr::New( );
	output->DeepCopy( inputMesh );

	// Get selected cells
	vtkDataArray* selected = output->GetCellData()->GetArray("select");
	if ( !selected )
	{
		throw Core::Exceptions::Exception( "ReplaceScalarProcessor::Update", "Cannot find selected array" );
	}

	// Find scalar
	vtkDataArray* vtkArray = blShapeUtils::ShapeUtils::GetArray( output, m_ScalarName );
	if ( vtkArray == NULL )
	{
		throw Core::Exceptions::Exception( "ReplaceScalarProcessor::Update", "Cannot find input array" );
	}

	// Replace values
	vtkIdType N = vtkArray->GetNumberOfTuples( );
	vtkSmartPointer<vtkIdList> pointCells = vtkSmartPointer<vtkIdList>::New( );
	for ( vtkIdType dataId = 0; dataId < N; ++dataId )
	{
		switch ( m_ScalarName.mode )
		{
		case blShapeUtils::ShapeUtils::SCALAR_ARRAY_POINT_DATA:
			{
				// Get all cells that uses current point
				output->GetPointCells( dataId, pointCells );
				
				// Check if any cell has been selected
				for ( vtkIdType idCell = 0 ; idCell < pointCells->GetNumberOfIds( ) ; idCell++ )
				{
					if ( selected->GetTuple1( pointCells->GetId( idCell ) ) == 20 )
					{
						vtkArray->SetTuple1( dataId, m_ScalarValue );
						break;
					}
				}
			}
			break;
		case blShapeUtils::ShapeUtils::SCALAR_ARRAY_CELL_DATA:
			if ( selected->GetTuple1( dataId ) == 20 )
			{
				vtkArray->SetTuple1( dataId, m_ScalarValue );
			}
			break;
		}
	}

	UpdateOutput( OUTPUT_SURFACE, output, "Replaced Scalar", true, 1, GetInputDataEntity( INPUT_SURFACE ) );
}

double ReplaceScalarProcessor::GetScalarValue( ) const
{
	return m_ScalarValue;
}

void ReplaceScalarProcessor::SetScalarValue( double val )
{
	m_ScalarValue = val;
}

void ReplaceScalarProcessor::SetSelectedScalarArray( 
	const blShapeUtils::ShapeUtils::VTKScalarType &scalarName )
{
	m_ScalarName = scalarName;
}
