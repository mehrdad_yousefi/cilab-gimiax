// Copyright 2011 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "ExtractContoursProcessor.h"

#include "coreDataEntity.h"
#include "coreVTKPolyDataHolder.h"
#include "coreVTKImageDataHolder.h"

#include "mitkImage.h"
#include "mitkSurface.h"
#include "mitkGeometry2DDataToSurfaceFilter.h"
#include "mitkPlaneGeometry.h"
#include "mitkSlicedGeometry3D.h"

#include "vtkCutter.h"
#include "vtkPlane.h"
#include "vtkAppendPolyData.h"

ExtractContoursProcessor::ExtractContoursProcessor( )
{
	// name
    SetName( "ExtractContoursProcessor" );
	// input
    SetNumberOfInputs( 2 );
	GetInputPort(0)->SetName( "Input Surface" );
	GetInputPort(0)->SetDataEntityType( Core::SurfaceMeshTypeId );
	GetInputPort(1)->SetName( "Input Image" );
	GetInputPort(1)->SetDataEntityType( Core::ImageTypeId );
	// output
    SetNumberOfOutputs( 1 );
    GetOutputPort(0)->SetReuseOutput(false);
}

ExtractContoursProcessor::~ExtractContoursProcessor()
{
}

void ExtractContoursProcessor::Update()
{
	// get the input mesh 
	Core::vtkPolyDataPtr inputMesh;
    GetProcessingData( 0, inputMesh );

	// Generate local variable
	mitk::Image::Pointer mitkImage;
	GetProcessingData( 1, mitkImage );

	mitk::SlicedGeometry3D::Pointer slicedWorldGeometry = NULL;
	slicedWorldGeometry = dynamic_cast< mitk::SlicedGeometry3D * >(
		mitkImage->GetTimeSlicedGeometry()->GetGeometry3D( GetInputPort( 1 )->GetSelectedTimeStep() ) );
	Core::vtkImageDataPtr vtkImage = mitkImage->GetVtkImageData( GetInputPort( 1 )->GetSelectedTimeStep() );

	vtkSmartPointer<vtkAppendPolyData> append = vtkSmartPointer<vtkAppendPolyData>::New();

	for ( unsigned int s = 0 ; s < slicedWorldGeometry->GetSlices() ; s++ )
	{
		mitk::PlaneGeometry::Pointer requestedslice;
		requestedslice = static_cast< mitk::PlaneGeometry * >(
			slicedWorldGeometry->GetGeometry2D( s ) );

		mitk::Point3D origin = requestedslice->GetOrigin();
		mitk::Vector3D normal = requestedslice->GetNormal( );

		vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
		vtkSmartPointer<vtkCutter> cutter = vtkSmartPointer<vtkCutter>::New();
		cutter->SetCutFunction(plane.GetPointer());
		cutter->SetInput( inputMesh );

		vtkFloatingPointType vp[3], vnormal[3];
		vp[ 0 ] = vtkImage->GetOrigin()[ 0 ];
		vp[ 1 ] = vtkImage->GetOrigin()[ 1 ];
		vp[ 2 ] = vtkImage->GetOrigin()[ 2 ] + s * vtkImage->GetSpacing()[ 2 ];
		plane->SetOrigin(vp);
		vnormal[ 0 ] = normal[ 0 ];
		vnormal[ 1 ] = normal[ 1 ];
		vnormal[ 2 ] = normal[ 2 ];
		plane->SetNormal(vnormal);

		cutter->Update();

		Core::vtkPolyDataPtr out = cutter->GetOutput();

		append->AddInput( out );
	}

	append->Update( );
	Core::vtkPolyDataPtr output = append->GetOutput();
	UpdateOutput( 0, output, "Extracted contours", true, 1, GetInputDataEntity( 0 ) );
}
