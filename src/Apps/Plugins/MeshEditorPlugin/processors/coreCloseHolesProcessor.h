/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreCloseHolesProcessor_H
#define _coreCloseHolesProcessor_H

#include "corePluginMacros.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreBaseFilter.h"
#include "coreBaseProcessor.h"
#include "meCloseHoles.h"
#include "vtkMEDFillingHole.h"


namespace Core{

/**
Close the surface using using meCloseHoles and meHoleFiller.
The user can choose different options.

\ingroup MeshEditorPlugin
\author Chiara Riccobene
\author Albert Sanchez
\date 24 sept 2010
*/

class PLUGIN_EXPORT CloseHolesProcessor : public Core::BaseProcessor
{
public:
	coreDeclareSmartPointerClassMacro(Core::CloseHolesProcessor, Core::BaseProcessor);

	typedef enum
	{
		INPUT_SURFACE,
		INPUT_POINT,
		NUMBER_OF_INPUTS,
	} INPUT_TYPE;

	typedef enum
	{
		OUTPUT_HOLE,
		SURFACE_CLOSED,
		SURFACE_HOLES,
		NUMBER_OF_OUTPUTS,
	} OUTPUT_TYPE;

	//!
	void Update();

	//!
	void ComputeHoles();
	//!
	bool CheckInputs();

	//!
	void SetParams (meMeshVolumeClosingParamsPtr param);
	meMeshVolumeClosingParamsPtr GetParams();

	//! see m_closeAll
	void SetCloseAll(bool val);
	
	//! see m_CloseHoleMethod
	void SetMethod( int method);

	//! selects the hole you want to fill in ( with m_closeAll false)
	void SelectHole();

	//! see m_PatchOutput
	void SetPatchOnly( bool val);

	//!
	void SetFillingType(bool smooth, bool smoothmembrane);
	
	//!
	vtkMEDFillingHole::FillingTypes GetFillingType();

	//!
	void SetThinPlateSteps(int n);

	//!
	int GetThinPlateSteps();

	//! 
	Core::vtkPolyDataPtr GetHoles();

	//! 
	Core::vtkPolyDataPtr GetInputHole();

	//!
	std::vector<std::string> GetLastPatches() { return m_lastPatches;};

	//! 
	//void SetSelectedPoint(Core::vtkPolyDataPtr selectedPoint){ m_SelectedPoint = selectedPoint;};

	
private:
	/**
	*/
	CloseHolesProcessor( );
	
	//! Jos�-Maria Algorithm to close holes ( methods 0-4)
	meCloseHoles::Pointer	m_meCloseHoles;

	//! parameters for CloseRefineFair algo
	meMeshVolumeClosingParamsPtr m_param;

	//! Choose to fill every hole or to not
	bool m_closeAll;

	//! Method to fill the hole
	int m_CloseHoleMethod;

	//! true returns patch, false returns closed mesh
	bool m_PatchOutput;

	vtkMEDFillingHole::FillingTypes m_FillingType;
	int m_ThinPlateSteps;

	Core::vtkPolyDataPtr m_Holes;
	Core::vtkPolyDataPtr m_InputHole;

	std::vector<std::string> m_lastPatches;
	//Core::vtkPolyDataPtr m_SelectedPoint;
	
};

} // Core

#endif //_coreCloseHolesProcessor_H
