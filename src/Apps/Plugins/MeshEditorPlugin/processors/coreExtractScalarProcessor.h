/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreExtractScalarProcessor_H
#define _coreExtractScalarProcessor_H

#include "corePluginMacros.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreBaseProcessor.h"

namespace Core{

/**
Cut the surface using using meRingCutAPI.

\ingroup MeshEditorPlugin
\author Chiara Riccobene
\date 6 nov 2009
*/

class PLUGIN_EXPORT ExtractScalarProcessor : public Core::BaseProcessor
{
public:
	coreDeclareSmartPointerClassMacro(Core::ExtractScalarProcessor, Core::BaseProcessor);
	//!
	void Update();

	//!
	std::string GetScalarName() const;
	void SetScalarName(std::string val);

private:
	//!
	ExtractScalarProcessor( );

private:
	//!
	std::string m_ScalarName;
	
};

} // Core

#endif //_coreExtractScalarProcessor_H
