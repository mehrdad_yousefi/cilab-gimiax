/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreSurfaceInteractorProcessor.h"
#include "coreImageDataEntityMacros.h"
#include "coreDataEntityHelper.h"
#include "coreReportExceptionMacros.h"

#include "vtkPointData.h"
#include "vtkCellData.h"
#include "blVTKHelperTools.h"
#include "blShapeUtils.h"


Core::SurfaceInteractorProcessor::SurfaceInteractorProcessor( )
{
	SetNumberOfInputs( NUMBER_OF_INPUTS );
	GetInputPort( INPUT_SURFACE )->SetName( "Input surface" );
	GetInputPort( INPUT_SURFACE )->SetDataEntityType( Core::SurfaceMeshTypeId );
	SetNumberOfOutputs( NUMBER_OF_OUTPUTS );
	SetOutputDataName( OUTPUT_SURFACE, "Output surface" );
	GetOutputPort( OUTPUT_SURFACE )->SetDataEntityType( Core::SurfaceMeshTypeId );
	// Always reuse output
	GetOutputPort( OUTPUT_SURFACE )->SetReuseOutput( true );

	SetName( "SurfaceInteractorProcessor" );

}

void Core::SurfaceInteractorProcessor::Update()
{
	// Get the mesh 
	Core::vtkPolyDataPtr vtkInputMesh;
	GetProcessingData( 0, vtkInputMesh );

	// Call the function
	Core::vtkPolyDataPtr outputMesh  = Core::vtkPolyDataPtr::New();
	outputMesh->DeepCopy( vtkInputMesh);

	if ( outputMesh.GetPointer() == NULL ||
		!outputMesh->GetCellData()->HasArray("select"))
	{
		throw Exception( "SurfaceInteractor cannot be applied" );
	}

	switch (m_processorType)
	{
	case RemoveCells:
		{
			outputMesh = blShapeUtils::ShapeUtils::GetShapeRegion(vtkInputMesh,0,0, "select");
			outputMesh->GetCellData()->RemoveArray("select");
			GetOutputPort( 0 )->UpdateOutput( outputMesh, 0, NULL );
		}
		break;
	case LocalRefiner:
		{
			LocalRefinerProcessor::Pointer refProc =
				LocalRefinerProcessor::New();
			refProc->SetInputDataEntity(0,GetInputDataEntity(0));
			refProc->GetFilter( )->GetParams( )->m_DensityFactor = sqrt(double(4));
			refProc->Update();
			GetOutputPort(0)->UpdateOutput(
				refProc->GetOutputPort(0)->GetDataEntity()->GetProcessingData(),
				0,
				NULL);
		}
		break;
	case LoopSubdivision:
		{
			LoopSubdivisionProcessor::Pointer loopProc =
				LoopSubdivisionProcessor::New();
			loopProc->SetInputDataEntity(0,GetInputDataEntity(0));
			loopProc->Update();
			GetOutputPort(0)->UpdateOutput(
				loopProc->GetOutputPort(0)->GetDataEntity()->GetProcessingData(),
				0,
				NULL);
		}
		break;
	case EdgeSwap:
		{
			EdgeSwappingProcessor::Pointer swapProc =
				EdgeSwappingProcessor::New();
			swapProc->SetInputDataEntity(0,GetInputDataEntity(0));
			swapProc->Update();
			GetOutputPort(0)->UpdateOutput(
				swapProc->GetOutputPort(0)->GetDataEntity()->GetProcessingData(),
				0,
				NULL);
		}
		break;
	case TaubinSmooth:
		{
			TaubinSmoothProcessor::Pointer smoothProc =
				TaubinSmoothProcessor::New();
			smoothProc->SetInputDataEntity(0,GetInputDataEntity(0));
			smoothProc->Update();
			GetOutputPort(0)->UpdateOutput(
				smoothProc->GetOutputPort(0)->GetDataEntity()->GetProcessingData(),
				0,
				NULL);
		}
		break;
	}

}

void Core::SurfaceInteractorProcessor::SetProcessorType( ProcessorType type )
{
	m_processorType = type;
}
