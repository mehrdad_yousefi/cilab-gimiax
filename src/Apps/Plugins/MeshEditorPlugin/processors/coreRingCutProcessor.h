/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreRingCutProcessor_H
#define _coreRingCutProcessor_H

#include "corePluginMacros.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreBaseFilter.h"
#include "coreBaseProcessor.h"
#include "meRingCut.h"

namespace Core{

/**
Cut the surface using using meRingCutAPI.

\ingroup MeshEditorPlugin
\author Chiara Riccobene
\date 6 nov 2009
*/

class PLUGIN_EXPORT RingCutProcessor : public Core::BaseProcessor
{
public:
	coreDeclareSmartPointerClassMacro(Core::RingCutProcessor, Core::BaseProcessor);
	//!
	void Update();
	//!
	void ComputePlaneUsingRingCut();
	//!
	bool CheckInputs();

private:
	/**
	*/
	RingCutProcessor( );
	
	meRingCut::Pointer	m_meRingCut;

};

} // Core

#endif //_coreRingCutProcessor_H
