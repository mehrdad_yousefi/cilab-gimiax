/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _BooleanOperationsProcessor_H
#define _BooleanOperationsProcessor_H

#include "coreBaseProcessor.h"


/**
Processor for 

\ingroup MeshEditorPlugin
\author Xavi Planes
\date 16 feb 2009
*/
class BooleanOperationsProcessor : public Core::BaseProcessor
{
public:

	typedef enum
	{
		INPUT_SURFACE_1,
		INPUT_SURFACE_2,
		INPUTS_NUMBER
	} INPUT_TYPE;

	typedef enum
	{
		OUTPUT_SURFACE,
		OUTPUTS_NUMBER
	}OUTPUT_TYPE;
public:
	//!
	coreProcessor(BooleanOperationsProcessor, Core::BaseProcessor);
	
	//! Call library to perform operation
	void Update( );

	//!
	void SubtractSurfaces();

	int GetOperation() const { return m_operation; }
	void SetOperation(int val) { m_operation = val; }

private:
	//!
	BooleanOperationsProcessor();

	//!
	~BooleanOperationsProcessor();

	//! Purposely not implemented
	BooleanOperationsProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );

	double m_computedMeasure;

	unsigned char m_lowerValue;

	unsigned char m_upperValue;

	int m_operation;

};
    

#endif //_BooleanOperationsProcessor_H
