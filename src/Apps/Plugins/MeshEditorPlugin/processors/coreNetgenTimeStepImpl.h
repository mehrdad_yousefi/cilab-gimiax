/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreNetgenTimeStepImpl_H
#define _coreNetgenTimeStepImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "meNetgenMesh.h"

namespace Core{


/**
Surface implementation for Netgen class

- "Points": std::vector<Point3D>*
- "SurfaceElements": std::vector<SurfaceElement3D>*

\author Xavi Planes
\date 03 nov 2009
\ingroup MeshEditorPlugin
*/
class PLUGIN_EXPORT NetgenImpl : public DataEntityImpl
{
public:
	coreDeclareSmartPointerClassMacro( Core::NetgenImpl, DataEntityImpl )

	coreDefineSingleDataFactory( NetgenImpl, meNetgenMesh::Pointer, SurfaceMeshTypeId )
	//@{ 
	/// \name Interface
public:
	boost::any GetDataPtr() const;
	virtual void CleanTemporalData( );
private:
	virtual void SetAnyData( boost::any val );
	virtual void SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem = gmCopyMemory );
	virtual void GetData( blTagMap::Pointer tagMap );
	virtual void ResetData( );
	//@}

private:
	//!
	NetgenImpl( );

	//!
	virtual ~NetgenImpl( );

private:

	//!
	meNetgenMesh::Pointer m_NetgenMesh;

	//! Temporal Data
	std::vector<Point3D> m_Points;

	//! Temporal Data
	std::vector<SurfaceElement3D> m_SurfaceElements;

};

} // end namespace Core

#endif // _coreNetgenTimeStepImpl_H

