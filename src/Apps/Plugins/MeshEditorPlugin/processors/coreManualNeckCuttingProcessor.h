/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef ManualNeckCuttingProcessor_H
#define ManualNeckCuttingProcessor_H

#include "coreBaseProcessor.h"

#include "itkImage.h"

namespace Core{

/**
Processor for subtracting two images

\ingroup SandboxPlugin
\author Berta Marti
\date 24 oct 2008
*/
class PLUGIN_EXPORT ManualNeckCuttingProcessor : public Core::BaseProcessor
{
public:
	//!
	coreDeclareSmartPointerClassMacro(ManualNeckCuttingProcessor, Core::SmartPointerObject);
	
	typedef enum
	{
		INPUT_ANEURYSM,
		INPUT_POINT,
		NUMBER_OF_INPUTS
	} INPUT_TYPE;

	typedef enum
	{
		LOOP_POINTS,
		OUTPUT_MESH1,
		OUTPUT_MESH2,
		NUMBER_OF_OUTPUTS
	} OUTPUT_TYPE;

	
	typedef itk::Image<float,3> ImageType;
		
	//! Call library to perform operation
	void Update( );

	vtkSmartPointer< vtkPoints> pointSet;

	void CreateLoopView();

	//! Return the index of the output mesh corresponding to the sac (0 if sac not identified)
	int GetSacIndex(){return m_sacIndex;};

	//! Set the index of the output mesh
	void SetSacIndex(int sacIndex){ m_sacIndex = sacIndex; };

	//! Clean up data when the widget is closed
	void CleanUp();

	//! Use the best guess for the neck on the basis of the mesh having the lower number of holes (the first one if equals). Otherwise use the inverse criteria (major number)
	void SetUseMinorNumberOfHolesGuess(bool bVal); 

	bool GetUseMinorNumberOfHolesGuess( ); 

private:
	//!
	ManualNeckCuttingProcessor();

	//!
	~ManualNeckCuttingProcessor();

	//! Purposely not implemented
	ManualNeckCuttingProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );

	//! Get the polydata with the lines passing through the landmarks
	Core::vtkPolyDataPtr GetLandmarkLine(
		Core::vtkPolyDataPtr landmarks,
		Core::vtkPolyDataPtr inputMesh );

	/** Get the polydata with the lines passing through the landmarks
	with 2 points each line and orient polydata
	*/
	Core::vtkPolyDataPtr GetLandmarkLineRefined(
		Core::vtkPolyDataPtr landmarks );

	//! identify which mesh output is the sac (1=first; 2=second)..if 0, sac not identified
	int m_sacIndex;

	bool m_useMinorNumberOfHoles;



	
};
    
} // namespace Core{

#endif //
