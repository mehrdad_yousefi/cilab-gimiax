/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreCloseHolesProcessor.h"
#include "coreDataEntityHelper.h"
#include "coreEnvironment.h"

#include "vtkPointSet.h"
#include "vtkCleanPolyData.h"

#include "meCloseHoles.h"
#include "meHoleFiller.h"

#include "blShapeUtils.h"
#include "blTextUtils.h"

Core::CloseHolesProcessor::CloseHolesProcessor( )
{
	SetNumberOfInputs( NUMBER_OF_INPUTS ); 
	GetInputPort( INPUT_SURFACE )->SetName( "Input surface" );
	GetInputPort( INPUT_SURFACE )->SetDataEntityType( Core::SurfaceMeshTypeId );
	GetInputPort( INPUT_POINT )->SetName( "Input Point Hole" );
	GetInputPort( INPUT_POINT )->SetDataEntityType( Core::PointSetTypeId );

	SetNumberOfOutputs( NUMBER_OF_OUTPUTS );
	GetOutputPort( SURFACE_CLOSED )->SetDataEntityType( Core::SurfaceMeshTypeId );
	GetOutputPort( SURFACE_CLOSED )->SetReuseOutput( true );
	GetOutputPort( SURFACE_CLOSED )->SetDataEntityName( "SurfaceClosed" );
	GetOutputPort( SURFACE_HOLES )->SetDataEntityType(  Core::SkeletonTypeId | Core::SurfaceMeshTypeId );
	GetOutputPort( SURFACE_HOLES )->SetReuseOutput( true );
	GetOutputPort( SURFACE_HOLES )->SetDataEntityName( "SurfaceHoles" );

	SetName( "CloseHolesProcessor" );
	
	m_param = meMeshVolumeClosingParamsPtr( new meMeshVolumeClosingParams);
	m_meCloseHoles = meCloseHoles::New();

	m_CloseHoleMethod = 4;
	m_closeAll = true;
	m_PatchOutput = false;

	m_Holes = Core::vtkPolyDataPtr::New();
	m_InputHole = NULL;
}

void Core::CloseHolesProcessor::Update()
{
	Core::vtkPolyDataPtr input;
	Core::vtkPolyDataPtr output = vtkPolyData::New();
	std::string name;
	
	GetProcessingData(INPUT_SURFACE,input);

	if(m_CloseHoleMethod < 6)
	{

		m_meCloseHoles->SetAlgorithm((meCloseHoles::closeHoleAlgorithmEnum)m_CloseHoleMethod);
		m_meCloseHoles->SetInput(input);

		if (m_closeAll)
		{
			m_meCloseHoles->Update();
		}
		else
		{
			if ( m_InputHole ) m_meCloseHoles->SetInputHole( m_InputHole );
			m_meCloseHoles->Run();
		}	
	
		if (!m_PatchOutput)
		{
			name = "SurfaceClosed";
			output->DeepCopy(m_meCloseHoles->GetOutput());
			UpdateOutput(SURFACE_CLOSED, output, name , true ,1, GetInputDataEntity(INPUT_SURFACE) );
		}
		else
		{	
			int size = m_meCloseHoles->GetPatchOutput().size();
			m_lastPatches.clear();

			Core::DataEntity::Pointer inputDataEntity = GetInputDataEntity(INPUT_SURFACE);
			for (int i=0;i< size;i++)
			{
				std::ostringstream currentFileName;
				name = "Patch_";
				currentFileName << name << i;
				output->DeepCopy(m_meCloseHoles->GetPatchOutput().at(i));
				UpdateOutput(SURFACE_CLOSED,output,currentFileName.str(),true,1, inputDataEntity);
				m_lastPatches.push_back(currentFileName.str());
			}
		}
	}
	else if (m_CloseHoleMethod == 6) //"Peter Liepa Algorithm"
	{
		vtkSmartPointer<vtkMEDFillingHole> fillingHoleFilter = vtkMEDFillingHole::New();
		fillingHoleFilter->SetInput(input);
		if (m_closeAll)
		{
			fillingHoleFilter->SetFillingType(m_FillingType);
			fillingHoleFilter->SetSmoothThinPlateSteps(m_ThinPlateSteps);
			fillingHoleFilter->SetFillAllHole();  
			fillingHoleFilter->Update();  
		}
		else
		{
			fillingHoleFilter->SetFillingType(m_FillingType);
			fillingHoleFilter->SetSmoothThinPlateSteps(m_ThinPlateSteps);
			
			int pointID = input->FindPoint(m_InputHole->GetPoint(0));
			fillingHoleFilter->SetFillAHole(pointID);  
			
			fillingHoleFilter->Update();  
		}
		name = "SurfaceClosed";
		output->DeepCopy(fillingHoleFilter->GetOutput());
		UpdateOutput(SURFACE_CLOSED,output,name,true,1,GetInputDataEntity(INPUT_SURFACE));
		m_InputHole = NULL;
		
	}

}

void Core::CloseHolesProcessor::ComputeHoles()
{
	try
	{
		Core::vtkPolyDataPtr input;
		GetProcessingData(INPUT_SURFACE,input);

		vtkSmartPointer<vtkPolyDataNormals>  normals = 
			vtkSmartPointer<vtkPolyDataNormals>::New();
		normals->SetInput(input);
		normals->SplittingOff();
		normals->SetFeatureAngle(180.0);
		normals->ComputePointNormalsOff();
		normals->ComputeCellNormalsOn();
		normals->AutoOrientNormalsOn();
		normals->ConsistencyOn();
		normals->Update();
		normals->GetOutput()->BuildLinks();

		vtkSmartPointer<vtkFeatureEdges>  edges = vtkSmartPointer<vtkFeatureEdges>::New();
		edges->SetInput(normals->GetOutput());
		edges->BoundaryEdgesOn();
		edges->FeatureEdgesOff();
		edges->ManifoldEdgesOff();
		edges->NonManifoldEdgesOff();
		edges->Update();          

		//PolyDataConnectivityFilter: each hole is identified as a region
		vtkSmartPointer<vtkPolyDataConnectivityFilter> connec = 
			vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
		connec->SetInput(edges->GetOutput());
		connec->SetExtractionModeToAllRegions();
		connec->SetColorRegions(1);
		connec->Update();

		vtkSmartPointer<vtkCleanPolyData> cleaner = vtkCleanPolyData::New();
		cleaner->SetInput(connec->GetOutput());
		cleaner->Update();

		Core::vtkPolyDataPtr output = Core::vtkPolyDataPtr::New();
		output->DeepCopy(cleaner->GetOutput());
		m_Holes->DeepCopy(output);
		m_InputHole = NULL;

		UpdateOutput( SURFACE_HOLES, output, "SurfaceHoles", true, 1);//,GetInputDataEntity( INPUT_SURFACE ) );
	}
	coreCatchExceptionsAddTraceAndThrowMacro(Core::CloseHolesProcessor::ComputeHoles() );


}

Core::vtkPolyDataPtr Core::CloseHolesProcessor::GetHoles()
{
	return m_Holes;
}

Core::vtkPolyDataPtr Core::CloseHolesProcessor::GetInputHole()
{
	return m_InputHole;
}

bool Core::CloseHolesProcessor::CheckInputs()
{

	Core::vtkPolyDataPtr polydata ;
	GetProcessingData( INPUT_SURFACE , polydata );
	return true;

}

void Core::CloseHolesProcessor::SetCloseAll( bool val )
{
	m_closeAll = val;
}

void Core::CloseHolesProcessor::SetParams( meMeshVolumeClosingParamsPtr param )
{
	m_param = param;
}

meMeshVolumeClosingParamsPtr Core::CloseHolesProcessor::GetParams()
{
	return m_param;
}

void Core::CloseHolesProcessor::SetMethod( int method )
{
	m_CloseHoleMethod = method;
}

void Core::CloseHolesProcessor::SelectHole()
{

	try
	{
		Core::vtkPolyDataPtr holes;
		Core::DataEntityHelper::GetProcessingData(
			GetOutputDataEntityHolder(SURFACE_HOLES),
			holes);

		Core::vtkPolyDataPtr polydata ;
		GetProcessingData( INPUT_POINT , polydata );

		if ( polydata->GetNumberOfPoints() != 1)
			return;
		
		vtkSmartPointer<vtkPolyDataConnectivityFilter> connectivity =
			vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
		connectivity->SetInput(holes);
		connectivity->SetExtractionModeToClosestPointRegion();
		connectivity->SetClosestPoint(polydata->GetPoint(0));
		connectivity->Update();

		if(m_InputHole==NULL)
			m_InputHole = vtkPolyData::New();
		m_InputHole->DeepCopy(connectivity->GetOutput());

		UpdateOutput(OUTPUT_HOLE,m_InputHole,"Selected Hole", true,1,GetOutputDataEntity(SURFACE_HOLES));
	}
	coreCatchExceptionsAddTraceAndThrowMacro("SelectHole")


}

void Core::CloseHolesProcessor::SetPatchOnly( bool val )
{
	m_PatchOutput = val;
}

void Core::CloseHolesProcessor::SetFillingType(bool smooth, bool smoothmembrane)
{ 
	if (!smooth)
	{
		m_FillingType = vtkMEDFillingHole::Flat;
	}
	else
	{
		if (smoothmembrane)
		{
			m_FillingType = vtkMEDFillingHole::SmoothMembrane;
		}
		else
		{
			m_FillingType = vtkMEDFillingHole::SmoothThinPlate;
		}
	}
}

vtkMEDFillingHole::FillingTypes Core::CloseHolesProcessor::GetFillingType()
{ 
	return m_FillingType; 
}

void  Core::CloseHolesProcessor::SetThinPlateSteps(int n) { 
	m_ThinPlateSteps = n; 
}

int  Core::CloseHolesProcessor::GetThinPlateSteps()
{ 
	return m_ThinPlateSteps; 
}
