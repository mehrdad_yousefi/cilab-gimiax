/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreManualNeckCuttingProcessor.h"

#include <string>
#include <iostream>

#include "coreReportExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityHelper.txx"
#include "coreKernel.h"
#include "coreProcessorManager.h"


#include "vtkDijkstraGraphGeodesicPath.h"
#include "vtkCleanPolyData.h"
#include "vtkAppendPolyData.h"

#include "meVTKManualNeckCuttingFilter.h"

#include "blShapeUtils.h"


Core::ManualNeckCuttingProcessor::ManualNeckCuttingProcessor( )
{
	BaseProcessor::SetNumberOfInputs( NUMBER_OF_INPUTS );
	GetInputPort( INPUT_ANEURYSM )->SetName( "Aneurysm with vessel" );
	GetInputPort( INPUT_ANEURYSM )->SetDataEntityType( Core::SurfaceMeshTypeId );
	GetInputPort( INPUT_POINT )->SetName( "Input Point" );
	GetInputPort( INPUT_POINT )->SetDataEntityType( Core::PointSetTypeId );
	m_sacIndex = 0;

	BaseProcessor::SetNumberOfOutputs( NUMBER_OF_OUTPUTS + 1 );
	m_useMinorNumberOfHoles = true; 

}

Core::ManualNeckCuttingProcessor::~ManualNeckCuttingProcessor()
{
}

void Core::ManualNeckCuttingProcessor::Update()
{
	//! The filter to cut the neck of the aneuriysm
	vtkSmartPointer<meVTKManualNeckCuttingFilter> filter = vtkSmartPointer<meVTKManualNeckCuttingFilter>::New();

	Core::vtkPolyDataPtr landmarks;
	Core::DataEntityHelper::GetProcessingData(GetInputDataEntityHolder(INPUT_POINT),landmarks);

	// the input mesh
	Core::vtkPolyDataPtr inputMesh;
	Core::DataEntityHelper::GetProcessingData(GetInputDataEntityHolder(INPUT_ANEURYSM),inputMesh);

	filter->SetInput(0,inputMesh);

	// The input line containing the point along which the neck is going to be cut
	if (landmarks->GetNumberOfPoints() < 3)
		throw Core::Exceptions::Exception(
			"ManualCuttingNeckProcessor::Update",
			"You must select at least 3 points to define the line");
 
	//Step 0: Get landmark points and cells
	Core::vtkPolyDataPtr landmarksLinePoints;
	landmarksLinePoints = GetLandmarkLine( landmarks, inputMesh );

	//Step 1: build line as a collection of lines of 2 points!
	Core::vtkPolyDataPtr landmarksLine;
	landmarksLine = GetLandmarkLineRefined( landmarksLinePoints );

	filter->SetInput(1,landmarksLine);

	filter->Update();

	std::vector <vtkSmartPointer <vtkPolyData> > regions;
	regions.push_back(filter->GetOutput(0));
	regions.push_back(filter->GetOutput(1));
	// The landmarks line
	UpdateOutput( LOOP_POINTS, landmarksLine, "Lines", true, 1, GetInputDataEntity(INPUT_ANEURYSM) );

	// The two components: result of cutting
	unsigned int numHoles1 = blShapeUtils::ShapeUtils::ComputeNumberOfHoles(regions[0]); 		
	unsigned int numHoles2 = blShapeUtils::ShapeUtils::ComputeNumberOfHoles(regions[1]); 		
	
	m_sacIndex = 0;
	std::vector <std::string> regionNames;
	regionNames.push_back("Sac");
	regionNames.push_back("Vessel");

	//m_sacIndex = 1;  //best guess: the first one..otherwise the one that has less holes
	(numHoles1<=numHoles2) ? m_sacIndex =1 : m_sacIndex = 2;

	if (m_useMinorNumberOfHoles)
	{
		UpdateOutput( OUTPUT_MESH1, regions[m_sacIndex-1], regionNames[0], true, 1, GetInputDataEntity(INPUT_ANEURYSM) );	
	}
	else
	{
		UpdateOutput( OUTPUT_MESH1, regions[2-m_sacIndex], regionNames[0], true, 1, GetInputDataEntity(INPUT_ANEURYSM) );	
	}
}

void Core::ManualNeckCuttingProcessor::SetUseMinorNumberOfHolesGuess(bool bVal)
{
	m_useMinorNumberOfHoles = bVal; 
}

bool Core::ManualNeckCuttingProcessor::GetUseMinorNumberOfHolesGuess( )
{
	return m_useMinorNumberOfHoles;
}

Core::vtkPolyDataPtr Core::ManualNeckCuttingProcessor::GetLandmarkLine( 
	Core::vtkPolyDataPtr landmarks, 
	Core::vtkPolyDataPtr inputMesh )
{
	vtkIdType idS, idE;
	size_t k;
	vtkSmartPointer<vtkAppendPolyData> poly= vtkSmartPointer<vtkAppendPolyData>::New();
	for(size_t i = 0; i < landmarks->GetNumberOfPoints(); i++ )
	{
		idS = inputMesh->FindPoint(landmarks->GetPoint(i));
		k= ( i != landmarks->GetNumberOfPoints()-1) ?  i+1 : 0;
		idE = inputMesh->FindPoint(landmarks->GetPoint(k));
		vtkSmartPointer<vtkDijkstraGraphGeodesicPath> geoPath = vtkSmartPointer<vtkDijkstraGraphGeodesicPath>::New();
		geoPath->SetInput(inputMesh);
		geoPath->SetStartVertex(idS);
		geoPath->SetEndVertex(idE);
		geoPath->Update();
		poly->AddInput(geoPath->GetOutput());
	}
	poly->Update();

	vtkSmartPointer<vtkCleanPolyData> clean = vtkSmartPointer<vtkCleanPolyData>::New();
	clean->SetInput(poly->GetOutput());
	clean->Update();

	return clean->GetOutput();
}

Core::vtkPolyDataPtr Core::ManualNeckCuttingProcessor::GetLandmarkLineRefined( 
	Core::vtkPolyDataPtr landmarks )
{

	Core::vtkPolyDataPtr landmarksLine = vtkPolyData::New();
	vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
	vtkSmartPointer<vtkCellArray>	cellPolys = vtkSmartPointer<vtkCellArray>::New( );

	points->DeepCopy( landmarks->GetPoints() );
	landmarksLine->SetPoints( points );
	landmarksLine->SetLines( cellPolys );

	for (size_t pt =0; pt < landmarks->GetNumberOfLines(); pt++)
	{
		vtkSmartPointer< vtkIdList> listcell = vtkIdList::New();
		landmarks->GetCellPoints(pt,listcell);
		for (size_t m =0; m < listcell->GetNumberOfIds()- 1; m++)
		{
			vtkSmartPointer< vtkIdList> list = vtkIdList::New();
			list->InsertNextId(listcell->GetId(m));
			list->InsertNextId(listcell->GetId(m+1));
			cellPolys->InsertNextCell(list);
		}
	}

	return landmarksLine;
}

void Core::ManualNeckCuttingProcessor::CleanUp()
{
	int i;
	for (i = 0; i < this->GetNumberOfOutputs(); i++) 
	{
		if(GetOutputDataEntityHolder(i)->GetSubject().IsNotNull())
		{
			//look in the list if it is present..otherwise reparent it to root and discard it
			
			Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
			Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();
			if (!list->IsInTheList(GetOutputDataEntityHolder(i)->GetSubject()->GetId()))
			{
				GetOutputDataEntityHolder(i)->GetSubject()->SetFather(NULL);	
				GetOutputDataEntityHolder(i)->SetSubject(NULL);
			}
		}
	}

}