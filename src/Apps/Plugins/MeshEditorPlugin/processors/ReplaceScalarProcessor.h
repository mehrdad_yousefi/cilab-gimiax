/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _ReplaceScalarProcessor_H
#define _ReplaceScalarProcessor_H

#include "coreBaseProcessor.h"

#include "blShapeUtils.h"

/**
Replace a scalar value of a specific scalar array

This processor modified the input surface mesh directly

\ingroup MeshEditorPlugin
\author Xavi Planes
\date Mar 2013
*/
class ReplaceScalarProcessor : public Core::BaseProcessor
{
public:

	typedef enum
	{
		INPUT_SURFACE,
		INPUTS_NUMBER
	};

	typedef enum
	{
		OUTPUT_SURFACE,
		OUTPUTS_NUMBER
	};

public:
	//!
	coreProcessor(ReplaceScalarProcessor, Core::BaseProcessor);
	
	//! Call library to perform operation
	void Update( );

	//!
	void SetScalarValue( double val );
	double GetScalarValue( ) const;

	//!
	void SetSelectedScalarArray( const blShapeUtils::ShapeUtils::VTKScalarType &scalarName );

private:
	//!
	ReplaceScalarProcessor();

	//!
	~ReplaceScalarProcessor();

	//! Purposely not implemented
	ReplaceScalarProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );

private:

	//!
	double m_ScalarValue;

	//!
	blShapeUtils::ShapeUtils::VTKScalarType m_ScalarName;
};
    

#endif //_ReplaceScalarProcessor_H
