// Copyright 2011 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#ifndef ExtractContoursProcessor_H
#define ExtractContoursProcessor_H

#include "coreBaseProcessor.h"



/**
\brief Extract contours resulting from the intersection of an image and a surface mesh
\ingroup MeshEditorPlugin
\author Xavi Planes
\date Sept 2011
*/
class PLUGIN_EXPORT ExtractContoursProcessor : public Core::BaseProcessor
{

public:

    //! Smart pointer and processor factory.
    coreProcessor(ExtractContoursProcessor, Core::BaseProcessor);

    //! Perform process.
    void Update();
    
private:

    //! Constructor.
    ExtractContoursProcessor();

    //! Destructor.
    ~ExtractContoursProcessor();

    //! Copy constructor: purposely not implemented.
    ExtractContoursProcessor( const Self& );

    //! Assignement operator: purposely not implemented.
    void operator = ( const Self& );

private:

};

#endif // ExtractContoursProcessor_H
