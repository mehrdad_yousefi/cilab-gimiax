/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "BooleanOperationsProcessor.h"

#include <string>
#include <iostream>

#include "coreReportExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityHelper.txx"
#include "coreKernel.h"

#include "vtkSmartPointer.h"
#include "vtkMath.h"
#include "coreVTKImageDataHolder.h"
#include "coreVTKPolyDataHolder.h"
#include "vtkSelectEnclosedPoints.h"
#include "vtkThresholdPoints.h"

#include "vtkImageMathematics.h"
#include "vtkImageStencil.h"
#include "vtkPolyDataToImageStencil.h"
#include "vtkImageThreshold.h"
#include "vtkImageToImageFilter.h"
#include "itkVTKImageToImageFilter.txx"
#include "vtkImageCast.h"

#include "meBinImageToMeshFilter.h"
#include "meRemoveInnerSurfaceFilter.h"

#include "vtkBooleanOperationPolyDataFilter.h"
#include "vtkThreshold.h"
#include "vtkDataSetSurfaceFilter.h"
#include "vtkReverseSense.h"
#include "vtkIntersectionPolyDataFilter.h"
#include "vtkDistancePolyDataFilter.h"


BooleanOperationsProcessor::BooleanOperationsProcessor( )
{
	SetName( "BooleanOperationsProcessor" );
	
	BaseProcessor::SetNumberOfInputs( INPUTS_NUMBER );
	GetInputPort(INPUT_SURFACE_1)->SetName( "Surface 1" );
	GetInputPort(INPUT_SURFACE_1)->SetDataEntityType( Core::SurfaceMeshTypeId);
	GetInputPort(INPUT_SURFACE_2)->SetName( "Surface 2" );
	GetInputPort(INPUT_SURFACE_2)->SetDataEntityType( Core::SurfaceMeshTypeId);
	BaseProcessor::SetNumberOfOutputs( OUTPUTS_NUMBER );
	
}

BooleanOperationsProcessor::~BooleanOperationsProcessor()
{
}

void BooleanOperationsProcessor::Update()
{
	Core::vtkPolyDataPtr inputMesh1;
	GetProcessingData( BooleanOperationsProcessor::INPUT_SURFACE_1, inputMesh1 );

	Core::vtkPolyDataPtr inputMesh2;
	GetProcessingData( BooleanOperationsProcessor::INPUT_SURFACE_2, inputMesh2 );
	if(( inputMesh1 == NULL )||(inputMesh2 == NULL))
		throw Core::Exceptions::Exception("BooleanOperationsProcessor::Update",
			"Please select a valid input mesh");

	vtkSmartPointer<vtkIntersectionPolyDataFilter> intersection =
		vtkSmartPointer<vtkIntersectionPolyDataFilter>::New();
	intersection->SetInput(0, inputMesh1);
	intersection->SetInput(1, inputMesh2);
	//intersection->SetInputConnection( 0, inputMesh1 );
	//intersection->SetInputConnection( 1, inputMesh2 );

	vtkSmartPointer<vtkDistancePolyDataFilter> distance =
		vtkSmartPointer<vtkDistancePolyDataFilter>::New();
	distance->SetInput( 0, intersection->GetOutput(1) );
	distance->SetInput( 1, intersection->GetOutput( 2 ) );

	vtkSmartPointer<vtkThreshold> thresh1 =
		vtkSmartPointer<vtkThreshold>::New();
	thresh1->AllScalarsOn();
	thresh1->SetInputArrayToProcess
		( 0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, "Distance" );
	thresh1->SetInputConnection( distance->GetOutputPort( 0 ) );

	vtkSmartPointer<vtkThreshold> thresh2 =
		vtkSmartPointer<vtkThreshold>::New();
	thresh2->AllScalarsOn();
	thresh2->SetInputArrayToProcess
		( 0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, "Distance" );
	thresh2->SetInputConnection( distance->GetOutputPort( 1 ) );

	if ( m_operation == vtkBooleanOperationPolyDataFilter::UNION )
	{
		thresh1->ThresholdByUpper( 0.0 );
		thresh2->ThresholdByUpper( 0.0 );
	}
	else if ( m_operation == vtkBooleanOperationPolyDataFilter::INTERSECTION )
	{
		thresh1->ThresholdByLower( 0.0 );
		thresh2->ThresholdByLower( 0.0 );
	}
	else // Difference
	{
		thresh1->ThresholdByUpper( 0.0 );
		thresh2->ThresholdByLower( 0.0 );
	}

	vtkSmartPointer<vtkDataSetSurfaceFilter> surface1 =
		vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
	surface1->SetInputConnection( thresh1->GetOutputPort() );

	vtkSmartPointer<vtkDataSetSurfaceFilter> surface2 =
		vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
	surface2->SetInputConnection( thresh2->GetOutputPort() );

	vtkSmartPointer<vtkReverseSense> reverseSense =
		vtkSmartPointer<vtkReverseSense>::New();
	reverseSense->SetInputConnection( surface2->GetOutputPort() );
	if ( m_operation == vtkBooleanOperationPolyDataFilter::SUBTRACTION ) // difference
	{
		reverseSense->ReverseCellsOn();
		reverseSense->ReverseNormalsOn();
	}

	vtkSmartPointer<vtkAppendPolyData> appender =
		vtkSmartPointer<vtkAppendPolyData>::New();
	appender->SetInputConnection( surface1->GetOutputPort() );
	if ( m_operation == vtkBooleanOperationPolyDataFilter::SUBTRACTION )
	{
		appender->AddInputConnection( reverseSense->GetOutputPort() );
	}
	else
	{
		appender->AddInputConnection( surface2->GetOutputPort() );
	}
	appender->Update();
	if (appender->GetOutput() != NULL)
	{
		if (m_operation == vtkBooleanOperationPolyDataFilter::SUBTRACTION)
			UpdateOutput(OUTPUT_SURFACE, appender->GetOutput(),"outputSubtraction", false, 1);
		if (m_operation == vtkBooleanOperationPolyDataFilter::UNION)
			UpdateOutput(OUTPUT_SURFACE, appender->GetOutput(),"outputUnion", false, 1);
		if (m_operation == vtkBooleanOperationPolyDataFilter::INTERSECTION)
			UpdateOutput(OUTPUT_SURFACE, appender->GetOutput(),"outputIntersection", false, 1);
	}

}
