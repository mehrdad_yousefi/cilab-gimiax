/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreExtractSurfacesProcessor.h"
#include "coreDataEntityHelper.h"
#include "coreEnvironment.h"

#include "vtkPointSet.h"
#include "vtkCleanPolyData.h"
#include "vtkPolyDataConnectivityFilter.h"

#include "blShapeUtils.h"
#include "blTextUtils.h"

Core::ExtractSurfacesProcessor::ExtractSurfacesProcessor( )
{
	SetNumberOfInputs( NUMBER_OF_INPUTS );
	GetInputPort( INPUT_MESH )->SetName( "Input mesh" );
	GetInputPort( INPUT_MESH )->SetDataEntityType( Core::SurfaceMeshTypeId );
	SetNumberOfOutputs( NUMBER_OF_OUTPUTS );
}

std::string GetString(int iPos)
{
	if (iPos==0) return "0";

	std::string ret="";
	while (iPos>0)
	{
		ret = std::string(1,(char)(iPos%10+'0')) + ret;
		iPos/=10;
	}
	return ret;
}

void Core::ExtractSurfacesProcessor::Update()
{
	unsigned int numSurfaces;
	std::string numStr;
	int iPos;
	vtkSmartPointer<vtkCleanPolyData> clean;

	vtkPolyDataPtr inputPolyData;
	GetProcessingData(INPUT_MESH, inputPolyData );

	
	vtkSmartPointer<vtkPolyDataConnectivityFilter> connectivity = 
		vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
	connectivity->SetInput( inputPolyData );
	connectivity->SetExtractionModeToLargestRegion();
	connectivity->Update();

	numSurfaces = connectivity->GetNumberOfExtractedRegions();

	Core::DataEntity::Pointer fatherDataEntity = GetInputDataEntity( 0 );

	connectivity->SetExtractionModeToSpecifiedRegions();

	for (iPos=0; iPos<numSurfaces && iPos<NUMBER_OF_OUTPUTS; iPos++)
	{
		connectivity->InitializeSpecifiedRegionList();
		connectivity->AddSpecifiedRegion( iPos );
		connectivity->Update();
			
		clean = vtkSmartPointer<vtkCleanPolyData>::New();
		clean->SetInput(connectivity->GetOutput());
		clean->Update();

		UpdateOutput( iPos, clean->GetOutput(), "Extracted surface #" + GetString(iPos), false, 1, fatherDataEntity );
	}
		
}
