/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "medWidgetCollective.h"

#include "wxID.h"

#include "coreFrontEndPlugin.h"
#include "corePluginTabFactory.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreSimpleProcessingWidget.h"

#include "ptExtractMainSurfacePanelWidget.h"
#include "ptTaubinSmoothSurfacePanelWidget.h"
#include "ptEdgeSwappingPanelWidget.h"
#include "ptLocalRefinerPanelWidget.h"
#include "ptLoopSubdivisionPanelWidget.h"
#include "ptThresholdPanelWidget.h"
#include "VolumeClosingPanelWidget.h"
#include "ptSkeletonizationPanelWidget.h"
#include "ptTetraGeneratorPanelWidget.h"
#include "ptNGOptimizationPanelWidget.h"
#include "ptRingCutPanelWidget.h"
#include "ptSkeletonCutPanelWidget.h"
#include "ptMeshStatisticsPanelWidget.h"
#include "ptExtractScalarWidget.h"
#include "BooleanOperationsPanelWidget.h"
#include "ExtractContoursProcessor.h"
#include "ManualNeckCuttingPanelWidget.h"
#include "MeshEditingWidget.h"
#include "ReplaceScalarWidget.h"

#include "coreSurfaceSelectorWidget.h"
#include "coreToolbarMeshEditing.h"

#include "ptMeshCreationWidget.h"

//#include ".xpm"
#include "extractsurface.xpm"
#include "ringcut.xpm"
#include "closeholes.xpm"
#include "SelectCellsSurfaceButton.xpm"

medWidgetCollective::medWidgetCollective( ) 
{
}

void medWidgetCollective::Init(  )
{
	Core::Runtime::Kernel::RuntimeGraphicalInterfacePointer gIface;
	gIface = Core::Runtime::Kernel::GetGraphicalInterface();

	Core::WindowConfig windowConfig = Core::WindowConfig( ).ProcessingTool( );
	windowConfig.Category("Statistics");

	gIface->RegisterFactory(
		ptMeshStatisticsPanelWidget::Factory::NewBase(), 
		windowConfig.Caption( "Statistics on Tetra mesh" ) );

	//Advanced Surface editing
	windowConfig.Category( "Advanced Surface Editing" );
	gIface->RegisterFactory(
		ptRingCutPanelWidget::Factory::NewBase(), 
		Core::WindowConfig().Category("Advanced Surface Editing").Caption( RINGCUTWIDGETNAME ).Bitmap( ringcut_xpm )
		.Id( wxID_RingCutWidget ).VerticalLayout().ProcessingTool()  );
	gIface->RegisterFactory(
		ptSkeletonizationPanelWidget::Factory::NewBase(), 
		windowConfig.Caption( "Skeletonization" ) );
	gIface->RegisterFactory(
		ptTetraGeneratorPanelWidget::Factory::NewBase(), 
		windowConfig.Caption( "Tetra generation" ) );

	gIface->RegisterFactory(
		ptExtractScalarWidget::Factory::NewBase(), 
		windowConfig.Caption( "Extract Scalar" ) );

	gIface->RegisterFactory( ReplaceScalarWidget::Factory::NewBase(), windowConfig.Caption( "Replace Scalar" ) );

	gIface->RegisterFactory(
		ManualNeckCuttingPanelWidget::Factory::NewBase(), 
		windowConfig.Caption( "Manual neck cutting" ) );
	

	// Add local mesh editing tools	
	std::list<std::string> factoryList;
	factoryList.push_back( ptLocalRefinerPanelWidget::Factory::GetNameClass() );
	factoryList.push_back( ptTaubinSmoothSurfacePanelWidget::Factory::GetNameClass() );
	factoryList.push_back( ReplaceScalarWidget::Factory::GetNameClass() );

	gIface->RegisterFactory(
		SurfaceSelectorWidget::Factory::New( factoryList ), 
		Core::WindowConfig().Category("Advanced Surface Editing").Caption( SURFACEWIDGETNAME ).Bitmap(selectcellssurfacebutton_xpm)
		.Id( wxID_SurfaceSelectorWidget ).VerticalLayout().ProcessingTool() );

	gIface->RegisterFactory(
		VolumeClosingPanelWidget::Factory::NewBase(), 
		Core::WindowConfig().Category("Basic Surface Editing").Caption( HOLEFILLINGWIDGETNAME ).Bitmap( closeholes_xpm )
		.Id( wxID_VolumeClosingWidget ).ProcessorObservers().VerticalLayout().ProcessingTool()  );

	gIface->RegisterFactory(
		ptExtractMainSurfacePanelWidget::Factory::NewBase(),
		Core::WindowConfig().Category("Basic Surface Editing").Caption( EXTRACTSURFACEWIDGETNAME ).Bitmap( extractsurface_xpm )
		.Id( wxID_ExtractSurfaceWidget ).ProcessorObservers().VerticalLayout().ProcessingTool()  );


	//Basic surface editing
	windowConfig.Category( "Basic Surface Editing" );

	gIface->RegisterFactory(
		ptTaubinSmoothSurfacePanelWidget::Factory::NewBase(), 
		windowConfig.Caption( "Taubin Smooth" ) );
	gIface->RegisterFactory(
		ptLoopSubdivisionPanelWidget::Factory::NewBase(), 
		windowConfig.Caption( "Loop Subdivision Refiner Surface" ) );
	gIface->RegisterFactory(
		ptLocalRefinerPanelWidget::Factory::NewBase(), 
		windowConfig.Caption( "Local Refiner" ) );
	gIface->RegisterFactory(
		ptEdgeSwappingPanelWidget::Factory::NewBase(), 
		windowConfig.Caption( "Edge Swapped Surface" ) );
	gIface->RegisterFactory(
		ptThresholdPanelWidget::Factory::NewBase(), 
		windowConfig.Caption( "Threshold Surface" ) );
	gIface->RegisterFactory(
		ptMeshCreationWidget::Factory::NewBase(), 
		windowConfig.Caption( "Mesh Creation" ) );
	gIface->RegisterFactory(
		BooleanOperationsPanelWidget::Factory::NewBase(), 
		windowConfig.Caption( "Boolean Operations" ) );

	// Add local mesh editing tools	
	std::list<std::string> meshFactoryList;
	meshFactoryList.push_back( SurfaceSelectorWidget::Factory::GetNameClass() );
	meshFactoryList.push_back( ptRingCutPanelWidget::Factory::GetNameClass() );
	meshFactoryList.push_back( VolumeClosingPanelWidget::Factory::GetNameClass() );
	meshFactoryList.push_back( ptExtractMainSurfacePanelWidget::Factory::GetNameClass() );
	gIface->RegisterFactory(
		MeshEditingWidget::Factory::New( meshFactoryList ), 
		windowConfig.Caption( "Mesh Editing" ) );
	
	//gIface->RegisterFactory(
	//	Core::Widgets::ToolbarMeshEditing::Factory::NewBase( ),
	//	Core::WindowConfig( ).Toolbar( ).Top( ).Show().Caption( "Mesh Editor Toolbar" ) );

	typedef Core::Widgets::SimpleProcessingWidget<ExtractContoursProcessor> ExtractContourWidget;
	gIface->RegisterFactory( 
		ExtractContourWidget::Factory::NewBase(),
		Core::WindowConfig( ).ProcessingTool().ProcessorObservers()
		.Caption( "Extract contours" ).Category("Basic Surface Editing") );

}
