/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef COREDataEntityTest_H
#define COREDataEntityTest_H

#include "cxxtest/TestSuite.h"

/** Test Core::DataEntity
\author Xavi Planes
\date 22 sept 2010
\ingroup CoreTests
 */
class CoreDataEntityTest : public CxxTest::TestSuite
{
public:
	CoreDataEntityTest( );
	virtual ~CoreDataEntityTest();

	static CoreDataEntityTest *createSuite();
	static void destroySuite( CoreDataEntityTest *suite );

	//!
	void setUp();
	//!
	void tearDown();
public:
	//! Test shape
	void TestShape();
	//! Test image
	void TestImage();
	//! Test Copy
	void TestCopy();
};

#endif //COREDataEntityTest_H
