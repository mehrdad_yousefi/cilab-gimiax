// Copyright 2006 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "coreTestDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreKernel.h"
#include "GIMIASHeader.h"
#include "blShapeUtils.h"
#include "coreRenderingTreeMITK.h"
#include "coreWxEnvironment.h"
#include "corePluginProviderManager.h"
#include "coreWxEventsFactoriesRegistration.h"

const char* STR_INPUT_MESH = GIMIAS_DATA_FOLDER "/aneu.vtk";
const char* STR_INPUT_IMAGE_FILENAME = GIMIAS_DATA_FOLDER "/SmallImage.vtk";

void CoreTestDataEntity::test0()
{
	CreateDataEntity();
	ChangeProcessingData();
	AddToRenderingTree();
	RemoveFromRenderingTree( );
	LoadDataEntity( );
}

void CoreTestDataEntity::setUp()
{
	// We don't create anything here
	//blCxxTest::setUp( );

	Core::Runtime::Environment::Pointer runtime;
	runtime = Core::Runtime::WxEnvironment::New().GetPointer();
	runtime->InitApplication( wxTheApp->argc, wxTheApp->argv, Core::Runtime::Graphical );

	Core::Runtime::Kernel::Initialize( runtime );

	Core::Runtime::Kernel::InitializeGraphicalInterface( NULL, Core::Runtime::Graphical );

	Core::WxEventsFactoriesRegistration::Register();

	Core::Runtime::Kernel::GetGraphicalInterface( )->GetPluginProviderManager( )->LoadConfiguration( true );
	Core::Runtime::Kernel::GetGraphicalInterface( )->GetPluginProviderManager( )->UpdateProviders( "", false );
}

void CoreTestDataEntity::tearDown()
{
	// Destroy internal referneces
	m_dataEntityHolderOutput->SetSubject ( NULL );
	m_tree = NULL;

	// Cleanup allocation of all the members of static objects
	Core::Runtime::Kernel::Terminate(); 

	// We cannot detect memory leaks of MITK because there are some
	// classes that use static member variables like mitk::EnumerationProperty
	// And we should implement the functinos to clear all this memory
	// Unregister the created factories. We need to assure that all
	// static memory is deleted
	//blCxxTest::tearDown( );
}

void CoreTestDataEntity::CreateDataEntity()
{
	m_dataEntityHolderOutput = Core::DataEntityHolder::New();

	// Load shape from file
	this->m_PolyData = Core::vtkPolyDataPtr::New();
	m_PolyData.TakeReference( blShapeUtils::ShapeUtils::LoadShapeFromFile( STR_INPUT_MESH ) );

	Core::DataEntity::Pointer dataEntity = Core::DataEntity::New( Core::SurfaceMeshTypeId );
	dataEntity->AddTimeStep( this->m_PolyData );
	m_dataEntityHolderOutput->SetSubject( dataEntity );
}

void CoreTestDataEntity::ChangeProcessingData()
{
	// Load shape from file again
	this->m_PolyData = Core::vtkPolyDataPtr::New();
	m_PolyData.TakeReference( blShapeUtils::ShapeUtils::LoadShapeFromFile( STR_INPUT_MESH ) );

	Core::DataEntity::Pointer dataEntity = Core::DataEntity::New( Core::SurfaceMeshTypeId );
	dataEntity->AddTimeStep( this->m_PolyData );
	m_dataEntityHolderOutput->SetSubject( dataEntity );
}

void CoreTestDataEntity::AddToRenderingTree()
{
	m_tree = Core::RenderingTreeMITK::New( );

	mitk::DataTreeNode::Pointer node;
	boost::any anyData = m_tree->Add( m_dataEntityHolderOutput->GetSubject( ) );
	Core::CastAnyProcessingData( anyData, node );
}

void CoreTestDataEntity::RemoveFromRenderingTree()
{
	m_tree->Remove( m_dataEntityHolderOutput->GetSubject( )->GetId( ) );
}

void CoreTestDataEntity::TestGetProcessingData()
{
	this->CreateDataEntity();
	TS_ASSERT(this->m_PolyData);
	Core::vtkPolyDataPtr polyData;
	TS_ASSERT( m_dataEntityHolderOutput->GetSubject()->GetProcessingData(polyData) );
	TS_ASSERT( polyData == this->m_PolyData );

	Core::vtkPolyDataPtr polyData2;
	Core::CastAnyProcessingData( m_dataEntityHolderOutput->GetSubject()->GetProcessingData(), polyData2 );
	TS_ASSERT(polyData == polyData2 );
}

void CoreTestDataEntity::LoadDataEntity( )
{
	std::vector<std::string> filenames;
	filenames.push_back( STR_INPUT_IMAGE_FILENAME );

	Core::DataEntity::Pointer dataEntity;
	dataEntity = Core::DataEntityHelper::LoadDataEntity( filenames );
	TS_ASSERT( dataEntity.IsNotNull() );
}


