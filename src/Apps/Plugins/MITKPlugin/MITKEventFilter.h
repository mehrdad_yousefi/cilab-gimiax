/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _MITKPluginEventFilter_H
#define _MITKPluginEventFilter_H

#include "coreFrontEndPlugin.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreWxEnvironment.h"
#include "wxMitkAbortEventFilter.h"

namespace MITKPlugin{

/**
Filter wxWidgets events for managing better MITK rendering like Volume rendering

\ingroup MITKPlugin
\author Xavi Planes
\date Nov 2010
*/

class EventFilter : 
	public Core::Runtime::wxBaseEventFilter,
	public mitk::wxMitkAbortEventFilter
{
public:
	typedef EventFilter						Self;
	typedef mitk::wxMitkAbortEventFilter	Superclass;
	typedef itk::SmartPointer<Self>			Pointer;
	typedef itk::SmartPointer<const Self>	ConstPointer;

	itkTypeMacro(Self, Superclass);
	itkFactorylessNewMacro( Self );

private:
	//! The constructor instantiates all the widgets and registers them.
	EventFilter( );

	//!
	~EventFilter( );

	//! 
	int eventFilter(wxEvent& event);

private:
};

} // namespace MITKPlugin

#endif //_MITKPluginEventFilter_H
