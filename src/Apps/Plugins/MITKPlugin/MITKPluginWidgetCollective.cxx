/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "MITKPluginWidgetCollective.h"

#include "wxID.h"

#include "coreFrontEndPlugin.h"
#include "corePluginTab.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreSettings.h"
#include "coreWindowConfig.h"

#include "wxMitkApp.h"

#include "mitkGlobalInteraction.h"

#include "coreToolbarMultiRenderWindow.h"

#include "coreLandmarkSelectorWidget.h"
#include "coreBBoxWidget.h"
#include "coreMeasurementWidget.h"

#include "coreMultiRenderWindowMITK.h"
#include "coreMultiRenderWindowMITKConfig.h"

#include "coreVolumeMeshMaterialPropertiesWidget.h"
#include "coreVolumeImagePropertiesWidget.h"
#include "coreSurfaceMeshMaterialPropertiesWidget.h"
#include "coreITKTransformVisualPropertiesWidget.h"
#include "PropagateWidget.h"
#include "ptCroppingWidget.h"

#include "coreDataInformation.h"

#include "CGNSFileWriterDialog.h"
#include "DICOMWriterWidget.h"

#include "PointInteractorOn.xpm"
#include "MeasurementInteractorOn.xpm"
#include "BoundingBox.xpm"

#include "MITKEventFilter.h"
#include "coreMITKPreview.h"

const char* GIMIAS_STATE_MACHINES_FILENAME = "/GimiasStateMachines.xml";


MITKPlugin::WidgetCollective::WidgetCollective( ) 
{
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();
	mitk::wxMitkApp::Initialize( settings->GetApplicationPathSubPath("StateMachine.xml") );

	LoadStateMachines( );

	Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
	gIface = Core::Runtime::Kernel::GetGraphicalInterface();

	// Toolbars
	gIface->RegisterFactory(
		Core::Widgets::ToolbarMultiRenderWindow::Factory::NewBase( ),
		Core::WindowConfig( ).Toolbar( ).Top( ).Show().Caption( "MultiRenderWindow Toolbar" ) );

	// Selection tools
	gIface->RegisterFactory( 
		Core::Widgets::LandmarkSelectorWidget::Factory::NewBase( ),
		Core::WindowConfig( ).SelectionTool()
		.Caption("Landmark selector").Bitmap( pointinteractoron_xpm )
		.Id( wxID_LandmarkSelectorWidget ) );

	gIface->RegisterFactory( 
		Core::Widgets::MeasurementWidget::Factory::NewBase( ),
		Core::WindowConfig( ).SelectionTool()
		.Caption("Measurement").Bitmap( measurementinteractoron_xpm )
		.Id( wxID_MeasurementWidget ) );

	gIface->RegisterFactory( 
		Core::Widgets::BBoxWidget::Factory::NewBase( ),
		Core::WindowConfig( ).SelectionTool()
		.Caption("Bounding Box").Bitmap( boundingbox_xpm )
		.Id( wxID_BBoxWidget ) );

	// MultiRenderWindow
	gIface->RegisterFactory(
		Core::Widgets::MultiRenderWindowMITK::Factory::NewBase( ), 
		Core::WindowConfig( ).RenderWindow( ).Caption( "Multi Render Window MITK" ) );

	gIface->RegisterFactory(
		Core::Widgets::MultiRenderWindowMITKConfig::Factory::NewBase( ), 
		Core::WindowConfig( ).RenderWindowConfig( ).Caption( "Multi Render Window MITK Config"  ) );

	// Information
	gIface->RegisterFactory(
		Core::Widgets::VolumeImageInformationWidget::Factory::NewBase( ), 
		Core::WindowConfig( ).DataInformation( ).Caption( "Volume Image" ) );

	gIface->RegisterFactory(
		Core::Widgets::MeasurementInformationWidget::Factory::NewBase( ), 
		Core::WindowConfig( ).DataInformation( ).Caption( "Measurement" ) );

	gIface->RegisterFactory(
		Core::Widgets::PolyDataInformationWidget::Factory::NewBase( ), 
		Core::WindowConfig( ).DataInformation( ).Caption( "Surface mesh" ) );

	gIface->RegisterFactory(
		Core::Widgets::UnstructuredGridInformationWidget::Factory::NewBase( ), 
		Core::WindowConfig( ).DataInformation( ).Caption( "Volume mesh" ) );

	gIface->RegisterFactory(
		Core::Widgets::SignalInformationWidget::Factory::NewBase( ), 
		Core::WindowConfig( ).DataInformation( ).Caption( "Signal") );

	// Visual Props
	gIface->RegisterFactory(
		Core::Widgets::VolumeMeshMaterialPropertiesWidget::Factory::NewBase( ), 
		Core::WindowConfig( ).VisualProps( ).Caption( "VolumeMeshVisualProps" ) );

	gIface->RegisterFactory(
		Core::Widgets::VolumeImagePropertiesWidget::Factory::NewBase( ), 
		Core::WindowConfig( ).VisualProps( ).Caption( "ImageVisualProps" ) );

	gIface->RegisterFactory(
		Core::Widgets::SurfaceMeshMaterialPropertiesWidget::Factory::NewBase( ), 
		Core::WindowConfig( ).VisualProps( ).Caption( "SurfaceMeshVisualProps" ) );

	gIface->RegisterFactory(
		Core::Widgets::ITKTransformVisualPropertiesWidget::Factory::NewBase( ), 
		Core::WindowConfig( ).VisualProps( ).Caption( "ITKTransformVisualProps" ) );

	// IO headers widgets
	gIface->RegisterFactory( 
		Core::Widgets::CGNSFileWriterDialog::Factory::NewBase( ),
		Core::WindowConfig( ).IOHeaderWindow().Caption( "CGNS File Header Writer" ) );

	gIface->RegisterFactory( 
		Core::Widgets::DICOMWriterWidget::Factory::NewBase( ),
		Core::WindowConfig( ).IOHeaderWindow().Caption( "DICOM File Header Writer" ) );

	m_EventFilter = MITKPlugin::EventFilter::New( );

	Core::Runtime::WxEnvironment* environment;
	environment = dynamic_cast<Core::Runtime::WxEnvironment*> ( Core::Runtime::Kernel::GetApplicationRuntime().GetPointer() );
	environment->RegisterEventFilter( m_EventFilter );

	// Tools
	gIface->RegisterFactory(
		PropagateWidget::Factory::NewBase(),
		Core::WindowConfig( ).ProcessingTool( ).Category("Filtering")
		.Caption("Propagate data") );

	gIface->RegisterFactory( 
		ptCroppingWidget::Factory::NewBase(),
		Core::WindowConfig( ).ProcessingTool( ).Category("Filtering")
		.Caption( "Cropping tool" ) );

	gIface->RegisterFactory( Core::DataEntityPreview::GetNameClass(), Core::MITKPreview::Factory::New() );
}

void MITKPlugin::WidgetCollective::LoadStateMachines()
{
	std::string strPath;
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();
	strPath = settings->GetResourcePath() + GIMIAS_STATE_MACHINES_FILENAME;

	// load interaction patterns from XML-file
	bool bResult = mitk::GlobalInteraction::GetInstance()->GetStateMachineFactory()->LoadBehavior( strPath );
}

MITKPlugin::WidgetCollective::~WidgetCollective()
{
	Core::Runtime::WxEnvironment* environment;
	environment = dynamic_cast<Core::Runtime::WxEnvironment*> ( Core::Runtime::Kernel::GetApplicationRuntime().GetPointer() );
	environment->UnRegisterEventFilter( m_EventFilter );

	m_EventFilter = NULL;

	// Process idle events and destroy the pending objects like MultiRenderWindowEventHandler
	wxTheApp->ProcessIdle();

	mitk::wxMitkApp::CleanUpMITK( );
}

