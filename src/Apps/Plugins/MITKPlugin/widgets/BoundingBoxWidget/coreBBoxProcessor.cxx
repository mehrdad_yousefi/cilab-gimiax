/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreBBoxProcessor.h"
#include "itkImageToVTKImageFilter.h"
#include "itkRegionOfInterestImageFilter.h"
#include "coreImageDataEntityMacros.h"
#include "itkImageBase.h"
#include "itkImageRegion.h"
#include "vtkCubeSource.h"
#include "coreVTKPolyDataHolder.h"
#include "coreVTKUnstructuredGridHolder.h"


Core::BBoxProcessor::BBoxProcessor( )
{
	SetNumberOfInputs( 1 );
	GetInputPort( 0 )->SetName( "Input" );
	GetInputPort( 0 )->SetDataEntityType( Core::UnknownTypeId );
	SetNumberOfOutputs( 2 );
	GetOutputPort( 0 )->SetName( "Output" );
	GetOutputPort( 1 )->SetName( "Output bbox" );

	SetName( "BBoxProcessor" );

	m_ProcessorDataHolder = CroppingBoundingBoxHolder::New( );
	m_ProcessorDataHolder->SetSubject(Core::BoundingBox::New());
}

Core::BBoxProcessor::CroppingBoundingBoxHolder::Pointer 
Core::BBoxProcessor::GetProcessorDataHolder() const
{
	return m_ProcessorDataHolder;
}

void Core::BBoxProcessor::ResetBox()
{
	if( !GetInputDataEntity( 0 ) )
		return;

	if( GetInputDataEntity( 0 )->IsImage() )
	{
		// Generate a new instance, to avoid switching input data to VTK
		Core::vtkImageDataPtr image;
		GetProcessingData( 0, image, -1, true );
		itk::ImageRegion<3> region;
		region.SetIndex( 0, 0 );
		region.SetIndex( 1, 0 );
		region.SetIndex( 2, 0 );
		region.SetSize( 0, image->GetDimensions()[ 0 ] );
		region.SetSize( 1, image->GetDimensions()[ 1 ] );
		region.SetSize( 2, image->GetDimensions()[ 2 ] );
		GetProcessorDataHolder()->GetSubject()->SetBox( region );
		GetProcessorDataHolder()->GetSubject()->SetSpacing( image->GetSpacing() );
		GetProcessorDataHolder()->GetSubject()->SetOrigin( image->GetOrigin() );
		GetProcessorDataHolder()->NotifyObservers();
	}
	else if( GetInputDataEntity( 0 )->IsSurfaceMesh() ||
		     GetInputDataEntity( 0 )->IsVolumeMesh() )
	{
		double bounds[ 6 ];
		if ( GetInputDataEntity( 0 )->IsSurfaceMesh() )
		{
			Core::vtkPolyDataPtr surface;
			GetProcessingData( 0, surface, -1, true );
			surface->GetBounds( bounds );
		}
		else if ( GetInputDataEntity( 0 )->IsVolumeMesh() )
		{
			Core::vtkUnstructuredGridPtr inputVolumeVtk;
			GetProcessingData( 0, inputVolumeVtk, -1, true );
			inputVolumeVtk->GetBounds( bounds );
		}

		GetProcessorDataHolder()->GetSubject()->SetBounds( bounds );
		GetProcessorDataHolder()->NotifyObservers();
	}
}

void Core::BBoxProcessor::ExportBBox()
{
	Core::BoundingBox::Pointer parameters;
	parameters = GetProcessorDataHolder()->GetSubject();
	if( parameters.IsNull() )
		return ;

	double bounds[ 6 ];
	parameters->GetBounds( bounds );

	vtkSmartPointer<vtkCubeSource> cube = vtkSmartPointer<vtkCubeSource>::New();
	cube->SetBounds( bounds );
	cube->Update();

	UpdateOutput( 1, cube->GetOutput(), "Bounding Box", false, 1, GetInputDataEntity( 0 ) );
}

