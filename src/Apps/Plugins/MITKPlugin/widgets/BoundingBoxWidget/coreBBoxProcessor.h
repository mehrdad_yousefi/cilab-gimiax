/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreBBoxProcessor_H
#define _coreBBoxProcessor_H

#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreBoundingBox.h"
#include "coreBaseProcessor.h"

namespace Core{

/**
Crops an image or contour using a bounding box.

\ingroup MITKPlugin
\author Maarten Nieber
\date 18 nov 2008
*/

class PLUGIN_EXPORT BBoxProcessor : public Core::BaseProcessor
{
public:

	typedef Core::DataHolder< Core::BoundingBox::Pointer > 
		CroppingBoundingBoxHolder;

public:
	coreDeclareSmartPointerClassMacro(Core::BBoxProcessor, Core::BaseFilter);
	//!
	void ResetBox();
	//!
	CroppingBoundingBoxHolder::Pointer GetProcessorDataHolder() const;
	//!
	void ExportBBox( );
private:
	/**
	*/
	BBoxProcessor( );

private:

	//! The cropping parameters
	CroppingBoundingBoxHolder::Pointer m_ProcessorDataHolder;
};

} // Core

#endif //_coreBBoxProcessor_H
