/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreMultiRenderWindowEventHandler.h"

#include "coreWxMitkGraphicalInterface.h"
#include "coreKernel.h"
#include "coreMainMenu.h"
#include "coreSettings.h"
#include "corePluginTab.h"
#include "coreMultiRenderWindowMITK.h"
#include "coreDataTreeMITKHelper.h"
#include "coreBaseMainWindow.h"

#include "wxUnicode.h"
#include "wxMitkSelectableGLWidget.h"

using namespace Core::Widgets;

MultiRenderWindowEventHandler::MultiRenderWindowEventHandler( MultiRenderWindowMITK* renderWindow ) : wxEvtHandler()
{
	m_RenderWindow = renderWindow;

	m_RenderWindow->GetMetadataHolder( )->AddObserver( 
		this, &MultiRenderWindowEventHandler::UpdateMenu );

	UpdateMenu();
}

Core::Widgets::MultiRenderWindowEventHandler::~MultiRenderWindowEventHandler()
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
	gIface = Core::Runtime::Kernel::GetGraphicalInterface();
	wxFrame* mainWindow = dynamic_cast<wxFrame*> ( gIface->GetMainWindow( ) );
	if ( !mainWindow )
	{
		return;
	}

	wxEvtHandler* mainWindowEvtHandler = mainWindow->GetMenuBar()->GetEventHandler();

	mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuEnableAxis, this, wxID_LockAxis);
	mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuShowAnnotationCubeMenuItem, this, wxID_ShowAnnotationCubeMenuItem);
	mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuShowAnnotationMenuItem, this, wxID_ShowCornerAnnotationsMenuItem);
	mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLinkPlanesItem, this, wxID_ViewLinkPlanes);
	mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewShowLabelsItem, this, wxID_ViewShowLabels);
	mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuEnableLevelWindowInteractor, this, wxID_LevelWindowInteractor);

	mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayout2DViewsLeft);
	mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutDefault);
	mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutOnly2DViews);
	mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutXView);
	mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutYView);
	mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutZView);
	mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutBig3DView);
	mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutDefault2x2_YXZ3D);
	mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutDefault2x2_XYZ3D);

	wxMenu* menuView = GetViewMenu( );
	if ( !menuView )
	{
		return;
	}

	if ( menuView->FindItem( wxID_CurrentMultiView ) != NULL )
	{
		menuView->Destroy( wxID_CurrentMultiView );
	}
	if ( menuView->FindItem( wxID_ViewLayout ) != NULL )
	{
		menuView->Destroy( wxID_ViewLayout );
	}
}

void Core::Widgets::MultiRenderWindowEventHandler::AppendMenuItems()
{
	// edit GIMIAS menu: add MultiRenderWindow options...
	wxMenu* menuView = GetViewMenu( );

	Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
	gIface = Core::Runtime::Kernel::GetGraphicalInterface();
	wxFrame* mainWindow = dynamic_cast<wxFrame*> ( gIface->GetMainWindow( ) );
	wxEvtHandler* mainWindowEvtHandler = mainWindow->GetMenuBar()->GetEventHandler();
	
	if ( wxEvtHandler::GetEvtHandlerEnabled() )
	{

		wxMenuItemList list = menuView->GetMenuItems();
		int pos = 0;
		while ( pos < list.size( ) && !list[pos]->IsSeparator( ) )
		{
			pos++;
		}
	
		if ( menuView->FindItem( wxID_CurrentMultiView ) == NULL )
		{
			wxMenu *subMenu = new wxMenu();

			subMenu->Append( wxID_LockAxis, wxT("Lock &Axis\tCtrl+I"), wxT("Enables or disables the axis"), wxITEM_CHECK );
			mainWindowEvtHandler->Bind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuEnableAxis, this, wxID_LockAxis);

			subMenu->Append( wxID_ShowAnnotationCubeMenuItem, wxT("Show A&nnotation Cube\tCtrl+A"), wxT("Show annotation cube"), wxITEM_CHECK );
			mainWindowEvtHandler->Bind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuShowAnnotationCubeMenuItem, this, wxID_ShowAnnotationCubeMenuItem);

			subMenu->Append( wxID_ShowCornerAnnotationsMenuItem, wxT("Show Corner Ann&otations\tCtrl+N"), wxT("Show Corner Annotations"), wxITEM_CHECK );
			mainWindowEvtHandler->Bind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuShowAnnotationMenuItem, this, wxID_ShowCornerAnnotationsMenuItem);

			subMenu->Append( wxID_ViewLinkPlanes, wxT("Link planes"), wxT("Link &planes"), wxITEM_CHECK);
			mainWindowEvtHandler->Bind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLinkPlanesItem, this, wxID_ViewLinkPlanes);

			subMenu->Append(wxID_ViewShowLabels, wxT("Show landmarks labels"), wxT("Show &landmarks labels"), wxITEM_CHECK);
			mainWindowEvtHandler->Bind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewShowLabelsItem, this, wxID_ViewShowLabels);

			subMenu->Append(wxID_LevelWindowInteractor, wxT("Level window interaction\tCtrl+W"), wxT("Level &window interaction"), wxITEM_CHECK);
			mainWindowEvtHandler->Bind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuEnableLevelWindowInteractor, this, wxID_LevelWindowInteractor);

			menuView->Insert(pos,wxID_CurrentMultiView, wxT("&MultiRenderWindow"), subMenu );
		}


		if ( menuView->FindItem( wxID_ViewLayout ) == NULL )
		{
			wxMenu *subMenu = new wxMenu();
			
			subMenu->Append(wxID_ViewLayout2DViewsLeft, wxT("2D Views Left"), wxT("2D Views Left"), wxITEM_RADIO);
			mainWindowEvtHandler->Bind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayout2DViewsLeft);

			subMenu->Append(wxID_ViewLayoutDefault, wxT("ZXY View"), wxT("XYZ View"), wxITEM_RADIO);
			mainWindowEvtHandler->Bind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutDefault);
			
			subMenu->Append(wxID_ViewLayoutOnly2DViews, wxT("2D Views only"), wxT("2D Views only"), wxITEM_RADIO);
			mainWindowEvtHandler->Bind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutOnly2DViews);
			
			subMenu->Append(wxID_ViewLayoutXView, wxT("X View"), wxT("X View"), wxITEM_RADIO);
			mainWindowEvtHandler->Bind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutXView);
			
			subMenu->Append(wxID_ViewLayoutYView, wxT("Y View"), wxT("Y View"), wxITEM_RADIO);
			mainWindowEvtHandler->Bind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutYView);
			
			subMenu->Append(wxID_ViewLayoutZView, wxT("Z View"), wxT("Z View"), wxITEM_RADIO);
			mainWindowEvtHandler->Bind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutZView);
			
			subMenu->Append(wxID_ViewLayoutBig3DView, wxT("3D View"), wxT("3D View"), wxITEM_RADIO);
			mainWindowEvtHandler->Bind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutBig3DView);

			subMenu->Append(wxID_ViewLayoutDefault2x2_YXZ3D, wxT("YXZ View"), wxT("YXZ View"), wxITEM_RADIO);			
			mainWindowEvtHandler->Bind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutDefault2x2_YXZ3D);

			subMenu->Append(wxID_ViewLayoutDefault2x2_XYZ3D, wxT("XYZ View"), wxT("XYZ View"), wxITEM_RADIO);
			mainWindowEvtHandler->Bind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutDefault2x2_XYZ3D);

			menuView->Insert(pos,wxID_ViewLayout, wxT("&Layout"), subMenu );
		}
	}
	else
	{
		wxMenu* menuView = GetViewMenu( );
		if ( !menuView )
		{
			return;
		}

		if ( menuView->FindItem( wxID_CurrentMultiView ) != NULL )
		{
			mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuEnableAxis, this, wxID_LockAxis);
			mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuShowAnnotationCubeMenuItem, this, wxID_ShowAnnotationCubeMenuItem);
			mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuShowAnnotationMenuItem, this, wxID_ShowCornerAnnotationsMenuItem);
			mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLinkPlanesItem, this, wxID_ViewLinkPlanes);
			mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewShowLabelsItem, this, wxID_ViewShowLabels);
			mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuEnableLevelWindowInteractor, this, wxID_LevelWindowInteractor);

			menuView->Destroy( wxID_CurrentMultiView );
		}

		if ( menuView->FindItem( wxID_ViewLayout ) != NULL )
		{
			mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayout2DViewsLeft);
			mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutDefault);
			mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutOnly2DViews);
			mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutXView);
			mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutYView);
			mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutZView);
			mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutBig3DView);
			mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutDefault2x2_YXZ3D);
			mainWindowEvtHandler->Unbind( wxEVT_COMMAND_MENU_SELECTED, &MultiRenderWindowEventHandler::OnMenuViewLayout, this, wxID_ViewLayoutDefault2x2_XYZ3D);

			menuView->Destroy( wxID_ViewLayout );
		}


	}



}

void Core::Widgets::MultiRenderWindowEventHandler::OnMenuEnableAxis( wxCommandEvent& event )
{
	try
	{
		m_RenderWindow->EnableAxis( !event.IsChecked() );
		UpdateMenu();
	}
	coreCatchExceptionsReportAndNoThrowMacro(MultiRenderWindowEventHandler::OnMenuEnableAxis)
}

void Core::Widgets::MultiRenderWindowEventHandler::OnMenuShowAnnotationCubeMenuItem( wxCommandEvent& event )
{
	try
	{
		// Change the state
		m_RenderWindow->EnableAnnotatedCube( 
			!m_RenderWindow->GetMetadata( )->GetTagValue<bool>( "AnnotationCubeShown" ) );

		UpdateMenu();
	}
	coreCatchExceptionsReportAndNoThrowMacro(MultiRenderWindowEventHandler::OnMenuShowAnnotationCubeMenuItem)
}


void Core::Widgets::MultiRenderWindowEventHandler::OnMenuShowAnnotationMenuItem( 
	wxCommandEvent& event )
{
	try
	{
		m_RenderWindow->EnableCornerAnnotations( event.GetInt() );
		UpdateMenu();
	}
	coreCatchExceptionsReportAndNoThrowMacro(
		MultiRenderWindowEventHandler::OnMenuShowAnnotationMenuItem)
}

void Core::Widgets::MultiRenderWindowEventHandler::OnMenuEnableLevelWindowInteractor( 
	wxCommandEvent& event )
{
	try
	{
		m_RenderWindow->EnableWindowLevelInteractor( event.GetInt() );
		UpdateMenu();
	}
	coreCatchExceptionsReportAndNoThrowMacro(
		MultiRenderWindowEventHandler::OnMenuEnableLevelWindowInteractor)
}

void Core::Widgets::MultiRenderWindowEventHandler::UpdateMenu()
{
	AppendMenuItems( );

	wxMenu* menuView = GetViewMenu( );

	if ( menuView->FindItem( wxID_CurrentMultiView ) != NULL )
	{
		menuView->Check( wxID_LockAxis, 
			m_RenderWindow->GetMetadata()->GetTagValue<bool>( "AxisLocked" ) );
		menuView->Check( wxID_ShowAnnotationCubeMenuItem, 
			m_RenderWindow->GetMetadata()->GetTagValue<bool>( "AnnotationCubeShown" ) );
		menuView->Check( wxID_ShowCornerAnnotationsMenuItem, 
			m_RenderWindow->GetShowCornerAnnotations( ) );
		menuView->Check( wxID_ViewLinkPlanes, 
			m_RenderWindow->GetMetadata()->GetTagValue<bool>( "LinkPlanesEnabled" ) );
		menuView->Check( wxID_LevelWindowInteractor, 
			m_RenderWindow->GetMetadata()->GetTagValue<bool>( "LevelWindowInteractor" ) );
	}

	if ( menuView->FindItem( wxID_ViewLayout ) != NULL )
	{

		// If it's single, check current window
		int layoutType = m_RenderWindow->GetMetadata()->GetTagValue<int>( "LayoutType" );
		if ( layoutType == mitk::Single )
		{
			mitk::wxMitkMultiRenderWindow::WidgetListType widgets;
			widgets = m_RenderWindow->GetWidgets();
			mitk::wxMitkMultiRenderWindow::WidgetListType::iterator it;
			int count = 0;
			for ( it = widgets.begin() ; it != widgets.end() ; it++ )
			{
				if ( (*it)->IsShown() )
				{
					switch(count)
					{
					case mitk::ThreeD_Win:layoutType = mitk::Single3D;break;
					case mitk::X_Win:layoutType = mitk::Single2D_X;break;
					case mitk::Y_Win:layoutType = mitk::Single2D_Y;break;
					case mitk::Z_Win:layoutType = mitk::Single2D_Z;break;
					}
				}
				count++;
			}
		}

		switch ( layoutType )
		{
		case mitk::NoneLayout:menuView->Check( wxID_ViewLayoutDefault, true );break;
		case mitk::Default2x2:menuView->Check( wxID_ViewLayoutDefault, true );break;
		case mitk::Left2DRight3D:menuView->Check( wxID_ViewLayout2DViewsLeft, true );break;
		case mitk::Single3D:menuView->Check( wxID_ViewLayoutBig3DView, true );break;
		case mitk::Single2D_X:menuView->Check( wxID_ViewLayoutXView, true );break;
		case mitk::Single2D_Y:menuView->Check( wxID_ViewLayoutYView, true );break;
		case mitk::Single2D_Z:menuView->Check( wxID_ViewLayoutZView, true );break;
		case mitk::Only2D:menuView->Check( wxID_ViewLayoutOnly2DViews, true );break;
		case mitk::Only2D_XYZ:menuView->Check( wxID_ViewLayoutOnly2DViews_XYZ, true );break;
		case mitk::Default2x2_YXZ3D:menuView->Check( wxID_ViewLayoutDefault2x2_YXZ3D, true );break;
		case mitk::Default2x2_XYZ3D:menuView->Check( wxID_ViewLayoutDefault2x2_XYZ3D, true );break;
		default:break;
		}
	}
}

wxMenu* Core::Widgets::MultiRenderWindowEventHandler::GetViewMenu()
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
	gIface = Core::Runtime::Kernel::GetGraphicalInterface();
	wxFrame* mainFrame = dynamic_cast<wxFrame*> ( gIface->GetMainWindow() );
	if ( !mainFrame )
	{
		return NULL;
	}

	wxMenuBar* menuBar = mainFrame->GetMenuBar();

	int menuPos = menuBar->FindMenu(_U("View"));
	if(menuPos == wxNOT_FOUND)
	{
		return NULL;
	}

	wxMenu* menuView = menuBar->GetMenu(menuPos);
	return menuView;
}

void Core::Widgets::MultiRenderWindowEventHandler::OnMenuViewLinkPlanesItem( wxCommandEvent& event )
{
	m_RenderWindow->SetPlaneRotationLinked( event.IsChecked() );
	UpdateMenu();
}

void Core::Widgets::MultiRenderWindowEventHandler::OnMenuViewShowLabelsItem( wxCommandEvent& event )
{
	Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
	Core::DataEntityHolder::Pointer holder;
	holder = dataContainer->GetDataEntityList()->GetSelectedDataEntityHolder();

	Core::DataTreeMITKHelper::ChangeShowLabelsProperty(  
		holder->GetSubject(),
		m_RenderWindow->GetPrivateRenderingTree(),  
		event.IsChecked( ) );

	mitk::RenderingManager::GetInstance()->RequestUpdateAll();
	UpdateMenu();
}

void Core::Widgets::MultiRenderWindowEventHandler::OnMenuViewLayout( wxCommandEvent& event )
{
	mitk::LayoutConfiguration layout;

	if ( event.GetId() == wxID_ViewLayout2DViewsLeft )
	{
		layout = mitk::Left2DRight3D;
	}else if ( event.GetId() == wxID_ViewLayoutDefault )
	{
		layout = mitk::Default2x2;
	}else if ( event.GetId() == wxID_ViewLayoutOnly2DViews )
	{
		layout = mitk::Only2D;
	}else if ( event.GetId() == wxID_ViewLayoutOnly2DViews_XYZ )
	{
		layout = mitk::Only2D_XYZ;
	}else if ( event.GetId() == wxID_ViewLayoutXView )
	{
		layout = mitk::Single2D_X;
	}else if ( event.GetId() == wxID_ViewLayoutYView )
	{
		layout = mitk::Single2D_Y;
	}else if ( event.GetId() == wxID_ViewLayoutZView )
	{
		layout = mitk::Single2D_Z;
	}else if ( event.GetId() == wxID_ViewLayoutBig3DView ) 
	{
		layout = mitk::Single3D;
	}else if ( event.GetId( ) == wxID_ViewLayoutDefault2x2_YXZ3D )
	{
		layout = mitk::Default2x2_YXZ3D;
	}else if ( event.GetId( ) == wxID_ViewLayoutDefault2x2_XYZ3D )
	{
		layout = mitk::Default2x2_XYZ3D;
	}

	m_RenderWindow->SetCurrentLayout( layout );

	UpdateMenu();
}

void Core::Widgets::MultiRenderWindowEventHandler::SetEvtHandlerEnabled( bool enabled )
{
	bool modified = enabled != wxEvtHandler::GetEvtHandlerEnabled();

	wxEvtHandler::SetEvtHandlerEnabled( enabled );

	if ( modified )
	{
		UpdateMenu( );
	}
}

void Core::Widgets::MultiRenderWindowEventHandler::SetRenderWindow( MultiRenderWindowMITK* renderWindow )
{
	m_RenderWindow = renderWindow;
}

bool Core::Widgets::MultiRenderWindowEventHandler::ProcessEvent(wxEvent& event)
{
	if ( m_RenderWindow == NULL )
	{
		throw Core::Exceptions::Exception( "MultiRenderWindowEventHandler::ProcessEvent", 
			"PrecessEvent called after the handler has been uninitialized" );
	}

	return wxEvtHandler::ProcessEvent( event );
}
