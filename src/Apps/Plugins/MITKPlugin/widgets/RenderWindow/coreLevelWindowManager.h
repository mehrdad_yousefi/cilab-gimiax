/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreLevelWindowManager_H
#define coreLevelWindowManager_H

#include "coreObject.h"

namespace Core
{
	class RenderingTreeMITK;

/** 
\brief Manage level window property of images

\ingroup MITKPlugin
\author Xavi Planes
\date July 2012
*/
class PLUGIN_EXPORT LevelWindowManager : public Core::SmartPointerObject
{

public:
	coreDeclareSmartPointerClassMacro(Core::LevelWindowManager, Core::SmartPointerObject);

	//!
	void SetRenderingTree( Core::RenderingTreeMITK* renderingTree );

protected:

	//!
	LevelWindowManager( );

	//!
	virtual ~LevelWindowManager( );

	//!
	void OnModifiedRenderingTree( );

protected:

	//!
	Core::RenderingTreeMITK* m_RenderingTree;
};

}

#endif // coreLevelWindowManager_H

