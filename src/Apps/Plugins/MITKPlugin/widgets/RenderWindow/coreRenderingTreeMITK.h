/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#ifndef coreRenderingTreeMITK_H
#define coreRenderingTreeMITK_H

#include "coreRenderingTree.h"
#include "mitkDataStorage.h"
#include "blMITKUtils.h"
#include "coreLevelWindowManager.h"

namespace Core
{
/**
\brief Specialization of RenderingTree for MITK

\ingroup MITKPlugin
\author Xavi Planes
\date Oct 2010
*/
class PLUGIN_EXPORT RenderingTreeMITK : public Core::RenderingTree
{
public:
	coreDeclareSmartPointerClassMacro(Core::RenderingTreeMITK, Core::RenderingTree);
	
	//! Redefined
	virtual boost::any Add( DataEntity::Pointer dataEntity, bool bShow = true, bool bInitializeViews = true );
	//! Redefined
	virtual bool Remove(unsigned int dataEntityId, bool bInitializeViews = true);
	//! Redefined
	void RemoveAll();
	//! Redefined
	long GetNumberOfDataEntities( );
	//! Redefined
	boost::any GetNode(Core::DataEntity* dataEntity);
	//! Redefined
	boost::any GetNode(unsigned int dataEntityId);
	//! Redefined
	std::vector<DataEntity::Pointer> GetAllDataEntities( );
	//! Redefined
	virtual void Show( DataEntity::Pointer dataEntity, bool bShow, bool initializeViews = true );
	//! Redefined
	bool IsDataEntityRendered(Core::DataEntity* dataEntity);
	//! Redefined
	bool IsDataEntityShown(Core::DataEntity* dataEntity);
	//! Redefined
	void UpdateMetadata( Core::DataEntity::Pointer dataEntity );
	//! Redefined
	void UpdateRenderingProperties( Core::DataEntity::Pointer dataEntity );
	//! Redefined
	void SetProperty( Core::DataEntity::Pointer, blTag::Pointer property );
	//! Redefined
	blTag::Pointer GetProperty( Core::DataEntity::Pointer dataEntity, const std::string& name );

	//!
	mitk::DataStorage::Pointer GetDataStorage() const;
	void SetDataStorage(mitk::DataStorage::Pointer tree);

	//!
	mitk::DataStorage::SetOfObjects::ConstPointer GetAllDataEntityNodes( );

	//! Get the data entity from a node
	static Core::DataEntity* GetDataEntity( mitk::DataTreeNode::Pointer node );
	//!
	bool HasNode(mitk::DataTreeNode::Pointer node);
	//!
	mitk::DataTreeNode::Pointer GetRootNode(void);
	//!
	virtual bool Remove(mitk::DataTreeNode::Pointer node, bool bInitializeViews = true);
	//!
	bool HasDataEntityOfType(Core::DataEntityType type);

protected:

	//!
	RenderingTreeMITK(void);
	virtual ~RenderingTreeMITK(void);

	//!
	void OnModifiedDataEntity(Core::SmartPointerObject* dataEntity);

	//!
	void ConnectToOnModifiedAnyNodeSignal(Core::DataEntity::Pointer dataEntity);

	//!
	void DisconnectToOnModifiedAnyNodeSignal(Core::DataEntity* dataEntity);

	//!
	mitk::DataTreeNode::Pointer CreateRenderingNodeWithDefaultProperties(
		Core::DataEntity::Pointer dataEntity);

	//!
	void OnModifiedRenderingTreeNode( );

	coreDeclareNoCopyConstructors(RenderingTreeMITK);

private:
	//!
	mitk::DataStorage::Pointer m_DataStorage;

	//! Map Core::DataEntity ID with observer of property node TAG
	std::map<mitk::DataTreeNode::Pointer, unsigned long> m_MapObserversNode;

	//!
	Core::LevelWindowManager::Pointer m_LevelWindowManager;
};

} // namespace Core

#endif

