/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreMultiRenderWindowFocus_H
#define coreMultiRenderWindowFocus_H

#include "coreObject.h"
#include "coreDataEntityHolder.h"

namespace Core
{
namespace Widgets
{
	class MultiRenderWindowMITK;
/** 
\brief Center the MultiRenderWindow 2D views to one points
depending on the slected data from the data list

\ingroup MITKPlugin
\author Xavi Planes
\date 16 July 2009
*/
class PLUGIN_EXPORT MultiRenderWindowFocus : public Core::SmartPointerObject
{
public:
	coreDeclareSmartPointerClassMacro(Core::Widgets::MultiRenderWindowFocus, Core::SmartPointerObject);

	//!
	void Init( 
		Core::Widgets::MultiRenderWindowMITK* multiRenderWindow,
		Core::DataEntityHolder::Pointer selectedDataEntity );

	//!
	double GetPixelValue() const;
	void SetPixelValue(double val);

protected:

	MultiRenderWindowFocus( );

	virtual ~MultiRenderWindowFocus( );

	//!
	void OnSelectedModified( );

protected:

	//! The window to center
	Core::Widgets::MultiRenderWindowMITK* m_MultiRenderWindow;

	//!
	Core::DataEntityHolder::Pointer m_SelectedDataEntity;

	//!
	double m_PixelValue;
};

}
}

#endif // coreMultiRenderWindowFocus_H

