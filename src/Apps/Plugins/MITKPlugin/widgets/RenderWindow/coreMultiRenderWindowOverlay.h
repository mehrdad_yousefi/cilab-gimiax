/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreMultiRenderWindowOverlay_H
#define coreMultiRenderWindowOverlay_H

#include "coreObject.h"
#include "coreDataEntityHolder.h"

#include "itkCommand.h"
#include "itkImage.h"

#include "mitkVector.h"

#include "wxMitkSelectableGLWidget.h"

namespace mitk
{
	class Stepper;
	class Image;
}

namespace Core
{
namespace Widgets
{
class MultiRenderWindowMITK;

/** 
\brief Show overlay info to the 2D views

\ingroup MITKPlugin
\author Xavi Planes
\date 29 July 2009
*/
class PLUGIN_EXPORT MultiRenderWindowOverlay : public Core::SmartPointerObject
{
	typedef std::map<mitk::wxMitkSelectableGLWidget*, unsigned long> TagMapType;

public:
	coreDeclareSmartPointerClassMacro(Core::Widgets::MultiRenderWindowOverlay, Core::SmartPointerObject);

	//!
	void Init( 
		Core::Widgets::MultiRenderWindowMITK* multiRenderWindow,
		Core::DataEntityHolder::Pointer selectedDataEntity );

	//!
	void RefreshOverlayText( );

	//! 
	static void GetPixelValue( 
		mitk::Image* image, 
		mitk::Point3D axisCenter,
		unsigned int timeStep,
		std::string &pixelValue,
		mitk::Point3D &pt_units );

protected:

	MultiRenderWindowOverlay( );

	virtual ~MultiRenderWindowOverlay( );

	//!
	void OnSelectedModified( );

	//!
	void OnPosStepperChanged();

	//!
	void OnTimestepChanged();
	
	//!
	void OnChangedMetadata (blTagMap* tagMap, const std::string &name);

	//!
	void InitOberversToSteppers( );

	//!
	void RemoveOberversToSteppers( );

	//!
	mitk::Stepper* GetTime();

	//!
	std::string GetPatientInfo( );

	//!
	std::string GetAxisPixelValue( );

	//!
	std::string GetSlicePos( mitk::wxMitkSelectableGLWidget* widget );
protected:

	//! The window to center
	Core::Widgets::MultiRenderWindowMITK* m_MultiRenderWindow;

	//!
	Core::DataEntityHolder::Pointer m_SelectedDataEntity;

	//!
	itk::SimpleMemberCommand<MultiRenderWindowOverlay>::Pointer m_PosStepperChangedCommand;

	//!
	itk::SimpleMemberCommand<MultiRenderWindowOverlay>::Pointer m_TimeStepperChangedCommand;

	//!
	TagMapType m_PosStepperChangedCommandTag;

	//!
	unsigned long m_TimeStepperChangedCommandTagT;

	//!
	typedef itk::Image< double, 3 > ImageType;

};

}
}

#endif // coreMultiRenderWindowOverlay_H

