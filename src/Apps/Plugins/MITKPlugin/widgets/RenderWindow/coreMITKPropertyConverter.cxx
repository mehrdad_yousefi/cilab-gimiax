/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreMITKPropertyConverter.h"
#include "mitkMaterialProperty.h"
#include "mitkColorProperty.h"
#include "mitkTransferFunction.h"

using namespace Core;

//!
MitkPropertyConverter::MitkPropertyConverter(void)
{
	m_FromMITKToTag = true;
}

//!
MitkPropertyConverter::~MitkPropertyConverter(void)
{
}
blTag::Pointer Core::MitkPropertyConverter::GetTag() const
{
	return m_Tag;
}

void Core::MitkPropertyConverter::SetTag( blTag::Pointer val )
{
	m_Tag = val;
}

mitk::BaseProperty::Pointer Core::MitkPropertyConverter::GetMitkProperty() const
{
	return m_MitkProperty;
}

void Core::MitkPropertyConverter::SetMitkProperty( mitk::BaseProperty* val )
{
	m_MitkProperty = val;
}

std::string Core::MitkPropertyConverter::GetPropertyKey() const
{
	return m_PropertyKey;
}

void Core::MitkPropertyConverter::SetPropertyKey( std::string val )
{
	m_PropertyKey = val;
}

void Core::MitkPropertyConverter::SetNode( mitk::DataTreeNode::Pointer node )
{
	m_Node = node;
}

void Core::MitkPropertyConverter::Update()
{
	if ( m_FromMITKToTag )
	{
		ConvertFromMITK();
	}
	else
	{
		ConvertToMITK();
	}
}

void Core::MitkPropertyConverter::SetConvertFromMITK( )
{
	m_FromMITKToTag = true;
}


void Core::MitkPropertyConverter::SetConvertToMITK( )
{
	m_FromMITKToTag = false;
}

void Core::MitkPropertyConverter::ConvertToMITK( )
{
	m_MitkProperty = NULL;
	m_PropertyKey = "";
	if ( m_Tag.IsNull() )
	{
		return;
	}

	m_PropertyKey = m_Tag->GetName();
	if ( m_Tag->GetValue( ).type() == typeid( bool ) )
	{
		mitk::BoolProperty::Pointer prop;
		prop = mitk::BoolProperty::New( m_Tag->GetValueCasted<bool>( ) );
		m_MitkProperty = prop.GetPointer();
	}
	else if ( m_Tag->GetValue( ).type() == typeid( int ) )
	{
		mitk::IntProperty::Pointer prop;
		prop = mitk::IntProperty::New( m_Tag->GetValueCasted<int>( ) );
		m_MitkProperty = prop.GetPointer();
	}
	else if ( m_Tag->GetValue( ).type() == typeid( float ) )
	{
		mitk::FloatProperty::Pointer prop;
		prop = mitk::FloatProperty::New( m_Tag->GetValueCasted<float>( ) );
		m_MitkProperty = prop.GetPointer();
	}
	else if ( m_Tag->GetValue( ).type() == typeid( double ) )
	{
		mitk::FloatProperty::Pointer prop;
		prop = mitk::FloatProperty::New( m_Tag->GetValueCasted<double>( ) );
		m_MitkProperty = prop.GetPointer();
	}
	else if ( m_Tag->GetValue( ).type() == typeid( std::string ) )
	{
		std::string value = m_Tag->GetValueCasted<std::string>( );
		if ( m_Tag->GetName().find( "color" ) != std::string::npos )
		{
			mitk::Color color;
			std::stringstream stream;
			stream.str( value );
			stream >> color;
			mitk::ColorProperty::Pointer prop = mitk::ColorProperty::New( color );
			m_MitkProperty = prop.GetPointer();
		}
		else if ( m_Tag->GetName( ) == "scalar mode" )
		{
			mitk::VtkScalarModeProperty::Pointer scalarMode = mitk::VtkScalarModeProperty::New();
			scalarMode->SetValue( m_Tag->GetValueCasted<std::string>( ) );
			m_MitkProperty = scalarMode.GetPointer();
		}
		else
		{
			mitk::StringProperty::Pointer prop;
			prop = mitk::StringProperty::New( value );
			m_MitkProperty = prop.GetPointer();
		}
	}
	else if ( m_Tag->GetValue( ).type() == typeid( blTagMap::Pointer ) )
	{
		if ( m_Tag->GetName( ) == "levelwindow" )
		{
			blTagMap::Pointer levelWinTag = m_Tag->GetValueCasted<blTagMap::Pointer>( );
			double center = levelWinTag->GetTagValue<double>( "center" );
			double width = levelWinTag->GetTagValue<double>( "width" );
			double lowerWindowBound = levelWinTag->GetTagValue<double>( "lowerWindowBound" );
			double upperWindowBound = levelWinTag->GetTagValue<double>( "upperWindowBound" );
			double rangeMin = levelWinTag->GetTagValue<double>( "rangeMin" );
			double rangeMax = levelWinTag->GetTagValue<double>( "rangeMax" );
			double defaultRangeMin = levelWinTag->GetTagValue<double>( "defaultRangeMin" );
			double defaultRangeMax = levelWinTag->GetTagValue<double>( "defaultRangeMax" );

			mitk::LevelWindow levelWindow( center, width );
			levelWindow.SetDefaultRangeMinMax(defaultRangeMin, defaultRangeMax);
			levelWindow.SetRangeMinMax(rangeMin, rangeMax);
			levelWindow.SetWindowBounds(lowerWindowBound, upperWindowBound);
			levelWindow.SetDefaultLevelWindow(center, width);

			mitk::LevelWindowProperty::Pointer levelwinProp;
			levelwinProp = mitk::LevelWindowProperty::New( );
			levelwinProp->SetLevelWindow( levelWindow );
			m_MitkProperty = levelwinProp.GetPointer();
		}
		else if ( m_Tag->GetName( ) == "TransferFunction" )
		{
			blTagMap::Pointer tagTransferFunction = m_Tag->GetValueCasted<blTagMap::Pointer>( );

			mitk::TransferFunction::Pointer transferFunction = mitk::TransferFunction::New( );

			// First min max, later Mode
			int min = tagTransferFunction->GetTagValue<int>( "Min" );
			int max = tagTransferFunction->GetTagValue<int>( "Max" );
			transferFunction->SetMax( max );
			transferFunction->SetMin( min );

			int mode = tagTransferFunction->GetTagValue<int>( "Mode" );
			transferFunction->SetTransferFunctionMode( mode );

			if ( mode == mitk::TransferFunction::TF_CUSTOM )
			{
				// ScalarOpacityPoints points
				blTagMap::Pointer tagScalarOpacityPoints;
				tagScalarOpacityPoints = tagTransferFunction->GetTagValue<blTagMap::Pointer>( "ScalarOpacityPoints" );
				mitk::TransferFunction::ControlPoints scalarOpacityPoints;
				for ( blTagMap::Iterator it = tagScalarOpacityPoints->GetIteratorBegin() ; 
					it != tagScalarOpacityPoints->GetIteratorEnd() ; it++ )
				{
					std::pair<double, double> point;
					point.first = atof( it->first.c_str( ) );
					point.second = it->second->GetValueCasted<double>( );
					scalarOpacityPoints.push_back( point );
				}
				transferFunction->SetScalarOpacityPoints( scalarOpacityPoints );

				// GradientOpacityPoints points
				blTagMap::Pointer tagGradientOpacityPoints;
				tagGradientOpacityPoints = tagTransferFunction->GetTagValue<blTagMap::Pointer>( "GradientOpacityPoints" );
				mitk::TransferFunction::ControlPoints gradientOpacityPoints;
				for ( blTagMap::Iterator it = tagGradientOpacityPoints->GetIteratorBegin() ; 
					it != tagGradientOpacityPoints->GetIteratorEnd() ; it++ )
				{
					std::pair<double, double> point;
					point.first = atof( it->first.c_str( ) );
					point.second = it->second->GetValueCasted<double>( );
					gradientOpacityPoints.push_back( point );
				}
				transferFunction->SetGradientOpacityPoints( gradientOpacityPoints );

				// RGBPoints points
				blTagMap::Pointer tagRGBPoints;
				tagRGBPoints = tagTransferFunction->GetTagValue<blTagMap::Pointer>( "RGBPoints" );
				mitk::TransferFunction::RGBControlPoints RGBPoints;
				for ( blTagMap::Iterator it = tagRGBPoints->GetIteratorBegin() ; 
					it != tagRGBPoints->GetIteratorEnd() ; it++ )
				{
					std::pair<double, itk::RGBPixel<double> > point;
					std::stringstream stream;
					stream.str( it->first );
					stream >> point.first;

					std::string value = it->second->GetValueCasted<std::string>( );
					std::stringstream colorStream;
					colorStream.str( value );
					colorStream >> point.second;

					RGBPoints.push_back( point );
				}
				transferFunction->SetRGBPoints( RGBPoints );
			}

			mitk::TransferFunctionProperty::Pointer transferFunctionProp;
			transferFunctionProp = mitk::TransferFunctionProperty::New( transferFunction );
			m_MitkProperty = transferFunctionProp.GetPointer();
		}
		else if ( m_Tag->GetName( ) == "material" )
		{
			blTagMap::Pointer tagMaterial = m_Tag->GetValueCasted<blTagMap::Pointer>( );
			// Set node to forward interpolation property
			mitk::MaterialProperty::Pointer material = mitk::MaterialProperty::New( m_Node );
			mitk::Color color;

			material->SetAmbientCoefficient( tagMaterial->GetTagValue<double>( "AmbientCoefficient" ) );
			std::stringstream stream1;
			stream1.str( tagMaterial->GetTag( "AmbientColor" )->GetValueAsString() );
			stream1 >> color;
			material->SetAmbientColor( color );

			material->SetDiffuseCoefficient( tagMaterial->GetTagValue<double>( "DiffuseCoefficient" ) );
			std::stringstream stream2;
			stream2.str( tagMaterial->GetTag( "DiffuseColor" )->GetValueAsString() );
			stream2 >> color;
			material->SetDiffuseColor( color );

			material->SetSpecularCoefficient( tagMaterial->GetTagValue<double>( "SpecularCoefficient" ) );
			std::stringstream stream3;
			stream3.str( tagMaterial->GetTag( "SpecularColor" )->GetValueAsString() );
			stream3 >> color;
			material->SetSpecularColor( color );
			material->SetSpecularPower( tagMaterial->GetTagValue<double>( "SpecularPower" ) );

			std::stringstream stream4;
			stream4.str( tagMaterial->GetTag( "Color" )->GetValueAsString() );
			stream4 >> color;
			material->SetColor( color );

			material->SetOpacity( tagMaterial->GetTagValue<double>( "Opacity" ) );
			material->SetInterpolation( mitk::MaterialProperty::InterpolationType( tagMaterial->GetTagValue<int>( "Interpolation" ) ) );
			material->SetRepresentation( mitk::MaterialProperty::RepresentationType( tagMaterial->GetTagValue<int>( "Representation" ) ) );
			material->SetLineWidth( tagMaterial->GetTagValue<float>( "LineWidth" ) );
			material->SetPointSize( tagMaterial->GetTagValue<float>( "PointSize" ) );

			m_MitkProperty = material.GetPointer();

		}
	}

}

void Core::MitkPropertyConverter::ConvertFromMITK(  )
{
	m_Tag = NULL;

	if ( m_MitkProperty.IsNull( ) )
	{
		return;
	}

	if ( strcmp( m_MitkProperty->GetNameOfClass(), "BoolProperty" ) == 0 )
	{
		mitk::BoolProperty::Pointer p = dynamic_cast<mitk::BoolProperty*>( m_MitkProperty.GetPointer( ) );
		m_Tag = blTag::New( m_PropertyKey, p->GetValue() );
	}
	else if ( strcmp( m_MitkProperty->GetNameOfClass(), "IntProperty" ) == 0 )
	{
		mitk::IntProperty::Pointer p = dynamic_cast<mitk::IntProperty*>( m_MitkProperty.GetPointer( ) );
		m_Tag = blTag::New( m_PropertyKey, p->GetValue() );
	}
	else if ( strcmp( m_MitkProperty->GetNameOfClass(), "FloatProperty" ) == 0 )
	{
		mitk::FloatProperty::Pointer p = dynamic_cast<mitk::FloatProperty*>( m_MitkProperty.GetPointer( ) );
		m_Tag = blTag::New( m_PropertyKey, p->GetValue() );
	}
	else if ( strcmp( m_MitkProperty->GetNameOfClass(), "StringProperty" ) == 0 )
	{
		mitk::StringProperty::Pointer p = dynamic_cast<mitk::StringProperty*>( m_MitkProperty.GetPointer( ) );
		m_Tag = blTag::New( m_PropertyKey, std::string( p->GetValue() ) );
	}
	else if ( strcmp( m_MitkProperty->GetNameOfClass(), "VtkScalarModeProperty" ) == 0 )
	{
		mitk::VtkScalarModeProperty::Pointer p = dynamic_cast<mitk::VtkScalarModeProperty*>( m_MitkProperty.GetPointer( ) );
		m_Tag = blTag::New( m_PropertyKey, p->GetValueAsString() );
	}
	else if ( strcmp( m_MitkProperty->GetNameOfClass(), "ColorProperty" ) == 0 )
	{
		mitk::ColorProperty* p = dynamic_cast<mitk::ColorProperty*>( m_MitkProperty.GetPointer( ) );
		mitk::Color color = p->GetValue();
		std::stringstream stream;
		stream << color;
		m_Tag = blTag::New( m_PropertyKey, stream.str(	) );
	}
	else if ( strcmp( m_MitkProperty->GetNameOfClass(), "LevelWindowProperty" ) == 0 )
	{
		mitk::LevelWindowProperty* p = dynamic_cast<mitk::LevelWindowProperty*>( m_MitkProperty.GetPointer( ) );
		blTagMap::Pointer levelWin = blTagMap::New();
		double center = p->GetLevelWindow( ).GetLevel();
		double window = p->GetLevelWindow( ).GetWindow();
		double lowerWindowBound = p->GetLevelWindow().GetLowerWindowBound( );
		double upperWindowBound = p->GetLevelWindow().GetUpperWindowBound( );
		double rangeMin = p->GetLevelWindow().GetRangeMin( );
		double rangeMax = p->GetLevelWindow().GetRangeMax( );
		double defaultRangeMin = p->GetLevelWindow().GetDefaultRangeMin( );
		double defaultRangeMax = p->GetLevelWindow().GetDefaultRangeMax( );

		levelWin->AddTag( "center", center );
		levelWin->AddTag( "width", window );
		levelWin->AddTag( "lowerWindowBound", lowerWindowBound );
		levelWin->AddTag( "upperWindowBound", upperWindowBound );
		levelWin->AddTag( "rangeMin", rangeMin );
		levelWin->AddTag( "rangeMax", rangeMax );
		levelWin->AddTag( "defaultRangeMin", defaultRangeMin );
		levelWin->AddTag( "defaultRangeMax", defaultRangeMax );

		m_Tag = blTag::New( "levelwindow", levelWin );
	}
	else if ( strcmp( m_MitkProperty->GetNameOfClass(), "TransferFunctionProperty" ) == 0 )
	{
		mitk::TransferFunctionProperty* p = dynamic_cast<mitk::TransferFunctionProperty*>( m_MitkProperty.GetPointer( ) );
		mitk::TransferFunction::Pointer transferFunction = p->GetValue( );
		blTagMap::Pointer tagTF = blTagMap::New();

		tagTF->AddTag( "Mode", int( transferFunction->GetTransferFunctionMode() ) );
		if ( transferFunction->GetTransferFunctionMode() == mitk::TransferFunction::TF_CUSTOM )
		{
			// ScalarOpacityPoints points
			blTagMap::Pointer tagScalarOpacityPoints = blTagMap::New();
			mitk::TransferFunction::ControlPoints scalarOpacityPoints;
			scalarOpacityPoints = transferFunction->GetScalarOpacityPoints( );
			for(unsigned int i=0; i<=scalarOpacityPoints.size()-1;i++)
			{
				std::stringstream stream;
				stream << scalarOpacityPoints[i].first;
				tagScalarOpacityPoints->AddTag( stream.str( ), scalarOpacityPoints[i].second );
			}
			tagTF->AddTag( "ScalarOpacityPoints", tagScalarOpacityPoints );

			// GradientOpacityPoints points
			blTagMap::Pointer tagGradientOpacityPoints = blTagMap::New();
			mitk::TransferFunction::ControlPoints gradientOpacityPoints;
			gradientOpacityPoints = transferFunction->GetGradientOpacityPoints( );
			for(unsigned int i=0; i<=gradientOpacityPoints.size()-1;i++)
			{
				std::stringstream stream;
				stream << gradientOpacityPoints[i].first;
				tagGradientOpacityPoints->AddTag( stream.str( ), gradientOpacityPoints[i].second );
			}
			tagTF->AddTag( "GradientOpacityPoints", tagGradientOpacityPoints );

			// RGBPoints points
			blTagMap::Pointer tagRGBPoints = blTagMap::New();
			mitk::TransferFunction::RGBControlPoints RGBPoints;
			RGBPoints = transferFunction->GetRGBPoints( );
			for(unsigned int i=0; i<RGBPoints.size();i++)
			{
				std::stringstream stream;
				stream << RGBPoints[i].first;
				std::stringstream color;
				color << RGBPoints[i].second;
				tagRGBPoints->AddTag( stream.str( ), color.str( ) );
			}
			tagTF->AddTag( "RGBPoints", tagRGBPoints );

		}

		tagTF->AddTag( "Min", transferFunction->GetMin() );
		tagTF->AddTag( "Max", transferFunction->GetMax() );

		m_Tag = blTag::New( "TransferFunction", tagTF );
	}
	else if ( strcmp( m_MitkProperty->GetNameOfClass(), "MaterialProperty" ) == 0 )
	{
		mitk::MaterialProperty::Pointer p = dynamic_cast<mitk::MaterialProperty*>(m_MitkProperty.GetPointer());

		blTagMap::Pointer tagMaterial = blTagMap::New();
		
		tagMaterial->AddTag( "AmbientCoefficient", double( p->GetAmbientCoefficient() ) );
		std::stringstream stream1;
		stream1 << p->GetAmbientColor();
		tagMaterial->AddTag( "AmbientColor", stream1.str() );

		tagMaterial->AddTag( "DiffuseCoefficient", double( p->GetDiffuseCoefficient() ) );
		std::stringstream stream2;
		stream2 << p->GetDiffuseColor();
		tagMaterial->AddTag( "DiffuseColor", stream2.str() );

		tagMaterial->AddTag( "SpecularCoefficient", double( p->GetSpecularCoefficient() ) );
		std::stringstream stream3;
		stream3 << p->GetSpecularColor();
		tagMaterial->AddTag( "SpecularColor", stream3.str() );
		tagMaterial->AddTag( "SpecularPower", double( p->GetSpecularPower() ) );

		std::stringstream stream4;
		stream4 << p->GetColor();
		tagMaterial->AddTag( "Color", stream4.str() );

		tagMaterial->AddTag( "Opacity", double( p->GetOpacity() ) );
		tagMaterial->AddTag( "Interpolation", int( p->GetInterpolation() ) );
		tagMaterial->AddTag( "Representation", int( p->GetRepresentation() ) );
		tagMaterial->AddTag( "LineWidth", float( p->GetLineWidth() ) );
		tagMaterial->AddTag( "PointSize", float( p->GetPointSize() ) );

		m_Tag = blTag::New( "material", tagMaterial );
	}

}
