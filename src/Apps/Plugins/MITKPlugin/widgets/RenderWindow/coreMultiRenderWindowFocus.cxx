/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreMultiRenderWindowFocus.h"
#include "coreMultiRenderWindowMITK.h"
#include "coreDataEntity.h"
#include "coreRenDataFactory.h"

#include "blMITKUtils.h"


Core::Widgets::MultiRenderWindowFocus::MultiRenderWindowFocus()
{
	m_MultiRenderWindow = NULL;
	m_PixelValue = 1;
}

Core::Widgets::MultiRenderWindowFocus::~MultiRenderWindowFocus()
{
	if ( m_SelectedDataEntity.IsNotNull() )
	{
		m_SelectedDataEntity->RemoveObserver( 
			this, 
			&MultiRenderWindowFocus::OnSelectedModified );
	}
}

void Core::Widgets::MultiRenderWindowFocus::Init( 
	Core::Widgets::MultiRenderWindowMITK* multiRenderWindow, 
	Core::DataEntityHolder::Pointer selectedDataEntity )
{
	m_MultiRenderWindow = multiRenderWindow;
	m_SelectedDataEntity = selectedDataEntity;
	m_SelectedDataEntity->AddObserver( 
		this, 
		&MultiRenderWindowFocus::OnSelectedModified );
}

void Core::Widgets::MultiRenderWindowFocus::OnSelectedModified()
{
	if ( m_SelectedDataEntity.IsNull() ||
		 m_SelectedDataEntity->GetSubject().IsNull( ) ||
		 !m_MultiRenderWindow->/*IsEnabled()*/IsThisEnabled() )
	{
		return;
	}

	Core::DataEntity::Pointer dataEntity;
	dataEntity = m_SelectedDataEntity->GetSubject();

	if ( dataEntity->IsROI( ) )
	{
		// Center the 2D views to the first point of the mask image
		mitk::BaseData::Pointer mitkData;
		mitkData = Core::RenDataFactory::GetBaseRenderingData( dataEntity );
		mitk::Image* image;
		image = dynamic_cast<mitk::Image*>(mitkData.GetPointer()); 
		if ( image != NULL )
		{
			int iTimePos = m_MultiRenderWindow->GetTimeNavigationController()->GetTime( )->GetPos();
			mitk::Point3D point;
			bool found;
			found = blMITKUtils::ExtractImagePoint( image, iTimePos, m_PixelValue, point );
			if ( found )
			{
				m_MultiRenderWindow->SelectSliceByPoint( point );
			}
		}
	}


}

double Core::Widgets::MultiRenderWindowFocus::GetPixelValue() const
{
	return m_PixelValue;
}

void Core::Widgets::MultiRenderWindowFocus::SetPixelValue( double val )
{
	m_PixelValue = val;
}
