/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreRenderingTreeMITK.h"
#include "coreAssert.h"
#include "coreReportExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityInfoHelper.h"
#include "coreEnvironment.h"
#include "coreLogger.h"
#include "coreKernel.h"
#include "coreDataTreeMITKHelper.h"
#include "coreMitkProperties.h"
#include "coreRenDataFactory.h"
#include "coreVTKImageDataHolder.h"
#include "coreMITKPropertyConverter.h"

#include "mitkRenderingManager.h"
#include "mitkDataStorage.h"
#include "mitkProperties.h"
#include "mitkStringProperty.h"
#include "mitkLevelWindowProperty.h"
#include "mitkMaterialProperty.h"
#include "mitkDataTreeHelper.h"
#include "mitkRenderingManager.h"
#include "mitkStandaloneDataStorage.h"
#include "mitkNodePredicateProperty.h"

#include "blMITKUtils.h"

#include <sstream>

using namespace Core;

//!
RenderingTreeMITK::RenderingTreeMITK(void)
{
	typedef mitk::GenericProperty<boost::any> DataEntityProperty;
	// Set the internal-tree
	this->m_DataStorage = mitk::StandaloneDataStorage::New();

	// Setup the defaults of the tree
	mitk::DataTreeNode::Pointer node;
	std::string nodeName = "<root>";
	
	m_LevelWindowManager = Core::LevelWindowManager::New( );
	m_LevelWindowManager->SetRenderingTree( this );

	try
	{
		// Each rendering tree should have one single root element, that holds an empty Data Entity.
		node = mitk::DataTreeNode::New();
		node->SetProperty("name", mitk::StringProperty::New(nodeName));
		node->SetProperty("renderingId", mitk::IntProperty::New(0));
		node->SetProperty("helper object", mitk::BoolProperty::New(true));
		node->SetProperty("dataEntity", Core::DataEntityProperty::New(NULL));
		m_DataStorage->Add( node );
	}
	coreCatchExceptionsAddTraceAndThrowMacro(RenderingTreeMITK::RenderingTreeMITK);
}

//!
RenderingTreeMITK::~RenderingTreeMITK(void)
{
	m_LevelWindowManager->SetRenderingTree( NULL );

	// The mitk::DataTree is still in memory because there are some references
	// from mitk::DataStorage (the reference count is not 0 here)
	// Some rendering data has a reference to the processing data of the 
	// DataEntity. We need to remove all references before destroying the 
	// DataEntities.
	RemoveAll( );

	// Remove the reference from the data storage
	m_DataStorage = NULL;
}

/** 
Sets the mitk tree internal representation to be held by the RenderingTreeMITK
*/
void RenderingTreeMITK::SetDataStorage(mitk::DataStorage::Pointer dataStorage)
{ 
	this->m_DataStorage = dataStorage;
}

/**
Returns true if the tree contains a node representing a data entity of a given type.
*/
bool RenderingTreeMITK::HasDataEntityOfType(Core::DataEntityType type)
{
	mitk::NodePredicateProperty::Pointer predicate;
	predicate = mitk::NodePredicateProperty::New("dataType", mitk::IntProperty::New(type));
	mitk::DataTreeNode::Pointer node;
	node = m_DataStorage->GetNode( predicate );
	bool found = node.IsNotNull();

	return found;
}

/**
*/
boost::any RenderingTreeMITK::Add(
					DataEntity::Pointer dataEntity,
					bool bShow /*= true*/,
					bool bInitializeViews /*= true*/ )
{
	if ( dataEntity.IsNull() )
	{
		throw Core::Exceptions::Exception( "RenderingTreeMITK::Add", 
			"The requested DataEntity cannot be NULL, if you plan to add it to the RenderingTreeMITK" );
	}
	coreAssertMacro(this->m_DataStorage.IsNotNull() 
		&& "The RenderingTreeMITK internal structures must be valid and initialized");
	mitk::DataTreeNode::Pointer node;
	try
	{
		Core::CastAnyProcessingData( GetNode( dataEntity ), node );
		bool bDataIsInRenderingTree = node.IsNotNull();

		// Create the node and add it to the rendering tree
		if ( !bDataIsInRenderingTree )
		{
			node = CreateRenderingNodeWithDefaultProperties(dataEntity);
			if(node.IsNotNull())
			{
				node->SetVisibility( bShow );

				// Get parent node
				DataEntity::Pointer parentDataEntity = dataEntity->GetFather();
				mitk::DataTreeNode::Pointer parentNode;
				Core::CastAnyProcessingData( GetNode( parentDataEntity ), parentNode );

				// Connect signal before adding the node, to avoid calling the factory twice
				ConnectToOnModifiedSignal(dataEntity);

				m_DataStorage->Add( node, parentNode );

				// Apply the rendering properties to overwrite default ones
				// After is added to the data storage
				UpdateRenderingProperties( dataEntity );

				ConnectToOnModifiedAnyNodeSignal(dataEntity);

				// Add the connections so as to enable removing the nodes from the tree 
				// when the dataentity is destroyed
				this->ConnectToOnDestroySignal(dataEntity);

				// Notify that the tree has been modified (tree browser or MultiRenderWindow)
				this->Modified();
			}
		}
		else
		{
			// Build rendering data
			Core::RenDataFactory::Pointer builder = Core::RenDataFactory::New( );
			builder->SetInputDataEntity( 0, dataEntity );
			builder->Update();

			// Update rendering node with the new rendering data
			mitk::BaseData::Pointer mitkData;
			mitkData = Core::RenDataFactory::GetBaseRenderingData( dataEntity );
			node->SetData( mitkData );

			node->SetVisibility( bShow );

			// Notify that the tree has been modified (tree browser or MultiRenderWindow)
			this->Modified();

			//mitk::RenderingManager::GetInstance()->RequestUpdateAll();
		}

	}
	coreCatchExceptionsAddTraceAndThrowMacro(RenderingTreeMITK::Add)

	return node;
}

void Core::RenderingTreeMITK::RemoveAll()
{
	m_DataStorage->Remove( m_DataStorage->GetAll() );
}

/**
Removes the given rendering node. 
This function will kill the rendering node and all its subtree, and then notify its observers

\param the rendering node to be removed.
*/
bool RenderingTreeMITK::Remove(mitk::DataTreeNode::Pointer node, bool bInitializeViews /*= true*/)
{
	if ( node.IsNull() )
	{
		throw Core::Exceptions::Exception( "RenderingTreeMITK::Remove", 
			"The requested rendering node cannot be NULL, so as to removing it from the RenderingTreeMITK" );
	}

	// Use raw pointer to avoid increasing reference count
	Core::DataEntity* nodeDataEntity = NULL;

	bool wasRemoved = false;
	try
	{
		nodeDataEntity = GetDataEntity( node );

		if(nodeDataEntity != NULL)
		{
			this->DisconnectOnDestroyedSignal(nodeDataEntity->GetId());
			this->DisconnectOnModifiedSignal(nodeDataEntity);
			this->DisconnectToOnModifiedAnyNodeSignal(nodeDataEntity);
		}
		
		m_DataStorage->Remove( node );

		wasRemoved = true;

		this->Modified();
			
	}
	coreCatchExceptionsAddTraceAndThrowMacro(RenderingTreeMITK::Remove)
	return wasRemoved;
}

/**
Removes all the rendering nodes mapped to the data entity, given just its id. If there were removed successfully,
it also casts a signal so observers could notice the event. This function will
kill the implied rendering nodes and all its subtrees.

\param dataEntityId is the id of the DataEntity to be removed
\sa DataEntity
*/
bool RenderingTreeMITK::Remove(unsigned int dataEntityId, bool bInitializeViews /*= true*/)
{			
	bool removedAny = false;

	try
	{
		mitk::NodePredicateProperty::Pointer predicate;
		predicate = mitk::NodePredicateProperty::New(
			"dataEntityID",
			mitk::IntProperty::New(dataEntityId));

		mitk::DataTreeNode::Pointer node;
		node = m_DataStorage->GetNode( predicate );
		if ( node.IsNotNull() )
		{
			removedAny = Remove( node, bInitializeViews );
		}
	}
	coreCatchExceptionsAddTraceAndThrowMacro(RenderingTreeMITK::Remove)

	return removedAny;
}

/**
Returns true if the given node is being held by this RenderingTreeMITK
*/
bool RenderingTreeMITK::HasNode(mitk::DataTreeNode::Pointer node)
{
	return m_DataStorage->Exists( node );
}

/**
Returns the root rendering node of this RenderingTreeMITK
*/
mitk::DataTreeNode::Pointer RenderingTreeMITK::GetRootNode(void)
{
	mitk::NodePredicateProperty::Pointer predicate;
	predicate = mitk::NodePredicateProperty::New(
		"name",
		mitk::StringProperty::New("<root>"));

	mitk::DataTreeNode::Pointer node;
	node = m_DataStorage->GetNode( predicate );

	return node;
}

/**
 */
mitk::DataStorage::Pointer Core::RenderingTreeMITK::GetDataStorage() const
{
	return m_DataStorage;
}

long Core::RenderingTreeMITK::GetNumberOfDataEntities()
{
	mitk::DataStorage::SetOfObjects::ConstPointer set;
	set = m_DataStorage->GetSubset( mitk::NodePredicateProperty::New("dataEntity") );
	return long( set->size( ) );
}

/**
Creates and returns a rendering tree node with the default rendering properties and links to the given data entity.
The properties are assigned differently depending on the data entity type.
*/
mitk::DataTreeNode::Pointer RenderingTreeMITK::CreateRenderingNodeWithDefaultProperties(
	Core::DataEntity::Pointer dataEntity)
{
	mitk::DataTreeNode::Pointer newNode;
	try
	{
		// This is the tree to work with, so attach here the DataEntity
		newNode = mitk::DataTreeNode::New();

		// We need to set the window level of the ROI mask image to a default fixed
		// value. Otherwise, it is set to Auto and then, when we call 
		// SetData( ), the default properties of mitk::ImageMapper2D::SetDefaultProperties()
		// will set it to Auto and reallocate the buffer to be continuous
		// using the Channel buffer data and the Volume pointint to this buffer
		if ( dataEntity->IsROI() )
		{
			newNode->SetProperty( "levelwindow", mitk::LevelWindowProperty::New( mitk::LevelWindow(8.0, 16) ) );
			newNode->AddProperty( "dontBinarize", mitk::BoolProperty::New( true), NULL, true ) ;
		}

		// Build rendering data
		Core::RenDataFactory::Pointer factory = Core::RenDataFactory::New( );
		factory->SetInputDataEntity( 0, dataEntity );
		factory->Update();

		mitk::BaseData::Pointer mitkData;
		mitkData = Core::RenDataFactory::GetBaseRenderingData( dataEntity );
		if ( mitkData.IsNull() )
		{
			return NULL;
		}

		// link the node with the DataEntity
		std::ostringstream name;
		name << Core::DataEntityInfoHelper::GetShortDescription(dataEntity);
		newNode->SetProperty("name", mitk::StringProperty::New(name.str()));
		newNode->SetVisibility( true );
		newNode->SetProperty("dataType", mitk::IntProperty::New( dataEntity->GetType() ) );
		newNode->SetProperty("dataEntityID", mitk::IntProperty::New( dataEntity->GetId() ) );

		// assign the dataEntity as a property, so as to to help find it later
		newNode->SetProperty("dataEntity", 
			Core::DataEntityProperty::New(dataEntity.GetPointer()));

		// add a pointer to the node because some functions need to access
		// it before adding to the MITK data storage
		blTagMap::Pointer rendering = dataEntity->GetMetadata()->GetTagValue<blTagMap::Pointer>( "Rendering" );
		if ( rendering.IsNull() )
		{
			rendering = blTagMap::New( );
			dataEntity->GetMetadata()->AddTag( "Rendering", rendering );
		}
		
		// mitk::LevelWindow should be configured before setting the data
		// because SetAuto is called and it takes some time to compute the window level
		blTagMap::Pointer renderingTags;
		renderingTags = dataEntity->GetMetadata( )->GetTagValue<blTagMap::Pointer>( "Rendering" );
		if ( renderingTags.IsNotNull() )
		{
			blTag::Pointer levelWindowTag = renderingTags->GetTag( "levelwindow" );
			MitkPropertyConverter::Pointer converter = MitkPropertyConverter::New( );
			converter->SetTag( levelWindowTag );
			converter->SetConvertToMITK( );
			converter->Update();
			mitk::BaseProperty::Pointer newProperty = converter->GetMitkProperty();
			if ( newProperty.IsNotNull() )
			{
				if ( newNode->GetProperty( levelWindowTag->GetName().c_str() ) )
					newNode->ReplaceProperty( levelWindowTag->GetName().c_str(), newProperty);
				else
					newNode->SetProperty( levelWindowTag->GetName().c_str(), newProperty);
			}
		}

		// Set data and set default properties for each mapper
		newNode->SetData( mitkData );

		// Apply default properties. This function should be called
		// after SetData( ) because LUT functions need the data
		Core::RenDataFactory::SetNodeDefaultProperties( newNode );

	}
	coreCatchExceptionsAddTraceAndThrowMacro(RenderingTreeMITK::CreateRenderingNodeWithDefaultProperties);
	return newNode;
}

void Core::RenderingTreeMITK::Show( 
	DataEntity::Pointer dataEntity, bool bShow, bool initializeViews /*= true*/ )
{
	try
	{
		mitk::DataTreeNode::Pointer node;
		Core::CastAnyProcessingData( GetNode( dataEntity ), node );

		// Change the property
		if ( node.IsNull() )
		{
			return;
		}

		// If node property has not changed -> do anything
		bool visibility;
		node->GetVisibility( visibility, NULL );
		if ( visibility == bShow )
			return;

		node->SetVisibility( bShow );

		if ( initializeViews )
			Modified();
	}
	coreCatchExceptionsAddTraceAndThrowMacro(RenderingTreeMITK::Show)
}

boost::any Core::RenderingTreeMITK::GetNode( 
	Core::DataEntity* dataEntity )
{
	if ( dataEntity == NULL )
	{
		return NULL;
	}

	// Search in data storage
	mitk::NodePredicateProperty::Pointer predicate;
	predicate = mitk::NodePredicateProperty::New(
		"dataEntity", 
		Core::DataEntityProperty::New(dataEntity) );

	mitk::DataTreeNode::Pointer node;
	node = m_DataStorage->GetNode( predicate );

	return node;
}

boost::any Core::RenderingTreeMITK::GetNode( 
	unsigned int dataEntityId )
{
	mitk::NodePredicateProperty::Pointer predicate;
	predicate = mitk::NodePredicateProperty::New(
		"dataEntityID",
		mitk::IntProperty::New(dataEntityId));

	mitk::DataTreeNode::Pointer node;
	node = m_DataStorage->GetNode( predicate );
	return node;
}

/**
*/
Core::DataEntity* 
Core::RenderingTreeMITK::GetDataEntity( mitk::DataTreeNode::Pointer node )
{
	if ( node.IsNull( ) )
	{
		return NULL;
	}

	Core::DataEntityProperty::Pointer dataEntityProp = NULL;
	dataEntityProp = static_cast<Core::DataEntityProperty*>
		( node->GetProperty("dataEntity") );

	if ( dataEntityProp.IsNull() )
	{
		return NULL;
	}

	Core::DataEntity* dataEntity = NULL;
	dataEntity = static_cast<Core::DataEntity*>
		( dataEntityProp->GetValue() );

	return dataEntity;
}

bool Core::RenderingTreeMITK::IsDataEntityRendered(Core::DataEntity* dataEntity)
{
	if( dataEntity == NULL )
		return false;

	mitk::DataTreeNode::Pointer node;
	Core::CastAnyProcessingData( GetNode( dataEntity ), node );
	if (node.IsNull())
		return false;

	return true;
}

bool Core::RenderingTreeMITK::IsDataEntityShown( Core::DataEntity* dataEntity )
{
	if( dataEntity == NULL )
		return false;

	mitk::DataTreeNode::Pointer node;
	Core::CastAnyProcessingData( GetNode( dataEntity ), node );
	if (node.IsNull())
		return false;

	bool isVisible;
	node->GetPropertyValue("visible", isVisible);
	return isVisible;
}

mitk::DataStorage::SetOfObjects::ConstPointer Core::RenderingTreeMITK::GetAllDataEntityNodes()
{
	mitk::DataStorage::SetOfObjects::ConstPointer set;
	set = m_DataStorage->GetSubset( mitk::NodePredicateProperty::New("dataEntity") );
	return set;
}

std::vector<DataEntity::Pointer> Core::RenderingTreeMITK::GetAllDataEntities()
{
	std::vector<DataEntity::Pointer> vector;

	mitk::DataStorage::SetOfObjects::ConstPointer set;
	set = GetAllDataEntityNodes();
	mitk::DataStorage::SetOfObjects::ConstIterator it;
	for ( it = set->Begin( ) ; it != set->End() ; it++ )
	{
		Core::DataEntity::Pointer currentDataEntity;
		currentDataEntity = GetDataEntity( it->Value() );
		if ( currentDataEntity.IsNotNull() )
		{
			vector.push_back( currentDataEntity );
		}
	}

	return vector;
}

void Core::RenderingTreeMITK::OnModifiedDataEntity( Core::SmartPointerObject* object )
{
	Core::DataEntity::Pointer dataEntity;
	dataEntity = dynamic_cast<Core::DataEntity*> ( object );
	if ( dataEntity.IsNull() )
	{
		return;
	}

	mitk::DataTreeNode::Pointer node;
	Core::CastAnyProcessingData( GetNode( dataEntity ), node );
	if ( node.IsNull() )
	{
		return;
	}

	// Build rendering data
	Core::RenDataFactory::Pointer builder = Core::RenDataFactory::New( );
	builder->SetInputDataEntity( 0, dataEntity );
	builder->Update();

	// Update rendering node with the new rendering data
	mitk::BaseData::Pointer mitkData;
	mitkData = Core::RenDataFactory::GetBaseRenderingData( dataEntity );
	
	// If mitkData is NULL -> Reset node data and avoid accessing a non valid memmory / NULL data
	if ( mitkData.IsNull() )
	{
		Remove( node );
		return;
	}

	node->SetData( mitkData );
	Core::DataTreeMITKHelper::UpdateRenderingNodeLabels( node );
	Core::DataTreeMITKHelper::ApplyLookupTableToNMImage( node );	
}

void Core::RenderingTreeMITK::UpdateMetadata( Core::DataEntity::Pointer dataEntity )
{
	mitk::DataTreeNode::Pointer node;
	Core::CastAnyProcessingData( GetNode( dataEntity ), node );
	if ( node.IsNull() )
	{
		return;
	}

	blTagMap::Pointer renderingTags;
	renderingTags = dataEntity->GetMetadata()->GetTagValue<blTagMap::Pointer>( "Rendering" );
	if ( renderingTags.IsNull())
	{
		renderingTags = blTagMap::New( );
		dataEntity->GetMetadata( )->AddTag( "Rendering", renderingTags );
	}

	MitkPropertyConverter::Pointer converter = MitkPropertyConverter::New( );
	mitk::PropertyList* propList = node->GetPropertyList();
	const mitk::PropertyList::PropertyMap* map = propList->GetMap( );
	mitk::PropertyList::PropertyMap::const_iterator it;
	for ( it = map->begin() ; it != map->end() ; it++ )
	{
		const char* propertyKey = it->first.c_str();
		mitk::BaseProperty* prop = it->second.first;

		if ( propertyKey == "dataEntityID" ||
			 propertyKey == "dataType" )
		{
			continue;
		}

		converter->SetPropertyKey( propertyKey );
		converter->SetMitkProperty( prop );
		converter->SetConvertFromMITK( );
		converter->Update();
		blTag::Pointer tag = converter->GetTag();
		if ( tag.IsNotNull() )
		{
			renderingTags->AddTag( tag->GetName(), tag->GetValue() );
		}
	}
}

void Core::RenderingTreeMITK::UpdateRenderingProperties( Core::DataEntity::Pointer dataEntity )
{
	mitk::DataTreeNode::Pointer node;
	Core::CastAnyProcessingData( GetNode( dataEntity ), node );
	if ( node.IsNull() )
	{
		return;
	}

	blTagMap::Pointer renderingTags;
	renderingTags = dataEntity->GetMetadata( )->GetTagValue<blTagMap::Pointer>( "Rendering" );
	if ( renderingTags.IsNull() )
	{
		return;
	}

	blTagMap::ListIterator it;
	for ( it = renderingTags->ListBegin() ; it != renderingTags->ListEnd() ; it++)
	{
		std::string propertyKey = renderingTags->GetKey( it );

		if ( propertyKey == "dataEntityID" ||
			 propertyKey == "dataType" )
		{
			continue;
		}

		SetProperty( dataEntity, renderingTags->GetTag( it ) );
	}

	// The LUT property cannot be serialized
	blMITKUtils::UpdateLookupTableProperty( node );
}

void Core::RenderingTreeMITK::ConnectToOnModifiedAnyNodeSignal( Core::DataEntity::Pointer dataEntity )
{
	mitk::DataTreeNode::Pointer node;
	Core::CastAnyProcessingData( GetNode( dataEntity ), node );
	mitk::BoolProperty::Pointer visibleProperty;
	if ( node.IsNull() || node->GetPropertyList() == NULL || !node->GetProperty( visibleProperty, "visible" ) )
	{
		return;
	}

	if ( m_MapObserversNode.find( node ) == m_MapObserversNode.end( ) )
	{
		itk::SimpleMemberCommand<RenderingTreeMITK>::Pointer command;
		command = itk::SimpleMemberCommand<RenderingTreeMITK>::New();
		command->SetCallbackFunction(
			this,
			&RenderingTreeMITK::OnModifiedRenderingTreeNode );
		m_MapObserversNode[ node ] = visibleProperty->AddObserver(
			itk::ModifiedEvent(), 
			command);
	}
}

void Core::RenderingTreeMITK::DisconnectToOnModifiedAnyNodeSignal( Core::DataEntity* dataEntity )
{
	mitk::DataTreeNode::Pointer node;
	Core::CastAnyProcessingData( GetNode( dataEntity ), node );
	m_MapObserversNode.erase( node );
}

void Core::RenderingTreeMITK::OnModifiedRenderingTreeNode()
{
	
}

void Core::RenderingTreeMITK::SetProperty( 
	Core::DataEntity::Pointer dataEntity, 
	blTag::Pointer property )
{
	if ( dataEntity.IsNull() )
	{
		return;
	}

	blTagMap::Pointer renderingTags;
	renderingTags = dataEntity->GetMetadata( )->GetTagValue<blTagMap::Pointer>( "Rendering" );
	if ( renderingTags.IsNull() )
	{
		renderingTags = blTagMap::New( );
		dataEntity->GetMetadata( )->AddTag( "Rendering", renderingTags );
	}
	renderingTags->AddTag( property );

	mitk::DataTreeNode::Pointer node;
	Core::CastAnyProcessingData( GetNode( dataEntity ), node );
	if ( node.IsNull() )
	{
		return;
	}

	// Convert to MITK property
	MitkPropertyConverter::Pointer converter = MitkPropertyConverter::New( );
	converter->SetTag( property );
	converter->SetConvertToMITK();
	converter->SetNode( node );
	converter->Update();
	mitk::BaseProperty::Pointer newProperty = converter->GetMitkProperty();
	if ( newProperty.IsNull() )
	{
		return;
	}

	// Special case for color property and surface mesh color
	// If color property is set from code, it should be passed to diffuse color
	if ( property.IsNotNull() && 
		 property->GetName() == "color" &&
		 dataEntity->GetType() == Core::SurfaceMeshTypeId )
	{
		mitk::ColorProperty::Pointer colorProp;
		colorProp = dynamic_cast<mitk::ColorProperty*> ( newProperty.GetPointer() );

		mitk::MaterialProperty::Pointer material;
		material = static_cast<mitk::MaterialProperty*>(node->GetProperty("material"));

		if ( colorProp.IsNotNull() && material.IsNotNull() )
		{
			mitk::Color color = colorProp->GetColor();
			// Remove old property if any, to avoid the error "Trying to set existing property to a property with different type."
			node->GetPropertyList( )->DeleteProperty( "diffuse_color" );
			material->SetDiffuseColor( color );
		}
	}

	// Update property on the MITK node
	if ( node->GetProperty( property->GetName().c_str() ) )
		node->ReplaceProperty( property->GetName().c_str(), newProperty);
	else
		node->SetProperty( property->GetName().c_str(), newProperty);

	// The LUT property should be updated by ProcessorOutputObserver when setting blLookupTablesType property
	// The LUT property cannot be serialized
	if ( property.IsNotNull( ) && property->GetName() == "blLookupTablesType" )
	{
		blMITKUtils::UpdateLookupTableProperty( node );
	}
}

blTag::Pointer Core::RenderingTreeMITK::GetProperty( 
	Core::DataEntity::Pointer dataEntity, const std::string& name )
{
	mitk::DataTreeNode::Pointer node;
	Core::CastAnyProcessingData( GetNode( dataEntity ), node );
	if ( node.IsNull() )
	{
		return NULL;
	}

	mitk::PropertyList* propList = node->GetPropertyList();
	mitk::BaseProperty* prop = propList->GetProperty( name.c_str() );
	if ( !prop )
	{
		return NULL;
	}

	MitkPropertyConverter::Pointer converter = MitkPropertyConverter::New( );
	converter->SetMitkProperty( prop );
	converter->SetPropertyKey( name );
	converter->SetConvertFromMITK();
	converter->Update();
	return converter->GetTag();
}

