/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreMultiRenderWindowOverlay.h"
#include "coreMultiRenderWindowMITK.h"
#include "coreDataEntity.h"
#include "coreDataEntityInfoHelper.h"
#include "wxMitkRenderWindow.h"
#include "wxMitkSelectableGLWidget.h"
#include "vtkLinearTransform.h"
#include "coreDataEntityHelper.h"
#include "coreRenDataFactory.h"

#include "mitkLine.h"
#include "mitkImageTimeSelector.h"
#include "mitkImageAccessByItk.h"

Core::Widgets::MultiRenderWindowOverlay::MultiRenderWindowOverlay()
{
	m_MultiRenderWindow = NULL;
	m_PosStepperChangedCommand = itk::SimpleMemberCommand<MultiRenderWindowOverlay>::New();
	m_PosStepperChangedCommand->SetCallbackFunction(
		this, 
		&MultiRenderWindowOverlay::OnPosStepperChanged);

	m_TimeStepperChangedCommand = itk::SimpleMemberCommand<MultiRenderWindowOverlay>::New();
	m_TimeStepperChangedCommand->SetCallbackFunction(
		this, 
		&MultiRenderWindowOverlay::OnTimestepChanged);

	m_TimeStepperChangedCommandTagT = -1;
}

Core::Widgets::MultiRenderWindowOverlay::~MultiRenderWindowOverlay()
{
	if ( m_SelectedDataEntity.IsNotNull() )
	{
		m_SelectedDataEntity->RemoveObserver( 
			this, 
			&MultiRenderWindowOverlay::OnSelectedModified );
	}

	RemoveOberversToSteppers();
}

void Core::Widgets::MultiRenderWindowOverlay::Init( 
	Core::Widgets::MultiRenderWindowMITK* multiRenderWindow, 
	Core::DataEntityHolder::Pointer selectedDataEntity )
{
	if ( m_SelectedDataEntity.IsNotNull() )
	{
		m_SelectedDataEntity->RemoveObserver( 
			this, 
			&MultiRenderWindowOverlay::OnSelectedModified );
	}

	if ( m_MultiRenderWindow )
	{
		m_MultiRenderWindow->GetMetadataHolder()->RemoveObserver( 
			this,
			&MultiRenderWindowOverlay::RefreshOverlayText );
	}

	RemoveOberversToSteppers();

	m_MultiRenderWindow = multiRenderWindow;
	m_SelectedDataEntity = selectedDataEntity;

	m_SelectedDataEntity->AddObserver( 
		this, 
		&MultiRenderWindowOverlay::OnSelectedModified );

	m_MultiRenderWindow->GetMetadataHolder()->AddObserver( 
		this,
		&MultiRenderWindowOverlay::RefreshOverlayText );

	InitOberversToSteppers( );
}

void Core::Widgets::MultiRenderWindowOverlay::OnSelectedModified()
{
	RefreshOverlayText( );
	
	if(this->m_SelectedDataEntity->GetSubject().IsNotNull())
	{
		this->m_SelectedDataEntity->GetSubject()->GetMetadata()->AddObserverOnChangedTag<MultiRenderWindowOverlay>(
			this, 
			&MultiRenderWindowOverlay::OnChangedMetadata);
	}
			
}


void Core::Widgets::MultiRenderWindowOverlay::OnChangedMetadata (
	blTagMap* tagMap,
	const std::string &name)
{
	this->RefreshOverlayText( );
}


void Core::Widgets::MultiRenderWindowOverlay::OnPosStepperChanged()
{
	RefreshOverlayText( );
}

void Core::Widgets::MultiRenderWindowOverlay::OnTimestepChanged()
{
	RefreshOverlayText( );
}

void Core::Widgets::MultiRenderWindowOverlay::RefreshOverlayText()
{
	if ( m_MultiRenderWindow == NULL || 
		!m_MultiRenderWindow->/*IsEnabled()*/IsThisEnabled() ||
		!m_MultiRenderWindow->GetMetadata( )->GetTagValue<bool>( "CornerAnnotationsShown" ) )
	{
		return;
	}

	// Scan all widgets
	std::string patientInfo = GetPatientInfo();
	mitk::wxMitkMultiRenderWindow::WidgetListType widgets = m_MultiRenderWindow->GetWidgets();
	mitk::wxMitkMultiRenderWindow::WidgetListType::iterator it;
	for ( it = widgets.begin() ; it != widgets.end() ; it++ )
	{
		mitk::wxMitkSelectableGLWidget* widget = *it;

		widget->GetCornerAnnotation( )->SetUpLeftAnnotation( patientInfo.c_str() );

		std::stringstream sstreamUpRight;
		sstreamUpRight << GetSlicePos( widget ) << GetAxisPixelValue( );
		widget->GetCornerAnnotation( )->SetUpRightAnnotation( sstreamUpRight.str().c_str() );
	}

	mitk::RenderingManager::GetInstance( )->RequestUpdateAll( 
		mitk::RenderingManager::REQUEST_UPDATE_2DWINDOWS );

}

void Core::Widgets::MultiRenderWindowOverlay::InitOberversToSteppers()
{
	mitk::wxMitkMultiRenderWindow::WidgetListType widgets = m_MultiRenderWindow->GetWidgets();
	mitk::wxMitkMultiRenderWindow::WidgetListType::iterator it;
	for ( it = widgets.begin() ; it != widgets.end() ; it++ )
	{
		mitk::Stepper* stepper = (*it)->GetSliceNavigationController()->GetSlice();

		unsigned long tag = stepper->AddObserver(
			itk::ModifiedEvent(), 
			m_PosStepperChangedCommand);

		m_PosStepperChangedCommandTag[ (*it) ] = tag;
	}

	if ( GetTime() )
	{
		m_TimeStepperChangedCommandTagT = GetTime()->AddObserver(
			itk::ModifiedEvent(), 
			m_TimeStepperChangedCommand);
	}
}

mitk::Stepper* Core::Widgets::MultiRenderWindowOverlay::GetTime()
{
	if ( m_MultiRenderWindow->GetTimeNavigationController() == NULL )
	{
		return NULL;
	}
	return m_MultiRenderWindow->GetTimeNavigationController()->GetTime();
}

template <class T>
std::string to_string( const T& input )
{
  std::stringstream sstream;
  sstream << input;
  return sstream.str();
}

std::string to_string( const char& input )
{
  std::stringstream sstream;
  sstream << int(input);
  return sstream.str();
}

std::string to_string( const unsigned char& input )
{
  std::stringstream sstream;
  sstream << int(input);
  return sstream.str();
}

template < typename TPixel, unsigned int VImageDimension > 
void InternalCompute(
	itk::Image< TPixel, VImageDimension >* itkImage, 
	const itk::Index< 3 > &index,
	std::string &pixelValue )
{
	typename itk::Image< TPixel, VImageDimension >::IndexType pixelIndex;
	pixelIndex[ 0 ] = index[ 0 ];
	pixelIndex[ 1 ] = index[ 1 ];
	if(VImageDimension == 3)
		pixelIndex[ 2 ] = index[ 2 ];

	if ( itkImage->GetLargestPossibleRegion( ).IsInside( pixelIndex ) )
	{
		TPixel pixel = itkImage->GetPixel( pixelIndex );
		pixelValue = to_string( pixel );
	}
	else
	{
		pixelValue = "None";
	}

}

void Core::Widgets::MultiRenderWindowOverlay::GetPixelValue( 
	mitk::Image* image,
	mitk::Point3D axisCenter,
	unsigned int timeStep,
	std::string &pixelValue,
	mitk::Point3D &pt_units )
{
	if ( image == NULL )
	{
		return;
	}

	image->SetRequestedRegionToLargestPossibleRegion(); //@todo without this, Image::GetScalarMin does not work for dim==3 (including sliceselector!)
	image->Update();
	mitk::ImageTimeSelector::Pointer timeSelector=mitk::ImageTimeSelector::New();
	timeSelector->SetInput( image );
	timeSelector->SetTimeNr( timeStep );
	timeSelector->UpdateLargestPossibleRegion();

	// Transform from world to index using MITK,
	// There's a bug in MITK that has been solved in 2010. However
	// this bug requires to change 10 classes
	image->GetGeometry( timeStep )->WorldToIndex( axisCenter, pt_units );

	// Convert to integer index values
	itk::Index<3> index;
	for(int i=0;i<3;++i){
		index[i]=(int)(floor(pt_units[i] ));
	}

	// Get value of pixel
	AccessByItk_2( timeSelector->GetOutput() , InternalCompute, index, pixelValue );
}

void Core::Widgets::MultiRenderWindowOverlay::RemoveOberversToSteppers()
{

	if ( m_MultiRenderWindow != NULL )
	{
		mitk::wxMitkMultiRenderWindow::WidgetListType widgets = m_MultiRenderWindow->GetWidgets();
		mitk::wxMitkMultiRenderWindow::WidgetListType::iterator it;
		for ( it = widgets.begin() ; it != widgets.end() ; it++ )
		{
			// Check if observer for this widget is present
			TagMapType::iterator itTag = m_PosStepperChangedCommandTag.find( (*it) );
			if ( itTag != m_PosStepperChangedCommandTag.end() )
			{
				unsigned long tag = itTag->second;

				mitk::Stepper* stepper = (*it)->GetSliceNavigationController()->GetSlice();
				stepper->RemoveObserver( tag );
			}
		}

		m_PosStepperChangedCommandTag.clear();

		if ( GetTime() && m_TimeStepperChangedCommandTagT != -1 )
		{
			GetTime()->RemoveObserver(m_TimeStepperChangedCommandTagT);
		}
	}

}

std::string Core::Widgets::MultiRenderWindowOverlay::GetPatientInfo()
{
	if ( m_SelectedDataEntity.IsNull() ||
		m_SelectedDataEntity->GetSubject().IsNull( ) )
	{
		return "";
	}

	Core::DataEntity::Pointer dataEntity;
	dataEntity = m_SelectedDataEntity->GetSubject();

	// Recollect information
	Core::DataEntityMetadata::Pointer meta = dataEntity->GetMetadata();
	Core::ModalityType modalityType;

	blTagMap::Pointer patient = meta->GetTagValue<blTagMap::Pointer>( "Patient" );

	std::stringstream sstream;
	if ( patient.IsNotNull( ) )
	{
		std::string patientName;
		if ( patient->FindTagByName( "Patient name" ).IsNotNull() )
		{
			patient->FindTagByName( "Patient name" )->GetValue<std::string>(patientName);
			if ( !patientName.empty() )
			{
				sstream << "Patient name: " << patientName << std::endl;
			}
		}

		std::string patientSex;
		if ( patient->FindTagByName( "Patient sex" ).IsNotNull() )
		{
			patient->FindTagByName( "Patient sex" )->GetValue<std::string>(patientSex);
			if ( !patientSex.empty() )
			{
				sstream << "Patient sex: " << patientSex << std::endl;
			}
		}

		std::string patientBirthDate;
		if ( patient->FindTagByName( "Patient birth date" ).IsNotNull() )
		{
			patient->FindTagByName( "Patient birth date" )->GetValue<std::string>(patientBirthDate);
			if ( !patientBirthDate.empty() )
			{
				sstream << "Patient birth date: " << patientBirthDate << std::endl;
			}
		}
	}

	if ( meta->FindTagByName( "Modality" ).IsNotNull() )
	{
		meta->FindTagByName( "Modality" )->GetValue<Core::ModalityType>(modalityType);
		if ( modalityType != Core::UnknownModality && 
			 !Core::DataEntityInfoHelper::GetModalityTypeAsString( modalityType ).empty() )
		{
			sstream << "Modality: " << Core::DataEntityInfoHelper::GetModalityTypeAsString( modalityType );
		}
	}

	return sstream.str();
}

std::string Core::Widgets::MultiRenderWindowOverlay::GetAxisPixelValue()
{
	if ( m_MultiRenderWindow == NULL )
	{
		return "";
	}

	//find the center in physical coordinates
	mitk::Point3D axisCenter;
	bool bAxisCenterComputed = m_MultiRenderWindow->GetIntersectionPoint(axisCenter);

	//now take the image as itk (or vtk) and get the pixel value
	std::stringstream sstream;
	if ( bAxisCenterComputed && 
		 !m_SelectedDataEntity->GetSubject().IsNull() && 
		 m_SelectedDataEntity->GetSubject()->IsImage() )
	{
		mitk::BaseData::Pointer mitkData;
		mitkData = Core::RenDataFactory::GetBaseRenderingData( m_SelectedDataEntity->GetSubject() );
		mitk::Image* image;
		image = dynamic_cast<mitk::Image*>(mitkData.GetPointer()); 

		std::string pixelValue;
		mitk::Point3D pt_units;
		GetPixelValue( 
			image, 
			axisCenter,
			m_MultiRenderWindow->Get3D()->GetMitkRenderWindow( )->GetRenderer()->GetTimeStep(),
			pixelValue,
			pt_units );
		sstream << "Intensity: " << pixelValue << std::endl;
	}

	return sstream.str();
}

std::string Core::Widgets::MultiRenderWindowOverlay::GetSlicePos( 
	mitk::wxMitkSelectableGLWidget* widget )
{
	int pos = widget->GetSliceNavigationController( )->GetSlice()->GetPos();
	std::stringstream sstream;
	sstream << "Slice: " << pos << std::endl;
	return sstream.str();
}

