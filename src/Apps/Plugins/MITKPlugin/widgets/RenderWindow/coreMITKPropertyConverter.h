/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#ifndef coreMITKPropertyConverter_H
#define coreMITKPropertyConverter_H

#include "coreObject.h"
#include "blTag.h"
#include "mitkBaseProperty.h"

namespace Core
{
/**
\brief Converter from mitk::BaseProperty to blTag::Pointer and viceversa

Valid rendering properties for metadata that will be converted to MITK properties are:
- any property of type bool
- any property of type int
- any property of type float
- any property of type double
- string:
  - With the name "color": is a string like "0.3,0.6,0.7"
  - any property of type double
- blTagMap::Pointer
  - With the name "levelwindow", all this child tags should be of type double:
    - "center"
	- "width"
	- "lowerWindowBound"
	- "upperWindowBound"
	- "rangeMin"
	- "rangeMax"
	- "defaultRangeMin"
	- "defaultRangeMax"
  - With the name "TransferFunction"
    - "ScalarOpacityPoints"
	- "GradientOpacityPoints"
    - "RGBPoints"
    - "Min"
	- "Max"
  - "material"

\ingroup MITKPlugin
\author Xavi Planes
\date Sept 2011
*/
class PLUGIN_EXPORT MitkPropertyConverter : public Core::SmartPointerObject
{
public:
	coreDeclareSmartPointerClassMacro(Core::MitkPropertyConverter, Core::SmartPointerObject);
	
	//!
	blTag::Pointer GetTag() const;
	void SetTag(blTag::Pointer val);

	//!
	mitk::BaseProperty::Pointer GetMitkProperty() const;
	void SetMitkProperty(mitk::BaseProperty* val);

	//!
	std::string GetPropertyKey() const;
	void SetPropertyKey(std::string val);

	//! Convert from MITK to blTag or viceversa
	void SetConvertFromMITK();
	void SetConvertToMITK();

	//!
	void SetNode( mitk::DataTreeNode::Pointer node );

	//!
	void Update( );

protected:

	//!
	MitkPropertyConverter(void);
	virtual ~MitkPropertyConverter(void);

	//!
	void ConvertToMITK( );

	//!
	void ConvertFromMITK( );

	//!
	std::string MitkColorToString(  );
private:
	//!
	blTag::Pointer m_Tag;
	//!
	mitk::BaseProperty::Pointer m_MitkProperty;
	//!
	std::string m_PropertyKey;
	//!
	bool m_FromMITKToTag;
	//!
	mitk::DataTreeNode::Pointer m_Node;
};

} // namespace Core

#endif

