/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreLevelWindowManager.h"
#include "coreRenderingTreeMITK.h"

Core::LevelWindowManager::LevelWindowManager()
{
	m_RenderingTree = NULL;
}

Core::LevelWindowManager::~LevelWindowManager()
{
	SetRenderingTree( NULL );
}

void Core::LevelWindowManager::SetRenderingTree( Core::RenderingTreeMITK* renderingTree )
{
	if ( m_RenderingTree )
	{
		m_RenderingTree->RemoveObserverOnModified( this, &LevelWindowManager::OnModifiedRenderingTree );
	}

	m_RenderingTree = renderingTree;

	if ( m_RenderingTree )
	{
		m_RenderingTree->AddObserverOnModified( this, &LevelWindowManager::OnModifiedRenderingTree );
	}

}

void Core::LevelWindowManager::OnModifiedRenderingTree( )
{
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();

	std::string shareLevelWindowStr;
	settings->GetPluginProperty( "GIMIAS", "ShareLevelWindow", shareLevelWindowStr );
	bool shareLevelWindow = shareLevelWindowStr == "true" ? true : false;
	if ( !shareLevelWindow )
	{
		return;
	}

	// Get all nodes
	mitk::DataStorage::SetOfObjects::ConstPointer nodes;
	nodes = m_RenderingTree->GetAllDataEntityNodes();

	// Get affected nodes
	std::list<mitk::LevelWindowProperty::Pointer> levelWindows;
	mitk::DataStorage::SetOfObjects::ConstIterator it;
	for ( it = nodes->Begin( ) ; it != nodes->End() ; it++ )
	{
		mitk::DataTreeNode::Pointer node = it->Value();
		if (node.IsNull())
		  continue;

		bool segmentation;
		bool propFound = node->GetBoolProperty( "segmentation", segmentation );
		if ( propFound && segmentation )
		  continue;

		mitk::LevelWindowProperty::Pointer levelWindowProperty;
		levelWindowProperty = dynamic_cast<mitk::LevelWindowProperty*>(node->GetProperty("levelwindow"));
		if (levelWindowProperty.IsNull())
		  continue;

		levelWindows.push_back( levelWindowProperty );
	}

	// Compute min/max global range
	mitk::ScalarType rangeMin = FLT_MAX;
	mitk::ScalarType rangeMax = FLT_MIN;
	std::list<mitk::LevelWindowProperty::Pointer>::iterator itLevel;
	for ( itLevel = levelWindows.begin( ) ; itLevel != levelWindows.end( ) ; itLevel++ )
	{
		mitk::LevelWindow levelWindow = (*itLevel)->GetLevelWindow( );

		// Update min/max
		if( levelWindow.GetRangeMin( ) < rangeMin )
		{
			rangeMin = levelWindow.GetRangeMin( );
		}
		if( levelWindow.GetRangeMax( ) > rangeMax )
		{
			rangeMax = levelWindow.GetRangeMax( );
		}
	}

	// Update range min/max
	for ( itLevel = levelWindows.begin( ) ; itLevel != levelWindows.end( ) ; itLevel++ )
	{
		mitk::LevelWindow levelWindow = (*itLevel)->GetLevelWindow( );
		levelWindow.SetRangeMinMax( rangeMin, rangeMax );
		(*itLevel)->SetLevelWindow( levelWindow );
	}
}
