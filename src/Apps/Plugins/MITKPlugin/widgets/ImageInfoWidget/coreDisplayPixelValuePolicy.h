/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef DISPLAYPIXELVALUEPOLICY_H
#define DISPLAYPIXELVALUEPOLICY_H

#include "corePointTrackingInteractorWithPolicy.h"
#include "coreImageInformation.h"
#include "CILabNamespaceMacros.h"
#include "mitkImage.h"

namespace Core{

/**
\brief This is a concrete implementation of PointTrackingInteractorPolicy
that responds to the event of a new selected point by
displaying the pixel intensity in the view.

\ingroup MITKPlugin
\author Maarten Nieber & Martin Bianculli
\date 28 feb 2008
*/

class DisplayPixelValuePolicy : public PointTrackingInteractorPolicy
{
public:
	//Type of the information of the image
	typedef Core::DataHolder< Core::ImageInformation::Pointer > 
		ImageInformationHolder;

	coreDeclareSmartPointerClassMacro(
		Core::DisplayPixelValuePolicy, 
		PointTrackingInteractorPolicy);	

	//! Sets the itk Image Pointer 
	void SetImage(mitk::Image::Pointer mitkImage);

	/**
	Sets the image information holder.
	\ param _imageInformationHolder - May not be NULL (or an assertion 
	is raised). If the holder has no
	subject, then a subject will be assigned to the holder automatically 
	using _imageInformationHolder->SetSubject.
	*/
	void SetInformationHolder(
		ImageInformationHolder::Pointer imageInformationHolder);

	/** Respond to the event that a new point has been selected in 
	PointTrackingInteractorWithPolicy.
	*/
	virtual void OnNewPoint(const Superclass::PointType& worldCoordinate, int timeStep);

protected:
	//! Default Constructor
	DisplayPixelValuePolicy(void);

	//! The volume Image
	mitk::Image::Pointer image;
	//! The image information holder
	ImageInformationHolder::Pointer m_ImageInformationHolder;
	
	coreDeclareNoCopyConstructors(DisplayPixelValuePolicy);
};

} // Core

#endif //DISPLAYPIXELVALUEPOLICY_H
