/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreImageInformation_H
#define coreImageInformation_H

#include "coreObject.h"
#include "coreDataHolder.h"
#include <CILabNamespaceMacros.h>
#include <boost/shared_ptr.hpp>
#include <itkPoint.h>

namespace Core{

/**
\brief This class is used to store some information on an image, such as 
the current location of the mouse pointer, and the value
at the current mouse pointer location.

\ingroup MITKPlugin
\author Maarten Nieber
\date 25 feb 2008
*/

class ImageInformation : public Core::SmartPointerObject
{
public:
	coreDeclareSmartPointerClassMacro(
		Core::ImageInformation, 
		Core::SmartPointerObject);
	//! World coordinates
	itk::Point<double, 3> coordinate;
	//! Image coordinates
	itk::Point<double, 3> worldCoordinate;
	//! Pixel value
	double pixelValue;
	bool coordinateIsInsideImage;

public:
	//! Purposely private
	ImageInformation()
	{
		coordinateIsInsideImage = false;
	};

private:
	coreDeclareNoCopyConstructors(ImageInformation);
	
};


} // Core

#endif //coreImageInformation_H
