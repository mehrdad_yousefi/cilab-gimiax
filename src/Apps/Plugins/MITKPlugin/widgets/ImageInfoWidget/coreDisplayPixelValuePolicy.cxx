/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

/**
This is a concrete implementation of PointTrackingInteractorPolicy that 
responds to the event of a new selected point by
displaying the pixel intensity in the view.

\author Martin Bianculli
28 feb 2008
*/

#include "coreDisplayPixelValuePolicy.h"
#include <coreImageInfoWidget.h>
#include <coreReportExceptionMacros.h>
#include <CILabAssertMacros.h>
#include "itkImage.h"
#include "coreMultiRenderWindowOverlay.h"
using namespace Core;


DisplayPixelValuePolicy::DisplayPixelValuePolicy()
{
}

void DisplayPixelValuePolicy::SetImage( mitk::Image::Pointer image)
{
	this->image = image;
}

void DisplayPixelValuePolicy::SetInformationHolder( 
	ImageInformationHolder::Pointer imageInformationHolder) 
{
	cilabAssertMacro(imageInformationHolder && "Arg imageInformationHolder may not be NULL");
	this->m_ImageInformationHolder = imageInformationHolder;
	if( !this->m_ImageInformationHolder->GetSubject() )
	{
		this->m_ImageInformationHolder->SetSubject( 
			Core::ImageInformation::New() );
	}
}


void DisplayPixelValuePolicy::OnNewPoint(
	const Superclass::PointType& worldCoordinate, int timeStep)
{
	Core::ImageInformation::Pointer imageInformation;
	imageInformation = this->m_ImageInformationHolder->GetSubject();
	imageInformation->coordinateIsInsideImage = false;

	if ( this->image.IsNotNull( ) )
	{

		mitk::Point3D point = worldCoordinate;
		std::string pixelValue;
		mitk::Point3D pt_units;
		Core::Widgets::MultiRenderWindowOverlay::GetPixelValue( image, point, timeStep, pixelValue, pt_units );
		if ( !pixelValue.empty( ) && pixelValue != "None" )
		{
			imageInformation->coordinateIsInsideImage = true;
			imageInformation->pixelValue = atof( pixelValue.c_str( ) );
			imageInformation->coordinate[ 0 ] = pt_units[ 0 ];
			imageInformation->coordinate[ 1 ] = pt_units[ 1 ];
			imageInformation->coordinate[ 2 ] = pt_units[ 2 ];
			imageInformation->worldCoordinate[ 0 ] = point[ 0 ];
			imageInformation->worldCoordinate[ 1 ] = point[ 1 ];
			imageInformation->worldCoordinate[ 2 ] = point[ 2 ];
			
		}
	}

	this->m_ImageInformationHolder->NotifyObservers();
}
