/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreMITKPreview_H
#define coreMITKPreview_H

#include "gmDataHandlingWin32Header.h"
#include "coreDataEntityPreview.h"

namespace Core{

class DataEntity;

/** 
\brief Preview of a DataEntity using wxWidgets

\author Xavi Planes
\date Nov 2012
\ingroup gmDataHandling
*/
class PLUGIN_EXPORT MITKPreview : public Core::DataEntityPreview
{
public:
	coreDeclareSmartPointerClassMacro(Core::MITKPreview,Core::SmartPointerObject);
	coreDefineFactory( Core::MITKPreview );

public:
	//! Overwride
	virtual void Load( );

	//! Overwride
	virtual void Save( );

	//! Overwride
	virtual void SaveTempFile( );

	//! Overwride
	virtual boost::any GetImage( );

	//! Overwride
	virtual void SetImage( boost::any val );

	//! Overwride
	bool IsOk( );

	//! Overwride
	void Copy( DataEntityPreview::Pointer val );

protected:
	//!
	MITKPreview( );

	//!
	virtual ~MITKPreview();

private:
	//!
	wxImage m_Image;
};

} // end namespace Core


#endif // corePreview_H

