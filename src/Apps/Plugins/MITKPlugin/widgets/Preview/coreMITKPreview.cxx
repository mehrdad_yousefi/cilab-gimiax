/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreMITKPreview.h"
#include "itksys/SystemTools.hxx"

Core::MITKPreview::MITKPreview( )
{
}

Core::MITKPreview::~MITKPreview()
{
}

void Core::MITKPreview::Load( )
{
	if ( wxFileExists( GetFileName( ) ) )
	{
		m_Image.LoadFile( GetFileName( ) );
	}
}

void Core::MITKPreview::Save( )
{
	if ( !GetFileName( ).empty( ) )
	{
		if ( m_Image.IsOk( ) )
		{
			m_Image.SaveFile( GetFileName( ) );
		}
		else
		{
			itksys::SystemTools::RemoveFile( GetFileName( ).c_str( ) );
		}
	}
}

void Core::MITKPreview::SaveTempFile( )
{
	if ( m_Image.IsOk( ) )
	{
		m_Image.SaveFile( GetTmpFileName( ) );
	}
	else
	{
		itksys::SystemTools::RemoveFile( GetTmpFileName( ).c_str( ) );
	}
}

boost::any Core::MITKPreview::GetImage( )
{
	return m_Image;
}

void Core::MITKPreview::SetImage( boost::any val )
{
	if ( val.type( ) == typeid( wxImage ) )
	{
		m_Image = boost::any_cast<wxImage>( val );
	}
	else
	{
		m_Image = wxNullImage;
	}
}

bool Core::MITKPreview::IsOk( )
{
	return m_Image.IsOk( );
}


void Core::MITKPreview::Copy( DataEntityPreview::Pointer val )
{
	if ( val.IsNull( ) )
	{
		return;
	}

	wxImage image = boost::any_cast<wxImage>( val->GetImage( ) );
	m_Image = wxImage( image );
}
