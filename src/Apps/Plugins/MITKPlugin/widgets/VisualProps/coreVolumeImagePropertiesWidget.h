/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreVolumeImagePropertiesWidget_H
#define coreVolumeImagePropertiesWidget_H

#include "coreObject.h"
#include "coreDataHolder.h"
#include "wxMitkImagePropertiesWidget.h"
#include <mitkDataTreeNode.h>
#include "coreVisualProperties.h"
#include <wx/collpane.h>

namespace Core
{
namespace Widgets 
{ 
/** 
\brief This widget forms part of the Appearance Suitecase.
\ingroup MITKPlugin
\author Juan Antonio Moya
\date 07 Feb 2008
\sa wxMitkImagePropertiesWidget and wxMitkTransferFunctionWidget
*/
class PLUGIN_EXPORT VolumeImagePropertiesWidget 
	: public mitk::wxMitkImagePropertiesWidget, public VisualPropertiesBase
{
public:
	//!
	coreDefineBaseWindowFactory( Core::Widgets::VolumeImagePropertiesWidget );

	coreClassNameMacro(Core::Widgets::VolumeImagePropertiesWidget);

	VolumeImagePropertiesWidget(
		wxWindow* parent, 
		wxWindowID id, 
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = wxBORDER_NONE, 
		const wxString& name = wxT("VolumeImagePropertiesWidget"));
	virtual ~VolumeImagePropertiesWidget(void);

	void SetInputHolder(DataEntityHolder::Pointer _aInputHolder)
	{
		this->m_InputHolder = _aInputHolder;
		this->m_InputHolder->AddObserver(
			this, 
			&VolumeImagePropertiesWidget::OnInputHolderChangeSubject, 
			Core::DH_SUBJECT_MODIFIED_OR_NEW_SUBJECT);
	};
	DataEntityHolder::Pointer GetInputHolder(void) const 
	{ 
		return this->m_InputHolder; 
	};
	//!
	bool IsValidData( Core::DataEntity::Pointer dataEntity )
	{
		return dataEntity->IsImage();
	}

	//!
	void SetMultiRenderWindow(Core::Widgets::RenderWindowBase* val);

	//!
	void OnBtnImport(wxCommandEvent& event);

    wxDECLARE_EVENT_TABLE();

private:
	void OnInputHolderChangeSubject(void);
	DataEntityHolder::Pointer m_InputHolder;

	//!
	wxBitmapButton* m_btnImport;

};

}
}

#endif // VolumeImagePropertiesWidget_H
