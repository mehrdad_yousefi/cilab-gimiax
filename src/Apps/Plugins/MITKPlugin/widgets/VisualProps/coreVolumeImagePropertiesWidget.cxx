/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreVolumeImagePropertiesWidget.h"
#include "coreDataEntity.h"
#include "coreAssert.h"
#include "coreRenderingTreeMITK.h"
#include "coreMultiRenderWindowMITK.h"
#include "wxMitkTransferFunctionWidget.h"
#include "gmTransferFunctionIOBase.h"

#include "OpenFile.xpm"

#define wxID_BTN_IMPORT wxID( "wxID_BTN_IMPORT" )

using namespace Core::Widgets;
using namespace mitk;

BEGIN_EVENT_TABLE(VolumeImagePropertiesWidget, mitk::wxMitkImagePropertiesWidget)
	EVT_BUTTON				( wxID_BTN_IMPORT, VolumeImagePropertiesWidget::OnBtnImport )
END_EVENT_TABLE()

//!
VolumeImagePropertiesWidget::VolumeImagePropertiesWidget(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
: wxMitkImagePropertiesWidget(parent, id, pos, size, style, name)
{
	this->SetAutoLayout(true);

	// Load presets icons
	SetPresetsFolder( Core::Runtime::Kernel::GetApplicationSettings()->GetCoreResourceForFile( "Presets" ) );

	// Set user presets folder
	SetPresetsFolder( Core::Runtime::Kernel::GetApplicationSettings()->GetProjectHomePath( ) + "/Presets" );

	wxWindow *editorWindow = ::wxFindWindowByName( "Transfer Function Editor", GetTransferFunctionWidget( ) );
	wxCollapsiblePane* editorCollpasiblepane = dynamic_cast<wxCollapsiblePane*> ( editorWindow );
	if ( !editorCollpasiblepane || 
		editorCollpasiblepane->GetPane( ) == NULL || 
		editorCollpasiblepane->GetPane( )->GetSizer( ) == NULL )
		return;

	// Create presets management buttons
	wxImage image( openfile_xpm );
	m_btnImport = new wxBitmapButton(editorCollpasiblepane->GetPane( ), wxID_BTN_IMPORT, image.Rescale( 24, 24 ) );
	m_btnImport->SetToolTip( "Open Transfer Function" );

	// Manage layout
	wxSizer* buttonsSizer = new wxBoxSizer(wxHORIZONTAL);
	buttonsSizer->Add( m_btnImport, 0, wxALL, 5 );
	editorCollpasiblepane->GetPane( )->GetSizer( )->Insert( 0, buttonsSizer, 0, wxALIGN_LEFT);
}

//!
VolumeImagePropertiesWidget::~VolumeImagePropertiesWidget(void)
{
}


/** When the input changes, update the widget with the new data */
void VolumeImagePropertiesWidget::OnInputHolderChangeSubject(void)
{
	coreAssertMacro(this->m_InputHolder.IsNotNull() && "The holder may not be null! Have you initialized this widget and its holder properly?");
	Core::DataEntity::Pointer dataEntity = this->m_InputHolder->GetSubject();

	if( dataEntity.IsNotNull() && 
		dataEntity->IsImage() &&
		GetMultiRenderWindow() && 
		GetMultiRenderWindow()->GetRenderingTree().IsNotNull() )
	{
		DisableVolumeRendering( dataEntity->IsROI( ) );

		boost::any anyData = GetMultiRenderWindow()->GetRenderingTree()->GetNode( dataEntity );
		mitk::DataTreeNode::Pointer node;
		Core::CastAnyProcessingData( anyData, node );
		this->SetDataTreeNode(node.GetPointer());
	}
	else
		this->SetDataTreeNode(NULL);
}

void Core::Widgets::VolumeImagePropertiesWidget::SetMultiRenderWindow( 
	Core::Widgets::RenderWindowBase* val )
{
	Core::BaseWindow::SetMultiRenderWindow( val );

	mitk::wxMitkRenderWindow* renderWindow = NULL;
	MultiRenderWindowMITK* mitkRenderWindow;
	mitkRenderWindow = dynamic_cast<MultiRenderWindowMITK*> ( val );
	if ( mitkRenderWindow && mitkRenderWindow->Get3D( ) )
	{
		renderWindow = mitkRenderWindow->Get3D( )->GetMitkRenderWindow( );
	}

	SetMitkRenderWindow( renderWindow );
}

void Core::Widgets::VolumeImagePropertiesWidget::OnBtnImport(wxCommandEvent& event)
{
	// Show dialog to select file
	std::string dataPath;
	wxFileDialog openFileDialog(this, wxT("Import Transfer Function"), wxT(""), wxT(""), wxT(""), 
		wxFD_OPEN | wxFD_FILE_MUST_EXIST );
	if(openFileDialog.ShowModal() != wxID_OK)
	{
		return;
	}

	// Try to read the file using registered factories
	Core::FactoryManager::FactoryListType factories;
	factories = Core::FactoryManager::FindAll( gmTransferFunctionIOBase::GetNameClass() );
	Core::FactoryManager::FactoryListType::iterator itFactory = factories.begin( );
	mitk::TransferFunction::Pointer transferFunction;
	while ( itFactory != factories.end( ) )
	{
		Core::SmartPointerObject::Pointer object = (*itFactory)->CreateInstance();
		gmTransferFunctionIOBase::Pointer reader = dynamic_cast<gmTransferFunctionIOBase*> ( object.GetPointer( ) );
		reader->SetFilename( openFileDialog.GetPath() );
		reader->Update( );
		transferFunction = reader->GetOutput( );
		itFactory++;
	}
	if ( transferFunction.IsNull( ) )
		return;


	// Use the transfer function
	mitk::TransferFunctionProperty::Pointer transferFunctionProp;
	transferFunctionProp = mitk::TransferFunctionProperty::New( transferFunction );
	GetTransferFunctionWidget( )->SetVolumeRenderingCheck( true, false );
	GetTransferFunctionWidget( )->GetDataTreeNode( )->ReplaceProperty( "TransferFunction", transferFunctionProp.GetPointer() );
}
