/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreITKTransformVisualPropertiesWidget_H
#define coreITKTransformVisualPropertiesWidget_H

#include "coreObject.h"
#include "coreDataHolder.h"
#include <wxMitkTransformVisualPropWidget.h>
#include <mitkDataTreeNode.h>
#include "coreVisualProperties.h"

namespace Core
{
namespace Widgets 
{ 
/** 

\ingroup MITKPlugin
\author Xavi Planes
\date Nov 2010
*/
class PLUGIN_EXPORT ITKTransformVisualPropertiesWidget 
	: public mitk::wxMitkTransformVisualPropWidget, public VisualPropertiesBase
{
public:
	//!
	coreDefineBaseWindowFactory( Core::Widgets::ITKTransformVisualPropertiesWidget );

	coreClassNameMacro(Core::Widgets::ITKTransformVisualPropertiesWidget);

	ITKTransformVisualPropertiesWidget(
		wxWindow* parent, 
		wxWindowID id, 
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = wxBORDER_NONE, 
		const wxString& name = wxT("ITKTransformVisualPropertiesWidget"));
	virtual ~ITKTransformVisualPropertiesWidget(void);

	void SetInputHolder(DataEntityHolder::Pointer _aInputHolder)
	{
		this->m_InputHolder = _aInputHolder;
		this->m_InputHolder->AddObserver(
			this, 
			&ITKTransformVisualPropertiesWidget::OnInputHolderChangeSubject, 
			Core::DH_SUBJECT_MODIFIED_OR_NEW_SUBJECT);
	};
	DataEntityHolder::Pointer GetInputHolder(void) const
	{ 
		return this->m_InputHolder; 
	};
	//!
	bool IsValidData( Core::DataEntity::Pointer dataEntity )
	{
		return dataEntity->IsTransform();
	}
	//!
	void SetMultiRenderWindow(Core::Widgets::RenderWindowBase* val);

private:
	void OnInputHolderChangeSubject(void);
	DataEntityHolder::Pointer m_InputHolder;

};

}
}


#endif // ITKTransformVisualPropertiesWidget_H
