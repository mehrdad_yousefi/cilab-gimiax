/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreITKTensorRenDataBuilder.h"
#include "coreAssert.h"
#include <mitkITKImageImport.h>
#include <typeinfo>

#include "itkDTITensor.h"
#include "itkComputeFAFilter.h"
#include "itkImageToVTKImageFilter.h"

#include "vtkImageData.h"

using namespace Core;

//!
ITKTensorRenDataBuilder::ITKTensorRenDataBuilder(void)
{
}

//!
ITKTensorRenDataBuilder::~ITKTensorRenDataBuilder(void)
{
}

//!
void ITKTensorRenDataBuilder::Update( )
{
	// Get input
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );

	if(dataEntity->GetTimeStep( 0 )->GetDataPtr().empty())
	{
		return;
	}

	// Create an empty mitk::Image
	Core::DataEntity::Pointer renDataEntity = dataEntity->GetRenderingData( "MITK" );
	if ( renDataEntity.IsNull() )
	{
		renDataEntity = Core::DataEntity::New( Core::ImageTypeId );
		renDataEntity->SwitchImplementation( typeid( mitk::Image::Pointer ) );
	}

	// Get MITK pointer
	mitk::Image::Pointer mitk3DImage;
	renDataEntity->GetProcessingData( mitk3DImage );

	const std::type_info* typeID = &dataEntity->GetTimeStep( 0 )->GetDataPtr().type();
	
	if(*typeID == typeid(itk::Image<itk::DTITensor<float>,3>::Pointer))
	{
		GetMitkImageFromItkImage( dataEntity->GetTimeStep( 0 )->GetDataPtr(), mitk3DImage );
	}
	
	dataEntity->SetRenderingData( "MITK", renDataEntity );
}

void Core::ITKTensorRenDataBuilder::GetMitkImageFromItkImage( 
	AnyProcessingData processingData, mitk::Image::Pointer mitkImage )
{
	typedef itk::Image<itk::DTITensor<float>,3> TensorImageType;
	typedef TensorImageType::Pointer TensorImageTypePtr;
	typedef itk::Image<float,3> InputImageType;
	TensorImageTypePtr itkImage = NULL;
	CastAnyProcessingData<TensorImageTypePtr>(processingData, itkImage);

	if (itkImage)
	{

		typedef itk::ComputeFAFilter<TensorImageType,InputImageType> FAfilterType;
		FAfilterType::Pointer FAfilter = FAfilterType::New();
		FAfilter->SetInput(itkImage);
		FAfilter->Update();
		const InputImageType* inputImage;
		inputImage = FAfilter->GetOutput();

		vtkImageData* imageData = vtkImageData::New();
		typedef itk::ImageToVTKImageFilter<InputImageType> ToVtkImageFilter;
		ToVtkImageFilter::Pointer imageAdaptor = ToVtkImageFilter::New();
		imageAdaptor->SetInput( inputImage );
		imageAdaptor->Update();
		imageData->DeepCopy(imageAdaptor->GetOutput());
		
		// Reuse the processing data buffer
		mitkImage->Initialize(imageData);
		mitkImage->SetImportVolume(imageData->GetScalarPointer(),0, 0, mitk::Image::ReferenceMemory );
		mitk::Point3D origin;
		origin[0] = mitk::Point3D::ValueType( imageData->GetOrigin()[0] );
		origin[1] = mitk::Point3D::ValueType( imageData->GetOrigin()[1] );
		origin[2] = mitk::Point3D::ValueType( imageData->GetOrigin()[2] );
		mitkImage->GetGeometry( 0 )->SetOrigin(origin);
	}
	
}

mitk::BaseData::Pointer Core::ITKTensorRenDataBuilder::GetBaseData()
{
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );
	mitk::Image::Pointer data;
	dataEntity->GetProcessingData( data );
	return data.GetPointer();
}

void Core::ITKTensorRenDataBuilder::SetNodeDefaultProperties( mitk::DataTreeNode::Pointer newNode )
{

}
