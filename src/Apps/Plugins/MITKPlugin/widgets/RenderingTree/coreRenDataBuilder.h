/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreRenDataBuilder_H
#define coreRenDataBuilder_H

#include "corePluginMacros.h"
#include "coreBaseFilter.h"
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/any.hpp>

namespace Core
{

/** 
\brief This is a base class for the RenDataBuilder classes.
The RenDataBuilder is responsible of translating the processing 
representation of a DataEntity to its rendering representation.

\ingroup MITKPlugin
\author Juan Antonio Moya
\date 28 Mar 2008
*/
class PLUGIN_EXPORT RenDataBuilder : public Core::BaseFilter
{
public:

	coreDeclareSmartPointerTypesMacro( Core::RenDataBuilder, Core::BaseFilter )
	coreClassNameMacro( Core::RenDataBuilder )

	/**
	Provide a body for this function that builds the Rendering 
	representation of the \a processing data.
	If the builds fails, it should return a NULL pointer.
	*/
	virtual void Update( ) = 0;

	//!
	virtual mitk::BaseData::Pointer GetBaseData( ) = 0;

	//! Set node default properties
	virtual void SetNodeDefaultProperties( mitk::DataTreeNode::Pointer newNode ) = 0;

protected:
	//!
	RenDataBuilder(void);;
	
	//!
	virtual ~RenDataBuilder(void);;

protected:

	coreDeclareNoCopyConstructors(RenDataBuilder);

	//!
	boost::any m_RenderingData;
};

}

#endif
