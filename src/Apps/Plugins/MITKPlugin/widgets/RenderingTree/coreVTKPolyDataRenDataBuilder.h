/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreVTKPolyDataRenDataBuilder_H
#define coreVTKPolyDataRenDataBuilder_H

#include "corePluginMacros.h"
#include "coreRenDataBuilder.h"

namespace Core
{
/** 
\brief A specialization of the RenDataBuilder class for VTK PolyData objects.

\ingroup MITKPlugin
\sa RenDataBuilder
\author Juan Antonio Moya
\date 28 Mar 2008
*/
class PLUGIN_EXPORT VTKPolyDataRenDataBuilder : public RenDataBuilder
{
public:
	coreDeclareSmartPointerClassMacro( Core::VTKPolyDataRenDataBuilder, RenDataBuilder );

	//@{ 
	//! \name Overrides of parent implementation.
	void Update( );
	mitk::BaseData::Pointer GetBaseData( );
	void SetNodeDefaultProperties( mitk::DataTreeNode::Pointer newNode );
	//@}

protected:
	VTKPolyDataRenDataBuilder(void);

	//!
	virtual ~VTKPolyDataRenDataBuilder(void);

private:
	coreDeclareNoCopyConstructors(VTKPolyDataRenDataBuilder);

};

}

#endif
