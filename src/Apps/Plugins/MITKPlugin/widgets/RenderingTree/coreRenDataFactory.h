/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreRenDataFactory_H
#define _coreRenDataFactory_H

#include "corePluginMacros.h"
#include "coreBaseFilter.h"
#include "coreRenDataBuilder.h"

namespace Core
{

/** 
\brief This is a factory for the RenDataFactory classes.

RenDataFactory will check the type of DataEntity and will call the 
concrete implementation of RenDataBuilder. Currently the supported 
rendering representation is MITK. When the rendering data is created, it 
is passed as a boost::any pointer calling to the DataEntity::SetRenderingData( ).

RenderingData is created automatically when the DataEntity is added to 
the RenderingTree.

\ingroup MITKPlugin
\author Xavi Planes
\date 03 Nov 2009
*/
class PLUGIN_EXPORT RenDataFactory : public Core::BaseFilter
{
public:

	coreDeclareSmartPointerClassMacro( Core::RenDataFactory, Core::BaseFilter )

	/**
	Provide a body for this function that builds the Rendering 
	representation of the \a processing data.
	If the builds fails, it should return a NULL pointer.
	*/
	void Update( );

	//! Helper function to cast internal pointer to mitk::BaseData
	static mitk::BaseData::Pointer GetBaseData( DataEntity::Pointer renDataentity );

	//! Helper function to cast RenderingData pointer to mitk::BaseData
	static mitk::BaseData::Pointer GetBaseRenderingData( DataEntity::Pointer dataentity );

	//! Set node default properties
	static void SetNodeDefaultProperties( mitk::DataTreeNode::Pointer newNode );

protected:
	//!
	RenDataFactory(void);;
	
	//!
	virtual ~RenDataFactory(void);;

	//!
	static RenDataBuilder::Pointer CreateBuilder( Core::DataEntity::Pointer dataEntity );

protected:

	coreDeclareNoCopyConstructors(RenDataFactory);
};

}

#endif
