/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "corePointSetRenDataBuilder.h"
#include "blVTKHelperTools.h"
#include <vtkPolyData.h>
#include <mitkSurface.h>
#include <mitkPointSet.h>
#include "mitkPointOperation.h"
#include "mitkInteractionConst.h"

#include "coreVTKPolyDataHolder.h"
#include "coreDataEntity.h"
#include "coreRenderingTreeMITK.h"
#include "coreDataTreeMITKHelper.h"

using namespace Core;

//!
PointSetRenDataBuilder::PointSetRenDataBuilder(void)
{
}

//!
PointSetRenDataBuilder::~PointSetRenDataBuilder(void)
{
}

//!
void PointSetRenDataBuilder::Update()
{
	// Get input
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );

	// Get rendering data
	Core::DataEntity::Pointer renDataEntity = dataEntity->GetRenderingData( "MITK" );
	if ( renDataEntity.IsNull() )
	{
		renDataEntity = Core::DataEntity::New( Core::PointSetTypeId );
		renDataEntity->SwitchImplementation( typeid( mitk::PointSet::Pointer ) );
	}

	// Try to copy data ptr if the data type is the same
	// Otherwise, try to copy the data
	try
	{
		renDataEntity->CopyDataPtr( dataEntity );
	}
	catch (...)
	{
		renDataEntity->Copy( dataEntity, gmReferenceMemory );
	}

	dataEntity->SetRenderingData( "MITK", renDataEntity );
}

mitk::BaseData::Pointer Core::PointSetRenDataBuilder::GetBaseData()
{
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );
	mitk::PointSet::Pointer data;
	dataEntity->GetProcessingData( data );
	return data.GetPointer();
}

void Core::PointSetRenDataBuilder::SetNodeDefaultProperties( 
	mitk::DataTreeNode::Pointer newNode )
{
	Core::DataEntity::Pointer dataEntity = RenderingTreeMITK::GetDataEntity( newNode );
	if ( dataEntity.IsNull( ) )
	{
		return;
	}

	if ( dataEntity->IsMeasurement() )
	{

		newNode->SetProperty("show contour", mitk::BoolProperty::New(true));
		newNode->SetProperty("name", mitk::StringProperty::New("distance"));
		newNode->SetProperty("show distances",mitk::BoolProperty::New(true));

		//Additional properties for father images
		Core::DataEntity::Pointer father = dataEntity->GetFather();
		if ( father.IsNull() ||
			father->GetType() != Core::ImageTypeId)
		{
			Core::vtkPolyDataPtr polydata;
			if ( father->GetProcessingData( polydata ) )
			{
				double pointSize;
				if ( blMITKUtils::ComputePointSize( polydata, pointSize, 0.005 ) )
				{
					newNode->SetProperty("pointsize", mitk::FloatProperty::New(pointSize));	
					newNode->SetProperty("contoursize", mitk::FloatProperty::New(pointSize/2));
				}
			}

			return;
		}

		blTagMap::Pointer tagMap = blTagMap::New( );
		father->GetTimeStep( 0 )->GetData( tagMap );

		blTag::Pointer tag = tagMap->FindTagByName( "Spacing" );
		if ( tag.IsNull() ) return ;
		std::vector<double> spacing = tag->GetValueCasted< std::vector<double> >();

		//spacing in mm!
		double pointSize;
		pointSize  = *std::max_element( spacing.begin(), spacing.end() );
		newNode->ReplaceProperty("pointsize", mitk::FloatProperty::New( pointSize ) );
		newNode->ReplaceProperty("contoursize", mitk::FloatProperty::New(pointSize/2));

	}
	else
	{
		mitk::StringVectorProperty::Pointer labelVector;
		labelVector = mitk::StringVectorProperty::New( );
		newNode->SetProperty( "label", labelVector );
		newNode->SetProperty( "show label", mitk::BoolProperty::New( true ) );

		double size;
		size = ComputeSizeOfPoint(newNode);
		//resize points in the special case of a pointset child of a surface
		if (size > 0)
			newNode->SetProperty("pointsize", mitk::FloatProperty::New(size));	

		Core::DataTreeMITKHelper::UpdateRenderingNodeLabels( newNode );

	}

}

double Core::PointSetRenDataBuilder::ComputeSizeOfPoint( mitk::DataTreeNode::Pointer node)
{
	double pointSize = -1;
	blMITKUtils::ComputePointSize( node->GetData(), pointSize, 0.005 );

	//mitk::TimeSlicedGeometry::Pointer geometry;
	//geometry = m_DataStorage->ComputeVisibleBoundingGeometry3D( NULL, "includeInBoundingBox" );
	//if ( geometry.IsNotNull() )
	//{
	//	pointSize = std::max( pointSize, 0.005 * geometry->GetDiagonalLength() );
	//}

	return pointSize;
}
