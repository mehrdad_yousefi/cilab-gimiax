/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreVTKImageDataRenDataBuilder.h"
#include <vtkImageData.h>
#include <mitkImage.h>
#include "vtkArrayCalculator.h"

#include "coreDataEntity.h"
#include "coreVTKImageDataHolder.h"
#include "coreDataTreeMITKHelper.h"
#include "coreRenderingTreeMITK.h"

using namespace Core;

//!
VTKImageDataRenDataBuilder::VTKImageDataRenDataBuilder(void)
{
}

//!
VTKImageDataRenDataBuilder::~VTKImageDataRenDataBuilder(void)
{
}

void VTKImageDataRenDataBuilder::Update( )
{
	// Get input
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );

	// Create an empty mitk::Image
	Core::DataEntity::Pointer renDataEntity = dataEntity->GetRenderingData( "MITK" );
	if ( renDataEntity.IsNull() )
	{
		renDataEntity = Core::DataEntity::New( Core::ImageTypeId );
		renDataEntity->SwitchImplementation( typeid( mitk::Image::Pointer ) );
	}

	// Get MITK pointer
	mitk::Image::Pointer mitk3DImage;
	renDataEntity->GetProcessingData( mitk3DImage );

	// Get Number of components
	blTagMap::Pointer properties = blTagMap::New( );
	dataEntity->GetTimeStep( 0 )->GetData( properties );
	int numComponents = 1;
	if ( properties.IsNotNull() )
	{
		blTag::Pointer tag;
		tag = properties->FindTagByName( "NumberOfComponents" );
		if ( tag.IsNotNull() ) 
		{
			numComponents = tag->GetValueCasted<int>();
		}
	}

	if ( numComponents == 1)
	{
		// Copy the processing data to rendering data, referencing the memory
		renDataEntity->Copy( dataEntity, Core::gmReferenceMemory );
		dataEntity->SetRenderingData( "MITK", renDataEntity );
	}
	else if  ( numComponents >= 2)
	{
		// Get vtkImageData
		Core::vtkImageDataPtr vtkImage;
		Core::DataEntity::Pointer vtkDataEntity = Core::DataEntity::New( Core::ImageTypeId );
		vtkDataEntity->SwitchImplementation( typeid( Core::vtkImageDataPtr ) );
		vtkDataEntity->Copy( dataEntity, Core::gmReferenceMemory );

		renDataEntity->SwitchImplementation( typeid( Core::vtkImageDataPtr ) );
		renDataEntity->Resize( vtkDataEntity->GetNumberOfTimeSteps(), typeid( Core::vtkImageDataPtr ) );
		for ( TimeStepIndex i = 0 ; i < vtkDataEntity->GetNumberOfTimeSteps() ; i++ )
		{

			vtkDataEntity->GetProcessingData( vtkImage, i );

			// Compute magnitude
			vtkSmartPointer<vtkArrayCalculator> calc = vtkSmartPointer<vtkArrayCalculator>::New();
			calc->SetInput( vtkImage );
			calc->SetAttributeModeToUsePointData();

			// Create function ("sqrt(s_0*s_0 + s_1*s_1)")
			std::stringstream streamFunc;
			streamFunc << "sqrt(";
			for ( int comp = 0 ; comp < numComponents ; comp++ )
			{
				std::stringstream stream;
				stream << "s_" << comp;
				calc->AddScalarVariable(stream.str().c_str(),"scalars", comp);
				streamFunc << stream.str() << "*" << stream.str(); 
				if ( comp != numComponents - 1 )
				{
					streamFunc << " + ";
				}
			}
			streamFunc << ")";
			calc->SetFunction( streamFunc.str().c_str() );

			calc->SetResultArrayName("scalars");
			calc->SetResultArrayType( vtkImage->GetScalarType() );
			calc->Update();

			// We need to copy the data, otherwise this doesn't work!!!
			Core::vtkImageDataPtr vtkImageMagnitude = Core::vtkImageDataPtr::New();
			vtkImageMagnitude->DeepCopy(calc->GetOutput());
			renDataEntity->SetTimeStep( vtkImageMagnitude, i );
		}

		// Convert it to MITK
		renDataEntity->SwitchImplementation( typeid( mitk::Image::Pointer ) );
		dataEntity->SetRenderingData( "MITK", renDataEntity );
	}
}


mitk::BaseData::Pointer Core::VTKImageDataRenDataBuilder::GetBaseData()
{
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );
	mitk::Image::Pointer mitk3DImage;
	dataEntity->GetProcessingData( mitk3DImage );
	return mitk3DImage.GetPointer();
}

void Core::VTKImageDataRenDataBuilder::SetNodeDefaultProperties( 
	mitk::DataTreeNode::Pointer newNode )
{

	Core::DataEntity::Pointer dataEntity = RenderingTreeMITK::GetDataEntity( newNode );
	if ( dataEntity.IsNull( ) )
	{
		return;
	}

	if ( dataEntity->IsROI() )
	{
		blMITKUtils::ApplyMaskImageProperties(newNode);
	}
	else
	{

		newNode->SetProperty("volumerendering", mitk::BoolProperty::New(false));
		newNode->SetProperty( "opacity", mitk::FloatProperty::New(1.0f));
		newNode->SetProperty( "color", mitk::ColorProperty::New(1.0,1.0,1.0));
		newNode->SetProperty("use color", mitk::BoolProperty::New(false));
		newNode->SetProperty("texture interpolation", mitk::BoolProperty::New(true));
		newNode->SetProperty("reslice interpolation", 
			mitk::VtkResliceInterpolationProperty::New( VTK_RESLICE_NEAREST ) );
		newNode->SetProperty("layer", mitk::IntProperty::New(0));
		newNode->SetProperty("in plane resample extent by geometry", mitk::BoolProperty::New(false));

		// Default LUT
		blMITKUtils::ApplyLookupTable( 
			true, 
			newNode, 
			blLookupTables::LUT_TYPE_MITK_GRAY,
			blShapeUtils::ShapeUtils::VTKScalarType( ) );

		Core::DataTreeMITKHelper::ApplyLookupTableToNMImage(newNode);	
	}

}
