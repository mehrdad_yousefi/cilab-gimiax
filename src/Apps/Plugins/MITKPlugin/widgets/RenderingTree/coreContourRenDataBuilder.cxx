/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreContourRenDataBuilder.h"
#include "blVTKHelperTools.h"
#include <vtkPolyData.h>
#include <mitkSurface.h>
#include <mitkContour.h>
#include "coreVTKPolyDataHolder.h"
#include "coreDataEntity.h"

using namespace Core;

//!
ContourRenDataBuilder::ContourRenDataBuilder(void)
{
}

//!
ContourRenDataBuilder::~ContourRenDataBuilder(void)
{
}

//!
void ContourRenDataBuilder::Update( )
{
	// Get input
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );

	//! Check processing data
	if( !CheckProcessingDataIsOk( dataEntity->GetTimeStep( 0 ) ) )
	{
		return;
	}

	// Get rendering data
	Core::DataEntity::Pointer renDataEntity = dataEntity->GetRenderingData( "MITK" );
	if ( renDataEntity.IsNull() )
	{
		renDataEntity = Core::DataEntity::New( Core::ContourTypeId );
		renDataEntity->SwitchImplementation( typeid( mitk::Contour::Pointer ) );
	}

	mitk::Contour::Pointer mitkContour;
	renDataEntity->GetProcessingData( mitkContour );

	mitkContour->Expand( dataEntity->GetNumberOfTimeSteps() );

	// For all time steps
	for ( unsigned iTimeStep = 0 ; iTimeStep < dataEntity->GetNumberOfTimeSteps() ; iTimeStep++ )
	{
		// Be careful with this code. If you create a new rendering data
		// The observers to the data entity needs to update the node in the
		// rendering tree with SetData function each time this function is called
		vtkPolyDataPtr meshPtr;
		dataEntity->GetProcessingData( meshPtr, iTimeStep );

		// Add all the points
		vtkPoints *points = meshPtr->GetPoints( );
		if ( points != NULL )
		{
			for( int i = 0; i < points->GetNumberOfPoints(); i++ )
			{
				mitk::Point3D mitkPoint;
				mitkPoint[ 0 ] = mitk::ScalarType( points->GetPoint( i )[ 0 ] );
				mitkPoint[ 1 ] = mitk::ScalarType( points->GetPoint( i )[ 1 ] );
				mitkPoint[ 2 ] = mitk::ScalarType( points->GetPoint( i )[ 2 ] );
				mitkContour->AddVertex( mitkPoint );
			}
		}

	}

	dataEntity->SetRenderingData( "MITK", renDataEntity );
}

bool Core::ContourRenDataBuilder::CheckProcessingDataIsOk( 
	Core::DataEntityImpl::Pointer timeStep )
{
	vtkPolyDataPtr meshPtr;
	bool worked = Core::CastAnyProcessingData( timeStep->GetDataPtr(), meshPtr ); 
	return worked;
}

mitk::BaseData::Pointer Core::ContourRenDataBuilder::GetBaseData()
{
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );
	mitk::Contour::Pointer data;
	dataEntity->GetProcessingData( data );
	return data.GetPointer();
}

void Core::ContourRenDataBuilder::SetNodeDefaultProperties( 
	mitk::DataTreeNode::Pointer newNode )
{
	newNode->SetColor( 0, 1, 0 );
	newNode->SetOpacity( 1 );
	newNode->SetProperty( "Width", mitk::FloatProperty::New(3) );
}
