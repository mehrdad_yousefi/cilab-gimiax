/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreRenDataFactory.h"

#include "coreContourRenDataBuilder.h"
#include "corePointSetRenDataBuilder.h"
#include "coreVTKImageDataRenDataBuilder.h"
#include "coreVTKPolyDataRenDataBuilder.h"
#include "coreVTKUnstructuredGridRenDataBuilder.h"
#include "coreSignalRenDataBuilder.h"
#include "coreITKTensorRenDataBuilder.h"
#include "coreITKTransformRenDataBuilder.h"
#include "coreCuboidRenDataBuilder.h"
#include "coreRenderingTreeMITK.h"

Core::RenDataFactory::RenDataFactory( void )
{
	SetNumberOfInputs( 1 );
	SetNumberOfOutputs( 0 );
}

Core::RenDataFactory::~RenDataFactory( void )
{

}

Core::RenDataBuilder::Pointer Core::RenDataFactory::CreateBuilder( 
	Core::DataEntity::Pointer dataEntity )
{
	RenDataBuilder::Pointer builder;
	if ( dataEntity->IsImage() )
	{
		builder = VTKImageDataRenDataBuilder::New();
	}
	else if ( dataEntity->IsSurfaceMesh() )
	{
		builder = VTKPolyDataRenDataBuilder::New();
	}
	else if ( dataEntity->IsVolumeMesh() )
	{
		builder = VTKUnstructuredGridRenDataBuilder::New();
	}
	else if ( dataEntity->IsSignal() )
	{
		builder = SignalRenDataBuilder::New();
	}
	else if ( dataEntity->IsPointSet() || dataEntity->IsMeasurement() )
	{
		builder = PointSetRenDataBuilder::New();
	}
	else if ( dataEntity->IsContour() )
	{
		builder = ContourRenDataBuilder::New();
	}
	else if ( dataEntity->IsTensor())
	{
		builder = ITKTensorRenDataBuilder::New();
	}
	else if ( dataEntity->IsTransform())
	{
		builder = ITKTransformRenDataBuilder::New();
	}
	else if ( dataEntity->IsCuboid() )
	{
		builder = CuboidRenDataBuilder::New( );
	}

	return builder;
}

void Core::RenDataFactory::Update()
{
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );
	if ( dataEntity.IsNull( ) || 
		 dataEntity->GetNumberOfTimeSteps() == 0 )
	{
		return ;
	}

	DataEntity::Pointer renDataEntity = dataEntity->GetRenderingData( "MITK" );
	if ( renDataEntity.IsNotNull() &&
		renDataEntity->GetMTime() > dataEntity->GetMTime() )
	{
		return ;
	}

	// Switch correct builder
	RenDataBuilder::Pointer builder = CreateBuilder( dataEntity );

	if ( builder )
	{
		builder->SetInputDataEntity( 0, dataEntity );
		builder->Update( );
	}

}

mitk::BaseData::Pointer Core::RenDataFactory::GetBaseData( 
	DataEntity::Pointer dataEntity )
{
	RenDataBuilder::Pointer builder = CreateBuilder( dataEntity );
	if ( builder.IsNull() )
	{
		return NULL;
	}

	builder->SetInputDataEntity( 0, dataEntity );
	return builder->GetBaseData( );
}

mitk::BaseData::Pointer Core::RenDataFactory::GetBaseRenderingData(
	DataEntity::Pointer dataEntity )
{
	Core::DataEntity::Pointer renDataEntity = dataEntity->GetRenderingData( "MITK" );
	if ( renDataEntity.IsNull() )
	{
		return NULL;
	}

	return GetBaseData( renDataEntity );
}

void Core::RenDataFactory::SetNodeDefaultProperties( mitk::DataTreeNode::Pointer newNode )
{
	Core::DataEntity* de = RenderingTreeMITK::GetDataEntity( newNode );

	RenDataBuilder::Pointer builder = CreateBuilder( de );
	if ( builder.IsNull() )
	{
		return ;
	}

	builder->SetNodeDefaultProperties( newNode );
}
