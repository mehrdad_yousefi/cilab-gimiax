/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreITKTransformRenDataBuilder.h"
#include "mitkTransform.h"
#include "mitkTransformObjectFactory.h"
#include "coreITKTransformImpl.h"
#include "coreRenderingTreeMITK.h"

using namespace Core;

ITKTransformRenDataBuilder::ITKTransformRenDataBuilder()
{
	RegisterTransformObjectFactory( );
}

ITKTransformRenDataBuilder::~ITKTransformRenDataBuilder()
{
}

void ITKTransformRenDataBuilder::Update( )
{
	// Get input
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );

	// Try casting to the expected format. The first image is used to initialization
	Core::ITKTransformImpl::TransformPointer transform; 
	bool worked = dataEntity->GetProcessingData( transform, 0 );
	if(!worked || transform.GetPointer() == NULL)
	{
		return;
	}


	// Get rendering data
	Core::DataEntity::Pointer renDataEntity = dataEntity->GetRenderingData( "MITK" );
	if ( renDataEntity.IsNull() )
	{
		renDataEntity = Core::DataEntity::New( Core::TransformId );
		renDataEntity->SwitchImplementation( typeid( mitk::Transform::Pointer ) );
	}

	// Set the same pointer
	// Try to copy data ptr if the data type is the same
	// Otherwise, try to copy the data
	try
	{
		renDataEntity->CopyDataPtr( dataEntity );
	}
	catch (...)
	{
		renDataEntity->Copy( dataEntity, gmReferenceMemory );
	}

	dataEntity->SetRenderingData( "MITK", renDataEntity );
}

mitk::BaseData::Pointer Core::ITKTransformRenDataBuilder::GetBaseData()
{
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );
	mitk::Transform::Pointer data;
	dataEntity->GetProcessingData( data );
	return data.GetPointer();
}

void Core::ITKTransformRenDataBuilder::SetNodeDefaultProperties( mitk::DataTreeNode::Pointer newNode )
{
	Core::DataEntity::Pointer dataEntity = RenderingTreeMITK::GetDataEntity( newNode );
	if ( dataEntity.IsNull( ) )
	{
		return;
	}

	Core::DataEntity::Pointer parent = dataEntity->GetFather( );
	if ( parent.IsNotNull() )
	{
		Core::vtkImageDataPtr image;
		parent->GetProcessingData( image, 0, false, true );
		if ( image )
		{
			int dims[3];
			image->GetDimensions( dims );
			newNode->AddProperty( "gridSpacingX", mitk::FloatProperty::New( dims[ 0 ] / 10 ), NULL, true );
			newNode->AddProperty( "gridSpacingY", mitk::FloatProperty::New( dims[ 1 ] / 10 ), NULL, true );
			newNode->AddProperty( "gridSpacingZ", mitk::FloatProperty::New( dims[ 2 ] / 10 ), NULL, true );

			newNode->AddProperty( "gridOffsetX", mitk::FloatProperty::New( 0.0 ), NULL, true );
			newNode->AddProperty( "gridOffsetY", mitk::FloatProperty::New( 0.0 ), NULL, true );
			newNode->AddProperty( "gridOffsetZ", mitk::FloatProperty::New( 0.0 ), NULL, true );
		}
	}

	// Don't include in the bounding box
	newNode->SetProperty("includeInBoundingBox", mitk::BoolProperty::New(false));
}