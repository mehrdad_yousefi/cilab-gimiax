/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreSignalRenDataBuilder.h"
#include "coreDataEntity.h"
#include "blMitkSignal.h"

using namespace Core;

//!
SignalRenDataBuilder::SignalRenDataBuilder(void)
{
}

//!
SignalRenDataBuilder::~SignalRenDataBuilder(void)
{
}

//!
void SignalRenDataBuilder::Update( )
{

	//! Check processing data
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );

	// Get rendering data
	Core::DataEntity::Pointer renDataEntity = dataEntity->GetRenderingData( "MITK" );
	if ( renDataEntity.IsNull() )
	{
		renDataEntity = Core::DataEntity::New( Core::SignalTypeId );
		renDataEntity->SwitchImplementation( typeid( blMitk::Signal::Pointer ) );
	}

	// Try to copy data ptr if the data type is the same
	// Otherwise, try to copy the data
	try
	{
		renDataEntity->CopyDataPtr( dataEntity );
	}
	catch (...)
	{
		renDataEntity->Copy( dataEntity, gmReferenceMemory );
	}

	dataEntity->SetRenderingData( "MITK", renDataEntity );
}

mitk::BaseData::Pointer Core::SignalRenDataBuilder::GetBaseData()
{
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );
	blMitk::Signal::Pointer data;
	dataEntity->GetProcessingData( data );
	return data.GetPointer();
}

void Core::SignalRenDataBuilder::SetNodeDefaultProperties( mitk::DataTreeNode::Pointer newNode )
{
	// Don't include in the bounding box
	newNode->SetProperty("includeInBoundingBox", mitk::BoolProperty::New(false));
}