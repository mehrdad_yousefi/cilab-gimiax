/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreCuboidRenDataBuilder.h"
#include "coreDataEntity.h"
#include "mitkCuboid.h"

using namespace Core;

//!
CuboidRenDataBuilder::CuboidRenDataBuilder(void)
{
}

//!
CuboidRenDataBuilder::~CuboidRenDataBuilder(void)
{
}

//!
void CuboidRenDataBuilder::Update()
{
	// Get input
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );

	// Get rendering data
	Core::DataEntity::Pointer renDataEntity = dataEntity->GetRenderingData( "MITK" );
	if ( renDataEntity.IsNull() )
	{
		renDataEntity = Core::DataEntity::New( Core::CuboidTypeId );
		renDataEntity->SwitchImplementation( typeid( mitk::Cuboid::Pointer ) );
	}

	// Set the same pointer
	// Try to copy data ptr if the data type is the same
	// Otherwise, try to copy the data
	try
	{
		renDataEntity->CopyDataPtr( dataEntity );
	}
	catch (...)
	{
		renDataEntity->Copy( dataEntity, gmReferenceMemory );
	}

	dataEntity->SetRenderingData( "MITK", renDataEntity );
}

mitk::BaseData::Pointer Core::CuboidRenDataBuilder::GetBaseData()
{
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );
	mitk::Cuboid::Pointer data;
	dataEntity->GetProcessingData( data );
	return data.GetPointer();
}

void Core::CuboidRenDataBuilder::SetNodeDefaultProperties( mitk::DataTreeNode::Pointer newNode )
{

}