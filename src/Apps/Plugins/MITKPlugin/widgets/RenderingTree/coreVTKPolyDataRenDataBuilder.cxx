/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreVTKPolyDataRenDataBuilder.h"
#include "blVTKHelperTools.h"
#include <vtkPolyData.h>
#include <mitkSurface.h>
#include "coreVTKPolyDataHolder.h"
#include "coreDataEntity.h"
#include "coreVTKPolyDataImpl.h"
#include "coreRenderingTreeMITK.h"
#include "coreDataTreeMITKHelper.h"

using namespace Core;

//!
VTKPolyDataRenDataBuilder::VTKPolyDataRenDataBuilder(void)
{
}

//!
VTKPolyDataRenDataBuilder::~VTKPolyDataRenDataBuilder(void)
{
}

//!
void VTKPolyDataRenDataBuilder::Update( )
{
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );

	// Get rendering data
	Core::DataEntity::Pointer renDataEntity = dataEntity->GetRenderingData( "MITK" );
	if ( renDataEntity.IsNull() )
	{
		renDataEntity = Core::DataEntity::New( Core::SurfaceMeshTypeId );
		renDataEntity->SwitchImplementation( typeid( mitk::Surface::Pointer ) );
	}

	// Try to copy data ptr if the data type is the same
	// Otherwise, try to copy the data
	try
	{
		renDataEntity->CopyDataPtr( dataEntity );
	}
	catch (...)
	{
		renDataEntity->Copy( dataEntity, gmReferenceMemory );
	}

	dataEntity->SetRenderingData( "MITK", renDataEntity );
}

mitk::BaseData::Pointer Core::VTKPolyDataRenDataBuilder::GetBaseData()
{
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );
	mitk::Surface::Pointer data;
	dataEntity->GetProcessingData( data );
	return data.GetPointer();
}

void Core::VTKPolyDataRenDataBuilder::SetNodeDefaultProperties( 
	mitk::DataTreeNode::Pointer newNode )
{
	// Set the material properties if it was a mesh
	mitk::MaterialProperty::Pointer materialProperty;
	materialProperty = mitk::MaterialProperty::New( newNode );
	// Default properties of ParaView:
	materialProperty->SetAmbientColor( 1, 1, 1 );
	materialProperty->SetAmbientCoefficient( 0 );
	materialProperty->SetDiffuseColor( 1, 1, 1 );
	materialProperty->SetDiffuseCoefficient( 1 );
	materialProperty->SetSpecularColor( 1, 1, 1 );
	materialProperty->SetSpecularCoefficient( 0.10 );
	materialProperty->SetSpecularPower( 100 );
	materialProperty->SetOpacity( 1 );
	materialProperty->SetInterpolation( mitk::MaterialProperty::Gouraud );
	newNode->SetProperty("material", materialProperty );

	// Get processing data as vtkPolyData
	DataTreeMITKHelper::ApplyLUTToFirstScalarsVector( newNode );
}


