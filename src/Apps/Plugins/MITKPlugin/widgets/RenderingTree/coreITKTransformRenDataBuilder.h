/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreITKTransformRenDataBuilder_H
#define coreITKTransformRenDataBuilder_H

#include "corePluginMacros.h"
#include "coreRenDataBuilder.h"

namespace Core
{
/** 

\ingroup MITKPlugin
\author: Xavi Planes
\date: 30 July 2010
*/

class PLUGIN_EXPORT ITKTransformRenDataBuilder 
	: public Core::RenDataBuilder
{
public:
	coreDeclareSmartPointerClassMacro( 
		Core::ITKTransformRenDataBuilder, 
		RenDataBuilder)

	//@{ 
	//! \name Overrides of parent implementation.
	void Update( );
	mitk::BaseData::Pointer GetBaseData( );
	void SetNodeDefaultProperties( mitk::DataTreeNode::Pointer newNode );
	//@}

protected:
	ITKTransformRenDataBuilder();
	~ITKTransformRenDataBuilder();

private:
	coreDeclareNoCopyConstructors(ITKTransformRenDataBuilder);
};

}

#endif
