/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreContourRenDataBuilder_H
#define coreContourRenDataBuilder_H

#include "corePluginMacros.h"
#include "coreRenDataBuilder.h"

namespace Core
{
/** 
\brief A specialization of the RenDataBuilder class for VTK PolyData objects.

\ingroup MITKPlugin
\sa RenDataBuilder
\author Juan Antonio Moya
\date 28 Mar 2008
*/
class PLUGIN_EXPORT ContourRenDataBuilder : public RenDataBuilder
{
public:
	coreDeclareSmartPointerClassMacro( Core::ContourRenDataBuilder, RenDataBuilder );

	//@{ 
	//! \name Overrides of parent implementation.
	void Update( );
	mitk::BaseData::Pointer GetBaseData( );
	void SetNodeDefaultProperties( mitk::DataTreeNode::Pointer newNode );
	//@}

protected:
	//!
	ContourRenDataBuilder(void);

	//!
	virtual ~ContourRenDataBuilder(void);

	//! Check processing data is OK
	bool CheckProcessingDataIsOk( Core::DataEntityImpl::Pointer timeStep );

private:
	coreDeclareNoCopyConstructors(ContourRenDataBuilder);
};

}

#endif
