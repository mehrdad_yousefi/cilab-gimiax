/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreITKTensorRenDataBuilder_H
#define coreITKTensorRenDataBuilder_H

#include "corePluginMacros.h"
#include "coreRenDataBuilder.h"
#include <itkImage.h>
#include <mitkImage.h>

namespace Core
{
/** 
\brief A specialization of the RenDataBuilder class for ITK Tensor objects.

\ingroup MITKPlugin
\sa RenDataBuilder
\author Chiara Riccobene
\date 10 Jun 2010
*/
class PLUGIN_EXPORT ITKTensorRenDataBuilder : public RenDataBuilder
{
public:
	coreDeclareSmartPointerClassMacro( Core::ITKTensorRenDataBuilder, RenDataBuilder );

	//@{ 
	//! \name Overrides of parent implementation.
	void Update( );
	mitk::BaseData::Pointer GetBaseData( );
	void SetNodeDefaultProperties( mitk::DataTreeNode::Pointer newNode );
	//@}

protected:
	ITKTensorRenDataBuilder(void);

	//!
	virtual ~ITKTensorRenDataBuilder(void);


	mitk::BaseData::Pointer BuildSingleTimeStep(
		const Core::DataEntityImpl::Pointer timeStep);

	void GetMitkImageFromItkImage(
		AnyProcessingData processingData, mitk::Image::Pointer mitkImage );

private:
	coreDeclareNoCopyConstructors(ITKTensorRenDataBuilder);
};

}

#endif
