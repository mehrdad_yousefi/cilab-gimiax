/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreRenDataBuilder.h"

#include "coreContourRenDataBuilder.h"
#include "corePointSetRenDataBuilder.h"
#include "coreVTKImageDataRenDataBuilder.h"
#include "coreVTKPolyDataRenDataBuilder.h"
#include "coreVTKUnstructuredGridRenDataBuilder.h"

Core::RenDataBuilder::RenDataBuilder( void )
{
	SetNumberOfInputs( 1 );
	SetNumberOfOutputs( 0 );
}

Core::RenDataBuilder::~RenDataBuilder( void )
{

}

