/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreVTKUnstructuredGridRenDataBuilder.h"
#include "blVTKHelperTools.h"
#include <vtkUnstructuredGrid.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkSmartPointer.h>
#include <mitkUnstructuredGrid.h>
#include <mitkSurface.h>
#include "coreVTKUnstructuredGridHolder.h"
#include "coreVTKPolyDataHolder.h"
#include "blMitkSurface.h"
#include "vtkArrayCalculator.h"
#include "coreDataTreeMITKHelper.h"

using namespace Core;

//!
VTKUnstructuredGridRenDataBuilder::VTKUnstructuredGridRenDataBuilder(void)
{
}

//!
VTKUnstructuredGridRenDataBuilder::~VTKUnstructuredGridRenDataBuilder(void)
{
}

//!
void VTKUnstructuredGridRenDataBuilder::Update( )
{
	// Get input
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );

	// Get rendering data
	Core::DataEntity::Pointer renDataEntity = dataEntity->GetRenderingData( "MITK" );
	if ( renDataEntity.IsNull() )
	{
		renDataEntity = Core::DataEntity::New( Core::SurfaceMeshTypeId );
		renDataEntity->SwitchImplementation( typeid( blMitk::Surface::Pointer ) );
	}

	mitk::Surface::Pointer mitkSurface;
	renDataEntity->GetProcessingData( mitkSurface );

	vtkPolyDataPtr renPolydata;
	mitkSurface->Expand( dataEntity->GetNumberOfTimeSteps() );
	for ( int iCount = 0 ; iCount < dataEntity->GetNumberOfTimeSteps() ; iCount++ )
	{
		vtkUnstructuredGridPtr volumeMesh;
		dataEntity->GetProcessingData( volumeMesh, iCount );
		if ( volumeMesh == NULL )
		{
			return;
		}
		
		vtkSmartPointer<vtkDataSetSurfaceFilter> filter = vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
		filter->SetInput( volumeMesh );
		filter->Update();
		renPolydata = vtkPolyDataPtr::New();
		renPolydata->DeepCopy(filter->GetOutput());

		// Compute magnitude for PointData arrays with multiple components
		vtkSmartPointer<vtkArrayCalculator> calc = vtkSmartPointer<vtkArrayCalculator>::New();
		calc->SetInput( renPolydata );
		calc->SetAttributeModeToUsePointData();

		int nA = renPolydata->GetPointData( )->GetNumberOfArrays( );
		for ( int i = 0; i < nA; i++ ) 
		{
			std::string arrayName( renPolydata->GetPointData( )->GetArrayName( i ) );
			vtkDataArray * dataArray;
			dataArray = renPolydata->GetPointData( )->GetArray( arrayName.c_str( ) );
			if ( dataArray == NULL ) 
			{
				continue;
			}

			int nComponents = dataArray->GetNumberOfComponents( ); 
			if ( nComponents > 1 )
			{
				std::stringstream strFunction;
				strFunction << "sqrt(";
				for ( int c = 0 ; c < nComponents ; c++ )
				{
					std::stringstream strComp;
					strComp << "s_" << c;
					calc->AddScalarVariable( strComp.str().c_str(), arrayName.c_str(), c);
					if ( c!= 0 )
					{
						strFunction << " + ";
					}
					strFunction << strComp.str() << "*" << strComp.str();
				}
				strFunction << ")";
				calc->SetFunction( strFunction.str().c_str() );
				// Don't overrite existing array because it creates confusion 
				// in the function vtkArrayCalculator
				// Use different name to avoid conflicting errors
				std::stringstream stream;
				stream << arrayName << "_Mag";
				calc->SetResultArrayName( stream.str().c_str() );
				calc->SetResultArrayType( dataArray->GetDataType() );
				calc->Update();

				// Copy array
				//vtkDataArray * outDataArray = calc->GetOutput()->GetPointData()->GetArray( arrayName.c_str() );
				//renPolydata->GetPointData()->GetArray( arrayName.c_str() )->DeepCopy( outDataArray );
			}
		}


		mitkSurface->SetVtkPolyData( renPolydata, iCount );
	}

	dataEntity->SetRenderingData( "MITK", renDataEntity );

}

mitk::BaseData::Pointer Core::VTKUnstructuredGridRenDataBuilder::GetBaseData()
{
	Core::DataEntity::Pointer dataEntity = GetInputDataEntity( 0 );
	mitk::Surface::Pointer data;
	dataEntity->GetProcessingData( data );
	return data.GetPointer();
}

void Core::VTKUnstructuredGridRenDataBuilder::SetNodeDefaultProperties( mitk::DataTreeNode::Pointer newNode )
{
	// Set the material properties if it was a mesh
	// add a default transfer function
	mitk::TransferFunction::Pointer tf = mitk::TransferFunction::New();
	float m_Min =1; //image->GetScalarValueMin();
	float m_Max =2; //image->GetScalarValueMax();
	tf->GetScalarOpacityFunction()->Initialize();
	tf->GetScalarOpacityFunction()->AddPoint ( m_Min, 0 );
	tf->GetScalarOpacityFunction()->AddPoint ( m_Max, 1 );
	tf->GetColorTransferFunction()->AddRGBPoint(m_Min,1,0,0);
	tf->GetColorTransferFunction()->AddRGBPoint(m_Max,1,1,0);
	tf->GetGradientOpacityFunction()->Initialize();
	tf->GetGradientOpacityFunction()->AddPoint(m_Min,1.0);
	tf->GetGradientOpacityFunction()->AddPoint(0.0,1.0);
	tf->GetGradientOpacityFunction()->AddPoint((m_Max*0.25),1.0);
	tf->GetGradientOpacityFunction()->AddPoint(m_Max,1.0);  
	newNode->SetProperty ( "TransferFunction", mitk::TransferFunctionProperty::New( tf.GetPointer() ) );

	//code sent by mitk users list
	//mitk::UnstructuredGridVtkMapper3D::SetProperties( newNode );
	newNode->SetProperty ("outline polygons", mitk::BoolProperty::New(true));
	mitk::MaterialProperty::Pointer materialProperty;
	materialProperty = mitk::MaterialProperty::New(1.0f, 1.0f, 1.0f, 1.0f, newNode);
	newNode->SetProperty("material", materialProperty );
	// Get processing data as vtkPolyData
	DataTreeMITKHelper::ApplyLUTToFirstScalarsVector( newNode );
}