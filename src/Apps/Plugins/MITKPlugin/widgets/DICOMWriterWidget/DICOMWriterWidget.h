/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _DICOMWriterWidget_H
#define _DICOMWriterWidget_H

#include <vector>
#include <map>
#include <limits>
#include <cmath>
#include <algorithm>
#include <sstream>

#include "blTagMap.h"

#include "DICOMWriterWidgetUI.h"
#include "coreBaseWindow.h"

namespace Core{
	namespace Widgets {

		#define wxID_DICOMWriterWidget wxID("wxID_DICOMWriterWidget")

		/**
		Widget that is shown when the image data is of type float or double
		If user answer yes, the data will be casted to int
		\author Xavi Planes
		\ingroup MITKPlugin
		\date Apr 2011
		*/
		class DICOMWriterWidget : 
			public DICOMWriterWidgetUI,
			public Core::BaseWindow 
		{
			// OPERATIONS
		public:
			class Factory : public Core::BaseWindowIOFactory
			{
			public: 
				coreDeclareSmartPointerTypesMacro(Factory,BaseWindowIOFactory)
					coreFactorylessNewMacro(Factory)
					coreClassNameMacro(Core::Widgets::DICOMWriterWidgetFactory)
					coreCommonFactoryFunctions( DICOMWriterWidget )
			protected:
				Factory( ){
					m_DataEntitytype = Core::ImageTypeId;
					m_Extension = ".dcm";
				}
				virtual bool CheckType( 
					Core::DataEntityType type, 
					const std::string &ext, 
					bool isReading,
					Core::DataEntity::Pointer dataEntity );
			};

			class Processor : public BaseProcessor
			{
			public:
				coreDeclareSmartPointerClassMacro( Processor, BaseProcessor );

				void Update();
			private:
				Processor( );
			};

			//!
			DICOMWriterWidget(wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

			//!
			~DICOMWriterWidget( );

			//!
			Core::BaseProcessor::Pointer GetProcessor( );

			/** Set and Get properties of the window to serialize them
			or visualize in the GUI
			*/
			void SetProperties( blTagMap::Pointer tagMap );
			void GetProperties( blTagMap::Pointer tagMap );


		private:

			// ATTRIBUTES
			Processor::Pointer m_Processor;
		private:
		};

	} //namespace Widgets
} //namespace Core

#endif //_DICOMWriterWidget_H
