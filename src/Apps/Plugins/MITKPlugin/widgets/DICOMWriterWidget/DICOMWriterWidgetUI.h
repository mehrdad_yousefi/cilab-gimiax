// -*- C++ -*- generated by wxGlade 0.6.3 on Fri Apr 08 16:54:43 2011

#include <wx/wx.h>
#include <wx/image.h>

#ifndef DICOMWRITERWIDGETUI_H
#define DICOMWRITERWIDGETUI_H

// begin wxGlade: ::dependencies
// end wxGlade

// begin wxGlade: ::extracode


// end wxGlade


class DICOMWriterWidgetUI: public wxPanel {
public:
    // begin wxGlade: DICOMWriterWidgetUI::ids
    // end wxGlade

    DICOMWriterWidgetUI(wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

private:
    // begin wxGlade: DICOMWriterWidgetUI::methods
    void set_properties();
    void do_layout();
    // end wxGlade

protected:
    // begin wxGlade: DICOMWriterWidgetUI::attributes
    wxStaticText* label_1;
    // end wxGlade
}; // wxGlade: end class


#endif // DICOMWRITERWIDGETUI_H
