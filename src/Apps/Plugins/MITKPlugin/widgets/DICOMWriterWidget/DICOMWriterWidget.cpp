/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "DICOMWriterWidget.h"
#include "coreVTKProcessor.h"
#include "vtkImageCast.h"
#include "coreProcessorManager.h"

using namespace Core::Widgets;

void Core::Widgets::DICOMWriterWidget::SetProperties( blTagMap::Pointer tagMap )
{
}

void Core::Widgets::DICOMWriterWidget::GetProperties( blTagMap::Pointer tagMap )
{
	
}

Core::BaseProcessor::Pointer Core::Widgets::DICOMWriterWidget::GetProcessor()
{
	return m_Processor.GetPointer( );
}

DICOMWriterWidget::DICOMWriterWidget(wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
	DICOMWriterWidgetUI(parent, id, pos, size, style)
{
	m_Processor = Processor::New( );
}

DICOMWriterWidget::~DICOMWriterWidget( ) 
{
}

bool Core::Widgets::DICOMWriterWidget::Factory::CheckType( 
	Core::DataEntityType type, 
	const std::string &ext, 
	bool isReading, 
	Core::DataEntity::Pointer dataEntity )
{
	if ( isReading )
	{
		return false;
	}

	if ( ext != m_Extension )
	{
		return false;
	}

	// If DICOM and FLOAT or DOUBLE -> Convert to INT
	bool scalarIsFloatOrDouble = false;
	Core::vtkImageDataPtr image;
	bool worked = dataEntity->GetProcessingData( image );
	if( worked && image != NULL && 
		( image->GetScalarType() == VTK_FLOAT || image->GetScalarType() == VTK_DOUBLE ) )
	{
		scalarIsFloatOrDouble = true;
	}

	return scalarIsFloatOrDouble;
}

void Core::Widgets::DICOMWriterWidget::Processor::Update()
{
	if ( GetInputDataEntity( 0 ).IsNull( ) )
	{
		return;
	}

	VTKProcessor<vtkImageCast>::Pointer processor;
	processor = VTKProcessor<vtkImageCast>::New();
	processor->GetFilter( )->SetOutputScalarTypeToInt( );
	processor->SetInputDataEntity( 0, GetInputDataEntity( 0 ) );
	processor->GetOutputPort( 0 )->SetReuseOutput( true );
	processor->SetMultithreading( false );
	processor->GetInputPort( 0 )->SetUpdateMode( BaseFilterInputPort::UPDATE_ACCESS_SINGLE_TIME_STEP );
	processor->GetInputPort( 0 )->SetTimeStepSelection( BaseFilterInputPort::SELECT_ALL_TIME_STEPS );

	// Execute it
	Core::Runtime::Kernel::GetProcessorManager()->Execute( processor.GetPointer( ) );

	UpdateOutput( 0, processor->GetOutputDataEntity( 0 ) );
}

Core::Widgets::DICOMWriterWidget::Processor::Processor()
{
	SetNumberOfInputs( 1 );
	GetInputPort( 0 )->SetName( "Input surface" );
	GetInputPort( 0 )->SetDataEntityType( Core::ImageTypeId );

	SetNumberOfOutputs( 1 );
	GetOutputPort( 0 )->SetDataEntityType( Core::ImageTypeId );
	GetOutputPort( 0 )->SetReuseOutput( false );
	GetOutputPort( 0 )->SetDataEntityName( "DICOM image" );
}
