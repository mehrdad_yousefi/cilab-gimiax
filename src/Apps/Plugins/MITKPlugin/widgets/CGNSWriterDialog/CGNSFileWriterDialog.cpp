/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "CGNSFileWriterDialog.h"
#include "coreCgnsFileWriter.h"

using namespace Core::Widgets;

CGNSFileWriterDialog::CGNSFileWriterDialog( wxWindow* parent, int id ) :
	CGNSFileWriterDialogUI(parent,id)
{
	try 
	{
	
	} 	
	coreCatchExceptionsReportAndNoThrowMacro( 
		"CGNSFileWriterDialog::CGNSFileWriterDialog" );
}

CGNSFileWriterDialog::~CGNSFileWriterDialog( ) 
{
}

void CGNSFileWriterDialog::CheckBoxClicked(wxCommandEvent& event)
{
	int pos = event.GetId();
	wxCheckBox* checkBox = m_MapIdToCheckBox[pos];

	bool bChecked = checkBox->GetValue();

	std::pair<int,bool> posPair = m_MapSelected[pos];
	posPair.second = bChecked;

	m_MapSelected[pos] = posPair;
}

Core::BaseProcessor::Pointer Core::Widgets::CGNSFileWriterDialog::GetProcessor()
{
	return NULL;
}

void Core::Widgets::CGNSFileWriterDialog::SetProperties( blTagMap::Pointer tagMap )
{
	int idWx = 100;

	int dataEntityId = tagMap->GetTagValue<int>( "DataEntityID" );
	Core::DataContainer::Pointer data = Core::Runtime::Kernel::GetDataContainer();
	Core::DataEntityList::Pointer list = data->GetDataEntityList();
	Core::DataEntity::Pointer dataEntity = list->GetDataEntity( dataEntityId );
	Core::IO::CGNSFileWriter::GenerateMetadata( dataEntity );

	std::vector<std::string> scalars;
	std::vector<std::string> vectors;

	blTag::Pointer tag = tagMap->FindTagByName( "ScalarsPoint" );
	if ( tag.IsNotNull() )
	{
		tag->GetValue( scalars );
	}

	tag = tagMap->FindTagByName( "VectorsPoint" );
	if ( tag.IsNotNull() )
	{
		tag->GetValue( vectors );
	}

	std::vector<std::string>::iterator it;
	for ( it = scalars.begin(); it != scalars.end(); it++ ) 
	{
		wxCheckBox* checkbox = new wxCheckBox( this, ++idWx, *it );
		checkbox->SetValue( true );
		sizer_2_staticbox->GetContainingSizer()->Add(checkbox, 0, 0, 0);

		Connect( 
			idWx, 
			wxEVT_COMMAND_CHECKBOX_CLICKED, 
			wxCommandEventHandler(CGNSFileWriterDialog::CheckBoxClicked) 
			);

		m_MapSelected[idWx] = std::pair<int,bool>(1,true);
		m_MapIdToString[idWx] = *it;
		m_MapIdToCheckBox[idWx] = checkbox;
	}

	for ( it = vectors.begin(); it != vectors.end(); it++ ) 
	{	
		wxCheckBox* checkbox = new wxCheckBox( this, ++idWx, *it );
		checkbox->SetValue( true );
		sizer_3_staticbox->GetContainingSizer()->Add(checkbox, 0, 0, 0);

		Connect( 
			idWx, 
			wxEVT_COMMAND_CHECKBOX_CLICKED, 
			wxCommandEventHandler(CGNSFileWriterDialog::CheckBoxClicked) 
			);

		m_MapSelected[idWx] = std::pair<int,bool>(3,true);
		m_MapIdToString[idWx] = *it;
		m_MapIdToCheckBox[idWx] = checkbox;
	}

	GetSizer()->Fit(this);
	Layout();	
}

void Core::Widgets::CGNSFileWriterDialog::GetProperties( blTagMap::Pointer tagMap )
{
	std::vector<std::string> scalars;
	std::vector<std::string> vectors;

	std::map<int,std::pair<int,bool> >::iterator it;
	for ( it = m_MapSelected.begin(); it!= m_MapSelected.end(); ++it )
	{
		if ( (*it).second.first == 1 && (*it).second.second == true )
		{
			scalars.push_back(m_MapIdToString[(*it).first]);
		}
		else if ( (*it).second.first == 3 && (*it).second.second == true )
		{
			vectors.push_back(m_MapIdToString[(*it).first]);
		}
	}
	
	tagMap->AddTag( "ScalarsPoint", scalars );
	tagMap->AddTag( "VectorsPoint", vectors );
}



bool Core::Widgets::CGNSFileWriterDialog::Factory::CheckType( 
	Core::DataEntityType type, 
	const std::string &ext, 
	bool isReading, 
	Core::DataEntity::Pointer dataEntity )
{
	if ( isReading )
	{
		return false;
	}

	bool isCGNS = false;
	if ( dataEntity.IsNotNull() )
	{
		blTag::Pointer tag = dataEntity->GetMetadata()->FindTagByName( "IsCGNS" );
		if ( tag.IsNotNull() )
		{
			tag->GetValue( isCGNS );
		}
	}

	return isCGNS;
}

