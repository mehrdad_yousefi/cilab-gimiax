/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _CGNSFileWriterDialog_H
#define _CGNSFileWriterDialog_H

#include <vector>
#include <map>
#include <limits>
#include <cmath>
#include <algorithm>
#include <sstream>

#include "blTagMap.h"

#include "CGNSFileWriterDialogUI.h"

#include "coreBaseWindow.h"

namespace Core{
	namespace Widgets {

		/**
		\author Albert Sanchez
		\date 10 06 2010
		*/

		#define wxID_CGNSFileWriterDialog wxID("wxID_CGNSFileWriterDialog")


		class CGNSFileWriterDialog : 
			public CGNSFileWriterDialogUI,
			public Core::BaseWindow
		{
			// OPERATIONS
		public:
			class Factory : public Core::BaseWindowIOFactory
			{
			public: 
				coreDeclareSmartPointerTypesMacro(Factory,BaseWindowIOFactory)
				coreFactorylessNewMacro(Factory)
				coreClassNameMacro(Core::Widgets::CGNSFileWriterDialogFactory)
				coreCommonFactoryFunctions( Core::Widgets::CGNSFileWriterDialog )
			protected:
				Factory( ){
					m_DataEntitytype = Core::VolumeMeshTypeId;
					m_Extension = ".cgns";
				}
				virtual bool CheckType( 
					Core::DataEntityType type, 
					const std::string &ext, 
					bool isReading,
					Core::DataEntity::Pointer dataEntity );
			};

			//!
			CGNSFileWriterDialog( wxWindow* parent, int id );

			//!
			~CGNSFileWriterDialog( );

			//!
			virtual Core::BaseProcessor::Pointer GetProcessor( );

			//!
			virtual void SetProperties( blTagMap::Pointer tagMap );
			virtual void GetProperties( blTagMap::Pointer tagMap );

		private:

			//!
			void CheckBoxClicked(wxCommandEvent& event);

			// ATTRIBUTES
		private:

			std::map<int,std::string> m_MapIdToString;
			std::map<int,std::pair<int,bool> > m_MapSelected;
			std::map<int,wxCheckBox*> m_MapIdToCheckBox;

		};

	} //namespace Widgets
} //namespace Core

#endif //_CGNSFileWriterDialog_H
