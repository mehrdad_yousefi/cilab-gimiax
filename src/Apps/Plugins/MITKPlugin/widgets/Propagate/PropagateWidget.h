/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _PropagateWidget_H
#define _PropagateWidget_H

#include "PropagateWidgetUI.h"
#include "coreProcessingWidget.h"
#include "PropagateProcessor.h"

/**
Propagate a DataEntity through time

\author Xavi Planes
\ingroup MITKPlugin
\date Aug 2012
*/
class PropagateWidget : 
	public PropagateWidgetUI,
	public Core::Widgets::ProcessingWidget
{

public:
	coreDefineBaseWindowFactory( PropagateWidget );

	//!
    PropagateWidget(wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

	//!
	~PropagateWidget( );

	//!
	Core::BaseProcessor::Pointer GetProcessor();

private:
	//!
    void OnPropagateBtn(wxCommandEvent &event);

protected:

	//!
	PropagateProcessor::Pointer m_Processor;
};


#endif // _PropagateWidget_H
