/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "PropagateWidget.h"


PropagateWidget::PropagateWidget(wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    PropagateWidgetUI(parent, id, pos, size, style)
{
	m_Processor = PropagateProcessor::New( );
}

PropagateWidget::~PropagateWidget( )
{
}

//!
Core::BaseProcessor::Pointer PropagateWidget::GetProcessor()
{
	return m_Processor.GetPointer( );
}

void PropagateWidget::OnPropagateBtn(wxCommandEvent &event)
{
	m_Processor->SetTimeStep( GetTimeStep() );
	m_Processor->SetRange(m_spinCtrlLeft->GetValue(),m_spinCtrlRight->GetValue());
	m_Processor->Update(  );
}

