/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreToolbarMultiRenderWindow_H
#define _coreToolbarMultiRenderWindow_H

#include "coreToolbarBase.h"

namespace Core
{
namespace Widgets
{

#define wxID_FitScene				wxID("wxID_FitScene")
#define wxID_InitAxis				wxID("wxID_InitAxis")
#define wxID_HideXYZPlanes			wxID("wxID_HideXYZPlanes")
#define wxID_LAYOUT_TOOL			wxID( "wxID_LAYOUT_TOOL" )

class MultiRenderWindowMITK;

/**
\brief Toolbar for RenderWindowBase
\ingroup MITKPlugin
\author Chiara Riccobene
\date 7 Jan 2010
*/
class PLUGIN_EXPORT ToolbarMultiRenderWindow: public Core::Widgets::ToolbarBase {
public:
	//!
	coreDefineBaseWindowFactory( Core::Widgets::ToolbarMultiRenderWindow );

	//!
	ToolbarMultiRenderWindow(wxWindow* parent, wxWindowID id = wxID_ANY, 
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = wxAUI_TB_DEFAULT_STYLE, 
		const wxString& name = wxPanelNameStr);


	//!
	void SetMultiRenderWindow( Core::Widgets::RenderWindowBase* val);

	//! Layout Group
	void OnSelectedLayout(wxCommandEvent& event);

	//! Axis and Planes
	void OnEnableAxis(wxCommandEvent& event);
	void OnFitScene(wxCommandEvent& event);
	void OnInitAxis(wxCommandEvent& event);
	//! Show/Hide the X, Y and Z plane cuts
	void OnHideXYZPlanes( wxCommandEvent& event );
	
protected:
	//!
	void UpdateState();
	
	//!
	void UpdateLayoutState( );

	//!
	MultiRenderWindowMITK* GetMultiRenderWindowMITK();

    wxDECLARE_EVENT_TABLE();
private:
	//!
	wxAuiToolBarItemArray m_append_items;

};

} // namespace Widget
} // namespace Core

#endif // _coreToolbarMultiRenderWindow_H
