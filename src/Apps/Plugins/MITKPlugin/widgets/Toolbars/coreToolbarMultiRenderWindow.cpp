/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreToolbarMultiRenderWindow.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreKernel.h"
#include "coreMultiRenderWindowMITK.h"
#include "coreMultiRenderWindowEventHandler.h"

//#include ".xpm"
#include "EnableAxisOn.xpm"
#include "FitSceneOn.xpm"
#include "InitAxis.xpm"
#include "XYZPlanesOn.xpm"
#include "2DViewsLeftOn.xpm"
#include "Big3DViewOn.xpm"
#include "Only2DViewsOn.xpm"
#include "StandartLayoutOn.xpm"
#include "XPlaneIconSetOn.xpm"
#include "YPlaneIconSetOn.xpm"
#include "ZPlaneIconSetOn.xpm"

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(Core::Widgets::ToolbarMultiRenderWindow, Core::Widgets::ToolbarBase)
  EVT_TOOL(wxID_FitScene, Core::Widgets::ToolbarMultiRenderWindow::OnFitScene)
  EVT_TOOL(wxID_LockAxis, Core::Widgets::ToolbarMultiRenderWindow::OnEnableAxis)
  EVT_TOOL(wxID_InitAxis, Core::Widgets::ToolbarMultiRenderWindow::OnInitAxis)
  EVT_TOOL(wxID_HideXYZPlanes, Core::Widgets::ToolbarMultiRenderWindow::OnHideXYZPlanes)
  EVT_TOOL(wxID_ANY, Core::Widgets::ToolbarMultiRenderWindow::OnSelectedLayout)
END_EVENT_TABLE()


Core::Widgets::ToolbarMultiRenderWindow::ToolbarMultiRenderWindow(
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
    Core::Widgets::ToolbarBase(parent, id, pos, size, style, name)
{
	wxBitmap bitmap;

	// View Interaction with planes
	bitmap = wxBitmap( fitsceneon_xpm );
	AddTool(wxID_FitScene, _T("Fit Scene"),
		bitmap, _T("Fit Scene"), wxITEM_CHECK);

	bitmap = wxBitmap( initaxis_xpm );
	AddTool(wxID_InitAxis, _T("Init Axis"),
		bitmap, _T("Initialize Axis"), wxITEM_CHECK);

	bitmap = wxBitmap( enableaxison_xpm );
	AddTool(wxID_LockAxis, _T("Lock Axis"),
		bitmap, _T("Lock Axis in the current position"), wxITEM_CHECK);

	bitmap = wxBitmap( xyzplaneson_xpm );
	AddTool(wxID_HideXYZPlanes, _T("Hide X Y Z planes"),
		bitmap, _T("Hide X Y Z planes"), wxITEM_CHECK);

	bitmap = wxBitmap( standartlayouton_xpm );
	AddTool(wxID_LAYOUT_TOOL, _T("Selected Layout"),
		bitmap, _T("Selected Layout"), wxITEM_NORMAL);

	// Layout
	wxAuiToolBarItemArray prepend_items;
	wxAuiToolBarItem item;

	item.SetKind(wxITEM_NORMAL);
    item.SetId(wxID_ViewLayoutDefault);
    item.SetLabel(_T("Standard (ZXY)"));
	item.SetBitmap( wxBitmap( standartlayouton_xpm ) );
    m_append_items.Add(item);

	item.SetKind(wxITEM_NORMAL);
    item.SetId(wxID_ViewLayout2DViewsLeft);
    item.SetLabel(_T("2D Views Left"));
	item.SetBitmap( wxBitmap( s2dviewslefton_xpm ) );
    m_append_items.Add(item);

	item.SetKind(wxITEM_NORMAL);
    item.SetId(wxID_ViewLayoutOnly2DViews);
    item.SetLabel(_T("Only 2d Views"));
	item.SetBitmap( wxBitmap( only2dviewson_xpm ) );
    m_append_items.Add(item);

	item.SetKind(wxITEM_NORMAL);
    item.SetId(wxID_ViewLayoutOnly2DViews_XYZ);
    item.SetLabel(_T("Only 2d Views (2up)"));
	item.SetBitmap( wxBitmap( only2dviewson_xpm ) );
    m_append_items.Add(item);

	item.SetKind(wxITEM_NORMAL);
    item.SetId(wxID_ViewLayoutXView);
    item.SetLabel(_T("X View"));
	item.SetBitmap( wxBitmap( xplaneiconseton_xpm ) );
    m_append_items.Add(item);

	item.SetKind(wxITEM_NORMAL);
    item.SetId(wxID_ViewLayoutYView);
    item.SetLabel(_T("Y View"));
	item.SetBitmap( wxBitmap( yplaneiconseton_xpm ) );
    m_append_items.Add(item);

	item.SetKind(wxITEM_NORMAL);
    item.SetId(wxID_ViewLayoutZView);
    item.SetLabel(_T("Z View"));
	item.SetBitmap( wxBitmap( zplaneiconseton_xpm ) );
    m_append_items.Add(item);

	item.SetKind(wxITEM_NORMAL);
    item.SetId(wxID_ViewLayoutBig3DView);
    item.SetLabel(_T("Single 3D window"));
	item.SetBitmap( wxBitmap( big3dviewon_xpm ) );
    m_append_items.Add(item);

	item.SetKind(wxITEM_NORMAL);
    item.SetId(wxID_ViewLayoutDefault2x2_YXZ3D);
    item.SetLabel(_T("Standard (YXZ)"));
	item.SetBitmap( wxBitmap( standartlayouton_xpm ) );
    m_append_items.Add(item);

	item.SetKind(wxITEM_NORMAL);
    item.SetId(wxID_ViewLayoutDefault2x2_XYZ3D);
    item.SetLabel(_T("Standard (XYZ)"));
	item.SetBitmap( wxBitmap( standartlayouton_xpm ) );
    m_append_items.Add(item);

	SetCustomOverflowItems(prepend_items, m_append_items);

	SetOverflowVisible( true );

	Realize();

	Enable( GetMultiRenderWindowMITK() );
}
	
void Core::Widgets::ToolbarMultiRenderWindow::OnSelectedLayout( wxCommandEvent& event )
{
	mitk::LayoutConfiguration layoutType = mitk::Default2x2;
	if ( event.GetId( ) == wxID_ViewLayoutDefault ) layoutType = mitk::Default2x2;
	else if ( event.GetId( ) == wxID_ViewLayout2DViewsLeft ) layoutType = mitk::Left2DRight3D;
	else if ( event.GetId( ) == wxID_ViewLayoutBig3DView ) layoutType = mitk::Single3D;
	else if ( event.GetId( ) == wxID_ViewLayoutXView ) layoutType = mitk::Single2D_X;
	else if ( event.GetId( ) == wxID_ViewLayoutYView ) layoutType = mitk::Single2D_Y;
	else if ( event.GetId( ) == wxID_ViewLayoutZView ) layoutType = mitk::Single2D_Z;
	else if ( event.GetId( ) == wxID_ViewLayoutOnly2DViews ) layoutType = mitk::Only2D;
	else if ( event.GetId( ) == wxID_ViewLayoutOnly2DViews_XYZ ) layoutType = mitk::Only2D_XYZ;
	else if ( event.GetId( ) == wxID_ViewLayoutDefault2x2_YXZ3D ) layoutType = mitk::Default2x2_YXZ3D;
	else if ( event.GetId( ) == wxID_ViewLayoutDefault2x2_XYZ3D ) layoutType = mitk::Default2x2_XYZ3D;
	else
	{
		event.Skip();
		return;
	}

	GetMultiRenderWindowMITK()->SetCurrentLayout( layoutType );
	UpdateState();
	event.Skip();
}

void Core::Widgets::ToolbarMultiRenderWindow::OnEnableAxis( wxCommandEvent& event )
{
	if ( /*IsEnabled()*/IsThisEnabled() )
	{
		GetMultiRenderWindowMITK()->EnableAxis(!GetToolToggled( wxID_LockAxis ) );
		ToggleTool(wxID_LockAxis, GetMultiRenderWindow()->GetMetadata( )->GetTagValue<bool>( "AxisLocked" ) );
	}
}

void Core::Widgets::ToolbarMultiRenderWindow::OnFitScene( wxCommandEvent& event )
{
	if( GetToolToggled(wxID_FitScene) )
	{
		GetMultiRenderWindowMITK()->FitScene();
		GetMultiRenderWindowMITK()->RequestUpdate();
		ToggleTool(wxID_FitScene, false);
	}
}

void Core::Widgets::ToolbarMultiRenderWindow::OnInitAxis( wxCommandEvent& event )
{
	if( GetToolToggled(wxID_InitAxis))
	{
		GetMultiRenderWindowMITK()->ReInitializeViewControllers( );
		ToggleTool(wxID_InitAxis, false);
	}
}

void Core::Widgets::ToolbarMultiRenderWindow::OnHideXYZPlanes( wxCommandEvent& event )
{
	if ( GetMultiRenderWindow() != NULL )
	{
		GetMultiRenderWindowMITK()->EnableDisplayPlaneSubtree( !(GetToolToggled( wxID_HideXYZPlanes )) );
	}
}

void Core::Widgets::ToolbarMultiRenderWindow::SetMultiRenderWindow( Core::Widgets::RenderWindowBase* val )
{
	if ( GetMultiRenderWindow() != NULL )
	{
		GetMultiRenderWindow()->GetMetadataHolder( )->RemoveObserver( 
			this,
			&Core::Widgets::ToolbarMultiRenderWindow::UpdateState );
	}

	BaseWindow::SetMultiRenderWindow( val );

	if ( GetMultiRenderWindowMITK() != NULL )
	{
		GetMultiRenderWindow()->GetMetadataHolder( )->AddObserver( 
			this,
			&Core::Widgets::ToolbarMultiRenderWindow::UpdateState );
	}

	UpdateState( );
}

void Core::Widgets::ToolbarMultiRenderWindow::UpdateState()
{
	// Multi render window
	if ( GetMultiRenderWindowMITK() && GetMultiRenderWindow()->GetMetadataHolder( ).IsNotNull() )
	{
		ToggleTool(wxID_LockAxis, GetMultiRenderWindow()->GetMetadata( )->GetTagValue<bool>( "AxisLocked" ) );
		ToggleTool(wxID_HideXYZPlanes, !GetMultiRenderWindow()->GetMetadata( )->GetTagValue<bool>( "SlicesPlanesEnabled" ) );
	}

	UpdateLayoutState( );

	Enable( GetMultiRenderWindowMITK() );
}

void Core::Widgets::ToolbarMultiRenderWindow::UpdateLayoutState()
{
	// Multi render window
	if ( GetMultiRenderWindowMITK() && 
		GetMultiRenderWindow()->GetMetadataHolder( ).IsNotNull() )
	{
		// If it's single, check current window
		int layoutType = GetMultiRenderWindow()->GetMetadata( )->GetTagValue<int>( "LayoutType" );
		if ( layoutType == mitk::Single )
		{
			mitk::wxMitkMultiRenderWindow::WidgetListType widgets;
			widgets = GetMultiRenderWindowMITK( )->GetWidgets();
			mitk::wxMitkMultiRenderWindow::WidgetListType::iterator it;
			int count = 0;
			for ( it = widgets.begin() ; it != widgets.end() ; it++ )
			{
				if ( (*it)->IsShown() )
				{
					switch(count)
					{
					case mitk::ThreeD_Win:layoutType = mitk::Single3D;break;
					case mitk::X_Win:layoutType = mitk::Single2D_X;break;
					case mitk::Y_Win:layoutType = mitk::Single2D_Y;break;
					case mitk::Z_Win:layoutType = mitk::Single2D_Z;break;
					}
				}
				count++;
			}
		}

		// Get layout tool
		int layoutId = -1;
		switch ( layoutType )
		{
		case mitk::NoneLayout:layoutId = wxID_ViewLayoutDefault;break;
		case mitk::Default2x2:layoutId = wxID_ViewLayoutDefault;break;
		case mitk::Left2DRight3D:layoutId = wxID_ViewLayout2DViewsLeft;break;
		case mitk::Single3D:layoutId = wxID_ViewLayoutBig3DView;break;
		case mitk::Single2D_X:layoutId = wxID_ViewLayoutXView;break;
		case mitk::Single2D_Y:layoutId = wxID_ViewLayoutYView;break;
		case mitk::Single2D_Z:layoutId = wxID_ViewLayoutZView;break;
		case mitk::Only2D:layoutId = wxID_ViewLayoutOnly2DViews;break;
		case mitk::Only2D_XYZ:layoutId = wxID_ViewLayoutOnly2DViews_XYZ;break;
		case mitk::Default2x2_YXZ3D:layoutId = wxID_ViewLayoutDefault;break;
		case mitk::Default2x2_XYZ3D:layoutId = wxID_ViewLayoutDefault;break;
		default:break;
		}

		if ( layoutId != -1 )
		{
			int count = m_append_items.GetCount();
			for (int i = 0; i < count; ++i)
			{
				if ( m_customOverflowAppend[i].GetId( ) == layoutId )
				{
					SetToolBitmap( wxID_LAYOUT_TOOL, m_customOverflowAppend[i].GetBitmap( ) );
					Refresh( );
				}
			}
		}
	}

	Enable( GetMultiRenderWindowMITK() );
}

Core::Widgets::MultiRenderWindowMITK* Core::Widgets::ToolbarMultiRenderWindow::GetMultiRenderWindowMITK()
{
	return dynamic_cast<MultiRenderWindowMITK*> ( GetMultiRenderWindow() );
}
