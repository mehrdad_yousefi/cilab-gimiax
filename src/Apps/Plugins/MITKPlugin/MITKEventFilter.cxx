/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "MITKEventFilter.h"


MITKPlugin::EventFilter::EventFilter( ) 
{

	
}

MITKPlugin::EventFilter::~EventFilter()
{
}

int MITKPlugin::EventFilter::eventFilter( wxEvent& event )
{
	return mitk::wxMitkAbortEventFilter::eventFilter( event );
}

