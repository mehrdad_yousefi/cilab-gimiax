/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "itkDataEntityIO.h"
#include "itkMetaDataObject.h"

#include "coreDataEntity.h"

#include "vtkPointData.h"
#include "vtkDataArray.h"
#include "vtkImageData.h"
#include "vtkSmartPointer.h"

#include <string>

namespace itk {

DataEntityIO
::DataEntityIO()
{
}

DataEntityIO
::~DataEntityIO()
{
}

bool
DataEntityIO
::IsADataEntity(const char* filename)
{
  Core::DataEntity::Pointer dataEntity;
  int timeStep;
  dataEntity = this->FileNameToDataEntity(filename, timeStep);

  if (dataEntity.IsNotNull())
    {
    return true;
    }

  return false;
}

Core::DataEntity*
DataEntityIO
::FileNameToDataEntity(const char* filename, int &timeStep)
{
  std::string fname = filename;
  std::string::size_type loc;

  // check that the filename starts with "DataEntity:"
  loc = fname.find("DataEntity:");
  if (loc != std::string::npos && (loc == 0))
  {
    // now pull off the scene
  Core::DataEntity::Pointer dataEntity;
  Core::DataEntity* ptr;
  timeStep = 0;
  sscanf(filename, "DataEntity:%p:%d", &ptr, &timeStep);
  dataEntity = ptr;

      if (dataEntity.IsNull())
        {
        // not a valid scene pointer
        return 0;
        }
    return dataEntity;
  }

  return 0;
}


bool
DataEntityIO
::CanReadFile(const char* filename)
{
  return this->IsADataEntity(filename);
}

void
DataEntityIO
::ReadImageInformation()
{
  Core::DataEntity::Pointer dataEntity;

  int timeStep;
  dataEntity = this->FileNameToDataEntity( m_FileName.c_str(), timeStep );
  if (dataEntity.IsNull())
  {
    return;
  }

  blTagMap::Pointer tagMap = blTagMap::New( );
  dataEntity->GetTimeStep( timeStep )->GetData( tagMap );

  // VTK is only 3D
  this->SetNumberOfDimensions(3);

  blTag::Pointer tag;
  tag = tagMap->FindTagByName( "Dimensions" );
  if ( tag.IsNull() ) return ;
  std::vector<int> dimensions = tag->GetValueCasted< std::vector<int> >();
  for ( int i = 0 ; i < 3 ; i++ )
  {
    if ( i < dimensions.size() )
      this->SetDimensions(i, dimensions[ i ]);
	else
		this->SetDimensions( i, 1 );
  }

  m_Direction.resize(3);
  for ( unsigned i=0; i<GetNumberOfDimensions(); i++) 
  {
    for (unsigned j = 0; j < GetNumberOfDimensions(); j++)
    {
      if (i == j)
      {
        m_Direction[j][i] = 1.0;
      }
      else
      {
        m_Direction[j][i] = 0.0;
      }
    }
  }

  tag = tagMap->FindTagByName( "Origin" );
  if ( tag.IsNull() ) return ;
  m_Origin = tag->GetValueCasted< std::vector<double> >();
  if ( m_Origin.size() == 2 ) m_Origin.push_back( 0 );

  tag = tagMap->FindTagByName( "Spacing" );
  if ( tag.IsNull() ) return ;
  m_Spacing = tag->GetValueCasted< std::vector<double> >();
  if ( m_Spacing.size() == 2 ) m_Spacing.push_back( 1 );

  
  tag = tagMap->FindTagByName( "ScalarType" );
  if ( tag.IsNull() ) return ;
  std::string scalarType = tag->GetValueCasted<std::string>();

  tag = tagMap->FindTagByName( "NumberOfComponents" );
  if ( tag.IsNull() ) return ;
  int numComponents = tag->GetValueCasted<int>();
  if ( numComponents != 1 )
  {
    return ;
  }

  // PixelType
  if (this->GetNumberOfComponents() == 1)
  {
    this->SetPixelType(SCALAR);
  }

  // ComponentType
  if ( scalarType == "char" ) this->SetComponentType(CHAR);
  else if ( scalarType == "unsigned char" ) this->SetComponentType(UCHAR);
  else if ( scalarType == "int" ) this->SetComponentType(INT);
  else if ( scalarType == "unsigned int" ) this->SetComponentType(UINT);
  else if ( scalarType == "short" ) this->SetComponentType(SHORT);
  else if ( scalarType == "unsigned short" ) this->SetComponentType(USHORT);
  else if ( scalarType == "float" ) this->SetComponentType(FLOAT);
  else if ( scalarType == "double" ) this->SetComponentType(DOUBLE);
  else if ( scalarType == "long" ) this->SetComponentType(LONG);
  else if ( scalarType == "unsigned long" ) this->SetComponentType(ULONG);
  else 
  {
    itkWarningMacro("Unknown scalar type.");
    this->SetComponentType(UNKNOWNCOMPONENTTYPE);
  }

}

// Read from the MRML scene
void
DataEntityIO
::Read(void *buffer)
{
  Core::DataEntity::Pointer dataEntity;

  int timeStep;
  dataEntity = this->FileNameToDataEntity( m_FileName.c_str(), timeStep );
  if (dataEntity.IsNull())
  {
    return;
  }

  blTagMap::Pointer tagMap = blTagMap::New( );
  dataEntity->GetTimeStep( timeStep )->GetData( tagMap );

  blTag::Pointer tag;
  tag = tagMap->FindTagByName( "ScalarPointer" );
  if ( tag.IsNull() ) return ;
  void *scalarPointer = tag->GetValueCasted<void*>();

  // buffer is preallocated, memcpy the data
  memcpy(buffer, scalarPointer, this->GetImageSizeInBytes());
}

bool
DataEntityIO
::CanWriteFile(const char* filename)
{
  return this->IsADataEntity(filename);
}

// Write to the MRML scene

void
DataEntityIO
::WriteImageInformation()
{
}


void
DataEntityIO
::WriteImageInformation(Core::DataEntity::Pointer dataEntity, vtkImageData *img)
{

  // Fill in dimensions
  // VTK is only 3D, only copy the first 3 dimensions, fill in with
  // reasonable defaults for the rest
  if (this->GetNumberOfDimensions() > 3)
    {
    itkWarningMacro("Dimension of image is too high for VTK (Dimension = "
                    << this->GetNumberOfDimensions() << ")" );
    }
  
  int dim[3];
  double origin[3];
  double spacing[3];
  
  for (int i=0; (i < this->GetNumberOfDimensions()) && (i < 3); ++i)
    {
    dim[i] = this->GetDimensions(i);
    origin[i] = this->GetOrigin(i);
    spacing[i] = this->GetSpacing(i);
    }
  if (this->GetNumberOfDimensions() < 3)
    {
    // VTK is only 3D, fill in remaining dimensions
    for (int i = 0 ; i < 3; ++i)
      {
      dim[i] = 1;
      origin[i] = 0.0;
      spacing[i] = 1.0;
      }
    }
  img->SetDimensions(dim);
  img->SetOrigin(origin);
  img->SetSpacing(spacing);

  // ComponentType
  switch (this->GetComponentType())
    {
    case FLOAT: img->SetScalarTypeToFloat(); break;
    case DOUBLE: img->SetScalarTypeToDouble(); break;
    case INT: img->SetScalarTypeToInt(); break;
    case UINT: img->SetScalarTypeToUnsignedInt(); break;
    case SHORT: img->SetScalarTypeToShort(); break;
    case USHORT: img->SetScalarTypeToUnsignedShort(); break;
    case LONG: img->SetScalarTypeToLong(); break;
    case ULONG: img->SetScalarTypeToUnsignedLong(); break;
    case CHAR: img->SetScalarTypeToChar(); break;
    case UCHAR: img->SetScalarTypeToUnsignedChar(); break;
    default:
      // What should we do?
      itkWarningMacro("Unknown scalar type.");
      img->SetScalarTypeToShort();
      break;
    }

  // Scalar, Diffusion Weighted, or Vector image
  img->SetNumberOfScalarComponents(this->GetNumberOfComponents());

}

// Write to the MRML scene

void
DataEntityIO
::Write(const void *buffer)
{
  Core::DataEntity::Pointer dataEntity;
  int timeStep;
  dataEntity = this->FileNameToDataEntity( m_FileName.c_str(), timeStep );
  if (dataEntity.IsNotNull())
    {
    
    // Need to create a VTK ImageData to hang off the node if there is
    // not one already there
    //
  vtkSmartPointer<vtkImageData> img;
  dataEntity->GetProcessingData( img );
    if (!img)
      {
      img = vtkSmartPointer<vtkImageData>::New();
      dataEntity->SetTimeStep( img, timeStep );
      }

    // Configure the information on the node/image data
    //
    //
    this->WriteImageInformation(dataEntity, img);

    // Allocate the data, copy the data 
    //
    //
    // Everything but tensor images are passed in the scalars
    img->AllocateScalars();

    memcpy(img->GetScalarPointer(), buffer,
            img->GetPointData()->GetScalars()->GetNumberOfComponents() *
            img->GetPointData()->GetScalars()->GetNumberOfTuples() *
            img->GetPointData()->GetScalars()->GetDataTypeSize()
    );
  }
}


void
DataEntityIO
::PrintSelf(std::ostream& os, Indent indent) const
{
  Superclass::PrintSelf(os, indent);
 
}

} // end namespace itk
