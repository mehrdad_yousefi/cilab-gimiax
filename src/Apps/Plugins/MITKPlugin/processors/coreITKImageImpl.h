/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreITKImageImpl_H
#define _coreITKImageImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "itkImage.h"
#include <typeinfo>

namespace Core
{
/** 
ITKImageDataEntityBuilder inherits from abstract class DataEntityBuilder 
and enables building DataEntity from ITKImageData 

- "Origin": std::vector<double>
- "Spacing": std::vector<double>
- "Dimensions": std::vector<int>
- "Buffer": void*

\ingroup MITKPlugin
\author: Jakub Lyko
\date: 5.06.2008
*/
template <class ImageType>
class ITKImageImpl : public DataEntityImpl
{
public:

	coreDeclareSmartPointerClassMacro( Core::ITKImageImpl<ImageType>, DataEntityImpl)

	coreDefineSingleDataFactory2Types( Core::ITKImageImpl<ImageType>, typename ImageType::Pointer, ImageType*, ImageTypeId )

	//@{ 
	/// \name Interface
public:
	boost::any GetDataPtr() const;
	virtual bool IsValidType( const std::string &datatypename );
	virtual void SetDataPtr( boost::any val );
	virtual void SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem = gmCopyMemory );
	virtual void GetData( blTagMap::Pointer tagMap );

private:
	virtual void SetAnyData( boost::any val );
	virtual void ResetData( );
	//@}

protected:
	//!
	ITKImageImpl();
	//!
	~ITKImageImpl();

private:

	//!
	typename ImageType::Pointer m_Data;
	//!
	std::string m_ComponentType;
};

}

#include "coreITKImageImpl.txx"

#endif // _coreITKImageImpl_H
