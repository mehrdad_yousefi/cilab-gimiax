/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreVTKUnstructuredGridReader.h"
#include "coreVTKUnstructuredGridHolder.h"
#include "vtkXMLUnstructuredGridReader.h"
#include "vtkUnstructuredGridReader.h"
#include "vtkPluginFilterWatcher.h"

using namespace Core::IO;

//!
VTKUnstructuredGridReader::VTKUnstructuredGridReader(void) 
{
	m_ValidExtensionsList.push_back( ".vtk" );
	m_ValidExtensionsList.push_back( ".vti" );
	m_ValidExtensionsList.push_back( ".vol" );
	m_ValidExtensionsList.push_back( ".vtu" );
	m_ValidTypesList.push_back( VolumeMeshTypeId );
}

//!
VTKUnstructuredGridReader::~VTKUnstructuredGridReader(void)
{
}

//!
void VTKUnstructuredGridReader::ReadData( )
{
	ReadAllTimeSteps( Core::VolumeMeshTypeId );
}


boost::any VTKUnstructuredGridReader::ReadSingleTimeStep( int iTimeStep, const std::string &filename )
{
	Core::vtkUnstructuredGridPtr pVolume;

	// Configure callback
	vtkSmartPointer<vtkCallbackCommand> errorCallback;
	errorCallback = vtkSmartPointer<vtkCallbackCommand>::New();
	errorCallback->SetCallback( ErrorFunction );
	errorCallback->SetClientData( GetUpdateCallback().GetPointer() );

	// Configure callback
	vtkSmartPointer<vtkCallbackCommand> progressCallback;
	progressCallback = vtkSmartPointer<vtkCallbackCommand>::New();
	progressCallback->SetCallback(ProgressFunction);
	progressCallback->SetClientData( GetUpdateCallback().GetPointer() );

	// Try standard reader
	if ( GetExtension() == ".vtk" || GetExtension() == ".vti" || GetExtension() == ".vol" )
	{
		vtkSmartPointer<vtkUnstructuredGridReader> uReader;
		uReader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
		uReader->ReadAllScalarsOn();
		uReader->ReadAllVectorsOn();
		uReader->SetFileName(filename.c_str());
		uReader->AddObserver( vtkCommand::ErrorEvent, errorCallback );
		uReader->AddObserver( vtkCommand::ProgressEvent, progressCallback);
		uReader->Update();
		if (uReader->GetOutput()->GetNumberOfPoints())
		{
			pVolume = Core::vtkUnstructuredGridPtr::New();
			pVolume->ShallowCopy(uReader->GetOutput());			
		}

	}
	else if ( GetExtension() == ".vtu" )
	{
		vtkSmartPointer<vtkXMLUnstructuredGridReader> uReader;
		uReader = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();
		uReader->SetFileName(filename.c_str());
		uReader->AddObserver( vtkCommand::ErrorEvent, errorCallback );
		uReader->AddObserver( vtkCommand::ProgressEvent, progressCallback);
		uReader->Update();
		if (uReader->GetOutput()->GetNumberOfPoints())
		{
			pVolume = Core::vtkUnstructuredGridPtr::New();
			pVolume->ShallowCopy(uReader->GetOutput());			
		}
	}

	if ( pVolume.GetPointer() == NULL )
	{
		return boost::any( );
	}

	// Check the scale input data settings attribute
	bool scaleSmallInputData = false;
	if ( Core::Runtime::Kernel::GetApplicationSettings().IsNotNull() )
	{
		std::string scaleSmallInputDataStr;
		Core::Runtime::Kernel::GetApplicationSettings()->GetPluginProperty( "GIMIAS", "ScaleSmallInputData", scaleSmallInputDataStr );
		scaleSmallInputData = scaleSmallInputDataStr == "true" ? true : false;
	}

	double *bounds = pVolume->GetBounds();
	if ( scaleSmallInputData &&
		abs( bounds[1] - bounds[0] ) < 0.10 &&
		abs( bounds[3] - bounds[2] ) < 0.10 &&
		abs( bounds[5] - bounds[4] ) < 0.10 )
	{
		vtkUnstructuredGridPtr newProcessingDataPtr = vtkUnstructuredGridPtr::New();
		//mesh was given in meters!
		blVTKHelperTools::ScaleVolume( 
			pVolume, 
			newProcessingDataPtr, 
			1000 );	
		return newProcessingDataPtr;
	}

	return pVolume;
}


void VTKUnstructuredGridReader::ErrorFunction(
	vtkObject* caller, long unsigned int eventId, void* clientData, void* callData)
{
	vtkAlgorithm* filter = static_cast<vtkAlgorithm*>(caller);
	Core::UpdateCallback* callback = static_cast<Core::UpdateCallback*> (clientData);
	char* msg = static_cast<char*>(callData);
	callback->SetExceptionMessage( msg );
	callback->Modified();
}


void VTKUnstructuredGridReader::ProgressFunction(
	vtkObject* caller, long unsigned int eventId, void* clientData, void* callData)
{
	vtkAlgorithm* filter = static_cast<vtkAlgorithm*>(caller);
	Core::UpdateCallback* callback = static_cast<Core::UpdateCallback*> (clientData);
	callback->SetProgress( filter->GetProgress() );
	callback->Modified();

	filter->SetAbortExecute( callback->GetAbortProcessing() );
}


