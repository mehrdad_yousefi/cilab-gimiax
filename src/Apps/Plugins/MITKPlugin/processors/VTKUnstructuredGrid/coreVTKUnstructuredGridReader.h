/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreVTKUnstructuredGridReader_H
#define coreVTKUnstructuredGridReader_H

#include "coreBaseDataEntityReader.h"

namespace Core
{
namespace IO
{
/** 
A specialization of the DataEntityReader class for VTK PolyData objects.
If wraps the VTKPolyDataReader class to be used as a DataEntityReader 
that can be registered by the Core.

\ingroup MITKPlugin
\author Chiara Riccobene
\date  2008
*/
class PLUGIN_EXPORT VTKUnstructuredGridReader : public BaseDataEntityReader
{
public:
	coreDeclareSmartPointerClassMacro(
		Core::IO::VTKUnstructuredGridReader, 
		BaseDataEntityReader);

	void ReadData( );

	//!
	static void ErrorFunction(
		vtkObject* caller, long unsigned int eventId, void* clientData, void* callData);

	//!
	static void ProgressFunction(
		vtkObject* caller, long unsigned int eventId, void* clientData, void* callData);

protected:
	VTKUnstructuredGridReader(void);
	virtual ~VTKUnstructuredGridReader(void);

	//!
	virtual boost::any ReadSingleTimeStep( 
		int iTimeStep, 
		const std::string &filename );

private:
	coreDeclareNoCopyConstructors(VTKUnstructuredGridReader);
};

}
}

#endif
