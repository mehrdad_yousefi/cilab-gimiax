/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreVTKUnstructuredGridWriter.h"
#include "coreDataEntity.h"
#include <blShapeUtils.h>
#include "coreVTKUnstructuredGridHolder.h"
#include "coreDataEntityInfoHelper.h"
#include "coreException.h"

using namespace Core::IO;

//!
VTKUnstructuredGridWriter::VTKUnstructuredGridWriter(void)
{
	m_ValidExtensionsList.push_back( ".vtk" );
	m_ValidExtensionsList.push_back( ".vti" );
	m_ValidExtensionsList.push_back( ".vol" );
	m_ValidTypesList.push_back( VolumeMeshTypeId );
}

//!
VTKUnstructuredGridWriter::~VTKUnstructuredGridWriter(void)
{
}

//!
void VTKUnstructuredGridWriter::WriteData( )
{
	WriteAllTimeSteps( );
}

void Core::IO::VTKUnstructuredGridWriter::WriteSingleTimeStep( 
	const std::string& fileName, 
	Core::DataEntity::Pointer dataEntity, 
	int iTimeStep )
{
	vtkUnstructuredGridPtr volume = NULL;
	bool worked = dataEntity->GetProcessingData(volume, iTimeStep);
	if( !worked || volume == NULL )
	{
		throw Core::Exceptions::Exception(
			"VTKPolyDataWriter::WriteSingleTimeStep",
			"Input data is not of the correct type" );
	}

	bool warning = vtkObject::GetGlobalWarningDisplay();
	vtkObject::SetGlobalWarningDisplay( false );

	// Get the header
	blShapeUtils::ShapeUtils::SaveVolumeToFile(volume, fileName.c_str());

	vtkObject::SetGlobalWarningDisplay( warning );

}

