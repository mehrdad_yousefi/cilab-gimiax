/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreMITKCuboidImpl_H
#define _coreMITKCuboidImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "mitkSurface.h"

namespace Core{

/**
Set of DataEntityImpl
\author Xavi Planes
\date 06 Sept 2010
\ingroup MITKPlugin
*/
class PLUGIN_EXPORT MITKCuboidImpl : public Core::DataEntityImpl
{
public:
	coreDeclareSmartPointerClassMacro( Core::MITKCuboidImpl, DataEntityImpl )

	coreDefineMultipleDataFactory( Core::MITKCuboidImpl, mitk::Cuboid::Pointer, CuboidTypeId )


public:

	//@{ 
	/// \name Interface
	boost::any GetDataPtr() const;

private:
	void SetAnyData( boost::any val );
	void ResetData( );
	//@}


protected:
	//!
	MITKCuboidImpl( );

	//!
	virtual ~MITKCuboidImpl( );

protected:

	//!
	mitk::Cuboid::Pointer m_Data;
};

} // end namespace Core

#endif // _coreMITKCuboidImpl_H

