/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreITKVectorImageImpl.h"

void ITKVectorImageImplTest( )
{
	typedef itk::Image<char,3> imageType3D;
	typedef itk::Image<char,4> imageType4D;

	ITKVectorImageImpl< imageType4D, imageType3D >::Pointer image1;
	image1 = ITKVectorImageImpl< imageType4D, imageType3D >::New();

	ITKVectorImageImpl< imageType4D, imageType3D >::Pointer image2;
	image2 = ITKVectorImageImpl< imageType4D, imageType3D >::New();

	image1->DeepCopy( image2 );
}
