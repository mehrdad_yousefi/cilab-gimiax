/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreVtkPolyDataImpl_H
#define _coreVtkPolyDataImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "coreVTKPolyDataHolder.h"

namespace Core{

/**
Surface time steps for DataEntity

- "Points": std::vector<Point3D>*
- "SurfaceElements": std::vector<SurfaceElement3D>*

\author Xavi Planes
\date 03 nov 2009
\ingroup MITKPlugin
*/
class PLUGIN_EXPORT VtkPolyDataImpl : public DataEntityImpl
{
public:
	coreDeclareSmartPointerClassMacro( Core::VtkPolyDataImpl, DataEntityImpl )
	
	coreDefineSingleDataFactory2Types( Core::VtkPolyDataImpl, vtkPolyDataPtr, vtkPolyData*, SurfaceMeshTypeId )

	//@{ 
	/// \name Interface
public:
	void SetDataPtr( boost::any val );
	boost::any GetDataPtr() const;
	virtual void* GetVoidPtr( ) const;
	virtual void SetVoidPtr( void* ptr );
	virtual void CleanTemporalData( );

private:
	virtual void SetAnyData( boost::any val );
	virtual void SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem = gmCopyMemory );
	virtual void GetData( blTagMap::Pointer tagMap );
	bool IsValidType( const std::string &datatypename );
	virtual void ResetData( );
	//@}

private:
	//!
	VtkPolyDataImpl( );

	//!
	virtual ~VtkPolyDataImpl( );

private:

	vtkPolyDataPtr m_Data;

	//! Temporal Data
	std::vector<Point3D> m_Points;

	//! Temporal Data
	std::vector<SurfaceElement3D> m_SurfaceElements;
};

} // end namespace Core

#endif // _coreVtkPolyDataImpl_H

