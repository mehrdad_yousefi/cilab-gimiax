/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreSliceImageImpl_H
#define _coreSliceImageImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "blSliceImage.h"

namespace Core{

/**

Data entity for storing images defined by slices and not volumes

\author Chiara Riccobene
\date 11 Aug 2009
\ingroup MITKPlugin
*/

class PLUGIN_EXPORT SliceImageImpl : public DataEntityImpl
{
public:
	typedef blSliceImage::Pointer DataType;

	coreDeclareSmartPointerClassMacro( Core::SliceImageImpl, DataEntityImpl );

	coreDefineSingleDataFactory( Core::SliceImageImpl, DataType, ImageTypeId )

	//@{ 
	/// \name Interface
public:
	boost::any GetDataPtr() const;

private:
	virtual void GetData( blTagMap::Pointer tagMap );
	virtual void SetAnyData( boost::any val );
	virtual void ResetData( );
	//@}

protected:
	//!
	SliceImageImpl( );

	//!
	virtual ~SliceImageImpl();

	//! Not implemented
	SliceImageImpl(const Self&);

	//! Not implemented
   	void operator=(const Self&);

private:
	//!
	DataType m_Data;
};


}

#endif //_coreSliceImageImpl_H
