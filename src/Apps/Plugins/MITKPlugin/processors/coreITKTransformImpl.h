/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreITKTransformImpl_H
#define _coreITKTransformImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "itkTransformBase.h"

namespace Core
{
/** 

std::list<itk::TransformBase::Pointer>

\ingroup MITKPlugin
\author: Martin Bianculli
\date: 30 Oct 2008
*/

class PLUGIN_EXPORT ITKTransformImpl : public DataEntityImpl
{
public:

	typedef itk::TransformBase				TransformType;
	typedef TransformType::Pointer			TransformPointer;

	coreDeclareSmartPointerClassMacro( Core::ITKTransformImpl , DataEntityImpl)

	coreDefineSingleDataFactory( Core::ITKTransformImpl, TransformPointer, TransformId )

	//@{ 
	/// \name Interface
public:
	boost::any GetDataPtr() const;
	void SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem = gmCopyMemory );
	void GetData( blTagMap::Pointer tagMap );

private:
	virtual void SetAnyData( boost::any val );
	virtual void ResetData( );
	//@}


protected:

	//!
	ITKTransformImpl ();

	//!
	~ITKTransformImpl ();

private:
	//!
	TransformPointer m_Data;

};

}

#endif
