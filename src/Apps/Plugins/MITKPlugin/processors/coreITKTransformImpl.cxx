/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreITKTransformImpl.h"

using namespace Core;

ITKTransformImpl::ITKTransformImpl()
{
	ResetData();
}

ITKTransformImpl::~ITKTransformImpl()
{
}

boost::any Core::ITKTransformImpl::GetDataPtr() const
{
	return m_Data;
}

void Core::ITKTransformImpl::ResetData()
{
	m_Data = NULL;
}

void Core::ITKTransformImpl::SetAnyData( boost::any val )
{
	m_Data = boost::any_cast<TransformPointer> ( val );
}

void Core::ITKTransformImpl::SetData(
	blTagMap::Pointer tagMap, 
	ImportMemoryManagementType mem /*= gmCopyMemory */ )
{
	blTag::Pointer tag;

	tag = tagMap->FindTagByName( "DataPtr" );
	if ( tag.IsNotNull() )
	{
		TransformPointer srcData;
		if ( tag->GetValue<TransformPointer>( srcData ) )
		{
			m_Data = srcData;
			return;
		}
	}

	// Throw exception
	DataEntityImpl::SetData( tagMap, mem );
}

void Core::ITKTransformImpl::GetData( blTagMap::Pointer tagMap )
{
	tagMap->AddTag( "DataPtr", m_Data );
}
