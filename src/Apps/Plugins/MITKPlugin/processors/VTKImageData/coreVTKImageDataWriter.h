/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreVTKImageDataWriter_H
#define coreVTKImageDatariter_H

#include "coreBaseDataEntityWriter.h"

namespace Core
{
namespace IO
{
/** 
A specialization of the DataEntityWriter class for VTK ImageData images.
If wraps the vtkImageDataWriter class to be used as a DataEntityWriter 
that can be registered by the Core.

\ingroup MITKPlugin
\author Juan Antonio Moya & Mart�n Bianculli
\date 14 Apr 2008
*/
class PLUGIN_EXPORT VTKImageDataWriter : public BaseDataEntityWriter
{
public:
	coreDeclareSmartPointerClassMacro(
		Core::IO::VTKImageDataWriter, 
		BaseDataEntityWriter);

	void WriteData( );

protected:
	VTKImageDataWriter(void);
	virtual ~VTKImageDataWriter(void);

	//! Write Single data (image)
	void WriteSingleTimeStep( 
		const std::string& fileName, 
		Core::DataEntity::Pointer dataEntity,
		int iTimeStep );

	//!
	static void ProgressFunction(
		vtkObject* caller, 
		long unsigned int eventId, 
		void* clientData, 
		void* callData);

	//!
	void AddCallback( vtkAlgorithm* writer );

private:
	coreDeclareNoCopyConstructors(VTKImageDataWriter);
};

}
}

#endif
