/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreVTKImageDataReader.h"
#include "coreVTKImageDataHolder.h"
#include "coreStringHelper.h"
#include "coreDataTreeMITKHelper.h"

#include "vtkGenericDataObjectReader.h"
#include "vtkImageData.h"
#include "vtkStructuredPoints.h"

#include "blV3DImageFileReader.h"
#include "vtkXMLImageDataReader.h"
#include "vtkStructuredPointsReader.h"
#include "vtkImageReader2Factory.h"
#include "vtkImageReader2Collection.h"

#include "vtkCallbackCommand.h"

using namespace Core::IO;

VTKImageDataReader::VTKImageDataReader(void)
{
	// vtkStructuredPointsReader
	m_ValidExtensionsList.push_back( ".vtk" );
	// vtkXMLImageDataReader
	m_ValidExtensionsList.push_back( ".vti" );
	// blV3DImageFileReader
	m_ValidExtensionsList.push_back( ".v3d" );

	vtkSmartPointer<vtkImageReader2Collection> readers2;
	readers2 = vtkSmartPointer<vtkImageReader2Collection>::New();
	vtkImageReader2Factory::GetRegisteredReaders( readers2 );
	vtkCollectionSimpleIterator sit;
	readers2->InitTraversal(sit);
	vtkImageReader2* ret;
	while( ret = readers2->GetNextImageReader2(sit) )
	{
		m_ValidExtensionsList.push_back( ret->GetFileExtensions() );
	}

	m_ValidTypesList.push_back( ImageTypeId );
	m_ValidTypesList.push_back( ROITypeId );

	SetName( "VTK image reader" );
}

VTKImageDataReader::~VTKImageDataReader(void)
{
}

void VTKImageDataReader::ReadData()
{
	bool warning = vtkObject::GetGlobalWarningDisplay();
	vtkObject::SetGlobalWarningDisplay( false );

	Core::DataEntityType dataEntityType;
	std::string header;
	dataEntityType = Core::DataTreeMITKHelper::ReadDataEntityTypeDescription( m_Filenames[ 0 ] );
	if ( dataEntityType == UnknownTypeId )
	{
		dataEntityType = ImageTypeId;
	}

	vtkObject::SetGlobalWarningDisplay( warning );

	ReadAllTimeSteps( dataEntityType );
}


boost::any VTKImageDataReader::ReadSingleTimeStep( int iTimeStep, const std::string &filename )
{

	vtkSmartPointer<vtkImageData> pImage;
	
	bool warning = vtkObject::GetGlobalWarningDisplay();
	vtkObject::SetGlobalWarningDisplay( false );

	// Internal reader
	if ( GetExtension() == ".v3d" )
	{
		blV3DImageFileReader::Pointer reader = blV3DImageFileReader::New();
		reader->SetFileName(filename);
		reader->Update();
		pImage = vtkSmartPointer<vtkImageData>::New();
		pImage->DeepCopy( reader->GetVTKOutput() );
	}
	else if ( GetExtension() == ".vti" )
	{
		vtkSmartPointer<vtkXMLImageDataReader> reader;
		reader = vtkSmartPointer<vtkXMLImageDataReader>::New();
		if ( reader->CanReadFile( filename.c_str() ) )
		{
			AddCallback( reader );
			reader->SetFileName( filename.c_str() );
			reader->Update();
			pImage = reader->GetOutput();
		}
	}
	else if ( GetExtension() == ".vtk" )
	{
		vtkSmartPointer<vtkStructuredPointsReader> reader;
		reader = vtkSmartPointer<vtkStructuredPointsReader>::New();
		reader->SetFileName(filename.c_str());
		if ( reader->IsFileStructuredPoints( ) )
		{
			AddCallback( reader );
			reader->Update();
			pImage = reader->GetOutput();
		}
	}
	else 
	{
		vtkSmartPointer<vtkImageReader2> reader;
		reader = vtkImageReader2Factory::CreateImageReader2( filename.c_str() );
		if ( reader )
		{
			AddCallback( reader );
			reader->SetFileName(filename.c_str());
			reader->Update();
			//! Check that the .mhd image is a image 
			if ( reader->GetOutput()->GetPointData()->GetScalars()->GetNumberOfComponents() == 1 )
			{
				pImage = reader->GetOutput();
			}
		}
	}

	vtkObject::SetGlobalWarningDisplay( warning );

	// Return empty boost::any
	if ( pImage == NULL )
	{
		return boost::any( );
	}
	else
	{
		// Check the scale input data settings attribute
		bool scaleSmallInputData = false;
		if ( Core::Runtime::Kernel::GetApplicationSettings().IsNotNull() )
		{
			std::string scaleSmallInputDataStr;
			Core::Runtime::Kernel::GetApplicationSettings()->GetPluginProperty( "GIMIAS", "ScaleSmallInputData", scaleSmallInputDataStr );
			scaleSmallInputData = scaleSmallInputDataStr == "true" ? true : false;
		}

		// Increase size of image by 1000
		double *bounds =  pImage->GetBounds();
		if (scaleSmallInputData &&
			abs( bounds[1] - bounds[0] ) < 0.10 &&
			abs( bounds[3] - bounds[2] ) < 0.10 &&
			abs( bounds[5] - bounds[4] ) < 0.10 )
		{
			double spc[3];
			pImage->GetSpacing(spc);
			pImage->SetSpacing(spc[0]*1000,spc[1]*1000,spc[2]*1000);
			pImage->Update();
		}	
	}

	return pImage;
}

void Core::IO::VTKImageDataReader::AddCallback( vtkAlgorithm* reader )
{
	vtkSmartPointer<vtkCallbackCommand> progressCallback;
	progressCallback = vtkSmartPointer<vtkCallbackCommand>::New();
	progressCallback->SetCallback(ProgressFunction);
	progressCallback->SetClientData( GetUpdateCallback().GetPointer() );
	reader->AddObserver( vtkCommand::ProgressEvent, progressCallback);
}

void VTKImageDataReader::ProgressFunction(
	vtkObject* caller, long unsigned int eventId, void* clientData, void* callData)
{
	vtkAlgorithm* reader = static_cast<vtkAlgorithm*>(caller);
	Core::UpdateCallback* callback = static_cast<Core::UpdateCallback*> (clientData);
	callback->SetProgress( reader->GetProgress() );
	callback->Modified();

	if ( callback->GetAbortProcessing() )
	{
		reader->AbortExecuteOn();
	}
}

