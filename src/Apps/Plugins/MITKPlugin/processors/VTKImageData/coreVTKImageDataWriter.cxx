/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreVTKImageDataWriter.h"
#include "coreDataEntity.h"
#include "coreException.h"

#include <stdio.h>
#include <string.h>

#include "vtkStructuredPointsWriter.h"
#include "vtkSmartPointer.h"
#include "vtkXMLImageDataWriter.h"
#include "vtkCallbackCommand.h"
#include "coreVTKImageDataImpl.h"

#include "coreVTKImageDataHolder.h"

using namespace Core::IO;

//!
VTKImageDataWriter::VTKImageDataWriter(void)
{
	m_ValidExtensionsList.push_back( ".vtk" );
	m_ValidExtensionsList.push_back( ".vti" );
	m_ValidTypesList.push_back( ImageTypeId );
	m_ValidTypesList.push_back( ROITypeId );
}

//!
VTKImageDataWriter::~VTKImageDataWriter(void)
{
}

//!
void VTKImageDataWriter::WriteData()
{
	WriteAllTimeSteps( );
}

void Core::IO::VTKImageDataWriter::WriteSingleTimeStep( 
	const std::string& fileName, 
	Core::DataEntity::Pointer dataEntity, 
	int iTimeStep )
{
	Core::DataEntity::Pointer vtkDataEntity = Core::DataEntity::New( Core::ImageTypeId );
	vtkDataEntity->SwitchImplementation( typeid( vtkImageDataPtr ) ); 
	vtkDataEntity->Copy( dataEntity, Core::gmReferenceMemory );

	// Get the image
	Core::vtkImageDataPtr image;
	bool worked = vtkDataEntity->GetProcessingData( image, iTimeStep );

	if ( !worked )
	{
		VtkImageDataImpl::Pointer vtkImageDataTimeStep = VtkImageDataImpl::New( );
		vtkImageDataTimeStep->DeepCopy( dataEntity->GetTimeStep( iTimeStep ) );
		worked = CastAnyProcessingData( vtkImageDataTimeStep->GetDataPtr( ), image );
	}
	
	if( !worked || image == NULL )
	{
		throw Core::Exceptions::Exception(
			"VTKImageDataWriter::WriteSingleTimeStep",
			"Input data is not of the correct type" );
	}

	// Get extension
	std::string ext = itksys::SystemTools::GetFilenameLastExtension( fileName );
	std::string extension = itksys::SystemTools::LowerCase(ext);

	bool warning = vtkObject::GetGlobalWarningDisplay();
	vtkObject::SetGlobalWarningDisplay( false );

	// Check if directory exists
	std::string path = itksys::SystemTools::GetFilenamePath( fileName );
	if ( !path.empty( ) && !itksys::SystemTools::FileExists( path.c_str(), false ) )
	{
		throw Core::Exceptions::Exception(
			"VTKImageDataWriter::WriteSingleTimeStep",
			"Output directory doesn't exist" );
	}

	// Write to disk
	if ( extension == ".vtk" )
	{
		vtkSmartPointer<vtkStructuredPointsWriter> writer;
		writer = vtkStructuredPointsWriter::New();
		AddCallback( writer );
		writer->SetFileName(fileName.c_str());
		writer->SetInput(image);
		writer->SetFileTypeToBinary();
		writer->Update();
	}
	else if ( extension == ".vti" )
	{
		vtkSmartPointer<vtkXMLImageDataWriter> writer;
		writer = vtkXMLImageDataWriter::New();
		AddCallback( writer );
		writer->SetFileName(fileName.c_str());
		writer->SetInput(image);
		writer->Update();
	}

	vtkObject::SetGlobalWarningDisplay( warning );

}

void Core::IO::VTKImageDataWriter::AddCallback( vtkAlgorithm* writer )
{
	vtkSmartPointer<vtkCallbackCommand> progressCallback;
	progressCallback = vtkSmartPointer<vtkCallbackCommand>::New();
	progressCallback->SetCallback(ProgressFunction);
	progressCallback->SetClientData( GetUpdateCallback().GetPointer() );
	writer->AddObserver( vtkCommand::ProgressEvent, progressCallback);
}

void VTKImageDataWriter::ProgressFunction(
	vtkObject* caller, long unsigned int eventId, void* clientData, void* callData)
{
	vtkAlgorithm* writer = static_cast<vtkAlgorithm*>(caller);
	Core::UpdateCallback* callback = static_cast<Core::UpdateCallback*> (clientData);
	callback->SetProgress( writer->GetProgress() );
	callback->Modified();

	if ( callback->GetAbortProcessing() )
	{
		writer->AbortExecuteOn();
	}
}


