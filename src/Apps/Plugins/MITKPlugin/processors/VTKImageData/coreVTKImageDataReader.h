/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreVTKImageDataReader_H
#define coreVTKImageDataReader_H

#include "coreBaseDataEntityReader.h"

class vtkDataReader;

namespace Core
{
namespace IO
{
/** 
A specialization of the DataEntityReader class for VTK ImageData images.
If wraps the vtkImageReader class to be used as a DataEntityReader that 
can be registered by the Core.

\ingroup MITKPlugin
\author Juan Antonio Moya & Mart�n Bianculli
\date 14 Apr 2008
*/
class PLUGIN_EXPORT VTKImageDataReader : public BaseDataEntityReader
{
public:
	coreDeclareSmartPointerClassMacro(
		Core::IO::VTKImageDataReader, 
		BaseDataEntityReader);

	void ReadData();

protected:
	VTKImageDataReader(void);
	virtual ~VTKImageDataReader(void);

	//!
	virtual boost::any ReadSingleTimeStep( 
		int iTimeStep, 
		const std::string &filename );

	//!
	void AddCallback( vtkAlgorithm* reader );

	//!
	static void ProgressFunction(
		vtkObject* caller, 
		long unsigned int eventId, 
		void* clientData, 
		void* callData);

private:
	coreDeclareNoCopyConstructors(VTKImageDataReader);
};

}
}

#endif //coreVTKImageData
