/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef PropagateProcessor_H
#define PropagateProcessor_H

#include "coreBaseProcessor.h"

/**
\brief Propagate DataEntity over time

\ingroup gmProcessors
\author Xavi Planes
\date Aug 2012
*/

class PropagateProcessor : public Core::BaseProcessor
{
public:
	//!
	coreProcessor(PropagateProcessor, Core::BaseProcessor);

	//!
	PropagateProcessor( );

	//!
	~PropagateProcessor();

	//! Sets the range. Range must be less than (number of time steps / 2)
	void SetRange(int left, int right);

	//!
	void Update( );

private:

	//! Purposely not implemented
	PropagateProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );


private:
	//! Range left for propagation
	int m_left;
	//! Range right for propagation
	int m_right;
};

#endif //PropagateProcessor_H

