/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreMitkSignalImpl_H
#define _coreMitkSignalImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "blMitkSignal.h"

namespace Core{

/**
Set of DataEntityImpl
\author Xavi Planes
\date 06 Sept 2010
\ingroup MITKPlugin
*/
class PLUGIN_EXPORT MitkSignalImpl : public Core::DataEntityImpl
{
public:
	coreDeclareSmartPointerClassMacro( Core::MitkSignalImpl, DataEntityImpl )

	coreDefineMultipleDataFactory( Core::MitkSignalImpl, blMitk::Signal::Pointer, SignalTypeId )


public:

	//@{ 
	/// \name Interface
	boost::any GetDataPtr() const;
	size_t GetSize() const;
	void SetSize(size_t size, DataEntityImpl::Pointer data);
	DataEntityImpl::Pointer GetAt( size_t num );
	void SetAt( 
		size_t pos, 
		DataEntityImpl::Pointer data,
		ImportMemoryManagementType mem = gmCopyMemory );

private:
	void SetAnyData( boost::any val );
	virtual void ResetData( );
	//@}


protected:
	//!
	MitkSignalImpl( );

	//!
	virtual ~MitkSignalImpl( );

protected:

	//!
	blMitk::Signal::Pointer m_Data;
};

} // end namespace Core

#endif // _coreMitkSignalImpl_H

