/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreVTKImageDataImpl.h"

Core::VtkImageDataImpl::VtkImageDataImpl( ) 
{
	m_Data = vtkSmartPointer<vtkImageData>::New();
}

Core::VtkImageDataImpl::~VtkImageDataImpl()
{

}

boost::any Core::VtkImageDataImpl::GetDataPtr() const
{
	return m_Data;
}

void Core::VtkImageDataImpl::SetDataPtr( boost::any val )
{
	if ( val.type() == typeid( vtkSmartPointer<vtkImageData> ) )
	{
		SetAnyData( val );
	}
	else if ( val.type() == typeid( vtkImageData* ) )
	{
		vtkImageData* processingDataCasted;
		processingDataCasted = boost::any_cast<vtkImageData*> ( val );
		SetAnyData( vtkImageDataPtr( processingDataCasted ) );
	}
	else
	{
		throw Exceptions::DataTypeNotValidException( "VtkImageDataImpl::SetDataPtr" );
	}
}

bool Core::VtkImageDataImpl::IsValidType( const std::string &datatypename )
{
	return ( datatypename == typeid( vtkSmartPointer<vtkImageData> ).name( ) ||
			 datatypename == typeid( vtkImageData* ).name( ) );
}

void Core::VtkImageDataImpl::ResetData()
{
	m_Data = vtkSmartPointer<vtkImageData>::New();
}

void Core::VtkImageDataImpl::SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem )
{
	blTag::Pointer tag;
	
	tag = tagMap->FindTagByName( "DataPtr" );
	if ( tag.IsNotNull() )
	{
		DataType srcData;
		if ( tag->GetValue<DataType>( srcData ) && mem == gmCopyMemory )
		{
			m_Data->DeepCopy( srcData );
			return;
		}
	}

	std::vector<double> temp1;
	std::vector<int> temp2;

	tag = SafeFindTag( tagMap, "Origin" );
	temp1 = tag->GetValueCasted< std::vector<double> >();
	// VTK needs always 3 elements
	if ( temp1.size() == 2 ) temp1.push_back( 0 );
	m_Data->SetOrigin( &temp1[ 0 ] );

	tag = SafeFindTag( tagMap, "Spacing" );
	temp1 = tag->GetValueCasted< std::vector<double> >();
	// VTK needs always 3 elements and MITK needs always spacing > 0
	if ( temp1.size() == 2 ) temp1.push_back( 1 );
	m_Data->SetSpacing( &temp1[ 0 ] );

	tag = SafeFindTag( tagMap, "Dimensions" );
	temp2 = tag->GetValueCasted< std::vector<int> >();
	// VTK needs always 3 elements and 1 slice
	if ( temp2.size() == 2 ) temp2.push_back( 1 );
	m_Data->SetDimensions( &temp2[ 0 ] );

	tag = SafeFindTag( tagMap, "ScalarPointer" );
	void *scalarPointer = tag->GetValueCasted<void*>();

	tag = SafeFindTag( tagMap, "ScalarSizeInBytes" );
	size_t sizeInBytes = tag->GetValueCasted<size_t>();

	tag = SafeFindTag( tagMap, "NumberOfComponents" );
	int numComponents = tag->GetValueCasted<int>();

	tag = SafeFindTag( tagMap, "ScalarType" );
	std::string scalarType = tag->GetValueCasted<std::string>();
	if (scalarType == "float" )
	{
		m_Data->SetScalarTypeToFloat();
	}
	else if (scalarType == "double" )
	{
		m_Data->SetScalarTypeToDouble();
	}
	else if (scalarType == "int" )
	{
		m_Data->SetScalarTypeToInt();
	}
	else if (scalarType == "unsigned int" )
	{
		m_Data->SetScalarTypeToUnsignedInt();
	}
	else if (scalarType == "long" )
	{
		m_Data->SetScalarTypeToLong();
	}
	else if (scalarType == "unsigned long" )
	{
		m_Data->SetScalarTypeToUnsignedLong();
	}
	else if (scalarType == "short" )
	{
		m_Data->SetScalarTypeToShort();
	}
	else if (scalarType == "unsigned short" )
	{
		m_Data->SetScalarTypeToUnsignedShort();
	}
	else if (scalarType == "unsigned char" )
	{
		m_Data->SetScalarTypeToUnsignedChar();
	}
	else if (scalarType == "signed char" )
	{
		m_Data->SetScalarTypeToSignedChar();
	}
	else if (scalarType == "char" )
	{
		m_Data->SetScalarTypeToChar();
	}

	switch( mem )
	{
	case gmCopyMemory:
		m_Data->AllocateScalars();
		memcpy(m_Data->GetScalarPointer(), scalarPointer, sizeInBytes );
		SetMemoryOwner( "MITKPlugin" );
		break;
	case gmManageMemory:
		break;
	case gmReferenceMemory:
		vtkDataArray *scalars;
		scalars = vtkDataArray::CreateDataArray( m_Data->GetScalarType( ) );
		scalars->SetName( "scalars" );
		scalars->SetNumberOfComponents( numComponents );
		size_t size = sizeInBytes/scalars->GetDataTypeSize();
		scalars->SetVoidArray( scalarPointer, size, 1 );
		m_Data->GetPointData( )->SetScalars( scalars );
		SetMemoryOwner( "Unknown" );
		break;
	}

	// Xavi: I don't know why do we need this
	m_Data->SetNumberOfScalarComponents( numComponents );
}

void Core::VtkImageDataImpl::GetData( blTagMap::Pointer tagMap )
{
	std::vector<double> temp1;
	std::vector<int> temp2;

	temp1.assign( &m_Data->GetOrigin()[ 0 ], &m_Data->GetOrigin()[ 0 ] + 3 );
	tagMap->AddTag( "Origin", temp1 );
	temp1.assign( &m_Data->GetSpacing()[ 0 ], &m_Data->GetSpacing()[ 0 ] + 3 );
	tagMap->AddTag( "Spacing", temp1 );
	temp2.assign( &m_Data->GetDimensions()[ 0 ], &m_Data->GetDimensions()[ 0 ] + 3 );
	tagMap->AddTag( "Dimensions", temp2 );

	vtkDataArray *scalars = m_Data->GetPointData()->GetScalars();

	// Deformation field does not have scalar data
	// and calling the function GetScalarPointer() will create a new one
	if ( scalars != NULL )
	{
		tagMap->AddTag( "ScalarType", std::string( scalars->GetDataTypeAsString() ) );
		tagMap->AddTag( "ScalarPointer", scalars->GetVoidPointer( 0 ) );
		size_t sizeInBytes = scalars->GetNumberOfComponents() *
			scalars->GetNumberOfTuples() * scalars->GetDataTypeSize();
		tagMap->AddTag( "ScalarSizeInBytes", sizeInBytes );
		tagMap->AddTag( "NumberOfComponents", scalars->GetNumberOfComponents( ) );
	}

	vtkDataArray *vectors = m_Data->GetPointData( )->GetVectors( );
	if ( vectors != NULL )
	{
		tagMap->AddTag( "ScalarType", std::string( vectors->GetDataTypeAsString() ) );
		tagMap->AddTag( "ScalarPointer", vectors->GetVoidPointer( 0 ) );
		size_t sizeInBytes = vectors->GetNumberOfComponents() *
			vectors->GetNumberOfTuples() * vectors->GetDataTypeSize();
		tagMap->AddTag( "ScalarSizeInBytes", sizeInBytes );
		tagMap->AddTag( "NumberOfComponents", vectors->GetNumberOfComponents( ) );
	}

	tagMap->AddTag( "DataPtr", m_Data );
}

void* Core::VtkImageDataImpl::GetVoidPtr() const
{
	void* ptr; ptr = m_Data; return ptr;
}

void Core::VtkImageDataImpl::SetVoidPtr( void* ptr )
{
	m_Data = reinterpret_cast<vtkImageData*> ( ptr );
	SetMemoryOwner( "Unknown" );
}

void Core::VtkImageDataImpl::SetAnyData( boost::any val )
{
	m_Data = boost::any_cast<DataType> ( val );
	if ( m_Data == NULL )
	{
		throw Exceptions::DataPtrIsNullException( "VtkImageDataImpl::SetDataPtr" );
	}

	SetMemoryOwner( "Unknown" );
}

