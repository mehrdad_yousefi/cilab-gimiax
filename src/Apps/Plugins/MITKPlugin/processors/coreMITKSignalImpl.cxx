/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreMITKSignalImpl.h"
#include "coreSignalImpl.h"

using namespace Core;

/**
*/
Core::MitkSignalImpl::MitkSignalImpl( ) 
{
	ResetData( );
}

Core::MitkSignalImpl::~MitkSignalImpl()
{

}

boost::any Core::MitkSignalImpl::GetDataPtr() const
{
	return m_Data;
}

void Core::MitkSignalImpl::ResetData()
{
	m_Data = blMitk::Signal::New();
}

size_t Core::MitkSignalImpl::GetSize() const
{
	return m_Data->GetTimeSteps();
}

void Core::MitkSignalImpl::SetSize( size_t size, DataEntityImpl::Pointer data )
{
	m_Data->Expand( size );
}

void Core::MitkSignalImpl::SetAnyData( boost::any val )
{
	m_Data = boost::any_cast<blMitk::Signal::Pointer> ( val );
	if ( m_Data.GetPointer( ) == NULL )
	{
		throw Exceptions::DataPtrIsNullException( "MitkSignalImpl::SetDataPtr" );
	}

	SetMemoryOwner( "Unknown" );
}

DataEntityImpl::Pointer Core::MitkSignalImpl::GetAt( size_t num )
{
	blSignalCollective::Pointer signal = m_Data->GetSignal( );
	SignalImpl::Pointer signalImpl = SignalImpl::New( );
	signalImpl->SetDataPtr( signal );
	return signalImpl.GetPointer();
}

void Core::MitkSignalImpl::SetAt( 
	size_t pos, DataEntityImpl::Pointer data, ImportMemoryManagementType mem /*= gmCopyMemory */ )
{
	if ( data->GetDataPtr().type() != typeid( blSignalCollective::Pointer ) )
	{
		throw Exceptions::DataTypeNotValidException( "MitkSignalImpl::SetAt" );
	}

	blSignalCollective::Pointer signal;
	signal = boost::any_cast<blSignalCollective::Pointer>( data->GetDataPtr() );
	m_Data->SetSignal( signal );
}

