/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreDICOMRTFileReader_H
#define coreDICOMRTFileReader_H

#include "coreBaseDataEntityReader.h"
#include "dcmDataSet.h"

namespace Core
{
namespace IO
{

/** 
Read DICOMRT
 - RTDOSE
 - RTSTRUCT: The input is the reference image. Output is a vector of RTSTRUCT images

\ingroup MITKPlugin
\author Xavi Planes
\date May 2011
*/
class DICOMRTFileReader : public Core::IO::BaseDataEntityReader
{
public:
	coreDeclareSmartPointerClassMacro(
		Core::IO::DICOMRTFileReader, 
		BaseDataEntityReader);

	//!
	void Update();

	/** Read RTSTRUCT files and add information into DataSet
	\note cannot be read by gdcm::DicomDir because IsReadable( ) returns false
	\param [in] path path to the directory
	\param [in] dataSet input dataset, already parsed
	*/
	static void ReadRTSTRUCTDataSet( 
		const std::string &path,
		dcmAPI::DataSet::Pointer dataSet );

	//! Find a DataEntity with modality different of RTDOSE and RTSTRUCT
	static DataEntity::Pointer FindReferenceImage( 
		const std::vector<DataEntity::Pointer> &dataEntities );

	/** Bug in gdcm. When sopclassuid_used == "RT Dose Storage" the 
	x and y spacing is not correctly read*/
	static void ReadSpacing( 
		const std::string &filename, 
		std::vector< Core::vtkImageDataPtr > processingDataVector );

	/** Take a input vtk image and apply DOSE grid scaling factor
	\param [in] doseImage input RTDOSE image
	\param [in] doseFilePath Slice paths to read grid scaling
	\return RTDOSE with grid scaling multiplied
	*/
	static Core::vtkImageDataPtr ComputeDoseGridScaling( 
		Core::vtkImageDataPtr doseImage,
		std::string doseFilePath );

	//! Create volume image using ImagePositionPatient tag
	template<class PixelType> 
	static Core::vtkImageDataPtr CreateVolumeImageFromZeroZSpacing( 
		std::vector< std::string > sliceFilePaths, 
		bool reorient );

protected:
	DICOMRTFileReader( );
	virtual ~DICOMRTFileReader(void);

	//!
	virtual boost::any ReadSingleTimeStep( 
		int iTimeStep, 
		const std::string &filename );

	//!
	std::vector<Core::vtkImageDataPtr> CreateVTKImageFromRT( 
		Core::vtkImageDataPtr referenceImage, 
		const std::string &filepath,
		std::vector<std::string> &roiNames,
		std::vector<std::string> &roiColors );

};

}
}

#include "coreDICOMRTFileReader.txx"

#endif //coreDICOMRTFileReader_H
