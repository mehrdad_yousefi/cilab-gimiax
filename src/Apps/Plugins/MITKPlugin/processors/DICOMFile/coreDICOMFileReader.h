/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreDICOMFileReader_H
#define coreDICOMFileReader_H

#include "coreBaseDataEntityReader.h"
#include "blSliceImage.h"
#include "coreVTKImageDataHolder.h"

#include "dcmPatient.h"
#include "dcmSeries.h"
#include "dcmAbstractImageReader.h"

namespace Core
{
namespace IO
{

/** 
A specialization of the DataEntityReader class DICOM files.

Read DICOM files:
 - DICOMDIR
 - DICOM file
 - DICOM directory

\ingroup MITKPlugin
\author Xavi Planes
\date 15 June 2009
*/
class PLUGIN_EXPORT DICOMFileReader : public BaseDataEntityReader
{
public:
	coreDeclareSmartPointerTypesMacro(Core::IO::DICOMFileReader,BaseDataEntityReader)
	coreClassNameMacro(Core::IO::DICOMFileReader)

	enum INPUT_PATH_TYPE{
		INPUT_PATH_DICOMDIR,
		INPUT_PATH_DIRECTORY,
		INPUT_PATH_DCMFILE
	};

	//! Select data to load
	enum LOAD_FILTER_TYPE{
		LOAD_FILTER_NONE = 0,
		//! Multi slice per file
		LOAD_FILTER_MULTISLICE = 1,
		//! Single slice per file
		LOAD_FILTER_SINGLESLICE = 2,
		LOAD_FILTER_ALL = 3,
	};

	typedef std::vector<dcmAPI::Slice::Pointer> TimePointSlicesType;
	typedef std::vector<TimePointSlicesType> SeriesSlicesType;
	typedef std::vector<SeriesSlicesType> StudySlicesType;

	static Pointer New( const std::string &resourcePath );

	//!
	virtual ::itk::LightObject::Pointer CreateAnother(void) const;

	//!
	virtual void SetFileNames( const std::vector< std::string > &filenames );

	//!
	void Update();

	//!
	void RegisterReader( dcmAPI::AbstractImageReader::Pointer );

	//!
	void UnRegisterReader( dcmAPI::AbstractImageReader::Pointer );

	//!
	void ClearReaderList( );

	//!
	std::list<dcmAPI::AbstractImageReader::Pointer> GetRegisteredReaders( );

	//! Read DataSet
	virtual void ReadDataSet( const std::string &filename );

	//!
	dcmAPI::DataSet::Pointer GetDataSet() const;
	void SetDataSet(dcmAPI::DataSet::Pointer val);

	//!
	bool GetExportDICOMSlices() const;
	void SetExportDICOMSlices(bool val);

	//!
	bool GetImportTaggedMR() const;
	void SetImportTaggedMR(bool val);

	//!
	bool GetReorientData() const;
	void SetReorientData(bool val);

	//!
	void SetActiveTimeTag( dcmAPI::TagId );

	//!
	dcmAPI::TagId GetActiveTimeTag( );

	//!
	void SetSelectedStudyID(std::string val);
	bool IsSelectedStudyID(std::string val);

	//!
	void SetSelectedSerieID(std::string val);
	bool IsSelectedSerieID(std::string val);

	//!
	void SetSelectedSliceID(std::string val);
	bool IsSelectedSliceID(std::string val);

	//!
	void SetSelectedTimePointID(std::string val);
	bool IsSelectedTimePointID(std::string val);

	//!
	void ClearSelection( );

	//!
	INPUT_PATH_TYPE GetInputPathType() const;

	//!
	LOAD_FILTER_TYPE GetLoadFilter() const;
	void SetLoadFilter( LOAD_FILTER_TYPE filter );

	//!
	bool GetPostProcessData() const;
	void SetPostProcessData(bool val);

	//!
	float GetTimeTolerance() const;
	void SetTimeTolerance(float val);


protected:
	DICOMFileReader( const std::string &resourcePath );
	virtual ~DICOMFileReader(void);

	/** Read slices information and create DataEntities 
	Set each DataEntity as output
	*/
	virtual void ProcessPatients( );

	//!
	virtual void ProcessPatient( dcmAPI::Patient::Pointer patientData );

	//!
	virtual void ProcessStudy( dcmAPI::Study::Pointer studyData );

	//!
	virtual void ProcessSerie( dcmAPI::Series::Pointer seriesData );

	//!
	virtual void ProcessTimePoint( dcmAPI::TimePoint::Pointer timePointData );

	//! 
	void ImportDICOM( std::string filename );

	//! Use special readers for DICOM files that contain multiple slices
	bool ReadMultiSliceDICOMFile( const std::string &filename );

	//!
	void AddPatientData( );

	//!
	void AddStudyData( );

	//!
	void AddSeriesData(  );

	//!
	virtual boost::any ReadSingleTimeStep( 
		int iTimeStep, 
		const std::string &filename );

	//!
	virtual void AddTimePoint( 
		Core::DataEntity::Pointer dataEntity,
		const TimePointSlicesType &timePointSlices );

	//!
	virtual blSliceImage::Pointer CreateSliceImage(
		const TimePointSlicesType &timePointSlices );

	//!
	virtual Core::vtkImageDataPtr CreateVTKImage(
		const TimePointSlicesType &timePointSlices );

	//!
	Core::vtkImageDataPtr Extract2DVtkImageFrom3DVtkImage( 	
		Core::vtkImageDataPtr threeDInputImage, 
		int sliceNumber );

	//! Get first slice file path
	std::string GetFilePath( dcmAPI::Series::Pointer seriesData );

	//!
	dcmAPI::Slice::Pointer GetFirstSlice( );

private:
	coreDeclareNoCopyConstructors(DICOMFileReader);

	//!
	std::string m_ResourcePath;

	//!
	std::list<dcmAPI::AbstractImageReader::Pointer> m_ReaderList;

	//!
	dcmAPI::DataSet::Pointer m_DataSet;

	//!
	bool m_ExportDICOMSlices;

	//!
	bool m_ImportTaggedMR;

	//! Load MultiSlice data for visualization purposes
	LOAD_FILTER_TYPE m_LoadFilter;

	/** Post process DICOM data
	When reading RTDOSE, apply look up table and grid scaling
	Read RTSTRUCT only if this flag is on
	*/
	bool m_PostProcessData;

	//!
	dcmAPI::TagId m_ActiveTimeTag;

	//!
	std::vector<std::string> m_SelectedStudyID;

	//!
	std::vector<std::string> m_SelectedSerieID;

	//!
	std::vector<std::string> m_SelectedTimePointID;

	//!
	std::vector<std::string> m_SelectedSliceID;

	//!
	bool m_ReorientData;

	//!
	INPUT_PATH_TYPE m_InputPathType;

	//!
	std::vector<DataEntity::Pointer> m_TempDataEntities;

	//!
	DataEntity::Pointer m_CurrentDataEntity;

	//!
	dcmAPI::Patient::Pointer m_PatientData;

	//!
	dcmAPI::Study::Pointer m_StudyData;

	//!
	dcmAPI::Series::Pointer m_SeriesData;

	//! Tolerance on the time comparison when finding a timepoint (absolute).
	float m_TimeTolerance;
};

}
}

#endif //coreDICOMFileReader
