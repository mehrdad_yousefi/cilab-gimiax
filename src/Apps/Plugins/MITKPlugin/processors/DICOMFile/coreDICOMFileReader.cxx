/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDICOMFileReader.h"

#include "coreDICOMFileReaderHelper.h"
#include "coreDICOMRTFileReader.h"
#include "coreVTKImageDataHolder.h"
#include "coreDirectory.h"

#include "blImageUtils.h"
#include "blSignalCollective.h"

#include <dcmDataSetReader.h>
#include <dcmImageUtilities.h>
#include <dcmIOUtils.h>
#include <dcmConfigFileReader.h>

#include <itksys/SystemTools.hxx>

#include "vtkImageClip.h"

#include "dcmStandardImageReader.h"

using namespace Core::IO;


DICOMFileReader::DICOMFileReader( const std::string &resourcePath )
{
	m_ResourcePath = resourcePath;
	m_ValidExtensionsList.push_back( "" );
	m_ValidExtensionsList.push_back( ".dcm" );
	m_ValidTypesList.push_back( ImageTypeId );
	m_ExportDICOMSlices = false;
	m_ImportTaggedMR = false;
	m_ReorientData = true;
	m_LoadFilter = LOAD_FILTER_ALL;
	m_PostProcessData = true;
	m_DataSet = dcmAPI::DataSet::New( );
	m_TimeTolerance = 0;

	dcmAPI::AbstractImageReader::Pointer reader( new dcmAPI::StandardImageReader() );
	RegisterReader( reader );
}

DICOMFileReader::~DICOMFileReader(void)
{
}

void DICOMFileReader::Update()
{

	if ( m_Filenames.size( ) == 0 )
	{
		return;
	}

	ImportDICOM( m_Filenames[ 0 ] );

}

void Core::IO::DICOMFileReader::ImportDICOM( std::string filename )
{
	try
	{
		ReadDataSet( filename );

		ProcessPatients( );
	}
	catch(...)
	{
		m_TempDataEntities.clear();
		m_CurrentDataEntity = NULL;
		throw;
	}

	m_CurrentDataEntity = NULL;
	m_TempDataEntities.clear();
}

bool DICOMFileReader::ReadMultiSliceDICOMFile( const std::string &filename )
{
	// Only works with registered readers
	if ( m_ReaderList.empty() )
	{
		return false;
	}

	std::vector< float > timestamps;
	dcmAPI::EcgData ecgData;
	std::vector< Core::vtkImageDataPtr > processingDataVector;
	blSignalCollective::Pointer signalCollectiveECG;

	// If output has been already computed before
	for ( int i = 0 ; i < GetNumberOfInputs() ; i++ )
	{
		DataEntity::Pointer dataEntity = GetInputDataEntity( i );

		blTag::Pointer seriesTag;
		seriesTag = dataEntity->GetMetadata()->GetTag( "SeriesId" );
		std::string seriesId;
		if ( seriesTag.IsNotNull() )
		{
			seriesId = seriesTag->GetValueAsString();
		}

		if ( seriesId == m_SeriesData->GetTagAsText(dcmAPI::tags::SeriesId) )
		{
			try
			{
				GetProcessingData( i, processingDataVector );

				// Time
				for ( size_t i = 0 ; i < dataEntity->GetNumberOfTimeSteps() ; i++ )
				{
					timestamps.push_back( dataEntity->GetTimeAtTimeStep( i ) );
				}
			}
			catch(...)
			{
				GetProcessingData( i, signalCollectiveECG );
			}
		}
	}

	// Use specific registered readers
	if ( processingDataVector.empty( ) )
	{

		std::list<dcmAPI::AbstractImageReader::Pointer>::iterator it;
		for ( it = m_ReaderList.begin() ; it != m_ReaderList.end() ; it++ )
		{
			try
			{
				(*it)->SetFilename(filename);
				(*it)->Update();
				(*it)->GetVolumes(processingDataVector,timestamps);
				(*it)->GetEcgData(ecgData);

				if ( processingDataVector.size() )
				{
					break;
				}
			}
			catch(...)
			{}

		}
	}

	// RTDOSE
	if ( m_CurrentDataEntity->GetMetadata()->GetModality() == Core::RTDOSE )
	{
		DICOMRTFileReader::ReadSpacing( filename, processingDataVector );
	}

	// Create an image data entity
	if ( processingDataVector.size() )
	{
		if ( m_SelectedTimePointID.empty() )
		{
			m_CurrentDataEntity->AddTimeSteps( processingDataVector );
		}
		else 
		{
			for ( int i = 0 ; i < m_SelectedTimePointID.size() ; i++ )
			{
				int timepointNr = atoi( m_SelectedTimePointID[ i ].c_str() ) - 1;
				if ( m_SelectedSliceID.empty() )
				{
					m_CurrentDataEntity->AddTimeStep( processingDataVector[ timepointNr ] );
				}
				else
				{
					for ( int i = 0 ; i < m_SelectedSliceID.size() ; i++ )
					{
						int sliceNr = atoi( m_SelectedSliceID[ i ].c_str() );
						Core::vtkImageDataPtr vtkImage;
						vtkImage = Extract2DVtkImageFrom3DVtkImage(
							processingDataVector[ timepointNr ],
							sliceNr );
						m_CurrentDataEntity->AddTimeStep( vtkImage );

						if ( m_SelectedSliceID.size() == 1 )
							m_CurrentDataEntity->GetMetadata()->SetName( "Slice" + m_SelectedSliceID[ i ] );
					}
				}

				if ( m_SelectedTimePointID.size() == 1 )
					m_CurrentDataEntity->GetMetadata()->SetName( "Timepoint" + m_SelectedTimePointID[ i ] );
			}
		}
		m_TempDataEntities.push_back(m_CurrentDataEntity);
	}

	if( timestamps.size() > 0 )
		m_CurrentDataEntity->SetTimeForAllTimeSteps( timestamps );

	// Create the ECG data entity
	std::vector< double> cachedECGData;
	ecgData.GetData(cachedECGData);
	if ( cachedECGData.size() )
	{
		signalCollectiveECG = blSignalCollective::New();

		blSignal::Pointer signal = blSignal::BuildSignalFromEcgData( 
			cachedECGData, 
			ecgData.GetStartTime(), 
			ecgData.GetTimeIncrement(), 
			100 );

		signalCollectiveECG->SetNumberOfSignals(1);
		signalCollectiveECG->SetSignal( 0 , signal );

	}

	if ( signalCollectiveECG.IsNotNull() )
	{
		m_CurrentDataEntity = Core::DataEntity::New( SignalTypeId );

		AddPatientData( );
		AddStudyData( );
		AddSeriesData(  );

		m_CurrentDataEntity->AddTimeStep( signalCollectiveECG );
		m_CurrentDataEntity->GetMetadata( )->SetName( "ECG" );
		m_CurrentDataEntity->SetFather( m_TempDataEntities[ m_TempDataEntities.size( ) - 1 ] );
		m_TempDataEntities.push_back(m_CurrentDataEntity);
	}

	return !processingDataVector.empty();
}

void Core::IO::DICOMFileReader::AddPatientData(  )
{
	// Get patient information
	std::string patientName = m_PatientData->GetTagAsText(dcmAPI::tags::PatientName);
	std::string patientBirthDate = m_PatientData->GetTagAsText(dcmAPI::tags::PatientDate);
	std::string patientSex = m_PatientData->GetTagAsText(dcmAPI::tags::PatientSex);
	dcmAPI::TagId patientWeightTag(0x0010, 0x1030, "Patient's Weight");
	std::string patientWeight = m_PatientData->GetTagAsText(patientWeightTag);

	blTagMap::Pointer dePatientData = blTagMap::New( );
	dePatientData->AddTag( dcmAPI::tags::PatientName.m_description, patientName );
	dePatientData->AddTag( dcmAPI::tags::PatientSex.m_description, patientSex );
	if ( !patientWeight.empty( ) )
		dePatientData->AddTag( patientWeightTag.m_description, patientWeight );
	std::string formatedData = dcmAPI::CreateDateFromRawDcmTagDate( patientBirthDate );
	dePatientData->AddTag( dcmAPI::tags::PatientDate.m_description, formatedData );
	m_CurrentDataEntity->GetMetadata( )->AddTag( "Patient", dePatientData );

	m_CurrentDataEntity->GetMetadata()->SetName( "Patient" + patientName );

}

void Core::IO::DICOMFileReader::AddSeriesData( )
{

	std::string modality = m_SeriesData->GetTagAsText(dcmAPI::tags::Modality);
	std::string seriesId = m_SeriesData->GetTagAsText(dcmAPI::tags::SeriesId);

	DataEntityInfoHelper::ModalityType modalityType;
	modalityType = DataEntityInfoHelper::GetModalityType( modality );

	m_CurrentDataEntity->GetMetadata()->SetModality( modalityType );
	m_CurrentDataEntity->GetMetadata()->AddTag( "SeriesId", seriesId );

	// Set name
	if ( m_SelectedSerieID.size() == 1 )
		m_CurrentDataEntity->GetMetadata()->SetName( "Series" + 
			m_SeriesData->GetTagAsText(dcmAPI::tags::SeriesId) );
}

void Core::IO::DICOMFileReader::AddStudyData( )
{
	// Set name
	m_CurrentDataEntity->GetMetadata()->SetName( "Study" + 
		m_StudyData->GetTagAsText(dcmAPI::tags::StudyId) );
}

boost::any Core::IO::DICOMFileReader::ReadSingleTimeStep( int iTimeStep, const std::string &filename )
{
	return NULL;
}

::itk::LightObject::Pointer Core::IO::DICOMFileReader::CreateAnother( void ) const
{
	::itk::LightObject::Pointer smartPtr;
	smartPtr = DICOMFileReader::New( m_ResourcePath ).GetPointer();

	// Pass the registered readers
	DICOMFileReader* reader = dynamic_cast<DICOMFileReader*> ( smartPtr.GetPointer() );
	reader->ClearReaderList( );
	std::list<dcmAPI::AbstractImageReader::Pointer>::const_iterator it;
	for ( it = m_ReaderList.begin() ; it != m_ReaderList.end() ; it++ )
	{
		reader->RegisterReader( *it );
	}

	return smartPtr;
}

Core::IO::DICOMFileReader::Pointer Core::IO::DICOMFileReader::New( const std::string &resourcePath )
{
	Pointer smartPtr;
	DICOMFileReader *rawPtr = new DICOMFileReader( resourcePath );
	smartPtr = rawPtr;
	rawPtr->UnRegister();
	return smartPtr;
}

void Core::IO::DICOMFileReader::RegisterReader( dcmAPI::AbstractImageReader::Pointer reader )
{
	// Put specific readers at the beginning of the list
	// standard reader should be the last one
	m_ReaderList.push_front( reader );
}

void Core::IO::DICOMFileReader::UnRegisterReader( dcmAPI::AbstractImageReader::Pointer reader )
{
	std::list<dcmAPI::AbstractImageReader::Pointer>::iterator it;
	it = std::find( m_ReaderList.begin(), m_ReaderList.end(), reader );
	if ( it != m_ReaderList.end() )
	{
		m_ReaderList.erase( it );
	}
}

std::list<dcmAPI::AbstractImageReader::Pointer> Core::IO::DICOMFileReader::GetRegisteredReaders()
{
	return m_ReaderList;
}

void Core::IO::DICOMFileReader::ReadDataSet( const std::string &filename )
{
	if ( m_DataSet->GetPatientsVector( ).size() != 0 )
	{
		return;
	}

	dcmAPI::DataSetReader::Pointer reader = dcmAPI::DataSetReader::New( );
	std::list<dcmAPI::AbstractImageReader::Pointer>::iterator it;
	for ( it = m_ReaderList.begin() ; it != m_ReaderList.end() ; it++ )
	{
		reader->AddReader( *it );
	}
	reader->SetDataSet( m_DataSet );

	// Configure reader parameters
	reader->SetTimeTolerance( m_TimeTolerance );

	bool isDirectory = itksys::SystemTools::FileIsDirectory( filename.c_str() );
	if ( isDirectory )
	{
		dcmAPI::TagId tag = m_ActiveTimeTag; // (YM) usefull?
        if ( !tag.IsSet() )
		{
			const std::string DICOMTagsFile = m_ResourcePath + Core::IO::SlashChar + "DICOMTags.xml";
			reader->SetTagsConfigurationFilename( DICOMTagsFile );
		}
        else 
		{
			reader->SetTagsConfigurationFilename( "" );
			reader->SetTimeTag( tag );
			reader->SetTimeTagLocation( true );
        }

		reader->ReadDirectory( filename.c_str() );
		m_InputPathType = INPUT_PATH_DIRECTORY;
	}
	else
	{
		// Try DICOMDIR else try DCM File
		try
		{
			reader->ReadDicomDir( filename.c_str() );
			m_InputPathType = INPUT_PATH_DICOMDIR;
		}
		catch(...)
		{
			reader->ReadDcmFile( filename.c_str() );
			m_InputPathType = INPUT_PATH_DCMFILE;
		}
	}

	DICOMRTFileReader::ReadRTSTRUCTDataSet( filename, m_DataSet );

}
dcmAPI::DataSet::Pointer Core::IO::DICOMFileReader::GetDataSet() const
{
	return m_DataSet;
}

void Core::IO::DICOMFileReader::SetDataSet( dcmAPI::DataSet::Pointer val )
{
	m_DataSet = val;

}

bool Core::IO::DICOMFileReader::GetExportDICOMSlices() const
{
	return m_ExportDICOMSlices;
}

void Core::IO::DICOMFileReader::SetExportDICOMSlices( bool val )
{
	m_ExportDICOMSlices = val;
}

void Core::IO::DICOMFileReader::SetActiveTimeTag( dcmAPI::TagId tagId )
{
	m_ActiveTimeTag = tagId;
}

bool Core::IO::DICOMFileReader::GetImportTaggedMR() const
{
	return m_ImportTaggedMR;
}

void Core::IO::DICOMFileReader::SetImportTaggedMR( bool val )
{
	m_ImportTaggedMR = val;
}

void Core::IO::DICOMFileReader::SetSelectedSerieID( std::string val )
{
	m_SelectedSerieID.push_back( val );
}

void Core::IO::DICOMFileReader::SetSelectedSliceID( std::string val )
{
	m_SelectedSliceID.push_back( val );
}

void Core::IO::DICOMFileReader::SetSelectedTimePointID( std::string val )
{
	m_SelectedTimePointID.push_back( val );
}

Core::vtkImageDataPtr Core::IO::DICOMFileReader::Extract2DVtkImageFrom3DVtkImage(
	Core::vtkImageDataPtr threeDInputImage, 
	int sliceNumber)
{
	Core::vtkImageDataPtr twoDVtkImage = Core::vtkImageDataPtr::New();

	//IMAGE CLIP---------------------------------------------------------
	// this works with spacing < 0
	vtkSmartPointer < vtkImageClip > imageClip = vtkSmartPointer< vtkImageClip >::New() ;
	int dim[3];
	threeDInputImage->GetDimensions(dim);
	imageClip->SetInput(threeDInputImage);
	imageClip->ClipDataOn();
	imageClip->SetOutputWholeExtent(0, dim[0], 0, dim[1], sliceNumber-1, sliceNumber-1);
	imageClip->Update();
	twoDVtkImage->ShallowCopy(imageClip->GetOutput());
	//-------------------------------------------------------------------
	
	return twoDVtkImage;
}

void Core::IO::DICOMFileReader::ClearSelection()
{
	m_SelectedStudyID.clear();
	m_SelectedSerieID.clear();
	m_SelectedTimePointID.clear();
	m_SelectedSliceID.clear();
}
void Core::IO::DICOMFileReader::SetSelectedStudyID( std::string val )
{
	m_SelectedStudyID.push_back( val );
}

bool Core::IO::DICOMFileReader::IsSelectedStudyID( std::string val )
{
	std::vector<std::string>::iterator it;
	it = std::find( m_SelectedStudyID.begin( ), m_SelectedStudyID.end( ), val );
	return ( m_SelectedStudyID.empty() || it != m_SelectedStudyID.end() );
}
bool Core::IO::DICOMFileReader::IsSelectedSerieID( std::string val )
{
	std::vector<std::string>::iterator it;
	it = std::find( m_SelectedSerieID.begin( ), m_SelectedSerieID.end( ), val );
	return ( m_SelectedSerieID.empty() || it != m_SelectedSerieID.end() );
}

bool Core::IO::DICOMFileReader::IsSelectedSliceID( std::string val )
{
	std::vector<std::string>::iterator it;
	it = std::find( m_SelectedSliceID.begin( ), m_SelectedSliceID.end( ), val );
	return ( m_SelectedSliceID.empty() || it != m_SelectedSliceID.end( ) );
}

bool Core::IO::DICOMFileReader::IsSelectedTimePointID( std::string val )
{
	std::vector<std::string>::iterator it;
	it = std::find( m_SelectedTimePointID.begin( ), m_SelectedTimePointID.end( ), val );
	return  ( m_SelectedTimePointID.empty() || it != m_SelectedTimePointID.end( ) );
}

bool Core::IO::DICOMFileReader::GetReorientData() const
{
	return m_ReorientData;
}

void Core::IO::DICOMFileReader::SetReorientData( bool val )
{
	m_ReorientData = val;
}

void Core::IO::DICOMFileReader::SetFileNames( const std::vector< std::string > &filenames )
{
	BaseDataEntityReader::SetFileNames( filenames );
	m_DataSet->Clear( );
	SetNumberOfOutputs( 0 );
	SetNumberOfInputs( 0 );
	ClearSelection();
}

Core::IO::DICOMFileReader::INPUT_PATH_TYPE Core::IO::DICOMFileReader::GetInputPathType() const
{
	return m_InputPathType;
}

void Core::IO::DICOMFileReader::ProcessPatients( )
{

	// Read data from DataSet
	dcmAPI::PatientIdVectorPtr patiendIdVector = m_DataSet->GetPatientIds();
	for(unsigned i=0; i < patiendIdVector->size(); i++)
	{
		std::string currentDcmPatientId = patiendIdVector->at(i);
		m_PatientData = m_DataSet->GetPatient(currentDcmPatientId);

		ProcessPatient( m_PatientData );
	}

	// Add as output
	SetNumberOfOutputs( m_TempDataEntities.size() );
	for( size_t i = 0;  i<m_TempDataEntities.size(); i++)
	{
		SetOutputDataEntity( i, m_TempDataEntities[ i ] );
	}

}

void Core::IO::DICOMFileReader::ProcessPatient( dcmAPI::Patient::Pointer patientData )
{

	//reading studies
	dcmAPI::StudyIdVectorPtr studiesIdVector = patientData->StudyIds();
	for(unsigned i=0; i < studiesIdVector->size(); i++)
	{
		std::string currentDcmStudyId = studiesIdVector->at(i);
		m_StudyData = patientData->Study(currentDcmStudyId);

		ProcessStudy( m_StudyData );
	}

}

void Core::IO::DICOMFileReader::ProcessStudy( dcmAPI::Study::Pointer studyData )
{
	// Order slices for DICOMRT. CT should be read first beacuse RTSTRUCT
	// will use the dimensions and RTDOSE will use the spacing
	std::list<std::string> orderedSeriesId;
	dcmAPI::SeriesIdVectorPtr seriesIdVector = studyData->SeriesIds();
	for(unsigned i=0; i < seriesIdVector->size(); i++)
	{
		std::string currentDcmSeriesId = seriesIdVector->at(i);
		dcmAPI::Series::Pointer seriesData = studyData->Series(currentDcmSeriesId);

		if ( IsSelectedSerieID( currentDcmSeriesId )  )
		{
			std::string modality = seriesData->GetTagAsText(dcmAPI::tags::Modality);
			if ( modality == "RTSTRUCT" || modality == "RTDOSE" )
			{
				orderedSeriesId.push_back( currentDcmSeriesId );
			}
			else
			{
				orderedSeriesId.push_front( currentDcmSeriesId );
			}
		}
	}

	//reading series
	std::list<std::string>::iterator it;
	for( it = orderedSeriesId.begin(); it != orderedSeriesId.end() ; it++)
	{
		std::string currentDcmSeriesId = *it;
		m_SeriesData = studyData->Series(currentDcmSeriesId);

		if ( IsSelectedSerieID( currentDcmSeriesId ) )
		{
			ProcessSerie( m_SeriesData );
		}
	}

}

void Core::IO::DICOMFileReader::ProcessSerie( dcmAPI::Series::Pointer seriesData )
{

	m_CurrentDataEntity = Core::DataEntity::New( );
	m_CurrentDataEntity->SetType( Core::ImageTypeId );

	// Add dicom tags into meta data
	AddPatientData( );
	AddStudyData( );
	AddSeriesData(  );

	// Check if this serie is single slice per file or multiple slice per file
	dcmAPI::DataSet::StorageType storageType = dcmAPI::DataSet::SINGLE_SLICE_PER_FILE;
	dcmAPI::Slice::Pointer slice = GetFirstSlice( );
	if ( slice )
	{
		double numberOfFrames = slice->GetTagAsNumber( dcmAPI::tags::NumberOfFrames );
		if(numberOfFrames > 1)
		{
			storageType = dcmAPI::DataSet::MULTI_SLICE_PER_FILE;
		}
	}

	bool serieHasBeenLoaded = false;
	// RTSTRUCT is multi slice
	if ( seriesData->GetTagAsText( dcmAPI::tags::Modality ) == "RTSTRUCT" &&
		 ( m_LoadFilter & LOAD_FILTER_MULTISLICE ) &&
		 m_PostProcessData )
	{
		// Read RTSTRUCT
		DICOMRTFileReader::Pointer rtReader = DICOMRTFileReader::New( );
		std::vector<std::string> filenames;
		filenames.push_back( GetFilePath( seriesData ) );
		rtReader->SetFileNames( filenames );

		// Find reference image
		Core::DataEntity::Pointer reference = DICOMRTFileReader::FindReferenceImage( m_TempDataEntities );

		// Set input data entity to the reference image
		rtReader->SetInputDataEntity( 0, reference );
		rtReader->Update( );
		for ( unsigned i = 0 ; i < rtReader->GetNumberOfOutputs() ; i++ )
		{
			Core::DataEntity::Pointer dataEntity = rtReader->GetOutputDataEntity( i );
			blTagMap* currMetadata = m_CurrentDataEntity->GetMetadata();
			if ( dataEntity.IsNotNull( ) )
			{
				// Use m_CurrentDataEntity as base and add the meta data of RTSTRUCT
				blTagMap* rtStructMetadata = dataEntity->GetMetadata();
				currMetadata->AddTags( rtStructMetadata );
				dataEntity->GetMetadata( )->AddTags( currMetadata );
				dataEntity->SetFather( reference );
				m_TempDataEntities.push_back( dataEntity );
			}
		}

		serieHasBeenLoaded = true;
	}
	else if ( storageType == dcmAPI::DataSet::MULTI_SLICE_PER_FILE &&
		 GetInputPathType() == INPUT_PATH_DCMFILE &&
		 m_LoadFilter & LOAD_FILTER_MULTISLICE )
	{
		// Get file path from input file
		serieHasBeenLoaded = ReadMultiSliceDICOMFile( m_Filenames[ 0 ] );
	}
	else if( storageType == dcmAPI::DataSet::MULTI_SLICE_PER_FILE &&
		m_LoadFilter & LOAD_FILTER_MULTISLICE )
	{
		// Get file path from sliceData
		std::string filename = GetFilePath( seriesData );
		
		// Read data 
		serieHasBeenLoaded = ReadMultiSliceDICOMFile( filename );
	}
	else if ( storageType == dcmAPI::DataSet::SINGLE_SLICE_PER_FILE && 
			  m_LoadFilter & LOAD_FILTER_SINGLESLICE )
	{
		bool nonParalelSlices = false;

		// Check non parallel slices
		dcmAPI::TimePointIdVectorPtr timePointIdVector = seriesData->TimePointIds();
		for(unsigned i=0; i < timePointIdVector->size(); i++)
		{
			std::string currentDcmTimepointId = timePointIdVector->at(i);
			dcmAPI::TimePoint::Pointer timePointData = seriesData->TimePoint(currentDcmTimepointId);

			// If there's only one selected slice, is always non parallel
			if ( IsSelectedTimePointID( currentDcmTimepointId ) && currentDcmTimepointId.size() != 1 )
			{
				nonParalelSlices = nonParalelSlices || dcmAPI::IOUtils::TimepointContainsNonParallelSlices(timePointData);
			}
		}

		// Set DataEntity type
		if ( ( nonParalelSlices || m_ExportDICOMSlices ) &&
			m_ImportTaggedMR == false )
		{
			m_CurrentDataEntity->SetType( Core::SliceImageTypeId );
		}
		else
		{
			m_CurrentDataEntity->SetType( Core::ImageTypeId );
		}

		// Process Time point
		for(unsigned i=0; i < timePointIdVector->size(); i++)
		{
			std::string currentDcmTimepointId = timePointIdVector->at(i);
			dcmAPI::TimePoint::Pointer timePointData = seriesData->TimePoint(currentDcmTimepointId);

			if ( IsSelectedTimePointID( currentDcmTimepointId ) )
			{
				ProcessTimePoint( timePointData );
			}
		}

		if ( m_CurrentDataEntity->GetNumberOfTimeSteps() )
		{
			m_TempDataEntities.push_back( m_CurrentDataEntity );
		}
	}

	// RTDOSE. Apply this only when creating data entities
	if ( m_CurrentDataEntity->GetMetadata()->GetModality() == Core::RTDOSE &&
		 m_PostProcessData )
	{
		// Set rendering tags
		blTagMap::Pointer rendering = blTagMap::New( );
		rendering->AddTag( "blLookupTablesType", int(21) );
		rendering->AddTag( "opacity", float(0.5) );
		m_CurrentDataEntity->GetMetadata()->AddTag( "Rendering", rendering );

		// For each time step, use dose grid scaling factor
		for ( size_t i = 0 ; i < m_CurrentDataEntity->GetNumberOfTimeSteps() ; i++ )
		{
			Core::vtkImageDataPtr doseImage;
			m_CurrentDataEntity->GetProcessingData( doseImage, i );
			doseImage = Core::IO::DICOMRTFileReader::ComputeDoseGridScaling( doseImage, GetFilePath( seriesData ) );
			m_CurrentDataEntity->SetTimeStep( doseImage, i );
		}
	}

	// Read window level
	if ( slice != NULL )
	{
		std::string centerTag = slice->GetTagAsText(dcmAPI::tags::WindowCenter);
		std::string widthTag = slice->GetTagAsText(dcmAPI::tags::WindowWidth);
		
		double center[ 2 ] = {0,0};
		double width[ 2 ] = {0,0};
		dcmAPI::ImageUtilities::StringToDouble(centerTag.c_str(), center, 2);
		dcmAPI::ImageUtilities::StringToDouble(widthTag.c_str(), width, 2);
		// Example: Center: 40, -600, Width: 400, 1200
		if ( center[ 0 ] && width[ 0 ] && center[ 1 ] && width[ 1 ] )
		{
			blTagMap::Pointer rendering = blTagMap::New( );
			blTagMap::Pointer levelWin = blTagMap::New();
			levelWin->AddTag( "center", center[ 0 ] );
			levelWin->AddTag( "width", width[ 0 ] );
			levelWin->AddTag( "lowerWindowBound", center[ 1 ] - width[ 1 ] / 2.0 );
			levelWin->AddTag( "upperWindowBound", center[ 1 ] + width[ 1 ] / 2.0 );
			levelWin->AddTag( "rangeMin", std::min( center[ 0 ], center[ 1 ] ) - std::max( width[ 0 ], width[ 1 ] ) / 2.0 );
			levelWin->AddTag( "rangeMax", std::max( center[ 0 ], center[ 1 ] ) + std::max( width[ 0 ], width[ 1 ] ) / 2.0 );
			levelWin->AddTag( "defaultRangeMin", center[ 0 ] - width[ 0 ] / 2.0 );
			levelWin->AddTag( "defaultRangeMax", center[ 0 ] + width[ 0 ] / 2.0 );

			rendering->AddTag( "levelwindow", levelWin );
			m_CurrentDataEntity->GetMetadata()->AddTag( "Rendering", rendering );
		}

		// Image information
		gdcm::File gdcmFile;
		gdcmFile.SetFileName( slice->GetTagAsText( dcmAPI::tags::SliceFilePath ) );
		if ( gdcmFile.Load( ) )
		{
			blTagMap::Pointer imageGroup = blTagMap::New( );
			gdcm::DocEntry* entry = NULL;

			// AcquisitionTime
			entry = gdcmFile.GetDocEntry( dcmAPI::tags::AcquisitionTime.m_group, dcmAPI::tags::AcquisitionTime.m_element );
			if ( entry )
			{
				std::string acquisitionTime = gdcmFile.GetEntryForcedAsciiValue( entry->GetGroup(), entry->GetElement( ) );
				if ( !acquisitionTime.empty( ) )
					imageGroup->AddTag( "First " + dcmAPI::tags::AcquisitionTime.m_description, acquisitionTime );
			}
		
			// Patient weight
			blTagMap::Pointer dePatientData;
			dePatientData = m_CurrentDataEntity->GetMetadata()->GetTagValue<blTagMap::Pointer>( "Patient" );
			entry = gdcmFile.GetDocEntry( dcmAPI::tags::PatientWeight.m_group, dcmAPI::tags::PatientWeight.m_element );
			if ( entry && dePatientData.IsNotNull( ) )
			{
				std::string patientWeight = gdcmFile.GetEntryForcedAsciiValue( entry->GetGroup(), entry->GetElement( ) );
				if ( !patientWeight.empty( ) )
					dePatientData->AddTag( dcmAPI::tags::PatientWeight.m_description, patientWeight );
			}

			// PET
			// 	Radiopharmaceutical Information Sequence
			gdcm::SeqEntry *sqi = dynamic_cast<gdcm::SeqEntry *> ( gdcmFile.GetDocEntry( 0x0054, 0x0016 ) );
			if ( sqi )
			{
				blTagMap::Pointer radioGroup = blTagMap::New( );
				for(unsigned int pd = 0; pd < sqi->GetNumberOfSQItems() ; ++pd)
				{
			        gdcm::SQItem* item = sqi->GetSQItem( pd );

					// Fill all tags 
					gdcm::DocEntry* docEntry = item->GetFirstEntry();
					while(docEntry)
					{
						std::string value = item->GetEntryForcedAsciiValue( docEntry->GetGroup( ), docEntry->GetElement( ) );
						radioGroup->AddTag( docEntry->GetName(), value );
						docEntry = item->GetNextEntry();
					}
				}
				if ( radioGroup->GetLength( ) )
					imageGroup->AddTag( "Radiopharmaceutical Information Sequence", radioGroup );
			}

			if ( imageGroup->GetLength( ) )
				m_CurrentDataEntity->GetMetadata()->AddTag( "Image", imageGroup );
		}
	}
}

void Core::IO::DICOMFileReader::ProcessTimePoint( dcmAPI::TimePoint::Pointer timePointData )
{
	if ( m_SelectedTimePointID.size() == 1 )
		m_CurrentDataEntity->GetMetadata()->SetName( "Timepoint" + 
			timePointData->GetTagAsText(dcmAPI::tags::TimePointId) );


	// Get file path from slices
	TimePointSlicesType timePointSlices;
	dcmAPI::SliceIdVectorPtr sliceIdVector = timePointData->SliceIds();
	for(unsigned i=0; i < sliceIdVector->size(); i++)
	{
		std::string currentDcmSliceId = sliceIdVector->at(i);
		dcmAPI::Slice::Pointer sliceData = timePointData->Slice(currentDcmSliceId);

		if ( IsSelectedSliceID( currentDcmSliceId ) )
		{
			timePointSlices.push_back( sliceData );

			if ( m_SelectedSliceID.size() == 1 )
				m_CurrentDataEntity->GetMetadata()->SetName( 
					sliceData->GetTagAsText(dcmAPI::tags::SliceFileName) );
		}
	}

	AddTimePoint( m_CurrentDataEntity, timePointSlices );

}


void Core::IO::DICOMFileReader::AddTimePoint( 
	Core::DataEntity::Pointer dataEntity, 
	const TimePointSlicesType &timePointSlices )
{
	if ( timePointSlices.empty() )
	{
		return;
	}

	// Disable graphical warnings because this code can be executed in a working thread
	bool warning = ::itk::Object::GetGlobalWarningDisplay();
	::itk::Object::SetGlobalWarningDisplay( false );

	if ( dataEntity->GetType() == Core::SliceImageTypeId )
	{

		blSliceImage::Pointer sliceImage = CreateSliceImage( timePointSlices );
		dataEntity->AddTimeStep( sliceImage );
	}
	else if ( dataEntity->GetType() == Core::ImageTypeId )
	{
		Core::vtkImageDataPtr vtkImage = CreateVTKImage( timePointSlices );
		if ( vtkImage != NULL )
		{
			dataEntity->AddTimeStep( vtkImage );
		}
	}

	::itk::Object::SetGlobalWarningDisplay( warning );
}

blSliceImage::Pointer Core::IO::DICOMFileReader::CreateSliceImage( const TimePointSlicesType &timePointSlices )
{

	itk::ImageIOBase::IOComponentType pixelType;
	std::vector< std::string > sliceFilePaths;
	std::vector< std::string > slicePositions;
	std::vector< std::string > sliceOrientations;
	for ( unsigned j = 0 ; j < timePointSlices.size() ; j++ )
	{
		dcmAPI::Slice::Pointer sliceData = timePointSlices[ j ];
		pixelType = dcmAPI::ImageUtilities::ParsePixelType( sliceData->GetTagAsText( dcmAPI::tags::PxType ) );
		sliceFilePaths.push_back( sliceData->GetTagAsText( dcmAPI::tags::SliceFilePath ) );
		slicePositions.push_back(sliceData->GetTagAsText(dcmAPI::tags::ImagePositionPatient));
		sliceOrientations.push_back(sliceData->GetTagAsText(dcmAPI::tags::ImageOrientationPatient));
	}

	blSliceImage::Pointer sliceImage = blSliceImage::New();
	switch ( pixelType )
	{
		blVtkTemplateMacro( 
			( sliceImage = dcmAPI::ImageUtilities::ReadMultiSliceImageSliceFromFiles< PixelT >( sliceFilePaths ) ) 
			);
	}

	if ( sliceImage == NULL )
	{
		return NULL;
	}

	if (sliceFilePaths.size() != slicePositions.size() ||
		sliceOrientations.size() != slicePositions.size())
	{
		return NULL;
	}

	for (size_t id = 0; id < sliceFilePaths.size(); id++)
	{
		double orientation[6] = {0,0,0,0,0,0};
		double position[3] = {0,0,0};
		dcmAPI::ImageUtilities::StringToDouble(sliceOrientations.at(id).c_str(),orientation, 6);
		dcmAPI::ImageUtilities::StringToDouble(slicePositions.at(id).c_str(),position, 3);
		//fill sliceImage with the orientation and the position
		sliceImage->SetSliceOrientation( orientation, id);
		sliceImage->SetSlicePosition( position, id);
	}

	return sliceImage;
}

Core::vtkImageDataPtr Core::IO::DICOMFileReader::CreateVTKImage( 
	const TimePointSlicesType &timePointSlices )
{
	itk::ImageIOBase::IOComponentType pixelType;
	std::vector< std::string > sliceFilePaths;
	float zSpacing;
	
	// Read pixel, path and other tags from dicom file
	for ( unsigned j = 0 ; j < timePointSlices.size() ; j++ )
	{
		dcmAPI::Slice::Pointer sliceData = timePointSlices[ j ];
		pixelType = dcmAPI::ImageUtilities::ParsePixelType( sliceData->GetTagAsText( dcmAPI::tags::PxType ) );
		std::string slicePath = sliceData->GetTagAsText( dcmAPI::tags::SliceFilePath );
		sliceFilePaths.push_back( slicePath );

		// Read z spacing because some DICOM files from DICOMRT have spacing == 0
		if ( j == 0 )
		{
			gdcm::File gdcmFile;
			gdcmFile.SetFileName( slicePath );
			gdcmFile.Load();
			zSpacing = gdcmFile.GetZSpacing( );
		}
	}

	Core::vtkImageDataPtr vtkImage;
	try
	{
		if ( zSpacing == 0 && sliceFilePaths.size() >= 2 )
		{
			// Call template function to create the volume image using origin as z spacing
			switch ( pixelType )
			{
				blVtkTemplateMacro( 
					( vtkImage = Core::IO::DICOMRTFileReader::CreateVolumeImageFromZeroZSpacing<PixelT>( sliceFilePaths, m_ReorientData ) ) );
			}

		}
		else if ( timePointSlices.size( ) == 1 )
		{
			switch(pixelType)
			{
				blVtkTemplateMacro(
					( vtkImage = dcmAPI::ImageUtilities::Read2DVtkImageFromFile< PixelT>( 
					sliceFilePaths[ 0 ].c_str() ) ) 
					);
			}
		}
		else
		{
			// Create VTK image
			switch ( pixelType )
			{
				blVtkTemplateMacro( 
					( vtkImage = dcmAPI::ImageUtilities::ReadMultiSliceVtkImageFromFiles< PixelT, 3 >( 
					sliceFilePaths, m_ReorientData, m_ImportTaggedMR ) ) 
					);
			}
		}
	}
	catch(...)
	{

	}

	return vtkImage;
}

std::string Core::IO::DICOMFileReader::GetFilePath( dcmAPI::Series::Pointer seriesData )
{
	std::string filename;
	dcmAPI::TimePointIdVectorPtr timePointIdVector = seriesData->TimePointIds();
	if ( timePointIdVector->size() )
	{
		std::string currentDcmTimepointId = timePointIdVector->at(0);
		dcmAPI::TimePoint::Pointer timePointData = seriesData->TimePoint(currentDcmTimepointId);
		dcmAPI::SliceIdVectorPtr sliceIdVector = timePointData->SliceIds();
		if ( sliceIdVector->size() )
		{
			std::string currentDcmSliceId = sliceIdVector->at(0);
			dcmAPI::Slice::Pointer sliceData = timePointData->Slice(currentDcmSliceId);
			filename = sliceData->GetTagAsText( dcmAPI::tags::SliceFilePath );
		}
	}

	return filename;
}

dcmAPI::Slice::Pointer Core::IO::DICOMFileReader::GetFirstSlice()
{
	dcmAPI::TimePointIdVectorPtr timePointIdVector = m_SeriesData->TimePointIds();
	for(unsigned i=0; i < timePointIdVector->size(); i++)
	{
		dcmAPI::TimePoint::Pointer timePointData = m_SeriesData->TimePoint( timePointIdVector->at(i) );
		dcmAPI::SliceIdVectorPtr sliceIdVector = timePointData->SliceIds();
		if ( !sliceIdVector->empty( ) )
			return timePointData->Slice( (*sliceIdVector)[ 0 ] );
	}

	return dcmAPI::Slice::New();
}

Core::IO::DICOMFileReader::LOAD_FILTER_TYPE 
	Core::IO::DICOMFileReader::GetLoadFilter() const
{
	return m_LoadFilter;
}

void Core::IO::DICOMFileReader::SetLoadFilter( LOAD_FILTER_TYPE filter )
{
	m_LoadFilter = filter;
}

bool Core::IO::DICOMFileReader::GetPostProcessData() const
{
	return m_PostProcessData;
}

void Core::IO::DICOMFileReader::SetPostProcessData( bool val )
{
	m_PostProcessData = val;
}

void Core::IO::DICOMFileReader::ClearReaderList()
{
	m_ReaderList.clear();
}

float Core::IO::DICOMFileReader::GetTimeTolerance() const
{
	return m_TimeTolerance;
}

void Core::IO::DICOMFileReader::SetTimeTolerance( float val )
{
	m_TimeTolerance = val;
}
