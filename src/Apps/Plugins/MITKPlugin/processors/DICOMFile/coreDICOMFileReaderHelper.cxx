/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDICOMFileReaderHelper.h"

#undef BUILD_SHARED_LIBS

#include "coreDICOMFileReader.h"
#include "coreDirectory.h"
#include "coreStringHelper.h"

#include "dcmDataSetReader.h"
#include "dcmImageUtilities.h"
#include "dcmIOUtils.h"

#include <itksys/SystemTools.hxx>
#include <itksys/Directory.hxx>

#include "blXMLTagMapReader.h"
#include "blXMLTagMapWriter.h"

using namespace Core::IO;
using namespace dcmAPI;

dcmAPI::TagId Core::IO::DICOMFileReaderHelper::DetectDICOMTimeTag( 
	const std::string &filename,
	const std::string &resourcePath )
{
	dcmAPI::TagId emptyTag;

	// Find the first dicom file
	itksys::Directory dir;
	std::string folder = filename;
	// First two files are "." + ".."
	while ( dir.Load( folder.c_str() ) && dir.GetNumberOfFiles() > 2 )
	{
		folder = folder + "/" + dir.GetFile( 2 );
	}

	if ( itksys::SystemTools::FileIsDirectory( folder.c_str() ) )
	{
		return emptyTag;
	}

	// Load using gdcm
	std::string firstDICOMFile = folder;
	GdcmFilePtr gdcmFile(new gdcm::File());
	gdcmFile->SetFileName( firstDICOMFile );
	if(!gdcmFile->Load())
	{
		return emptyTag;
	}

	// Get manufacturer and modality tags
	GdcmValEntryPtr valEntry;
	gdcm::DocEntry* entry = NULL;
	std::string modality, manufacturer;

	entry = gdcmFile->GetDocEntry(tags::Manufacturer.m_group, tags::Manufacturer.m_element);
	if(entry != NULL)
	{
		valEntry.reset(new gdcm::ValEntry(entry));
		manufacturer = StringHelper::ToLowerCase( IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue()) );
	}

	entry = gdcmFile->GetDocEntry(tags::Modality.m_group, tags::Modality.m_element);
	if(entry != NULL)
	{
		valEntry.reset(new gdcm::ValEntry(entry));
		modality = IOUtils::CleanGdcmEntryStringValue(valEntry->GetValue());
	}

	// Read DICOM time tags from XML
	blXMLTagMapReader::Pointer reader = blXMLTagMapReader::New( );
	std::string DICOMTagsFile = resourcePath + Core::IO::SlashChar + "DICOMTags.xml";
	reader->SetFilename( DICOMTagsFile.c_str() );
	reader->Update();
	blTagMap::Pointer tagMap = reader->GetOutput( );
	if ( tagMap.IsNull() )
	{
		return emptyTag;
	}

	// Get corresponding time tag
	blTag::Pointer manufacturerTag = tagMap->FindTagByName( manufacturer );
	if ( manufacturerTag.IsNull( ) )
	{
		manufacturerTag = tagMap->FindTagByName( "Default" );
	}

	blTagMap::Pointer modalityTagMap;
	modalityTagMap = manufacturerTag->GetValueCasted<blTagMap::Pointer>();
	if ( modalityTagMap.IsNull() )
	{
		return emptyTag;
	}

	blTag::Pointer timeTag = modalityTagMap->GetTag( modality );
	if ( timeTag.IsNull() )
	{
		return emptyTag;
	}

	Tag dcmTimeTag( timeTag->GetValueAsString() );

	std::cout 
		<< "Manufacturer: " << manufacturer 
		<< ", Modality: " << modality 
		<< ", Time tag: " << dcmTimeTag.m_tagId << ":" << dcmTimeTag.m_value << std::endl;


	return dcmTimeTag.m_tagId;
}
