/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


//
#include "gdcmFile.h"
#include "gdcmSeqEntry.h"
#include "gdcmSQItem.h"
#include "gdcmValEntry.h"
//


template<class PixelType> 
Core::vtkImageDataPtr Core::IO::DICOMRTFileReader::CreateVolumeImageFromZeroZSpacing( 
	std::vector< std::string > sliceFilePaths, bool reorient )
{
	typedef itk::Image<PixelType,2> SliceImageType;
	typedef itk::Image<PixelType,3> VolumeImageType;

	// Read slices
	std::vector<typename SliceImageType::Pointer> sliceImageVector;
	std::vector<typename VolumeImageType::PointType > slicePositions;
	typename VolumeImageType::PointType firstOrigin;
	typename VolumeImageType::DirectionType direction;
	for ( size_t id=0; id < sliceFilePaths.size(); id++)
	{	
		// Read slice image
		typename SliceImageType::Pointer sliceImage;
		sliceImage = dcmAPI::ImageUtilities::ReadImageFromFile< SliceImageType >( sliceFilePaths.at(id).c_str() );
		sliceImageVector.push_back( sliceImage );

		// Read image position patient
		gdcm::File gdcmFile;
		gdcmFile.SetFileName( sliceFilePaths[ id ] );
		gdcmFile.Load();
		gdcm::ValEntry position = gdcmFile.GetDocEntry(
			dcmAPI::tags::ImagePositionPatient.m_group, dcmAPI::tags::ImagePositionPatient.m_element);
		std::string val = dcmAPI::IOUtils::CleanGdcmEntryStringValue(position.GetValue());
		double positionDouble[ 3 ];
		dcmAPI::ImageUtilities::StringToDouble( val.c_str(), positionDouble, 3 );
		typename VolumeImageType::PointType point;
		point[ 0 ] = positionDouble[ 0 ];
		point[ 1 ] = positionDouble[ 1 ];
		point[ 2 ] = positionDouble[ 2 ];
		
		slicePositions.push_back( point );

		if ( id == 0 )
		{
			firstOrigin = point;
			// Get image orientation
			gdcm::ValEntry orientation = gdcmFile.GetDocEntry( 
				dcmAPI::tags::ImageOrientationPatient.m_group, dcmAPI::tags::ImageOrientationPatient.m_element );
			std::string val = dcmAPI::IOUtils::CleanGdcmEntryStringValue(orientation.GetValue());
			double firstOrientation[ 6 ];
			dcmAPI::ImageUtilities::StringToDouble(val.c_str(),firstOrientation, 6);

			// Orientation
			vnl_vector<double> rowDirection(3), columnDirection(3);
			rowDirection[0] = firstOrientation[0];
			rowDirection[1] = firstOrientation[1];
			rowDirection[2] = firstOrientation[2];
			columnDirection[0] = firstOrientation[3];
			columnDirection[1] = firstOrientation[4];
			columnDirection[2] = firstOrientation[5];

			vnl_vector<double> sliceDirection = vnl_cross_3d(rowDirection, columnDirection);
			direction[ 0 ][ 0 ] = rowDirection[ 0 ];
			direction[ 0 ][ 1 ] = rowDirection[ 1 ];
			direction[ 0 ][ 2 ] = rowDirection[ 2 ];
			direction[ 1 ][ 0 ] = columnDirection[ 0 ];
			direction[ 1 ][ 1 ] = columnDirection[ 1 ];
			direction[ 1 ][ 2 ] = columnDirection[ 2 ];
			direction[ 2 ][ 0 ] = sliceDirection[ 0 ];
			direction[ 2 ][ 1 ] = sliceDirection[ 1 ];
			direction[ 2 ][ 2 ] = sliceDirection[ 2 ];
	
		}
	}

	// Get spacing and dimensions
	typename VolumeImageType::SpacingType spacing;
	typename VolumeImageType::IndexType index;
	typename VolumeImageType::SizeType size;
	typename VolumeImageType::RegionType region;
	typename SliceImageType::Pointer sliceImage0 = sliceImageVector[ 0 ];
	typename SliceImageType::Pointer sliceImage1 = sliceImageVector[ 1 ];
	spacing[ 0 ] = sliceImage0->GetSpacing( )[ 0 ];
	spacing[ 1 ] = sliceImage0->GetSpacing( )[ 0 ];
	spacing[ 2 ] = slicePositions[ 1 ][ 2 ] - slicePositions[ 0 ][ 2 ];
	index[ 0 ] = 0;
	index[ 1 ] = 0;
	index[ 2 ] = 0;
	size[ 0 ] = sliceImage0->GetLargestPossibleRegion().GetSize( 0 );
	size[ 1 ] = sliceImage0->GetLargestPossibleRegion().GetSize( 1 );
	size[ 2 ] = sliceFilePaths.size();
	region.SetIndex( index );
	region.SetSize( size );

	// Create volume image
	typename VolumeImageType::Pointer volumeImage;
	volumeImage = VolumeImageType::New( );
	volumeImage->SetSpacing( spacing );
	volumeImage->SetOrigin( firstOrigin );
	volumeImage->SetRegions( region );
	volumeImage->Allocate();
	volumeImage->SetDirection(direction);

	// Iterate over all slices
	for ( size_t id=0; id < sliceImageVector.size(); id++)
	{	
		// Slice region
		typename SliceImageType::RegionType sliceRegionToRequest;
		sliceRegionToRequest = sliceImageVector[ id ]->GetLargestPossibleRegion( );
		itk::ImageRegionConstIterator<SliceImageType> it (sliceImageVector[ id ], sliceRegionToRequest);

		// Volume region
		typename VolumeImageType::RegionType volumeRegionToRequest;
		volumeRegionToRequest = volumeImage->GetLargestPossibleRegion();
		volumeRegionToRequest.SetIndex( 2, id );
		volumeRegionToRequest.SetSize( 2, 1 );
		itk::ImageRegionIterator<VolumeImageType> ot (volumeImage, volumeRegionToRequest );
		while (!it.IsAtEnd())
		{
			ot.Set(it.Get());
			++it;
			++ot;
		}
	}
	
	// Orient image
	typename VolumeImageType::Pointer orientedImage = NULL;
	if ( reorient )
	{
		orientedImage = blITKImageUtils::ApplyOrientationToImage < VolumeImageType > ( volumeImage );	
	}
	else
	{
		orientedImage = volumeImage;
	}

	// Convert to VTK image
	Core::vtkImageDataPtr vtkImage;
	vtkImage = blImageUtils::ConvertFromItkImageToVtkImage < VolumeImageType > ( orientedImage );
	return vtkImage;
}
