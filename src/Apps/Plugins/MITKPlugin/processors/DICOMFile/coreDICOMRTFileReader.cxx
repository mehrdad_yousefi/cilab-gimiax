/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDICOMRTFileReader.h"
#include "coreDirectory.h"
#include "coreVTKPolyDataHolder.h"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itksys/SystemTools.hxx"

// here go your #includes

#include "itkPluginUtilities.h"

#include "gdcmUtil.h"
#include "gdcmGlobal.h"
#include "gdcmFile.h"
#include "gdcmSeqEntry.h"
#include "gdcmSQItem.h"
#include "gdcmValEntry.h"
#include "gdcmTS.h"
#include "gdcmDicomDir.h"

#include "itkOrientedImage.h"
#include "itkSpatialObjectPoint.h"
#include "itkPolygonSpatialObject.h"
#include "itkGroupSpatialObject.h"
#include "itkExtractImageFilter.h"
#include <itkSpatialObjectToImageFilter.h>

#include "blTextUtils.h"
#include "blImageUtils.h"

#include <limits.h>

#include "vtkPolyDataToImageStencil.h"
#include "vtkImageStencil.h"
#include "vtkLinearExtrusionFilter.h"
#include "vtkLassoStencilSource.h"
#include "vtkImageMathematics.h"
#include "vtkImageCast.h"

#include "dcmIOUtils.h"

const unsigned int Dimension = 3;
typedef int PixelType;
typedef itk::Image< PixelType, Dimension >   ImageType;
typedef itk::Image< PixelType, 2 >   ImageSliceType;
typedef itk::GroupSpatialObject<2> GroupType;
typedef itk::SpatialObjectToImageFilter<GroupType, ImageSliceType> SpatialObjectToImageFilterType;
typedef itk::PolygonSpatialObject<2> PolygonType;

Core::IO::DICOMRTFileReader::DICOMRTFileReader()
{
	SetNumberOfInputs( 1 );
	GetInputPort( 0 )->SetDataEntityType( Core::ImageTypeId );
	GetInputPort( 0 )->SetName( "Reference image" );
}

Core::IO::DICOMRTFileReader::~DICOMRTFileReader( void )
{

}

std::vector<Core::vtkImageDataPtr> Core::IO::DICOMRTFileReader::CreateVTKImageFromRT( 
	Core::vtkImageDataPtr referenceImage, 
	const std::string &filepath,
	std::vector<std::string> &roiNames,
	std::vector<std::string> &roiColors )
{
	// Create an empty image
	std::vector<Core::vtkImageDataPtr> imageVector;

	//Modality (0008,0060) �RTSTRUCT�
	//Patient ID (0010,0020)
	//Patient's Name (0010,0010)
	//SOP Instance UID (0008,0018)
	//Structure Set ROI Sequence (3006,0020)
	//>ROI Number (3006,0022)
	//>ROI Name (3006,0026)
	//ROI Contour Sequence (3006,0039)
	//>Referenced ROI Number (3006,0084)
	//>Contour Sequence (3006,0040)
	//>>Contour Geometric Type (3006,0042)
	//>>Number of Contour Points (3006,0046)
	//>>Contour Data (3006,0050)

	gdcm::File* gdcmFile = new gdcm::File();
	gdcmFile->SetFileName( filepath );
	gdcmFile->SetMaxSizeLoadEntry( INT_MAX );
	if(!gdcmFile->Load())
	{
		delete gdcmFile;
		return imageVector;
	}

	// Structure Set ROI Sequence (3006,0020)
	gdcm::SeqEntry *ssqi = gdcmFile->GetSeqEntry(0x3006,0x0020);
	if ( ssqi == NULL )
	{
		delete gdcmFile;
		return imageVector;
	}

	// ROI Contour Sequence (3006,0039)
	gdcm::SeqEntry *sqi = gdcmFile->GetSeqEntry(0x3006,0x0039);
	if ( sqi ==0 )
	{
		return imageVector;
	}

	//loop through structures
	gdcm::ValEntry* valEntry;
	for(unsigned int pd = 0; pd < sqi->GetNumberOfSQItems() ; ++pd)
	{
		Core::vtkImageDataPtr blackImage = Core::vtkImageDataPtr::New();
		blackImage->SetOrigin( referenceImage->GetOrigin() );
		blackImage->SetSpacing( referenceImage->GetSpacing() );
		blackImage->SetDimensions( referenceImage->GetDimensions() );
		blackImage->SetScalarTypeToUnsignedChar();
		blackImage->AllocateScalars();
		// fill the image with foreground voxels:
		unsigned char inval = 0;
		unsigned char outval = 1;
		vtkIdType count = blackImage->GetNumberOfPoints();
		for (vtkIdType i = 0; i < count; ++i)
		{
			blackImage->GetPointData()->GetScalars()->SetTuple1(i, inval);
		}

		gdcm::SQItem* item = sqi->GetSQItem( pd );

		// Referenced ROI Number (3006,0084)
		valEntry = item->GetValEntry( 0x3006, 0x0084 );
		std::string roiNumber;
		if( valEntry == 0 )
		{
			continue;
		}
		roiNumber = valEntry->GetValue();

		// ROI Display Color (3006,002a)
		valEntry = item->GetValEntry( 0x3006, 0x002a );
		if( valEntry == 0 )
		{
			continue;
		}
		std::string roiColor;
		roiColor = valEntry->GetValue();

		// find structure_set_roi_sequence corresponding to roi_contour_sequence (by comparing id numbers)
		unsigned int spd = 0;
		std::string sroiNumber;
		gdcm::SQItem* sitem = NULL;
		do
		{
			sitem = ssqi->GetSQItem( spd );
			// ROI Number (3006,0022)
			valEntry = sitem->GetValEntry( 0x3006, 0x0022 );
			if( valEntry != 0 )
			{
				sroiNumber = valEntry->GetValue();
			}
			spd++;

		} while( spd < ssqi->GetNumberOfSQItems( ) && sroiNumber != roiNumber );

		if ( sroiNumber != roiNumber )
		{
			continue;
		}

		// ROI Name (3006,0026)
		valEntry = sitem->GetValEntry( 0x3006,0x0026 );
		std::string str_currentOrgan;
		if (valEntry == 0 )
		{
			continue;
		}
		str_currentOrgan = valEntry->GetValue();
		roiNames.push_back( str_currentOrgan );
		roiColors.push_back( roiColor );

		// Contour Sequence (3006,0040)
		gdcm::SeqEntry *csq = item->GetSeqEntry( 0x3006, 0x0040 );
		if ( !csq || csq->GetNumberOfSQItems() == 0 )
		{
			imageVector.push_back( 0 );
			continue;
		}

		// For each slice, there could be multiple lasso
		std::list< vtkSmartPointer<vtkLassoStencilSource> > lassoVector;
		vtkIdType pointId = 0;
		for(unsigned int i = 0; i < csq->GetNumberOfSQItems() ; ++i)
		{
			gdcm::SQItem* citem = csq->GetSQItem( i );
			// Contour Data (3006,0050)
			valEntry = citem->GetValEntry( 0x3006,0x0050 );
			if ( valEntry == 0 )
			{
				imageVector.push_back( 0 );
				continue;
			}

			// Get all coordinates
			std::string position;
			position = valEntry->GetValue();
			std::list<std::string> posWords;
			blTextUtils::ParseLine( position, '\\', posWords );

			// Convert to pixels
			vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
			std::list<std::string>::iterator it=posWords.begin();
			while ( it != posWords.end() )
			{
				double point[ 3 ];
				point[0] = atof( it->c_str( ) );
				it++;
				point[1] = atof( it->c_str( ) );
				it++;
				point[2] = atof( it->c_str( ) );
				it++;

				points->InsertNextPoint( point[0] ,point[1], point[2] );
			}

			// Get current slice index
			double *point = points->GetPoint( 0 );
			int sliceIndex = floor( ( point[ 2 ] - blackImage->GetOrigin()[ 2 ] ) / blackImage->GetSpacing()[ 2 ] );

			// Find the last empty position for this sliceIndex
			std::list< vtkSmartPointer<vtkLassoStencilSource> >::iterator itLasso;
			itLasso = lassoVector.begin();
			while ( itLasso != lassoVector.end() )
			{
				if ( (*itLasso)->GetSlicePoints( sliceIndex ) == NULL )
				{
					break;
				}
				itLasso++;
			}

			// If no slot found -> add a new lasso
			if ( itLasso == lassoVector.end() )
			{
				vtkSmartPointer<vtkLassoStencilSource> lasso;
				lasso = vtkSmartPointer<vtkLassoStencilSource>::New();
				lasso->SetInformationInput( blackImage );
				lasso->SetSlicePoints( sliceIndex, points );
				lassoVector.push_back( lasso );
			}
			else
			{
				(*itLasso)->SetSlicePoints( sliceIndex, points );
			}
		}

		// Combine all images
		std::list< vtkSmartPointer<vtkLassoStencilSource> >::iterator itLasso;
		for ( itLasso = lassoVector.begin(); itLasso != lassoVector.end() ; itLasso++ )
		{
			(*itLasso)->Update();

			vtkSmartPointer<vtkImageStencil> imageStencil;
			imageStencil = vtkSmartPointer<vtkImageStencil>::New();
			imageStencil->SetInput( blackImage );
			imageStencil->SetStencil( (*itLasso)->GetOutput( ) );
			imageStencil->ReverseStencilOn();
			imageStencil->SetBackgroundValue( pd + 1 );
			imageStencil->Update();
			blackImage = imageStencil->GetOutput();
		}

		imageVector.push_back( blackImage );
	}

	return imageVector;
}


void Core::IO::DICOMRTFileReader::Update()
{
	if ( GetInputDataEntity( 0 ).IsNull() )
	{
		return;
	}

	Core::vtkImageDataPtr referenceImage;
	GetProcessingData( 0, referenceImage );
	if ( referenceImage == NULL )
	{
		return;
	}

	std::vector<Core::vtkImageDataPtr> outputImageVector;
	std::vector<std::string> roiNames;
	std::vector<std::string> roiColors;
	outputImageVector = CreateVTKImageFromRT( referenceImage, m_Filenames[ 0 ], roiNames, roiColors );

	SetNumberOfOutputs( outputImageVector.size() );
	for ( size_t i = 0 ; i < outputImageVector.size() ; i++ )
	{
		if ( outputImageVector[ i ] )
		{
			GetOutputPort( i )->SetDataEntityType( Core::ImageTypeId | Core::ROITypeId );
			UpdateOutput( i, outputImageVector[ i ], roiNames[ i ] );

			// Add color
			if ( GetOutputDataEntity( i ).IsNotNull() )
			{
				blTagMap::Pointer rendering = blTagMap::New( );
				std::string strColor = roiColors[ i ];

				// Convert from 0..255 to 0..1
				std::list<std::string> colorValues;
				blTextUtils::ParseLine( strColor, '\\', colorValues );
				double color[ 3 ];
				std::list<std::string>::iterator it;
				int count = 0;
				std::stringstream stream;
				for ( it = colorValues.begin() ; it != colorValues.end() ; it++ )
				{
					color[ count ] = atoi( it->c_str() );
					stream << color[ count ] / 256;
					if ( count != 2 )
					{
						stream << ",";
					}
					count++;
				}

				rendering->AddTag( "color", stream.str( ) );
				rendering->AddTag( "use color", true );
				GetOutputDataEntity( i )->GetMetadata()->AddTag( "Rendering", rendering );

				// Add "MultiROILevel" metadata
				blTagMap::Pointer roilevels = blTagMap::New();
				roilevels->AddTag( "struct", int( i + 1 ) );
				GetOutputDataEntity( i )->GetMetadata()->AddTag( "MultiROILevel", roilevels );
			}
		}
	}
}

boost::any Core::IO::DICOMRTFileReader::ReadSingleTimeStep(
	int iTimeStep, const std::string &filename )
{
	return NULL;
}

void Core::IO::DICOMRTFileReader::ReadRTSTRUCTDataSet( 
	const std::string &path, 
	dcmAPI::DataSet::Pointer dataSet )
{
	// Get all files in directory, recursively 
	Core::IO::Directory::Pointer directory;
	directory = Core::IO::Directory::New( );
	directory->SetDirNameFullPath( path );
	
	Core::IO::DirEntryFilter::Pointer filter = Core::IO::DirEntryFilter::New( );
	filter->SetMode( Core::IO::DirEntryFilter::FilesOnly );
	
	directory->SetFilter( filter );
	directory->SetRecursive( true );
	Core::IO::FileNameList files = directory->GetContents();

	// Get path of slices already read
	dcmAPI::DataSetReader::Pointer reader;
	reader = dcmAPI::DataSetReader::New( );
	reader->SetDataSet( dataSet );
	std::vector<std::string> pathsAlreadyRead;
	pathsAlreadyRead = reader->GetFilepathsOfSlices();

	// for each file, check if it's RTSTRUCT
	gdcm::VectDocument list;
	gdcm::File *f;
	Core::IO::FileNameList::iterator it;
	for( it = files.begin() ; it != files.end(); ++it )
	{
		if ( std::find( pathsAlreadyRead.begin( ), pathsAlreadyRead.end(), *it ) == pathsAlreadyRead.end() )
		{
			f = new gdcm::File( );
			f->SetFileName( it->c_str() );
			f->Load( );

			// Is RTSTRUCT?
			if ( f->GetEntryValue(0x0008,0x0060) == "RTSTRUCT" )
			{
				// Add the file to the chained list:
				list.push_back(f);
			}
		}
	}

	// Fill data set with new series/time point/slice
	dcmAPI::TagId PatientCurID(0x0010, 0x0011, "Patient ID");
	for(gdcm::VectDocument::iterator it=list.begin();
		it!=list.end();
		++it)
	{
		// Get the first patient
		dcmAPI::DataSet::PatientsVector patients = dataSet->GetPatientsVector();
		if ( patients.empty() )
		{
			break;
		}

		dcmAPI::Patient::Pointer patient = patients.at( 0 );
		dcmAPI::StudyIdVectorPtr studiesIdVector = patient->StudyIds();
		if ( studiesIdVector->empty() )
		{
			break;
		}
		dcmAPI::Study::Pointer study = patient->Study( studiesIdVector->at( 0 ) );

		dcmAPI::Series::Pointer series = dcmAPI::Series::New();

		// Get last series ID and add 1
		dcmAPI::SeriesIdVectorPtr seriesIdVector = study->SeriesIds();
		std::stringstream str;
		str << seriesIdVector->size() + 1;
		series->AddTag(dcmAPI::tags::SeriesId, str.str( ) );

		series->AddTag(dcmAPI::tags::Modality, (*it)->GetEntryValue(dcmAPI::tags::Modality.m_group, dcmAPI::tags::Modality.m_element ) );
		series->AddTag(dcmAPI::tags::SeriesInstanceUID, (*it)->GetEntryValue(dcmAPI::tags::SeriesInstanceUID.m_group, dcmAPI::tags::SeriesInstanceUID.m_element ) );
		series->AddTag(dcmAPI::tags::SeriesNumber, (*it)->GetEntryValue(dcmAPI::tags::SeriesNumber.m_group, dcmAPI::tags::SeriesNumber.m_element ) );
		series->AddTag(dcmAPI::tags::SeriesDescription, (*it)->GetEntryValue(dcmAPI::tags::SeriesDescription.m_group, dcmAPI::tags::SeriesDescription.m_element ) );
		study->AddSeries( series );

		dcmAPI::TimePoint::Pointer timePoint = dcmAPI::TimePoint::New();
		series->AddTimePoint( timePoint );

		dcmAPI::Slice::Pointer slice =dcmAPI:: Slice::New();
		timePoint->AddSlice( slice );
		slice->AddTag(dcmAPI::tags::SliceFilePath, (*it)->GetFileName( ) );
		slice->AddTag(dcmAPI::tags::ImageOrientationPatient, (*it)->GetEntryValue(dcmAPI::tags::ImageOrientationPatient.m_group, dcmAPI::tags::ImageOrientationPatient.m_element ) );

		delete dynamic_cast<gdcm::File *>(*it);
	}
}

Core::DataEntity::Pointer Core::IO::DICOMRTFileReader::FindReferenceImage( 
	const std::vector<Core::DataEntity::Pointer> &dataEntities )
{
	std::vector<DataEntity::Pointer>::const_iterator it = dataEntities.begin();
	Core::DataEntity::Pointer reference;
	while ( it != dataEntities.end() )
	{
		if ( (*it)->GetMetadata( )->GetModality( ) != Core::RTDOSE && 
			(*it)->GetMetadata( )->GetModality( ) != Core::RTSTRUCT )
		{
			reference = *it;
			break;
		}
		it++;
	}

	return reference;
}

void Core::IO::DICOMRTFileReader::ReadSpacing( 
	const std::string &filename, 
	std::vector< Core::vtkImageDataPtr > processingDataVector )
{
	if ( processingDataVector.empty() )
	{
		return;
	}

	// Bug in gdcm. When sopclassuid_used == "RT Dose Storage" the 
	// x and y spacing is not correctly read
	gdcm::File header;
	header.SetFileName( filename );
	header.Load();

	gdcm::TS *ts = gdcm::Global::GetTS();
	std::string sopclassuid_used;
	const std::string &mediastoragesopclassuid_str = header.GetEntryValue(0x0002,0x0002);
	const std::string &mediastoragesopclassuid = ts->GetValue(mediastoragesopclassuid_str);
	//D 0008|0016 [UI] [SOP Class UID]
	const std::string &sopclassuid_str = header.GetEntryValue(0x0008,0x0016);
	const std::string &sopclassuid = ts->GetValue(sopclassuid_str);
	if ( !( mediastoragesopclassuid == gdcm::GDCM_UNFOUND && sopclassuid == gdcm::GDCM_UNFOUND ) &&
		mediastoragesopclassuid == sopclassuid && 
		mediastoragesopclassuid == "RT Dose Storage" )
	{
		sopclassuid_used = mediastoragesopclassuid;
		const std::string &strSpacing = header.GetEntryValue(0x0028,0x0030);
		if ( strSpacing != gdcm::GDCM_UNFOUND )
		{
			double spacing[ 3 ];
			processingDataVector[ 0 ]->GetSpacing( spacing );
			float xspacing = 1.0;
			float yspacing = 1.0;
			int nbValues;
			if ( ( nbValues = sscanf( strSpacing.c_str(), 
				"%f \\%f ", &yspacing, &xspacing)) != 2 )
			{
				// if no values, xspacing is set to 1.0
				if ( nbValues == 0 )
					xspacing = 1.0;
				// if single value is found, xspacing is defaulted to yspacing
				if ( nbValues == 1 )
					xspacing = yspacing;

				if ( xspacing == 0.0 )
					xspacing = 1.0;
			}

			spacing[ 0 ] = xspacing;
			spacing[ 1 ] = yspacing;
			processingDataVector[ 0 ]->SetSpacing( spacing );
		}
	}
}

Core::vtkImageDataPtr Core::IO::DICOMRTFileReader::ComputeDoseGridScaling( 
	Core::vtkImageDataPtr doseImage, 
	std::string doseFilePath )
{
	gdcm::File gdcmFile;
	gdcmFile.SetFileName( doseFilePath );
	gdcmFile.Load();

	// Dose Grid Scaling
	gdcm::ValEntry dosefactor = gdcmFile.GetDocEntry(0x3004,0x000E);
	std::string valdose = dcmAPI::IOUtils::CleanGdcmEntryStringValue(dosefactor.GetValue());
	float doseGridScaling = atof(valdose.c_str());

	// Cast to float
	if ( doseImage->GetScalarType() != VTK_FLOAT )
	{
		vtkSmartPointer<vtkImageCast> cast = vtkSmartPointer<vtkImageCast>::New();
		cast->SetInput( doseImage );
		cast->SetOutputScalarTypeToFloat();
		cast->Update();
		doseImage = cast->GetOutput();
	}

	// Multiply by dose grid scaling
	vtkSmartPointer<vtkImageMathematics> mathematics;
	mathematics = vtkSmartPointer<vtkImageMathematics>::New( );
	mathematics->SetInput( doseImage );
	mathematics->SetOperationToMultiplyByK();
	mathematics->SetConstantK( doseGridScaling );
	mathematics->Update();
	return mathematics->GetOutput();
}
