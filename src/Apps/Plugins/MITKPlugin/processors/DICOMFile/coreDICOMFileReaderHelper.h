/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreDICOMFileReaderHelper_H
#define coreDICOMFileReaderHelper_H

#include "coreObject.h"
#include "dcmTypes.h"

namespace Core
{
namespace IO
{

/** 
Helper functions for reading DICOM

\ingroup MITKPlugin
\author Xavi Planes
\date Mar 2011
*/
class DICOMFileReaderHelper : public Core::Object
{
public:
	coreClassNameMacro(Core::IO::DICOMFileReaderHelper)

	/**
	Read the first DICOM file and check all tags to automatically set the 
	tag for time that allows to read correctly the series
	*/
	static dcmAPI::TagId DetectDICOMTimeTag( 
		const std::string &filename,
		const std::string &resourcePath );

};

}
}

#endif //coreDICOMFileReaderHelper
