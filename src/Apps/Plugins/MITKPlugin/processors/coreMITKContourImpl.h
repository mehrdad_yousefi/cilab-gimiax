/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreMitkContourImpl_H
#define _coreMitkContourImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "mitkContour.h"

namespace Core{

/**
Set of DataEntityImpl
\author Xavi Planes
\date 06 Sept 2010
\ingroup MITKPlugin
*/
class PLUGIN_EXPORT MitkContourImpl : public Core::DataEntityImpl
{
public:
	coreDeclareSmartPointerClassMacro( Core::MitkContourImpl, DataEntityImpl )

	coreDefineSingleDataFactory( MitkContourImpl, mitk::Contour::Pointer, ContourTypeId )


public:

	//@{ 
	/// \name Interface
	boost::any GetDataPtr() const;
	void GetData( blTagMap::Pointer tagMap );
	void SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem = gmCopyMemory );

private:
	void SetAnyData( boost::any val );
	virtual void ResetData( );
	void CleanTemporalData();
	//@}


protected:
	//!
	MitkContourImpl( );

	//!
	virtual ~MitkContourImpl( );

protected:

	//!
	mitk::Contour::Pointer m_Data;

	//! Temporal Data
	std::vector<Point3D> m_Points;

	//! Temporal Data
	std::vector<SurfaceElement3D> m_SurfaceElements;
};

} // end namespace Core

#endif // _coreMitkContourImpl_H

