/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreMITKCuboidImpl.h"

using namespace Core;

/**
*/
Core::MITKCuboidImpl::MITKCuboidImpl( ) 
{
	ResetData( );
}

Core::MITKCuboidImpl::~MITKCuboidImpl()
{

}

boost::any Core::MITKCuboidImpl::GetDataPtr() const
{
	return m_Data;
}

void Core::MITKCuboidImpl::ResetData()
{
	m_Data = mitk::Cuboid::New();
}

void Core::MITKCuboidImpl::SetAnyData( boost::any val )
{
	m_Data = boost::any_cast<mitk::Cuboid::Pointer> ( val );
}

