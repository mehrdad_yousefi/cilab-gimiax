/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreVTKProcessor_H
#define _coreVTKProcessor_H

#include "coreBaseProcessor.h"
#include "vtkSmartPointer.h"

namespace Core{

/**
Templated Processor for VTK Filters

\ingroup MITKPlugin
\author Xavi Planes
\date 19 April 2010
*/
template < class T >
class VTKProcessor : public Core::BaseProcessor
{
public:
	coreDeclareSmartPointerClassMacro(VTKProcessor<T>, Core::BaseProcessor);
	//!
	void Update();

	/// Return internal filter
	T* GetFilter( );

	//!
	int ComputeNumberOfTimeSteps( );

private:
	/**
	*/
	VTKProcessor( );

	//! Pass input data from input port to vtk filter
	template<class DataType>
	void PassInputData( int portNum, int timeStep );

	//!
	static void ErrorFunction(
		vtkObject* caller, 
		long unsigned int eventId, 
		void* clientData, 
		void* callData);

protected:
	//!
	vtkSmartPointer<T> m_Filter;
};

} // Core

#include "coreVTKProcessor.txx"

#endif //_coreVTKProcessor_H
