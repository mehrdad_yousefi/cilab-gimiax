/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreCroppingProcessor.h"
#include "vtkExtractPolyDataGeometry.h"
#include "vtkExtractGeometry.h"
#include "vtkBox.h"
#include "vtkExtractVOI.h"
#include "coreVTKUnstructuredGridHolder.h"
#include "coreVTKPolyDataHolder.h"

Core::CroppingProcessor::CroppingProcessor( )
{
	SetNumberOfInputs( 1 );
	GetInputPort( 0 )->SetName( "Input data" );
	GetInputPort( 0 )->SetDataEntityType( Core::ImageTypeId | Core::SurfaceMeshTypeId | Core::VolumeMeshTypeId );
	SetNumberOfOutputs( 1 );
	GetOutputPort( 0 )->SetName( "Output data" );

	SetName( "CroppingProcessor" );

	m_ProcessorDataHolder = BoundingBoxHolder::New( );
	m_ProcessorDataHolder->SetSubject(Core::BoundingBox::New());
}

Core::BoundingBoxHolder::Pointer 
Core::CroppingProcessor::GetProcessorDataHolder() const
{
	return m_ProcessorDataHolder;
}

void Core::CroppingProcessor::Update()
{
	if( !GetInputDataEntity( 0 ) )
		return;

	if( GetInputDataEntity( 0 )->IsImage() )
	{
		vtkImageDataPtr image;
		GetProcessingData( 0, image );

		itk::ImageRegion<3> roi;
		roi = GetProcessorDataHolder()->GetSubject()->GetBox( );
		int voi[ 6 ];
		voi[ 0 ] = roi.GetIndex( 0 );
		voi[ 1 ] = roi.GetIndex( 0 ) + roi.GetSize( 0 );
		voi[ 2 ] = roi.GetIndex( 1 );
		voi[ 3 ] = roi.GetIndex( 1 ) + roi.GetSize( 1 );
		voi[ 4 ] = roi.GetIndex( 2 );
		voi[ 5 ] = roi.GetIndex( 2 ) + roi.GetSize( 2 );

		vtkSmartPointer<vtkExtractVOI> extractVOI = vtkSmartPointer<vtkExtractVOI>::New();
		extractVOI->SetInput( image );
		extractVOI->SetVOI( voi );
		extractVOI->Update();

		UpdateOutput( 0, extractVOI->GetOutput( ), "Cropped image", true, 1, GetInputDataEntity( 0 ) );
	}
	else if( GetInputDataEntity( 0 )->IsSurfaceMesh() )
	{
		vtkPolyDataPtr surface;
		GetProcessingData( 0, surface );

		double bounds[ 6 ];
		GetProcessorDataHolder()->GetSubject()->GetBounds( bounds );

		vtkSmartPointer<vtkBox> box = vtkSmartPointer<vtkBox>::New( );
		box->SetBounds( bounds );

		vtkSmartPointer<vtkExtractPolyDataGeometry> extract;
		extract = vtkSmartPointer<vtkExtractPolyDataGeometry>::New( );
		extract->SetInput( surface );
		extract->SetImplicitFunction( box );
		extract->Update( );

		UpdateOutput( 0, extract->GetOutput( ), "Cropped surface", true, 1, GetInputDataEntity( 0 ) );
	}
	else if( GetInputDataEntity( 0 )->IsVolumeMesh() )
	{
		vtkUnstructuredGridPtr volumeMesh;
		GetProcessingData( 0, volumeMesh );

		double bounds[ 6 ];
		GetProcessorDataHolder()->GetSubject()->GetBounds( bounds );

		vtkSmartPointer<vtkBox> box = vtkSmartPointer<vtkBox>::New( );
		box->SetBounds( bounds );

		vtkSmartPointer<vtkExtractGeometry> extract;
		extract = vtkSmartPointer<vtkExtractGeometry>::New( );
		extract->SetInput( volumeMesh );
		extract->SetImplicitFunction( box );
		extract->Update( );

		UpdateOutput( 0, extract->GetOutput( ), "Cropped volume", true, 1, GetInputDataEntity( 0 ) );
	}
}

