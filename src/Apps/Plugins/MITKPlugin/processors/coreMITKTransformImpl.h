/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreMitkTransformImpl_H
#define _coreMitkTransformImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "mitkTransform.h"

namespace Core{

/**
Set of DataEntityImpl
\author Xavi Planes
\date 06 Sept 2010
\ingroup MITKPlugin
*/
class PLUGIN_EXPORT MitkTransformImpl : public Core::DataEntityImpl
{
public:
	coreDeclareSmartPointerClassMacro( Core::MitkTransformImpl, DataEntityImpl )

	coreDefineMultipleDataFactory( Core::MitkTransformImpl, mitk::Transform::Pointer, TransformId )


public:

	//@{ 
	/// \name Interface
	boost::any GetDataPtr() const;
	size_t GetSize() const;
	void SetSize(size_t size, DataEntityImpl::Pointer data);
	DataEntityImpl::Pointer GetAt( size_t num );
	void SetAt( 
		size_t pos, 
		DataEntityImpl::Pointer data,
		ImportMemoryManagementType mem = gmCopyMemory );

private:
	void SetAnyData( boost::any val );
	virtual void ResetData( );
	//@}


protected:
	//!
	MitkTransformImpl( );

	//!
	virtual ~MitkTransformImpl( );

protected:

	//!
	mitk::Transform::Pointer m_Data;
};

} // end namespace Core

#endif // _coreMitkTransformImpl_H

