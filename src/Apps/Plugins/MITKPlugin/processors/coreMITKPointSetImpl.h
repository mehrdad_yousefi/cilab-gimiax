/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreMITKPointSetImpl_H
#define _coreMITKPointSetImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "mitkPointSet.h"

namespace Core
{
/** 
\brief MITK PointSet data

\ingroup MITKPlugin
\author: Xavi Planes
\date: 09 Sept 2010
*/
class PLUGIN_EXPORT MitkPointSetImpl : public DataEntityImpl
{
public:
	coreDeclareSmartPointerClassMacro( Core::MitkPointSetImpl, DataEntityImpl )

	coreDefineMultipleDataFactory( Core::MitkPointSetImpl, mitk::PointSet::Pointer, PointSetTypeId )

	//@{ 
	/// \name Interface
public:
	boost::any GetDataPtr() const;
	size_t GetSize() const;
	void SetSize(size_t size, DataEntityImpl::Pointer data);
	void SetAt( 
		size_t pos, 
		DataEntityImpl::Pointer data,
		ImportMemoryManagementType mem = gmCopyMemory );

private:
	void SetAnyData( boost::any val );
	virtual void ResetData( );
	void SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem = gmCopyMemory );
	void GetData( blTagMap::Pointer tagMap );
	//@}

protected:
	//!
	MitkPointSetImpl();

	//!
	virtual ~MitkPointSetImpl();

private:
	//!
	mitk::PointSet::Pointer	m_Data;
};

}

#endif // _coreMITKPointSetImpl_H
