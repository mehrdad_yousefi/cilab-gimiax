/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreVTKPolyDataReader.h"
#include <blShapeUtils.h>
#include "coreVTKPolyDataHolder.h"
#include "coreDataTreeMITKHelper.h"
#include "vtkOBJReader.h"
#include "vtkPPolyDataNormals.h"

using namespace Core::IO;

//!
VTKPolyDataReader::VTKPolyDataReader(void) 
{
	m_ValidExtensionsList.push_back( ".vtk" );
	m_ValidExtensionsList.push_back( ".vti" );
	m_ValidExtensionsList.push_back( ".stl" );
	m_ValidExtensionsList.push_back( ".vtp" );
	m_ValidExtensionsList.push_back( ".obj" );
	m_ValidTypesList.push_back( SurfaceMeshTypeId );
	m_ValidTypesList.push_back( PointSetTypeId );
	m_ValidTypesList.push_back( ContourTypeId );
}

//!
VTKPolyDataReader::~VTKPolyDataReader(void)
{
}

//!
void VTKPolyDataReader::ReadData( )
{
	bool warning = vtkObject::GetGlobalWarningDisplay();
	vtkObject::SetGlobalWarningDisplay( false );

	// Read data entity description if any...
	m_DataEntityType = Core::DataTreeMITKHelper::ReadDataEntityTypeDescription( m_Filenames[ 0 ] );
	if ( m_DataEntityType == UnknownTypeId )
	{
		m_DataEntityType = SurfaceMeshTypeId;
	}

	vtkObject::SetGlobalWarningDisplay( warning );

	// build the data entity
	ReadAllTimeSteps( m_DataEntityType );
}

boost::any VTKPolyDataReader::ReadSingleTimeStep( int iTimeStep, const std::string &filename )
{
	bool warning = vtkObject::GetGlobalWarningDisplay();
	vtkObject::SetGlobalWarningDisplay( false );

	Core::vtkPolyDataPtr pSurface;
	pSurface.TakeReference(
		blShapeUtils::ShapeUtils::LoadShapeFromFile( filename.c_str() ) );

	// We should check extension otherwise it may crash 
	if ( pSurface.GetPointer() == NULL && GetExtension() == ".obj" )
	{
		vtkSmartPointer<vtkOBJReader> reader = vtkSmartPointer<vtkOBJReader>::New();
		reader->SetFileName( filename.c_str() );
		reader->Update( );
		if (reader->GetOutput())
		{
			pSurface = Core::vtkPolyDataPtr::New();
			pSurface->ShallowCopy(reader->GetOutput());			
		}
	}

	vtkObject::SetGlobalWarningDisplay( warning );

	if ( pSurface.GetPointer() == NULL )
	{
		return boost::any( );
	}

	// We need to compute normals for STL files because otherwise, the lighting interpolation
	// option doesn't work
	if ( GetExtension( ) == ".stl" )
	{
		vtkSmartPointer<vtkPPolyDataNormals> normals = vtkSmartPointer<vtkPPolyDataNormals>::New( );
		normals->SetInput( pSurface );
		normals->Update( );
		pSurface = normals->GetOutput( );
	}

	// Check the scale input data settings attribute
	bool scaleSmallInputData = false;
	if ( Core::Runtime::Kernel::GetApplicationSettings().IsNotNull() )
	{
		std::string scaleSmallInputDataStr;
		Core::Runtime::Kernel::GetApplicationSettings()->GetPluginProperty( "GIMIAS", "ScaleSmallInputData", scaleSmallInputDataStr );
		scaleSmallInputData = scaleSmallInputDataStr == "true" ? true : false;
	}

	double *bounds = pSurface->GetBounds();
	if ( m_DataEntityType == SurfaceMeshTypeId &&
		 scaleSmallInputData &&
		 abs( bounds[1] - bounds[0] ) < 0.10 &&
		 abs( bounds[3] - bounds[2] ) < 0.10 &&
		 abs( bounds[5] - bounds[4] ) < 0.10 )
	{
		vtkPolyDataPtr newProcessingDataPtr = vtkPolyDataPtr::New();
		//mesh was given in meters!
		blVTKHelperTools::ScaleShape( 
			pSurface, 
			newProcessingDataPtr, 
			1000 );	
		return newProcessingDataPtr;
	}

	return pSurface;
}
