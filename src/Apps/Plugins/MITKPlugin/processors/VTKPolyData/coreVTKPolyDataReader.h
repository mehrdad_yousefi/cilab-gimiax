/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreVTKPolyDataReader_H
#define coreVTKPolyDataReader_H

#include "coreBaseDataEntityReader.h"

namespace Core
{
namespace IO
{
/** 
A specialization of the DataEntityReader class for VTK PolyData objects.
If wraps the VTKPolyDataReader class to be used as a DataEntityReader that 
can be registered by the Core.

\ingroup MITKPlugin
\author Juan Antonio Moya
\date 20 Mar 2008
*/
class PLUGIN_EXPORT VTKPolyDataReader : public BaseDataEntityReader
{
public:
	coreDeclareSmartPointerClassMacro(
		Core::IO::VTKPolyDataReader, 
		BaseDataEntityReader);

	void ReadData( );

protected:
	VTKPolyDataReader(void);
	virtual ~VTKPolyDataReader(void);

	//!
	virtual boost::any ReadSingleTimeStep( 
		int iTimeStep, 
		const std::string &filename );

private:
	coreDeclareNoCopyConstructors(VTKPolyDataReader);

	//!
	Core::DataEntityType m_DataEntityType;
};

}
}

#endif
