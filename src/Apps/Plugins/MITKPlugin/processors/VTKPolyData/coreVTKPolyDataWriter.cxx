/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreVTKPolyDataWriter.h"
#include "coreDataEntity.h"
#include <blShapeUtils.h>
#include "coreVTKPolyDataHolder.h"
#include "coreDataEntityInfoHelper.h"
#include "coreException.h"
#include "coreVTKPolyDataImpl.h"
#include "vtkTriangleFilter.h"

using namespace Core::IO;

//!
VTKPolyDataWriter::VTKPolyDataWriter(void) 
{
	m_ValidExtensionsList.push_back( ".vtk" );
	m_ValidExtensionsList.push_back( ".vti" );
	m_ValidExtensionsList.push_back( ".stl" );
	m_ValidExtensionsList.push_back( ".vtp" );
	m_ValidTypesList.push_back( SurfaceMeshTypeId );
	m_ValidTypesList.push_back( PointSetTypeId );
	m_ValidTypesList.push_back( ContourTypeId );
}

//!
VTKPolyDataWriter::~VTKPolyDataWriter(void)
{
}

//!
void VTKPolyDataWriter::WriteData()
{
	WriteAllTimeSteps( );
}

void Core::IO::VTKPolyDataWriter::WriteSingleTimeStep( 
	const std::string& fileName, 
	Core::DataEntity::Pointer dataEntity, 
	int iTimeStep )
{
	vtkPolyDataPtr surface = NULL;

	bool worked = dataEntity->GetProcessingData( surface, iTimeStep );

	// Convert always to VTK to save
	if ( surface.GetPointer() == NULL )
	{
		VtkPolyDataImpl::Pointer polyDataTimeStep;
		polyDataTimeStep = VtkPolyDataImpl::New( );
		polyDataTimeStep->DeepCopy( dataEntity->GetTimeStep( iTimeStep ) );
		CastAnyProcessingData( polyDataTimeStep->GetDataPtr( ), surface );
	}

	if( !worked || surface == NULL )
	{
		throw Core::Exceptions::Exception(
			"VTKPolyDataWriter::WriteSingleTimeStep",
			"Input data is not of the correct type" );
	}

	// Check if directory exists
	std::string path = itksys::SystemTools::GetFilenamePath( fileName );
	if ( !path.empty( ) && !itksys::SystemTools::FileExists( path.c_str(), false ) )
	{
		throw Core::Exceptions::Exception(
			"VTKPolyDataWriter::WriteSingleTimeStep",
			"Output directory doesn't exist" );
	}

	bool warning = vtkObject::GetGlobalWarningDisplay();
	vtkObject::SetGlobalWarningDisplay( false );

	// Special case for STL
	if ( GetExtension( ) == ".stl" )
	{
		// STL can only write polys
		if ( surface->GetNumberOfPolys( ) == 0 )
		{
			vtkSmartPointer<vtkTriangleFilter> triangleFilter = vtkSmartPointer<vtkTriangleFilter>::New();
			triangleFilter->SetInput( surface );
			triangleFilter->Update();
			surface = triangleFilter->GetOutput( );
		}

	}

	// Get the header
	blShapeUtils::ShapeUtils::SaveShapeToFile(surface, fileName.c_str(), NULL );

	vtkObject::SetGlobalWarningDisplay( warning );
}

