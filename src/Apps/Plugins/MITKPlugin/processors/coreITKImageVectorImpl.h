/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreITKImageVectorImpl_H
#define _coreITKImageVectorImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "itkImage.h"
#include <typeinfo>

namespace Core
{
/** 
ITKImageDataEntityBuilder inherits from abstract class DataEntityBuilder 
and enables building DataEntity from ITKImageData 

- "Origin": std::vector<double>
- "Spacing": std::vector<double>
- "Dimensions": std::vector<int>
- "Buffer": void*

\ingroup MITKPlugin
\author Xavi Planes
\date June 2012
*/
template <class ImageType>
class ITKImageVectorImpl : public DataEntityImpl
{
public:

	coreDeclareSmartPointerClassMacro( Core::ITKImageVectorImpl<ImageType>, DataEntityImpl)

	coreDefineSingleDataFactory2Types( Core::ITKImageVectorImpl<ImageType>, typename ImageType::Pointer, ImageType*, ImageTypeId )

	//@{ 
	/// \name Interface
public:
	boost::any GetDataPtr() const;
	virtual bool IsValidType( const std::string &datatypename );
	virtual void SetDataPtr( boost::any val );
	virtual void SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem = gmCopyMemory );
	virtual void GetData( blTagMap::Pointer tagMap );

private:
	virtual void SetAnyData( boost::any val );
	virtual void ResetData( );
	//@}

protected:
	//!
	ITKImageVectorImpl();
	//!
	~ITKImageVectorImpl();

private:

	//!
	typename ImageType::Pointer m_Data;
	//!
	std::string m_ComponentType;
};

}

#include "coreITKImageVectorImpl.txx"

#endif // _coreITKImageVectorImpl_H
