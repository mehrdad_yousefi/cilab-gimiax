/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreVTKUnstructuredGridImpl.h"

Core::VtkUnstructuredGridImpl::VtkUnstructuredGridImpl( ) 
{
	ResetData();
}

Core::VtkUnstructuredGridImpl::~VtkUnstructuredGridImpl()
{

}

boost::any Core::VtkUnstructuredGridImpl::GetDataPtr() const
{
	return m_Data;
}

void Core::VtkUnstructuredGridImpl::SetDataPtr( boost::any val )
{
	if ( val.type() == typeid( DataType ) )
	{
		SetAnyData( val );
	}
	else if ( val.type() == typeid( vtkUnstructuredGrid* ) )
	{
		vtkUnstructuredGrid* processingDataCasted;
		processingDataCasted = boost::any_cast<vtkUnstructuredGrid*> ( val );
		SetAnyData( DataType( processingDataCasted ) );
	}
	else
	{
		throw Exceptions::DataTypeNotValidException( "VtkUnstructuredGridImpl::SetDataPtr" );
	}

	SetMemoryOwner( "Unknown" );
}

bool Core::VtkUnstructuredGridImpl::IsValidType( const std::string &datatypename )
{
	return ( datatypename == typeid( DataType ).name( ) ||
			 datatypename == typeid( vtkUnstructuredGrid* ).name( ) );
}

void Core::VtkUnstructuredGridImpl::ResetData()
{
	SetAnyData( DataType::New() );
}

void* Core::VtkUnstructuredGridImpl::GetVoidPtr() const
{
	void* ptr; ptr = m_Data; return ptr;
}

void Core::VtkUnstructuredGridImpl::SetVoidPtr( void* ptr )
{
	SetAnyData( reinterpret_cast<vtkUnstructuredGrid*> ( ptr ) );
	SetMemoryOwner( "Unknown" );
}

void Core::VtkUnstructuredGridImpl::SetAnyData( boost::any val )
{
	m_Data = boost::any_cast<DataType> ( val );
	if ( m_Data == NULL )
	{
		throw Exceptions::DataPtrIsNullException( "VtkUnstructuredGridImpl::SetDataPtr" );
	}
	SetMemoryOwner( "Unknown" );
}

void Core::VtkUnstructuredGridImpl::SetData( 
	blTagMap::Pointer tagMap, ImportMemoryManagementType mem/* = gmCopyMemory*/ )
{
	blTag::Pointer tagDataPtr = tagMap->FindTagByName( "DataPtr" );
	if ( tagDataPtr.IsNotNull() )
	{
		DataType srcData;
		if ( tagDataPtr->GetValue<DataType>( srcData ) )
		{
			m_Data->DeepCopy( srcData );
			SetMemoryOwner( "MITKPlugin" );
			return;
		}
	}

	throw Exceptions::NotImplementedException( "VtkUnstructuredGridImpl::SetData" );
}

void Core::VtkUnstructuredGridImpl::GetData( blTagMap::Pointer tagMap )
{
	tagMap->AddTag( "DataPtr", m_Data );
}

