/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef COREVTKPOLYDATAHOLDER_H
#define COREVTKPOLYDATAHOLDER_H

#include "corePluginMacros.h"
#include "coreDataHolder.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"

namespace Core{

typedef vtkSmartPointer<vtkPolyData> vtkPolyDataPtr;
typedef Core::DataHolder< vtkPolyDataPtr > vtkPolyDataHolder;

}

#endif //COREVTKPOLYDATAHOLDER_H
