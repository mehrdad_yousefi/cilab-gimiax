/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreMITKTransformImpl.h"
#include "coreITKTransformImpl.h"

using namespace Core;

/**
*/
Core::MitkTransformImpl::MitkTransformImpl( ) 
{
	ResetData( );
}

Core::MitkTransformImpl::~MitkTransformImpl()
{

}

boost::any Core::MitkTransformImpl::GetDataPtr() const
{
	return m_Data;
}

void Core::MitkTransformImpl::ResetData()
{
	m_Data = mitk::Transform::New();
}

void Core::MitkTransformImpl::SetAnyData( boost::any val )
{
	m_Data = boost::any_cast<mitk::Transform::Pointer> ( val );
	if ( m_Data.GetPointer() == NULL )
	{
		throw Exceptions::DataPtrIsNullException( "MitkTransformImpl::SetDataPtr" );
	}
}

size_t Core::MitkTransformImpl::GetSize() const
{
	return m_Data->GetTimeSteps();
}

void Core::MitkTransformImpl::SetSize( size_t size, DataEntityImpl::Pointer data )
{
	m_Data->Initialize( size );
}

DataEntityImpl::Pointer Core::MitkTransformImpl::GetAt( size_t num )
{
	ITKTransformImpl::TransformPointer transform = m_Data->GetTransform( num );
	ITKTransformImpl::Pointer transformImpl = ITKTransformImpl::New( );
	transformImpl->SetDataPtr( transform );
	return transformImpl.GetPointer();
}

void Core::MitkTransformImpl::SetAt( 
	size_t pos, DataEntityImpl::Pointer data, ImportMemoryManagementType mem /*= gmCopyMemory */ )
{
	if ( data->GetDataPtr().type() != typeid( ITKTransformImpl::TransformPointer ) )
	{
		throw Exceptions::DataTypeNotValidException( "MitkTransformImpl::SetAt" );
	}

	ITKTransformImpl::TransformPointer transform;
	transform = boost::any_cast<ITKTransformImpl::TransformPointer>( data->GetDataPtr() );
	m_Data->SetTransform( pos, transform );
}

