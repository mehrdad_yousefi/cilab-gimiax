/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "PropagateProcessor.h"

PropagateProcessor::PropagateProcessor() 
{
	SetNumberOfInputs( 1 );
	GetInputPort( 0 )->SetName( "Input data" );
	GetInputPort( 0 )->SetDataEntityType( Core::UnknownTypeId );
	GetInputPort( 0 )->SetUpdateMode( Core::BaseFilterInputPort::UPDATE_ACCESS_MULTIPLE_TIME_STEP );

	SetNumberOfOutputs( 1 );
	GetOutputPort( 0 )->SetName( "Propagated data" );
	GetOutputPort( 0 )->SetDataEntityType( Core::UnknownTypeId );
	
	m_left = 0;
	m_right = 0;
}


PropagateProcessor::~PropagateProcessor() 
{

}

void PropagateProcessor::SetRange(int left, int right)
{
	m_left = left;
	m_right = right;
}

void PropagateProcessor::Update( ) 
{
	if ( GetInputDataEntity( 0 ).IsNull( ) )
	{
		return;
	}

	int nTotalTimeSteps = GetInputDataEntity( 0 )->GetNumberOfTimeSteps( );
	int currentTimeStep = GetTimeStep( );

	int firstTimeStep = std::max( currentTimeStep + m_left, 0 ); //note m_left is < 0
	int lastTimeStep = std::min( currentTimeStep + m_right, nTotalTimeSteps - 1 );

	// Copy input to output
	Core::DataEntity::Pointer outDataEntity = Core::DataEntity::New( );
	outDataEntity->Copy( GetInputDataEntity( 0 ) );
	outDataEntity->SetType( GetInputDataEntity( 0 )->GetType( ) );
	outDataEntity->GetMetadata( )->SetName( "Propagated data" );

	// Get current time step
	Core::DataEntityImpl::Pointer timeStepImpl = GetInputDataEntity( 0 )->GetTimeStep( currentTimeStep );
	
	for ( int t = 0; t < nTotalTimeSteps ; t++ )
	{
		if((t>=firstTimeStep) && (t<=lastTimeStep))
		{
			outDataEntity->GetTimeStep( t )->DeepCopy( timeStepImpl );
		}
	}

	UpdateOutput( 0, outDataEntity, GetInputDataEntity( 0 ), GetInputDataEntity( 0 )->GetMetadata( ).GetPointer( ) );
}


