/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreVTKProcessor.h"
#include "vtkInformation.h"
#include "vtkWindowedSincPolyDataFilter.h"
#include "coreVTKPolyDataHolder.h"
#include "coreVTKImageDataHolder.h"

void PLUGIN_EXPORT coreVTKProcessorFunc( )
{
	Core::VTKProcessor<vtkWindowedSincPolyDataFilter>::Pointer processor;
	processor = Core::VTKProcessor<vtkWindowedSincPolyDataFilter>::New( );
	processor->Update( );

	Core::vtkPolyDataPtr polyData;
	processor->GetProcessingData<Core::vtkPolyDataPtr>( 0, polyData, 0 );

}




