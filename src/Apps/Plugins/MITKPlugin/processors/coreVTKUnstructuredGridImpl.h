/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreVtkUnstructuredGridImpl_H
#define _coreVtkUnstructuredGridImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "coreVTKUnstructuredGridHolder.h"

namespace Core{

/**
Surface time steps for DataEntity

- "Points"
- "SurfaceElements"

\author Xavi Planes
\date 03 nov 2009
\ingroup MITKPlugin
*/
class PLUGIN_EXPORT VtkUnstructuredGridImpl : public DataEntityImpl
{
public:

	typedef vtkUnstructuredGridPtr DataType;

	coreDeclareSmartPointerClassMacro( Core::VtkUnstructuredGridImpl, DataEntityImpl )
	
	coreDefineSingleDataFactory2Types( Core::VtkUnstructuredGridImpl, vtkUnstructuredGridPtr, vtkUnstructuredGrid*, VolumeMeshTypeId )

	//@{ 
	/// \name Interface
public:
	void SetDataPtr( boost::any val );
	boost::any GetDataPtr() const;
	virtual void* GetVoidPtr( ) const;
	virtual void SetVoidPtr( void* ptr );

private:
	virtual void SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem = gmCopyMemory );
	virtual void GetData( blTagMap::Pointer tagMap );
	void SetAnyData( boost::any val );
	bool IsValidType( const std::string &datatypename );
	virtual void ResetData( );
	//@}

private:
	//!
	VtkUnstructuredGridImpl( );

	//!
	virtual ~VtkUnstructuredGridImpl( );

private:

	//!
	DataType m_Data;

};

} // end namespace Core

#endif // _coreVtkUnstructuredGridImpl_H

