/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreMITKPointSetImpl.h"
#include "coreVTKPolyDataHolder.h"

using namespace Core;

MitkPointSetImpl::MitkPointSetImpl()
{
	ResetData();
}

MitkPointSetImpl::~MitkPointSetImpl()
{
}

boost::any Core::MitkPointSetImpl::GetDataPtr() const
{
	return m_Data;
}

void Core::MitkPointSetImpl::ResetData()
{
	m_Data = mitk::PointSet::New( );
}

void Core::MitkPointSetImpl::SetAnyData( boost::any val )
{
	m_Data = boost::any_cast<mitk::PointSet::Pointer> ( val );
	if ( m_Data.GetPointer( ) == NULL )
	{
		throw Exceptions::DataPtrIsNullException( "MitkPointSetImpl::SetDataPtr" );
	}
	
	SetMemoryOwner( "Unknown" );
}

void Core::MitkPointSetImpl::SetData(
	blTagMap::Pointer tagMap, ImportMemoryManagementType mem /*= gmCopyMemory */ )
{
	blTag::Pointer tagDataEntitySetImplSet = SafeFindTag( tagMap, "DataEntityImplSet" );

	DataEntityImplSetType setOfDataEntitySetImpl;
	setOfDataEntitySetImpl = tagDataEntitySetImplSet->GetValueCasted<DataEntityImplSetType>();

	DataEntityImplSetType::iterator it;
	int time = 0;
	for ( it = setOfDataEntitySetImpl.begin() ; it != setOfDataEntitySetImpl.end() ; it++)
	{
		// Deep copy from input surface mesh to mitk::PointSet with in time
		SetAt( time, (*it) );

		time++;
	}

	SetMemoryOwner( "MITKPlugin" );
}

void Core::MitkPointSetImpl::GetData( blTagMap::Pointer tagMap )
{
	
}

size_t Core::MitkPointSetImpl::GetSize() const
{
	return m_Data->GetTimeSteps();
}

void Core::MitkPointSetImpl::SetSize( size_t size, DataEntityImpl::Pointer data )
{
	m_Data->Expand( size );
}



void Core::MitkPointSetImpl::SetAt( 
	size_t pos, 
	DataEntityImpl::Pointer data,
	ImportMemoryManagementType mem /*= gmCopyMemory*/ )
{
	if ( data->GetDataPtr().type() != typeid( vtkPolyDataPtr ) )
	{
		throw Exceptions::DataTypeNotValidException( "MitkSurfaceImpl::SetAt" );
	}

	vtkPolyDataPtr polyData;
	polyData = boost::any_cast<vtkPolyDataPtr>( data->GetDataPtr() );

	mitk::PointSet::PointsContainer *pointsItk;
	mitk::PointSet::PointsContainer::Iterator it;
	pointsItk = m_Data->GetPointSet( pos )->GetPoints();
	vtkPoints *pointsVtk = polyData->GetPoints( );

	// mitkPointSet can hold an empty point at position 0
	// while vtk does not have empty points
	int mitkCount = 0;
	if ( pointsVtk )
	{
		for ( int i = 0 ; i != pointsVtk->GetNumberOfPoints() ; i++)
		{
			if ( pointsItk->IndexExists( i ) )
			{
				mitkCount++;
			}
		}
	}

	// When the user moves a point, we need to keep the same points reference
	// and change only the position, otherwise, it crashes
	if ( pointsVtk && pointsVtk->GetNumberOfPoints() == mitkCount )
	{
		for (it = pointsItk->Begin(); it != pointsItk->End(); it++)
		{
			if (pointsItk->IndexExists(it->Index()))
			{
				// Get VTK point
				double vtkPoint[ 3 ];
				pointsVtk->GetPoint( it->Index(), vtkPoint );

				// Convert from VTK to MITK point
				mitk::PointSet::PointType mitkPointVtk;
				mitkPointVtk[ 0 ] = mitk::PointSet::CoordinateType( vtkPoint[ 0 ] );
				mitkPointVtk[ 1 ] = mitk::PointSet::CoordinateType( vtkPoint[ 1 ] );
				mitkPointVtk[ 2 ] = mitk::PointSet::CoordinateType( vtkPoint[ 2 ] );
				
				// Get original MITK point
				mitk::PointSet::PointType mitkPoint;
				mitkPoint = m_Data->GetPoint( it->Index(), pos );
				
				// Only change if position has changed
				if ( mitkPointVtk != mitkPoint )
				{
					m_Data->SetPoint( it->Index(), mitkPoint, pos );
				}
			}
		}
	}
	else
	{

		// Remove all points
		while ( pointsItk->size( ) )
		{
			mitk::ScalarType timeInMS = 0.0;
			mitk::Point3D point;
			timeInMS = m_Data->GetTimeSlicedGeometry()->TimeStepToMS( pos );
			mitk::PointOperation* doOp = new mitk::PointOperation(
				mitk::OpREMOVE, timeInMS, point, pointsItk->begin( )->first );
			m_Data->ExecuteOperation( doOp );
		}

		// Add all the points
		if ( pointsVtk != NULL )
		{
			for( int i = 0; i < pointsVtk->GetNumberOfPoints(); i++ )
			{
				mitk::PointSet::PointType mitkPoint;
				mitkPoint[ 0 ] = mitk::PointSet::CoordinateType( pointsVtk->GetPoint( i )[ 0 ] );
				mitkPoint[ 1 ] = mitk::PointSet::CoordinateType( pointsVtk->GetPoint( i )[ 1 ] );
				mitkPoint[ 2 ] = mitk::PointSet::CoordinateType( pointsVtk->GetPoint( i )[ 2 ] );
				m_Data->InsertPoint( i, mitkPoint, pos );
			}
		}
	}
}

