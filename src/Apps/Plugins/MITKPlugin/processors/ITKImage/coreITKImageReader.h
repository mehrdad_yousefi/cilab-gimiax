/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreITKImageReader_H
#define coreITKImageReader_H

#include "coreBaseDataEntityReader.h"

namespace Core
{
namespace IO
{
/** 
Read ITK images

\ingroup MITKPlugin
\author Xavi Planes
\date Jan 2011
*/
class PLUGIN_EXPORT ITKImageReader : public BaseDataEntityReader
{
public:
	coreDeclareSmartPointerClassMacro(
		Core::IO::ITKImageReader, 
		BaseDataEntityReader);

	//!
	void ReadData();

	//!
	void UpdateProgress( itk::Object *caller, const itk::EventObject& event );

protected:
	ITKImageReader(void);
	virtual ~ITKImageReader(void);

	//!
	virtual boost::any ReadSingleTimeStep( 
		int iTimeStep, 
		const std::string &filename );

	//!
	void ReadImageHeader( const std::string &filename );

	//!
	void ReadWholeImage( Core::DataEntityType type );

private:
	coreDeclareNoCopyConstructors(ITKImageReader);

	//!
	itk::ImageIOBase::IOComponentType m_ComponentType;
	//!
	unsigned int m_NumberOfDimensions;
	//!
	unsigned int m_NumberOfComponents;

};

}
}

#endif //coreVTKImageData
