/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreITKImageReader.h"
#include "coreStringHelper.h"
#include "coreDataTreeMITKHelper.h"
#include "coreSettings.h"

#include "itkImageFileReader.h"
#include "itkImageIOFactory.h"
#include "itkAnalyzeImageIO.h"

using namespace Core::IO;


ITKImageReader::ITKImageReader(void)
{
	itk::ImageIOFactory::RegisterBuiltInFactories();

	std::list<itk::LightObject::Pointer> objects;
	objects = itk::ObjectFactoryBase::CreateAllInstance( "itkImageIOBase" );
	std::list<itk::LightObject::Pointer>::iterator i;
	for ( i = objects.begin(); i != objects.end(); ++i )
	{
		itk::ImageIOBase* imageIO = static_cast<itk::ImageIOBase*>( (*i).GetPointer() );
		itk::ImageIOBase::ArrayOfExtensionsType extensions;
		extensions = imageIO->GetSupportedReadExtensions( );
		m_ValidExtensionsList.insert( m_ValidExtensionsList.end( ), extensions.begin(), extensions.end() );
	}

	// These extensions are not included by default! GetSupportedReadExtensions() returns an empty array
	m_ValidExtensionsList.push_back( ".nrrd" );
	m_ValidExtensionsList.push_back( ".nhdr" );
	m_ValidExtensionsList.push_back( ".dcm" );
	m_ValidExtensionsList.push_back( ".rec" );
	//std::list<std::string>::iterator it;
	//for ( it = m_ValidExtensionsList.begin() ; it != m_ValidExtensionsList.end() ; it++ )
	//{
	//	std::cout << *it << std::endl;
	//}

	m_ValidTypesList.push_back( ImageTypeId );
	m_ValidTypesList.push_back( ROITypeId );

	SetName( "ITK image reader" );

	m_ComponentType = itk::ImageIOBase::UNKNOWNCOMPONENTTYPE;
	m_NumberOfDimensions = 0;
	m_NumberOfComponents = 0;
}

ITKImageReader::~ITKImageReader(void)
{
}


/**
The blVtkTemplateMacro is used to centralize the set of types
 supported by LoadITKImageTemplate( ) function.  It also avoids duplication 
 of long switch statement case lists.
\note Is base in vtkTemplateMacro.
*/
#define ItkTemplateMacroCase(typeN, typeP, callFucntion)     \
  case typeN: { typedef typeP PixelT; callFucntion; }; break;

#define ItkTemplateMacro(call)                                          \
	ItkTemplateMacroCase(itk::ImageIOBase::DOUBLE, double, call);       \
	ItkTemplateMacroCase(itk::ImageIOBase::FLOAT, float, call);         \
	ItkTemplateMacroCase(itk::ImageIOBase::LONG, long, call);           \
	ItkTemplateMacroCase(itk::ImageIOBase::ULONG, unsigned long, call); \
	ItkTemplateMacroCase(itk::ImageIOBase::INT, int, call);             \
	ItkTemplateMacroCase(itk::ImageIOBase::UINT, unsigned int, call);   \
	ItkTemplateMacroCase(itk::ImageIOBase::SHORT, short, call);         \
	ItkTemplateMacroCase(itk::ImageIOBase::USHORT, unsigned short, call);\
	ItkTemplateMacroCase(itk::ImageIOBase::CHAR, char, call);           \
	ItkTemplateMacroCase(itk::ImageIOBase::UCHAR, unsigned char, call)

#define ITK_EXPOSE_METADATA( tag, dictionary, itr, type )\
	if ( itr->second->GetMetaDataObjectTypeInfo() == typeid( type ) )\
	{\
		type value;\
		itk::ExposeMetaData<type>(dictionary, itr->first, value);\
		tag = blTag::New( itr->first, value ); \
	}


template<class ImageType>
void ReadMetadata( 
	blTagMap::Pointer metadata,
	typename ImageType::Pointer image )
{
	if ( image.IsNull( ) || metadata.IsNull( ) )
	{
		return;
	}

	itk::MetaDataDictionary dictionary = image->GetMetaDataDictionary( );

	itk::MetaDataDictionary::ConstIterator itr = dictionary.Begin();
	itk::MetaDataDictionary::ConstIterator end = dictionary.End();
	
	while(itr != end)
	{
		// Get value
		blTag::Pointer tag;

		ITK_EXPOSE_METADATA( tag, dictionary, itr, bool );
		ITK_EXPOSE_METADATA( tag, dictionary, itr, std::string );
		ITK_EXPOSE_METADATA( tag, dictionary, itr, float );
		ITK_EXPOSE_METADATA( tag, dictionary, itr, double );
		ITK_EXPOSE_METADATA( tag, dictionary, itr, char );
		ITK_EXPOSE_METADATA( tag, dictionary, itr, unsigned char );
		ITK_EXPOSE_METADATA( tag, dictionary, itr, short );
		ITK_EXPOSE_METADATA( tag, dictionary, itr, unsigned short );
		ITK_EXPOSE_METADATA( tag, dictionary, itr, int );
		ITK_EXPOSE_METADATA( tag, dictionary, itr, unsigned int );
		ITK_EXPOSE_METADATA( tag, dictionary, itr, long );
		ITK_EXPOSE_METADATA( tag, dictionary, itr, unsigned long );

		if ( tag.IsNotNull( ) )
		{
			metadata->AddTag( tag );
		}
		itr++;
	} // end while ( it != m_Data->GetIteratorEn()

	
}

template <class ImageType>
typename ImageType::Pointer LoadITKImage2D( 
	const char *imageFileName, ITKImageReader* gmReader, blTagMap::Pointer metadata )
{
	typename itk::ImageFileReader<ImageType>::Pointer reader;
	reader = itk::ImageFileReader<ImageType>::New();
	reader->SetFileName( imageFileName );

	// Disable graphical warnings because this code can be executed in a working thread
	bool warning = ::itk::Object::GetGlobalWarningDisplay();
	::itk::Object::SetGlobalWarningDisplay( false );

	// Add progress
	itk::MemberCommand<ITKImageReader>::Pointer command;
	command = itk::MemberCommand<ITKImageReader>::New();
	command->SetCallbackFunction( gmReader, &ITKImageReader::UpdateProgress );
	reader->AddObserver( itk::ProgressEvent(), command );

	// Update
	reader->Update();
	typename ImageType::Pointer image = reader->GetOutput( );

	::itk::Object::SetGlobalWarningDisplay( warning );

	ReadMetadata<ImageType>( metadata, image );

	return image;
}

template <class ImageType>
typename ImageType::Pointer LoadITKImage3D( 
	const char *imageFileName, ITKImageReader* gmReader, 
	bool orientImages, blTagMap::Pointer metadata )
{
	// does not seem to make a difference to use itk::OrientedImage
	typename itk::ImageFileReader<ImageType>::Pointer reader;
	reader = itk::ImageFileReader<ImageType>::New();
	reader->SetFileName( imageFileName );

	// Disable graphical warnings because this code can be executed in a working thread
	bool warning = ::itk::Object::GetGlobalWarningDisplay();
	::itk::Object::SetGlobalWarningDisplay( false );

	// Add progress
	itk::MemberCommand<ITKImageReader>::Pointer command;
	command = itk::MemberCommand<ITKImageReader>::New();
	command->SetCallbackFunction( gmReader, &ITKImageReader::UpdateProgress );
	reader->AddObserver( itk::ProgressEvent(), command );

	// Update
	reader->Update();

	// Reorient the data
	typename ImageType::Pointer orientedImage;
	if ( orientImages )
	{
		orientedImage = blITKImageUtils::ApplyOrientationToImage< ImageType >( reader->GetOutput() );

		// when reading Analyze images, set origin to 0,0,0 because when
		// changing orientation, origin changes
		if ( dynamic_cast<itk::AnalyzeImageIO*> ( reader->GetImageIO() ) != NULL )
		{
			double origin[ 3 ] = {0,0,0};
			orientedImage->SetOrigin( origin );
		}
	}
	else
	{
		orientedImage = reader->GetOutput();
	}

	::itk::Object::SetGlobalWarningDisplay( warning );

	ReadMetadata<ImageType>( metadata, orientedImage );

	return orientedImage;
}

void ITKImageReader::ReadData()
{
	// Read data entity type from header (backwards compatibility)
	Core::DataEntityType dataEntityType;
	std::string header;
	dataEntityType = Core::DataTreeMITKHelper::ReadDataEntityTypeDescription( m_Filenames[ 0 ] );
	if ( dataEntityType == UnknownTypeId )
	{
		dataEntityType = ImageTypeId;
	}

	// Read header information
	ReadImageHeader( m_Filenames[ 0 ] );
	if ( m_NumberOfComponents > 1 )
	{
		dataEntityType = DataEntityType( ImageTypeId | VectorFieldTypeId );
	}

	if ( m_NumberOfDimensions > 3 )
	{
		ReadWholeImage( dataEntityType );
	}
	else
	{
		// Read all time steps
		ReadAllTimeSteps( dataEntityType );
	}
}

void ITKImageReader::ReadImageHeader( const std::string &filename )
{
	bool warning = ::itk::Object::GetGlobalWarningDisplay();
	::itk::Object::SetGlobalWarningDisplay( false );

	itk::ImageIOBase::Pointer imageIO;
	imageIO = itk::ImageIOFactory::CreateImageIO( 
		filename.c_str(), 
		itk::ImageIOFactory::ReadMode );
	if ( imageIO.IsNull() )
	{
		::itk::Object::SetGlobalWarningDisplay( warning );
		return;
	}

	imageIO->SetFileName( filename );
	imageIO->ReadImageInformation( );

	m_ComponentType = imageIO->GetComponentType( );
	m_NumberOfDimensions = imageIO->GetNumberOfDimensions( );
	m_NumberOfComponents = imageIO->GetNumberOfComponents( );

	::itk::Object::SetGlobalWarningDisplay( warning );
}

boost::any ITKImageReader::ReadSingleTimeStep( int iTimeStep, const std::string &filename )
{

	bool warning = ::itk::Object::GetGlobalWarningDisplay();
	::itk::Object::SetGlobalWarningDisplay( false );

	// Check the orientation settings attribute
	bool orientImages = true;
	if ( Core::Runtime::Kernel::GetApplicationSettings().IsNotNull() )
	{
		std::string orient;
		Core::Runtime::Kernel::GetApplicationSettings()->GetPluginProperty( "GIMIAS", "OrientImagesWhenReading", orient );
		orientImages = orient == "false" ? false : true;
	}

	blTagMap::Pointer metadata;
	if ( GetOutputDataEntity( 0 ).IsNotNull( ) )
	{
		metadata = GetOutputDataEntity( 0 )->GetMetadata( );
	}

	// Read image
	boost::any image;
	if ( m_NumberOfComponents == 1 )
	{
		if(m_NumberOfDimensions == 2)
		{
			switch ( m_ComponentType )
			{
				ItkTemplateMacro( ( image = LoadITKImage2D< itk::Image<PixelT, 2> >( filename.c_str(), this, metadata ) ) );
			}
		}
		else if (m_NumberOfDimensions == 3)
		{
			switch ( m_ComponentType )
			{
				ItkTemplateMacro( ( image = LoadITKImage3D< itk::Image<PixelT, 3> >( filename.c_str(), this, orientImages, metadata ) ) );
			}
		}
		else if (m_NumberOfDimensions == 4)
		{
			switch ( m_ComponentType )
			{
				ItkTemplateMacro( ( image = LoadITKImage2D< itk::Image<PixelT, 4> >( filename.c_str(), this, metadata ) ) );
			}
		}
	}
	else if ( m_NumberOfComponents == 2 )
	{
		if(m_NumberOfDimensions == 2)
		{
			switch ( m_ComponentType )
			{
				ItkTemplateMacro( ( image = LoadITKImage2D< itk::Image<itk::Vector<PixelT,2>, 2> >( filename.c_str(), this, metadata ) ) );
			}
		}
		else if (m_NumberOfDimensions == 3)
		{
			switch ( m_ComponentType )
			{
				ItkTemplateMacro( ( image = LoadITKImage3D< itk::Image<itk::Vector<PixelT,2>, 3> >( filename.c_str(), this, false, metadata ) ) );
			}
		}
		else if (m_NumberOfDimensions == 4)
		{
			switch ( m_ComponentType )
			{
				ItkTemplateMacro( ( image = LoadITKImage2D< itk::Image<itk::Vector<PixelT,2>, 4> >( filename.c_str(), this, metadata ) ) );
			}
		}
	}
	else if ( m_NumberOfComponents == 3 )
	{
		if(m_NumberOfDimensions == 2)
		{
			switch ( m_ComponentType )
			{
				ItkTemplateMacro( ( image = LoadITKImage2D< itk::Image<itk::Vector<PixelT,3>, 2> >( filename.c_str(), this, metadata ) ) );
			}
		}
		else if (m_NumberOfDimensions == 3)
		{
			switch ( m_ComponentType )
			{
				ItkTemplateMacro( ( image = LoadITKImage3D< itk::Image<itk::Vector<PixelT,3>, 3> >( filename.c_str(), this, false, metadata ) ) );
			}
		}
		else if (m_NumberOfDimensions == 4)
		{
			switch ( m_ComponentType )
			{
				ItkTemplateMacro( ( image = LoadITKImage2D< itk::Image<itk::Vector<PixelT,3>, 4> >( filename.c_str(), this, metadata ) ) );
			}
		}
	}
	::itk::Object::SetGlobalWarningDisplay( warning );

	return image;
}

void ITKImageReader::ReadWholeImage( Core::DataEntityType type )
{
	if ( m_Filenames.size() == 0 )
	{
		return ;
	}

	// Read type from metadata and overwrite input parameter
	if ( GetNumberOfOutputs() && GetOutputDataEntity( 0 ).IsNotNull() )
	{
		int temp;
		blTag::Pointer tag;
		tag = GetOutputDataEntity( 0 )->GetMetadata()->FindTagByName( "Type" );
		if ( tag.IsNotNull() && tag->GetValue( temp ) && temp != UnknownTypeId )
		{
			type = Core::DataEntityType( temp );
		}
	}

	// Load a single itk::Image
	boost::any processingData = ReadSingleTimeStep( 0, m_Filenames[ 0 ].c_str() );
	if ( processingData.empty() )
	{
		return;
	}

	// Create a single DataEntity
	GetOutputPort( 0 )->SetDataEntityType( type );
	UpdateOutput(0, processingData, RemoveExtensionAndLastDigits(m_Filenames[ 0 ], GetExtension()), true );
	if ( GetOutputDataEntity( 0 ).IsNotNull() )
	{
		GetOutputDataEntity( 0 )->GetMetadata( )->AddTag( "FilePath", m_Filenames[ 0 ] );
	}
}

void ITKImageReader::UpdateProgress(
	itk::Object *caller,
	const itk::EventObject& event)
{
	itk::ProcessObject *processObject = (itk::ProcessObject*)caller;
	if (typeid(event) == typeid(itk::ProgressEvent)) 
	{
		GetUpdateCallback()->SetProgress( processObject->GetProgress() );
		GetUpdateCallback()->Modified();
	}

	if ( GetUpdateCallback()->GetAbortProcessing() )
	{
		processObject->AbortGenerateDataOn();
	}
}
