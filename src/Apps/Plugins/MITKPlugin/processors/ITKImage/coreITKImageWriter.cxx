/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreITKImageWriter.h"
#include "coreDataEntity.h"
#include "coreException.h"
#include "coreStringHelper.h"

#include "itkAnalyzeImageIO.h"
#include "itkMetaDataDictionary.h"

#include <stdio.h>
#include <string.h>


using namespace Core::IO;


/**
The ItkTemplateMacro is used to centralize the set of types. 
It also avoids duplication of long switch statement case lists.
\note Is base in vtkTemplateMacro.
*/
#define ItkTemplateMacroCase(scalarType, typeName, typeP, callFucntion)     \
	if ( scalarType == typeName ) { typedef typeP PixelT; callFucntion; }

#define ItkTemplateMacro(scalarType, call)                          \
	ItkTemplateMacroCase(scalarType, "double", double, call);       \
	ItkTemplateMacroCase(scalarType, "float", float, call);         \
	ItkTemplateMacroCase(scalarType, "long", long, call);           \
	ItkTemplateMacroCase(scalarType, "unsigned long", unsigned long, call); \
	ItkTemplateMacroCase(scalarType, "int", int, call);             \
	ItkTemplateMacroCase(scalarType, "unsigned int", unsigned int, call);   \
	ItkTemplateMacroCase(scalarType, "short", short, call);         \
	ItkTemplateMacroCase(scalarType, "unsigned short", unsigned short, call);\
	ItkTemplateMacroCase(scalarType, "char", char, call);           \
	ItkTemplateMacroCase(scalarType, "unsigned char", unsigned char, call)

#define ITK_ENCAPSULATE_METADATA( tag, dictionary, name, type )\
	if ( tag->GetTypeName( ) == name )\
		itk::EncapsulateMetaData<type>( dictionary, tag->GetName( ).c_str(), tag->GetValueCasted<type>( ) );


//!
ITKImageWriter::ITKImageWriter(void)
{
	itk::ImageIOFactory::RegisterBuiltInFactories();

	std::list<itk::LightObject::Pointer> objects;
	objects = itk::ObjectFactoryBase::CreateAllInstance( "itkImageIOBase" );
	std::list<itk::LightObject::Pointer>::iterator i;
	for ( i = objects.begin(); i != objects.end(); ++i )
	{
		itk::ImageIOBase* imageIO = static_cast<itk::ImageIOBase*>( (*i).GetPointer() );
		itk::ImageIOBase::ArrayOfExtensionsType extensions;
		extensions = imageIO->GetSupportedWriteExtensions( );
		m_ValidExtensionsList.insert( m_ValidExtensionsList.end( ), extensions.begin(), extensions.end() );

	}

	m_ValidExtensionsList.push_back( ".dcm" );
	m_ValidExtensionsList.push_back( ".nrrd" );
	m_ValidExtensionsList.push_back( ".rec" );

	m_ValidTypesList.push_back( ImageTypeId );
	m_ValidTypesList.push_back( ROITypeId );
}

//!
ITKImageWriter::~ITKImageWriter(void)
{
}

//!
void ITKImageWriter::WriteData()
{
	// For MHD files and multiple time steps, write all data in a single file
	std::string extension = itksys::SystemTools::GetFilenameExtension( m_Filenames[ 0 ].c_str( ) );
	extension = Core::StringHelper::ToLowerCase( extension );
	if ( GetInputPort( 0 )->GetUpdateMode() == BaseFilterInputPort::UPDATE_ACCESS_MULTIPLE_TIME_STEP &&
		 GetInputDataEntity( 0 )->GetNumberOfTimeSteps( ) > 1 &&
		 ( extension == ".mhd" || extension == ".nrrd" ) )
	{
		WriteWholeImage( GetInputDataEntity( 0 ) );
	}
	else
	{
		WriteAllTimeSteps( );
	}
}


template<class ImageType>
void WriteMetadata( 
	blTagMap::Pointer metadata,
	typename ImageType::Pointer image )
{
	if ( image.IsNull( ) || metadata.IsNull( ) )
	{
		return;
	}

	itk::MetaDataDictionary dictionary;

	// Iterate over metadata
	blTagMap::Iterator it = metadata->GetIteratorBegin();
	while ( it != metadata->GetIteratorEnd() )
	{
		blTag::Pointer tag = metadata->GetTag(it);

		ITK_ENCAPSULATE_METADATA( tag, dictionary, "bool", bool );
		ITK_ENCAPSULATE_METADATA( tag, dictionary, "std::string", std::string );
		ITK_ENCAPSULATE_METADATA( tag, dictionary, "float", float );
		ITK_ENCAPSULATE_METADATA( tag, dictionary, "double", double );
		ITK_ENCAPSULATE_METADATA( tag, dictionary, "char", char );
		ITK_ENCAPSULATE_METADATA( tag, dictionary, "unsigned char", unsigned char );
		ITK_ENCAPSULATE_METADATA( tag, dictionary, "short", short );
		ITK_ENCAPSULATE_METADATA( tag, dictionary, "unsigned short", unsigned short );
		ITK_ENCAPSULATE_METADATA( tag, dictionary, "int", int );
		ITK_ENCAPSULATE_METADATA( tag, dictionary, "unsigned int", unsigned int );
		ITK_ENCAPSULATE_METADATA( tag, dictionary, "long", long );
		ITK_ENCAPSULATE_METADATA( tag, dictionary, "unsigned long", unsigned long );
		
		it++;
	} // end while ( it != m_Data->GetIteratorEn()

	
	image->SetMetaDataDictionary(dictionary);
}

template<class ImageType>
typename ImageType::Pointer ExtractITKImage( 
	Core::DataEntity::Pointer dataEntity,
	int iTimeStep)
{
	typename ImageType::Pointer itkFinalImage;

	// Convert to itk
	Core::DataEntity::Pointer itkDataEntity = Core::DataEntity::New( Core::ImageTypeId );
	itkDataEntity->SwitchImplementation( typeid( typename ImageType::Pointer ) ); 
	itkDataEntity->Copy( dataEntity, Core::gmReferenceMemory );
	typename ImageType::Pointer image;
	bool worked = itkDataEntity->GetProcessingData( image, iTimeStep );
	if( !worked || image.IsNull() )
	{
		throw Core::Exceptions::Exception(
			"ITKImageWriter::WriteSingleTimeStep",
			"Input data is not of the correct type" );
	}

	// Add metadata
	WriteMetadata<ImageType>( dataEntity->GetMetadata( ).GetPointer(), image );

	return image;
}


template<class ImageType>
typename ImageType::Pointer PreProcess3DITKImage( 
	typename ImageType::Pointer image,
	const std::string& fileName)
{
	typename ImageType::Pointer itkFinalImage;

	std::string ext = itksys::SystemTools::GetFilenameLastExtension( fileName );
	std::string extension = itksys::SystemTools::LowerCase(ext);


	// Reorient Analyze image
	itk::AnalyzeImageIO::Pointer imageIO = itk::AnalyzeImageIO::New( );
	if( imageIO->CanWriteFile( fileName.c_str() ) )
	{
		// Change orientation to RPS because Analyze can only write RPI, PIR and RIP
		typename itk::OrientImageFilter<ImageType, ImageType>::Pointer orienter;
		orienter = itk::OrientImageFilter<ImageType, ImageType>::New();
		orienter->UseImageDirectionOn();
		orienter->SetDesiredCoordinateOrientation(itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_RPI);
		orienter->SetInput(image);
		orienter->Update();
		orienter->GetOutput();
		itkFinalImage = orienter->GetOutput();

		// The origin is changed, so set it to 0 for Analyze images...
		// Analyze format cannot store origin
		double origin[ 3 ] = {0,0,0};
		itkFinalImage->SetOrigin( origin );
	}
	else
	{
		itkFinalImage = image;
	}

	return itkFinalImage;
}

template<class ImageType>
void WriteITKImage( 
	typename ImageType::Pointer image,
	const std::string& fileName,
	ITKImageWriter* gmWriter)
{
	// Write image
	typedef itk::ImageFileWriter< ImageType > WriterType;
	typename WriterType::Pointer writer = WriterType::New();
	writer->SetFileName( fileName.c_str() );

	// Flip images for dicom files
	std::string ext = itksys::SystemTools::GetFilenameLastExtension( fileName );
	std::string extension = itksys::SystemTools::LowerCase(ext);
	bool flipImage = false;//(extension == ".dcm")? true : false;
	if(flipImage)
	{
		typename ImageType::Pointer itkFinalImage;
		itkFinalImage = blITKImageUtils::FlipItkImage< ImageType >( image );
		writer->SetInput( itkFinalImage );
	}
	else
	{
		writer->SetInput( image );
	}

	// Add progress
	itk::MemberCommand<ITKImageWriter>::Pointer command;
	command = itk::MemberCommand<ITKImageWriter>::New();
	command->SetCallbackFunction( gmWriter, &ITKImageWriter::UpdateProgress );
	writer->AddObserver( itk::ProgressEvent(), command );

	// Update
	writer->Update();

}

template<class ImageType>
void WriteITKImage2D( 
	Core::DataEntity::Pointer dataEntity,
	const std::string& fileName,
	int iTimeStep,
	ITKImageWriter* gmWriter)
{
	typename ImageType::Pointer image;

	image = ExtractITKImage< ImageType >( dataEntity, iTimeStep );
	WriteITKImage< ImageType >( image, fileName, gmWriter );
}

template<class ImageType>
void WriteITKImage3D( 
	Core::DataEntity::Pointer dataEntity,
	const std::string& fileName,
	int iTimeStep,
	ITKImageWriter* gmWriter)
{
	typename ImageType::Pointer image;

	image = ExtractITKImage< ImageType >( dataEntity, iTimeStep );
	image = PreProcess3DITKImage< ImageType >( image, fileName );
	WriteITKImage< ImageType >( image, fileName, gmWriter );
}

void Core::IO::ITKImageWriter::WriteSingleTimeStep( 
	const std::string& fileName, 
	Core::DataEntity::Pointer dataEntity, 
	int iTimeStep )
{

	// Get data information
	DataEntityImpl::Pointer impl = dataEntity->GetTimeStep( iTimeStep );
	blTagMap::Pointer tagMap = blTagMap::New( );
	impl->GetData( tagMap );

	// Get scalar type
	blTag::Pointer tag;
	tag = tagMap->FindTagByName( "ScalarType" );
	if ( tag.IsNull() ) return ;
	std::string scalarType = tag->GetValueCasted<std::string>();

	tag = tagMap->FindTagByName( "Dimensions" );
	if ( tag.IsNull() ) return ;
	std::vector<int> temp2 = tag->GetValueCasted< std::vector<int> >();

	tag = tagMap->FindTagByName( "NumberOfComponents" );
	if ( tag.IsNull() ) return ;
	int numComponents = tag->GetValueCasted<int>();

	// Disable graphical warnings because this code can be executed in a working thread
	bool warning = ::itk::Object::GetGlobalWarningDisplay();
	::itk::Object::SetGlobalWarningDisplay( false );

	// Switch to itk::Image
	if ( numComponents == 1 )
	{
		if ( temp2.size() == 2 )
		{
			ItkTemplateMacro( scalarType, 
				( WriteITKImage2D< itk::Image<PixelT,2> >( dataEntity, fileName, iTimeStep, this ) ) );
		}
		else if ( temp2.size() == 3 )
		{
			ItkTemplateMacro( scalarType, 
				( WriteITKImage3D< itk::Image<PixelT,3> >( dataEntity, fileName, iTimeStep, this ) ) );
		}
	}
	else if ( numComponents == 2 )
	{
		if ( temp2.size() == 2 )
		{
			ItkTemplateMacro( scalarType, 
				( WriteITKImage2D< itk::Image<itk::Vector<PixelT,2>,2> >( dataEntity, fileName, iTimeStep, this ) ) );
		}
		else if ( temp2.size() == 3 )
		{
			ItkTemplateMacro( scalarType, 
				( WriteITKImage3D< itk::Image<itk::Vector<PixelT,2>,3> >( dataEntity, fileName, iTimeStep, this ) ) );
		}
	}
	else if ( numComponents == 3 )
	{
		if ( temp2.size() == 2 )
		{
			ItkTemplateMacro( scalarType, 
				( WriteITKImage2D< itk::Image<itk::Vector<PixelT,3>,2> >( dataEntity, fileName, iTimeStep, this ) ) );
		}
		else if ( temp2.size() == 3 )
		{
			ItkTemplateMacro( scalarType, 
				( WriteITKImage3D< itk::Image<itk::Vector<PixelT,3>,3> >( dataEntity, fileName, iTimeStep, this ) ) );
		}
	}

	::itk::Object::SetGlobalWarningDisplay( warning );
}

void Core::IO::ITKImageWriter::WriteWholeImage( 
	Core::DataEntity::Pointer dataEntity )
{

	// Get data information
	DataEntityImpl::Pointer impl = dataEntity->GetTimeStep( 0 );
	blTagMap::Pointer tagMap = blTagMap::New( );
	impl->GetData( tagMap );

	// Get scalar type
	blTag::Pointer tag;
	tag = tagMap->FindTagByName( "ScalarType" );
	if ( tag.IsNull() ) return ;
	std::string scalarType = tag->GetValueCasted<std::string>();

	tag = tagMap->FindTagByName( "Dimensions" );
	if ( tag.IsNull() ) return ;
	std::vector<int> temp2 = tag->GetValueCasted< std::vector<int> >();

	tag = tagMap->FindTagByName( "NumberOfComponents" );
	if ( tag.IsNull() ) return ;
	int numComponents = tag->GetValueCasted<int>();

	// Disable graphical warnings because this code can be executed in a working thread
	bool warning = ::itk::Object::GetGlobalWarningDisplay();
	::itk::Object::SetGlobalWarningDisplay( false );

	// Switch to itk::Image
	if ( numComponents == 1 )
	{
		if ( temp2.size() == 2 )
		{
			ItkTemplateMacro( scalarType, 
				( WriteITKImage2D< itk::Image<PixelT,3> >( dataEntity, m_Filenames[ 0 ], -1, this ) ) );
		}
		else if ( temp2.size() == 3 )
		{
			ItkTemplateMacro( scalarType, 
				( WriteITKImage2D< itk::Image<PixelT,4> >( dataEntity, m_Filenames[ 0 ], -1, this ) ) );
		}
	}
	else if ( numComponents == 2 )
	{
		if ( temp2.size() == 2 )
		{
			ItkTemplateMacro( scalarType, 
				( WriteITKImage2D< itk::Image<itk::Vector<PixelT,2>,3> >( dataEntity, m_Filenames[ 0 ], -1, this ) ) );
		}
		else if ( temp2.size() == 3 )
		{
			ItkTemplateMacro( scalarType, 
				( WriteITKImage2D< itk::Image<itk::Vector<PixelT,2>,4> >( dataEntity, m_Filenames[ 0 ], -1, this ) ) );
		}
	}
	else if ( numComponents == 3 )
	{
		if ( temp2.size() == 2 )
		{
			ItkTemplateMacro( scalarType, 
				( WriteITKImage2D< itk::Image<itk::Vector<PixelT,3>,3> >( dataEntity, m_Filenames[ 0 ], -1, this ) ) );
		}
		else if ( temp2.size() == 3 )
		{
			ItkTemplateMacro( scalarType, 
				( WriteITKImage2D< itk::Image<itk::Vector<PixelT,3>,4> >( dataEntity, m_Filenames[ 0 ], -1, this ) ) );
		}
	}

	::itk::Object::SetGlobalWarningDisplay( warning );
}

void Core::IO::ITKImageWriter::UpdateProgress(
	itk::Object *caller,
	const itk::EventObject& event)
{
	itk::ProcessObject *processObject = (itk::ProcessObject*)caller;
	if (typeid(event) == typeid(itk::ProgressEvent)) 
	{
		GetUpdateCallback()->SetProgress( processObject->GetProgress() );
		GetUpdateCallback()->Modified();

		if ( GetUpdateCallback()->GetAbortProcessing() )
		{
			processObject->AbortGenerateDataOn();
		}
	}
}
