/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreITKImageWriter_H
#define coreITKImageWriter_H

#include "coreBaseDataEntityWriter.h"

namespace Core
{
namespace IO
{
/** 
Write ITK images

\ingroup MITKPlugin
\author Xavi Planes
\date Jan 2011
*/
class PLUGIN_EXPORT ITKImageWriter : public BaseDataEntityWriter
{
public:
	coreDeclareSmartPointerClassMacro(
		Core::IO::ITKImageWriter, 
		BaseDataEntityWriter);

	void WriteData( );

	//!
	void UpdateProgress(
		itk::Object *caller,
		const itk::EventObject& event);

protected:
	ITKImageWriter(void);
	virtual ~ITKImageWriter(void);

	//! Write Single data (image)
	void WriteSingleTimeStep( 
		const std::string& fileName, 
		Core::DataEntity::Pointer dataEntity,
		int iTimeStep );

	//! Write whole image
	void WriteWholeImage( Core::DataEntity::Pointer dataEntity );

private:
	coreDeclareNoCopyConstructors(ITKImageWriter);
};

}
}

#endif // coreITKImageWriter_H
