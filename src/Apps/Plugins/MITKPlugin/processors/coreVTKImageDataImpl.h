/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreVtkImageDataImpl_H
#define _coreVtkImageDataImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "coreVTKImageDataHolder.h"

namespace Core{

/**
Stores a vtkSmartPointer<vtkImageData>

- "Origin": std::vector<double>
- "Spacing": std::vector<double>
- "Dimensions": std::vector<int>
- "ScalarType": std::string
- "ScalarPointer": void*
- "ScalarSizeInBytes": size_t
- "NumberOfComponents": int

\author Xavi Planes
\date 08 Sept 2010
\ingroup MITKPlugin
*/
class PLUGIN_EXPORT VtkImageDataImpl : public DataEntityImpl
{
public:
	typedef vtkSmartPointer<vtkImageData> DataType;

	coreDeclareSmartPointerClassMacro( Core::VtkImageDataImpl, DataEntityImpl )
	
	coreDefineSingleDataFactory2Types( Core::VtkImageDataImpl, DataType, vtkImageData*, ImageTypeId )

	//@{ 
	/// \name Interface
public:
	void SetDataPtr( boost::any val );
	boost::any GetDataPtr() const;
	virtual void* GetVoidPtr( ) const;
	virtual void SetVoidPtr( void* ptr );

private:
	virtual void SetAnyData( boost::any val );
	virtual void SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem = gmCopyMemory );
	virtual void GetData( blTagMap::Pointer tagMap );
	bool IsValidType( const std::string &datatypename );
	virtual void ResetData( );
	//@}

private:
	//!
	VtkImageDataImpl( );

	//!
	virtual ~VtkImageDataImpl( );

private:

	DataType m_Data;

};

} // end namespace Core

#endif // _coreVtkImageDataImpl_H

