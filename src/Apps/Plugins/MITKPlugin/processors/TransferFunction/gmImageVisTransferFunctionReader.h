/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _gmImageVisTransferFunctionReader_H
#define _gmImageVisTransferFunctionReader_H

#include "gmTransferFunctionIOBase.h"

/**
Reader transfer function from ImageVis3D

\ingroup MITKPlugin
\author Xavi Planes
\date Jan 2013
*/
class PLUGIN_EXPORT gmImageVisTransferFunctionReader : public gmTransferFunctionIOBase
{
public:
	coreDeclareSmartPointerClassMacro( gmImageVisTransferFunctionReader, gmTransferFunctionIOBase);
	coreDefineFactory( gmImageVisTransferFunctionReader )

private:
	//!
	gmImageVisTransferFunctionReader( );

	//!
	~gmImageVisTransferFunctionReader( );

	//!
	void InternalUpdate( );

};


#endif // _gmImageVisTransferFunctionReader_H

