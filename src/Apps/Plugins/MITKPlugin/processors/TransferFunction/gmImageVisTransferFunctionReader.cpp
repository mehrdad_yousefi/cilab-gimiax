/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "gmImageVisTransferFunctionReader.h"
#include "blTextUtils.h"

gmImageVisTransferFunctionReader::gmImageVisTransferFunctionReader()
{

}

gmImageVisTransferFunctionReader::~gmImageVisTransferFunctionReader()
{

}

void gmImageVisTransferFunctionReader::InternalUpdate()
{
	// Initialize pointer
	m_Data = NULL;

	// Open file
	std::ifstream fileStream;
	fileStream.open ( m_Filename.c_str() );
	if ( !fileStream.good())
	{
		return;
	}

	// Create a new instance
	m_Data = mitk::TransferFunction::New( );

	// Read data
	bool fileisOk = true;
	std::string line;

	// Read number of entries: 2850
	fileisOk = std::getline(fileStream,line) && fileisOk;
	if ( !fileisOk ) 
	{
		m_Data = NULL;
		return;
	}
	int numEntries = atoi( line.c_str( ) );

	// Initialize
	m_Data->SetMax( numEntries );
	m_Data->SetMin( 0 );
	m_Data->SetTransferFunctionMode( mitk::TransferFunction::TF_CUSTOM );

	// Read all entries: 0.902012 0.902012 0 0.902012
	int count = 0;
	while ( fileisOk && count < numEntries )
	{
		// Read line
		fileisOk = std::getline(fileStream,line) && fileisOk;
		if ( !fileisOk ) 
		{
			m_Data = NULL;
			return;
		}

		// Parse numbers
		std::list<std::string> words;
		blTextUtils::ParseLine( line, ' ', words ); 
		if ( words.size( ) != 4 ) 
		{
			m_Data = NULL;
			return;
		}

		// Get RGB color
		std::list<std::string>::iterator itWord = words.begin( );
		double r,g,b;
		r = atof( itWord->c_str( ) );
		itWord++;
		g = atof( itWord->c_str( ) );
		itWord++;
		b = atof( itWord->c_str( ) );
		itWord++;
		m_Data->GetColorTransferFunction( )->AddRGBPoint( count, r, g, b );

		// Opacity
		float opacity = atof( itWord->c_str( ) );
		itWord++;
		m_Data->GetScalarOpacityFunction( )->AddPoint( count, opacity );
		count++;
	}
	fileStream.close();
}

