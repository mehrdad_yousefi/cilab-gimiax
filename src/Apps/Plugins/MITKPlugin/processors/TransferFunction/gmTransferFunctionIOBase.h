/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _gmTransferFunctionIOBase_H
#define _gmTransferFunctionIOBase_H

#include "coreObject.h"
#include "mitkTransferFunction.h"

/**
Base reader/writer for all Trasnfer function reader/writers. 

\ingroup MITKPlugin
\author Xavi Planes
\date Jan 2013
*/
class PLUGIN_EXPORT gmTransferFunctionIOBase : public Core::SmartPointerObject
{
public:
	coreDeclareSmartPointerTypesMacro(gmTransferFunctionIOBase,Core::SmartPointerObject);
	coreClassNameMacro(gmTransferFunctionIOBase);
	coreTypeMacro(gmTransferFunctionIOBase,Core::SmartPointerObject);

	//! Set the complete filename 
	void SetFilename( const char* );

	//! execute
	virtual void Update( );

	//! Returns the output of the reader
	mitk::TransferFunction::Pointer GetOutput( );


protected:
	gmTransferFunctionIOBase( );

	~gmTransferFunctionIOBase( );

	//!
	virtual void InternalUpdate( ) = 0;

protected:

	//!
	mitk::TransferFunction::Pointer m_Data;

	//!
	std::string m_Filename;

	//!
	unsigned int m_Version;
};

#endif //_gmTransferFunctionIOBase_H
