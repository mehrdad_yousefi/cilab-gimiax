/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "gmTransferFunctionIOBase.h"


gmTransferFunctionIOBase::gmTransferFunctionIOBase() 
{
	m_Version = 0;
}

gmTransferFunctionIOBase::~gmTransferFunctionIOBase()
{
}

void gmTransferFunctionIOBase::SetFilename( const char* filename )
{
	m_Filename = filename;
}

void gmTransferFunctionIOBase::Update()
{
	InternalUpdate();
}

mitk::TransferFunction::Pointer gmTransferFunctionIOBase::GetOutput()
{
	return m_Data;
}

