/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreITKImageVectorImpl.h"
#include <typeinfo>

using namespace Core;

#define SetVectorComponentType( type1, IOComponent ) \
	if ( typeid( typename ImageType::PixelType::ValueType ) == typeid( type1 ) ) \
	{ \
		m_ComponentType = IOComponent; \
	}

// Create an itk image sharing the input buffer and cast to the internal type
#define CastVectorData( name, PixType, IDimension, vectorDimension ) \
	if (scalarType == name ) \
	{ \
		typedef itk::Image<itk::Vector<PixType,vectorDimension>,IDimension> InputImageType; \
		typename InputImageType::Pointer inputImage = InputImageType::New( ); \
		inputImage->SetOrigin( &origin[ 0 ] ); \
		inputImage->SetSpacing( &spacing[ 0 ] ); \
		inputImage->SetRegions( region ); \
		inputImage->GetPixelContainer( )->SetImportPointer(  \
			(typename InputImageType::PixelType*) (scalarPointer),  \
			region.GetNumberOfPixels( ) ); \
		typedef itk::CastImageFilter< InputImageType, ImageType > FilterType; \
		typename FilterType::Pointer filter = FilterType::New(); \
		filter->SetInput( inputImage ); \
		filter->Update( ); \
		m_Data = filter->GetOutput( ); \
	}

template <class ImageType>
ITKImageVectorImpl<ImageType>::ITKImageVectorImpl()
{
	ResetData();

	SetVectorComponentType( char, "char" )
	else SetVectorComponentType( unsigned char, "unsigned char" )
	else SetVectorComponentType( int, "int" )
	else SetVectorComponentType( unsigned int, "unsigned int" )
	else SetVectorComponentType( short, "short" )
	else SetVectorComponentType( unsigned short, "unsigned short" )
	else SetVectorComponentType( float, "float" )
	else SetVectorComponentType( double, "double" )
	else SetVectorComponentType( long, "long" )
	else SetVectorComponentType( unsigned long, "unsigned long" )
}

template <class ImageType>
ITKImageVectorImpl<ImageType>::~ITKImageVectorImpl()
{
}

template <class ImageType>
boost::any Core::ITKImageVectorImpl<ImageType>::GetDataPtr() const
{
	return m_Data;
}

template <class ImageType>
void Core::ITKImageVectorImpl<ImageType>::ResetData()
{
	m_Data = ImageType::New();
}

template <class ImageType>
void Core::ITKImageVectorImpl<ImageType>::SetAnyData( boost::any val )
{
	m_Data = boost::any_cast<typename ImageType::Pointer> ( val );
}

template <class ImageType>
bool Core::ITKImageVectorImpl<ImageType>::IsValidType( const std::string &datatypename )
{
	return ( datatypename == typeid( typename ImageType::Pointer ).name( ) ||
			 datatypename == typeid( ImageType* ).name( ) );
}

template <class ImageType>
void Core::ITKImageVectorImpl<ImageType>::SetDataPtr( boost::any val )
{
	if ( val.type() == typeid( typename ImageType::Pointer ) )
	{
		m_Data = boost::any_cast<typename ImageType::Pointer> ( val );
	}
	else if ( val.type() == typeid( ImageType* ) )
	{
		m_Data = boost::any_cast<ImageType*> ( val );
	}
	else
	{
		throw Exceptions::DataTypeNotValidException( "ITKImageVectorImpl<ImageType>::SetDataPtr" );
	}
}

template <class ImageType>
void Core::ITKImageVectorImpl<ImageType>::SetData( 
	blTagMap::Pointer tagMap, ImportMemoryManagementType mem/* = gmCopyMemory*/ )
{
	blTag::Pointer tag;

	ResetData();

	tag = SafeFindTag( tagMap, "Origin" );
	std::vector<double> origin = tag->GetValueCasted< std::vector<double> >();

	tag = SafeFindTag( tagMap, "Spacing" );
	std::vector<double> spacing = tag->GetValueCasted< std::vector<double> >();

	tag = SafeFindTag( tagMap, "Dimensions" );
	std::vector<int> dimensions = tag->GetValueCasted< std::vector<int> >();
	typename ImageType::RegionType region;
	for ( int i = 0 ; i < ImageType::ImageDimension ; i++ )
	{
		region.SetSize( i, dimensions[ i ] );
	}

	tag = SafeFindTag( tagMap, "ScalarPointer" );
	void *scalarPointer = tag->GetValueCasted<void*>();

	tag = SafeFindTag( tagMap, "ScalarType" );
	std::string scalarType = tag->GetValueCasted<std::string>();

	tag = SafeFindTag( tagMap, "NumberOfComponents" );
	int numComponents = tag->GetValueCasted<int>();
	if ( numComponents != ImageType::PixelType::Dimension )
	{
		std::stringstream stream;
		stream << "Number of components cannot be different of " << ImageType::PixelType::Dimension;
		return throw Core::Exceptions::RuntimeException( 
			"ITKImageImpl<ImageType>::SetData", 
			 stream.str().c_str() );
	}

	if ( numComponents == 2 )
	{
		CastVectorData( "char", char, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "unsigned char", unsigned char, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "int", int, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "unsigned int", unsigned int, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "short", short, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "unsigned short", unsigned short, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "float", float, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "double", double, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "long", long, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "unsigned long", unsigned long, ImageType::ImageDimension, ImageType::PixelType::Dimension)
	}
	else if ( numComponents == 3 )
	{
		CastVectorData( "char", char, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "unsigned char", unsigned char, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "int", int, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "unsigned int", unsigned int, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "short", short, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "unsigned short", unsigned short, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "float", float, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "double", double, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "long", long, ImageType::ImageDimension, ImageType::PixelType::Dimension)
		else CastVectorData( "unsigned long", unsigned long, ImageType::ImageDimension, ImageType::PixelType::Dimension)
	}
}

template <class ImageType>
void Core::ITKImageVectorImpl<ImageType>::GetData( blTagMap::Pointer tagMap )
{
	std::vector<double> temp1;
	std::vector<int> temp2;

	const typename ImageType::PointType & itkOrigin = m_Data->GetOrigin();
	temp1.assign( &itkOrigin[ 0 ], &itkOrigin[ 0 ] + ImageType::ImageDimension );
	tagMap->AddTag( "Origin", temp1 );

	const typename ImageType::SpacingType & itkspacing = m_Data->GetSpacing();
	temp1.assign( &itkspacing[ 0 ], &itkspacing[ 0 ] + ImageType::ImageDimension );
	tagMap->AddTag( "Spacing", temp1 );

	temp2.resize( ImageType::ImageDimension );
	for ( int i = 0 ; i < ImageType::ImageDimension ; i++ )
	{
		temp2[ i ] = m_Data->GetLargestPossibleRegion().GetSize().GetSize()[i];
	}
	tagMap->AddTag( "Dimensions", temp2 );

	tagMap->AddTag( "ScalarType", m_ComponentType );

	tagMap->AddTag( "ScalarPointer", (void*) m_Data->GetBufferPointer() );

	size_t sizeInBytes = m_Data->GetPixelContainer( )->Size( ) * sizeof( typename ImageType::PixelType );
	tagMap->AddTag( "ScalarSizeInBytes", sizeInBytes );

	int numComponents = ImageType::PixelType::Dimension;
	tagMap->AddTag( "NumberOfComponents", numComponents );
}

