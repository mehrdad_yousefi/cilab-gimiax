/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreMITKImageImpl.h"
#include "coreVTKImageDataImpl.h"

using namespace Core;

/**
*/
Core::MitkImageImpl::MitkImageImpl( ) 
{
	ResetData( );
}

Core::MitkImageImpl::~MitkImageImpl()
{

}

boost::any Core::MitkImageImpl::GetDataPtr() const
{
	return m_Data;
}

void Core::MitkImageImpl::SetData( 
	blTagMap::Pointer tagMap, ImportMemoryManagementType mem/* = gmCopyMemory*/ )
{
	blTag::Pointer tagDataEntitySetImplSet = SafeFindTag( tagMap, "DataEntityImplSet" );

	DataEntityImplSetType setOfDataEntitySetImpl;
	setOfDataEntitySetImpl = tagDataEntitySetImplSet->GetValueCasted<DataEntityImplSetType>();

	ResetData( );
	if ( setOfDataEntitySetImpl.size() )
	{
		SetSize( setOfDataEntitySetImpl.size(), *setOfDataEntitySetImpl.begin() );
	}

	DataEntityImplSetType::iterator it;
	int time = 0;
	for ( it = setOfDataEntitySetImpl.begin() ; it != setOfDataEntitySetImpl.end() ; it++)
	{
		// Create a new VtkPolyDataImpl
		BaseFactory::Pointer factory;
		factory = DataEntityImplFactoryHelper::FindFactoryBySimilarType( "vtkImageData" );
		DataEntityImpl::Pointer dataEntityImpl;
		dataEntityImpl = DataEntityImpl::SafeDownCast( factory->CreateInstance() );

		// Copy data
		dataEntityImpl->DeepCopy( *it, mem );

		// Set data
		SetAt( time, dataEntityImpl.GetPointer(), mem );

		time++;
	}

	SetMemoryOwner( "MITKPlugin" );
}

void Core::MitkImageImpl::GetData( blTagMap::Pointer tagMap )
{

	DataEntityImplSetType dataEntityImplSet;
	int i = 0;
	while ( m_Data->IsVolumeSet( i ))
	{
		dataEntityImplSet.push_back( GetAt( i ) );
		i++;
	}

	tagMap->AddTag( "DataEntityImplSet", dataEntityImplSet );
}

void Core::MitkImageImpl::ResetData()
{
	if ( m_Data.IsNull() || GetSize() > 0 )
	{
		m_Data = mitk::Image::New();
	}
	m_Origin[ 0 ] = 0;
	m_Origin[ 1 ] = 0;
	m_Origin[ 2 ] = 0;
}

void Core::MitkImageImpl::SwitchImplementation( const std::type_info &type )
{
	throw Exceptions::NotImplementedException( "MitkImageImpl::SwitchImplementation" );
}

size_t Core::MitkImageImpl::GetSize() const
{
	int i = 0;
	while ( m_Data->IsVolumeSet( i ))
	{
		i++;
	}
	return i;
}

DataEntityImpl::Pointer Core::MitkImageImpl::GetAt( size_t pos )
{
	vtkImageDataPtr data = m_Data->GetVtkImageData( pos );
	data->SetOrigin( m_Origin[0], m_Origin[1], m_Origin[2] );
	VtkImageDataImpl::Pointer dataImpl = VtkImageDataImpl::New( );
	dataImpl->SetDataPtr( data );
	return dataImpl.GetPointer();
}


void Core::MitkImageImpl::SetAt( 
	size_t pos, 
	DataEntityImpl::Pointer data,
	ImportMemoryManagementType mem /*= gmCopyMemory*/ )
{
	if ( data->GetDataPtr().type() != typeid( vtkImageDataPtr ) )
	{
		throw Exceptions::DataTypeNotValidException( "MitkImageImpl::SetAt" );
	}

	vtkImageDataPtr timeStepData;
	timeStepData = boost::any_cast<vtkImageDataPtr>( data->GetDataPtr() );

	switch( mem )
	{
	case gmCopyMemory:
		m_Data->SetImportVolume( 
			timeStepData->GetScalarPointer(), pos, 0, mitk::Image::CopyMemory );
		SetMemoryOwner( "MITKPlugin" );
		break;
	case gmManageMemory:
		m_Data->SetImportVolume( 
			timeStepData->GetScalarPointer(), pos, 0, mitk::Image::ManageMemory );
		SetMemoryOwner( "MITKPlugin" );
		break;
	case gmReferenceMemory:
		m_Data->SetImportVolume( 
			timeStepData->GetScalarPointer(), pos, 0, mitk::Image::ReferenceMemory );
		SetMemoryOwner( "Unknown" );
		break;
	}

	m_Origin[0] = mitk::Point3D::ValueType( timeStepData->GetOrigin()[0] );
	m_Origin[1] = mitk::Point3D::ValueType( timeStepData->GetOrigin()[1] );
	m_Origin[2] = mitk::Point3D::ValueType( timeStepData->GetOrigin()[2] );
	m_Data->GetGeometry( pos )->SetOrigin( m_Origin );
}

void* Core::MitkImageImpl::GetVoidPtr() const
{
	void* ptr; ptr = m_Data; return ptr;
}

void Core::MitkImageImpl::SetVoidPtr( void* ptr )
{
	m_Data = reinterpret_cast<mitk::Image*> ( ptr );
	SetMemoryOwner( "Unknown" );
}

void Core::MitkImageImpl::SetSize( size_t size, DataEntityImpl::Pointer data )
{
	// Create a new VtkPolyDataImpl
	BaseFactory::Pointer factory;
	factory = DataEntityImplFactoryHelper::FindFactoryBySimilarType( "vtkImageData" );
	DataEntityImpl::Pointer dataEntityImpl;
	dataEntityImpl = DataEntityImpl::SafeDownCast( factory->CreateInstance() );

	// Reference data
	dataEntityImpl->DeepCopy( data, gmReferenceMemory );

	vtkImageDataPtr timeStepData;
	timeStepData = boost::any_cast<vtkImageDataPtr>( dataEntityImpl->GetDataPtr() );
	m_Data->Initialize( timeStepData, 1, size );
}

void Core::MitkImageImpl::SetAnyData( boost::any val )
{
	m_Data = boost::any_cast<mitk::Image::Pointer> ( val );
	if ( m_Data.GetPointer( ) == NULL )
	{
		throw Exceptions::DataPtrIsNullException( "MitkImageImpl::SetDataPtr" );
	}

	SetMemoryOwner( "Unknown" );
}

