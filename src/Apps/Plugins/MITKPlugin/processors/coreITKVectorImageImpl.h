/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreITKVectorImageImpl_H
#define _coreITKVectorImageImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "itkImage.h"
#include <typeinfo>

namespace Core
{
/** 
ITKImageDataEntityBuilder inherits from abstract class DataEntityBuilder 
and enables building DataEntity from ITKImageData 

- "Origin": std::vector<double>
- "Spacing": std::vector<double>
- "Dimensions": std::vector<int>
- "Buffer": void*

\ingroup MITKPlugin
\author Xavi Planes
\date June 2012
*/
template < class ImageType, class TimeStepImageType >
class ITKVectorImageImpl : public DataEntityImpl
{
public:
	typedef ITKVectorImageImpl<ImageType, TimeStepImageType> M_ITKVectorImageImpl;

	coreDeclareSmartPointerClassMacro( M_ITKVectorImageImpl, DataEntityImpl)

	coreDefineMultipleDataFactory2Types( M_ITKVectorImageImpl, typename ImageType::Pointer, ImageType*, ImageTypeId )

	//@{ 
	/// \name Interface

	virtual size_t GetSize() const;
	virtual void SetSize(size_t size, DataEntityImpl::Pointer data);
	virtual DataEntityImpl::Pointer GetAt( size_t num );
	virtual void SetAt( 
		size_t pos, 
		DataEntityImpl::Pointer data,
		ImportMemoryManagementType mem = gmCopyMemory );
	virtual void SwitchImplementation( const std::type_info &type );

	virtual boost::any GetDataPtr() const;
	virtual void ResetData( );
	virtual void* GetVoidPtr( ) const;
	virtual void SetVoidPtr( void* ptr);

private:
	virtual void SetAnyData( boost::any val );
	virtual void SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem = gmCopyMemory );
	virtual void GetData( blTagMap::Pointer tagMap );
	//@}

protected:
	//!
	ITKVectorImageImpl();
	//!
	~ITKVectorImageImpl();

private:

	//!
	typename ImageType::Pointer m_Data;
};

}

#include "coreITKVectorImageImpl.txx"

#endif // _coreITKVectorImageImpl_H
