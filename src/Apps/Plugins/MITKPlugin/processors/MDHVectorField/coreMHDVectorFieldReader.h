/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreMHDVectorFieldReader_H
#define coreMHDVectorFieldReader_H

#include "coreBaseDataEntityReader.h"

namespace Core
{
namespace IO
{
/** 

\ingroup MITKPlugin
\author Mart�n Bianculli
\date 08 Aug 2008
*/
class PLUGIN_EXPORT MHDVectorFieldReader : public BaseDataEntityReader
{
public:
	coreDeclareSmartPointerClassMacro(
		Core::IO::MHDVectorFieldReader, 
		BaseDataEntityReader);

	virtual void ReadData( );

protected:
	MHDVectorFieldReader(void);
	virtual ~MHDVectorFieldReader(void);

	//!
	virtual boost::any ReadSingleTimeStep( 
		int iTimeStep, 
		const std::string &filename );

private:
	coreDeclareNoCopyConstructors(MHDVectorFieldReader);
};

}
}

#endif //coreVTKImageData
