/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreBlMitkSurfaceImpl_H
#define _coreBlMitkSurfaceImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "blMitkSurface.h"
#include "coreMITKSurfaceImpl.h"

namespace Core{

/**
Set of DataEntityImpl
\author Xavi Planes
\date 06 Sept 2010
\ingroup MITKPlugin
*/
class PLUGIN_EXPORT BlMitkSurfaceImpl : public Core::MitkSurfaceImpl
{
public:
	coreDeclareSmartPointerClassMacro( Core::BlMitkSurfaceImpl, Core::MitkSurfaceImpl )

	coreDefineMultipleDataFactory( Core::BlMitkSurfaceImpl, blMitk::Surface::Pointer, SurfaceMeshTypeId )


public:

	//@{ 
	/// \name Interface
	virtual void ResetData( )
	{
		m_Data = blMitk::Surface::New( );
	}

private:
	//@}


protected:
	//!
	BlMitkSurfaceImpl( )
	{
		ResetData( );
	}

	//!
	virtual ~BlMitkSurfaceImpl( )
	{

	}

protected:

};

} // end namespace Core

#endif // _coreBlMitkSurfaceImpl_H

