/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreITKDTITensorImpl_H
#define _coreITKDTITensorImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "itkDTITensor.h"
#include "itkDTITensor.txx"

namespace Core{

/**

Data entity for storing tensors 

\author Chiara Riccobene
\date 07 June 2010
\ingroup MITKPlugin
*/

class PLUGIN_EXPORT ITKDTITensorImpl : public DataEntityImpl
{
public:
	typedef itk::DTITensor<float> ComponentType;
	typedef itk::Image<ComponentType,3>::Pointer DataType;

public:

	coreDeclareSmartPointerClassMacro( Core::ITKDTITensorImpl, DataEntityImpl );

	coreDefineSingleDataFactory( Core::ITKDTITensorImpl, DataType, TensorTypeId )

	//@{ 
	/// \name Interface
public:
	boost::any GetDataPtr() const;

private:
	virtual void SetAnyData( boost::any val );
	virtual void ResetData( );
	//@}


protected:
	//!
	ITKDTITensorImpl( );

	//!
	virtual ~ITKDTITensorImpl();

	//! Not implemented
	ITKDTITensorImpl(const Self&);

	//! Not implemented
   	void operator=(const Self&);

private:
	//!
	DataType m_Data;

};


}

#endif //_coreITKDTITensorImpl_H
