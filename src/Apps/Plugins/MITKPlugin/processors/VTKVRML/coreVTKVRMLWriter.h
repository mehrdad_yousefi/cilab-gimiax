/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreVTKVRMLWriter_H
#define coreVTKVRMLWriter_H

#include "coreBaseDataEntityWriter.h"

namespace Core
{
namespace IO
{
/** 
Write surface meshes as VRML data files

\ingroup MITKPlugin
\author Xavi Planes
\date Mar 2012
*/
class PLUGIN_EXPORT VTKVRMLWriter : public BaseDataEntityWriter
{
public:
	coreDeclareSmartPointerClassMacro(
		Core::IO::VTKVRMLWriter, 
		BaseDataEntityWriter);

	void WriteData( );

protected:
	VTKVRMLWriter(void);
	virtual ~VTKVRMLWriter(void);

	//! Write Single data 
	void WriteSingleTimeStep( 
		const std::string& fileName, 
		Core::DataEntity::Pointer dataEntity,
		int iTimeStep );

	//! Write Single data 
	void WriteDataEntity( 
		Core::DataEntity::Pointer dataEntity,
		vtkSmartPointer<vtkRenderer> ren );

private:
	coreDeclareNoCopyConstructors(VTKVRMLWriter);
};

}
}

#endif
