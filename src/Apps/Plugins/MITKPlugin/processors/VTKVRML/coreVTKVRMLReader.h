/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreVTKVRMLReader_H
#define _coreVTKVRMLReader_H

#include "coreBaseDataEntityReader.h"

namespace Core
{
namespace IO
{
/** 
Read surface meshes from VRML data files

\ingroup MITKPlugin
\author Xavi Planes
\date Mar 2012
*/
class PLUGIN_EXPORT VTKVRMLReader : public BaseDataEntityReader
{
public:
	coreDeclareSmartPointerClassMacro(
		Core::IO::VTKVRMLReader, 
		BaseDataEntityReader);

	void ReadData( );

protected:
	VTKVRMLReader(void);
	virtual ~VTKVRMLReader(void);

	//!
	virtual boost::any ReadSingleTimeStep( 
		int iTimeStep, 
		const std::string &filename );

private:
	coreDeclareNoCopyConstructors(VTKVRMLReader);
};

}
}

#endif
