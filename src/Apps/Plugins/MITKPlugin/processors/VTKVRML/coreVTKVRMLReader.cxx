/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreVTKVRMLReader.h"
#include "vtkVRMLImporter.h"
#include "vtkSmartPointer.h"
#include "coreVTKPolyDataHolder.h"
#include "coreFile.h"
#include "vtkProperty.h"

using namespace Core::IO;

//!
VTKVRMLReader::VTKVRMLReader(void) 
{
	m_ValidExtensionsList.push_back( ".wrl" );
	m_ValidTypesList.push_back( SurfaceMeshTypeId );
}

//!
VTKVRMLReader::~VTKVRMLReader(void)
{
}

//!
void VTKVRMLReader::ReadData( )
{
	if ( m_Filenames.size() == 0 )
	{
		return ;
	}

	bool warning = vtkObject::GetGlobalWarningDisplay();
	vtkObject::SetGlobalWarningDisplay( false );

	vtkSmartPointer<vtkVRMLImporter> VRMLImporter = vtkSmartPointer<vtkVRMLImporter>::New( );
	VRMLImporter->SetFileName ( m_Filenames[ 0 ].c_str( ) );
	VRMLImporter->Update( );
	vtkRenderer* renderer = VRMLImporter->GetRenderer();
	if ( renderer == NULL )
	{
		throw Core::Exceptions::Exception( "VTKVRMLReader::ReadData", 
			"Cannot find data objects in VRML file" );
	}

	vtkActorCollection* actors = renderer->GetActors();

	// Create one DataEntity for each object
	SetNumberOfOutputs( actors->GetNumberOfItems() );
	vtkActor *actor;
	actors->InitTraversal();
	actor = actors->GetNextActor();
	unsigned i = 0;
	while (  actor )
	{
		// Get dataset
		vtkMapper* mapper = actor->GetMapper( );
		vtkDataSet *dataset = mapper->GetInput( );
		if ( !dataset->IsA( "vtkPolyData" ) )
		{
			continue;
		}

		// Create name
		GetOutputPort( i )->SetDataEntityType( Core::SurfaceMeshTypeId );
		std::string name = Core::IO::File::GetFilenameName( m_Filenames[ 0 ] );
		name = itksys::SystemTools::GetFilenameWithoutExtension( name );
		std::stringstream sstream;
		sstream << name << i;

		// Copy dataset
		Core::vtkPolyDataPtr pSurface = Core::vtkPolyDataPtr::New();
		pSurface->DeepCopy( vtkPolyData::SafeDownCast( dataset ) );

		// Create meta data
		mitk::Color color;
		blTagMap::Pointer metaData = blTagMap::New( );
		blTagMap::Pointer rendering = blTagMap::New( );
		blTagMap::Pointer material = blTagMap::New( );
		rendering->AddTag( "material", material );
		metaData->AddTag( "Rendering", rendering );

		// Disable scalar visibility
		rendering->AddTag("scalar visibility", false );

		vtkProperty* prop = actor->GetProperty( );

		// Ambient
		color.Set( prop->GetAmbientColor( )[ 0 ], prop->GetAmbientColor( )[ 1 ], prop->GetAmbientColor( )[ 2 ] );
		std::stringstream stream1;
		stream1 << color;
		material->AddTag( "AmbientColor", stream1.str( ) );
		material->AddTag( "AmbientCoefficient", prop->GetAmbient( ) );

		// Diffuse
		color.Set( prop->GetDiffuseColor( )[ 0 ], prop->GetDiffuseColor( )[ 1 ], prop->GetDiffuseColor( )[ 2 ] );
		std::stringstream stream2;
		stream2 << color;
		material->AddTag( "DiffuseColor", stream2.str( ) );
		material->AddTag( "DiffuseCoefficient", prop->GetDiffuse( ) );

		// Specular
		color.Set( prop->GetSpecularColor( )[ 0 ], prop->GetSpecularColor( )[ 1 ], prop->GetSpecularColor( )[ 2 ] );
		std::stringstream stream3;
		stream3 << color;
		material->AddTag( "SpecularColor", stream3.str( ) );
		material->AddTag( "SpecularCoefficient", prop->GetSpecular( ) );
		material->AddTag( "SpecularPower", prop->GetSpecularPower( ) );

		// Color
		color.Set( prop->GetColor( )[ 0 ], prop->GetColor( )[ 1 ], prop->GetColor( )[ 2 ] );
		std::stringstream stream4;
		stream4 << color;
		material->AddTag( "Color", stream4.str( ) );

		// Other
		material->AddTag( "Opacity", prop->GetOpacity( ) );
		material->AddTag( "Interpolation", prop->GetInterpolation( ) );
		material->AddTag( "Representation", prop->GetRepresentation( )  );
		material->AddTag( "LineWidth", double( prop->GetLineWidth( ) ) );
		material->AddTag( "PointSize", double( prop->GetPointSize( ) ) );


		// Update output
		UpdateOutput( i, pSurface, sstream.str( ), true, 1, 0, metaData );
		if ( GetOutputDataEntity( i ).IsNotNull() )
		{
			GetOutputDataEntity( i )->GetMetadata( )->AddTag( "FilePath", m_Filenames[ 0 ] );
		}

		// Get next iteration
		actor = actors->GetNextActor();
		i++;
	}

	vtkObject::SetGlobalWarningDisplay( warning );
}



boost::any VTKVRMLReader::ReadSingleTimeStep( int iTimeStep, const std::string &filename )
{
	return boost::any( );
}
