/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreVTKVRMLWriter.h"
#include "coreDataEntity.h"
#include <blShapeUtils.h>
#include "coreVTKPolyDataHolder.h"
#include "coreDataEntityInfoHelper.h"
#include "coreException.h"
#include "coreVTKPolyDataImpl.h"
#include "vtkPolyDataMapper.h"
#include "vtkVRMLExporter.h"

using namespace Core::IO;

//!
VTKVRMLWriter::VTKVRMLWriter(void) 
{
	m_ValidExtensionsList.push_back( ".wrl" );
	m_ValidTypesList.push_back( SurfaceMeshTypeId );
}

//!
VTKVRMLWriter::~VTKVRMLWriter(void)
{
}

void VTKVRMLWriter::WriteData()
{
	if( GetInputDataEntity( 0 ).IsNull() )
	{
		throw Core::Exceptions::Exception( 
			"VTKVRMLWriter::WriteData()", 
			"Input data entity is NULL" );
	}

	// Check if directory exists
	int iTimeStep = GetInputPort( 0 )->GetSelectedTimeStep();
	std::string strFileName = GetTimeStepFilename( iTimeStep );
	std::string path = itksys::SystemTools::GetFilenamePath( strFileName );
	if ( !path.empty( ) && !itksys::SystemTools::FileExists( path.c_str(), false ) )
	{
		throw Core::Exceptions::Exception(
			"VTKVRMLWriter::WriteData",
			"Output directory doesn't exist" );
	}
	
	bool warning = vtkObject::GetGlobalWarningDisplay();
	vtkObject::SetGlobalWarningDisplay( false );

	try
	{
		// Create RenderWindow
		vtkSmartPointer<vtkRenderWindow> renWin = vtkSmartPointer<vtkRenderWindow>::New( );
		vtkSmartPointer<vtkRenderer> ren = vtkSmartPointer<vtkRenderer>::New();
		renWin->AddRenderer(ren);

		// Iterate over the DataEntities
		WriteDataEntity( GetInputDataEntity( 0 ), ren );

		vtkSmartPointer<vtkVRMLExporter> exporter = vtkSmartPointer<vtkVRMLExporter>::New( );
		exporter->SetRenderWindow ( renWin );
		exporter->SetFileName( strFileName.c_str( ) );
		exporter->Update( );
	}
	catch( ... )
	{
		vtkObject::SetGlobalWarningDisplay( warning );
		throw;
	}

	vtkObject::SetGlobalWarningDisplay( warning );

}

void Core::IO::VTKVRMLWriter::WriteDataEntity( 
	Core::DataEntity::Pointer dataEntity, 
	vtkSmartPointer<vtkRenderer> ren )
{
	vtkPolyDataPtr surface = NULL;

	int iTimeStep = GetInputPort( 0 )->GetSelectedTimeStep();
	bool worked = dataEntity->GetProcessingData( surface, iTimeStep );

	// Convert always to VTK to save
	if ( surface.GetPointer() == NULL )
	{
		VtkPolyDataImpl::Pointer polyDataTimeStep;
		polyDataTimeStep = VtkPolyDataImpl::New( );
		polyDataTimeStep->DeepCopy( dataEntity->GetTimeStep( iTimeStep ) );
		CastAnyProcessingData( polyDataTimeStep->GetDataPtr( ), surface );
	}

	if( !worked || surface == NULL )
	{
		return;
	}

	// Create actor and mapper
	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New( );
	mapper->SetInput( surface );

	vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper( mapper );

	ren->AddActor( actor );

	// Iterate over children
	Core::DataEntity::ChildrenListType childrenList = dataEntity->GetChildrenList();
	for ( int i = 0 ; i < childrenList.size() ;i++ )
	{
		WriteDataEntity( childrenList[ i ], ren );
	}


}


void Core::IO::VTKVRMLWriter::WriteSingleTimeStep( 
	const std::string& fileName, 
	Core::DataEntity::Pointer dataEntity, 
	int iTimeStep )
{
}
