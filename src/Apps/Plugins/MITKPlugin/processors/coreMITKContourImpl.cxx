/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreMITKContourImpl.h"

using namespace Core;

/**
*/
Core::MitkContourImpl::MitkContourImpl( ) 
{
	ResetData( );
}

Core::MitkContourImpl::~MitkContourImpl()
{

}

boost::any Core::MitkContourImpl::GetDataPtr() const
{
	return m_Data;
}

void Core::MitkContourImpl::ResetData()
{
	m_Data = mitk::Contour::New();
}

void Core::MitkContourImpl::SetAnyData( boost::any val )
{
	m_Data = boost::any_cast<mitk::Contour::Pointer> ( val );
	SetMemoryOwner( "Unknown" );
}


void Core::MitkContourImpl::SetData(
	blTagMap::Pointer tagMap, ImportMemoryManagementType mem /*= gmCopyMemory */ )
{
	blTag::Pointer tagPoints = SafeFindTag( tagMap, "Points" );
	std::vector<Point3D>* points;
	points = tagPoints->GetValueCasted< std::vector<Point3D>* >();

	m_Data->Initialize( );

	std::vector<Point3D>::iterator	it, end;
	for( it = points->begin(); it != points->end(); it++ )
	{
		mitk::PointSet::PointType point;
		point[ 0 ] = (*it)[ 0 ];
		point[ 1 ] = (*it)[ 1 ];
		point[ 2 ] = (*it)[ 2 ];
		m_Data->AddVertex( point );
	}

	SetMemoryOwner( "MITKPlugin" );
}

void Core::MitkContourImpl::GetData( blTagMap::Pointer tagMap )
{
	if ( GetGenerateTemporalData() && m_Data.IsNotNull( ) )
	{
		mitk::Contour::PointsContainerPointer points = m_Data->GetPoints( );
		mitk::Contour::PointsContainer::Iterator it, end;

		Point3D point;
		point.resize( 3 );

		for ( it = points->Begin( ) ; it != points->End( ) ; it++ )
		{
			mitk::PointSet::PointType out;
			if ( points->GetElementIfIndexExists( it->Index( ), &out ) )
			{
				point[ 0 ] = it->Value( )[ 0 ];
				point[ 1 ] = it->Value( )[ 1 ];
				point[ 2 ] = it->Value( )[ 2 ];
				m_Points.push_back( point );
			}
		}

		tagMap->AddTag( "Points", &m_Points );
		tagMap->AddTag( "SurfaceElements", &m_SurfaceElements );
	}

	tagMap->AddTag( "DataPtr", m_Data );
}

void Core::MitkContourImpl::CleanTemporalData()
{
	m_Points.clear();
	m_SurfaceElements.clear( );
}

