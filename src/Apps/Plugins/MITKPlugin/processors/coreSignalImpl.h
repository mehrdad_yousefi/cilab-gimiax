/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreSignalImpl_H
#define _coreSignalImpl_H

#include "corePluginMacros.h"
#include "coreDataEntityImplFactory.h"
#include "blSignalCollective.h"
#include "corePluginMacros.h"

namespace Core{

/**

Data entity for storing signals

\author Xavi Planes
\date 24 April 2009
\ingroup MITKPlugin
*/

class PLUGIN_EXPORT SignalImpl : public DataEntityImpl
{
public:
	typedef blSignalCollective::Pointer DataType;

public:
	coreDeclareSmartPointerClassMacro( Core::SignalImpl, DataEntityImpl );

	coreDefineMultipleDataFactory( Core::SignalImpl, DataType, SignalTypeId )


	//@{ 
	/// \name Interface
public:
	boost::any GetDataPtr() const;

private:
	virtual void SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem = gmCopyMemory );
	virtual void GetData( blTagMap::Pointer tagMap );
	virtual void SetAnyData( boost::any val );
	virtual void ResetData( );
	//@}

protected:
	//!
	SignalImpl( );

	//!
	virtual ~SignalImpl();

	//! Not implemented
	SignalImpl(const Self&);

	//! Not implemented
   	void operator=(const Self&);

private:

	//!
	DataType m_Data;
};


}

#endif //_coreSignalImpl_H
