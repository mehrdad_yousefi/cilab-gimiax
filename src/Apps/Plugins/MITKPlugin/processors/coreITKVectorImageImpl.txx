/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreITKVectorImageImpl.h"
#include <typeinfo>
#include "itkJoinSeriesImageFilter.h"

using namespace Core;

template <class ImageType, class TimeStepImageType>
ITKVectorImageImpl<ImageType,TimeStepImageType>::ITKVectorImageImpl()
{
	ResetData();
}

template <class ImageType, class TimeStepImageType>
ITKVectorImageImpl<ImageType,TimeStepImageType>::~ITKVectorImageImpl()
{
}

template <class ImageType, class TimeStepImageType>
boost::any Core::ITKVectorImageImpl<ImageType,TimeStepImageType>::GetDataPtr() const
{
	return m_Data;
}

template <class ImageType, class TimeStepImageType>
void Core::ITKVectorImageImpl<ImageType,TimeStepImageType>::SetData( 
	blTagMap::Pointer tagMap, ImportMemoryManagementType mem/* = gmCopyMemory*/ )
{
	blTag::Pointer tagDataEntitySetImplSet = SafeFindTag( tagMap, "DataEntityImplSet" );

	DataEntityImplSetType setOfDataEntitySetImpl;
	setOfDataEntitySetImpl = tagDataEntitySetImplSet->GetValueCasted<DataEntityImplSetType>();

	ResetData( );

	typedef itk::JoinSeriesImageFilter<TimeStepImageType, ImageType> ImageFilter3Dto4D;
	typename ImageFilter3Dto4D::Pointer imageFilter3Dto4D =  ImageFilter3Dto4D::New();

	DataEntityImplSetType::iterator it;
	int time = 0;
	for ( it = setOfDataEntitySetImpl.begin() ; it != setOfDataEntitySetImpl.end() ; it++)
	{
		// Create a new VtkPolyDataImpl
		BaseFactory::Pointer factory;
		factory = DataEntityImplFactoryHelper::FindFactoryBySimilarType( typeid( TimeStepImageType ).name() );
		if ( factory.IsNull( ) )
		{
			throw Exceptions::FactoryNotFoundException( "ITKVectorImageImpl<ImageType,TimeStepImageType>::SetData" );
		}

		DataEntityImpl::Pointer dataEntityImpl;
		dataEntityImpl = DataEntityImpl::SafeDownCast( factory->CreateInstance() );

		// Copy data
		dataEntityImpl->DeepCopy( *it, mem );

		typename TimeStepImageType::Pointer image;
		image = boost::any_cast<typename TimeStepImageType::Pointer>( dataEntityImpl->GetDataPtr() );

		imageFilter3Dto4D->PushBackInput( image );

		time++;
	}

	imageFilter3Dto4D->Update();
	m_Data = imageFilter3Dto4D->GetOutput( );
}

template <class ImageType, class TimeStepImageType>
void Core::ITKVectorImageImpl<ImageType,TimeStepImageType>::GetData( blTagMap::Pointer tagMap )
{
	DataEntityImplSetType dataEntityImplSet;

	unsigned int    timeDimension = ImageType::ImageDimension - 1;
    for (unsigned int time = 0; time<m_Data->GetBufferedRegion().GetSize()[timeDimension]; time++)
    {
		dataEntityImplSet.push_back( GetAt( time ) );
	}

	tagMap->AddTag( "DataEntityImplSet", dataEntityImplSet );
}


template <class ImageType, class TimeStepImageType>
void Core::ITKVectorImageImpl<ImageType,TimeStepImageType>::ResetData()
{
	m_Data = ImageType::New( );
}

template <class ImageType, class TimeStepImageType>
void Core::ITKVectorImageImpl<ImageType,TimeStepImageType>::SwitchImplementation( const std::type_info &type )
{
	throw Exceptions::NotImplementedException( "ITKVectorImageImpl<ImageType,TimeStepImageType>::SwitchImplementation" );
}

template <class ImageType, class TimeStepImageType>
size_t Core::ITKVectorImageImpl<ImageType,TimeStepImageType>::GetSize() const
{
    typename ImageType::SizeType size = m_Data->GetLargestPossibleRegion( ).GetSize( );
	return size[ ImageType::ImageDimension - 1 ];
}

template <class ImageType, class TimeStepImageType>
DataEntityImpl::Pointer Core::ITKVectorImageImpl<ImageType,TimeStepImageType>::GetAt( size_t pos )
{
	// Create timestep image
    typename TimeStepImageType::Pointer timeStepImage = TimeStepImageType::New();
    typename TimeStepImageType::RegionType region;
    typename TimeStepImageType::SizeType size;
    typename TimeStepImageType::IndexType index;
    typename TimeStepImageType::PointType  origin;
    typename TimeStepImageType::SpacingType  spacing;

	// Init values
    for (unsigned int d=0; d<TimeStepImageType::ImageDimension; d++)
    {
        origin[d] = m_Data->GetOrigin()[d];
        index[d] = m_Data->GetBufferedRegion().GetIndex()[d];
        size[d] = m_Data->GetBufferedRegion().GetSize()[d];
        spacing[d] = m_Data->GetSpacing()[d];
    }
    region.SetIndex(index);
    region.SetSize(size);

	// Initialize image
    timeStepImage->SetRegions(region);
    timeStepImage->SetOrigin(origin);
    timeStepImage->SetSpacing(spacing);

	// Set pointer to buffer
    unsigned long numberOfVoxelsPerTimePoint = m_Data->GetBufferedRegion().GetNumberOfPixels() 
		/ m_Data->GetBufferedRegion().GetSize()[ImageType::ImageDimension - 1];
    typename ImageType::PixelType* pixelContainer = m_Data->GetPixelContainer()->GetBufferPointer() + numberOfVoxelsPerTimePoint*pos;
    timeStepImage->GetPixelContainer()->SetImportPointer( pixelContainer, numberOfVoxelsPerTimePoint );

	// Create DataEntityImpl
	BaseFactory::Pointer factory;
	factory = DataEntityImplFactoryHelper::FindFactoryBySimilarType( typeid( TimeStepImageType ).name() );
	if ( factory.IsNull( ) )
	{
		throw Exceptions::FactoryNotFoundException( "ITKVectorImageImpl<ImageType,TimeStepImageType>::GetAt" );
	}

	DataEntityImpl::Pointer dataEntityImpl;
	dataEntityImpl = DataEntityImpl::SafeDownCast( factory->CreateInstance() );
	dataEntityImpl->SetDataPtr( timeStepImage );
	return dataEntityImpl.GetPointer();
}

template <class ImageType, class TimeStepImageType>
void Core::ITKVectorImageImpl<ImageType,TimeStepImageType>::SetAt( 
	size_t pos, 
	DataEntityImpl::Pointer data,
	ImportMemoryManagementType mem /*= gmCopyMemory*/ )
{
	throw Exceptions::NotImplementedException( "ITKVectorImageImpl<ImageType,TimeStepImageType>::SetAt" );
}

template <class ImageType, class TimeStepImageType>
void* Core::ITKVectorImageImpl<ImageType,TimeStepImageType>::GetVoidPtr() const
{
	void* ptr; ptr = m_Data; return ptr;
}

template <class ImageType, class TimeStepImageType>
void Core::ITKVectorImageImpl<ImageType,TimeStepImageType>::SetVoidPtr( void* ptr )
{
	m_Data = reinterpret_cast<ImageType*> ( ptr );
}

template <class ImageType, class TimeStepImageType>
void Core::ITKVectorImageImpl<ImageType,TimeStepImageType>::SetSize( size_t size, DataEntityImpl::Pointer data )
{
	// Create a new Time step image
	BaseFactory::Pointer factory;
	factory = DataEntityImplFactoryHelper::FindFactoryBySimilarType( typeid( TimeStepImageType ).name( ) );
	if ( factory.IsNull( ) )
	{
		throw Exceptions::FactoryNotFoundException( "ITKVectorImageImpl<ImageType,TimeStepImageType>::SetData" );
	}

	DataEntityImpl::Pointer dataEntityImpl;
	dataEntityImpl = DataEntityImpl::SafeDownCast( factory->CreateInstance() );

	// Copy data from source
	if ( data.IsNotNull( ) )
	{
		dataEntityImpl->DeepCopy( data, gmReferenceMemory );
	}

	// Create ImageType
	typename TimeStepImageType::Pointer image;
	image = boost::any_cast<typename TimeStepImageType::Pointer>( dataEntityImpl->GetDataPtr() );
	typedef itk::JoinSeriesImageFilter<TimeStepImageType, ImageType> ImageFilter3Dto4D;
	typename ImageFilter3Dto4D::Pointer imageFilter3Dto4D =  ImageFilter3Dto4D::New();
	for ( int i = 0 ; i < size ; i++ )
	{
		imageFilter3Dto4D->PushBackInput( image );
	}
	imageFilter3Dto4D->Update();
	m_Data = imageFilter3Dto4D->GetOutput( );
}

template <class ImageType, class TimeStepImageType>
void Core::ITKVectorImageImpl<ImageType,TimeStepImageType>::SetAnyData( boost::any val )
{
	m_Data = boost::any_cast<typename ImageType::Pointer> ( val );
	if ( m_Data.GetPointer( ) == NULL )
	{
		throw Exceptions::DataPtrIsNullException( "ITKVectorImageImpl<ImageType,TimeStepImageType>::SetDataPtr" );
	}
}
