/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _MITKPluginProcessorCollective_H
#define _MITKPluginProcessorCollective_H

#include "coreSmartPointerMacros.h"
#include "coreObject.h"

namespace MITKPlugin{

/**
This class instantiates all processors used in the plugin and registers them.

\ingroup MITKPlugin
\author Xavi Planes
\date Nov 2010
*/

class ProcessorCollective : public Core::SmartPointerObject
{
public:
	//!
	coreDeclareSmartPointerClassMacro(MITKPlugin::ProcessorCollective, Core::SmartPointerObject);

private:
	//! The constructor instantiates all the processors and connects them.
	ProcessorCollective();

	~ProcessorCollective( );

};

} // namespace MITKPlugin{

#endif //_MITKPluginProcessorCollective_H
