/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "MITKPluginProcessorCollective.h"
#include "coreDataEntityReader.h"
#include "coreDataEntityWriter.h"

#include "coreMITKSurfaceImpl.h"
#include "coreBlMITKSurfaceImpl.h"
#include "coreMITKImageImpl.h"
#include "coreMITKPointSetImpl.h"
#include "coreMITKCuboidImpl.h"
#include "coreMITKContourImpl.h"
#include "coreMITKSignalImpl.h"
#include "coreMITKTransformImpl.h"

#include "coreSignalImpl.h"
#include "coreSliceImageImpl.h"
#include "coreITKDTITensorImpl.h"
#include "coreITKImageImpl.h"
#include "coreITKImageVectorImpl.h"
#include "coreITKVectorImageImpl.h"
#include "coreITKTransformImpl.h"
#include "coreVTKPolyDataImpl.h"
#include "coreVTKImageDataImpl.h"
#include "coreVTKUnstructuredGridImpl.h"

#include "coreVTKImageDataReader.h"
#include "coreVTKImageDataWriter.h"
#include "coreVTKPolyDataReader.h"
#include "coreVTKPolyDataWriter.h"
#include "coreVTKUnstructuredGridReader.h"
#include "coreVTKUnstructuredGridWriter.h"
#include "coreMHDVectorFieldReader.h"
#include "coreCgnsFileReader.h"
#include "coreCgnsFileWriter.h"
#include "coreDICOMFileReader.h"
#include "coreITKTransformReader.h"
#include "coreITKTransformWriter.h"
#include "coreITKImageReader.h"
#include "coreITKImageWriter.h"
#include "coreVTKVRMLReader.h"
#include "coreVTKVRMLWriter.h"

#include "itkDataEntityIOFactory.h"
#include "itkMRCImageIOFactory.h"

using namespace Core;
using namespace Core::IO;

#define CALL_FUNCTION_ALL_TYPES( callFucntion )\
	{typedef char PixelType; callFucntion;}\
	{typedef unsigned char  PixelType; callFucntion;}\
	{typedef int PixelType; callFucntion;}\
	{typedef unsigned int PixelType; callFucntion;}\
	{typedef short PixelType; callFucntion;}\
	{typedef unsigned short PixelType; callFucntion;}\
	{typedef float PixelType; callFucntion;}\
	{typedef double PixelType; callFucntion;}\
	{typedef long PixelType; callFucntion;}\
	{typedef unsigned long PixelType; callFucntion;}


MITKPlugin::ProcessorCollective::ProcessorCollective()
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
	gIface = Core::Runtime::Kernel::GetGraphicalInterface();

	gIface->RegisterFactory( 
		Core::DataEntityImpl::GetNameClass(), Core::MitkSurfaceImpl::Factory::New() );
	
	gIface->RegisterFactory( 
		Core::DataEntityImpl::GetNameClass(), Core::BlMitkSurfaceImpl::Factory::New() );
	
	gIface->RegisterFactory( 
		Core::DataEntityImpl::GetNameClass(), Core::MitkPointSetImpl::Factory::New() );
	
	gIface->RegisterFactory( 
		Core::DataEntityImpl::GetNameClass(), Core::MitkImageImpl::Factory::New() );
	
	gIface->RegisterFactory( 
		Core::DataEntityImpl::GetNameClass(), Core::MitkContourImpl::Factory::New() );
	
	gIface->RegisterFactory( 
		Core::DataEntityImpl::GetNameClass(), Core::MITKCuboidImpl::Factory::New() );
	
	gIface->RegisterFactory( 
		Core::DataEntityImpl::GetNameClass(), Core::MitkSignalImpl::Factory::New() );
	
	gIface->RegisterFactory( 
		Core::DataEntityImpl::GetNameClass(), Core::MitkTransformImpl::Factory::New() );

	
	gIface->RegisterFactory( 
		DataEntityImpl::GetNameClass(), VtkPolyDataImpl::Factory::New() );
	
	gIface->RegisterFactory( 
		DataEntityImpl::GetNameClass(), VtkImageDataImpl::Factory::New() );

	gIface->RegisterFactory( 
		DataEntityImpl::GetNameClass(), VtkUnstructuredGridImpl::Factory::New() );

	// Scalar pixel types
	CALL_FUNCTION_ALL_TYPES( 
		gIface->RegisterFactory( DataEntityImpl::GetNameClass(), ITKImageImpl<itk::Image<PixelType,3> >::Factory::New() ); );
	CALL_FUNCTION_ALL_TYPES( 
		gIface->RegisterFactory( DataEntityImpl::GetNameClass(), ITKImageImpl<itk::Image<PixelType,2> >::Factory::New() ); );

	// Vector pixel types
	CALL_FUNCTION_ALL_TYPES( 
		gIface->RegisterFactory( DataEntityImpl::GetNameClass(), ITKImageVectorImpl<itk::Image<itk::Vector<PixelType, 2>, 2> >::Factory::New() ) );
	CALL_FUNCTION_ALL_TYPES( 
		gIface->RegisterFactory( DataEntityImpl::GetNameClass(), ITKImageVectorImpl<itk::Image<itk::Vector<PixelType, 2>, 3> >::Factory::New() ) );
	CALL_FUNCTION_ALL_TYPES( 
		gIface->RegisterFactory( DataEntityImpl::GetNameClass(), ITKImageVectorImpl<itk::Image<itk::Vector<PixelType, 3>, 2> >::Factory::New() ) );
	CALL_FUNCTION_ALL_TYPES( 
		gIface->RegisterFactory( DataEntityImpl::GetNameClass(), ITKImageVectorImpl<itk::Image<itk::Vector<PixelType, 3>, 3> >::Factory::New() ) );

	// Vector of images
	CALL_FUNCTION_ALL_TYPES( 
		gIface->RegisterFactory( DataEntityImpl::GetNameClass(), ITKVectorImageImpl<itk::Image<PixelType,4>, itk::Image<PixelType,3> >::Factory::New() ) );
	CALL_FUNCTION_ALL_TYPES( 
		gIface->RegisterFactory( DataEntityImpl::GetNameClass(), ITKVectorImageImpl<itk::Image<itk::Vector<PixelType,2>,4>, itk::Image<itk::Vector<PixelType,2>,3> >::Factory::New() ) );
	CALL_FUNCTION_ALL_TYPES( 
		gIface->RegisterFactory( DataEntityImpl::GetNameClass(), ITKVectorImageImpl<itk::Image<itk::Vector<PixelType,3>,4>, itk::Image<itk::Vector<PixelType,3>,3> >::Factory::New() ) );

	gIface->RegisterFactory( DataEntityImpl::GetNameClass(), ITKTransformImpl::Factory::New() );
	gIface->RegisterFactory( DataEntityImpl::GetNameClass(), SliceImageImpl::Factory::New() );
	gIface->RegisterFactory( DataEntityImpl::GetNameClass(), ITKDTITensorImpl::Factory::New() );

	gIface->RegisterFactory( Core::DataEntityImpl::GetNameClass(), Core::SignalImpl::Factory::New() );

	std::string resourcePath = Core::Runtime::Kernel::GetApplicationSettings()->GetResourcePath( );
	gIface->RegisterReader( BaseDataEntityReader::Pointer(DICOMFileReader::New( resourcePath )) );
	gIface->RegisterReader( BaseDataEntityReader::Pointer(VTKImageDataReader::New()) );
	gIface->RegisterReader( BaseDataEntityReader::Pointer(VTKPolyDataReader::New()) );
	gIface->RegisterReader( BaseDataEntityReader::Pointer(VTKUnstructuredGridReader::New()) );
	gIface->RegisterReader( BaseDataEntityReader::Pointer(MHDVectorFieldReader::New()) );
	gIface->RegisterReader( BaseDataEntityReader::Pointer(CGNSFileReader::New()) );
	gIface->RegisterReader( BaseDataEntityReader::Pointer(ITKTransformReader::New()) );
	gIface->RegisterReader( BaseDataEntityReader::Pointer(ITKImageReader::New()) );
	gIface->RegisterReader( BaseDataEntityReader::Pointer(VTKVRMLReader::New()) );

	gIface->RegisterWriter( BaseDataEntityWriter::Pointer(VTKImageDataWriter::New()) );
	gIface->RegisterWriter( BaseDataEntityWriter::Pointer(VTKPolyDataWriter::New()) );
	gIface->RegisterWriter( BaseDataEntityWriter::Pointer(VTKUnstructuredGridWriter::New()) );
	gIface->RegisterWriter( BaseDataEntityWriter::Pointer(CGNSFileWriter::New()) );
	gIface->RegisterWriter( BaseDataEntityWriter::Pointer(ITKTransformWriter::New()) );
	gIface->RegisterWriter( BaseDataEntityWriter::Pointer(ITKImageWriter::New()) );
	gIface->RegisterWriter( BaseDataEntityWriter::Pointer(VTKVRMLWriter::New()) );

	// Load specific itk reader for DataEntity
	itk::DataEntityIOFactory::RegisterOneFactory( );

	itk::MRCImageIOFactory::RegisterOneFactory();
}

MITKPlugin::ProcessorCollective::~ProcessorCollective()
{
	// UnLoad specific itk reader for DataEntity
	itk::DataEntityIOFactory::UnRegisterOneFactory( );

	// Unregister ITK IO factories added in itkImageReader
	itk::ObjectFactoryBase::ReHash();
}
