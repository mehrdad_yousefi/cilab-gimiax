/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _MITKPluginWidgetCollective_H
#define _MITKPluginWidgetCollective_H

#include "coreFrontEndPlugin.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreWidgetCollective.h"
#include "MITKEventFilter.h"

#include "MITKPluginProcessorCollective.h"

#define wxID_BBoxWidget wxID( "wxID_BBoxWidget" )

namespace MITKPlugin{

class SubtractPanelWidget;
class ShapeScalePanelWidget;
class ResamplePanelWidget;

/**

\ingroup MITKPlugin
\author Xavi Planes
\date Nov 2010
*/

class WidgetCollective : public Core::WidgetCollective
{
public:
	//!
	coreDeclareSmartPointerClassMacro(
		MITKPlugin::WidgetCollective, 
		Core::WidgetCollective );

private:
	//! The constructor instantiates all the widgets and registers them.
	WidgetCollective( );

	//!
	~WidgetCollective( );

	//!
	void LoadStateMachines();

private:

	//!
	EventFilter::Pointer m_EventFilter;
};

} // namespace MITKPlugin

#endif //_MITKPluginWidgetCollective_H
