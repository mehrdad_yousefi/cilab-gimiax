/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "msWidgetCollective.h"

#include "wxID.h"

#include "coreFrontEndPlugin.h"
#include "corePluginTabFactory.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreKernel.h"

#include "ManualSegmentationPanelWidget.h"
#include "MultiLevelROIPanelWidget.h"
#include "InteractiveSegmPanelWidget.h"

#include "ContourToolOn.xpm"
#include "mitkRegionGrowingTool.xpm"


msWidgetCollective::msWidgetCollective( ) 
{
}

void msWidgetCollective::Init(  )
{
	Core::Runtime::Kernel::RuntimeGraphicalInterfacePointer gIface;
	gIface = Core::Runtime::Kernel::GetGraphicalInterface();

	gIface->RegisterFactory(
		Core::Widgets::ManualSegmentationPanelWidget::Factory::NewBase(),
		Core::WindowConfig( ).SelectionTool()
		.Caption("Manual Segmentation").Bitmap( contourtoolon_xpm )
		.Id( wxID_ManualSegmentationWidget ) );

	gIface->RegisterFactory(
		Core::Widgets::MultiLevelROIPanelWidget::Factory::NewBase(),
		Core::WindowConfig( ).ProcessingTool( ).Category("Statistics")
		.Caption("Image Statistics").Bitmap( contourtoolon_xpm )
		.Id( wxID_MultiLevelROIWidget ) );


	gIface->RegisterFactory(
		Core::Widgets::InteractiveSegmPanelWidget::Factory::NewBase(),
		Core::WindowConfig( ).SelectionTool()
		.Caption("Interactive Segmentation").Bitmap( mitkRegionGrowingTool_xpm )
		.Id( wxID_InteractiveSegmentationWidget ) );

}

