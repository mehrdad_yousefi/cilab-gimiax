/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "ManualSegmentationPlugin.h"

// CoreLib
#include "coreReportExceptionMacros.h"
#include "corePluginMacros.h"
#include "coreProfile.h"

// Declaration of the plugin
coreBeginDefinePluginMacro(ManualSegmentationPlugin)
coreEndDefinePluginMacro()

ManualSegmentationPlugin::ManualSegmentationPlugin(void) : FrontEndPlugin()
{
	try
	{
		m_Processors = msProcessorCollective::New();

		m_Widgets = msWidgetCollective::New();
		m_Widgets->Init( );
	}
	coreCatchExceptionsReportAndNoThrowMacro(ManualSegmentationPlugin::ManualSegmentationPlugin)
}

ManualSegmentationPlugin::~ManualSegmentationPlugin(void)
{
}
