/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "InteractiveSegmPanelWidget.h"
#include "msPolygonTool.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"

// Core
#include "coreVTKPolyDataHolder.h"
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"
#include "coreRenderingTree.h"
#include "coreDataEntityListBrowser.h"
#include "coreReportExceptionMacros.h"
#include "corePluginTab.h"
#include "coreDataEntity.h"
#include "coreDataEntityList.h"
#include "coreDataEntityList.txx"
#include "coreProcessorInputWidget.h"
#include "coreSettings.h"
#include "coreDirectory.h"
#include "coreRenderWindowContainer.h"
#include "coreMultiRenderWindowMITK.h"
#include "coreSlicePlane.h"

// STD
#include <limits>
#include <cmath>
#include <algorithm>
#include <sstream>

// Mitk

#include "mitkLookupTableProperty.h"
#include "vtkPointData.h"
#include "mitkUndoController.h"
#include "mitkVerboseLimitedLinearUndo.h"
#include "mitkCoreObjectFactory.h"
#include "mitkProperties.h"
#include "mitkOverwriteSliceImageFilter.h"
#include "mitkApplyDiffImageOperation.h"
#include "mitkDiffImageApplier.h"

#include "wxMitkSelectableGLWidget.h"

#include "dynWxControlFactory.h"

// Resources
#include "resource/mitkUndo24x24.xpm"
#include "resource/mitkRedo24x24.xpm"
#include "resource/mitkAddContourToolOn.xpm"
#include "resource/mitkAddContourTool.xpm"
#include "resource/mitkSubtractContourToolOn.xpm"
#include "resource/mitkSubtractContourTool.xpm"
#include "resource/mitkDrawPaintbrushToolOn.xpm"
#include "resource/mitkDrawPaintbrushTool.xpm"
#include "resource/mitkErasePaintbrushToolOn.xpm"
#include "resource/mitkErasePaintbrushTool.xpm"
#include "resource/mitkRegionGrowingTool24x24On.xpm"
#include "resource/mitkRegionGrowingTool24x24.xpm"
#include "resource/mitkCorrectorTool2DOn.xpm"
#include "resource/mitkCorrectorTool2D.xpm"
#include "resource/mitkFillRegionToolOn.xpm"
#include "resource/mitkFillRegionTool.xpm"
#include "resource/mitkEraseRegionToolOn.xpm"
#include "resource/mitkEraseRegionTool.xpm"
#include "resource/mitkPolygonToolOn.xpm"
#include "resource/mitkPolygonTool.xpm"
#include "resource/mitkLiveWireToolOn.xpm"
#include "resource/mitkLiveWireTool.xpm"
#include "resource/mitkSplineToolOn.xpm"
#include "resource/mitkSplineTool.xpm"
#include "resource/mitkMeshTool.xpm"


using namespace Core::Widgets;

// Event the widget
BEGIN_EVENT_TABLE(InteractiveSegmPanelWidget,InteractiveSegmPanelWidgetUI)
	//EVT_TOGGLEBUTTON(wxID_EnableInteraction, Core::Widgets::InteractiveSegmPanelWidget::OnBtnEnable)
END_EVENT_TABLE()


InteractiveSegmPanelWidget::InteractiveSegmPanelWidget(
							  wxWindow* parent, 
							  int id,
							  const wxPoint& pos, 
							  const wxSize& size, 
							  long style) :
	InteractiveSegmPanelWidgetUI(parent, id, pos, size, style)
{
	try 
	{
		m_InterpolationEnabled = false;
		m_UndoController = NULL;
		m_UndoController = new mitk::UndoController();
		m_Segmentation = NULL;
		m_LevelWindowInteractorState = true;
		
		m_StartInteractorLocalCall = false;
		m_processor = InteractiveSegmentationProcessor::New();		

		m_InteractiveSegmInteractorHelper = NULL;
		m_InteractiveSegmInteractorHelper = InteractiveSegmInteractorHelper::New();
		m_InteractiveSegmInteractorHelper->SetNextRoiLevel(0);

		m_LastSliceDimension = -1;
		m_LastSliceIndex = -1;

		SetName("Interactive Segmentation");

		// Add the correct paths to the image
		m_btn_undo->SetBitmapLabel( wxBitmap( mitkUndo24x24_xpm ) );
		m_btn_redo->SetBitmapLabel( wxBitmap( mitkRedo24x24_xpm ) );
		m_btn_create_surface->SetBitmapLabel( wxBitmap( mitkMeshTool_xpm ) );

		AddToolButton(InterType::ADDTOOL, m_btn_add, wxBitmap( mitkAddContourToolOn_xpm ), wxBitmap( mitkAddContourTool_xpm ) );
		AddToolButton(InterType::SUBTRACTTOOL, m_btn_subtract, wxBitmap( mitkSubtractContourToolOff_xpm ), wxBitmap( mitkSubtractContourTool_xpm ) );
		AddToolButton(InterType::PAINTTOOL, m_btn_paint, wxBitmap( mitkDrawPaintbrushToolOn_xpm ), wxBitmap( mitkDrawPaintbrushTool_xpm ) );
		AddToolButton(InterType::WIPETOOL, m_btn_wipe, wxBitmap( mitkErasePaintbrushToolOff_xpm ), wxBitmap( mitkErasePaintbrushTool_xpm ) );
		AddToolButton(InterType::REGIONGROWINGTOOL, m_btn_regionGrowing, wxBitmap( mitkRegionGrowingTool24x24On_xpm ), wxBitmap( mitkRegionGrowingTool20x20_xpm ) );
		AddToolButton(InterType::CORRECTIONTOOL, m_btn_correction, wxBitmap( mitkCorrectorTool2DOn_xpm ), wxBitmap( mitkCorrectorTool2D_xpm ) );
		AddToolButton(InterType::FILLREGIONTOOL, m_btn_fill, wxBitmap( mitkFillRegionToolOn_xpm ), wxBitmap( mitkFillRegionTool_xpm ) );
		AddToolButton(InterType::ERASEREGIONTOOL, m_btn_erase, wxBitmap( mitkEraseRegionToolOff_xpm ), wxBitmap( mitkEraseRegionTool_xpm ) );
		AddToolButton(InterType::POLYGONTOOL, m_btn_polygon, wxBitmap( mitkPolygonToolOn_xpm ), wxBitmap( mitkPolygonTool_xpm ) );
		AddToolButton(InterType::LIVEWIRE, m_btn_livewire, wxBitmap( mitkLiveWireToolOn_xpm ), wxBitmap( mitkLiveWireTool_xpm ) );
		AddToolButton(InterType::SPLINE, m_btn_spline, wxBitmap( mitkSplineToolOn_xpm ), wxBitmap( mitkSplineTool_xpm ) );
		
		m_currentTool = InterType::INVALID_TOOL;

		m_Interpolator = mitk::SegmentationInterpolationController::New();
		itk::ReceptorMemberCommand<InteractiveSegmPanelWidget>::Pointer command = itk::ReceptorMemberCommand<InteractiveSegmPanelWidget>::New();
		command->SetCallbackFunction( this, &InteractiveSegmPanelWidget::OnInterpolationInfoChanged );
		m_Interpolator->AddObserver( itk::ModifiedEvent(), command );

	}
	coreCatchExceptionsReportAndNoThrowMacro( 
		"InteractiveSegmPanelWidget::InteractiveSegmPanelWidget" );

}

void InteractiveSegmPanelWidget::AddToolButton( TOOLS_TYPE tType, wxBitmapButton *btn, 
											   wxBitmap onIcon,   wxBitmap offIcon)
{
	TOOL_BTN currentBtn;
	currentBtn.SetElement(btn, onIcon, offIcon );
	m_toolsBtnMap.insert(std::pair<TOOLS_TYPE,TOOL_BTN>(tType,currentBtn));
	currentBtn.SetBitmapLabel(false);
}


InteractiveSegmPanelWidget::~InteractiveSegmPanelWidget( ) 
{
	StopInteraction();

	if(m_UndoController!=NULL)
		delete m_UndoController;

	std::map<mitk::SliceNavigationController::Pointer, unsigned long>::iterator it;
	for ( it = m_ObserverTags.begin( ) ; it != m_ObserverTags.end( ) ; it++ )
	{
		it->first->RemoveObserver( it->second );
	}
}


void InteractiveSegmPanelWidget::OnModifiedSelectedReferenceImageDataEntityHolder()
{
	Core::DataEntity::Pointer dataEntity = m_processor->GetInputDataEntity(
		InteractiveSegmentationProcessor::REFERENCE_IMAGE);

	if ( dataEntity.IsNotNull() )
	{
		//m_btnEnableInteraction->Enable(true);

	}
	else
	{
		//m_btnEnableInteraction->Enable(false);
	}
}


void InteractiveSegmPanelWidget::OnInit()
{
	m_Selection = 0;
	GetInputWidget(InteractiveSegmentationProcessor::REFERENCE_IMAGE)->Show(true);
	GetInputWidget(InteractiveSegmentationProcessor::REFERENCE_IMAGE)->SetAutomaticSelection(false);
	GetInputWidget(InteractiveSegmentationProcessor::REFERENCE_IMAGE)->SetDefaultDataEntityFlag(false);

	GetInputWidget(InteractiveSegmentationProcessor::INPUT_ROI)->Show(true);
	GetInputWidget(InteractiveSegmentationProcessor::INPUT_ROI)->SetAutomaticSelection(false);
	GetInputWidget(InteractiveSegmentationProcessor::INPUT_ROI)->SetDefaultDataEntityFlag(false);

	InitializeInterpolation();

	wxTextCtrl *helper = dynamic_cast<wxTextCtrl*> ( wxWindow::FindWindowById( wxID_HELP_CONTROL, this ) );
	if ( helper != NULL )
	{
		m_InitialHelpDescription = helper->GetValue( );
	}

	GetProcessorOutputObserver( 0 )->SetHideInput( false );
	GetProcessorOutputObserver( 0 )->PushProperty( blTag::New( "opacity", 0.75f ) );
}


void InteractiveSegmPanelWidget::UpdateWidget() 
{
}

//!
Core::BaseProcessor::Pointer InteractiveSegmPanelWidget::GetProcessor()
{
	return m_processor.GetPointer( );
}

//!
void InteractiveSegmPanelWidget::StartInteractor()
{
	//only start the interactor if the fnc has been called locally and m_RGrowInteractor exists;
	if((!m_StartInteractorLocalCall) || (m_InteractiveSegmInteractorHelper.IsNull()))
		return;

	if(m_processor->GetInputDataEntity( 
		InteractiveSegmentationProcessor::REFERENCE_IMAGE ).IsNull())
		return;

	//m_InteractiveSegmInteractorHelper->SetNextRoiLevel( m_Selection+1 );
	if(m_processor->GetInputDataEntity( InteractiveSegmentationProcessor::INPUT_ROI).IsNotNull())
		m_InteractiveSegmInteractorHelper->GetMaskImageDataHolder()->SetSubject(
			m_processor->GetInputDataEntity( InteractiveSegmentationProcessor::INPUT_ROI));
	
	m_InteractiveSegmInteractorHelper->SetRenderingTree( GetRenderingTree() );
	m_InteractiveSegmInteractorHelper->SetSelectedImageHolder( 
		m_processor->GetInputDataEntityHolder( 
			InteractiveSegmentationProcessor::REFERENCE_IMAGE ));
	
	m_InteractiveSegmInteractorHelper->OnModifiedSelected();

	m_InteractiveSegmInteractorHelper->StartInteraction();
	if ( m_InteractiveSegmInteractorHelper->GetInteractorStateHolder( )->GetSubject() == 
			InteractiveSegmInteractorHelper::INTERACTOR_DISABLED )
	{
		return;
	}

	m_processor->SetInputDataEntity( InteractiveSegmentationProcessor::INPUT_ROI, 
		m_InteractiveSegmInteractorHelper->GetMaskImageDataHolder()->GetSubject() );

	if ( !GetMultiRenderWindow() )
	{
		return ;
	}

	GetMultiRenderWindow()->GetMetadata()->AddTag( "AxisLocked", true );
	m_LevelWindowInteractorState = 
		GetMultiRenderWindow()->GetMetadata()->GetTagValue<bool>( "LevelWindowInteractor" );
	GetMultiRenderWindow()->GetMetadata()->AddTag( "LevelWindowInteractor", false );
	GetMultiRenderWindow()->GetMetadataHolder()->NotifyObservers();
	//m_btnEnableInteraction->SetLabel("Disable");
	//m_btnEnableInteraction->SetValue( true );

	UpdateWidget( );
	m_StartInteractorLocalCall = false;
}

//!
void InteractiveSegmPanelWidget::StopInteraction()
{
	if ( m_InteractiveSegmInteractorHelper.IsNotNull() )
	{
		m_InteractiveSegmInteractorHelper->StopInteraction();
	}
	else
		return;
	if ( GetMultiRenderWindow() )
	{
		GetMultiRenderWindow()->GetMetadata()->AddTag( "AxisLocked", false );
		GetMultiRenderWindow()->GetMetadata()->AddTag( "LevelWindowInteractor", m_LevelWindowInteractorState );
		GetMultiRenderWindow()->GetMetadataHolder()->NotifyObservers();
	}
	//m_btnEnableInteraction->SetLabel("Enable");
	//m_txtAdd->SetValue( "" );
	m_countLabels = 0;
	m_Selection = 0;
}

//!
bool InteractiveSegmPanelWidget::IsSelectionEnabled( )
{
	if( m_InteractiveSegmInteractorHelper.IsNull() )
	{
		return false;
	}
	return m_InteractiveSegmInteractorHelper->GetInteractorStateHolder()->GetSubject() ==
		InteractiveSegmInteractorHelper::INTERACTOR_ENABLED;
}


void Core::Widgets::InteractiveSegmPanelWidget::OnBtnEnable( wxCommandEvent& event )
{
/*	
	if (m_btnEnableInteraction->GetValue() == true)
	{
		m_StartInteractorLocalCall = true;
		StartInteractor();
	}
	else 
	{
		StopInteraction( );
	}
	*/
}

void Core::Widgets::InteractiveSegmPanelWidget::UpdateActiveToolBtn( TOOLS_TYPE tool)
{

	if(m_currentTool!=InterType::INVALID_TOOL)
	{
		BtnMap::iterator currIt;
		currIt = m_toolsBtnMap.find(m_currentTool);
		if(currIt!=m_toolsBtnMap.end())
			currIt->second.SetBitmapLabel(false);
	}

	if( tool!= InterType::INVALID_TOOL )
	{
		BtnMap::iterator newIt;
		newIt = m_toolsBtnMap.find(tool);
		if(newIt!=m_toolsBtnMap.end())
			newIt->second.SetBitmapLabel(true);
	}
	m_currentTool = tool;
}


void Core::Widgets::InteractiveSegmPanelWidget::ChangeTool(TOOLS_TYPE tool)
{
	if (m_processor->
		GetInputDataEntity(InteractiveSegmentationProcessor::REFERENCE_IMAGE).IsNull())
		return;

	TOOLS_TYPE newSelectedTool = InterType::INVALID_TOOL;
	StopInteraction();
	if(m_currentTool != tool)
	{
		m_InteractiveSegmInteractorHelper->SetToolType(tool);
		m_StartInteractorLocalCall = true;
		StartInteractor();
		newSelectedTool = tool;
		OnInterpolationActivated(true);
	}
	else
	{
		m_InteractiveSegmInteractorHelper->GetInteractor( )->DestroyInteractor( );
	}
	UpdateActiveToolBtn( newSelectedTool );
	UpdateHelperInformation( );
}


void Core::Widgets::InteractiveSegmPanelWidget::OnBtnAdd( wxCommandEvent& event )
{
	ChangeTool(InterType::ADDTOOL);
}

void Core::Widgets::InteractiveSegmPanelWidget::OnBtnSubtract( wxCommandEvent& event )
{
	ChangeTool(InterType::SUBTRACTTOOL);
}

void Core::Widgets::InteractiveSegmPanelWidget::OnBtnPaint( wxCommandEvent& event )
{
	ChangeTool(InterType::PAINTTOOL);
	m_InteractiveSegmInteractorHelper->SetBrushSize(m_sliderToolSize->GetValue());
}

void Core::Widgets::InteractiveSegmPanelWidget::OnBtnWipe( wxCommandEvent& event )
{
	ChangeTool(InterType::WIPETOOL);
	m_InteractiveSegmInteractorHelper->SetBrushSize(m_sliderToolSize->GetValue());
}

void Core::Widgets::InteractiveSegmPanelWidget::OnBtnRGrow( wxCommandEvent& event )
{
	ChangeTool(InterType::REGIONGROWINGTOOL);
}

void Core::Widgets::InteractiveSegmPanelWidget::OnBtnCorrect( wxCommandEvent& event )
{
	ChangeTool(InterType::CORRECTIONTOOL);
}

void Core::Widgets::InteractiveSegmPanelWidget::OnBtnFillRegion( wxCommandEvent& event )
{
	ChangeTool(InterType::FILLREGIONTOOL);
}

void Core::Widgets::InteractiveSegmPanelWidget::OnBtnEraseRegion( wxCommandEvent& event )
{
	ChangeTool(InterType::ERASEREGIONTOOL);
}

void Core::Widgets::InteractiveSegmPanelWidget::OnBtnPolygon( wxCommandEvent& event )
{
	ChangeTool(InterType::POLYGONTOOL);

	msp::InteractiveSegmInteractor::Pointer interactor;
	interactor = m_InteractiveSegmInteractorHelper->GetInteractor( );

	mitk::PolygonTool * cTool = dynamic_cast<mitk::PolygonTool *>(interactor->GetCurrentTool());
	if ( cTool )
	{
		cTool->SetContourMethod( ImageContourProcessor::METHOD_POLYGON );
		cTool->UpdateTimeStep( GetTimeStep( ) );
	}
}

void Core::Widgets::InteractiveSegmPanelWidget::OnBtnLiveWire( wxCommandEvent& event )
{
	// Change tool
	ChangeTool(InterType::LIVEWIRE);

	msp::InteractiveSegmInteractor::Pointer interactor;
	interactor = m_InteractiveSegmInteractorHelper->GetInteractor( );

	mitk::PolygonTool * cTool = dynamic_cast<mitk::PolygonTool *>(interactor->GetCurrentTool());
	if ( cTool )
	{
		cTool->SetContourMethod( ImageContourProcessor::METHOD_DIJKSTRA );
		cTool->UpdateTimeStep( GetTimeStep( ) );
	}
}

void Core::Widgets::InteractiveSegmPanelWidget::OnBtnSpline( wxCommandEvent& event )
{
	// Change tool
	ChangeTool(InterType::SPLINE);

	msp::InteractiveSegmInteractor::Pointer interactor;
	interactor = m_InteractiveSegmInteractorHelper->GetInteractor( );

	mitk::PolygonTool * cTool = dynamic_cast<mitk::PolygonTool *>(interactor->GetCurrentTool());
	if ( cTool )
	{
		cTool->SetContourMethod( ImageContourProcessor::METHOD_SPLINE );
		cTool->UpdateTimeStep( GetTimeStep( ) );
	}
}

void InteractiveSegmPanelWidget::OnBrushSizeChanged(wxScrollEvent &event)
{
	m_InteractiveSegmInteractorHelper->SetBrushSize(m_sliderToolSize->GetValue());
}


void InteractiveSegmPanelWidget::OnBtnUndo( wxCommandEvent& event )
{
	if(m_UndoController==NULL)
		return;

	mitk::VerboseLimitedLinearUndo::Pointer standardModel = 
		dynamic_cast<mitk::VerboseLimitedLinearUndo*>(m_UndoController->GetCurrentUndoModel());
	standardModel->Undo();
}

void InteractiveSegmPanelWidget::OnBtnRedo( wxCommandEvent& event )
{
	if(m_UndoController==NULL)
		return;

	mitk::VerboseLimitedLinearUndo::Pointer standardModel = 
		dynamic_cast<mitk::VerboseLimitedLinearUndo*>(m_UndoController->GetCurrentUndoModel());
	standardModel->Redo();
}


void InteractiveSegmPanelWidget::OnBtnNewSegmentation(wxCommandEvent &event)
{
	StopInteraction();
	
	m_processor->SetInputDataEntity( InteractiveSegmentationProcessor::INPUT_ROI, 
		NULL );

	if(m_InteractiveSegmInteractorHelper.IsNotNull())
		m_InteractiveSegmInteractorHelper->GetMaskImageDataHolder()->SetSubject(NULL);
}


void Core::Widgets::InteractiveSegmPanelWidget::UpdateData()
{
}

bool InteractiveSegmPanelWidget::Enable( bool enable ) 
{
	bool bReturn = InteractiveSegmPanelWidgetUI::Enable( enable );
	//if ( !enable ) 
	//{
	//	StopInteraction( );
	//}

	return bReturn;
}


void InteractiveSegmPanelWidget::InitializeInterpolation()
{
	if(m_FeedbackNode.IsNull())
	{
		m_FeedbackNode = mitk::DataTreeNode::New();
		mitk::CoreObjectFactory::GetInstance()->SetDefaultProperties( m_FeedbackNode );

		m_FeedbackNode->SetProperty( "binary", mitk::BoolProperty::New(true) );
		m_FeedbackNode->SetProperty( "outline binary", mitk::BoolProperty::New(true) );
		m_FeedbackNode->SetProperty( "color", mitk::ColorProperty::New(0.0, 1.0, 1.0) );
		m_FeedbackNode->SetProperty( "texture interpolation", mitk::BoolProperty::New(false) );
		m_FeedbackNode->SetProperty( "layer", mitk::IntProperty::New( 20 ) );
		m_FeedbackNode->SetProperty( "levelwindow", mitk::LevelWindowProperty::New( mitk::LevelWindow(0, 1) ) );
		m_FeedbackNode->SetProperty( "name", mitk::StringProperty::New("Interpolation feedback") );
		m_FeedbackNode->SetProperty( "opacity", mitk::FloatProperty::New(0.8) );
		m_FeedbackNode->SetProperty( "helper object", mitk::BoolProperty::New(true) );
	}
	
	Core::Widgets::MultiRenderWindowMITK *mrw = static_cast<Core::Widgets::MultiRenderWindowMITK *> ( GetMultiRenderWindow( ) );

	mitk::SliceNavigationController::Pointer slicer;
	mitk::wxMitkSelectableGLWidget *zyx [3] = {mrw->GetZ(), mrw->GetY(), mrw->GetX()};
	for(int i=0; i<3; i++)
	{
		slicer= zyx[i]->GetSliceNavigationController();

		Core::SlicePlane::PlaneOrientation orientation  = 
			Core::SlicePlane::PlaneOrientation( slicer->GetViewDirection( ) );

		switch (orientation)
		{
		case (SlicePlane::Transversal):
			{
				itk::ReceptorMemberCommand<InteractiveSegmPanelWidget>::Pointer command = itk::ReceptorMemberCommand<InteractiveSegmPanelWidget>::New();
				command->SetCallbackFunction( this, &InteractiveSegmPanelWidget::OnTransversalSliceChanged );
				unsigned long tag = slicer->AddObserver( mitk::SliceNavigationController::GeometrySliceEvent(NULL, 0), command );
				m_ObserverTags[ slicer ] = tag;
			}
			break;
		case (SlicePlane::Sagittal):
			{
				itk::ReceptorMemberCommand<InteractiveSegmPanelWidget>::Pointer command = itk::ReceptorMemberCommand<InteractiveSegmPanelWidget>::New();
				command->SetCallbackFunction( this, &InteractiveSegmPanelWidget::OnSagittalSliceChanged );
				unsigned long tag = slicer->AddObserver( mitk::SliceNavigationController::GeometrySliceEvent(NULL, 0), command );
				m_ObserverTags[ slicer ] = tag;
			}
			break;
		case (SlicePlane::Frontal):
			{
				itk::ReceptorMemberCommand<InteractiveSegmPanelWidget>::Pointer command = itk::ReceptorMemberCommand<InteractiveSegmPanelWidget>::New();
				command->SetCallbackFunction( this, &InteractiveSegmPanelWidget::OnFrontalSliceChanged );
				unsigned long tag = slicer->AddObserver( mitk::SliceNavigationController::GeometrySliceEvent(NULL, 0), command );
				m_ObserverTags[ slicer ] = tag;
			}
			break;
		}
	}
}


void InteractiveSegmPanelWidget::OnTransversalSliceChanged(const itk::EventObject& e)
{
	if ( TranslateAndInterpolateChangedSlice( e, 2 ) )
		mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}

void InteractiveSegmPanelWidget::OnSagittalSliceChanged(const itk::EventObject& e)
{
	if ( TranslateAndInterpolateChangedSlice( e, 0 ) )
		mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}

void InteractiveSegmPanelWidget::OnFrontalSliceChanged(const itk::EventObject& e)
{
	if ( TranslateAndInterpolateChangedSlice( e, 1 ) )
		mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}


bool InteractiveSegmPanelWidget::TranslateAndInterpolateChangedSlice(const itk::EventObject& e, unsigned int windowID)
{
	if (!m_InterpolationEnabled) return false;

	try
	{
		const mitk::SliceNavigationController::GeometrySliceEvent& event = dynamic_cast<const mitk::SliceNavigationController::GeometrySliceEvent&>(e);

		mitk::TimeSlicedGeometry* tsg = event.GetTimeSlicedGeometry();
		if (tsg )
		{
			mitk::SlicedGeometry3D* slicedGeometry = dynamic_cast<mitk::SlicedGeometry3D*>(tsg->GetGeometry3D(GetTimeStep()));
			if (slicedGeometry)
			{
				mitk::PlaneGeometry* plane = dynamic_cast<mitk::PlaneGeometry*>(slicedGeometry->GetGeometry2D( event.GetPos() ));
				Interpolate( plane, GetTimeStep() );
				return true;
			}    
		}
	}
	catch(std::bad_cast)
	{
		return false; // so what
	}

	return false;
}

void InteractiveSegmPanelWidget::Interpolate( mitk::PlaneGeometry* plane, unsigned int timeStep )
{
	if (m_InteractiveSegmInteractorHelper.IsNotNull())
	{
		mitk::DataTreeNode::Pointer node;
		Core::CastAnyProcessingData( GetRenderingTree()->GetNode(
			m_InteractiveSegmInteractorHelper->GetMaskImageDataHolder()->GetSubject()), node );

		if (node.IsNotNull())
		{
			m_Segmentation = dynamic_cast<mitk::Image*>(node->GetData());
			if (m_Segmentation)
			{
				int clickedSliceDimension(-1);
				int clickedSliceIndex(-1);

				// calculate real slice position, i.e. slice of the image and not slice of the TimeSlicedGeometry
				mitk::SegTool2D::DetermineAffectedImageSlice( m_Segmentation, plane, clickedSliceDimension, clickedSliceIndex );
				mitk::Image::Pointer interpolation = m_Interpolator->Interpolate( clickedSliceDimension, clickedSliceIndex, timeStep );
				
				m_FeedbackNode->SetData( interpolation );

				m_LastSliceDimension = clickedSliceDimension;
				m_LastSliceIndex = clickedSliceIndex;
			}
		}
	}
}


void InteractiveSegmPanelWidget::OnInterpolationActivated(bool on)
{
	m_InterpolationEnabled = on;

	Core::RenderingTreeMITK* treeMITK = dynamic_cast<Core::RenderingTreeMITK*> ( GetRenderingTree().GetPointer( ) );
	mitk::DataStorage::Pointer ds = treeMITK->GetDataStorage();
	
	try
	{
		if ( ds.IsNotNull() )
		{
			if (on)
			{
				ds->Add(m_FeedbackNode);
			}
			else
			{
				ds->Remove(m_FeedbackNode);
			}
		}
	}
	catch(...)
	{
		// don't care (double add/remove)
	}

	if (m_InteractiveSegmInteractorHelper.IsNotNull())
	{
		mitk::DataTreeNode::Pointer workingNode;
		Core::CastAnyProcessingData(GetRenderingTree()->GetNode(m_processor->GetInputDataEntity(
				InteractiveSegmentationProcessor::INPUT_ROI)),workingNode);
		mitk::DataTreeNode::Pointer referenceNode;
		Core::CastAnyProcessingData(GetRenderingTree()->GetNode(m_processor->GetInputDataEntity(
				InteractiveSegmentationProcessor::REFERENCE_IMAGE)),referenceNode);

		m_FeedbackNode->SetVisibility( on );

		if (!on)
		{
			mitk::RenderingManager::GetInstance()->RequestUpdateAll();
			return;
		}

		if (workingNode.IsNotNull())
		{
			m_FeedbackNode->ReplaceProperty( "color", workingNode->GetProperty("color") ); // use the same color as the original image (but outline - see constructor)
			m_Segmentation = dynamic_cast<mitk::Image*>(workingNode->GetData());

			if (m_Segmentation)
				m_Interpolator->SetSegmentationVolume( m_Segmentation );

			if (referenceNode.IsNotNull())
			{
				mitk::Image::Pointer referenceImage = dynamic_cast<mitk::Image*>(referenceNode->GetData());
				m_Interpolator->SetReferenceVolume( referenceImage ); // may be NULL
			}
			
		}
	}
	UpdateVisibleSuggestion();
}


void InteractiveSegmPanelWidget::OnInterpolationInfoChanged(const itk::EventObject& /*e*/)
{
	// something (e.g. undo) changed the interpolation info, we should refresh our display
	UpdateVisibleSuggestion();
}

void InteractiveSegmPanelWidget::UpdateVisibleSuggestion()
{
	if (m_InterpolationEnabled)
	{
		// determine which one is the current view, try to do an initial interpolation
		
		mitk::BaseRenderer* renderer = mitk::GlobalInteraction::GetInstance()->GetFocus();
		if (renderer && renderer->GetMapperID() == mitk::BaseRenderer::Standard2D)
		{
			const mitk::TimeSlicedGeometry* timeSlicedGeometry = dynamic_cast<const mitk::TimeSlicedGeometry*>( renderer->GetWorldGeometry() );
			if (timeSlicedGeometry)
			{
				mitk::SliceNavigationController::GeometrySliceEvent event( const_cast<mitk::TimeSlicedGeometry*>(timeSlicedGeometry), renderer->GetSlice() );
				TranslateAndInterpolateChangedSlice( event, 0 );
				TranslateAndInterpolateChangedSlice( event, 1 );
				TranslateAndInterpolateChangedSlice( event, 2 );
			}
		}
	}
	mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}

void InteractiveSegmPanelWidget::OnBtnInterpolate(wxCommandEvent &event)
{
	m_Interpolator->SetSegmentationVolume(m_Segmentation);
	//m_FeedbackNode->ReplaceProperty( "color", m_Segmentation->GetProperty("color") );
	UpdateVisibleSuggestion();
}

void InteractiveSegmPanelWidget::OnBtnAcceptLast(wxCommandEvent &event)
{
	if (m_Segmentation && m_FeedbackNode->GetData())
	{
		mitk::UndoStackItem::IncCurrObjectEventId();
		mitk::UndoStackItem::IncCurrGroupEventId();
		mitk::UndoStackItem::ExecuteIncrement(); // oh well designed undo stack, how do I love thee? let me count the ways... done

		//TODO: not implemented for reoriented slides yet (should use OverwriteDirectedPlaneImageFilter in this case)
		mitk::OverwriteSliceImageFilter::Pointer slicewriter = mitk::OverwriteSliceImageFilter::New();
		slicewriter->SetInput( m_Segmentation );
		slicewriter->SetCreateUndoInformation( true );
		slicewriter->SetSliceImage( dynamic_cast<mitk::Image*>(m_FeedbackNode->GetData()) );
		slicewriter->SetSliceDimension( m_LastSliceDimension );
		slicewriter->SetSliceIndex( m_LastSliceIndex );
		slicewriter->SetTimeStep( GetTimeStep() );
		slicewriter->Update();
		m_FeedbackNode->SetData(NULL);
		mitk::RenderingManager::GetInstance()->RequestUpdateAll();
	}
}


void InteractiveSegmPanelWidget::OnBtnAcceptAll(wxCommandEvent &event)
{
	// first creates a 3D diff image, then applies this diff to the segmentation
	if (m_Segmentation)
	{
		std::cout<<"**********aho"<<std::endl;
		int sliceDimension = m_LastSliceDimension;
		//int dummySliceIndex(-1);

		mitk::UndoStackItem::IncCurrObjectEventId();
		mitk::UndoStackItem::IncCurrGroupEventId();
		mitk::UndoStackItem::ExecuteIncrement(); // oh well designed undo stack, how do I love thee? let me count the ways... done

		// create a diff image for the undo operation
		mitk::Image::Pointer diffImage = mitk::Image::New();
		diffImage->Initialize( m_Segmentation );
		mitk::PixelType pixelType( typeid(short signed int) );
		diffImage->Initialize( pixelType, 3, m_Segmentation->GetDimensions() );

		memset( diffImage->GetData(), 0, (pixelType.GetBpe() >> 3) * diffImage->GetDimension(0) * diffImage->GetDimension(1) * diffImage->GetDimension(2) );
		// now the diff image is all 0

		unsigned int timeStep = GetTimeStep();

		// a slicewriter to create the diff image
		mitk::OverwriteSliceImageFilter::Pointer diffslicewriter = mitk::OverwriteSliceImageFilter::New();
		diffslicewriter->SetCreateUndoInformation( false );
		diffslicewriter->SetInput( diffImage );
		diffslicewriter->SetSliceDimension( sliceDimension );
		diffslicewriter->SetTimeStep( timeStep );

		unsigned int totalChangedSlices(0);
		unsigned int zslices = m_Segmentation->GetDimension( sliceDimension );
		//mitk::ProgressBar::GetInstance()->AddStepsToDo(zslices);
		for (unsigned int sliceIndex = 0; sliceIndex < zslices; ++sliceIndex)
		{
			mitk::Image::Pointer interpolation = m_Interpolator->Interpolate( sliceDimension, sliceIndex, timeStep );
			if (interpolation.IsNotNull()) // we don't check if interpolation is necessary/sensible - but m_Interpolator does
			{
				diffslicewriter->SetSliceImage( interpolation );
				diffslicewriter->SetSliceIndex( sliceIndex );
				diffslicewriter->Update();
				++totalChangedSlices;
			}
			//mitk::ProgressBar::GetInstance()->Progress();
		}

		if (totalChangedSlices > 0)
		{
			// store undo stack items
			if ( true )
			{
				// create do/undo operations (we don't execute the doOp here, because it has already been executed during calculation of the diff image
				mitk::ApplyDiffImageOperation* doOp = new mitk::ApplyDiffImageOperation( mitk::OpTEST, m_Segmentation, diffImage, timeStep );
				mitk::ApplyDiffImageOperation* undoOp = new mitk::ApplyDiffImageOperation( mitk::OpTEST, m_Segmentation, diffImage, timeStep );
				undoOp->SetFactor( -1.0 );
				std::stringstream comment;
				comment << "Accept all interpolations (" << totalChangedSlices << ")";
				mitk::OperationEvent* undoStackItem = new mitk::OperationEvent( mitk::DiffImageApplier::GetInstanceForUndo(), doOp, undoOp, comment.str() );
				mitk::UndoController::GetCurrentUndoModel()->SetOperationEvent( undoStackItem );

				// acutally apply the changes here
				mitk::DiffImageApplier::GetInstanceForUndo()->ExecuteOperation( doOp );
			}
		}

		m_FeedbackNode->SetData(NULL);
		mitk::RenderingManager::GetInstance()->RequestUpdateAll();
	}
}


void InteractiveSegmPanelWidget::UpdateHelperInformation( )
{
	wxTextCtrl *helper = dynamic_cast<wxTextCtrl*> ( wxWindow::FindWindowById( wxID_HELP_CONTROL, this ) );
	if ( helper == NULL )
	{
		return;
	}

	if ( !IsSelectionEnabled( ) )
	{
		helper->SetValue( m_InitialHelpDescription );
		return;
	}

	switch ( m_InteractiveSegmInteractorHelper->GetToolType( ) )
	{
	case msp::InteractiveSegmInteractor::ADDTOOL:
		helper->SetValue( "Use a lasso to add voxels\nUsage:\n\
Left mouse click + drag mouse: draw a green lasso and the inner part will be painted when unpressing left mouse button" );
		break;

	case msp::InteractiveSegmInteractor::SUBTRACTTOOL:
		helper->SetValue( "Use a lasso to subtract voxels\nUsage:\n\
Left mouse click + drag mouse: draw a red lasso and the inner part will be unpainted when unpressing left mouse button" );
		break;
	
	case msp::InteractiveSegmInteractor::PAINTTOOL:
		helper->SetValue( "Adjust the Brush size using the Set Brush Size slider. \n\
Left mouse click: Paint using a brush to add pixels to the segmentation. " );
		break;

	case msp::InteractiveSegmInteractor::WIPETOOL:
		helper->SetValue( "Adjust the Brush size using the Set Brush Size slider. \n\
Left mouse click: Paint using a brush to remove pixels to the segmentation. " );
		break;

	case msp::InteractiveSegmInteractor::CORRECTIONTOOL:
		helper->SetValue( "Trace a line to cut away unneeded portions of \
the segmentation.\nUsage:\nLeft mouse click + drag mouse: Draw a line" );
		break;

	case msp::InteractiveSegmInteractor::FILLREGIONTOOL:
		helper->SetValue( "Fill a hole surrounded by segmented region.\n\
Usage\n Left mouse click: Paint hole." );
		break;

	case msp::InteractiveSegmInteractor::ERASEREGIONTOOL:
		helper->SetValue( "Erase an isle. \n Usage\n Left mouse click: Erase isle" );
		break;

	case msp::InteractiveSegmInteractor::REGIONGROWINGTOOL:
		helper->SetValue( "Usage:\nSelect a point inside the region you want to segment\
and drag the mouse up/down to adjust the contour to segment. Unpress mouse button to paint inner zone" );
		break;

	case msp::InteractiveSegmInteractor::POLYGONTOOL:
		helper->SetValue( "This feature allows creating a mask image using a polygon. \
The polygon is defined using a set of control points that the user can add/move/remove. \n\
Usage:\n\
- Space key: paint inner zone\n\
- Tab key: Change mode: add new points at the end or modify current points\n\
- Shift + left mouse button: Add a new point\n\
- Left mouse button over a point: Select a point and move it\n\
- Left mouse button over a mask image with empty points: Automatically create a set of control points\n\
- Wheel mouse button: Change slide and keep current polygon" );
		break;
	case msp::InteractiveSegmInteractor::LIVEWIRE:
		helper->SetValue( "This feature allows creating a mask image using a live-wire. \
The live-wire is defined using a set of control points that the user can add/move/remove. \n\
Usage:\n\
- Space key: paint inner zone\n\
- Tab key: Change mode: add new points at the end or modify current points\n\
- Shift + left mouse button: Add a new point\n\
- Left mouse button over a point: Select a point and move it\n\
- Left mouse button over a mask image with empty points: Automatically create a set of control points\n\
- Wheel mouse button: Change slide and keep current polygon" );
		break;
	case msp::InteractiveSegmInteractor::SPLINE:
		helper->SetValue( "This feature allows creating a mask image using a spline. \
The spline is defined using a set of control points that the user can add/move/remove. \n\
Usage:\n\
- Space key: paint inner zone\n\
- Tab key: Change mode: add new points at the end or modify current points\n\
- Shift + left mouse button: Add a new point\n\
- Left mouse button over a point: Select a point and move it\n\
- Left mouse button over a mask image with empty points: Automatically create a set of control points\n\
- Wheel mouse button: Change slide and keep current polygon" );
		break;
		
	default:
		helper->SetValue( m_InitialHelpDescription );
		break;
	}
}

void InteractiveSegmPanelWidget::SetTimeStep( int time )
{
	BaseWindow::SetTimeStep( time );

	// Update interactor
	msp::InteractiveSegmInteractor::Pointer interactor;
	interactor = m_InteractiveSegmInteractorHelper->GetInteractor( );
	if ( interactor.IsNull( ) )
	{
		return;
	}

	mitk::PolygonTool * cTool = dynamic_cast<mitk::PolygonTool *>(interactor->GetCurrentTool());
	if ( cTool )
	{
		cTool->UpdateTimeStep( time );
	}
}

void InteractiveSegmPanelWidget::OnBtnCreateSurfaceMesh( wxCommandEvent& event )
{
	m_processor->Update( );
}

