/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "InteractiveSegmInteractorHelper.h"
#include "coreDataEntityHelper.h"
#include "coreReportExceptionMacros.h"
#include "coreDataEntityHolder.h"

#include <mitkBaseRenderer.h>
#include <mitkDataTree.h>
#include <mitkRenderingManager.h>
#include <mitkProperties.h>

#include "wxMitkRenderWindow.h"

#include "vtkImageMathematics.h"
#include "vtkImageShiftScale.h"
#include "vtkStringArray.h"
#include "vtkPointData.h"


InteractiveSegmInteractorHelper::InteractiveSegmInteractorHelper()
{
	m_InputDataHolder = Core::DataEntityHolder::New();
	m_InputDataHolder->AddObserver( 
		this, 
		&InteractiveSegmInteractorHelper::OnModifiedInput );

	m_maskImageDataHolder = Core::DataEntityHolder::New();
	m_maskImageDataHolder->AddObserver<InteractiveSegmInteractorHelper>(
		this, 
		&Self::OnMaskImageModified );

	m_selectedPointHolder = Core::DataEntityHolder::New();

	m_InteractorStateHolder = InteractorStateHolderType::New();
	m_InteractorStateHolder->SetSubject( INTERACTOR_DISABLED );

	SetNewMaskImage(false);
	m_nextRoiLevel = 1;
}


InteractiveSegmInteractorHelper::~InteractiveSegmInteractorHelper( void )
{

	m_InputDataHolder->RemoveObserver( 
		this, 
		&InteractiveSegmInteractorHelper::OnModifiedInput );

	if ( m_SelectedDataHolder.IsNotNull() )
	{
		m_SelectedDataHolder->RemoveObserver( 
			this, 
			&InteractiveSegmInteractorHelper::OnModifiedSelected );
	}

	if ( m_maskImageDataHolder.IsNotNull() )
	{
		m_maskImageDataHolder->RemoveObserver<InteractiveSegmInteractorHelper>(
			this, 
			&Self::OnMaskImageModified );
	}

}

void InteractiveSegmInteractorHelper::SetRenderingTree( 
	Core::RenderingTree::Pointer val )
{
	m_RenderingTree = val;
}

void InteractiveSegmInteractorHelper::SetSelectedImageHolder(
	Core::DataEntityHolder::Pointer dataEntity)
{
	if ( m_SelectedDataHolder.IsNotNull() )
	{
		m_SelectedDataHolder->RemoveObserver( 
			this, 
			&InteractiveSegmInteractorHelper::OnModifiedSelected );
	}
	m_SelectedDataHolder = dataEntity;
	m_SelectedDataHolder->AddObserver( 
		this, 
		&InteractiveSegmInteractorHelper::OnModifiedSelected );
}

Core::DataEntityHolder::Pointer InteractiveSegmInteractorHelper::GetSelectedImageHolder( )
{
	m_RenderingTree->Show( m_maskImageDataHolder->GetSubject(), false );
	return m_SelectedDataHolder;
}

void InteractiveSegmInteractorHelper::StartInteraction()
{
	if ( m_InputDataHolder.IsNull() )
	{
		return;
	}

	Core::DataEntity::Pointer inputDataEntity;
	inputDataEntity = m_InputDataHolder->GetSubject();
	if ( inputDataEntity.IsNull( ) )
	{
		return;
	}

	//CreatePointData(inputDataEntity);

	// Create a mask image with N time steps
	CreateMaskImage( inputDataEntity );
	mitk::DataTreeNode::Pointer segmentationNode;
	Core::CastAnyProcessingData( m_RenderingTree->Add(m_maskImageDataHolder->GetSubject()),segmentationNode);

	segmentationNode->SetProperty( "name", mitk::StringProperty::New( "Segmentation" ) );

	mitk::Color color;
	color.Set(1,0,0);
	// visualization properties
	segmentationNode->SetProperty( "binary", mitk::BoolProperty::New(true) );
	segmentationNode->SetProperty( "color", mitk::ColorProperty::New(color) );
	segmentationNode->SetProperty( "texture interpolation", mitk::BoolProperty::New(false) );
	segmentationNode->SetProperty( "layer", mitk::IntProperty::New(10) );
	segmentationNode->SetProperty( "levelwindow", mitk::LevelWindowProperty::New( mitk::LevelWindow(0.5, 1) ) );
	segmentationNode->SetProperty( "opacity", mitk::FloatProperty::New(0.3) );
	segmentationNode->SetProperty( "segmentation", mitk::BoolProperty::New(true) );
	//Here we change the reslice interpolation mode for a segmentation, so that contours in rotated slice can be shown correctly
	segmentationNode->SetProperty( "reslice interpolation", mitk::VtkResliceInterpolationProperty::New(VTK_RESLICE_CUBIC) );
	// initialize showVolume to false to prevent recalculating the volume while working on the segmentation
	segmentationNode->SetProperty( "showVolume", mitk::BoolProperty::New( false ) );


	// Create interactor
	if ( m_Interactor.IsNull( ) )
	{
		m_Interactor = msp::InteractiveSegmInteractor::New( 
			m_RenderingTree,
			m_maskImageDataHolder,
			m_InputDataHolder);
	}	

	
	try
	{
		m_Interactor->ConnectToDataTreeNode();
		//m_Interactor->SetPixelValue(3);
		//m_Interactor->SetEraseOnlySelectedColor(GetEraseOnlySelectedColor());

		m_InteractorStateHolder->SetSubject( INTERACTOR_ENABLED );

	}coreCatchExceptionsReportAndNoThrowMacro( InteractiveSegmInteractorHelper::StartInteraction );

}

void InteractiveSegmInteractorHelper::StopInteraction( bool show )
{
	if ( m_Interactor.IsNull() )
	{
		return;
	}
	
	m_Interactor->DisconnectFromDataTreeNode();

	// Disable destruction to allow reuse it for polygon
	//m_Interactor = NULL;

	m_InteractorStateHolder->SetSubject( INTERACTOR_DISABLED );

}

void InteractiveSegmInteractorHelper::OnModifiedInput()
{
	try{

		if ( m_InteractorStateHolder->GetSubject() == INTERACTOR_ENABLED && 
			 m_InputDataHolder->GetSubject( ).IsNull( ) )
		{
			StopInteraction();
			m_Interactor = NULL;
		}
	}
	coreCatchExceptionsLogAndNoThrowMacro( InteractiveSegmInteractorHelper::OnModifiedInput );
}

void InteractiveSegmInteractorHelper::OnModifiedSelected()
{
	try{

		Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
		Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();

		if ( m_SelectedDataHolder.IsNull() )
		{
			m_InputDataHolder->SetSubject( NULL );
			m_maskImageDataHolder->SetSubject( NULL );
//			m_selectedPointHolder->SetSubject ( NULL );
			return;
		}

		Core::DataEntity::Pointer selectedDataEntity;
		selectedDataEntity = m_SelectedDataHolder->GetSubject();
		if ( selectedDataEntity.IsNull( ) )
		{
			m_InputDataHolder->SetSubject( NULL );
			m_maskImageDataHolder->SetSubject( NULL );
//			m_selectedPointHolder->SetSubject ( NULL );
			return;
		}

		// Data entity is a ROI -> Check for the father
		if ( selectedDataEntity->GetType() & Core::ROITypeId )
		{
			if ( selectedDataEntity->GetFather( ).IsNotNull( ) && 
				selectedDataEntity->GetFather( )->IsImage() )
			{
				m_maskImageDataHolder->SetSubject( selectedDataEntity );
				m_InputDataHolder->SetSubject( selectedDataEntity->GetFather( ) );
			}
		}
		else if ( selectedDataEntity->IsImage() )
		{
			m_InputDataHolder->SetSubject( selectedDataEntity );
		}
		else
		{
			m_maskImageDataHolder->SetSubject( NULL );
			m_InputDataHolder->SetSubject( NULL );
		}

		// Connect subjects. When any of the holders is set to NULL, this
		// will also be set to NULL and the reference will be removed
		if ( m_maskImageDataHolder->GetSubject().IsNotNull( ) )
		{
			list->ConnectInputHolder( m_maskImageDataHolder );
		}
		if ( m_InputDataHolder->GetSubject().IsNotNull( ) )
		{
			list->ConnectInputHolder( m_InputDataHolder );
		}

	}
	coreCatchExceptionsLogAndNoThrowMacro( InteractiveSegmInteractorHelper::OnModifiedSelected );

}

InteractiveSegmInteractorHelper::InteractorStateHolderType::Pointer 
InteractiveSegmInteractorHelper::GetInteractorStateHolder() const
{	
	return m_InteractorStateHolder;
}

void InteractiveSegmInteractorHelper::OnMaskImageModified()
{
	Core::DataEntity::Pointer maskImage;
	maskImage = m_maskImageDataHolder->GetSubject();
	if ( maskImage.IsNull( ) )
		std::cout << "NULL" <<std::endl;
}

void InteractiveSegmInteractorHelper::SetColor(float r, float g, float b) {
	// Change the color of the ROI
	if ( m_RenderingTree->IsDataEntityRendered( m_maskImageDataHolder->GetSubject() ) ) {
		mitk::DataTreeNode::Pointer node;
		boost::any anyData = m_RenderingTree->GetNode( m_maskImageDataHolder->GetSubject() );
		Core::CastAnyProcessingData( anyData, node );
		node->SetColor(r,g,b);
	}
}

void InteractiveSegmInteractorHelper::SetOpacity(float opacity)
{
		// Change the color of the ROI
	if ( m_RenderingTree->IsDataEntityRendered(m_maskImageDataHolder->GetSubject()) )
	{
		m_RenderingTree->SetProperty(m_maskImageDataHolder->GetSubject(), blTag::New( "opacity", opacity ) );
	}
}

bool InteractiveSegmInteractorHelper::IsNewMaskImage() {
	return bNewMaskImage;
} 

void InteractiveSegmInteractorHelper::SetNewMaskImage(bool isNew) {
	bNewMaskImage = isNew;
} 


int InteractiveSegmInteractorHelper::GetNextRoiLevel() {
	return m_nextRoiLevel;
}

void InteractiveSegmInteractorHelper::SetNextRoiLevel(int n) {
	m_nextRoiLevel = n;
//	if ( m_ContourInteractor.IsNotNull() )
//	{
//		m_ContourInteractor->SetPixelValue(GetNextRoiLevel());
//	}
}


std::string InteractiveSegmInteractorHelper::GetNumMaskImagesString(int n) {
	if (n<0) return ("-" + GetNumMaskImagesString(-1*n));
	if (n==0) return "0";

	std::string ret = "";
	while (n>0) {
		ret = std::string(1,(char)(n%10+'0')) + ret;
		n/=10;
	}

	return ret;
}

void InteractiveSegmInteractorHelper::CreateMaskImage( 
	Core::DataEntity::Pointer inputDataEntity )
{
	Core::DataEntity::Pointer maskImage;
	maskImage = m_maskImageDataHolder->GetSubject();
	if ( maskImage.IsNull( ) )
	{
		std::string maskImageName = "MaskImage";
	
		maskImage = Core::DataEntity::New( ); 
		maskImage->SetType( Core::DataEntityType( Core::ImageTypeId  | Core::ROITypeId ) );
		maskImage->GetMetadata()->SetName( maskImageName );
		maskImage->Resize( inputDataEntity->GetNumberOfTimeSteps(), typeid( Core::vtkImageDataPtr ) );
		maskImage->SetFather( inputDataEntity );

		m_maskImageDataHolder->SetSubject( maskImage );

		//// Create empty image
		Core::vtkImageDataPtr referenceImage; 
		inputDataEntity->GetProcessingData( referenceImage, 0, false, true );

		vtkSmartPointer<vtkImageMathematics> operation = vtkSmartPointer<vtkImageMathematics>::New();
		operation->SetInput1(referenceImage);
		operation->SetInput2(referenceImage);
		operation->SetOperationToSubtract();
		operation->Update();

		vtkSmartPointer<vtkImageShiftScale> scaleFilter = vtkImageShiftScale::New();
		scaleFilter->SetOutputScalarTypeToUnsignedChar();
		scaleFilter->SetInput(operation->GetOutput());
		scaleFilter->Update();
	
		for ( unsigned i = 0 ; i < inputDataEntity->GetNumberOfTimeSteps() ;i++ )
		{
			Core::vtkImageDataPtr workingImage; 
			maskImage->GetProcessingData( workingImage, i );
			workingImage->DeepCopy( scaleFilter->GetOutput() );
		}
	}	
}



void InteractiveSegmInteractorHelper::CreatePointData( 
	Core::DataEntity::Pointer inputDataEntity )
{
	Core::DataEntity::Pointer selectionPointDE = m_selectedPointHolder->GetSubject();
	if ( selectionPointDE.IsNull( ) ) 
	{
		selectionPointDE = Core::DataEntity::New(Core::PointSetTypeId);
		selectionPointDE->GetMetadata()->SetName( "Correction Point" );
		selectionPointDE->Resize( 1, typeid( Core::vtkPolyDataPtr ) );

		m_selectedPointHolder->SetSubject( selectionPointDE );
		//Just put a point somewhere
		Core::vtkPolyDataPtr pointSet = vtkPolyData::New();
		vtkSmartPointer<vtkPoints> points = vtkPoints::New();
		points->Allocate(1);

		points->InsertPoint(0,0,0,0);
		pointSet->SetPoints(points);
		pointSet->Update();
		selectionPointDE->SetTimeStep(pointSet);

		vtkSmartPointer<vtkStringArray> stringData;
		stringData = vtkSmartPointer<vtkStringArray>::New();
		stringData->SetName( "LandmarksName" );
		stringData->SetNumberOfTuples(1);
		stringData->InsertValue(0, "pt");

		pointSet->GetPointData()->AddArray( stringData );
		selectionPointDE->SetFather(inputDataEntity);
		Core::DataEntityHelper::AddDataEntityToList(selectionPointDE);
		m_RenderingTree->Add(selectionPointDE,true,true);
	}
}




Core::DataEntityHolder::Pointer InteractiveSegmInteractorHelper::GetMaskImageDataHolder()
{
	return m_maskImageDataHolder;
}

msp::InteractiveSegmInteractor::Pointer InteractiveSegmInteractorHelper::GetInteractor()
{
	return m_Interactor;
}

void InteractiveSegmInteractorHelper::SetToolType(msp::InteractiveSegmInteractor::TOOLS_TYPE val)
{
	if(GetToolType()!=val)
		msp::InteractiveSegmInteractor::SetToolType(val);

	if ( m_InteractorStateHolder->GetSubject() == INTERACTOR_ENABLED )
	{
		StopInteraction();
	}
	StartInteraction();
}

msp::InteractiveSegmInteractor::TOOLS_TYPE InteractiveSegmInteractorHelper::GetToolType()
{
	return msp::InteractiveSegmInteractor::GetToolType();
}

void InteractiveSegmInteractorHelper::SetBrushSize(int val)
{
	if ( m_InteractorStateHolder->GetSubject() == INTERACTOR_ENABLED )
	{
		if(m_Interactor.IsNotNull())
			m_Interactor->SetBrushSize(val);
	}
}

int InteractiveSegmInteractorHelper::GetBrushSize()
{
	int sz = -1;
	if ( m_InteractorStateHolder->GetSubject() == INTERACTOR_ENABLED )
	{
		if(m_Interactor.IsNotNull())
			sz = m_Interactor->GetBrushSize();
	}
	return sz;
}
