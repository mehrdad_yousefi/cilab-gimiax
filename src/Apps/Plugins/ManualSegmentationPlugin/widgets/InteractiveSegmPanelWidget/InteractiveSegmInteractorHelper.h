/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef InteractiveSegmInteractorHelper_H
#define InteractiveSegmInteractorHelper_H


#include "coreObject.h"
#include <mitkCommon.h>
#include <mitkPointSet.h>
#include "coreSmartPointerMacros.h"
#include "coreDataEntityHolder.h"
#include "coreCommonDataTypes.h"
#include "InteractiveSegmInteractor.h"

namespace Core{ namespace Widgets {
	class AcquireDataEntityInputControl;
	class RenderWindowBase;
}}

/**
\brief This class is used by the PluginTab and the ViewToolBar to set the 
currentDataEntityHolder
       and the rendering tree that are needed by the contour Interactor

The mask image has only 1 time step.
Each time the current time step, changes, a new mask image is created

\ingroup ManualSegmentationPlugin
\author Luigi Carotenuto
\date 16 sept 2011
*/
class InteractiveSegmInteractorHelper 
	: public Core::SmartPointerObject
{

public:
	enum INTERACTOR_STATE
	{
		INTERACTOR_DISABLED,
		INTERACTOR_ENABLED
	};

	typedef Core::DataHolder<INTERACTOR_STATE> InteractorStateHolderType;

public:
	//! Create itk typedefs and new function for InteractiveSegmInteractorHelper
	coreDeclareSmartPointerClassMacro(
		InteractiveSegmInteractorHelper, 
		Core::SmartPointerObject);

	//!
	void SetRenderingTree(Core::RenderingTree::Pointer val);

	//!
	void SetSelectedImageHolder(Core::DataEntityHolder::Pointer dataEntity);

	//!
	Core::DataEntityHolder::Pointer GetSelectedImageHolder( );

	void OnModifiedSelected( );
		
	//!
	void StartInteraction( );

	//!
	void StopInteraction( bool show = true );
	
	//! Set the color of the mask image. This method should only be called after the mask image is created and attached to the rendering tree
	void SetColor(float r, float g, float b);

	//! Set the opacity of the mask image. This method should only be called after the mask image is created and attached to the rendering tree
	void SetOpacity(float opacity);


	//!
	InteractorStateHolderType::Pointer GetInteractorStateHolder() const;

	//!
	bool IsNewMaskImage();

	//!
	void SetNewMaskImage(bool isNew);

	//!
	int GetNextRoiLevel();

	//!
	void SetNextRoiLevel(int n);

	//!
//	void SetEraseOnlySelectedColor(bool b);

	//!
//	bool GetEraseOnlySelectedColor();

	//! 
	Core::DataEntityHolder::Pointer GetMaskImageDataHolder();

	msp::InteractiveSegmInteractor::Pointer GetInteractor();

	void SetToolType(msp::InteractiveSegmInteractor::TOOLS_TYPE);

	msp::InteractiveSegmInteractor::TOOLS_TYPE GetToolType();

	void SetBrushSize(int);

	int GetBrushSize();

private:
	//!
	InteractiveSegmInteractorHelper(void);

	//!
	~InteractiveSegmInteractorHelper(void);

	/** stop and start interaction
	\note The m_InputDataHolder MUST be an image
	*/
	void OnModifiedInput( );

	//!
	void OnTimeStep( );

	//!
	void OnMaskImageModified( );

	//!
	void CreateMaskImage( Core::DataEntity::Pointer inputDataEntity );

	//!
	void CreatePointData( Core::DataEntity::Pointer inputDataEntity );

	//!
	std::string GetNumMaskImagesString(int n);

private:

	//! Input data
	Core::DataEntityHolder::Pointer m_SelectedDataHolder;

	//! Input data
	Core::DataEntityHolder::Pointer m_InputDataHolder;

	//! selected point
	Core::DataEntityHolder::Pointer m_selectedPointHolder;

	//! Roi data
	Core::DataEntityHolder::Pointer m_maskImageDataHolder;

	//!
	msp::InteractiveSegmInteractor::Pointer m_Interactor;

	//!
	Core::RenderingTree::Pointer m_RenderingTree;

	//! State of the interactor to update the views
	InteractorStateHolderType::Pointer m_InteractorStateHolder;

	//!
	bool bNewMaskImage;

	//!
	int m_nextRoiLevel;

	//!
//	bool m_bEraseOnlySelectedColor;

	coreDeclareNoCopyConstructors(InteractiveSegmInteractorHelper);
};

#endif //InteractiveSegmInteractorHelper_h
