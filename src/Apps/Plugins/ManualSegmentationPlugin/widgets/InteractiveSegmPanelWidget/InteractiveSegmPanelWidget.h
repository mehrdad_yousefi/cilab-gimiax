/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _InteractiveSegmPanelWidget_H
#define _InteractiveSegmPanelWidget_H

#include "InteractiveSegmPanelWidgetUI.h"
#include "isegProcessor.h"

#include "InteractiveSegmInteractorHelper.h"
// CoreLib
#include "coreRenderingTree.h"
#include "coreFrontEndPlugin.h"
#include "coreCommonDataTypes.h"
#include "coreSelectionToolWidget.h"

// GuiBridgeLib
#include "gblWxConnectorOfWidgetChangesToSlotFunction.h"

#include <vector>
#include <map>

#include "blMITKUtils.h"
#include "mitkSegmentationInterpolationController.h"



namespace Core{ namespace Widgets {
	class AcquireDataEntityInputControl;
	class UserHelper;
	class DataEntityListBrowser;
}}

namespace Core{
	namespace Widgets {


/**
\ingroup ManualSegmentationPlugin
\author Valeria Barbarito
\date 02 10 2011
*/

#define wxID_InteractiveSegmentationWidget wxID("wxID_InteractiveSegmentationWidget")

class PLUGIN_EXPORT InteractiveSegmPanelWidget : 
	public InteractiveSegmPanelWidgetUI,
	public SelectionToolWidget
{

	typedef struct{
		wxBitmapButton *m_btn;
		wxBitmap m_iconOnFile, m_iconOffFile;
		void SetElement(wxBitmapButton *btn, wxBitmap iconOnFile, wxBitmap iconOffFile = wxNullBitmap)
		{
			m_iconOnFile = iconOnFile;
			m_iconOffFile = iconOffFile.IsOk( ) ? iconOffFile : iconOnFile;
			m_btn = btn;
		}

		void SetBitmapLabel(bool onOff)
		{
			if(m_btn!=NULL)
				m_btn->SetBitmapLabel( onOff? m_iconOnFile : m_iconOffFile );
		}
	} TOOL_BTN;

	typedef msp::InteractiveSegmInteractor InterType;
	typedef InterType::TOOLS_TYPE TOOLS_TYPE;

	// OPERATIONS
public:
	coreDefineBaseWindowFactory( InteractiveSegmPanelWidget )

	//!
	cilabDeclareExceptionMacro( Exception, std::exception );

		InteractiveSegmPanelWidget(
		wxWindow* parent, 
		int id = wxID_InteractiveSegmentationWidget,
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=0);

	//!
	~InteractiveSegmPanelWidget( );

	//!
	void OnInit();

	//!
	bool Enable( bool enable /*= true */ );

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

	//!
	void StartInteractor( );

	//!
	void StopInteraction( );

	//!
	bool IsSelectionEnabled( );

	//!
	void OnBtnAdd( wxCommandEvent& event );

	//!
	void OnBtnRGrow( wxCommandEvent& event );

	//!
	void OnBtnInterpolate(wxCommandEvent &event);

	//!
	void OnBtnAcceptAll(wxCommandEvent &event);

	//! Update m_ROIInteractorHelper and Mask Image data entity ROI levels and names
	void UpdateData();

private:
    wxDECLARE_EVENT_TABLE();

	//!
	void OnModifiedSelectedReferenceImageDataEntityHolder();
	
	//!
	void OnModifiedSelectedInputROIDataEntityHolder();

	//!
	void OnNewSelectedInputROIDataEntityHolder();

	//!
	void OnBtnEnable(wxCommandEvent& event);

	//!
	//void OnBtnAdd( wxCommandEvent& event );

	//!
	void OnBtnSubtract( wxCommandEvent& event );
	
	//!
	void OnBtnPaint( wxCommandEvent& event );
	
	//!
	void OnBtnWipe( wxCommandEvent& event );
	
	//!
	//void OnBtnRGrow( wxCommandEvent& event );

	//!
	void OnBtnCorrect(wxCommandEvent &event);

	//!
	void OnBtnEraseRegion(wxCommandEvent &event);

	//!
	void OnBtnFillRegion(wxCommandEvent &event);

	//!
    void OnBtnPolygon(wxCommandEvent &event);

	//!
    void OnBtnLiveWire(wxCommandEvent &event);

	//!
    void OnBtnSpline(wxCommandEvent &event);

	//!
	void OnBrushSizeChanged(wxScrollEvent &event);

	//!
	void OnBtnUndo( wxCommandEvent& event );

	//!
	void OnBtnRedo( wxCommandEvent& event );

	//!
	//void OnBtnInterpolate(wxCommandEvent &event);

	//!
	void OnBtnNewSegmentation(wxCommandEvent &event);

	//!
	void OnBtnAcceptLast(wxCommandEvent &event);

	//!
	//void OnBtnAcceptAll(wxCommandEvent &event);

	//!
	void OnBtnCreateSurfaceMesh( wxCommandEvent& event );

	//!
	void UpdateWidget();

	//! Update m_ROIInteractorHelper and Mask Image data entity ROI levels and names
	//void UpdateData();

	//!
	//void SetRenderingTree( RenderingTree::Pointer tree );

	//!
	Core::DataEntity::Pointer CreateROIImageDataEntityFromImageDataEntity( Core::DataEntity::Pointer inputDataEntity );


	void UpdateActiveToolBtn( TOOLS_TYPE tool);

	void AddToolButton(TOOLS_TYPE tType,
		wxBitmapButton *btn,  wxBitmap onIcon,   wxBitmap offIcon = wxNullBitmap );

	void ChangeTool(TOOLS_TYPE tool);

	void InitializeInterpolation();

	void OnTransversalSliceChanged(const itk::EventObject& e);

	void OnSagittalSliceChanged(const itk::EventObject& e);

	void OnFrontalSliceChanged(const itk::EventObject& e);

	bool TranslateAndInterpolateChangedSlice(const itk::EventObject& e, unsigned int windowID);

	void Interpolate( mitk::PlaneGeometry* plane, unsigned int timeStep );

	void OnInterpolationActivated(bool on);

	void OnInterpolationInfoChanged(const itk::EventObject& /*e*/);

	void UpdateVisibleSuggestion();

	void UpdateHelperInformation( );

	void SetTimeStep( int time );

	// ATTRIBUTES
private:

	//! Processor
	InteractiveSegmentationProcessor::Pointer m_processor;

	//! ROI interactor helper
	InteractiveSegmInteractorHelper::Pointer m_InteractiveSegmInteractorHelper;

	//! Rename levels
	std::map<int,std::string> m_MapLevelsToLabel;

	//Core::DataEntity::Pointer m_dataEntity;
	//Core::DataEntityHolder::Pointer m_dataEntityHolder;
	bool m_bEraseOnlySel;
	int m_Selection;
	int m_countLabels;

	boost::signals::connection m_ConnOnAdd;
	boost::signals::connection m_ConnOnRem;

	Core::BaseFilterOutputPort::Pointer m_OutputPort;

	//!
	bool m_LevelWindowInteractorState;

	//!permits to discriminate between local calls and external calls of the StartInteractor fnc
	bool m_StartInteractorLocalCall;


	mitk::UndoController* m_UndoController;

	typedef std::map<TOOLS_TYPE, TOOL_BTN> BtnMap;
	BtnMap m_toolsBtnMap;

	TOOLS_TYPE m_currentTool;

	mitk::SegmentationInterpolationController::Pointer m_Interpolator;

	bool m_InterpolationEnabled;

	mitk::DataTreeNode::Pointer m_FeedbackNode;

	mitk::Image* m_Segmentation;

	int	m_LastSliceDimension;
	int m_LastSliceIndex;

	std::string m_InitialHelpDescription;

	//! observer tags
	std::map<mitk::SliceNavigationController::Pointer, unsigned long> m_ObserverTags;
};

} //namespace Widgets
} //namespace Core

#endif //_InteractiveSegmPanelWidget_H
