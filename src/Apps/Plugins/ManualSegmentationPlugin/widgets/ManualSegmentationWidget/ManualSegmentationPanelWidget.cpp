/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "ManualSegmentationPanelWidget.h"
#include "mlrProcessor.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"

// Core
#include "coreVTKPolyDataHolder.h"
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"
#include "coreRenderingTree.h"
#include "coreDataEntityListBrowser.h"
#include "coreReportExceptionMacros.h"
#include "corePluginTab.h"
#include "coreDataEntity.h"
#include "coreDataEntityList.h"
#include "coreDataEntityList.txx"
#include "coreProcessorInputWidget.h"
#include "coreMultiRenderWindowFocus.h"
#include "coreMultiRenderWindowMITK.h"

// STD
#include <limits>
#include <cmath>
#include <algorithm>
#include <sstream>

// Core
#include "coreDataTreeHelper.h"

#include "mitkLookupTableProperty.h"
#include "vtkPointData.h"


using namespace Core::Widgets;

// Event the widget
BEGIN_EVENT_TABLE(ManualSegmentationPanelWidget,ManualSegmentationPanelWidgetUI)
EVT_CHECKBOX(wxID_CHK_COLOR,Core::Widgets::ManualSegmentationPanelWidget::OnChkColorChanged)
    EVT_COMBOBOX(wxID_COMBO_LEVEL,Core::Widgets::ManualSegmentationPanelWidget::OnNewComboSelection)
    EVT_COMBOBOX(wxID_COMBO_LUT,Core::Widgets::ManualSegmentationPanelWidget::OnNewComboLUTSelection)
    EVT_BUTTON(wxID_BTN_ADD,Core::Widgets::ManualSegmentationPanelWidget::OnAddBtn)
    EVT_TOGGLEBUTTON(wxID_EnableInteraction, Core::Widgets::ManualSegmentationPanelWidget::OnEnableInteraction)
END_EVENT_TABLE()

class wxROIValue : public wxClientData
{
public:
    wxROIValue( int value )
    {
        m_Value = value;
    }

    int m_Value;
};

ManualSegmentationPanelWidget::ManualSegmentationPanelWidget(
                              wxWindow* parent, 
                              int id,
                              const wxPoint& pos, 
                              const wxSize& size, 
                              long style) :
    ManualSegmentationPanelWidgetUI(parent, id, pos, size, style)
{
    try 
    {
        m_LevelWindowInteractorState = true;
        int nRI = ManualSegmentationProcessor::REFERENCE_IMAGE;
        int nIR = ManualSegmentationProcessor::INPUT_ROI;

        m_processor = ManualSegmentationProcessor::New();       
        m_processor->GetInputDataEntityHolder( nRI )->AddObserver(
            this,
            &ManualSegmentationPanelWidget::OnModifiedSelectedReferenceImageDataEntityHolder);
        m_processor->GetInputDataEntityHolder( nIR )->AddObserver(
            this,
            &ManualSegmentationPanelWidget::OnModifiedSelectedInputROIDataEntityHolder,
            Core::DH_SUBJECT_MODIFIED);
        m_processor->GetInputDataEntityHolder( nIR )->AddObserver(
            this,
            &ManualSegmentationPanelWidget::OnNewSelectedInputROIDataEntityHolder,
            Core::DH_NEW_SUBJECT);

        OnModifiedSelectedReferenceImageDataEntityHolder( );
        m_btnEnableInteraction->SetValue( false );

        m_ROIInteractorHelper = Core::ROIInteractorHelper::New();
        m_ROIInteractorHelper->SetNextRoiLevel(1);

        EnableGUI(false);
        m_roiColorSelector->Enable(false);
        m_chkEraseOnlySelectedColor->SetValue(true);

        SetName("Manual Segmentation");
    }
    coreCatchExceptionsReportAndNoThrowMacro( 
        "ManualSegmentationPanelWidget::ManualSegmentationPanelWidget" );

}

ManualSegmentationPanelWidget::~ManualSegmentationPanelWidget( ) 
{
    StopInteraction();
}

void ManualSegmentationPanelWidget::EnableGUI(bool bEnable)
{
    m_txtAdd->Enable(bEnable);
    m_btnAdd->Enable(bEnable);
    m_chkEraseOnlySelectedColor->Enable(bEnable);
    m_ComboLevel->Enable(bEnable);
    m_ComboLUT->Enable(bEnable);
}

void ManualSegmentationPanelWidget::OnModifiedSelectedReferenceImageDataEntityHolder()
{
    Core::DataEntity::Pointer dataEntity;
    dataEntity = m_processor->GetInputDataEntity(ManualSegmentationProcessor::REFERENCE_IMAGE);

    if ( dataEntity.IsNotNull() )
    {
        m_btnEnableInteraction->Enable(true);
    }
    else
    {
        m_btnEnableInteraction->Enable(false);
    }
}


void ManualSegmentationPanelWidget::OnModifiedSelectedInputROIDataEntityHolder()
{
    UpdateWidget();
}

void Core::Widgets::ManualSegmentationPanelWidget::OnNewSelectedInputROIDataEntityHolder()
{
    Core::DataEntity::Pointer dataEntity = m_processor->GetInputDataEntityHolder(
        ManualSegmentationProcessor::INPUT_ROI)->GetSubject();

    if ( dataEntity.IsNotNull() )
    {
        EnableGUI(true);
    }
    else
    {
        EnableGUI(false);
    }

    bool wasEnabled = IsSelectionEnabled();

    // If the data entity has changed->Stop current interaction
    if ( m_ROIInteractorHelper->GetMaskImageDataHolder()->GetSubject() !=
        m_processor->GetInputDataEntity( ManualSegmentationProcessor::INPUT_ROI ) ||
        dataEntity.IsNull() ) 
    {
        StopInteraction();
    }

    // If it was enabled before, enable it again
    if ( wasEnabled && !IsSelectionEnabled( ) && dataEntity.IsNotNull() ) 
    {
        // Set new input -> ROIInteractor helper
        m_ROIInteractorHelper->GetMaskImageDataHolder()->SetSubject( 
            m_processor->GetInputDataEntity( ManualSegmentationProcessor::INPUT_ROI ) );

        StartInteractor();
    }

    UpdateWidget( );
}

void ManualSegmentationPanelWidget::OnInit()
{
    //Colors
    InitComboLUT();

    setCurrentRoiColor(  );

    m_ROIInteractorHelper->SetRenderingTree( GetRenderingTree() );
    if ( GetListBrowser() )
    {
        m_ROIInteractorHelper->SetSelectedDataHolder( GetListBrowser()->GetSelectedDataEntityHolder() );
        m_ROIInteractorHelper->OnModifiedSelected();
    }

    GetInputWidget(ManualSegmentationProcessor::REFERENCE_IMAGE)->Show(false);

}

std::string ManualSegmentationPanelWidget::RemoveSpaces(std::string str) 
{
    std::string ret = "";

    for ( int i = 0; i < str.size(); i++ )
    {
        if ( str[i] != ' ') ret += str[i];
    }

    return ret;
}

void ManualSegmentationPanelWidget::OnAddBtn(wxCommandEvent& event) 
{
    std::string strToAdd = std::string( m_txtAdd->GetValue().c_str( ) );
    if (RemoveSpaces(strToAdd).size()<1)
    {
        throw Exception( "You must specify a name for the level" );
    }

    int itemId;
    itemId = m_ComboLevel->Append(strToAdd, new wxROIValue( FindNextROILevel( ) ) );
    m_ComboLevel->SetSelection( itemId );
    UpdateData();
}

void ManualSegmentationPanelWidget::OnNewComboSelection(wxCommandEvent& event) 
{
    int selection = 1;
    if ( m_ComboLevel->GetSelection() != -1 )
    {
        wxClientData* data = m_ComboLevel->GetClientObject( m_ComboLevel->GetSelection() );
        wxROIValue* value = dynamic_cast<wxROIValue*>( data );
        selection = value->m_Value;
    }

    // Update data
    m_ROIInteractorHelper->SetNextRoiLevel( selection );

    UpdateWidget();
}

void ManualSegmentationPanelWidget::OnChkColorChanged(wxCommandEvent& event) 
{
    // Update data
    m_ROIInteractorHelper->SetEraseOnlySelectedColor( 
        m_chkEraseOnlySelectedColor->GetValue() );

    UpdateWidget();
}


void ManualSegmentationPanelWidget::UpdateWidget() 
{
    // Update enable intreaction button
    m_btnEnableInteraction->SetValue( IsSelectionEnabled( ) );
    m_btnEnableInteraction->SetLabel( IsSelectionEnabled( )? "Disable" : "Enable" );

    // Update labels
    Core::DataEntity::Pointer dataEntity;
    dataEntity = m_processor->GetInputDataEntity( ManualSegmentationProcessor::INPUT_ROI );
    if (dataEntity.IsNull()) return;

    blTagMap::Pointer labelMap;
    DataEntityTag::Pointer tag = dataEntity->GetMetadata()->FindTagByName( "MultiROILevel" );
    if ( tag.IsNotNull( ) )
    {
        tag->GetValue( labelMap );
    }

    int selection = m_ComboLevel->GetSelection();
    m_ComboLevel->Clear();
    if ( labelMap.IsNotNull() )
    {
        blTagMap::Iterator it = labelMap->GetIteratorBegin();
        for ( ; it!= labelMap->GetIteratorEnd(); it++ )
        {
            int value;
            it->second->GetValue<int>( value );
            m_ComboLevel->Append(it->second->GetName(), new wxROIValue( value ) );
        }

        // restore selection if any selected, otherwise, select the first one
        if ( selection != -1 )
        {
            m_ComboLevel->SetSelection(selection);
        }
        else if ( m_ComboLevel->GetCount() != 0 )
        {
            m_ComboLevel->SetSelection( 0 );
        }
    }

    //update color
    setCurrentRoiColor();

    // Init LUT colors
    if ( GetROINode() )
    {
        int lutType;
        GetROINode()->GetIntProperty( "blLookupTablesType", lutType );
        m_ComboLUT->SetSelection( lutType );
    }
}

//!
Core::BaseProcessor::Pointer ManualSegmentationPanelWidget::GetProcessor( )
{
    return m_processor.GetPointer( );
}
void ManualSegmentationPanelWidget::OnBtnNewMaskImage(wxCommandEvent &event)
{
    m_ROIInteractorHelper->SetNewMaskImage(true);
    StartInteractor();
}

//!
void ManualSegmentationPanelWidget::StartInteractor( )
{
    m_ROIInteractorHelper->SetNextRoiLevel( GetCurrentROILevel() );
    m_ROIInteractorHelper->SetEraseOnlySelectedColor( 
        m_chkEraseOnlySelectedColor->GetValue() );
    m_ROIInteractorHelper->StartInteraction();
    if ( m_ROIInteractorHelper->GetInteractorStateHolder( )->GetSubject() == Core::ROIInteractorHelper::INTERACTOR_DISABLED )
    {
        return;
    }

    m_processor->SetInputDataEntity( 
        ManualSegmentationProcessor::INPUT_ROI, 
        m_ROIInteractorHelper->GetMaskImageDataHolder()->GetSubject() );

    if ( !GetMultiRenderWindow() )
    {
        return ;
    }

    GetMultiRenderWindow()->GetMetadata()->AddTag( "AxisLocked", true );
    m_LevelWindowInteractorState = 
        GetMultiRenderWindow()->GetMetadata()->GetTagValue<bool>( "LevelWindowInteractor" );
    GetMultiRenderWindow()->GetMetadata()->AddTag( "LevelWindowInteractor", false );
    GetMultiRenderWindow()->GetMetadataHolder()->NotifyObservers();

    UpdateWidget( );
}

//!
void ManualSegmentationPanelWidget::StopInteraction( )
{
    if ( m_ROIInteractorHelper.IsNotNull() )
    {
        m_ROIInteractorHelper->StopInteraction();
    }
    if ( GetMultiRenderWindow() )
    {
        GetMultiRenderWindow()->GetMetadata()->AddTag( "AxisLocked", false );
        GetMultiRenderWindow()->GetMetadata()->AddTag( "LevelWindowInteractor", m_LevelWindowInteractorState );
        GetMultiRenderWindow()->GetMetadataHolder()->NotifyObservers();
    }
    m_txtAdd->SetValue( "" );

    UpdateData( );

    UpdateWidget( );
}

//!
bool ManualSegmentationPanelWidget::IsSelectionEnabled( )
{
    if ( m_ROIInteractorHelper.IsNull() )
    {
        return false;
    }
    return m_ROIInteractorHelper->GetInteractorStateHolder()->GetSubject() == 
        Core::ROIInteractorHelper::INTERACTOR_ENABLED;
}


bool ManualSegmentationPanelWidget::Enable( bool enable ) 
{
    bool bReturn = ManualSegmentationPanelWidgetUI::Enable( enable );
    if ( !enable ) 
    {
        StopInteraction( );
    }

    return bReturn;
}

void ManualSegmentationPanelWidget::InitComboLUT() 
{
    m_ComboLUT->Clear();
    for ( int i = 0 ; i < blLookupTables::GetNumberOfLuts(); i++ )
    {
        blLookupTables::LUT_TYPE lutType = blLookupTables::LUT_TYPE( i );
        m_ComboLUT->Append( blLookupTables::GetLUTName( lutType ) );
    }
    m_ComboLUT->SetSelection(blLookupTables::LUT_TYPE_RAINBOW_HUE);
}

void ManualSegmentationPanelWidget::OnNewComboLUTSelection(wxCommandEvent& event) 
{
    blLookupTables::LUT_TYPE lutType;
    int currentSelection = m_ComboLUT->GetCurrentSelection();
    lutType = blLookupTables::LUT_TYPE( currentSelection );
    
    Core::DataEntityHolder::Pointer dataEntityHolder = m_processor->GetInputDataEntityHolder(ManualSegmentationProcessor::INPUT_ROI);
    if ( dataEntityHolder.IsNull() )
    {
        return;
    }

    Core::DataEntity::Pointer dataEntity = dataEntityHolder->GetSubject();
    if ( dataEntity.IsNull() ) 
    {
        return;
    }

    mitk::DataTreeNode::Pointer node;
    boost::any anyData = GetRenderingTree()->GetNode( dataEntity );
    Core::CastAnyProcessingData( anyData, node );
    blMITKUtils::ApplyLookupTableToImage(true,node,lutType);
    mitk::RenderingManager::GetInstance()->RequestUpdateAll( );

    UpdateWidget();
}

void ManualSegmentationPanelWidget::getTableColor(int i, double col[3]) 
{
    col[0] = 1;
    col[1] = 1;
    col[2] = 1;

    vtkLookupTable* vtkLut = GetLUT( );
    if ( vtkLut == NULL )
    {
        return;
    }

    vtkLut->GetColor( i, col );
}

void ManualSegmentationPanelWidget::setCurrentRoiColor() 
{
    // GetUpperWindowBound( ) minimum value is 16 
    if ( GetROINode().IsNotNull() )
    {
        mitk::LevelWindow levelWindow;
        GetROINode()->GetLevelWindow( levelWindow );

        double range[2];
        range[ 0 ] = levelWindow.GetLowerWindowBound();
        range[ 1 ] = levelWindow.GetUpperWindowBound();
        if ( GetLUT() )
        {
            GetLUT()->SetRange( range );
        }
    }

    double nextColor[ 3 ];
    getTableColor( GetCurrentROILevel(), nextColor );
    m_roiColorSelector->SetRGBColour( nextColor );
}

void Core::Widgets::ManualSegmentationPanelWidget::OnEnableInteraction( wxCommandEvent& event )
{
    if (m_btnEnableInteraction->GetValue() == true)
    {
        StartInteractor();
    }
    else 
    {
        StopInteraction( );
    }
}

void Core::Widgets::ManualSegmentationPanelWidget::UpdateData()
{
    // Update ROI Interactor
    m_ROIInteractorHelper->SetNextRoiLevel( GetCurrentROILevel() );
    m_ROIInteractorHelper->SetEraseOnlySelectedColor( 
        m_chkEraseOnlySelectedColor->GetValue() );

    Core::DataEntity::Pointer dataEntity;
    dataEntity = m_processor->GetInputDataEntity(ManualSegmentationProcessor::INPUT_ROI);
    if (dataEntity.IsNull()) return;

    // Add metadata tags
    blTagMap::Pointer ROIlevels = blTagMap::New();
    for ( int i = 0; i < m_ComboLevel->GetCount(); i++ )
    {
        std::string itemString = std::string( m_ComboLevel->GetString( i ).c_str());
        wxClientData* data = m_ComboLevel->GetClientObject( i );
        wxROIValue* value = dynamic_cast<wxROIValue*>( data );
        ROIlevels->AddTag( itemString, value->m_Value );
    }
    dataEntity->GetMetadata( )->AddTag( "MultiROILevel", ROIlevels );

    // If levels is null or empty, compute it and add it to the mask image
    MultiLevelROIProcessor::ComputeLevels( dataEntity );

    // Update image and VTK scalar range to update the legend
    if ( GetROINode().IsNotNull() )
    {
        mitk::Image::Pointer image;
        image = static_cast<mitk::Image*>( GetROINode()->GetData() );
        if (image.IsNotNull()) 
        {
            vtkImageData* vtkImage = image->GetVtkImageData( );
            if ( vtkImage )

            {
                vtkImage->GetPointData()->GetScalars()->Modified();
                vtkImage->Modified();
            }
        }

        // GetUpperWindowBound( ) minimum value is 16 
        mitk::LevelWindow levelWindow;
        GetROINode()->GetLevelWindow( levelWindow );

        double range[2];
        range[ 0 ] = levelWindow.GetLowerWindowBound();
        range[ 1 ] = levelWindow.GetUpperWindowBound();
        GetLUT()->SetRange( range );
    }


    // Notify observers
    dataEntity->Modified();
}

mitk::DataTreeNode::Pointer Core::Widgets::ManualSegmentationPanelWidget::GetROINode()
{
    if ( GetRenderingTree().IsNull() )
    {
        return NULL;
    }

    mitk::DataTreeNode::Pointer node;
    boost::any anyData = GetRenderingTree()->GetNode(
        m_processor->GetInputDataEntity(ManualSegmentationProcessor::INPUT_ROI) );
    Core::CastAnyProcessingData( anyData, node );
    return node;
}

vtkLookupTable* Core::Widgets::ManualSegmentationPanelWidget::GetLUT()
{
    mitk::DataTreeNode::Pointer node;
    node = GetROINode( );

    if ( node.IsNull() )
    {
        return NULL;
    }

    mitk::LookupTableProperty::Pointer mitkLutProp;
    node->GetProperty( mitkLutProp, "LookupTable" );
    mitk::LookupTable::Pointer mitkLut;
    if ( mitkLutProp.IsNull() )
    {
        return NULL;
    }
    mitkLut = mitkLutProp->GetLookupTable();

    vtkLookupTable *vtkLut = mitkLut->GetVtkLookupTable();
    return vtkLut;
}

void Core::Widgets::ManualSegmentationPanelWidget::SetRenderingTree( RenderingTree::Pointer tree )
{
    Core::BaseWindow::SetRenderingTree( tree );
    m_ROIInteractorHelper->SetRenderingTree( GetRenderingTree() );
}

void Core::Widgets::ManualSegmentationPanelWidget::OnFocusBtn( wxCommandEvent &event )
{
    MultiRenderWindowFocus::Pointer m_MultiRenderWindowFocus;
    m_MultiRenderWindowFocus = MultiRenderWindowFocus::New();
    MultiRenderWindowMITK* window = dynamic_cast<MultiRenderWindowMITK*> ( GetMultiRenderWindow() );

    Core::DataEntityHolder::Pointer inputHolder = Core::DataEntityHolder::New( );
    inputHolder->SetSubject( m_processor->GetInputDataEntity( ManualSegmentationProcessor::INPUT_ROI ) );
    if ( m_ComboLevel->GetSelection() == -1 )
    {
        m_MultiRenderWindowFocus->SetPixelValue( 1 );
    }
    else
    {
        wxClientData* data = m_ComboLevel->GetClientObject( m_ComboLevel->GetSelection() );
        wxROIValue* value = dynamic_cast<wxROIValue*>( data );
        m_MultiRenderWindowFocus->SetPixelValue( value->m_Value );
    }
    m_MultiRenderWindowFocus->Init( window, inputHolder );

    inputHolder->NotifyObservers();
}

int Core::Widgets::ManualSegmentationPanelWidget::FindNextROILevel()
{
    Core::DataEntity::Pointer dataEntity;
    dataEntity = m_processor->GetInputDataEntityHolder( ManualSegmentationProcessor::INPUT_ROI )->GetSubject();
    if ( dataEntity.IsNull() )
    {
        return 1;
    }

    blTagMap::Pointer labelMap;
    DataEntityTag::Pointer tag = dataEntity->GetMetadata()->FindTagByName( "MultiROILevel" );
    if ( tag.IsNull( ) )
    {
        return 1;
    }

    tag->GetValue( labelMap );
    if ( labelMap.IsNull() )
    {
        return 1;
    }

    int maxLevel = 1;
    blTagMap::Iterator it = labelMap->GetIteratorBegin();
    for ( ; it!= labelMap->GetIteratorEnd(); it++ )
    {
        int value;
        it->second->GetValue<int>( value );
        maxLevel = std::max( value, maxLevel);
    }

    return maxLevel + 1;
}

int Core::Widgets::ManualSegmentationPanelWidget::GetCurrentROILevel()
{
    int roiLevel;
    if ( m_ComboLevel->GetCount() != 0 && m_ComboLevel->GetSelection() != -1 )
    {
        wxClientData* data = m_ComboLevel->GetClientObject( m_ComboLevel->GetSelection() );
        wxROIValue* value = dynamic_cast<wxROIValue*>( data );
        roiLevel = value->m_Value;
    }
    else
    {
        roiLevel = 1;
    }

    return roiLevel;
}

void Core::Widgets::ManualSegmentationPanelWidget::OnRename( wxCommandEvent &event )
{
    if ( m_ComboLevel->GetSelection() != -1 )
    {
        std::string strToEdit = std::string( m_txtAdd->GetValue().c_str( ) );
        m_ComboLevel->SetString(m_ComboLevel->GetSelection(),strToEdit);
        m_ComboLevel->SetStringSelection( strToEdit );
        UpdateData();
    }
}

