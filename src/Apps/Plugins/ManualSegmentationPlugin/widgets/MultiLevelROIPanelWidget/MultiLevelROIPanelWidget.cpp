/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "MultiLevelROIPanelWidget.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"

// Core
#include "coreVTKPolyDataHolder.h"
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"
#include "coreRenderingTree.h"
#include "coreDataEntityListBrowser.h"
#include "coreReportExceptionMacros.h"
#include "corePluginTab.h"
#include "coreDataEntity.h"
#include "coreDataEntityList.h"
#include "coreDataEntityList.txx"
#include "coreProcessorInputWidget.h"

// STD
#include <limits>
#include <cmath>
#include <algorithm>
#include <sstream>

// Core
#include "coreDataTreeHelper.h"
#include "vtkImageToImageStencil.h"
#include "wxMitkSelectableGLWidget.h"
#include "coreMultiRenderWindowMITK.h"
#include "coreImageDataEntityMacros.h"

#include "vtkImageClip.h"

using namespace Core::Widgets;

// Event the widget
BEGIN_EVENT_TABLE(MultiLevelROIPanelWidget,MultiLevelROIPanelWidgetUI)
END_EVENT_TABLE()


MultiLevelROIPanelWidget::MultiLevelROIPanelWidget(
						 wxWindow* parent, 
						 int id ,
						 const wxPoint& pos, 
						 const wxSize& size, 
						 long style) :
	MultiLevelROIPanelWidgetUI(parent, id, pos, size, style)
{
	m_processor = MultiLevelROIProcessor::New();

	blTagMap::Pointer measures = m_processor->GetIntensityMeasures( );
	blTagMap::Iterator it = measures->GetIteratorBegin();
	for( ; it != measures->GetIteratorEnd() ; it++ )
	{
		blTag::Pointer currentTag = measures->GetTag(it);
		m_cmbStatisticValue->AppendString( currentTag->GetName( ) );
	}

	UpdateWidget( );
}

MultiLevelROIPanelWidget::~MultiLevelROIPanelWidget( ) 
{
}

void MultiLevelROIPanelWidget::OnInit()
{
	m_selectedLevel = -1;
	EnableGUI(false);

	GetProcessorOutputObserver( MultiLevelROIProcessor::MEASUREMENTS )->SetHideInput( false );
	GetProcessorOutputObserver( MultiLevelROIProcessor::SIGNAL )->SetHideInput( false );
	GetProcessorOutputObserver( MultiLevelROIProcessor::SIGNAL_HISTOGRAM )->SetHideInput( false );
}

void MultiLevelROIPanelWidget::EnableGUI(bool enable)
{
	m_comboBoxLevels->Enable(enable);
	label_4->Enable(enable);
	m_measureMean->Enable(enable);
	label_7->Enable(enable);
	m_measureSTD->Enable(enable);
	label_5->Enable(enable);
	m_measureMin->Enable(enable);
	label_6->Enable(enable);
	label_2->Enable(enable);
	m_measureMax->Enable(enable);
	m_rb3d->Enable(enable);
	m_rbX->Enable(enable);
	m_rbY->Enable(enable);
	m_rbZ->Enable(enable);
	button_1->Enable(enable);
	m_comboBoxLevels->Enable(enable);

	label_4_copy->Enable( enable );
	m_measureCount->Enable( enable );
	label_6_copy->Enable( enable );
	m_measureVolume->Enable( enable );
}

Core::BaseProcessor::Pointer MultiLevelROIPanelWidget::GetProcessor()
{
	return Core::BaseProcessor::Pointer(m_processor);
}


void MultiLevelROIPanelWidget::OnSelectLevel(wxCommandEvent &event)
{
	blTagMap::Pointer levels = GetLevels();
	// Position 0 is "None"
	if ( levels.IsNotNull() && event.GetInt() != 0 )
	{
		wxString selection = m_comboBoxLevels->GetString( event.GetInt() );
		m_selectedLevel = levels->GetTagValue<int>( selection.ToStdString() );
	}
	else
	{
		m_selectedLevel = -1;
	}
}

void MultiLevelROIPanelWidget::OnModifiedInput( int num )
{
	Core::DataEntity::Pointer dataEntity;
	dataEntity = m_processor->GetInputDataEntity(MultiLevelROIProcessor::INPUT_IMAGE);

	Core::DataEntity::Pointer dataEntityROI;
	dataEntityROI = m_processor->GetInputDataEntity(MultiLevelROIProcessor::INPUT_ROI);

	if (dataEntity.IsNull()) 
	{
		EnableGUI(false);
		return;
	}

	EnableGUI(true);

	//if ( dataEntityROI.IsNotNull( ) && dataEntityROI->GetNumberOfTimeSteps() < 2 )
	//{
	//	m_btnPropagate->Enable(false);
	//	label_3->Enable(false);
	//	m_spinCtrlLeft->Enable(false);
	//	label_8->Enable(false);
	//	m_spinCtrlRight->Enable(false);
	//}

	m_comboBoxLevels->Clear();
	m_comboBoxLevels->Append( "None" );

	// If levels is null, compute it and add it to the mask image
	MultiLevelROIProcessor::ComputeLevels( dataEntityROI );

	blTagMap::Pointer levels = GetLevels();
	if ( levels.IsNotNull() )
	{
		blTagMap::Iterator it;
		for ( it = levels->GetIteratorBegin(); it!= levels->GetIteratorEnd(); ++it )
		{
			m_comboBoxLevels->Append(it->second->GetName());
		}
	}

	// If there is at least one level
	if ( m_comboBoxLevels->GetCount() > 1 )
	{
		// Select the first one
		m_comboBoxLevels->SetSelection(1);
		wxString selection = m_comboBoxLevels->GetStringSelection( );
		m_selectedLevel = levels->GetTagValue<int>( selection.ToStdString() );
	}
	else
	{
		// Select the "None"
		m_comboBoxLevels->SetSelection(0);
		m_selectedLevel = -1;
	}
}

void MultiLevelROIPanelWidget::OnUpdateMeasuresBtn(wxCommandEvent& event)
{
	// Set state to processing (dialog box)
	Core::Runtime::Kernel::GetApplicationRuntime( )->SetAppState( 
		Core::Runtime::APP_STATE_PROCESSING );

	try
	{
		CalculateMeasures();
		UpdateMeasures();
	}
	coreCatchExceptionsReportAndNoThrowMacro( MultiLevelROIPanelWidget::OnUpdateMeasuresBtn )

	Core::Runtime::Kernel::GetApplicationRuntime( )->SetAppState( 
		Core::Runtime::APP_STATE_IDLE );
}

void MultiLevelROIPanelWidget::UpdateMeasures()
{
	blTagMap::Pointer measures = m_processor->GetIntensityMeasures();

	m_measureCount->SetValue( wxString::Format( "%d", measures->GetTagValue<long int>("count") ) );
	m_measureMean->SetValue( wxString::Format( "%.3f", measures->GetTagValue<double>("mean") ) );
	m_measureSTD->SetValue( wxString::Format( "%.3f", measures->GetTagValue<double>("std") ) );
	m_measureMin->SetValue( wxString::Format( "%.3f", measures->GetTagValue<double>("min") ) );
	m_measureMax->SetValue( wxString::Format( "%.3f", measures->GetTagValue<double>("max") ) );
	m_measureVolume->SetValue( wxString::Format( "%.3f", measures->GetTagValue<double>("volume") ) );
}

void MultiLevelROIPanelWidget::CalculateMeasures( )
{
	m_processor->SetSelectedLevel(m_selectedLevel);
	int domain = m_rb3d->GetValue()*1 + m_rbX->GetValue()*2 + m_rbY->GetValue()*3 + m_rbZ->GetValue()*4;

	mitk::wxMitkSelectableGLWidget* widget = NULL;
	Core::Widgets::MultiRenderWindowMITK* mitkRenderWindow;
	mitkRenderWindow = dynamic_cast<Core::Widgets::MultiRenderWindowMITK*> ( GetMultiRenderWindow() );
	if ( mitkRenderWindow )
	{
		switch (domain){
		case 1:break;
		case 2:widget = mitkRenderWindow->GetX();break;
		case 3:widget = mitkRenderWindow->GetY();break;
		case 4:widget = mitkRenderWindow->GetZ();break;
		default:break;
		}
	}

	int pos = -1;
	if ( widget )
	{
		pos = widget->GetSliceNavigationController()->GetSlice()->GetPos();
	}

	m_processor->SetDomain(domain);
	m_processor->SetPos(pos);
	m_processor->SetExportHistogram( m_chkExportHistogram->GetValue( ) );
	m_processor->SetNormalizeHistogramByVolume( m_chkNormalizeVolume->GetValue() );
	m_processor->SetExportSignal( m_chkExportSignal->GetValue() );
	m_processor->SetTimeStep( GetTimeStep( ) );
	m_processor->SetMeanAllLevels( m_rbtnAllLabels->GetValue( ) );
	m_processor->SetStatisticValue( m_cmbStatisticValue->GetStringSelection( ).ToStdString() );
	m_processor->Update( );
}

blTagMap::Pointer Core::Widgets::MultiLevelROIPanelWidget::GetLevels()
{
	Core::DataEntity::Pointer dataEntity;
	dataEntity = m_processor->GetInputDataEntityHolder(MultiLevelROIProcessor::INPUT_ROI)->GetSubject();
	if ( dataEntity.IsNull() )
	{
		return NULL;
	}

	blTagMap::Pointer levels = dataEntity->GetMetadata()->GetTagValue<blTagMap::Pointer>( "MultiROILevel" );
	return levels;
}

void MultiLevelROIPanelWidget::UpdateWidget()
{
	m_rbtnSingleLabel->Enable( m_chkExportSignal->GetValue( ) );
	m_rbtnAllLabels->Enable( m_chkExportSignal->GetValue( ) );
	m_comboBoxLevels->Enable( m_chkExportSignal->GetValue( ) && m_rbtnSingleLabel->GetValue( ) );
	m_cmbStatisticValue->Enable( m_chkExportSignal->GetValue( ) && m_rbtnAllLabels->GetValue( ) );		
}

void MultiLevelROIPanelWidget::OnExportSignal(wxCommandEvent &event)
{
	UpdateWidget( );
}

void MultiLevelROIPanelWidget::OnCurrentLabel(wxCommandEvent &event)
{
	UpdateWidget( );
}

void MultiLevelROIPanelWidget::OnAllLabels(wxCommandEvent &event)
{
	UpdateWidget( );
}

