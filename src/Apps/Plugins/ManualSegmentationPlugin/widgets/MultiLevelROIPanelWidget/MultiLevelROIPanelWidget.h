/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _MultiLevelROIPanelWidget_H
#define _MultiLevelROIPanelWidget_H

#include "MultiLevelROIPanelWidgetUI.h"
#include "mlrProcessor.h"

// CoreLib
#include "coreRenderingTree.h"
#include "coreFrontEndPlugin.h"
#include "coreCommonDataTypes.h"
#include "coreBaseWindow.h"

namespace Core{ namespace Widgets {
	class AcquireDataEntityInputControl;
	class UserHelper;
	class DataEntityListBrowser;
}}

namespace Core{
	namespace Widgets {


#define wxID_MultiLevelROIWidget wxID("wxID_MultiLevelROIWidget")

/**
\ingroup ManualSegmentationPlugin
\author Albert Sanchez
\date 06 05 2010
*/
class MultiLevelROIPanelWidget : 
	public MultiLevelROIPanelWidgetUI,
	public Core::Widgets::ProcessingWidget
{

	// OPERATIONS
public:
	coreDefineBaseWindowFactory( Core::Widgets::MultiLevelROIPanelWidget )
	//!
	MultiLevelROIPanelWidget(
		wxWindow* parent, 
		int id = wxID_MultiLevelROIWidget,
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=0);

	//!
	~MultiLevelROIPanelWidget( );

	//!
	Core::BaseProcessor::Pointer GetProcessor();

	//!
	void OnInit();
	
private:
    wxDECLARE_EVENT_TABLE();

	//!
	void OnUpdateMeasuresBtn(wxCommandEvent& event);

	//!
	void UpdateMeasures();

	//!
	void CalculateMeasures( );

	//!
	void OnModifiedInput( int num );

	//!
	void OnSelectLevel(wxCommandEvent &event);

	//!
	void EnableGUI(bool enable);

	//!
	blTagMap::Pointer GetLevels( );

	//!
	void OnExportSignal(wxCommandEvent &event);

	//!
    void OnCurrentLabel(wxCommandEvent &event);

	//!
    void OnAllLabels(wxCommandEvent &event);

	void UpdateWidget();

	// ATTRIBUTES
private:

	//! Processor
	MultiLevelROIProcessor::Pointer m_processor;

	int m_selectedLevel;

};
} //namespace Widgets
} //namespace Core

#endif //_MultiLevelROIPanelWidget_H
