/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef msImageContourProcessor_H
#define msImageContourProcessor_H

#include "coreBaseProcessor.h"
#include "vtkDijkstraImageGeodesicPath.h"

/**
\brief Compute contour from input 2D image based on a set of control points

\ingroup ManualSegmentationPlugin
\author Xavi Planes
\date  Sept 2012
*/

class ImageContourProcessor : public Core::BaseProcessor
{
public:
	enum CONTOUR_METHOD_TYPE
	{
		METHOD_POLYGON,
		METHOD_SPLINE,
		METHOD_DIJKSTRA
	};

	//!
	coreProcessor(ImageContourProcessor, Core::BaseProcessor);

	//!
	void Update();

	//!
	void SetContourMethod( CONTOUR_METHOD_TYPE method );
	CONTOUR_METHOD_TYPE GetContourMethod( );

private:
	//!
	ImageContourProcessor( );

	//!
	~ImageContourProcessor( );

	//! Purposely not implemented
	ImageContourProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );

	//!
	void ComputePolygonMethod( );

	//!
	void ComputeSplineMethod( );

	//!
	void ComputeDijkstraMethod( );

	//!
	void ComputeCostImage( );
private:
	//!
	CONTOUR_METHOD_TYPE m_ContourMethod;
	//!
	Core::vtkImageDataPtr m_CostImage;
	//!
	vtkSmartPointer<vtkDijkstraImageGeodesicPath> m_Dijkstra;
};

#endif //msImageContourProcessor_H

