/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "isegProcessor.h"

#include "vtkMarchingCubes.h"
#include "vtkCleanPolyData.h"
#include "vtkPolyDataNormals.h"

#include "coreVTKPolyDataHolder.h"


InteractiveSegmentationProcessor::InteractiveSegmentationProcessor() 
{
	SetNumberOfInputs( NUMBER_OF_INPUTS );

	GetInputPort( INPUT_ROI )->SetName( "ROI image" );
	GetInputPort( INPUT_ROI )->SetDataEntityType( Core::ImageTypeId | Core::ROITypeId );
	GetInputPort( INPUT_ROI )->SetNotValidDataEntityType( Core::ImageTypeId );

	GetInputPort( REFERENCE_IMAGE )->SetName( "Reference image" );
	GetInputPort( REFERENCE_IMAGE )->SetDataEntityType( Core::ImageTypeId );

	SetNumberOfOutputs( 1 );
}


InteractiveSegmentationProcessor::~InteractiveSegmentationProcessor() 
{

}

void InteractiveSegmentationProcessor::Update( )
{
	Core::vtkImageDataPtr maskImage;
	GetProcessingData( INPUT_ROI, maskImage );

	//The filter runs faster if gradient and normal calculations are turned off.
	vtkSmartPointer<vtkMarchingCubes> mcubes = vtkMarchingCubes::New();
	mcubes->SetInput( maskImage );
	mcubes->ComputeScalarsOff();
	mcubes->ComputeGradientsOff();
	mcubes->ComputeNormalsOff();
	mcubes->SetValue( 0, 0.5 );
	mcubes->Update();

	vtkSmartPointer<vtkCleanPolyData> cleanMesh = vtkSmartPointer<vtkCleanPolyData>::New();
	cleanMesh->SetInput( mcubes->GetOutput( ) );
	cleanMesh->ConvertPolysToLinesOff();
	cleanMesh->SetTolerance (1.0e-10);
	cleanMesh->SetAbsoluteTolerance (1.0e-10);
	cleanMesh->Update();

	vtkSmartPointer<vtkPolyDataNormals> normals = vtkPolyDataNormals::New();
	normals->SetInput( cleanMesh->GetOutput() );
	normals->SplittingOff();	//to avoid duplicate vertices due to sharp edges
	normals->Update();

	UpdateOutput( 0, normals->GetOutput( ), "Surface", false, 1, GetInputDataEntity( INPUT_ROI ) );
}
