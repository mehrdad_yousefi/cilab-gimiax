/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "msPolygonTool.h"
#include "msReduceContourProcessor.h"

#include "mitkToolManager.h"
#include "mitkOverwriteSliceImageFilter.h"
#include "mitkOverwriteDirectedPlaneImageFilter.h"

#include "mitkBaseRenderer.h"
#include "mitkRenderingManager.h"
//#include "mitkProperties.h"
#include "mitkPlanarCircle.h"
#include "mitkProperties.h"
#include "ipSegmentation.h"
#include "mitkIpPicTypeMultiplex.h"
#include "mitkNodePredicateData.h"

#include "blMITKUtils.h"

#include "vtkLine.h"

#include "resource/mitkPolygonTool.xpm"

namespace mitk {
	MITK_TOOL_MACRO(PLUGIN_EXPORT, PolygonTool, "Polygon tool");
}



mitk::PolygonTool::PolygonTool(int paintingPixelValue)
	:mitk::FeedbackContourTool("PressMoveReleaseWithShift"),
	m_PaintingPixelValue(paintingPixelValue),
	m_pixelValue(1),
	m_LastPositionEvent(0,0,0,0,0,Point2D(),Point3D())
{
	m_PointSet = mitk::PointSet::New();
	m_PointSetNode = DataTreeNode::New();
	m_PointSetNode->SetData( m_PointSet );
	m_PointSetNode->SetProperty("name", StringProperty::New("One of PointSet nodes"));
	m_PointSetNode->SetProperty("visible", BoolProperty::New(true));
	m_PointSetNode->SetProperty("helper object", BoolProperty::New(true));
	m_PointSetNode->SetProperty("layer", IntProperty::New(1000));
	m_PointSetNode->SetProperty("project", BoolProperty::New(true));
	//m_PointSetNode->SetProperty("show contour", BoolProperty::New(true));
	//m_PointSetNode->SetProperty("contourcolor", ColorProperty::New(0.0/255.0, 255.0/255.0, 0.0/255.0));

	m_PointSetInteractor = mitk::PointSetInteractor::New( "pointsetinteractor", m_PointSetNode );

	m_PointSetVisible = false;
	m_ToolManager = NULL;
	m_Precision = 0;
	m_Mode = MODE_ADD_POINTS;
	m_SliceImageChanged = true;
	m_UpdateContour = false;

	// Configure contour filter
	m_ContourProcessor = ImageContourProcessor::New( );
	Core::DataEntity::Pointer input = Core::DataEntity::New( Core::PointSetTypeId );
	input->AddTimeStep( m_PointSet );
	m_ContourProcessor->SetInputDataEntity( 1, input );


	CONNECT_ACTION( 80, OnAddPoint );
	CONNECT_ACTION( 1235, OnMousePressed );
	CONNECT_ACTION( 42, OnMouseReleased );
	CONNECT_ACTION( 49012, OnMoveSlice );
	CONNECT_ACTION( 49013, OnMoveSlice );
	CONNECT_ACTION( 1234, OnChangeMode );

	//! Call back called when the user adds a point to the point set with the mouse.
	typedef itk::SimpleMemberCommand<PolygonTool> MemberCommandType;

	typedef MemberCommandType::Pointer MemberCommandPointerType;
	MemberCommandPointerType dataChangedCommand;
	dataChangedCommand = MemberCommandType::New();
	dataChangedCommand->SetCallbackFunction( this, &Self::OnPointSetAddEvent );
	m_PointSet->AddObserver( mitk::PointSetAddEvent(), dataChangedCommand);

	dataChangedCommand = MemberCommandType::New();
	dataChangedCommand->SetCallbackFunction( this, &Self::OnPointSetRemoveEvent );
	m_PointSet->AddObserver( mitk::PointSetRemoveEvent(), dataChangedCommand);

	dataChangedCommand = MemberCommandType::New();
	dataChangedCommand->SetCallbackFunction( this, &Self::OnPointSetMoveEvent );
	m_PointSet->AddObserver( mitk::PointSetMoveEvent(), dataChangedCommand);


}

mitk::PolygonTool::~PolygonTool()
{
}

const char** mitk::PolygonTool::GetXPM() const
{
	return mitkPolygonTool_xpm;
}

const char* mitk::PolygonTool::GetName() const
{
	return "Polygon";
}

void mitk::PolygonTool::SetToolManager(ToolManager* manager)
{
	if ( m_ToolManager )
	{
		m_ToolManager->ReferenceDataChanged.RemoveListener( 
			mitk::MessageDelegate<PolygonTool>( this, &PolygonTool::OnReferenceDataChanged ) );
	}

	mitk::FeedbackContourTool::SetToolManager( manager );

	if ( m_ToolManager )
	{
		m_ToolManager->ReferenceDataChanged.AddListener( 
			mitk::MessageDelegate<PolygonTool>( this, &PolygonTool::OnReferenceDataChanged ) );
	}
}

void mitk::PolygonTool::Activated()
{
	Superclass::Activated();

	// Show all
	SetPointSetVisible( true );
	mitk::GlobalInteraction::GetInstance()->AddInteractor( m_PointSetInteractor );
	mitk::FeedbackContourTool::SetFeedbackContourVisible(true);
	SetMode( MODE_ADD_POINTS );
	m_SliceImageChanged = true;
	m_UpdateContour = true;
}

void mitk::PolygonTool::Deactivated()
{
	Superclass::Deactivated();

	// Hide all
	SetPointSetVisible( false );
	mitk::GlobalInteraction::GetInstance()->RemoveInteractor( m_PointSetInteractor );
	mitk::FeedbackContourTool::SetFeedbackContourVisible(false);
	m_UpdateContour = true;
}

void mitk::PolygonTool::UpdateContour( )
{
	StateEvent tmpEvent( 0, &m_LastPositionEvent );
	if ( m_SliceImageChanged && 
		m_ContourProcessor->GetContourMethod( ) == ImageContourProcessor::METHOD_DIJKSTRA &&
		SegTool2D::OnMouseMoved( NULL, &tmpEvent ) )
	{
		// Get 2D Slice VTK image and set origin
		mitk::Image::Pointer slice = mitk::FeedbackContourTool::GetAffectedReferenceSlice( &m_LastPositionEvent );

		// Set image
		Core::DataEntity::Pointer input = Core::DataEntity::New( Core::ImageTypeId );
		input->AddTimeStep( slice );
		m_ContourProcessor->SetInputDataEntity( 0, input );

		m_SliceImageChanged = false;
	}

	// Set pointset
	Core::DataEntity::Pointer input2 = Core::DataEntity::New( Core::PointSetTypeId );
	input2->AddTimeStep( m_PointSet );
	m_ContourProcessor->SetInputDataEntity( 1, input2 );

	// Update time step for pointset
	m_ContourProcessor->SetTimeStep( GetTimeStep( ) );

	// Update contour
	m_ContourProcessor->Update( );

	// Get final contour
	Core::DataEntity::Pointer output = m_ContourProcessor->GetOutputDataEntity( 0 );
	if ( output.IsNull( ) )
	{
		return;
	}

	mitk::Contour::Pointer finalContour;
	output->GetProcessingData( finalContour, 0, true );
	if ( !finalContour )
	{
		return;
	}

	// Update closed/open
	switch ( m_Mode )
	{
	case MODE_ADD_POINTS:
		if ( finalContour ) finalContour->SetClosed( false );
		break;
	case MODE_MOVE_POINTS:
		if ( finalContour ) finalContour->SetClosed( true );
		break;
	}

	// Set feedback contour
	FeedbackContourTool::SetFeedbackContour( *finalContour );

	m_UpdateContour = false;
}

void mitk::PolygonTool::UpdatePointSet( )
{
	if ( m_PointSet->GetPointSet( GetTimeStep( ) ).IsNull( ) )
	{
		m_PointSet->Expand(GetTimeStep()+1);
	}

	// Clear
	m_PointSet->GetPointSet( GetTimeStep( ) )->GetPoints( )->clear( );
	m_PointSet->GetPointSet( GetTimeStep( ) )->GetPointData( )->clear( );

	mitk::Contour* contour = FeedbackContourTool::GetFeedbackContour();
	mitk::Contour::PointsContainerPointer points = contour->GetPoints( );
	mitk::Contour::PointsContainer::Iterator it, end;
	mitk::PointSet::PointIdentifier pointId = 0;
	for ( it = points->Begin( ) ; it != points->End( ) ; it++ )
	{
		mitk::PointSet::PointType out;
		if ( points->GetElementIfIndexExists( it->Index( ), &out ) )
		{
			m_PointSet->InsertPoint( it->Index( ), out, GetTimeStep( ) );
		}
	}
}

void mitk::PolygonTool::OnPointSetAddEvent( )
{
	switch( m_Mode )
	{
	case MODE_ADD_POINTS: 
		break;
	case MODE_MOVE_POINTS: 
		{
			// Extract selected point
			mitk::Point3D selectedPoint;

			mitk::PointSet::PointsContainer *points;
			points = m_PointSet->GetPointSet( GetTimeStep( ) )->GetPoints( );
			mitk::PointSet::PointsContainer::Iterator	it, end = points->End();
			for( it = points->Begin(); it != end; it++ )
			{
				mitk::PointSet::PointType point;
				if ( m_PointSet->GetSelectInfo( it->Index(), GetTimeStep( ) ) )
				{
					selectedPoint = it->Value( );
					points->DeleteIndex( it->Index() );
					break;
				}
			}

			// Get a copy of all points
			std::vector<mitk::PointSet::PointType> pointList;
			for( it = points->Begin(); it != end; it++ )
			{
				mitk::PointSet::PointType point;
				if ( points->IndexExists(it->Index()) )
				{
					pointList.push_back( it->Value() );
				}
			}


			// Find closest point
			float bestDist = FLT_MAX;
			std::vector<mitk::PointSet::PointType>::iterator itPoint;
			mitk::Point3D closestPoint1;
			mitk::Point3D closestPoint2;
			long closestPointPos = -1;
			double selPoint[ 3 ] = { selectedPoint[0], selectedPoint[1], selectedPoint[2] };
			for( size_t i = 0 ; i < pointList.size( ); i++ )
			{
				mitk::Point3D point1 = pointList[ i ];
				mitk::Point3D point2 = pointList[ ( i + 1 ) % pointList.size( ) ];
				double p1[ 3 ] = { point1[0], point1[1], point1[2] };
				double p2[ 3 ] = { point2[0], point2[1], point2[2] };
				double t;
				double closestPoint[3];
				double distance = vtkLine::DistanceToLine( selPoint, p1, p2, t, closestPoint );
				if ( distance < bestDist )
				{
					closestPoint1 = point1;
					closestPoint2 = point2;
					bestDist = distance;
					closestPointPos = i;
				}
			}

			// Only one point
			if ( closestPointPos == -1 )
			{
				m_PointSet->InsertPoint( 0, selectedPoint, GetTimeStep( ) );
				m_PointSet->SetSelectInfo( 0, true, GetTimeStep( ) );
			}
			// Insert selected point in between
			else 
			{
				m_PointSet->GetPointSet( GetTimeStep( ) )->GetPoints( )->clear( );
				m_PointSet->GetPointSet( GetTimeStep( ) )->GetPointData( )->clear( );

				std::vector<mitk::PointSet::PointType>::iterator it;
				mitk::PointSet::PointIdentifier pointId = 0;
				for ( it = pointList.begin( ) ; it != pointList.end( ) ; it++ )
				{
					if ( pointId == closestPointPos )
					{
						if ( closestPoint1 == *it )
						{
							m_PointSet->InsertPoint( pointId++, *it, GetTimeStep( ) );
							m_PointSet->InsertPoint( pointId++, selectedPoint, GetTimeStep( ) );
							m_PointSet->SetSelectInfo( pointId - 1, true, GetTimeStep( ) );
						}
						else
						{
							m_PointSet->InsertPoint( pointId++, selectedPoint, GetTimeStep( ) );
							m_PointSet->SetSelectInfo( pointId - 1, true, GetTimeStep( ) );
							m_PointSet->InsertPoint( pointId++, *it, GetTimeStep( ) );
						}

					}
					else
					{
						m_PointSet->InsertPoint( pointId++, *it, GetTimeStep( ) );
					}
				}
			}
			break;
		}
	}


	UpdateContour( );

	if ( m_LastPositionEvent.GetSender() )
	{
		mitk::RenderingManager::GetInstance()->RequestUpdate( m_LastPositionEvent.GetSender()->GetRenderWindow() );
	}

}

void mitk::PolygonTool::OnPointSetMoveEvent( )
{
	UpdateContour( );

	if ( m_LastPositionEvent.GetSender() )
	{
		mitk::RenderingManager::GetInstance()->RequestUpdate( m_LastPositionEvent.GetSender()->GetRenderWindow() );
	}
}

void mitk::PolygonTool::OnPointSetRemoveEvent( )
{

	// Order the points from 0 to N. Otherwise, when adding a new point will be
	// added at the last removed position
	// Extract selected point
	mitk::PointSet::PointsContainer *points;
	points = m_PointSet->GetPointSet( GetTimeStep( ) )->GetPoints( );
	mitk::PointSet::PointsContainer::Iterator	it, end = points->End();

	// Get a copy of all points and compute distance
	std::vector<mitk::PointSet::PointType> pointList;
	for( it = points->Begin(); it != end; it++ )
	{
		mitk::PointSet::PointType point;
		if ( points->IndexExists(it->Index()) )
		{
			pointList.push_back( it->Value() );
		}
	}

	// Insert all points
	m_PointSet->GetPointSet( GetTimeStep( ) )->GetPoints( )->clear( );
	m_PointSet->GetPointSet( GetTimeStep( ) )->GetPointData( )->clear( );
	std::vector<mitk::PointSet::PointType>::iterator itPoint;
	mitk::PointSet::PointIdentifier pointId = 0;
	for ( itPoint = pointList.begin( ) ; itPoint != pointList.end( ) ; itPoint++ )
	{
		m_PointSet->InsertPoint( pointId++, *itPoint, GetTimeStep( ) );
	}


	UpdateContour( );

	if ( m_LastPositionEvent.GetSender() )
	{
		mitk::RenderingManager::GetInstance()->RequestUpdate( m_LastPositionEvent.GetSender()->GetRenderWindow() );
	}
}

void mitk::PolygonTool::SetMode( MODE_TYPE mode )
{
	m_Mode = mode;

	mitk::Contour* feedbackContour( mitk::FeedbackContourTool::GetFeedbackContour() );
	mitk::DataTreeNode::Pointer node;
	if ( m_ToolManager )
	{
		node = m_ToolManager->GetDataStorage()->GetNode( mitk::NodePredicateData::New( feedbackContour ) );
	}

	switch ( m_Mode )
	{
	case MODE_ADD_POINTS:
		if ( feedbackContour ) feedbackContour->SetClosed( false );
		if ( node.IsNotNull( ) ) node->SetFloatProperty( "Width", 2 );
		break;
	case MODE_MOVE_POINTS:
		if ( feedbackContour ) feedbackContour->SetClosed( true );
		if ( node.IsNotNull( ) ) node->SetFloatProperty( "Width", 1 );
		break;
	}
}

bool mitk::PolygonTool::OnChangeMode(mitk::Action* action, const mitk::StateEvent* stateEvent)
{

	switch ( m_Mode )
	{
	case MODE_ADD_POINTS:SetMode( MODE_MOVE_POINTS );break;
	case MODE_MOVE_POINTS:SetMode( MODE_ADD_POINTS );break;
	}

	const mitk::KeyEvent* keyEvent = dynamic_cast<const mitk::KeyEvent*>(stateEvent->GetEvent());
	if ( keyEvent && keyEvent->GetSender( ) )
	{
		mitk::RenderingManager::GetInstance()->RequestUpdate( keyEvent->GetSender()->GetRenderWindow() );
	}

	return true;
}

template<typename PicType>
long ComputeOffset( const mitkIpPicDescriptor *seg )
{
	long ofs = 0;
	int maxOfs = seg->n[0] * seg->n[1];
	while ( ofs < maxOfs && 
		( *(((PicType*)seg->data) + (ofs)) == 0 ) )
	{
		ofs++;
	}
	return ofs;
}

bool mitk::PolygonTool::OnAddPoint(mitk::Action* action, const mitk::StateEvent* stateEvent)
{
	const PositionEvent* positionEvent = dynamic_cast<const PositionEvent*>(stateEvent->GetEvent());
	if (!positionEvent) return false;
	m_LastPositionEvent = mitk::PositionEvent( *positionEvent );

	// If last sender or slice has changed -> Slice image has changed
	if ( !SegTool2D::OnMouseMoved( action, stateEvent ) )
	{
		m_SliceImageChanged = true;
	}

	// Store current slice and sender
	mitk::FeedbackContourTool::OnMousePressed( action, stateEvent );

	return true;
}

bool mitk::PolygonTool::OnMousePressed (mitk::Action* action, const mitk::StateEvent* stateEvent)
{
	const PositionEvent* positionEvent = dynamic_cast<const PositionEvent*>(stateEvent->GetEvent());
	if (!positionEvent) return false;

	m_LastPositionEvent = mitk::PositionEvent( *positionEvent );

	// If last sender or slice has changed -> Slice image has changed
	if ( !SegTool2D::OnMouseMoved( action, stateEvent ) )
	{
		m_SliceImageChanged = true;
	}

	// Store current slice and sender
	mitk::FeedbackContourTool::OnMousePressed( action, stateEvent );

	//// Update pointset interactor precision based on display zoom
	//mitk::Vector2D displayPoint;
	//displayPoint[ 0 ] = 10;
	//displayPoint[ 1 ] = 10;
	//mitk::Vector2D worldPoint;
	//positionEvent->GetSender()->GetDisplayGeometry( )->DisplayToWorld( displayPoint, worldPoint );
	//m_Precision = worldPoint[ 0 ] * 2;
	//// Set precision in mm
	//m_PointSetInteractor->SetPrecision( m_Precision );

	// If current pointset has a set of points -> Update it
	if ( m_PointSet->GetPointSet( GetTimeStep( ) ) && 
		 m_PointSet->GetPointSet( GetTimeStep( ) )->GetNumberOfPoints( ) )
	{
		// If contour needs to be updated -> Update it
		if ( m_UpdateContour )
		{
			UpdateContour( );
			// Referesh window
			assert( positionEvent->GetSender()->GetRenderWindow() );
			mitk::RenderingManager::GetInstance()->RequestUpdate( positionEvent->GetSender()->GetRenderWindow() );
		}

		return false;
	}
	else
	{
		// Detect contour con current slice
		mitk::Image::Pointer slice = mitk::FeedbackContourTool::GetAffectedWorkingSlice( positionEvent );
		if ( slice.IsNull() )
		{
			LOG_ERROR << "Unable to extract slice." << std::endl;
			return false;
		}    

		if ( DetectContourOnSliceImage( slice ) )
		{
			assert( positionEvent->GetSender()->GetRenderWindow() );
			mitk::RenderingManager::GetInstance()->RequestUpdate( positionEvent->GetSender()->GetRenderWindow() );
		}
	}

	return true;
}

bool mitk::PolygonTool::DetectContourOnSliceImage( mitk::Image::Pointer slice )
{
	mitkIpPicDescriptor* workingPicSlice = slice->GetSliceData()->GetPicDescriptor();

	// Compute initial offset of mask region
	long oneContourOffset( 0 );
	mitkIpPicTypeMultiplexR0( ComputeOffset, workingPicSlice, oneContourOffset );
	if ( oneContourOffset == workingPicSlice->n[ 0 ] * workingPicSlice->n[ 1 ] )
	{
		return false;
	}

	// Get contour points
	int numberOfContourPoints( 0 );
	int newBufferSize( 0 );
	float* contourPoints = ipMITKSegmentationGetContour8N( 
		workingPicSlice, oneContourOffset, numberOfContourPoints, newBufferSize ); // memory allocated with malloc
	if ( !contourPoints )
	{
		return false;
	}

	// Get points
	mitk::Contour::Pointer contourInImageIndexCoordinates = Contour::New();
	contourInImageIndexCoordinates->Initialize();
	mitk::Point3D point;
	for(int i=0; i<numberOfContourPoints; i++)
	{
		point[ 0 ] = contourPoints[ 2 * i + 0 ];
		point[ 1 ] = contourPoints[ 2 * i + 1 ];
		point[ 2 ] = 0;
		contourInImageIndexCoordinates->AddVertex( point + 0.5 );
	}

	free(contourPoints);

	// Project back
	Contour::Pointer contourInWorldCoordinates;
	contourInWorldCoordinates = FeedbackContourTool::BackProjectContourFrom2DSlice( 
		slice, contourInImageIndexCoordinates, false );   // true: sub 0.5 for ipSegmentation correctio

	// Reduce contour points
	ReduceContourProcessor::Pointer reducer = ReduceContourProcessor::New( );
	Core::DataEntity::Pointer input = Core::DataEntity::New( );
	input->SetTimeStep( contourInWorldCoordinates );
	reducer->SetInputDataEntity( 0, input );
	reducer->Update( );
	Core::DataEntity::Pointer output = reducer->GetOutputDataEntity( 0 );

	// Update feedback contour pointer
	Contour::Pointer reducedContour;
	output->GetProcessingData( reducedContour, 0, true );
	FeedbackContourTool::SetFeedbackContour( *reducedContour );

	UpdatePointSet( );

	SetMode( MODE_MOVE_POINTS );

	return true;
}

bool mitk::PolygonTool::OnMouseReleased(mitk::Action* action, const mitk::StateEvent* stateEvent)
{
	// Create a temporal state event because the event origin is a key
	StateEvent tmpEvent( stateEvent->GetId( ), &m_LastPositionEvent );
	stateEvent = &tmpEvent;

	// 1. Hide the feedback contour, find out which slice the user clicked, find out which slice of the toolmanager's working image corresponds to that
	const mitk::PositionEvent* positionEvent = dynamic_cast<const mitk::PositionEvent*>(stateEvent->GetEvent());
	if (!positionEvent) return false;
	if (!positionEvent->GetSender()) return false;
	if (!positionEvent->GetSender()->GetRenderWindow()) return false;

	mitk::RenderingManager::GetInstance()->RequestUpdate( positionEvent->GetSender()->GetRenderWindow() );

	if (!mitk::FeedbackContourTool::OnMouseReleased( action, stateEvent )) return false;

	mitk::DataTreeNode* workingNode( m_ToolManager->GetWorkingData(0) );
	if (!workingNode) return false;

	mitk::Image* image = dynamic_cast<mitk::Image*>(workingNode->GetData());
	const mitk::PlaneGeometry* planeGeometry( dynamic_cast<const mitk::PlaneGeometry*> (positionEvent->GetSender()->GetCurrentWorldGeometry2D() ) );
	if ( !image || !planeGeometry ) return false;

	int affectedDimension( -1 );
	int affectedSlice( -1 );
	mitk::SegTool2D::DetermineAffectedImageSlice( image, planeGeometry, affectedDimension, affectedSlice );

	// 2. Slice is known, now we try to get it as a 2D image and project the contour into index coordinates of this slice
	mitk::Image::Pointer slice = mitk::SegTool2D::GetAffectedImageSliceAs2DImage( positionEvent, image );

	size_t size = slice->GetDimensions( )[0]*slice->GetDimensions( )[1]*slice->GetDimensions( )[2];
	memset( (int*)slice->GetData(), 0, size );

	if ( slice.IsNull() )
	{
		LOG_ERROR << "Unable to extract slice." << std::endl;
		return false;
	}    

	mitk::Contour* feedbackContour( mitk::FeedbackContourTool::GetFeedbackContour() );
	mitk::Contour::Pointer projectedContour = FeedbackContourTool::ProjectContourTo2DSlice( slice, feedbackContour, true, false ); // true: actually no idea why this is neccessary, but it works :-(

	if (projectedContour.IsNull()) return false;

	mitk::FeedbackContourTool::FillContourInSlice( projectedContour, slice, m_PaintingPixelValue );

	// 3. Write the modified 2D working data slice back into the image
	if (affectedDimension != -1) {
		mitk::OverwriteSliceImageFilter::Pointer slicewriter = mitk::OverwriteSliceImageFilter::New();
		slicewriter->SetInput( image );
		slicewriter->SetCreateUndoInformation( true );
		slicewriter->SetSliceImage( slice );
		slicewriter->SetSliceDimension( affectedDimension );
		slicewriter->SetSliceIndex( affectedSlice );
		slicewriter->SetPixelValue(m_pixelValue);
		slicewriter->SetTimeStep( positionEvent->GetSender()->GetTimeStep( image ) );
		slicewriter->Update();
		if ( m_RememberContourPositions && m_PaintingPixelValue == 1)
		{
			this->AddContourmarker(positionEvent);
		}
	}
	else {
		mitk::OverwriteDirectedPlaneImageFilter::Pointer slicewriter = mitk::OverwriteDirectedPlaneImageFilter::New();
		slicewriter->SetInput( image );
		slicewriter->SetCreateUndoInformation( false );
		slicewriter->SetSliceImage( slice );
		slicewriter->SetPlaneGeometry3D( slice->GetGeometry() );
		slicewriter->SetTimeStep( positionEvent->GetSender()->GetTimeStep( image ) );
		slicewriter->SetPixelValue(m_pixelValue);
		slicewriter->Update();

		if ( m_RememberContourPositions && m_PaintingPixelValue == 1)
		{
			this->AddContourmarker(positionEvent);
		}

	}

	// 4. Make sure the result is drawn again --> is visible then. 
	assert( positionEvent->GetSender()->GetRenderWindow() );

	mitk::RenderingManager::GetInstance()->RequestUpdate( positionEvent->GetSender()->GetRenderWindow() );

	return true;
}

bool mitk::PolygonTool::OnMoveSlice(mitk::Action* action, const mitk::StateEvent* stateEvent)
{
	const mitk::PositionEvent* positionEvent = dynamic_cast<const mitk::PositionEvent*>(stateEvent->GetEvent());
	if (!positionEvent) return false;

	DataTreeNode* workingNode( m_ToolManager->GetWorkingData(0) );
	if ( !workingNode ) return false;

	Image* workingImage = dynamic_cast<Image*>(workingNode->GetData());
	if ( !workingImage ) return false;

	// first, we determine, which slice is affected
	const PlaneGeometry* planeGeometry( dynamic_cast<const PlaneGeometry*> (positionEvent->GetSender()->GetCurrentWorldGeometry2D() ) );
	if ( !workingImage || !planeGeometry ) return false;

	// determine slice number in image
	int affectedDimension( -1 );
	int affectedSlice( -1 );
	DetermineAffectedImageSlice( workingImage, planeGeometry, affectedDimension, affectedSlice );

	// Get world coordinates of affected dimension
	Geometry3D* imageGeometry = workingImage->GetGeometry(0);
	Point3D testPoint = imageGeometry->GetCenter();
	Point3D projectedPoint;
	planeGeometry->Project( testPoint, projectedPoint );

	// Update points for all time steps
	for ( unsigned int time = 0 ; time < m_PointSet->GetTimeSteps( ) ; time++ )
	{
		mitk::PointSet::DataType *pointSet = m_PointSet->GetPointSet( time );
		mitk::PointSet::PointsContainer *points = pointSet->GetPoints();
		mitk::PointSet::PointsContainer::Iterator	it, end;
		for( it = points->Begin(); it != points->End(); it++ )
		{
			mitk::PointSet::PointType point;
			if ( m_PointSet->GetPointIfExists( it->Index(), &point, GetTimeStep( ) ) )
			{
				point[ affectedDimension ] = projectedPoint[ affectedDimension ];
				m_PointSet->SetPoint( it->Index( ), point, GetTimeStep( ) );
			}
		}
	}
	m_PointSet->Modified( );

	m_SliceImageChanged = true;

	UpdateContour( );

	OnMousePressed( action, stateEvent );

	assert( positionEvent->GetSender()->GetRenderWindow() );
	mitk::RenderingManager::GetInstance()->RequestUpdate( positionEvent->GetSender()->GetRenderWindow() );

	return true;
}

void mitk::PolygonTool::SetPixelValue(int n) {
	m_pixelValue = n;
}

int mitk::PolygonTool::GetPixelValue() {
	return m_pixelValue;
}

void mitk::PolygonTool::SetPointSetVisible(bool visible)
{
	// begin of temporary fix for 3m3 release  
	this->Disable3dRendering();
	//end of temporary fix for 3m3 release

	if ( m_PointSetVisible == visible ) 
		return; // nothing changed

	if ( DataStorage* storage = m_ToolManager->GetDataStorage() )
	{
		if (visible)
		{
			storage->Add( m_PointSetNode );
		}
		else
		{
			storage->Remove( m_PointSetNode );
		}
	}

	m_PointSetVisible = visible;
}

void mitk::PolygonTool::OnReferenceDataChanged( )
{
	if ( m_ToolManager == NULL || m_ToolManager->GetReferenceData(0) == NULL )
	{
		return;
	}

	// resize point for mesh
	double pointSize = -1;
	//blMITKUtils::ComputePointSize(m_ToolManager->GetReferenceData(0)->GetData( ),pointSize,1);
	mitk::BaseData* data = m_ToolManager->GetReferenceData(0)->GetData( );
	pointSize = data->GetTimeSlicedGeometry()->GetDiagonalLength() / 75;

	m_PointSetNode->SetProperty("pointsize", mitk::FloatProperty::New( pointSize ) );
	m_PointSetNode->ReplaceProperty("contoursize", mitk::FloatProperty::New(pointSize/2));

	// Set precision to select a point or to add a new point
	m_Precision = pointSize * 2;
	m_PointSetInteractor->SetPrecision( m_Precision );

	m_SliceImageChanged = true;
	m_UpdateContour = true;
}

void mitk::PolygonTool::SetContourMethod( ImageContourProcessor::CONTOUR_METHOD_TYPE method )
{
	m_ContourProcessor->SetContourMethod( method );
}

void mitk::PolygonTool::UpdateTimeStep(unsigned int timeStep)
{
	// Reset contour
	mitk::Contour* contour = FeedbackContourTool::GetFeedbackContour();
	if ( contour )
	{
		contour->Initialize( );
	}

	//check if the vector of StartStates contains enough pointers to use timeStep
	if (timeStep >= 1)
	{
		//now check for this object
		this->ExpandStartStateVector(timeStep+1); //nothing is changed if the number of timesteps in data equals the number of startstates held in statemachine
	}

	if ( m_LastPositionEvent.GetSender( ) )
	{
		// Call HandleEvent() for EIDTIMECHANGED and update pointset size
		m_PointSetInteractor->OnTimeStepChanged( m_LastPositionEvent.GetSender( ) );

		// If current time step has points -> Propagate them
		if ( m_PointSet->GetPointSet( GetTimeStep( ) )->GetNumberOfPoints( ) && 
			m_PointSet->GetPointSet( timeStep )->GetNumberOfPoints( ) == 0 )
		{
			mitk::PointSet::PointsContainer *points;
			points = m_PointSet->GetPointSet( GetTimeStep( ) )->GetPoints( );
			mitk::PointSet::PointsContainer::Iterator	it, end = points->End();
			for( it = points->Begin(); it != end; it++ )
			{
				mitk::PointSet::PointType out;
				if ( points->GetElementIfIndexExists( it->Index( ), &out ) )
				{
					m_PointSet->InsertPoint( it->Index( ), out, timeStep );
				}
			}
		}

		assert( m_LastPositionEvent.GetSender( )->GetRenderWindow() );
		mitk::RenderingManager::GetInstance()->RequestUpdate( m_LastPositionEvent.GetSender( )->GetRenderWindow() );
	}

	// Update time of current state machine
	mitk::FeedbackContourTool::UpdateTimeStep(timeStep);

	m_SliceImageChanged = true;
	m_UpdateContour = true;
}
