/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef InteractiveSegmInteractor_H
#define InteractiveSegmInteractor_H


#include "coreRenderingTree.h"
#include "coreObject.h"
#include "coreDataEntityHolder.h"
#include "coreCommonDataTypes.h"

// MITK
#include "mitkDataTree.h"
#include "mitkToolManager.h"
#include "mitkImage.h"

//Core
#include "coreDataEntity.h"
#include "coreDataHolder.h"



namespace msp{

/**
\brief 
\ingroup ManualSegmentationPlugin
\author Luigi Carotenuto
\date 01-10-11
*/
class InteractiveSegmInteractor 
	: public Core::SmartPointerObject
{

// PUBLIC OPERATIONS
public:

	//! type of inputs for the processor
	typedef enum
	{
		INVALID_TOOL = -1,
		CONTOURTOOL,
		ADDTOOL,
		SUBTRACTTOOL,
		PAINTTOOL,
		WIPETOOL,
		CORRECTIONTOOL,
		FILLREGIONTOOL,
		ERASEREGIONTOOL,
		REGIONGROWINGTOOL,
		POLYGONTOOL,
		LIVEWIRE,
		SPLINE,
		NUMBER_OF_TOOLS
	} TOOLS_TYPE;

	//!
	coreDeclareSmartPointerClassMacro3Param(
		InteractiveSegmInteractor, 
		Core::SmartPointerObject,
		Core::RenderingTree::Pointer,
		Core::DataEntityHolder::Pointer,
		Core::DataEntityHolder::Pointer );

	//! Connect the interactor to the selectedData tree node
	void ConnectToDataTreeNode( );

	//! Disconnect the interactor from the rendering tree
	void DisconnectFromDataTreeNode();

	//! Redefined
	void DestroyInteractor();

	//!
	Core::RenderingTree::Pointer GetRenderingTree() const;

	//!
	void SetRenderingTree(Core::RenderingTree::Pointer val);

	//!
	Core::DataEntityHolder::Pointer GetSelectedMaskHolder() const;

	//!
	void SetSelectedMaskHolder(Core::DataEntityHolder::Pointer val);

	//!
	Core::DataEntityHolder::Pointer GetSelectedImageHolder() const;

	//!
	void SetSelectedImageHolder(Core::DataEntityHolder::Pointer val);

	bool IsConnectedToRenderingTree(Core::DataEntity::Pointer dataEntity);

	//!
	void SetPixelValue(int n);

	//!
	int GetPixelValue();

	//!
	void SetEraseOnlySelectedColor(bool b);

	//!
	bool GetEraseOnlySelectedColor();

	//!
	static void SetToolType(TOOLS_TYPE val){m_toolType = val;}

	//!
	static TOOLS_TYPE GetToolType(){return m_toolType;}


	void SetBrushSize(int val);

	int GetBrushSize();

	//!
	mitk::Tool* GetCurrentTool( );

// PRIVATE OPERATIONS
private:
	//! 
	InteractiveSegmInteractor(
		Core::RenderingTree::Pointer renderingTree,
		Core::DataEntityHolder::Pointer selectedMask,
		Core::DataEntityHolder::Pointer selectedData );

	virtual ~InteractiveSegmInteractor( );


		//! Connect m_pointSetNode to the m_renderingTree
	void ConnectNodeToTree( );

	//! Disconnect m_pointSetNode to the m_renderingTree
	void DisconnectNodeFromTree( );

	//! Connect the interactors to the global instance
	void ConnectInteractors();

	//! Disconnect all interactors to the global instance
	void DisconnectInteractors( );

	//! Redefined
	void CreateInteractor();

	//! Redefined
	void OnInteractorConnected();

	//!
	Core::DataEntity::Pointer GetSelectedMaskDataEntity( );

	//!
	Core::DataEntity::Pointer GetSelectedImageDataEntity( );

	//!
	mitk::DataTreeNode::Pointer GetSelectedMaskRenderingNode( );

	//!
	mitk::Image::Pointer GetSelectedMaskRenderingNodeAsImage( );

	//!
	mitk::DataTreeNode::Pointer GetSelectedImageRenderingNode( );

	//!
	void OnMaskModified( );

	void CreateEmptyWorkingData( );

	//!
	int GetCurrentToolId();

// ATTRIBUTES
private:

	//! Rendering data tree.
	Core::RenderingTree::Pointer m_renderingTree;
	
	//@{ 
	//! \name Data used from data container

	//!current mask image holder
	Core::DataEntityHolder::Pointer m_selectedMaskHolder;

	//! selected Image holder
	Core::DataEntityHolder::Pointer m_selectedImageHolder;
	//@}

	//! Interactor to select a VOI of an image
	mitk::ToolManager::Pointer m_toolManager;

	//!
	int m_pixelValue;

	//!
	bool m_bEraseOnlySelectedColor;

	//!
	static TOOLS_TYPE m_toolType;
	
	int m_brushSize;
};

} // msp

#endif //InteractiveSegmInteractor_h
