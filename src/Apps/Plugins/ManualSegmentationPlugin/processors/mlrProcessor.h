/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef MultiLevelROIProcessor_H
#define MultiLevelROIProcessor_H

#include "gmProcessorsWin32Header.h"
#include "itkImage.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreCommonDataTypes.h"
#include "coreBaseProcessor.h"
#include "blSignal.h"
#include "blTagMap.h"

#include <map>
#include <vector>


/**
\brief this is the processor for multi level roi

\sa MultiLevelROIPanelWidget
\ingroup ManualSegmentationPlugin
\author Albert Sanchez
\date  June 2010
*/

class MultiLevelROIProcessor : public Core::BaseProcessor
{
public:
	//!
	coreProcessor(MultiLevelROIProcessor, Core::BaseProcessor);

	typedef itk::Image<short,3> ImageTypeSeg;

	typedef enum
	{
		INPUT_IMAGE,
		INPUT_ROI,
		NUMBER_OF_INPUTS,
	} INPUT_TYPE;

	typedef enum
	{
		SIGNAL,
		SIGNAL_HISTOGRAM,
		MEASUREMENTS,
		NUMBER_OF_OUTPUTS,
	} OUTPUT_TYPE;


	//! Compute metadata "MultiROILevel" blTagMap using min and max values of pixel data
	static void ComputeLevels( Core::DataEntity::Pointer dataEntity );

	//!
	bool GetExportHistogram() const;
	void SetExportHistogram(bool val);

	//!
	bool GetExportSignal() const;
	void SetExportSignal(bool val);

	//!
	bool GetNormalizeHistogramByVolume() const;
	void SetNormalizeHistogramByVolume(bool val);

	//! Sets the selected level
	void SetSelectedLevel(int selectedLevel);

	//! domain = 1 => use all volume. domain = 2 => use X current slice. = 3 => Y. = 4 => Z.
	void SetDomain(int domain);

	//!
	void SetPos(int pos);

	//! Returns the intensity measures map
	blTagMap::Pointer GetIntensityMeasures( );

	//!
	void Update( );

	//!
	bool GetMeanAllLevels() const;
	void SetMeanAllLevels(bool val);

	//!
	void SetStatisticValue( const std::string value );
	std::string GetStatisticValue( );

private:

	//!
	MultiLevelROIProcessor( );

	//!
	~MultiLevelROIProcessor();

	//! Purposely not implemented
	MultiLevelROIProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );

	//! Use own algorithm
	Core::vtkImageDataPtr ClipImage( Core::vtkImageDataPtr inGrayscaleVolume );

	//! Creates the signal
	void ExportSignal();

	//!
	void ExportMeanAllLevels();

	//! Computes intensity measures
	std::vector<blSignal::Pointer> ComputeIntensityMeasures();

	std::vector<blSignal::Pointer> ComputeMeanIntensityForAllLevels();

	/** Computes intensity measures of a single time step
	This are the tag names:
	- "count": long int
	- "mean": double
	- "std": double
	- "min": double
	- "max": double
	- "volume": double
	*/
	void CalculateIntensityMeasures( 
		int timeStep, blTagMap::Pointer measures );

	//!
	void UpdateHistogram( long int totalVoxels, Core::vtkImageDataPtr hostogram );

private:

	//! Output image measures
	blTagMap::Pointer m_measures;

	//! Selected level. If m_selectedLevel is -1, doesn't use mask image.
	int m_selectedLevel;

	//! If m_Domain is -1, compute statistic for whole image.
	int m_Domain;

	//! m_Pos is used to  select the current slice in the dimension X,Y or Z.
	int m_Pos;

	//! Export histogram as SIGNAL_HISTOGRAM output
	bool m_ExportHistogram;

	//! Export signal as SIGNAL output
	bool m_ExportSignal;

	//!
	bool m_NormalizeHistogramByVolume;

	//!
	bool m_meanAllLevels;

	//!
	std::string m_StatisticValue;
};

#endif //MultiLevelROIProcessor_H

