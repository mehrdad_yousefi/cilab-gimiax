/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "msImageContourProcessor.h"

#include "vtkSmartPointer.h"
#include "vtkImageAnisotropicDiffusion2D.h"
#include "vtkImageGradientMagnitude.h"
#include "vtkImageShiftScale.h"
#include "vtkSplineFilter.h"

#include "blImageUtils.h"

ImageContourProcessor::ImageContourProcessor() 
{
	SetNumberOfInputs( 2 );
	GetInputPort( 0 )->SetName( "Image" );
	GetInputPort( 0 )->SetDataEntityType( Core::ImageTypeId );
	GetInputPort( 1 )->SetName( "Points" );
	GetInputPort( 1 )->SetDataEntityType( Core::PointSetTypeId );

	SetNumberOfOutputs( 1 );
	GetOutputPort( 0 )->SetName( "Contour" );
	GetOutputPort( 0 )->SetDataEntityType( Core::ContourTypeId );

	m_ContourMethod = METHOD_POLYGON;
}


ImageContourProcessor::~ImageContourProcessor() 
{

}

void ImageContourProcessor::SetContourMethod( CONTOUR_METHOD_TYPE method )
{
	m_ContourMethod = method;
}

ImageContourProcessor::CONTOUR_METHOD_TYPE ImageContourProcessor::GetContourMethod( )
{
	return m_ContourMethod;
}

void ImageContourProcessor::Update() 
{
	switch ( m_ContourMethod )
	{
	case METHOD_POLYGON: ComputePolygonMethod( ); break;
	case METHOD_DIJKSTRA:ComputeDijkstraMethod(); break;
	case METHOD_SPLINE:  ComputeSplineMethod(); break;
	}

	Modified( );
}

void ImageContourProcessor::ComputeSplineMethod( )
{
	// Get input
	mitk::PointSet::Pointer inputPointSet;
	GetProcessingData( 1, inputPointSet );
	mitk::PointSet::DataType *pointSet = inputPointSet->GetPointSet( GetTimeStep( ) );
	if ( !pointSet )
	{
		return;
	}
	mitk::PointSet::PointsContainer *points = pointSet->GetPoints();

	// Create polyline
	Core::vtkPolyDataPtr inPolyLine = Core::vtkPolyDataPtr::New( );
	vtkSmartPointer<vtkPoints>	pointsVtk = vtkSmartPointer<vtkPoints>::New( );
	pointsVtk->SetDataTypeToDouble( );
	pointsVtk->SetNumberOfPoints( points->size() );
	inPolyLine->SetPoints( pointsVtk );
	vtkSmartPointer<vtkCellArray>	cellArray = vtkSmartPointer<vtkCellArray>::New( );
	inPolyLine->SetLines( cellArray );

	// Fill point values
	mitk::PointSet::PointsContainer::Iterator	it, end;
	vtkIdType numPoint = 0;
	std::vector<vtkIdType> pointsId;
	for( it = points->Begin(); it != points->End(); it++ )
	{
		mitk::PointSet::PointType point;
		if ( inputPointSet->GetPointIfExists( it->Index(), &point, GetTimeStep() ) )
		{
			pointsVtk->SetPoint( numPoint, it->Value()[ 0 ], it->Value()[ 1 ], it->Value()[ 2 ] );
			pointsId.push_back( numPoint );
			numPoint++;
		}
	}
	if ( pointsId.size( ) )
	{
		cellArray->InsertNextCell( pointsId.size(), &pointsId[ 0 ] );
	}

	// Apply spline
	if ( pointsVtk->GetNumberOfPoints( ) >= 2 )
	{
		vtkSmartPointer<vtkSplineFilter> splineFilter;
		splineFilter = vtkSmartPointer<vtkSplineFilter>::New( );
		splineFilter->SetInput( inPolyLine );
		splineFilter->Update( );
		Core::vtkPolyDataPtr spline = splineFilter->GetOutput( );
		if ( !spline )
		{
			return;
		}
		pointsVtk = spline->GetPoints( );
	}

	// Update contour
	mitk::Contour::Pointer outputContour = mitk::Contour::New( );
	outputContour->Initialize( );
	for (	vtkIdType pointId = 0; pointId < pointsVtk->GetNumberOfPoints(); pointId++ )
	{
		double point[ 3 ];
		pointsVtk->GetPoint( pointId, &point[ 0 ] );
		mitk::Point3D mitkPoint;
		mitkPoint[ 0 ] = point[ 0 ];
		mitkPoint[ 1 ] = point[ 1 ];
		mitkPoint[ 2 ] = point[ 2 ];
		outputContour->AddVertex( mitkPoint );
	}

	GetOutputPort( 0 )->UpdateOutput( outputContour, 0, NULL );
}

void ImageContourProcessor::ComputePolygonMethod( )
{
	mitk::PointSet::Pointer inputPointSet;
	GetProcessingData( 1, inputPointSet );

	mitk::Contour::Pointer outputContour = mitk::Contour::New( );
	outputContour->Initialize( );

	mitk::PointSet::DataType *pointSet = inputPointSet->GetPointSet( GetTimeStep( ) );
	if ( !pointSet )
	{
		return;
	}

	mitk::PointSet::PointsContainer *points = pointSet->GetPoints();
	mitk::PointSet::PointsContainer::Iterator	it, end;
	for( it = points->Begin(); it != points->End(); it++ )
	{
		mitk::PointSet::PointType point;
		if ( inputPointSet->GetPointIfExists( it->Index(), &point, GetTimeStep() ) )
		{
			outputContour->AddVertex( point );
		}
	}

	GetOutputPort( 0 )->UpdateOutput( outputContour, 0, NULL );
}

void ImageContourProcessor::ComputeDijkstraMethod( )
{
	// Compute cost image
	ComputeCostImage( );

	// Get 2D slice
	mitk::Image::Pointer slice;
	GetProcessingData( 0, slice );
	Core::vtkImageDataPtr imageData = slice->GetVtkImageData( );
	imageData->SetOrigin( 
		slice->GetGeometry( 0 )->GetOrigin( ) [ 0 ],
		slice->GetGeometry( 0 )->GetOrigin( ) [ 1 ],
		slice->GetGeometry( 0 )->GetOrigin( ) [ 2 ] );

	// Initialize dijkstra
	m_Dijkstra = vtkSmartPointer<vtkDijkstraImageGeodesicPath>::New( );
	m_Dijkstra->SetInput( m_CostImage );

	// Get points
	mitk::PointSet::Pointer inputPointSet;
	GetProcessingData( 1, inputPointSet );

	// Compute dijkstra
	mitk::Contour::Pointer outputContour = mitk::Contour::New( );
	outputContour->Initialize( );

	mitk::PointSet::DataType *pointSet = inputPointSet->GetPointSet( GetTimeStep( ) );
	if ( !pointSet )
	{
		return;
	}

	mitk::PointSet::PointsContainer *points = pointSet->GetPoints();
	mitk::PointSet::PointsContainer::Iterator	it, end;
	mitk::PointSet::PointType prevPoint;
	for( it = points->Begin(); it != points->End(); it++ )
	{
		// Get point
		mitk::PointSet::PointType worldPoint;
		if ( !inputPointSet->GetPointIfExists( it->Index(), &worldPoint, GetTimeStep() ) )
		{
			continue;
		}

		// World to slice 2D
		mitk::PointSet::PointType point;
		slice->GetGeometry( 0 )->WorldToItkPhysicalPoint( worldPoint, point );
		// This coordinate should be 0 to use FindPoint( )
		point[ 2 ] = slice->GetGeometry( 0 )->GetOrigin( ) [ 2 ];

		// Compute dijkstra
		if ( it != points->Begin() )
		{
			// Find image indexes
			vtkIdType beginVertId, endVertId;
			beginVertId = imageData->FindPoint( prevPoint[ 0 ], prevPoint[ 1 ], prevPoint[ 2 ] );
			endVertId = imageData->FindPoint( point[ 0 ], point[ 1 ], point[ 2 ] );

			if ( beginVertId == -1 || endVertId == -1 ) 
			{
				continue;
			}

			// Call dijkstra
			m_Dijkstra->SetStartVertex( endVertId );
			m_Dijkstra->SetEndVertex( beginVertId );
			m_Dijkstra->Update( );
			vtkPolyData *pd = m_Dijkstra->GetOutput();

			// Get path and update contour
			vtkIdType npts = 0, *pts = NULL;
			pd->GetLines()->InitTraversal();
			pd->GetLines()->GetNextCell( npts, pts );
			for ( int i = 0; i < npts; ++i )
			{
				mitk::Point3D slicePoint;
				slicePoint[ 0 ] = pd->GetPoint( pts[i] )[ 0 ];
				slicePoint[ 1 ] = pd->GetPoint( pts[i] )[ 1 ];
				slicePoint[ 2 ] = pd->GetPoint( pts[i] )[ 2 ];

				// convert from 2D slice to world coordinates
				mitk::Point3D worldPoint;
				slice->GetGeometry( 0 )->ItkPhysicalPointToWorld( slicePoint, worldPoint );
				outputContour->AddVertex( worldPoint );
			}

		}

		prevPoint = point;
	}

	GetOutputPort( 0 )->UpdateOutput( outputContour, 0, NULL );
}

void ImageContourProcessor::ComputeCostImage( )
{
	if ( GetInputDataEntity( 0 )->GetMTime( ) < GetMTime( ) )
	{
		return;
	}

	mitk::Image::Pointer slice;
	GetProcessingData( 0, slice );
	Core::vtkImageDataPtr imageData = slice->GetVtkImageData( );
	imageData->SetOrigin( 
		slice->GetGeometry( 0 )->GetOrigin( ) [ 0 ],
		slice->GetGeometry( 0 )->GetOrigin( ) [ 1 ],
		slice->GetGeometry( 0 )->GetOrigin( ) [ 2 ] );

	vtkSmartPointer<vtkImageAnisotropicDiffusion2D> diffusion;
	diffusion = vtkSmartPointer<vtkImageAnisotropicDiffusion2D>::New( );
	diffusion->SetInput(imageData);
	diffusion->Update( );

    // Gradient magnitude for edges
    vtkSmartPointer<vtkImageGradientMagnitude> grad = vtkSmartPointer<vtkImageGradientMagnitude>::New();
    grad->SetDimensionality(2);
    grad->HandleBoundariesOn();
    grad->SetInputConnection(diffusion->GetOutputPort());
    grad->Update();

    double *range = grad->GetOutput()->GetScalarRange();

    // Invert the gradient magnitude so that low costs are
    // associated with strong edges and scale from 0 to 1
    vtkSmartPointer<vtkImageShiftScale> gradInvert = vtkSmartPointer<vtkImageShiftScale>::New();
    gradInvert->SetShift(-1.0*range[1]);
    gradInvert->SetScale(1.0 / (range[0] - range[1]));
    gradInvert->SetOutputScalarTypeToFloat();
    gradInvert->SetInputConnection(grad->GetOutputPort());
    gradInvert->Update();
    m_CostImage = gradInvert->GetOutput();
}

