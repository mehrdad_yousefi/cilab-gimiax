/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef InteractiveSegmentationProcessor_H
#define InteractiveSegmentationProcessor_H

#include "gmProcessorsWin32Header.h"
#include "itkImage.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreCommonDataTypes.h"
#include "corePluginMacros.h"
#include "coreDataEntityList.h"
#include "coreDataEntityHelper.h"
#include "coreRenderingTree.h"
#include "coreBaseProcessor.h"
//#include "coreProcessorFactory.h"

#include <map>
#include <vector>


/**
\brief this is the processor for manual segmentation. It only computes a surface mesh basedon input mask image
\sa ManualSegmentationPanelWidget
\ingroup ManualSegmentationPlugin
\author Luigi Carotenuto
\date  Sept 2011
*/

class InteractiveSegmentationProcessor : public Core::BaseProcessor
{
public:
	//!
	coreProcessor(InteractiveSegmentationProcessor, Core::BaseProcessor);


	typedef enum
	{
		REFERENCE_IMAGE,
		INPUT_ROI,
		NUMBER_OF_INPUTS,
	} INPUT_TYPE;

	//!
	InteractiveSegmentationProcessor( );

	//!
	~InteractiveSegmentationProcessor( );

	//! 
	void Update( );

private:

	//! Purposely not implemented
	InteractiveSegmentationProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );

private:
	
};

#endif //InteractiveSegmentationProcessor_H

