/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "msReduceContourProcessor.h"

#include "vtkMath.h"
#include "vtkPolygon.h"

ReduceContourProcessor::ReduceContourProcessor() 
{
	SetNumberOfInputs( 1 );
	GetInputPort( 0 )->SetName( "Contour" );
	GetInputPort( 0 )->SetDataEntityType( Core::ContourTypeId );

	SetNumberOfOutputs( 1 );
	GetOutputPort( 0 )->SetName( "Reduced Contour" );
	GetOutputPort( 0 )->SetDataEntityType( Core::ContourTypeId );
}


ReduceContourProcessor::~ReduceContourProcessor() 
{

}

void ReduceContourProcessor::Update() 
{
	Core::vtkPolyDataPtr inputContour;
	GetProcessingData( 0, inputContour );

	// Convert Contour to polyData
	Core::vtkPolyDataPtr outputContour = Core::vtkPolyDataPtr::New( );
	Cull( inputContour, outputContour );

	GetOutputPort( 0 )->UpdateOutput( outputContour, 0, NULL );
}


int ReduceContourProcessor::ReducePolyData2D( 
	mitk::Contour::Pointer inContour, mitk::Contour::Pointer outContour )
{
  if ( !inContour || !outContour ){ return 0; }

  mitk::Contour::PointsContainerPointer inPts = inContour->GetPoints( );
  int n = inPts->Size();

  if ( n < 3 ) { return 0; }

  mitk::Contour::PointsContainer::Element p0;
  p0 = inPts->GetElement( 0 );
  mitk::Contour::PointsContainer::Element p1;
  p1 = inPts->GetElement( n - 1 );

  bool minusNth = ( p0[0] == p1[0] && p0[1] == p1[1] && p0[2] == p1[2] );

  struct frenet
    {
    double t[3];   // unit tangent vector
    bool   state; // state of kappa: zero or non-zero  T/F
    };

  frenet* f;
  f = new frenet[n];
  double tL;
  // calculate the tangent vector by forward differences
  for ( int i = 0; i < n; ++i )
    {
    p0 = inPts->GetElement( i );
    p1 = inPts->GetElement( ( i + 1 ) % n );
	double vtkP0[ 3 ] = { p0[ 0 ], p0[ 1 ], p0[ 2 ] };
	double vtkP1[ 3 ] = { p1[ 0 ], p1[ 1 ], p1[ 2 ] };
    tL = sqrt( vtkMath::Distance2BetweenPoints( vtkP0, vtkP1 ) );
    if ( tL == 0.0 ){ tL = 1.0; }
    for ( int j = 0 ; j < 3; ++j )
      {
      f[i].t[j] = (p1[j] - p0[j]) / tL;
      }
    }

  // calculate kappa from tangent vectors by forward differences
  // mark those points that have very low curvature
  double eps = 1.e-10;

  for ( int i = 0; i < n; ++i )
    {
    f[i].state = ( fabs( vtkMath::Dot( f[i].t, f[( i + 1 ) % n].t ) - 1.0 ) < eps );
    }

  vtkPoints* tempPts = vtkPoints::New();

  // mark keepers
  vtkIdTypeArray* ids = vtkIdTypeArray::New();

  // for now, insist on keeping the first point for closure
  ids->InsertNextValue( 0 );

  for ( int i = 1; i < n; ++i )
    {
    bool pre = f[( i - 1 + n ) % n].state; // means fik != 1
    bool cur = f[i].state;  // means fik = 1
    bool nex = f[( i + 1 ) % n].state;

   // possible vertex bend patterns for keep: pre cur nex
   // 0 0 1
   // 0 1 1
   // 0 0 0
   // 0 1 0

   // definite delete pattern
   // 1 1 1

   bool keep = false;

   if      (  pre &&  cur &&  nex ) { keep = false; }
   else if ( !pre && !cur &&  nex ) { keep = true; }
   else if ( !pre &&  cur &&  nex ) { keep = true; }
   else if ( !pre && !cur && !nex ) { keep = true; }
   else if ( !pre &&  cur && !nex ) { keep = true; }  

   if ( keep  ) // not a current sure thing but the preceding delete was
      {
      ids->InsertNextValue( i );
      }
    }

  for ( int i = 0; i < ids->GetNumberOfTuples(); ++i )
    {
	  mitk::Contour::PointsContainer::Element p0;
	  p0 = inPts->GetElement( ids->GetValue( i ) );
	  outContour->AddVertex( p0 );
    }

  ids->Delete();
  tempPts->Delete();
  delete [] f;

  return 1;
}

void ConvertPointSequenceToPolyData(vtkPoints *inPts,
                                    const int & closed,
                                    vtkPolyData *outPoly)
{
  if ( !inPts || !outPoly )
    {
    return;
    }

  int npts = inPts->GetNumberOfPoints();

  if ( npts < 2 )
    {
    return;
    }

  double p0[3];
  double p1[3];
  inPts->GetPoint( 0, p0 );
  inPts->GetPoint( npts - 1, p1 );
  if ( p0[0] == p1[0] && p0[1] == p1[1] && p0[2] == p1[2] && closed )
    {
    --npts;
    }

  vtkPoints *temp = vtkPoints::New();
  temp->SetNumberOfPoints( npts );
  for ( int i = 0; i < npts; ++i )
    {
    temp->SetPoint( i, inPts->GetPoint( i ) );
    }

  vtkCellArray *cells = vtkCellArray::New();
  cells->Allocate( cells->EstimateSize( npts + closed, 2 ) );

  cells->InsertNextCell( npts + closed );

  for ( int i = 0; i < npts; ++i )
    {
    cells->InsertCellPoint( i );
    }

  if ( closed )
    {
    cells->InsertCellPoint( 0 );
    }

  outPoly->SetPoints( temp );
  temp->Delete();
  outPoly->SetLines( cells );
  cells->Delete();
} 

// --------------------------------------------------------------------------
// assumes a piecwise linear polyline with points at discrete locations
//
int ReduceContourProcessor::ReducePolyData2D(vtkPolyData *inPoly,
                 vtkPolyData *outPoly, const int & closed )
{
  if ( !inPoly || !outPoly )
    {
    return 0;
    }

  vtkPoints *inPts = inPoly->GetPoints();
  if ( !inPts )
    {
    return 0;
    }
  int n = inPts->GetNumberOfPoints();
  if ( n < 3 )
    {
    return 0;
    }

  double p0[3];
  inPts->GetPoint( 0, p0 );
  double p1[3];
  inPts->GetPoint( n - 1, p1 );
  bool minusNth = ( p0[0] == p1[0] && p0[1] == p1[1] && p0[2] == p1[2] );
  if ( minusNth && closed )
    {
    --n;
    }

  struct frenet {
    double t[3];  // unit tangent vector
    bool state;   // state of kappa: zero or non-zero  T/F
  };

  frenet *f;
  f = new frenet[n];
  double tL;
  // calculate the tangent vector by forward differences
  for ( int i = 0; i < n; ++i )
    {
    inPts->GetPoint( i, p0 );
    inPts->GetPoint( ( i + 1 ) % n, p1 );
    tL = sqrt( vtkMath::Distance2BetweenPoints( p0, p1 ) );
    if ( tL == 0.0 ) { tL = 1.0; }
    for ( int j = 0; j < 3; ++j )
      {
      f[i].t[j] = ( p1[j] - p0[j] ) / tL;
      }
    }

  // calculate kappa from tangent vectors by forward differences
  // mark those points that have very low curvature
  double eps = 1.e-10;

  for ( int i = 0; i < n; ++i )
    {
    f[i].state = ( fabs( vtkMath::Dot( f[i].t, f[( i + 1 ) % n].t ) - 1.0 )
                   < eps );
    }

  vtkPoints *tempPts = vtkPoints::New();

  // mark keepers
  vtkIdTypeArray *ids = vtkIdTypeArray::New();

  // for now, insist on keeping the first point for closure
  ids->InsertNextValue( 0 );

  for ( int i = 1; i < n; ++i )
    {
    bool pre = f[( i - 1 + n ) % n].state; // means fik != 1
    bool cur = f[i].state;                 // means fik = 1
    bool nex = f[( i + 1 ) % n].state;

    // possible vertex bend patterns for keep: pre cur nex
    // 0 0 1
    // 0 1 1
    // 0 0 0
    // 0 1 0

    // definite delete pattern
    // 1 1 1

    bool keep = false;

   if      (  pre &&  cur &&  nex ) { keep = false; }
   else if ( !pre && !cur &&  nex ) { keep = true; }
   else if ( !pre &&  cur &&  nex ) { keep = true; }
   else if ( !pre && !cur && !nex ) { keep = true; }
   else if ( !pre &&  cur && !nex ) { keep = true; }  

    if ( keep  ) // not a current sure thing but the preceding delete was
      {
      ids->InsertNextValue( i );
      }
    }

  for ( int i = 0; i < ids->GetNumberOfTuples(); ++i )
    {
    tempPts->InsertNextPoint( inPts->GetPoint( ids->GetValue( i ) ) );
    }

  if ( closed )
    {
    tempPts->InsertNextPoint( inPts->GetPoint( ids->GetValue( 0 ) ) );
    }

  ConvertPointSequenceToPolyData( tempPts, closed, outPoly );

  ids->Delete();
  tempPts->Delete();
  delete[] f;
  return 1;
}


double PointsArea(vtkPoints *points)
{
  int       numPoints = points->GetNumberOfPoints();
  vtkIdType *ids = new vtkIdType[numPoints + 1];
  int       i;

  for ( i = 0; i < numPoints; i++ )
    {
    ids[i] = i;
    }
  ids[i] = 0;
  double normal[3];
  double rval( vtkPolygon::ComputeArea(points, numPoints, ids, normal) );
  delete[] ids;
  return rval;
}

double PolyDataArea(vtkPolyData *pd)
{
  assert(pd->GetNumberOfLines() == 1);
  vtkIdType npts(0);
  vtkIdType *pts(0);
  double    normal[3];
  pd->GetLines()->InitTraversal();
  pd->GetLines()->GetNextCell(npts, pts);
  return vtkPolygon::ComputeArea(pd->GetPoints(),
                                 npts,
                                 pts,
                                 normal);
}

inline
void
IdsMinusThisPoint(vtkIdType *ids, int PointCount, int i)
{
  int k = 0;

  for ( int j = 0; j < PointCount; j++ )
    {
    if ( j != i )
      {
      ids[k] = j;
      k++;
      }
    }
  // leaving off first point? 1 is index of first point
  // in the reduced polygon;
  ids[k] = i == 0 ? 1 : 0;
} 

void ReduceContourProcessor::Cull(vtkPolyData *in, vtkPolyData *out)
{
  //
  // Algorithm:
  // Error = 0
  // Create BTPolygon from points in input. Compute Original Area.
  //
  // while Error < MaxError
  //
  //    foreach vertex in input
  //      Create Polygon from input, minus this vertex
  //      Compute area.
  //      subtract area from original area.
  //    endfor
  //    find minimum error vertex, remove it.
  //    error = minimum error
  //

  //

  // do an initial point count reduction based on
  // curvature through vertices of the polygons.
  vtkPolyData *in2 = vtkPolyData::New();

  ReducePolyData2D(in, in2, 1);

  double originalArea( PolyDataArea(in2) );

  //
  // SWAG numbers -- accept
  // area change of 0.5%,
  // regard 0.005% as the same as zero
  double maxError = originalArea * 0.005;
  double errEpsilon = maxError * 0.001;

  vtkPoints *curPoints = vtkPoints::New();
  curPoints->DeepCopy( in2->GetPoints() );
  int       PointCount = curPoints->GetNumberOfPoints();
  vtkIdType *ids = new vtkIdType[PointCount];
  vtkIdType minErrorPointID = -1;

  for (;; )
    {
    double minError = 10000000.00;
    //
    // remove each point, one at a time and find the minimum error
    for ( int i = 0; i < PointCount; i++ )
      {
      // build id list, minus the current point;
      IdsMinusThisPoint(ids, PointCount, i);
      double normal[3];
      double curArea = vtkPolygon::ComputeArea(curPoints,
                                               PointCount - 1,
                                               ids,
                                               normal);
      double thisError = fabs(originalArea - curArea);
      if ( thisError < minError )
        {
        minError = thisError;
        minErrorPointID = i;
        // if the area error is absurdly low, just get rid of
        // this point and move on.
        if ( thisError < errEpsilon )
          {
          break;
          }
        }
      }
    //
    // if we have a new winner for least important point
    if ( minError <= maxError )
      {
      vtkPoints *newPoints = vtkPoints::New();

      for ( int i = 0; i < PointCount; i++ )
        {
        if ( i == minErrorPointID )
          {
          continue;
          }
        double point[3];
        curPoints->GetPoint(i, point);
        newPoints->InsertNextPoint(point);
        }
      curPoints->Delete();
      curPoints = newPoints;
      --PointCount;
      }
    else
      {
      break;
      }
    }
  ConvertPointSequenceToPolyData(curPoints, 1, out);
  curPoints->Delete();
  in2->Delete();
  delete[] ids;
}
