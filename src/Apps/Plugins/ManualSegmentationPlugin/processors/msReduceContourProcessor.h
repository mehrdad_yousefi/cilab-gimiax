/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef msReduceContourProcessor_H
#define msReduceContourProcessor_H

#include "coreBaseProcessor.h"

/**
\brief Reduce contour points

\ingroup ManualSegmentationPlugin
\author Xavi Planes
\date  Sept 2012
*/

class ReduceContourProcessor : public Core::BaseProcessor
{
public:
	//!
	coreProcessor(ReduceContourProcessor, Core::BaseProcessor);

	//!
	void Update();

private:
	//!
	ReduceContourProcessor( );

	//!
	~ReduceContourProcessor( );

	//! Purposely not implemented
	ReduceContourProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );

	//!
	void Cull(vtkPolyData *in, vtkPolyData *out);

	//!
	int ReducePolyData2D( mitk::Contour::Pointer inContour, mitk::Contour::Pointer outContour );

	//!
	int ReducePolyData2D(vtkPolyData *inPoly,
					 vtkPolyData *outPoly, const int & closed );

private:
	
};

#endif //msReduceContourProcessor_H

