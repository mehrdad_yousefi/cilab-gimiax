/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "mlrProcessor.h"
#include "coreDataEntityHolder.h"
#include "coreReportExceptionMacros.h"
#include "coreDataEntityHelper.h"

#include "coreVTKImageDataHolder.h"

#include "blSignalCollective.h"

#include "vtkImageAccumulate.h"
#include "vtkImageReslice.h"
#include "vtkImageMask.h"
#include "vtkImageClip.h"

#include "blImageUtils.h"
//#include "svPlotManagerWidget.h"

MultiLevelROIProcessor::MultiLevelROIProcessor() 
{
	SetNumberOfInputs( NUMBER_OF_INPUTS );
	SetNumberOfOutputs( NUMBER_OF_OUTPUTS );

	GetInputPort( INPUT_IMAGE )->SetName( "Input image" );
	GetInputPort( INPUT_IMAGE )->SetDataEntityType( Core::ImageTypeId );
	GetInputPort( INPUT_IMAGE )->SetNotValidDataEntityType( Core::ImageTypeId | Core::ROITypeId );
	GetInputPort( INPUT_IMAGE )->SetUpdateMode( Core::BaseFilterInputPort::UPDATE_ACCESS_MULTIPLE_TIME_STEP );
	GetInputPort( INPUT_ROI )->SetName( "ROI image" );
	GetInputPort( INPUT_ROI )->SetDataEntityType( Core::ROITypeId );
	GetInputPort( INPUT_ROI )->SetUpdateMode( Core::BaseFilterInputPort::UPDATE_ACCESS_MULTIPLE_TIME_STEP );

	GetOutputPort( SIGNAL )->SetName( "Signal" );
	GetOutputPort( SIGNAL )->SetDataEntityType( Core::SignalTypeId );
	GetOutputPort( SIGNAL )->SetReuseOutput( true );
	GetOutputPort( SIGNAL_HISTOGRAM )->SetName( "Histogram" );
	GetOutputPort( SIGNAL_HISTOGRAM )->SetDataEntityType( Core::SignalTypeId );
	GetOutputPort( MEASUREMENTS )->SetName( "Measurements" );
	GetOutputPort( MEASUREMENTS )->SetDataEntityName( "Measurements" );
	GetOutputPort( MEASUREMENTS )->SetDataEntityType( Core::NumericDataTypeId );
	GetOutputPort( MEASUREMENTS )->SetReuseOutput( true );

	m_measures = blTagMap::New( );
	m_measures->AddTag( "count", (long int)( 0 ) );
	m_measures->AddTag( "mean", (double)( 0 ) );
	m_measures->AddTag( "std", (double)( 0 ) );
	m_measures->AddTag( "min", (double)( 0 ) );
	m_measures->AddTag( "max", (double)( 0 ) );
	m_measures->AddTag( "volume", (double)( 0 ) );

	m_ExportHistogram = false;
	m_ExportSignal = false;
	m_NormalizeHistogramByVolume = false;

	m_meanAllLevels = false;
}


MultiLevelROIProcessor::~MultiLevelROIProcessor() 
{

}

void MultiLevelROIProcessor::ExportSignal( ) 
{
	// The output data entity that holds the signal
	Core::DataEntity::Pointer dataEntityIntensityMeasures;

	// Compute the intensity measures
	std::vector<blSignal::Pointer> intensityMeasures = ComputeIntensityMeasures();


	// Create a signal collective. This collective holds all the signals 
	blSignalCollective::Pointer signalCollective = blSignalCollective::New();
	signalCollective->SetNumberOfSignals(intensityMeasures.size());

	for ( int i = 0; i < intensityMeasures.size(); i++ )
	{
		signalCollective->SetSignal(i,intensityMeasures[i]);  
	}

	// Get current level name
	std::string name;
	if ( GetInputDataEntity(INPUT_ROI).IsNotNull( ) )
	{
		blTagMap::Pointer ROIlevels;
		blTag::Pointer tag = GetInputDataEntity(INPUT_ROI)->GetMetadata()->FindTagByName( "MultiROILevel" );
		if ( tag.IsNull( ) )
		{
			throw Core::Exceptions::Exception( 
				"MultiLevelROIProcessor::ExportSignal",
				"Cannot retrieve roi levels" );
		}
		
		tag->GetValue( ROIlevels );
	
		// Selected pixel level value starts from 1
		// and the tag map starts from 0
		tag->GetValue( ROIlevels );
		blTagMap::Iterator it = ROIlevels->GetIteratorBegin();
		while( it!= ROIlevels->GetIteratorEnd() )
		{
			int currentValue;
			it->second->GetValue( currentValue );
			if ( currentValue == m_selectedLevel - 1 )
			{
				break;
			}
			it++;
		}
		if ( it!= ROIlevels->GetIteratorEnd() )
		{
			tag = it->second;
			name = tag->GetName();
		}

	}

	// Build DataEntity
	dataEntityIntensityMeasures = Core::DataEntity::New( Core::SignalTypeId );
	dataEntityIntensityMeasures->AddTimeStep( signalCollective );

	if ( GetOutputDataEntity( SIGNAL ).IsNotNull( ) )
	{
		GetOutputDataEntity( SIGNAL )->GetMetadata( )->SetName( name + " Measures signal" );
	}
	else
	{
		dataEntityIntensityMeasures->GetMetadata( )->SetName( name + " Measures signal" );
	}

	GetOutputPort( SIGNAL )->UpdateOutput( dataEntityIntensityMeasures, GetInputDataEntity(INPUT_IMAGE) );
}
void MultiLevelROIProcessor::ExportMeanAllLevels()
{
	// The output data entity that holds the signal
	Core::DataEntity::Pointer dataEntityIntensityMeasures;

	// Compute the intensity measures
	std::vector<blSignal::Pointer> intensityMeasures = ComputeMeanIntensityForAllLevels();



	// Create a signal collective. This collective holds all the signals 
	blSignalCollective::Pointer signalCollective = blSignalCollective::New();
	signalCollective->SetNumberOfSignals(intensityMeasures.size());

	dataEntityIntensityMeasures = Core::DataEntity::New( Core::SignalTypeId );
	dataEntityIntensityMeasures->AddTimeStep( signalCollective );

	for ( int i = 0; i < intensityMeasures.size(); i++ )
	{
		signalCollective->SetSignal(i,intensityMeasures[i]);  
	}

	if ( GetOutputDataEntity( SIGNAL ).IsNotNull( ) )
	{
		GetOutputDataEntity( SIGNAL )->GetMetadata( )->SetName( m_StatisticValue + " for all Levels" );
	}
	else
	{
		dataEntityIntensityMeasures->GetMetadata( )->SetName( m_StatisticValue + " for all Levels" );
	}

	GetOutputPort( SIGNAL )->UpdateOutput( dataEntityIntensityMeasures, GetInputDataEntity(INPUT_IMAGE) );
}
std::vector<blSignal::Pointer> MultiLevelROIProcessor::ComputeMeanIntensityForAllLevels( ) 
{
	std::vector<blSignal::Pointer> signals;
	if ( GetInputDataEntity( INPUT_ROI ).IsNull( ) )
	{
		return signals;
	}

	std::vector<double> timeVector;
	timeVector.resize(GetInputDataEntity( INPUT_ROI )->GetNumberOfTimeSteps());

	blTagMap::Pointer ROIlevels;
	blTag::Pointer tag = GetInputDataEntity(INPUT_ROI)->GetMetadata()->FindTagByName( "MultiROILevel" );
	if ( tag.IsNull( ) )
	{
		throw Core::Exceptions::Exception( 
			"MultiLevelROIProcessor::ExportSignal",
			"Cannot retrieve roi levels" );
	}

	tag->GetValue( ROIlevels );




	std::string name;
	// Selected pixel level value starts from 1
	// and the tag map starts from 0
	tag->GetValue( ROIlevels );
	blTagMap::Iterator it = ROIlevels->GetIteratorBegin();
	int levelsNum = 1;
	while( it!= ROIlevels->GetIteratorEnd() )
	{
		m_selectedLevel = levelsNum;
		std::vector<double>  meanIntensity;
		meanIntensity.resize(GetInputDataEntity( INPUT_ROI )->GetNumberOfTimeSteps());

		Core::DataEntity::Pointer inGrayscaleVolume;
		inGrayscaleVolume = GetInputDataEntity( INPUT_IMAGE );

		for ( int timeStep = 0; timeStep < GetInputDataEntity( INPUT_ROI )->GetNumberOfTimeSteps(); timeStep++ )
		{
			Core::vtkImageDataPtr parentImage;
			GetProcessingData( INPUT_IMAGE, parentImage, timeStep );

			blTagMap::Pointer measures = blTagMap::New( );
			CalculateIntensityMeasures( timeStep, measures );

			timeVector[timeStep]= timeStep;
			meanIntensity[timeStep] = measures->GetTagValue<double>(m_StatisticValue);
			
			if ( timeStep == GetTimeStep() )
			{
				m_measures->AddTags( measures );
			}

			inGrayscaleVolume->SetTimeAtTimeStep( timeStep, timeStep );
		}
		blTag::Pointer tag;
		std::string levelName;	
		tag = it->second;
		levelName = tag->GetName();
		// Create the mean intensity signal 
		blSignal::Pointer signalMean = blSignal::Build( levelName,
			"time step", "Value", timeVector, meanIntensity );
		signals.push_back( signalMean );
		it++;
		levelsNum++;
	}
	return signals;	
}
std::vector<blSignal::Pointer> MultiLevelROIProcessor::ComputeIntensityMeasures( ) 
{
	std::vector<blSignal::Pointer> signals;
	if ( GetInputDataEntity( INPUT_IMAGE ).IsNull( ) )
	{
		return signals;
	}

	std::vector<double> meanIntensity;
	std::vector<double> stdIntensity;
	std::vector<double> minIntensity;
	std::vector<double> maxIntensity;
	std::vector<double> volumeVector;

	std::vector<double> timeVector;
	timeVector.resize(GetInputDataEntity( INPUT_IMAGE )->GetNumberOfTimeSteps());
	meanIntensity.resize(GetInputDataEntity( INPUT_IMAGE )->GetNumberOfTimeSteps());
	stdIntensity.resize(GetInputDataEntity( INPUT_IMAGE )->GetNumberOfTimeSteps());
	minIntensity.resize(GetInputDataEntity( INPUT_IMAGE )->GetNumberOfTimeSteps());
	maxIntensity.resize(GetInputDataEntity( INPUT_IMAGE )->GetNumberOfTimeSteps());
	volumeVector.resize(GetInputDataEntity( INPUT_IMAGE )->GetNumberOfTimeSteps());

	Core::DataEntity::Pointer inGrayscaleVolume;
	inGrayscaleVolume = GetInputDataEntity( INPUT_IMAGE );


	for ( int timeStep = 0; timeStep < GetInputDataEntity( INPUT_IMAGE )->GetNumberOfTimeSteps(); timeStep++ )
	{
		Core::vtkImageDataPtr parentImage;
		GetProcessingData( INPUT_IMAGE, parentImage, timeStep );

		blTagMap::Pointer measures = blTagMap::New( );
		CalculateIntensityMeasures( timeStep, measures );

		timeVector[timeStep]= timeStep;
		meanIntensity[timeStep] = measures->GetTagValue<double>("mean");
		stdIntensity[timeStep] = measures->GetTagValue<double>("std");
		minIntensity[timeStep] = measures->GetTagValue<double>("min");
		maxIntensity[timeStep] = measures->GetTagValue<double>("max");
		volumeVector[timeStep] = measures->GetTagValue<double>("volume");

		if ( timeStep == GetTimeStep() )
		{
			m_measures->AddTags( measures );
		}

		inGrayscaleVolume->SetTimeAtTimeStep( timeStep, timeStep );
	}


	// Create the mean intensity signal 
	blSignal::Pointer signalMean = blSignal::Build( "Mean", "time step", "Level", timeVector, meanIntensity );
	blSignal::Pointer signalStd = blSignal::Build( "std dev", "time step", "Level", timeVector, stdIntensity );
	blSignal::Pointer signalMin = blSignal::Build( "Min", "time step", "Level", timeVector, minIntensity );
	blSignal::Pointer signalMax = blSignal::Build( "Max", "time step", "Level", timeVector, maxIntensity );
	blSignal::Pointer signalVolume = blSignal::Build( "Volume", "time step", "cm3", timeVector, volumeVector );

	signals.push_back( signalMean );
	signals.push_back( signalStd );
	signals.push_back( signalMin );
	signals.push_back( signalMax );
	signals.push_back( signalVolume );

	return signals;	

}


void MultiLevelROIProcessor::SetSelectedLevel(int selectedLevel)
{
	m_selectedLevel = selectedLevel;
}

void MultiLevelROIProcessor::SetDomain(int domain)
{
	m_Domain = domain;
}

void MultiLevelROIProcessor::SetPos(int pos)
{
	m_Pos = pos;
}

blTagMap::Pointer MultiLevelROIProcessor::GetIntensityMeasures( )
{
	return m_measures;
}

void MultiLevelROIProcessor::CalculateIntensityMeasures( 
	int timeStep, blTagMap::Pointer measures ) 
{
	Core::vtkImageDataPtr inGrayscaleVolume;
	GetProcessingData( INPUT_IMAGE, inGrayscaleVolume, timeStep );

	// If roi is NULL, compute over whole image
	Core::vtkImageDataPtr inLabelmapVolume;
	try
	{
		GetProcessingData( INPUT_ROI, inLabelmapVolume, timeStep );
	}
	catch(...)
	{
	}

	// If domain is not 3D -> extract 2D slice
	inGrayscaleVolume = ClipImage( inGrayscaleVolume );

	// Compute statistics
	vtkSmartPointer<vtkImageAccumulate> stat1 = vtkSmartPointer<vtkImageAccumulate>::New();
	stat1->SetInput( inGrayscaleVolume );

	// Use mask image
	if ( inLabelmapVolume != NULL && m_selectedLevel != -1 )
	{
		// Resample label map to the properties of input gray level
		// Otherwise, the statistics return 0
		vtkSmartPointer<vtkImageReslice> resample = vtkSmartPointer<vtkImageReslice>::New();
		resample->SetInput( inLabelmapVolume );
		resample->SetInformationInput( inGrayscaleVolume );
		resample->Update( );

		// use vtk's statistics class with the binary label map as a stencil
		vtkImageToImageStencil *stencil = vtkImageToImageStencil::New();
		stencil->SetInput(resample->GetOutput());
		stencil->ThresholdBetween(m_selectedLevel, m_selectedLevel);

		stat1->SetStencil( stencil->GetOutput() );
	}


	// Set histogram properties: number of bins and range for each bin
	// Output image will have (Nbins, 1, 1) dimensions
	double range[2];
	inGrayscaleVolume->GetScalarRange( range );
	// Number of bins
	int maxX = static_cast<int>(ceil( range[1] ) )-static_cast<int>( floor( range[0] ) )-1;
	stat1->SetComponentExtent(0,maxX,0,0,0,0 );
	// Origin of range
	stat1->SetComponentOrigin( range[0],0,0 );
	// Spacing for each bin
	stat1->SetComponentSpacing( 1,0,0 );

	// Update
	stat1->Update();

	// Update measures
	if ( (stat1->GetVoxelCount()) > 0 ) 
	{
		const double *spacing = inGrayscaleVolume->GetSpacing();
		double cubicMMPerVoxel = spacing[0] * spacing[1] * spacing[2];

		measures->AddTag( "count", (long int)( stat1->GetVoxelCount() ) );
		measures->AddTag( "volume", static_cast<double>(stat1->GetVoxelCount()) * cubicMMPerVoxel / 1000 );
		measures->AddTag( "min", stat1->GetMin()[0] );
		measures->AddTag( "max", stat1->GetMax()[0] );
		measures->AddTag( "mean", (stat1->GetMean())[0] );
		measures->AddTag( "std", (stat1->GetStandardDeviation())[0] );
	}
	else
	{

		measures->AddTag( "count", (long int)( 0 ) );
		measures->AddTag( "volume", static_cast<double>( 0 ) );
		measures->AddTag( "min", 0 );
		measures->AddTag( "max", 0 );
		measures->AddTag( "mean", 0 );
		measures->AddTag( "std", 0 );
	}

	// Update histogram signal
	UpdateHistogram( stat1->GetVoxelCount(), stat1->GetOutput() );
}

Core::vtkImageDataPtr MultiLevelROIProcessor::ClipImage( Core::vtkImageDataPtr inGrayscaleVolume )
{
	int xBegin;
	int xEnd;
	int yBegin;
	int yEnd;
	int zBegin;
	int zEnd;
	
	int dimensions[3];
	inGrayscaleVolume->GetDimensions(dimensions);

	switch (m_Domain)
	{
	case 1: //3d
		{
			xBegin = 0;
			xEnd = dimensions[0];
			yBegin = 0;
			yEnd = dimensions[1];
			zBegin = 0;
			zEnd = dimensions[2];
		}
		break;
	case 2: //current slice x
		{
			xBegin = m_Pos;
			xEnd = m_Pos;
			yBegin = 0;
			yEnd = dimensions[1];
			zBegin = 0;
			zEnd = dimensions[2];
		}
		break;
	case 3: //current slice y
		{
			xBegin = 0;
			xEnd = dimensions[0];
			yBegin = m_Pos;
			yEnd = m_Pos;
			zBegin = 0;
			zEnd = dimensions[2];
		}
		break;
	case 4: //current slice z
		{
			xBegin = 0;
			xEnd = dimensions[0];
			yBegin = 0;
			yEnd = dimensions[1];
			zBegin = m_Pos;
			zEnd = m_Pos;
		}
		break;
	default:
		{
			xBegin = 0;
			xEnd = dimensions[0];
			yBegin = 0;
			yEnd = dimensions[1];
			zBegin = 0;
			zEnd = dimensions[2];
		}
		break;
	}

	if ( m_Domain != 1 )
	{
		vtkSmartPointer < vtkImageClip > imageClip = vtkSmartPointer< vtkImageClip >::New() ;
		imageClip->SetInput(inGrayscaleVolume);
		imageClip->ClipDataOn();
		imageClip->SetOutputWholeExtent(xBegin, xEnd, yBegin, yEnd, zBegin, zEnd);
		imageClip->Update();
		inGrayscaleVolume = imageClip->GetOutput();
	}

	return inGrayscaleVolume;
}

void MultiLevelROIProcessor::ComputeLevels( Core::DataEntity::Pointer dataEntity )
{
	if ( dataEntity.IsNull() )
	{
		return;
	}

	blTagMap::Pointer levels = dataEntity->GetMetadata()->GetTagValue<blTagMap::Pointer>( "MultiROILevel" );
	if ( levels.IsNotNull() && levels->GetLength() != 0 )
	{
		return;
	}

	// Compute min and max level
	int minLevel = INT_MAX;
	int maxLevel = -INT_MAX;
	for ( int timeStep = 0; timeStep < dataEntity->GetNumberOfTimeSteps(); timeStep++ )
	{
		Core::vtkImageDataPtr inLabelmapVolume;
		dataEntity->GetProcessingData( inLabelmapVolume, timeStep );

		vtkSmartPointer<vtkImageAccumulate> stat1 = vtkSmartPointer<vtkImageAccumulate>::New();
		stat1->SetInput(inLabelmapVolume);
		stat1->Update();

		if ( (stat1->GetVoxelCount()) > 0 ) 
		{
			minLevel = std::min( int( stat1->GetMin()[ 0 ] ), minLevel);
			maxLevel = std::max( int( stat1->GetMax()[ 0 ] ), maxLevel);
		}

	}

	// Update meta data
	levels = blTagMap::New( );
	for ( int i = minLevel; i <= maxLevel; i++ )
	{
		if ( i == 0 )
		{
			continue;
		}

		std::stringstream str;
		str << i;
		levels->AddTag( str.str(), i );
	}
	dataEntity->GetMetadata( )->AddTag( "MultiROILevel", levels );
}

bool MultiLevelROIProcessor::GetExportHistogram() const
{
	return m_ExportHistogram;
}

void MultiLevelROIProcessor::SetExportHistogram( bool val )
{
	m_ExportHistogram = val;
}

bool MultiLevelROIProcessor::GetExportSignal() const
{
	return m_ExportSignal;
}

void MultiLevelROIProcessor::SetExportSignal( bool val )
{
	m_ExportSignal = val;
}

bool MultiLevelROIProcessor::GetMeanAllLevels() const 
{ 
	return m_meanAllLevels; 
}

void MultiLevelROIProcessor::SetMeanAllLevels(bool val) 
{ 
	m_meanAllLevels = val; 
}

void MultiLevelROIProcessor::SetStatisticValue( const std::string value )
{
	m_StatisticValue = value;
}

std::string MultiLevelROIProcessor::GetStatisticValue( )
{
	return m_StatisticValue;
}

void MultiLevelROIProcessor::Update()
{
	if (m_meanAllLevels)
	{
		ExportMeanAllLevels();
	}
	else if ( m_ExportSignal )
	{
		ExportSignal( );
	}

	CalculateIntensityMeasures( GetTimeStep(), GetIntensityMeasures( ) );

	// Update output
	GetOutputPort( MEASUREMENTS )->UpdateOutput( GetIntensityMeasures( ), 0, GetInputDataEntity( 0 ) );
}

bool MultiLevelROIProcessor::GetNormalizeHistogramByVolume() const
{
	return m_NormalizeHistogramByVolume;
}

void MultiLevelROIProcessor::SetNormalizeHistogramByVolume( bool val )
{
	m_NormalizeHistogramByVolume = val;
}

void MultiLevelROIProcessor::UpdateHistogram( 
	long int totalVoxels, Core::vtkImageDataPtr histogramImage )
{
	if ( !histogramImage || !m_ExportHistogram )
	{
		return;
	}

	std::vector<blSignal::SampleType> values;
	int* dims = histogramImage->GetDimensions();
	for (int z=0; z<dims[2]; z++)
	{
		for (int y=0; y<dims[1]; y++)
		{
			for (int x=0; x<dims[0]; x++)
			{
				// zero is the component, add another loop if you have more
				// than one component
				double v = histogramImage->GetScalarComponentAsDouble(x,y,z,0);

				if ( m_NormalizeHistogramByVolume && totalVoxels != 0 )
				{
					v = v * 100 / totalVoxels;
				}

				// do something with v
				values.push_back( v );
			}
		}
	} 

	double range[2];
	histogramImage->GetScalarRange( range );

	// Update signal collective or create a new one
	blSignalCollective::Pointer collective;
	if ( GetOutputDataEntity(SIGNAL_HISTOGRAM).IsNotNull() )
	{
		GetOutputDataEntity(SIGNAL_HISTOGRAM)->GetProcessingData( collective );
	}
	else
	{
		collective = blSignalCollective::New( );
	}

	// Set signal/DataEntity name
	std::string imageName = GetInputDataEntity( INPUT_IMAGE )->GetMetadata()->GetName();
	std::string signalName = imageName;
	if ( GetInputDataEntity( INPUT_ROI ).IsNotNull() )
	{
		signalName = GetInputDataEntity( INPUT_ROI )->GetMetadata()->GetName();
	}

	// Set y units
	std::string yUnits;
	if( m_NormalizeHistogramByVolume )
	{
		yUnits = "Volume (%)";
	}
	else
	{
		yUnits = "count";
	}

	// check if signal was already computed
	blSignal::Pointer signal = collective->GetSignal( signalName );
	if ( signal.IsNull() )
	{
		signal = blSignal::Build( signalName, yUnits, values );
		collective->AddSignal( signal );
	}
	else
	{
		signal->SetYVector( values );
	}
	signal->SetStartTime( range[0] );
	signal->SetXUnit( "Gy" );

	// Create meta data for this signal
	blTagMap::Pointer metadata;
	blTagMap::Pointer rendering;
	blTagMap::Pointer signalRendering;
	if ( GetOutputDataEntity(SIGNAL_HISTOGRAM).IsNotNull() )
	{
		metadata = GetOutputDataEntity(SIGNAL_HISTOGRAM)->GetMetadata();
		rendering = metadata->GetTagValue<blTagMap::Pointer>( "Rendering" );
		signalRendering = rendering->GetTagValue<blTagMap::Pointer>( signalName );
		if ( signalRendering.IsNull() )
		{
			signalRendering = blTagMap::New( );
			rendering->AddTag( signalName, signalRendering );
		}
	}
	else
	{
		metadata = blTagMap::New( );
		rendering = blTagMap::New( );
		signalRendering = blTagMap::New( );
		rendering->AddTag( signalName, signalRendering );
		metadata->AddTag( "Rendering", rendering );
	}

	// Group all signals under the same window
	signalRendering->AddTag( "group", imageName );
	signalRendering->AddTag( "width", int(2) );
	blTagMap::Pointer legend = blTagMap::New( );
	legend->AddTag( "x position", int( 300 ) );
	signalRendering->AddTag( "Legend", legend );

	// Get color from mask image
	if ( GetInputDataEntity( INPUT_ROI ).IsNotNull() )
	{
		blTagMap::Pointer metadata(GetInputDataEntity( INPUT_ROI )->GetMetadata());
		blTagMap::Pointer rendering = metadata->GetTagValue<blTagMap::Pointer>( "Rendering" );
		blTag::Pointer tagColor = rendering->GetTag( "color" );
		std::string color;
		if ( tagColor.IsNotNull( ) )
		{
			signalRendering->AddTag("color", tagColor->GetValueAsString() );
		}
	}

	// Norify observers
	if ( GetOutputDataEntity(SIGNAL_HISTOGRAM).IsNotNull() )
	{

		GetOutputDataEntity(SIGNAL_HISTOGRAM)->GetMetadata()->AddTags( metadata );
		GetOutputDataEntity(SIGNAL_HISTOGRAM)->Modified();
	}
	else
	{
		UpdateOutput( SIGNAL_HISTOGRAM, collective, "Histogram " + imageName,
			true, 1, GetInputDataEntity( INPUT_IMAGE ), metadata );
	}

}