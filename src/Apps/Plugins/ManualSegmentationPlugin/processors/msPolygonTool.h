/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _msPolygonTool_H
#define _msPolygonTool_H

#include "mitkCommon.h"
#include "MitkExtExports.h"
#include "mitkFeedbackContourTool.h"
#include "mitkPointSetInteractor.h"
#include "msImageContourProcessor.h"

namespace mitk
{

/**
  \brief Simple polygon tool.

  User can add/move/remove points using a point set interactor. 

  Observers pointset:
  - Added new point: add point to feedback contour
  - Removed point: remove point from feedback contour
  - Moved point: move a point of feedback contour

  Events that are handled by this interactor:
  - Left mouse button: Update contour based on contour method or detect contour
  - Tab: change mode
  - Space: Paint contour feedback
  - Wheel mouse: translate points to next slice
  - Time: translate points to next slice

  On Activate and on Deactivate handle status of interactors/visible helper objects

  Usage:
  - First, the user adds point at the end of the list.
  - Once satisfied, user press tab key and the mode changes to modify points
  - Finally, user adds random points between each two points
  - User can always press Space to paint inner zone or Tab key to change mode

  \note 3D+T

  m_PointSet is a 3D+T object and we should always access each time step. When
  the user moves the current time step, points are propagated

\ingroup gmInteractors
\author Xavi Planes
\date Sept 2012
*/
class PLUGIN_EXPORT PolygonTool : public mitk::FeedbackContourTool
{
  public:
	  enum MODE_TYPE{
		  MODE_ADD_POINTS,
		  MODE_MOVE_POINTS
	  };
    
    mitkClassMacro(PolygonTool, mitk::FeedbackContourTool);
    itkNewMacro(PolygonTool);

    virtual const char** GetXPM() const;
    virtual const char* GetName() const;

	void SetPixelValue(int n);
	int GetPixelValue();

	//! Add an observer to ReferenceDataChanged message
	virtual void SetToolManager(ToolManager*);

	//!
	void SetMode( MODE_TYPE mode );

	//!
	void SetContourMethod( ImageContourProcessor::CONTOUR_METHOD_TYPE method );

	//!
	virtual void UpdateTimeStep(unsigned int timeStep);

  protected:
   
    PolygonTool(int paintingPixelValue = 1); // purposely hidden
    virtual ~PolygonTool();

	//! Add pointset interactor and show it
    virtual void Activated();
	//! Hide pointset and contour
    virtual void Deactivated();
    
	/** Observer of PointSetAddEvent
	If mode is MODE_ADD_POINTS, then UpdateContour( )
	If mode is MOVE_MOVE_POINTS, then insert new point to closest segment
	*/
	virtual void OnPointSetAddEvent( );
	//! Observer of PointSetMoveEvent. Update contour
    virtual void OnPointSetMoveEvent( );
	//! Observer of PointSetRemoveEvent. Reset all points indexes from 0 to N
    virtual void OnPointSetRemoveEvent( );

	/** If working image contains an already segmented region,
	Extract contour points from it
	*/
	virtual bool OnMousePressed (mitk::Action*, const mitk::StateEvent* stateEvent);
	//! Store event position
	virtual bool OnAddPoint(mitk::Action*, const mitk::StateEvent* stateEvent);	
	//! Close the contour, project it to the image slice and fill it in 2D.
    virtual bool OnMouseReleased(mitk::Action*, const mitk::StateEvent*);
	//! Update pointset position of affected dimension. Call OnMousePressed( )
    virtual bool OnMoveSlice    (mitk::Action*, const mitk::StateEvent*);
	//! Switch m_Mode
    virtual bool OnChangeMode   (mitk::Action*, const mitk::StateEvent*);

	//! Change visible properties of pointset
	void SetPointSetVisible(bool visible);

	//! Update point size
	void OnReferenceDataChanged( );

	//! Update contour from PointSet
	void UpdateContour( );

	//! Update PointSet from contour
	void UpdatePointSet( );

	//! Detect contour points on the current slice image
	bool DetectContourOnSliceImage( mitk::Image::Pointer slice );

  protected:
	//!
	int m_pixelValue;
	//!
	int m_PaintingPixelValue;
	//! Pointset
    mitk::PointSet::Pointer      m_PointSet;
	//! Node in rendering tree
    mitk::DataTreeNode::Pointer m_PointSetNode;
	//! Standard interactor
	mitk::PointSetInteractor::Pointer m_PointSetInteractor;
	//! Is pointset visible?
    bool                  m_PointSetVisible;
	//! Precision of m_PointSetInteractor
	float m_Precision;
	//! Interaction mode
	MODE_TYPE m_Mode;
	//! Last event position captured when user clicks on slice
	mitk::PositionEvent m_LastPositionEvent;
	//! Compute contour using different methods
	ImageContourProcessor::Pointer m_ContourProcessor;
	//! Slice image has changed from last use of m_ContourProcessor
	bool m_SliceImageChanged;
	//! Update contour for the next user interaction
	bool m_UpdateContour;

};

} // namespace

#endif // _msPolygonTool_H


