/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// CoreLib
#include "InteractiveSegmInteractor.h"

#include "coreAssert.h"
#include "coreException.h"
#include "coreEnvironment.h"
#include "coreLogger.h"
#include "coreKernel.h"
#include "coreReportExceptionMacros.h"
#include "coreDataTreeHelper.h"
#include "coreDataEntityHelper.h"
#include "coreVTKPolyDataHolder.h"
#include "coreVTKImageDataRenDataBuilder.h"
#include "coreRenderingTreeMITK.h"

// Mitk
#include "mitkRenderingManager.h"
#include "mitkSmartPointerProperty.h"
#include "mitkGlobalInteraction.h"
#include "mitkInteractionConst.h"
#include "mitkDataTreeNode.h"
#include "mitkDataTreeHelper.h"
#include "mitkVtkResliceInterpolationProperty.h"
#include "mitkDataStorage.h"
#include "mitkContourTool.h"
#include "mitkAddContourTool.h"
#include "mitkSubtractContourTool.h"
#include "mitkPaintbrushTool.h"
#include "mitkDrawPaintbrushTool.h"
#include "mitkErasePaintbrushTool.h"
#include "mitkCorrectorTool2D.h"
#include "mitkEraseRegionTool.h"
#include "mitkFillRegionTool.h"
#include "mitkRegionGrowingTool.h"
//#include "mitkRegionGrow3DTool.h"
#include "msPolygonTool.h"

#include "vtkSmartPointer.h"
#include "vtkImageMathematics.h"
#include "blMITKUtils.h"

using  namespace msp;

InteractiveSegmInteractor::TOOLS_TYPE InteractiveSegmInteractor::m_toolType = 
			InteractiveSegmInteractor::INVALID_TOOL;

/**
 */
InteractiveSegmInteractor::InteractiveSegmInteractor(
						Core::RenderingTree::Pointer renderingTree,
						Core::DataEntityHolder::Pointer selectedMask,
						Core::DataEntityHolder::Pointer selectedImage )

{
	try{

		SetSelectedMaskHolder( selectedMask );
		SetSelectedImageHolder( selectedImage );
		SetRenderingTree( renderingTree );
		m_brushSize = 1;
		}
		coreCatchExceptionsAddTraceAndThrowMacro(
		"InteractiveSegmInteractor::InteractiveSegmInteractor");


}


/**
 */
InteractiveSegmInteractor::~InteractiveSegmInteractor( )

{
	if ( m_selectedMaskHolder.IsNotNull() )
	{
		m_selectedMaskHolder->RemoveObserver<InteractiveSegmInteractor>(
			this, 
			&Self::OnMaskModified );
	}

	DisconnectFromDataTreeNode();

	DestroyInteractor();
}

void InteractiveSegmInteractor::ConnectToDataTreeNode( )
{
	// Check input image and input mask image
	Core::DataEntity::Pointer inputImageDataEntity = GetSelectedImageDataEntity();
	if ( inputImageDataEntity.IsNull( ) )
	{
		throw Core::Exceptions::Exception( 
			"InteractiveSegmInteractor", 
			"Please select Input Image" );
	}

	Core::DataEntity::Pointer maskDataEntity = GetSelectedMaskDataEntity();
	if ( maskDataEntity.IsNotNull( ) )
	{
		Core::vtkImageDataPtr vtkImage;
		if ( maskDataEntity->GetProcessingData( vtkImage, 0, false, true ) )
		{
			if ( vtkImage->GetScalarType( ) == VTK_FLOAT || 
				 vtkImage->GetScalarType( ) == VTK_DOUBLE )
			{
				throw Core::Exceptions::Exception( 
					"InteractiveSegmInteractor", 
					"Mask Image cannot be double or float" );
			}
		}
	}


	if ( !IsConnectedToRenderingTree (GetSelectedMaskHolder()->GetSubject()))
		ConnectNodeToTree();

	CreateInteractor();

	ConnectInteractors();

	// Call the subclass
	OnInteractorConnected( );


	// add data entity to list
	OnMaskModified();

	// Show the node after changing the size
	m_renderingTree->Show( GetSelectedMaskDataEntity(), true );
}

void InteractiveSegmInteractor::DisconnectFromDataTreeNode()
{
	try
	{
		DisconnectInteractors();

		//Commented out. We don't want to disconnect the countour from the rendering node when the interactor is distructed. MB
		//DisconnectNodeFromTree();

		// Don't destroy interactor because PolygonTool reuses same instance
		//DestroyInteractor();
	}
	coreCatchExceptionsReportAndNoThrowMacro(
		"InteractiveSegmInteractor::DisconnectFromDataTreeNode()");
}

void InteractiveSegmInteractor::ConnectNodeToTree()
{
	if (GetSelectedImageDataEntity()->GetType() != Core::ImageTypeId )
	{
		throw Core::Exceptions::Exception( 
			"InteractiveSegmInteractor", 
			"Input Data should be an image" );
	}
	// Hide the node, because the properties of the node (point size)
	//CreateEmptyWorkingData();

	//! add a check before doing the connections of the interactor
	if (GetSelectedMaskDataEntity().IsNull() ) return;

	// will be set by the interactor class
	m_renderingTree->Add( 
		GetSelectedMaskDataEntity( ), 
		false, 
		false );
}

void InteractiveSegmInteractor::DisconnectNodeFromTree()
{
	if ( GetSelectedMaskDataEntity().IsNotNull() )
			m_renderingTree->Remove( 
				GetSelectedMaskDataEntity( ),
				false );
}

void InteractiveSegmInteractor::ConnectInteractors()
{
	// MITK checks if the interactor is in the list. It will not be added twice
	m_toolManager->RegisterClient();				

	int toolId = GetCurrentToolId();
	m_toolManager->ActivateTool(toolId);

	//GetCurrentTool( )->SetPixelValue( GetPixelValue() );
	//GetCurrentTool( )->SetEraseOnlySelectedColor( GetEraseOnlySelectedColor() );
}

void InteractiveSegmInteractor::DisconnectInteractors()
{
	if( m_toolManager )
	{
		m_toolManager->UnregisterClient();
		m_toolManager->SetWorkingData(NULL);
		//!TODO change SetReferenceData function in mitk::toolmanager in order not to accept null 
		m_toolManager->SetReferenceData(NULL);
	}
}

void InteractiveSegmInteractor::CreateInteractor()
{
	if ( m_toolManager.IsNull() )
	{
		Core::RenderingTreeMITK* treeMITK = dynamic_cast<Core::RenderingTreeMITK*> ( m_renderingTree.GetPointer( ) );
		m_toolManager = mitk::ToolManager::New( treeMITK->GetDataStorage() );
	}
}

bool InteractiveSegmInteractor::IsConnectedToRenderingTree(Core::DataEntity::Pointer dataEntity)
{
	mitk::DataTreeIteratorClone	itFound;
	bool bDataIsInRenderingTree = false;
	
	if( GetRenderingTree().IsNull() )
		return false;

	bDataIsInRenderingTree = GetRenderingTree()->IsDataEntityRendered( dataEntity );

	return bDataIsInRenderingTree;
}

void InteractiveSegmInteractor::DestroyInteractor()
{
	m_toolManager = NULL ;
}


void InteractiveSegmInteractor::OnInteractorConnected()
{
		m_toolManager->SetReferenceData(GetSelectedImageRenderingNode());
		m_toolManager->SetWorkingData(GetSelectedMaskRenderingNode());			
}

Core::DataEntity::Pointer InteractiveSegmInteractor::GetSelectedMaskDataEntity()
{
	if ( m_selectedMaskHolder.IsNull( ) )
	{
		return NULL;
	}

	return m_selectedMaskHolder->GetSubject();
}

Core::DataEntity::Pointer InteractiveSegmInteractor::GetSelectedImageDataEntity()
{
	return m_selectedImageHolder->GetSubject();
}

mitk::DataTreeNode::Pointer InteractiveSegmInteractor::GetSelectedMaskRenderingNode()
{
	mitk::DataTreeNode::Pointer node;
	boost::any anyData = GetRenderingTree()->GetNode( GetSelectedMaskDataEntity( ) );
	Core::CastAnyProcessingData( anyData, node );
	return node;
}

mitk::DataTreeNode::Pointer InteractiveSegmInteractor::GetSelectedImageRenderingNode()
{
	mitk::DataTreeNode::Pointer node;
	boost::any anyData = GetRenderingTree()->GetNode( GetSelectedImageDataEntity( ) );
	Core::CastAnyProcessingData( anyData, node );
	return node;
}

mitk::Image::Pointer InteractiveSegmInteractor::GetSelectedMaskRenderingNodeAsImage()
{
	if ( GetSelectedMaskDataEntity( ).IsNull() )
	{
		throw Core::Exceptions::Exception( 
			"InteractiveSegmInteractor", 

			"Input Mask is NULL" );
	}
	mitk::BaseData::Pointer renderingData;
	boost::any anyData = GetSelectedMaskDataEntity( )->GetRenderingData( "MITK" );
	Core::CastAnyProcessingData( anyData, renderingData );
	mitk::Image* mask = NULL;
	if ( renderingData.IsNull() )
	{
		throw Core::Exceptions::Exception( 
			"InteractiveSegmInteractor", 

			"Input Mask rendering data is NULL" );
	}
	mask = dynamic_cast<mitk::Image*>(renderingData.GetPointer()); 
	if ( mask == NULL )
	{
		throw Core::Exceptions::Exception( 
			"InteractiveSegmInteractor", 

			"Input Mask rendering data is not correct" );
	}
	return mask;
}

Core::RenderingTree::Pointer InteractiveSegmInteractor::GetRenderingTree() const
{
	return m_renderingTree;
}

void InteractiveSegmInteractor::SetRenderingTree( Core::RenderingTree::Pointer val )
{
	m_renderingTree = val;
}

Core::DataEntityHolder::Pointer InteractiveSegmInteractor::GetSelectedMaskHolder() const
{
	return m_selectedMaskHolder;
}

void InteractiveSegmInteractor::SetSelectedMaskHolder( Core::DataEntityHolder::Pointer val )
{
	if ( m_selectedMaskHolder.IsNotNull() )
	{
		m_selectedMaskHolder->RemoveObserver<InteractiveSegmInteractor>(
			this, 
			&Self::OnMaskModified );
	}

	// Observers to rendering data
	mitk::Image::Pointer mitkData = NULL;
	try
	{
		mitkData = GetSelectedMaskRenderingNodeAsImage( );
	}
	catch ( ... )
	{
	}

	m_selectedMaskHolder = val;


	// Observers to processing data
	m_selectedMaskHolder->AddObserver<InteractiveSegmInteractor>(
		this, 
		&Self::OnMaskModified );
}


void InteractiveSegmInteractor::OnMaskModified( )
{
	if (GetSelectedMaskHolder().IsNull() ) return;
	if (GetSelectedMaskHolder()->GetSubject().IsNull() ) return;
    
	GetSelectedMaskHolder()->GetSubject()->SetFather(GetSelectedImageDataEntity());
	Core::DataEntityHelper::AddDataEntityToList( GetSelectedMaskHolder() , false);
	
}

Core::DataEntityHolder::Pointer InteractiveSegmInteractor::GetSelectedImageHolder() const
{
	return m_selectedImageHolder;
}

void InteractiveSegmInteractor::SetSelectedImageHolder( Core::DataEntityHolder::Pointer val )
{
	m_selectedImageHolder = val;
}

void InteractiveSegmInteractor::CreateEmptyWorkingData( )
{
	if ( GetSelectedImageHolder().IsNull() ||  GetSelectedMaskHolder().IsNull()) return;

	Core::vtkImageDataPtr WorkingImage; 
	Core::DataEntityHelper::GetProcessingData(GetSelectedMaskHolder(),
												WorkingImage);

	Core::vtkImageDataPtr ReferenceImage; 
	Core::DataEntityHelper::GetProcessingData(GetSelectedImageHolder(),
												ReferenceImage);

	WorkingImage->Initialize();
	vtkSmartPointer<vtkImageMathematics> operation = vtkSmartPointer<vtkImageMathematics>::New();
	operation->SetInput1(ReferenceImage);
	operation->SetInput2(ReferenceImage);
	operation->SetOperationToSubtract();
	operation->Update();
	WorkingImage->DeepCopy(operation->GetOutput());
}


//!
void InteractiveSegmInteractor::SetPixelValue(int n) 
{
	m_pixelValue = n;

	if((m_toolType == ADDTOOL) || (m_toolType == SUBTRACTTOOL))
	{
		mitk::ContourTool * cTool = dynamic_cast<mitk::ContourTool *>(GetCurrentTool());
		if(cTool!=NULL)
			cTool->SetPixelValue(m_pixelValue);
	}
}

//!
int InteractiveSegmInteractor::GetPixelValue() 
{
	return m_pixelValue;
}

void InteractiveSegmInteractor::SetEraseOnlySelectedColor(bool b) 
{
	m_bEraseOnlySelectedColor = b;

	//if ( GetCurrentTool( ) )

	//{
	//	GetCurrentTool( )->SetEraseOnlySelectedColor( GetEraseOnlySelectedColor() );
	//}
}

bool InteractiveSegmInteractor::GetEraseOnlySelectedColor() {
	return m_bEraseOnlySelectedColor;
}

mitk::Tool* InteractiveSegmInteractor::GetCurrentTool()
{
	if( !m_toolManager )
	{
		return NULL;
	}

	int toolId = GetCurrentToolId();
	
	mitk::Tool * tool= NULL;
	if(toolId>=0)
		tool = m_toolManager->GetToolById(toolId);
	
	return tool;
}

int InteractiveSegmInteractor::GetCurrentToolId()
{
	int toolId = -1;
	switch (m_toolType)
	{
	case CONTOURTOOL:
		toolId = m_toolManager->GetToolIdByToolType	<mitk::ContourTool>();
		break;

	case ADDTOOL:
		toolId = m_toolManager->GetToolIdByToolType	<mitk::AddContourTool>();
		break;

	case SUBTRACTTOOL:
		toolId = m_toolManager->GetToolIdByToolType	<mitk::SubtractContourTool>();
		break;
	
	case PAINTTOOL:
		toolId = m_toolManager->GetToolIdByToolType	<mitk::DrawPaintbrushTool>();
		break;

	case WIPETOOL:
		toolId = m_toolManager->GetToolIdByToolType	<mitk::ErasePaintbrushTool>();
		break;

	case CORRECTIONTOOL:
		toolId = m_toolManager->GetToolIdByToolType	<mitk::CorrectorTool2D>();
		break;

	case FILLREGIONTOOL:
		toolId = m_toolManager->GetToolIdByToolType	<mitk::FillRegionTool>();
		break;

	case ERASEREGIONTOOL:
		toolId = m_toolManager->GetToolIdByToolType	<mitk::EraseRegionTool>();
		break;

	case REGIONGROWINGTOOL:
		toolId = m_toolManager->GetToolIdByToolType	<mitk::RegionGrowingTool>();
		break;

	case POLYGONTOOL:
		toolId = m_toolManager->GetToolIdByToolType	<mitk::PolygonTool>();
		break;

	case LIVEWIRE:
		toolId = m_toolManager->GetToolIdByToolType	<mitk::PolygonTool>();
		break;

	case SPLINE:
		toolId = m_toolManager->GetToolIdByToolType	<mitk::PolygonTool>();
		break;
	}
	return toolId;
}



void InteractiveSegmInteractor::SetBrushSize(int val)
{
	m_brushSize = 1;
	if((m_toolType == PAINTTOOL) || (m_toolType == WIPETOOL))
	{
		mitk::PaintbrushTool * cTool = dynamic_cast<mitk::PaintbrushTool *>(GetCurrentTool());
		if(cTool!=NULL)
		{
			cTool->SetSize(val);
			m_brushSize = val;
		}
	}
}

int InteractiveSegmInteractor::GetBrushSize()
{
	return m_brushSize;
}