#ifndef GSPPROCESSORCOLLECTIVE_H
#define GSPPROCESSORCOLLECTIVE_H

// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "CILabNamespaceMacros.h"

namespace gsp{

class ProcessorCollective : public itk::LightObject
{
public:
	CILAB_ITK_CLASS(ProcessorCollective, itk::LightObject, itkFactorylessNewMacro)

	ProcessorCollective();

private:
	
private:

};

} // gsp


#endif //GSPPROCESSORCOLLECTIVE_H
