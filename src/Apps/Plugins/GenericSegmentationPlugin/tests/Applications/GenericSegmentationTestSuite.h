/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include <cxxtest/TestSuite.h>

#include "coreDirectory.h"
#include "coreKernel.h"
#include "CISTIBToolkit.h"
#include "wxMitkApp.h"

#include "GenericSegmentationGlobalFixture.h"
#include "OtsuSegmentationTest.h"
#include "ThresholdSegmentationTest.h"
#include "RegionGrowSegmentationTest.h"
#include "VtkConnectedThresholdSegmentationTest.h"

#define testOtsuSegmentation_ENABLED 0
#define testThresholdSegmentation_ENABLED 0
#define testRegionGrowSegmentation_ENABLED 0
#define testVtkConnectedThresholdSegmentation_ENABLED 1

#define anyGenericSegmentationTest_ENABLED ( \
	testOtsuSegmentation_ENABLED + \
	testThresholdSegmentation_ENABLED + \
	testRegionGrowSegmentation_ENABLED + \
	testVtkConnectedThresholdSegmentation_ENABLED )

/**
\brief Tests for generic segmentation
\ingroup GenericSegmentationPlugin
\author Albert Sanchez
\date 01 10 2010
*/

#define MAX_INPUT_FILES 1

BEGIN_TEST_INPUTS_SETS_ARRAY( GenericSegmentationTestsInputFiles, MAX_INPUT_FILES )
BEGIN_TEST_INPUTS_SET()
TEST_INPUTS_SET_ENTRY( "OtsuTestImage-3dra.vtk" )
END_TEST_INPUTS_SET()
BEGIN_TEST_INPUTS_SET()
TEST_INPUTS_SET_ENTRY( "ThresholdTest_Image.vtk" )
END_TEST_INPUTS_SET()
BEGIN_TEST_INPUTS_SET()
TEST_INPUTS_SET_ENTRY( "ThresholdTest_Image.vtk" )
END_TEST_INPUTS_SET()
BEGIN_TEST_INPUTS_SET()
TEST_INPUTS_SET_ENTRY( "ThresholdTest_Image.vtk" )
END_TEST_INPUTS_SET()
BEGIN_TEST_INPUTS_SET()
TEST_INPUTS_SET_ENTRY( "ThresholdTest_BinImage27max.vtk" )
END_TEST_INPUTS_SET()
BEGIN_TEST_INPUTS_SET()
TEST_INPUTS_SET_ENTRY( "ThresholdTest_BinImage2750.vtk" )
END_TEST_INPUTS_SET()
BEGIN_TEST_INPUTS_SET()
TEST_INPUTS_SET_ENTRY( "ThresholdTest_BinImagemid50.vtk" )
END_TEST_INPUTS_SET()
BEGIN_TEST_INPUTS_SET()
TEST_INPUTS_SET_ENTRY( "ThresholdTest_Image.vtk" )
END_TEST_INPUTS_SET()
BEGIN_TEST_INPUTS_SET()
TEST_INPUTS_SET_ENTRY( "ThresholdTest_Image.vtk" )
END_TEST_INPUTS_SET()
BEGIN_TEST_INPUTS_SET()
TEST_INPUTS_SET_ENTRY( "ThresholdTest_Image.vtk" )
END_TEST_INPUTS_SET()
BEGIN_TEST_INPUTS_SET()
TEST_INPUTS_SET_ENTRY( "RegionGrowTest_Image.vtk" )
END_TEST_INPUTS_SET()
BEGIN_TEST_INPUTS_SET()
TEST_INPUTS_SET_ENTRY( "VtkConnectedThresholdTest_Image.vtk" )
END_TEST_INPUTS_SET()
END_TEST_INPUTS_SETS_ARRAY()

// Indexes for each test files input set
const int g_GenericSegmentationTestInputFilesArrayIndexes[] =
{
	// Test 0 (testOtsu)
	0,
	// Test 1 (testThreshold1A)
	1,
	// Test 2 (testThreshold1B)
	2,
	// Test 3 (testThreshold1C)
	3,
	// Test 4 (testThreshold2A)
	4,
	// Test 5 (testThreshold2B)
	5,
	// Test 6 (testThreshold2C)
	6,
	// Test 7 (testThreshold3A)
	7,
	// Test 8 (testThreshold3B)
	8,
	// Test 9 (testThreshold3C)
	9,
	// Test 10 (testRegionGrow)
	10,
	// Test 11 (testVtkConnectedThreshold)
	11
};

#define MAX_OUTPUT_FILES 2

BEGIN_TEST_OUTPUTS_SETS_ARRAY( GenericSegmentationTestsReferenceOutputFiles, MAX_OUTPUT_FILES )
BEGIN_TEST_OUTPUTS_SET()
TEST_OUTPUTS_SET_ENTRY( "OtsuSegmentationRef.vtk", Core::SurfaceMeshTypeId )
TEST_OUTPUTS_SET_ENTRY( "OtsuImageRef.vtk", Core::ImageTypeId )
END_TEST_OUTPUTS_SET()
BEGIN_TEST_OUTPUTS_SET()
TEST_OUTPUTS_SET_ENTRY( "ThresholdTest_BinImage27max.vtk", Core::ImageTypeId )
END_TEST_OUTPUTS_SET()
BEGIN_TEST_OUTPUTS_SET()
TEST_OUTPUTS_SET_ENTRY( "ThresholdTest_BinImage2750.vtk", Core::ImageTypeId )
END_TEST_OUTPUTS_SET()
BEGIN_TEST_OUTPUTS_SET()
TEST_OUTPUTS_SET_ENTRY( "ThresholdTest_BinImagemid50.vtk", Core::ImageTypeId )
END_TEST_OUTPUTS_SET()
BEGIN_TEST_OUTPUTS_SET()
TEST_OUTPUTS_SET_ENTRY( "ThresholdTest_BinMesh27max.vtk", Core::SurfaceMeshTypeId )
END_TEST_OUTPUTS_SET()
BEGIN_TEST_OUTPUTS_SET()
TEST_OUTPUTS_SET_ENTRY( "ThresholdTest_BinMesh2750.vtk", Core::SurfaceMeshTypeId )
END_TEST_OUTPUTS_SET()
BEGIN_TEST_OUTPUTS_SET()
TEST_OUTPUTS_SET_ENTRY( "ThresholdTest_BinMeshmid50.vtk", Core::SurfaceMeshTypeId )
END_TEST_OUTPUTS_SET()
BEGIN_TEST_OUTPUTS_SET()
TEST_OUTPUTS_SET_ENTRY( "ThresholdTest_Mesh1-4.vtk", Core::SurfaceMeshTypeId )
END_TEST_OUTPUTS_SET()
BEGIN_TEST_OUTPUTS_SET()
TEST_OUTPUTS_SET_ENTRY( "ThresholdTest_Mesh2-4.vtk", Core::SurfaceMeshTypeId )
END_TEST_OUTPUTS_SET()
BEGIN_TEST_OUTPUTS_SET()
TEST_OUTPUTS_SET_ENTRY( "ThresholdTest_Mesh3-4.vtk", Core::SurfaceMeshTypeId )
END_TEST_OUTPUTS_SET()
BEGIN_TEST_OUTPUTS_SET()
TEST_OUTPUTS_SET_ENTRY( "RegionGrowTest_Image27max.vtk", Core::ImageTypeId )
TEST_OUTPUTS_SET_ENTRY( "RegionGrowTest_Mesh27max.vtk", Core::SurfaceMeshTypeId )
END_TEST_OUTPUTS_SET()
BEGIN_TEST_OUTPUTS_SET()
TEST_OUTPUTS_SET_ENTRY( "VtkConnectedThresholdTest_Image27max.vtk", Core::ImageTypeId )
TEST_OUTPUTS_SET_ENTRY( "VtkConnectedThresholdTest_Mesh27max.vtk", Core::SurfaceMeshTypeId )
END_TEST_OUTPUTS_SET()
END_TEST_OUTPUTS_SETS_ARRAY()

// Indexes for each test files input set
const int g_GenericSegmentationTestOutputFilesArrayIndexes[] =
{
	// Test 0 (testOtsu)
	0,
	// Test 1 (testThreshold1A)
	1,
	// Test 2 (testThreshold1B)
	2,
	// Test 3 (testThreshold1C)
	3,
	// Test 4 (testThreshold2A)
	4,
	// Test 5 (testThreshold2B)
	5,
	// Test 6 (testThreshold2C)
	6,
	// Test 7 (testThreshold3A)
	7,
	// Test 8 (testThreshold3B)
	8,
	// Test 9 (testThreshold3C)
	9,
	// Test 10 (testRegionGrow)
	10,
	// Test 11 (testVtkConnectedThreshold)
	11
};

class OtsuSegmentationTestApp : public CxxTest::TestSuite 
{

public:
	int m_iSetupCount;

	OtsuSegmentationTest m_OtsuTest;

	ThresholdSegmentationTest1A m_ThresholdTest1A;
	ThresholdSegmentationTest1B m_ThresholdTest1B;
	ThresholdSegmentationTest1C m_ThresholdTest1C;
	ThresholdSegmentationTest2A m_ThresholdTest2A;
	ThresholdSegmentationTest2B m_ThresholdTest2B;
	ThresholdSegmentationTest2C m_ThresholdTest2C;
	ThresholdSegmentationTest3A m_ThresholdTest3A;
	ThresholdSegmentationTest3B m_ThresholdTest3B;
	ThresholdSegmentationTest3C m_ThresholdTest3C;

	RegionGrowSegmentationTest m_RegionGrowTest;

	VtkConnectedThresholdSegmentationTest m_VtkConnectedThresholdTest;

	std::vector<Core::BaseTest*> m_Tests;
	std::vector<std::string> m_Names;

public:
	OtsuSegmentationTestApp( )
	{
		m_iSetupCount = 0;

		m_Tests.push_back(&m_OtsuTest);
		m_Names.push_back("Otsu");
		
		m_Tests.push_back(&m_ThresholdTest1A);
		m_Names.push_back("Threshold");
		m_Tests.push_back(&m_ThresholdTest1B);
		m_Names.push_back("Threshold");
		m_Tests.push_back(&m_ThresholdTest1C);
		m_Names.push_back("Threshold");
		m_Tests.push_back(&m_ThresholdTest2A);
		m_Names.push_back("Threshold");
		m_Tests.push_back(&m_ThresholdTest2B);
		m_Names.push_back("Threshold");
		m_Tests.push_back(&m_ThresholdTest2C);
		m_Names.push_back("Threshold");
		m_Tests.push_back(&m_ThresholdTest3A);
		m_Names.push_back("Threshold");
		m_Tests.push_back(&m_ThresholdTest3B);
		m_Names.push_back("Threshold");
		m_Tests.push_back(&m_ThresholdTest3C);
		m_Names.push_back("Threshold");

		m_Tests.push_back(&m_RegionGrowTest);
		m_Names.push_back("RegionGrow");

		m_Tests.push_back(&m_VtkConnectedThresholdTest);
		m_Names.push_back("VtkConnectedThreshold");
	}

	~OtsuSegmentationTestApp( )
	{
	}

#if( anyGenericSegmentationTest_ENABLED )
	void setUp()
	{
		// Prepare the test results file
		std::string m_testResults = m_Tests[m_iSetupCount]->PATH_TEST + "TestResults.csv";
		m_Tests[m_iSetupCount]->SetLogFileName( m_testResults );
		// Set test input data path
		std::string TestDataPath = 
			m_Tests[m_iSetupCount]->PATH_TEST_DATA + "GenericSegmentation" + Core::IO::SlashChar
			+ m_Names[m_iSetupCount] + Core::IO::SlashChar;
		// Set test reference output data path
		std::string TestReferenceOutputDataPath = 
			m_Tests[m_iSetupCount]->PATH_TEST_OUTPUT + "GenericSegmentation" + Core::IO::SlashChar
			+ m_Names[m_iSetupCount] + Core::IO::SlashChar;


		// Prepare the inputs
		int iCurrentTestInputFilesArrayIndex = g_GenericSegmentationTestInputFilesArrayIndexes[m_iSetupCount];
		m_Tests[m_iSetupCount]->AddInputFilenamesSet(
			&g_GenericSegmentationTestsInputFiles[iCurrentTestInputFilesArrayIndex][0],
			TestDataPath );

		int iCurrentTestOutputFilesArrayIndex = g_GenericSegmentationTestOutputFilesArrayIndexes[m_iSetupCount];
		m_Tests[m_iSetupCount]->AddReferenceOutputFilenamesSet(
			&g_GenericSegmentationTestsReferenceOutputFiles[iCurrentTestOutputFilesArrayIndex][0],
			TestReferenceOutputDataPath );

		m_iSetupCount++;
	}

	void tearDown()
	{
		m_OtsuTest.Clean();
	}
#endif	// #if( anyGenericSegmentationTest_ENABLED )


	void testOtsu( void )
	{
#if( testOtsuSegmentation_ENABLED )

		// Do the test
		m_OtsuTest.Update( );

		// Get the test result
		bool bTestPassed = m_OtsuTest.CheckOutput( );

		// Store the test result
		m_OtsuTest.StoreTestResult( "GenericSegmentationTestSuite", 0, "testOtsu", 0, bTestPassed );

		// Final CxxTest assert
		TS_ASSERT( bTestPassed );

#else
		std::cout << "testOtsuSegmentation DISABLED" << std::endl;
#endif
	}

	void testThreshold1A( void )
	{
#if( testThresholdSegmentation_ENABLED )

		// Do the test
		m_ThresholdTest1A.Update( );

		// Get the test result
		bool bTestPassed = m_ThresholdTest1A.CheckOutput( );

		// Store the test result
		m_ThresholdTest1A.StoreTestResult( "GenericSegmentationTestSuite", 0, "testThreshold1A", 0, bTestPassed );

		// Final CxxTest assert
		TS_ASSERT( bTestPassed );

#else
		std::cout << "testThresholdSegmentation DISABLED" << std::endl;
#endif
	}

	void testThreshold1B( void )
	{
#if( testThresholdSegmentation_ENABLED )

		// Do the test
		m_ThresholdTest1B.Update( );

		// Get the test result
		bool bTestPassed = m_ThresholdTest1B.CheckOutput( );

		// Store the test result
		m_ThresholdTest1B.StoreTestResult( "GenericSegmentationTestSuite", 0, "testThreshold1B", 0, bTestPassed );

		// Final CxxTest assert
		TS_ASSERT( bTestPassed );

#else
		std::cout << "testThresholdSegmentation DISABLED" << std::endl;
#endif
	}

	void testThreshold1C( void )
	{
#if( testThresholdSegmentation_ENABLED )

		// Do the test
		m_ThresholdTest1C.Update( );

		// Get the test result
		bool bTestPassed = m_ThresholdTest1C.CheckOutput( );

		// Store the test result
		m_ThresholdTest1C.StoreTestResult( "GenericSegmentationTestSuite", 0, "testThreshold1C", 0, bTestPassed );

		// Final CxxTest assert
		TS_ASSERT( bTestPassed );

#else
		std::cout << "testThresholdSegmentation DISABLED" << std::endl;
#endif
	}

	void testThreshold2A( void )
	{
#if( testThresholdSegmentation_ENABLED )

		// Do the test
		m_ThresholdTest2A.Update( );

		// Get the test result
		bool bTestPassed = m_ThresholdTest2A.CheckOutput( );

		// Store the test result
		m_ThresholdTest2A.StoreTestResult( "GenericSegmentationTestSuite", 0, "testThreshold2A", 0, bTestPassed );

		// Final CxxTest assert
		TS_ASSERT( bTestPassed );

#else
		std::cout << "testThresholdSegmentation DISABLED" << std::endl;
#endif
	}

	void testThreshold2B( void )
	{
#if( testThresholdSegmentation_ENABLED )

		// Do the test
		m_ThresholdTest2B.Update( );

		// Get the test result
		bool bTestPassed = m_ThresholdTest2B.CheckOutput( );

		// Store the test result
		m_ThresholdTest2B.StoreTestResult( "GenericSegmentationTestSuite", 0, "testThreshold2B", 0, bTestPassed );

		// Final CxxTest assert
		TS_ASSERT( bTestPassed );

#else
		std::cout << "testThresholdSegmentation DISABLED" << std::endl;
#endif
	}

	void testThreshold2C( void )
	{
#if( testThresholdSegmentation_ENABLED )

		// Do the test
		m_ThresholdTest2C.Update( );

		// Get the test result
		bool bTestPassed = m_ThresholdTest2C.CheckOutput( );

		// Store the test result
		m_ThresholdTest2C.StoreTestResult( "GenericSegmentationTestSuite", 0, "testThreshold2C", 0, bTestPassed );

		// Final CxxTest assert
		TS_ASSERT( bTestPassed );

#else
		std::cout << "testThresholdSegmentation DISABLED" << std::endl;
#endif
	}

	void testThreshold3A( void )
	{
#if( testThresholdSegmentation_ENABLED )

		// Do the test
		m_ThresholdTest3A.Update( );

		// Get the test result
		bool bTestPassed = m_ThresholdTest3A.CheckOutput( );

		// Store the test result
		m_ThresholdTest3A.StoreTestResult( "GenericSegmentationTestSuite", 0, "testThreshold3A", 0, bTestPassed );

		// Final CxxTest assert
		TS_ASSERT( bTestPassed );

#else
		std::cout << "testThresholdSegmentation DISABLED" << std::endl;
#endif
	}

	void testThreshold3B( void )
	{
#if( testThresholdSegmentation_ENABLED )

		// Do the test
		m_ThresholdTest3B.Update( );

		// Get the test result
		bool bTestPassed = m_ThresholdTest3B.CheckOutput( );

		// Store the test result
		m_ThresholdTest3B.StoreTestResult( "GenericSegmentationTestSuite", 0, "testThreshold3B", 0, bTestPassed );

		// Final CxxTest assert
		TS_ASSERT( bTestPassed );

#else
		std::cout << "testThresholdSegmentation DISABLED" << std::endl;
#endif
	}

	void testThreshold3C( void )
	{
#if( testThresholdSegmentation_ENABLED )

		// Do the test
		m_ThresholdTest3C.Update( );

		// Get the test result
		bool bTestPassed = m_ThresholdTest3C.CheckOutput( );

		// Store the test result
		m_ThresholdTest3C.StoreTestResult( "GenericSegmentationTestSuite", 0, "testThreshold3C", 0, bTestPassed );

		// Final CxxTest assert
		TS_ASSERT( bTestPassed );

#else
		std::cout << "testThresholdSegmentation DISABLED" << std::endl;
#endif
	}

	void testRegionGrow( void )
	{
#if( testRegionGrowSegmentation_ENABLED )

		// Do the test
		m_RegionGrowTest.Update( );

		// Get the test result
		bool bTestPassed = m_RegionGrowTest.CheckOutput( );

		// Store the test result
		m_RegionGrowTest.StoreTestResult( "GenericSegmentationTestSuite", 0, "testRegionGrow", 0, bTestPassed );

		// Final CxxTest assert
		TS_ASSERT( bTestPassed );

#else
		std::cout << "testRegionGrowSegmentation DISABLED" << std::endl;
#endif
	}

	void testVtkConnectedThreshold( void )
	{
#if( testVtkConnectedThresholdSegmentation_ENABLED )

		// Do the test
		m_VtkConnectedThresholdTest.Update( );

		// Get the test result
		bool bTestPassed = m_VtkConnectedThresholdTest.CheckOutput( );

		// Store the test result
		m_VtkConnectedThresholdTest.StoreTestResult( "GenericSegmentationTestSuite", 0, "testVtkConnectedThreshold", 0, bTestPassed );

		// Final CxxTest assert
		TS_ASSERT( bTestPassed );

#else
		std::cout << "testVtkConnectedThreshold DISABLED" << std::endl;
#endif
	}


protected:

};
