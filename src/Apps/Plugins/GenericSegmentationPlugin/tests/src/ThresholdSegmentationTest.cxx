/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "ThresholdSegmentationTest.h"

#include "coreDataEntityReader.h"
#include "coreDataEntityWriter.h"

#include "boost/format.hpp"


/*
* ====================================================
* ========== THRESHOLD SEGMENTATION TEST 1A ==========
* ====================================================
*/



ThresholdSegmentationTest1A::ThresholdSegmentationTest1A(  )
{
	m_Processor = gsp::ThresholdProcessor::New( );
}

ThresholdSegmentationTest1A::~ThresholdSegmentationTest1A( )
{
}

void ThresholdSegmentationTest1A::Update( )
{
	// Read input image
	Core::IO::DataEntityReader::Pointer reader;
	reader = Core::IO::DataEntityReader::New( );
	reader->SetFileNames( GetInputFilenames( 0 ) );
	reader->Update();

	m_Processor->SetInputDataEntity( 0, reader->GetOutputDataEntity( ) );
	m_Processor->m_flag = 0;

	Core::vtkImageDataPtr imageData;
	m_Processor->GetInputDataEntity(0)->GetProcessingData(imageData);	

	double _max,_min;

	if ( imageData != NULL )
	{
		_max = imageData->GetScalarRange()[1]; 
		_min = imageData->GetScalarRange()[0]; 
	}

	m_Processor->SetUpperThreshold(_max);
	m_Processor->SetLowerThreshold(27000);
	m_Processor->Update();

	// Write output
	Core::IO::DataEntityWriter::Pointer writer;
	writer = Core::IO::DataEntityWriter::New( );
	writer->SetFileNames( GetOutputFilenames( 0 ) );
	writer->SetInputDataEntity( 0, m_Processor->GetOutputDataEntity( 0 ) );
	writer->Update( );
}

void ThresholdSegmentationTest1A::SetParameters( double LowerThreshold, double UpperThreshold )
{
	m_Processor->SetLowerThreshold(LowerThreshold);
	m_Processor->SetUpperThreshold(UpperThreshold);
}


/*
* ====================================================
* ========== THRESHOLD SEGMENTATION TEST 1B ==========
* ====================================================
*/



ThresholdSegmentationTest1B::ThresholdSegmentationTest1B(  )
{
	m_Processor = gsp::ThresholdProcessor::New( );
}

ThresholdSegmentationTest1B::~ThresholdSegmentationTest1B( )
{
}

void ThresholdSegmentationTest1B::Update( )
{
	// Read input image
	Core::IO::DataEntityReader::Pointer reader;
	reader = Core::IO::DataEntityReader::New( );
	reader->SetFileNames( GetInputFilenames( 0 ) );
	reader->Update();

	m_Processor->SetInputDataEntity( 0, reader->GetOutputDataEntity( ) );
	m_Processor->m_flag = 0;

	Core::vtkImageDataPtr imageData;
	m_Processor->GetInputDataEntity(0)->GetProcessingData(imageData);	

	m_Processor->SetUpperThreshold(50000);
	m_Processor->SetLowerThreshold(27000);
	m_Processor->Update();

	// Write output
	Core::IO::DataEntityWriter::Pointer writer;
	writer = Core::IO::DataEntityWriter::New( );
	writer->SetFileNames( GetOutputFilenames( 0 ) );
	writer->SetInputDataEntity( 0, m_Processor->GetOutputDataEntity( 0 ) );
	writer->Update( );
}

void ThresholdSegmentationTest1B::SetParameters( double LowerThreshold, double UpperThreshold )
{
	m_Processor->SetLowerThreshold(LowerThreshold);
	m_Processor->SetUpperThreshold(UpperThreshold);
}



/*
* ====================================================
* ========== THRESHOLD SEGMENTATION TEST 1C ==========
* ====================================================
*/



ThresholdSegmentationTest1C::ThresholdSegmentationTest1C(  )
{
	m_Processor = gsp::ThresholdProcessor::New( );
}

ThresholdSegmentationTest1C::~ThresholdSegmentationTest1C( )
{
}

void ThresholdSegmentationTest1C::Update( )
{
	// Read input image
	Core::IO::DataEntityReader::Pointer reader;
	reader = Core::IO::DataEntityReader::New( );
	reader->SetFileNames( GetInputFilenames( 0 ) );
	reader->Update();

	m_Processor->SetInputDataEntity( 0, reader->GetOutputDataEntity( ) );
	m_Processor->m_flag = 0;

	Core::vtkImageDataPtr imageData;
	m_Processor->GetInputDataEntity(0)->GetProcessingData(imageData);	

	double _max,_min;

	if ( imageData != NULL )
	{
		_max = imageData->GetScalarRange()[1]; 
		_min = imageData->GetScalarRange()[0]; 
	}

	m_Processor->SetUpperThreshold(50000);
	m_Processor->SetLowerThreshold(32768);
	m_Processor->Update();

	// Write output
	Core::IO::DataEntityWriter::Pointer writer;
	writer = Core::IO::DataEntityWriter::New( );
	writer->SetFileNames( GetOutputFilenames( 0 ) );
	writer->SetInputDataEntity( 0, m_Processor->GetOutputDataEntity( 0 ) );
	writer->Update( );	
}

void ThresholdSegmentationTest1C::SetParameters( double LowerThreshold, double UpperThreshold )
{
	m_Processor->SetLowerThreshold(LowerThreshold);
	m_Processor->SetUpperThreshold(UpperThreshold);
}


/*
* ====================================================
* ========== THRESHOLD SEGMENTATION TEST 2A ==========
* ====================================================
*/




ThresholdSegmentationTest2A::ThresholdSegmentationTest2A(  )
{
	m_Processor = gsp::ThresholdProcessor::New( );
}

ThresholdSegmentationTest2A::~ThresholdSegmentationTest2A( )
{
}

void ThresholdSegmentationTest2A::Update( )
{
	//Read input image
	Core::IO::DataEntityReader::Pointer reader2;
	reader2 = Core::IO::DataEntityReader::New( );
	reader2->SetFileNames( GetInputFilenames( 0 ) );
	reader2->Update();
	
	m_Processor->SetInputDataEntity( 0, reader2->GetOutputDataEntity( ) );
	m_Processor->m_flag = 1;
	m_Processor->SetIsoValue(1);
	m_Processor->Update();
	
	//Write output mesh
	Core::IO::DataEntityWriter::Pointer writer2;
	writer2 = Core::IO::DataEntityWriter::New( );
	writer2->SetFileNames( GetOutputFilenames( 0 ) );
	writer2->SetInputDataEntity( 0, m_Processor->GetOutputDataEntity( 1 ) );
	writer2->Update( );
}

void ThresholdSegmentationTest2A::SetParameters( double LowerThreshold, double UpperThreshold )
{
	m_Processor->SetLowerThreshold(LowerThreshold);
	m_Processor->SetUpperThreshold(UpperThreshold);
}



/*
* ====================================================
* ========== THRESHOLD SEGMENTATION TEST 2B ==========
* ====================================================
*/



ThresholdSegmentationTest2B::ThresholdSegmentationTest2B(  )
{
	m_Processor = gsp::ThresholdProcessor::New( );
}

ThresholdSegmentationTest2B::~ThresholdSegmentationTest2B( )
{
}

void ThresholdSegmentationTest2B::Update( )
{
	//Read input image
	Core::IO::DataEntityReader::Pointer reader2;
	reader2 = Core::IO::DataEntityReader::New( );
	reader2->SetFileNames( GetInputFilenames( 0 ) );
	reader2->Update();

	m_Processor->SetInputDataEntity( 0, reader2->GetOutputDataEntity( ) );
	m_Processor->m_flag = 1;
	m_Processor->SetIsoValue(1);
	m_Processor->Update();

	//Write output mesh
	Core::IO::DataEntityWriter::Pointer writer2;
	writer2 = Core::IO::DataEntityWriter::New( );
	writer2->SetFileNames( GetOutputFilenames( 0 ) );
	writer2->SetInputDataEntity( 0, m_Processor->GetOutputDataEntity( 1 ) );
	writer2->Update( );
}

void ThresholdSegmentationTest2B::SetParameters( double LowerThreshold, double UpperThreshold )
{
	m_Processor->SetLowerThreshold(LowerThreshold);
	m_Processor->SetUpperThreshold(UpperThreshold);
}


/*
* ====================================================
* ========== THRESHOLD SEGMENTATION TEST 2C ==========
* ====================================================
*/



ThresholdSegmentationTest2C::ThresholdSegmentationTest2C(  )
{
	m_Processor = gsp::ThresholdProcessor::New( );
}

ThresholdSegmentationTest2C::~ThresholdSegmentationTest2C( )
{
}

void ThresholdSegmentationTest2C::Update( )
{
	//Read input image
	Core::IO::DataEntityReader::Pointer reader2;
	reader2 = Core::IO::DataEntityReader::New( );
	reader2->SetFileNames( GetInputFilenames( 0 ) );
	reader2->Update();

	m_Processor->SetInputDataEntity( 0, reader2->GetOutputDataEntity( ) );
	m_Processor->m_flag = 1;
	m_Processor->SetIsoValue(1);
	m_Processor->Update();

	//Write output mesh
	Core::IO::DataEntityWriter::Pointer writer2;
	writer2 = Core::IO::DataEntityWriter::New( );
	writer2->SetFileNames( GetOutputFilenames( 0 ) );
	writer2->SetInputDataEntity( 0, m_Processor->GetOutputDataEntity( 1 ) );
	writer2->Update( );
}

void ThresholdSegmentationTest2C::SetParameters( double LowerThreshold, double UpperThreshold )
{
	m_Processor->SetLowerThreshold(LowerThreshold);
	m_Processor->SetUpperThreshold(UpperThreshold);
}



/*
* ====================================================
* ========== THRESHOLD SEGMENTATION TEST 3A ==========
* ====================================================
*/



ThresholdSegmentationTest3A::ThresholdSegmentationTest3A(  )
{
	m_Processor = gsp::ThresholdProcessor::New( );
}

ThresholdSegmentationTest3A::~ThresholdSegmentationTest3A( )
{
}

void ThresholdSegmentationTest3A::Update( )
{
	//Read input image
	Core::IO::DataEntityReader::Pointer reader2;
	reader2 = Core::IO::DataEntityReader::New( );
	reader2->SetFileNames( GetInputFilenames( 0 ) );
	reader2->Update();

	m_Processor->SetInputDataEntity( 0, reader2->GetOutputDataEntity( ) );
	m_Processor->m_flag = 1;
	m_Processor->SetIsoValue(16384);
	m_Processor->Update();

	//Write output mesh
	Core::IO::DataEntityWriter::Pointer writer2;
	writer2 = Core::IO::DataEntityWriter::New( );
	writer2->SetFileNames( GetOutputFilenames( 0 ) );
	writer2->SetInputDataEntity( 0, m_Processor->GetOutputDataEntity( 1 ) );
	writer2->Update( );
}

void ThresholdSegmentationTest3A::SetParameters( double LowerThreshold, double UpperThreshold )
{
	m_Processor->SetLowerThreshold(LowerThreshold);
	m_Processor->SetUpperThreshold(UpperThreshold);
}


/*
* ====================================================
* ========== THRESHOLD SEGMENTATION TEST 3B ==========
* ====================================================
*/


ThresholdSegmentationTest3B::ThresholdSegmentationTest3B(  )
{
	m_Processor = gsp::ThresholdProcessor::New( );
}

ThresholdSegmentationTest3B::~ThresholdSegmentationTest3B( )
{
}

void ThresholdSegmentationTest3B::Update( )
{
	//Read input image
	Core::IO::DataEntityReader::Pointer reader2;
	reader2 = Core::IO::DataEntityReader::New( );
	reader2->SetFileNames( GetInputFilenames( 0 ) );
	reader2->Update();

	m_Processor->SetInputDataEntity( 0, reader2->GetOutputDataEntity( ) );
	m_Processor->m_flag = 1;
	m_Processor->SetIsoValue(32768);
	m_Processor->Update();

	//Write output mesh
	Core::IO::DataEntityWriter::Pointer writer2;
	writer2 = Core::IO::DataEntityWriter::New( );
	writer2->SetFileNames( GetOutputFilenames( 0 ) );
	writer2->SetInputDataEntity( 0, m_Processor->GetOutputDataEntity( 1 ) );
	writer2->Update( );
}

void ThresholdSegmentationTest3B::SetParameters( double LowerThreshold, double UpperThreshold )
{
	m_Processor->SetLowerThreshold(LowerThreshold);
	m_Processor->SetUpperThreshold(UpperThreshold);
}


/*
* ====================================================
* ========== THRESHOLD SEGMENTATION TEST 3C ==========
* ====================================================
*/


ThresholdSegmentationTest3C::ThresholdSegmentationTest3C(  )
{
	m_Processor = gsp::ThresholdProcessor::New( );
}

ThresholdSegmentationTest3C::~ThresholdSegmentationTest3C( )
{
}

void ThresholdSegmentationTest3C::Update( )
{
	//Read input image
	Core::IO::DataEntityReader::Pointer reader2;
	reader2 = Core::IO::DataEntityReader::New( );
	reader2->SetFileNames( GetInputFilenames( 0 ) );
	reader2->Update();

	m_Processor->SetInputDataEntity( 0, reader2->GetOutputDataEntity( ) );
	m_Processor->m_flag = 1;
	m_Processor->SetIsoValue(49151);
	m_Processor->Update();

	//Write output mesh
	Core::IO::DataEntityWriter::Pointer writer2;
	writer2 = Core::IO::DataEntityWriter::New( );
	writer2->SetFileNames( GetOutputFilenames( 0 ) );
	writer2->SetInputDataEntity( 0, m_Processor->GetOutputDataEntity( 1 ) );
	writer2->Update( );
}

void ThresholdSegmentationTest3C::SetParameters( double LowerThreshold, double UpperThreshold )
{
	m_Processor->SetLowerThreshold(LowerThreshold);
	m_Processor->SetUpperThreshold(UpperThreshold);
}
