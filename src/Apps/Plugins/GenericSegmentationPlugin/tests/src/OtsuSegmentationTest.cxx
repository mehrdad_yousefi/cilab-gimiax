/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "OtsuSegmentationTest.h"

#include "coreDataEntityReader.h"
#include "coreDataEntityWriter.h"

#include "boost/format.hpp"


OtsuSegmentationTest::OtsuSegmentationTest(  )
{
	m_Processor = gsp::OtsuProcessor::New( );
}

OtsuSegmentationTest::~OtsuSegmentationTest( )
{
}

void OtsuSegmentationTest::Update( )
{
	// Read input image
	Core::IO::DataEntityReader::Pointer reader;
	reader = Core::IO::DataEntityReader::New( );

	reader->SetFileNames( GetInputFilenames( 0 ) );
	reader->Update();
	//reader->GetOutputDataEntity( )->GetMetadata( )->SetModality( Core::CT );
	m_Processor->SetInputDataEntity( 0, reader->GetOutputDataEntity( ) );
	// update
	m_Processor->Update();

	// Write output
	Core::IO::DataEntityWriter::Pointer writer;
	writer = Core::IO::DataEntityWriter::New( );
	writer->SetFileNames( GetOutputFilenames( 0 ) );
	writer->SetInputDataEntity( 0, m_Processor->GetOutputDataEntity( 1 ) );
	writer->Update( );
	
	Core::IO::DataEntityWriter::Pointer writer2;
	writer2 = Core::IO::DataEntityWriter::New( );
	writer2->SetFileNames( GetOutputFilenames( 1 ) );
	writer2->SetInputDataEntity( 0, m_Processor->GetOutputDataEntity( 0 ) );
	writer2->Update( );
}

