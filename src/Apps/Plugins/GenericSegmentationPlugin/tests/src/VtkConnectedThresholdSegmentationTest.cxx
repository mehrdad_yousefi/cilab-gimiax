/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "VtkConnectedThresholdSegmentationTest.h"
#include "coreDataEntityReader.h"
#include "coreDataEntityWriter.h"
#include "coreDataEntityHelper.h"
#include "boost/format.hpp"

VtkConnectedThresholdSegmentationTest::VtkConnectedThresholdSegmentationTest(  )
{
	m_Processor = gsp::VtkConnectedThresholdProcessor::New( );
}

VtkConnectedThresholdSegmentationTest::~VtkConnectedThresholdSegmentationTest( )
{
}

void VtkConnectedThresholdSegmentationTest::Update( )
{
	// Read Input Image
	Core::IO::DataEntityReader::Pointer readerImage;
	readerImage = Core::IO::DataEntityReader::New( );
	readerImage->SetFileNames( GetInputFilenames( 0 ) );
	readerImage->Update();

	// Prepare parameters
	double LowerThreshold = 27000.0;
	double UpperThreshold = 65535.0;

	ConnectedThresholdParameters params;

	params.lowerThreshold = LowerThreshold;
	params.upperThreshold = UpperThreshold;

	m_Processor->SetParameters(&params);
	
	double xCoord = 2.991;
	double yCoord = 2.262;
	double zCoord = 4.258;

	Core::vtkPolyDataPtr seedPoint = vtkPolyData::New();
	vtkSmartPointer< vtkPoints> point = vtkSmartPointer<vtkPoints>::New();
	point->InsertPoint(0, xCoord/100.0 ,yCoord/100.0, zCoord/100.0);
	seedPoint->SetPoints(point);
	Core::DataEntity::Pointer dataEntity = Core::DataEntityFactory::Build(
		seedPoint,
		Core::DataEntityFactory::VtkPointSet);

	// Set inputs and call update
	m_Processor->SetInputDataEntity( gsp::VtkConnectedThresholdProcessor::INPUT_IMAGE, readerImage->GetOutputDataEntity( ) );
	m_Processor->SetInputDataEntity( gsp::VtkConnectedThresholdProcessor::INPUT_SEED_POINT, dataEntity );
	m_Processor->Update();
	
	// Write output image
	Core::IO::DataEntityWriter::Pointer writerImage;
	writerImage = Core::IO::DataEntityWriter::New( );
	writerImage->SetFileNames( GetOutputFilenames( 0 ) );
	writerImage->SetInputDataEntity( 0, m_Processor->GetOutputDataEntity( 0 ) );
	writerImage->Update( );

	// Write output mesh
	Core::IO::DataEntityWriter::Pointer writerMesh;
	writerMesh = Core::IO::DataEntityWriter::New( );
	writerMesh->SetFileNames( GetOutputFilenames( 1 ) );
	writerMesh->SetInputDataEntity( 0, m_Processor->GetOutputDataEntity( 1 ) );
	writerMesh->Update( );

}
