/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "cxxtest/GlobalFixture.h"

/**
\brief Global fixture class for Tests for GenericSegmentationPlugin
\ingroup GenericSegmentationPlugin
\author Albert Sanchez
\date 01 10 2010
*/

class GenericSegmentationGlobalFixture : public CxxTest::GlobalFixture
{
public:
	bool setUpWorld()
	{
		try
		{
			std::string appname = CISTIB_TOOLKIT_BUILD_FOLDER + std::string( "/bin/Debug/gimias.exe" );
			Core::Runtime::Kernel::Initialize( NULL, Core::Runtime::Console, appname );
		}
		catch( ... )
		{
			return false;
		}

		return true;
	}

	bool tearDownWorld()
	{
		try
		{
			Core::Runtime::Kernel::Terminate();
			mitk::wxMitkApp::CleanUpMITK();
		}
		catch( ... )
		{
			return false;
		}
		
		return true;
	}
};

static GenericSegmentationGlobalFixture genericSegmentationGlobalFixture;

