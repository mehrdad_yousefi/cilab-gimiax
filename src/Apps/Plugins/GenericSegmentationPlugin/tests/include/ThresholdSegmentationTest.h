/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#pragma once

#include "coreBaseTest.h"
#include "ThresholdProcessor.h"


/**
\brief Tests for Threshold Segmentation processor

\ingroup GenericSegmentationPlugin
\author Albert Sanchez
\date 07 10 2010
*/

class ThresholdSegmentationTest1A : public Core::BaseTest
{
public:
	ThresholdSegmentationTest1A( );
	~ThresholdSegmentationTest1A( );
	//!
	void Update( );
	//!
	void SetParameters( double LowerThreshold, double UpperThreshold);
protected:
	//!
	gsp::ThresholdProcessor::Pointer m_Processor;

};


class ThresholdSegmentationTest1B : public Core::BaseTest
{
public:
	ThresholdSegmentationTest1B( );
	~ThresholdSegmentationTest1B( );
	//!
	void Update( );
	//!
	void SetParameters( double LowerThreshold, double UpperThreshold);
protected:
	//!
	gsp::ThresholdProcessor::Pointer m_Processor;

};


class ThresholdSegmentationTest1C : public Core::BaseTest
{
public:
	ThresholdSegmentationTest1C( );
	~ThresholdSegmentationTest1C( );
	//!
	void Update( );
	//!
	void SetParameters( double LowerThreshold, double UpperThreshold);
protected:
	//!
	gsp::ThresholdProcessor::Pointer m_Processor;

};


class ThresholdSegmentationTest2A : public Core::BaseTest
{
public:
	ThresholdSegmentationTest2A( );
	~ThresholdSegmentationTest2A( );
	//!
	void Update( );
	//!
	void SetParameters( double LowerThreshold, double UpperThreshold);
protected:
	//!
	gsp::ThresholdProcessor::Pointer m_Processor;

};


class ThresholdSegmentationTest2B : public Core::BaseTest
{
public:
	ThresholdSegmentationTest2B( );
	~ThresholdSegmentationTest2B( );
	//!
	void Update( );
	//!
	void SetParameters( double LowerThreshold, double UpperThreshold);
protected:
	//!
	gsp::ThresholdProcessor::Pointer m_Processor;

};


class ThresholdSegmentationTest2C : public Core::BaseTest
{
public:
	ThresholdSegmentationTest2C( );
	~ThresholdSegmentationTest2C( );
	//!
	void Update( );
	//!
	void SetParameters( double LowerThreshold, double UpperThreshold);
protected:
	//!
	gsp::ThresholdProcessor::Pointer m_Processor;

};


class ThresholdSegmentationTest3A : public Core::BaseTest
{
public:
	ThresholdSegmentationTest3A( );
	~ThresholdSegmentationTest3A( );
	//!
	void Update( );
	//!
	void SetParameters( double LowerThreshold, double UpperThreshold);
protected:
	//!
	gsp::ThresholdProcessor::Pointer m_Processor;

};


class ThresholdSegmentationTest3B : public Core::BaseTest
{
public:
	ThresholdSegmentationTest3B( );
	~ThresholdSegmentationTest3B( );
	//!
	void Update( );
	//!
	void SetParameters( double LowerThreshold, double UpperThreshold);
protected:
	//!
	gsp::ThresholdProcessor::Pointer m_Processor;

};


class ThresholdSegmentationTest3C : public Core::BaseTest
{
public:
	ThresholdSegmentationTest3C( );
	~ThresholdSegmentationTest3C( );
	//!
	void Update( );
	//!
	void SetParameters( double LowerThreshold, double UpperThreshold);
protected:
	//!
	gsp::ThresholdProcessor::Pointer m_Processor;

};

