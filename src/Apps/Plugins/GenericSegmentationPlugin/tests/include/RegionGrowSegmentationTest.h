/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#pragma once

#include "coreBaseTest.h"
#include "RegionGrowProcessor.h"


/**
\brief Tests for Region Grow Segmentation
\ingroup GenericSegmentationPlugin
\author Albert Sanchez
\date 07 10 2010
*/

class RegionGrowSegmentationTest : public Core::BaseTest
{

public:
	RegionGrowSegmentationTest( );
	~RegionGrowSegmentationTest( );
	//!
	void Update( );

protected:
	//!
	gsp::RegionGrowProcessor::Pointer m_Processor;

};

