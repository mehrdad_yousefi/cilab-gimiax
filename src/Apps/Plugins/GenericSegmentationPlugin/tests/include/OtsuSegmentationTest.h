/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#pragma once

#include "coreBaseTest.h"
#include "OtsuProcessor.h"


/**
\brief Tests for Otsu Segmentation processor

\ingroup GenericSegmentationPlugin
\author Chiara Riccobene
\date 5 10 2009
*/
class OtsuSegmentationTest : public Core::BaseTest
{

public:

	OtsuSegmentationTest( );

	~OtsuSegmentationTest( );

	//!
	void Update( );

protected:

	//!
	gsp::OtsuProcessor::Pointer m_Processor;

};

