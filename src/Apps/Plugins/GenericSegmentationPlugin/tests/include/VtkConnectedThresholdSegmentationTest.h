/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#pragma once

#include "coreBaseTest.h"
#include "VtkConnectedThresholdProcessor.h"


/**
\brief Tests for Vtk Connected Threshold Segmentation
\ingroup GenericSegmentationPlugin
\author Albert Sanchez
\date 07 10 2010
*/

class VtkConnectedThresholdSegmentationTest : public Core::BaseTest
{

public:
	VtkConnectedThresholdSegmentationTest( );
	~VtkConnectedThresholdSegmentationTest( );
	//!
	void Update( );

protected:
	//!
	gsp::VtkConnectedThresholdProcessor::Pointer m_Processor;

};

