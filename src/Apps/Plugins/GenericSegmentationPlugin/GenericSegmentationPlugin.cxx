// Copyright 2006 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
       #include <wx/wx.h>
#endif

#include "GenericSegmentationPlugin.h"
#include <coreReportExceptionMacros.h>
#include "coreWxMitkCoreMainWindow.h"
#include "corePluginTab.h"
#include "coreWxMitkGraphicalInterface.h"

using namespace Core::Plugins;
using namespace Core::Widgets;

// Declaration of the plugin
coreBeginDefinePluginMacro(GenericSegmentationPlugin)
coreEndDefinePluginMacro()


GenericSegmentationPlugin::GenericSegmentationPlugin(void)
: FrontEndPlugin()
{
	try
	{
		m_Processors = gsp::ProcessorCollective::New();
		m_WidgetCollective = gsp::WidgetCollective::New();
	}
	coreCatchExceptionsReportAndNoThrowMacro(GenericSegmentationPlugin::GenericSegmentationPlugin)
}     

GenericSegmentationPlugin::~GenericSegmentationPlugin(void)
{
}

