/*=========================================================================
/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*=========================================================================*/


#ifndef msSurfaceDeformation3D_H_HEADER_INCLUDED
#define msSurfaceDeformation3D_H_HEADER_INCLUDED



#include "mitkInteractor.h"
#include "mitkCommon.h"

#include "blShapeUtils.h"

#include <vtkType.h>

class vtkPolyData;

class SurfDeformationCallback
{
public:

	virtual ~SurfDeformationCallback( ){};
	virtual void OnRenderingDataModified() = 0;
	virtual void OnUndo(bool clearAll = false)
	{
	}
	virtual void OnParametersModified() = 0;
};

#define GAUSSMAX 2
#define GAUSSMIN 0.01
#define GAUSSSTEP 0.005
#define GAUSSDEFAULT 0.15

#define SPHRESMIN 10
#define SPHRESMAX 500
#define SPHRESSTEP 5

namespace mitk
{

	class DataTreeNode;
	class Surface;

	/**
	* \brief This class is an adaptation of the MITKSurfaceDeformationInteractor
	*   Affine interaction with objects in 3D windows.
	*
	*
	* \ingroup GenericSegmentationPlugin
	*/
	class SurfDeformationInteractor : public Interactor
	{
		enum {
			ACTION_MODIFYSIGMA,
			ACTION_MODIFYRADIUS,
			ACTION_MODIFYORTHOFACTOR,
			NUMBEROFACTIONS
		} ModifyActionType;

	public:
		mitkClassMacro(SurfDeformationInteractor, Interactor);
		mitkNewMacro3Param(Self, const char *, DataTreeNode *, DataTreeNode *);
		//mitkNewMacro2Param(Self, const char *, DataTreeNode *);

		/** \brief Sets the amount of precision */
		void SetPrecision( mitk::ScalarType precision );

		/**
		* \brief calculates how good the data, this statemachine handles, is hit
		* by the event.
		*
		* overwritten, cause we don't look at the boundingbox, we look at each point
		*/
		virtual float CalculateJurisdiction(mitk::StateEvent const *stateEvent) const;

		//!
		void SetCreateSphereMode(bool val);
		bool GetCreateSphereMode();

		//!
		void SetCallback(SurfDeformationCallback *callback);

		//!
		vtkPolyData * GetPreviousPolyData() const;

		//!
		double GetSphereRes() const;

		//!
		double GetGaussSigma() const;
		void SetGaussSigma( double val );

		//!
		void SetSelectedScalarArray( const blShapeUtils::ShapeUtils::VTKScalarType &selectedScalarArray );

		//! Fix last modified plane
		void FixPlane( const mitk::PlaneGeometry* planeGeometry, bool fix );

		//!
		bool IsPlaneFixed( const mitk::PlaneGeometry* planeGeometry );

		//!
		void UnFixAllPlanes( );

	protected:
		/**
		* \brief Constructor with Param n for limited Set of Points
		*
		* if no n is set, then the number of points is unlimited*
		*/
		SurfDeformationInteractor(const char *type, 
			DataTreeNode *dataTreeNode, DataTreeNode *pointSetDataTree);

		/**
		* \brief Default Destructor
		**/
		virtual ~SurfDeformationInteractor();

		virtual bool ExecuteAction( mitk::Action* action, 
			mitk::StateEvent const* stateEvent );

		enum
		{
			COLORIZATION_GAUSS,
			COLORIZATION_CONSTANT
		};

		bool ColorizeSurface( vtkPolyData *polyData, const mitk::Point3D &pickedPoint,
			int mode, double scalar = 0.0 );
	
		void UpdateTheSphere( mitk::Point3D center, mitk::Point3D spherePoint);


		void UpdateMaxDimension();

		void SetSphereRes(double val) { m_SphereRes = val; }

		//!
		void AutoUpdateParameters( );

	private:

		/** \brief to store the value of precision to pick a point */
		mitk::ScalarType m_Precision;

		mitk::Point3D m_InitialPickedPoint;
		mitk::Point2D m_InitialPickedDisplayPoint;
		vtkFloatingPointType m_InitialPickedPointWorld[4];

		mitk::Point3D m_CurrentPickedPoint;
		mitk::Point2D m_CurrentPickedDisplayPoint;
		vtkFloatingPointType m_CurrentPickedPointWorld[4];

		mitk::Point3D m_SurfaceColorizationCenter;

		mitk::Vector3D m_ObjectNormal;

		//! Selected original rendering data at current time step
		vtkPolyData *m_PolyData;

		mitk::DataTreeNode *m_PickedSurfaceNode;
		mitk::Surface *m_PickedSurface;
		vtkPolyData *m_PickedPolyData;

		vtkSmartPointer<vtkPolyData> m_OriginalPolyData;
		double m_GaussSigma;

		double m_SphereRes;
		double m_maxDim;

		mitk::DataTreeNode::Pointer m_pointSetNode;

		//set true when you want to create, move a new sphere
		bool m_createSphereMode;

		SurfDeformationCallback* m_CallBack;

		bool m_is2DView;

		int m_OriginalTimeStep;

		std::list<mitk::PlaneGeometry::Pointer> m_FixedPlanesList;

		//!
		blShapeUtils::ShapeUtils::VTKScalarType m_SelectedScalarArray;

		//!
		mitk::PlaneGeometry::Pointer m_PlaneGeometry;
	};

}

#endif /* msSurfaceDeformation3D_H_HEADER_INCLUDED */
