/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _GenericSegmentationPluginManualCorrectionsProcessor_H
#define _GenericSegmentationPluginManualCorrectionsProcessor_H

#include "coreBaseProcessor.h"

#include "blLinearAlgebraTypes.h"
#include "coreVTKPolyDataHolder.h"

class vtkPlane;


	struct NumParams{

		/// Percent of the total shape variability used to retrieve an instance
		double variability;

		/// Number of standard deviations the shape model should be constrained to
		double beta;

		/**
		Radius of the sphere that will affect the neighbour points in 
		the POINTSET mode.
		A Gaussian smoothing kernel used in the force propagation mechanism:
		sigma = m_PointsetRadius / 4;
		*/
		double m_PointsetRadius;

		/** 
		\brief Enable starting point for correction to use it as starting 
		point for all correction points
		*/
		bool m_bEnableStartingPoint;

		//! Starting correction point
		double m_StartingCorrectionPoint[ 3 ];

		//! Current correction point (not used)
		double m_CurrentCorrectionPoint[ 3 ];

		//! Array used for manual corrections
		std::string m_ManualCorrectionsArrayName;

		bool m_bPDModelRestricted;

		NumParams()
		{
			variability = 95.0;
			beta = 3.0;
			m_PointsetRadius = 20;
			m_bEnableStartingPoint = false;
			m_StartingCorrectionPoint[ 0 ] = 0;
			m_StartingCorrectionPoint[ 1 ] = 0;
			m_StartingCorrectionPoint[ 2 ] = 0;
			m_CurrentCorrectionPoint[ 0 ] = 0;
			m_CurrentCorrectionPoint[ 1 ] = 0;
			m_CurrentCorrectionPoint[ 2 ] = 0;
			m_ManualCorrectionsArrayName = "subpartID";
			m_bPDModelRestricted = false;
		}
	};

namespace GenericSegmentationPlugin
{
/**
Processor for 

\ingroup GenericSegmentationPlugin
\author Luigi Carotenuto
\date 25 feb 2010
*/
class ManualCorrectionsProcessor : public Core::BaseProcessor
{
public:

	typedef itk::Image<float,3> ImageType;

	typedef enum
	{
		INPUT_SURFACE_MESH,
		INPUT_POINTS,
		INPUT_NUM
	}INPUT_TYPE;

	typedef enum
	{
		Deformation,
		Translation,
		Scale
	}CorrectionType;

	typedef enum
	{
		OUTPUT_0,
		OUTPUTS_NUMBER
	}OUTPUT_TYPE;
public:
	//!
	coreProcessor(ManualCorrectionsProcessor, Core::BaseProcessor);

	typedef boost::shared_ptr<NumParams> NumParametersPtr;
	typedef std::vector<vtkPolyData*> ShapeVectorType;
	//@{ 
	/// \name Process

	const char* GetSubpartIDName();

	
	//! Call library to perform operation
	void Update( );

	/**
	\brief Set starting correction point and add extra info (regionID and 
	subpartID) to the correction point
	*/
	bool SetStartingCorrectionPoint( vtkIdType pointId );

	//! Get the last data from m_UndoList and set it as GetOutputDataEntityHolder( 0 )
	void Undo( );

	//!
	void ClearUndoList( );

	//! Load models from files
	void LoadModelFiles( );

	//! Initialize the time steps of the correction points using the input
	void InitializeCorrectionPoints( );

	//! Copy from input to output all time steps
	void InitializeOutputDataEntity( );

	//!
	void SetScalarArray( const char* name );

	//!
	const char* GetScalarArray( );

	//@}


	//@{ 
	/// \name Member variables

	//! 
	//Core::DataEntityHolder::Pointer GetCorrectionPointsHolder() const;

	Core::vtkPolyDataPtr GetCorrectionPoints(); // const;

	//! m_CurrentOutputDataEntityHolder
	//Core::DataEntityHolder::Pointer GetCurrentOutputDataEntityHolder() const;

	////! m_CurrentOutputDataEntityHolder
	//Core::DataEntity::Pointer GetCurrentOutputDataEntity() const;

	//!
	long GetUndoListSize( );

	//!
	void SetInteractionPlane( vtkPlane *plane );

	//!
	NumParametersPtr GetNumericalParam() const;

	//! 
	//void SetPDModel( smlAPIPDModel *pdModel );

	//! 
	//smlAPIPDModel *GetPDModel( );

	//!
	vtkPolyData* FillSubpartRegionID(
		vtkPolyData* correctionPoints, 
		vtkPolyData* paramInputShape );

	//!
	vtkPolyData* CreateEmptyCorrectionPoints(const char* arrayName);

	//!
	void UpdatePointSet();

	//!
	CorrectionType GetCorrectionType();
	void SetCorrectionType(CorrectionType val);

	//!
	void PropagateForces( vnl_vector<double>& paramRefShapePoints, int paramIter);

	//@}

private:
	void AddOutputToUndoList();


private:
	//!
	ManualCorrectionsProcessor();

	//!
	~ManualCorrectionsProcessor();

	//! Purposely not implemented
	ManualCorrectionsProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );
	//! Currently selected correction points
	//Core::DataEntityHolder::Pointer m_correctionPoints;

	//!
	//smlAPICardiacFitting	*m_pAPICardiacAnalysis;

	//!
	NumParametersPtr m_numericalParamPtr;

	//! Current output
	//Core::DataEntityHolder::Pointer m_CurrentOutputDataEntityHolder;

	//!
	std::list<Core::vtkPolyDataPtr> m_UndoList;

	//! 2D Interaction plane where the user has clicked the correction point
	vtkPlane *m_InteractionPlane;

	//!
	long m_lUndoMaxSize;

	//! Use translation vtk transform for the input mesh
	bool m_bUseTranslation;
	//! Use scale vtk transform for the input mesh
	bool m_bUseScale;

	CorrectionType m_correctionType;
	vnl_vector<float> curValue;
	baselib::VnlMatrixType curVector;

	//!
	vtkSmartPointer<vtkPolyData> m_WorkingShape;

	double point[3];
};
    
}   // namespace GenericSegmentationPlugin

#endif //_GenericSegmentationPluginManualCorrectionsProcessor_H
