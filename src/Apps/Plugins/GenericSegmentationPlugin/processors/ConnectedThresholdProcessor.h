#ifndef evoConnectedThresholdProcessor_H
#define evoConnectedThresholdProcessor_H

// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "blConnectedThresholdImageFilter.h"

#include "coreDataHolder.h"
#include "coreException.h"
#include "coreReportExceptionMacros.h"
#include "coreBaseProcessor.h"
#include "corePluginMacros.h"

#include "itkImage.h"


coreCreateRuntimeExceptionClassMacro(evoConnectedThresholdControllerException, 
									 "Error in evoConnectedThresholdController", /*NOT EXPORTING*/);

namespace gsp{

/**
This class runs the Connected Threshold filter.

\ingroup GenericSegmentationPlugin
\author Maarten Nieber
\date 20 Nov 2007
*/

class PLUGIN_EXPORT ConnectedThresholdProcessor : public Core::BaseProcessor
{
public:
	
	//!
	coreDeclareSmartPointerClassMacro(ConnectedThresholdProcessor, Core::SmartPointerObject);
	
	typedef enum
	{
		INPUT_IMAGE,
		INPUT_SEED_POINT,
		NUMBER_OF_INPUTS,
	} INPUT_TYPE;

	/** float for input image. */
	typedef itk::Image< float, 3 > InputImageType;

	/** Unsigned for output image. */
	typedef itk::Image< unsigned char, 3 > OutputImageType;

	//!
	typedef Core::DataHolder< InputImageType::Pointer > InputImageHolder;

	//!
	void Update( void );

	//!
	void SetLowerThreshold(double d);

	//!
	double GetLowerThreshold();

	//!
	void SetUpperThreshold(double d);

	//!
	double GetUpperThreshold();

	//!
	void SetReplaceValue(int i);

	//!
	int GetReplaceValue();

	//!
	void SetSeed(InputImageType::IndexType index);

	//!
	InputImageType::IndexType GetSeed();

	//!
	void SetNumberOfIterations(unsigned int u);

	//!
	unsigned int GetNumberOfIterations();

	//!
	void SetTimeStep(double d);

	//!
	double GetTimeStep();

	//!
	void SetConductance(double d);

	//!
	double GetConductance();

	//!
	void SetImageSpacing(bool b);

	//!
	bool GetImageSpacing();
	
private:
	ConnectedThresholdProcessor();
    //! purposely not implemented
	ConnectedThresholdProcessor( const Self& );	
	//! purposely not implemented
	void operator = ( const Self& );
	//! Filters the image
	blConnectedThresholdImageFilter::Pointer m_Filter;

	//! Lower threshold pixel value. The default value is 0.
	double m_LowerThreshold;
	//! Upper threshold pixel value. The default value is 0.
	double m_UpperThreshold;
	//! Intensity value in segmented image.
	unsigned char m_ReplaceValue;
	//! Seed point to start growing region algorithm.
	InputImageType::IndexType m_Seed;
	//! Number of iterations for anisotropic curvature smoothing filter. The default value is 5.
	unsigned int m_NumberOfIterations;
	//! Time step for anisotropic curvature smoothing filter. The default value is 0.0125.
	double m_TimeStep;
	//! Conductance for anisotropic curvature smoothing filter. The default value is 1.0.
	double m_Conductance;
	//! Use image spacing when applying anisotropic curvature smoothing filter. The default value is true.
	bool m_UseImageSpacing;
};

} // gsp

#endif //evoConnectedThresholdProcessor_H
