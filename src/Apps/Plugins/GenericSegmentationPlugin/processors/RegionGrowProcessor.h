#ifndef ASPREGIONGROWPROCESSOR_H
#define ASPREGIONGROWPROCESSOR_H

// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "coreException.h"
#include "coreDataHolder.h"
#include "coreReportExceptionMacros.h"
#include "coreBaseProcessor.h"
#include "corePluginMacros.h"

#include "boost/shared_ptr.hpp"

#include "itkPoint.h"
#include "itkNeighborhoodConnectedImageFilter.h"
#include "itkImage.h"


/**
This exception class is used in general cases where an error occurs during region growing.

\author Maarten Nieber
03 mar 2008
*/
coreCreateRuntimeExceptionClassMacro(RegionGrowControllerException, "Error in RegionGrowController", 
									 /*NOT EXPORTING*/);

namespace gsp{

/**
This class runs the itk region grow filter on the current input image.

\author Maarten Nieber
\date / 09 2/2007
*/

class PLUGIN_EXPORT RegionGrowProcessor : public Core::BaseProcessor
{
public:
	//!
	coreDeclareSmartPointerClassMacro(RegionGrowProcessor, Core::SmartPointerObject);

	typedef enum
	{
		INPUT_IMAGE,
		INPUT_SEED_POINT,
		NUMBER_OF_INPUTS,
	} INPUT_TYPE;

	//!
	typedef itk::Image< short, 3 > OutputImageType;

	//!
	void SetReplaceValue(double d);

	//!
	void SetUpperThreshold(double d);

	//!
	void SetLowerThreshold(double d);

	//!
	double GetReplaceValue();

	//!
	double GetUpperThreshold();

	//!
	double GetLowerThreshold();


	//! Runs the region grow filter
	void Update( void );

private:
	RegionGrowProcessor();
	//! purposely not implemented
	RegionGrowProcessor( const Self& );	
	//! purposely not implemented
	void operator=( const Self& );

private:
	//!
	double m_ReplaceValue;
	//!
	double m_UpperThreshold;
	//!
	double m_LowerThreshold;
};

} // gsp

#endif //ASPREGIONGROWPROCESSOR_H
