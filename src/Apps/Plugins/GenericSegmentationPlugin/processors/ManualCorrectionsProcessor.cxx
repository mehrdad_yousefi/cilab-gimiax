/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "ManualCorrectionsProcessor.h"

#include <string>
#include <iostream>

#include "coreReportExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityHelper.txx"
#include "coreKernel.h"

#include "vtkShortArray.h"
#include "vtkCellArray.h"
#include "vtkPlane.h"
#include "vtkTransform.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkMath.h"
#include "vtkPointLocator.h"
#include "vtkCutter.h"
#include "vtkCellLocator.h"
#include "blVTKHelperTools.h"
#include "blMath.h"
#include "vtkSmartPointer.h"
#include "vtkFloatArray.h"

#include "coreSettings.h"


const long UNDO_MAX_SIZE = 10;
const char* OUTPUT_NAME = "Shape Corrections";

namespace GenericSegmentationPlugin
{

ManualCorrectionsProcessor::ManualCorrectionsProcessor()
{
	SetName( "ManualCorrectionsProcessor" );

	m_lUndoMaxSize = UNDO_MAX_SIZE;

	m_numericalParamPtr.reset( new NumParams );
	m_numericalParamPtr->m_bPDModelRestricted = false;
	m_numericalParamPtr->m_PointsetRadius = 50;
	m_numericalParamPtr->m_ManualCorrectionsArrayName = "epiendo";
	
	BaseProcessor::SetNumberOfInputs( INPUT_NUM );
	GetInputPort(INPUT_SURFACE_MESH)->SetName(  "Input Surface" );
	GetInputPort(INPUT_SURFACE_MESH)->SetDataEntityType( Core::SurfaceMeshTypeId);

	GetInputPort(INPUT_POINTS)->SetName(  "Input Points" );
	GetInputPort(INPUT_POINTS)->SetDataEntityType( Core::PointSetTypeId);
	BaseProcessor::SetNumberOfOutputs( OUTPUTS_NUMBER );
	m_InteractionPlane = vtkPlane::New();


	m_correctionType = Deformation;
	m_WorkingShape = vtkSmartPointer<vtkPolyData>::New();

	point[0]=0.0;
	point[1]=0.0;
	point[2]=0.0;
}

ManualCorrectionsProcessor::~ManualCorrectionsProcessor()
{
	m_InteractionPlane->Delete();
}

void ManualCorrectionsProcessor::Update()
{
	try
	{
		Core::vtkPolyDataPtr polyData;
		polyData = m_UndoList.back( );

		m_WorkingShape->DeepCopy(polyData);

		Core::vtkPolyDataPtr vtkOutputMesh = Core::vtkPolyDataPtr::New();

		switch (m_correctionType)
		{
		case Deformation:
			{
				// only works with one time step ( think about vector of polydata)
				UpdatePointSet();
				vtkOutputMesh->DeepCopy( m_WorkingShape );

				break;
			}
		case Translation:
			{

				double diffPoints[ 3 ];
				double *currentPoint = GetCorrectionPoints()->GetPoints( )->GetPoint( 0 );
				diffPoints[ 0 ] = 
					currentPoint[ 0 ] - m_numericalParamPtr->m_StartingCorrectionPoint[ 0 ];
				diffPoints[ 1 ] = 
					currentPoint[ 1 ] - m_numericalParamPtr->m_StartingCorrectionPoint[ 1 ];
				diffPoints[ 2 ] = 
					currentPoint[ 2 ] - m_numericalParamPtr->m_StartingCorrectionPoint[ 2 ];

				// Use translation filter
				vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
				transform->Translate( diffPoints );

				vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter;
				transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
				transformFilter->SetInput( polyData );
				transformFilter->SetTransform( transform );
				transformFilter->Update();

				vtkOutputMesh->DeepCopy( transformFilter->GetOutput( ) );
				break;
			}
		case Scale:
			{
				double origin[ 3 ];
				origin[0] = origin[1] = origin[2] = 0.0;
				double scale;
				double dist1, dist2;
				double *currentPoint = GetCorrectionPoints()->GetPoints( )->GetPoint( 0 );
				dist1 = vtkMath::Distance2BetweenPoints(currentPoint,origin);
				dist2 = vtkMath::Distance2BetweenPoints(m_numericalParamPtr->m_StartingCorrectionPoint,origin);
				scale = sqrt(dist1)/sqrt(dist2);

				blVTKHelperTools::ScaleShape( 
					polyData, 
					vtkOutputMesh, 
					scale,
					true);

				break;
			}
		default:
			break;
		}

		if ( vtkOutputMesh.GetPointer() != NULL && vtkOutputMesh->GetNumberOfPoints() != 0 )
		{
			Core::vtkPolyDataPtr outputPolyData;
			GetProcessingData( INPUT_SURFACE_MESH, outputPolyData, GetTimeStep() );
			outputPolyData->DeepCopy( vtkOutputMesh );
			GetInputDataEntity( INPUT_SURFACE_MESH )->Modified();
		}
	}
	catch( ... )
	{
		throw;
	}

}
bool ManualCorrectionsProcessor::SetStartingCorrectionPoint( 
	vtkIdType	pointId )
{
	bool bReturn = false;
	try
	{

		Core::vtkPolyDataPtr	vtkFilteredMesh;
		Core::DataEntityHelper::GetProcessingData( 
			GetInputDataEntityHolder( INPUT_SURFACE_MESH ),
			vtkFilteredMesh,
			GetTimeStep( ) );

		if ( vtkFilteredMesh->GetNumberOfPoints() != 0 &&
			GetCorrectionPoints()->GetPoints( )->GetNumberOfPoints( ) != 0 )
		{

			vtkPolyData *outputPoints;
			outputPoints = FillSubpartRegionID( 
				GetCorrectionPoints( ),
				vtkFilteredMesh );
			GetCorrectionPoints( )->DeepCopy( outputPoints );

			m_numericalParamPtr->m_bEnableStartingPoint = true;
			GetCorrectionPoints( )->GetPoint( 
				0, 
				m_numericalParamPtr->m_StartingCorrectionPoint );

			bReturn = true;
		}
		
		// Reuse input
		GetOutputDataEntityHolder( 0 )->SetSubject( GetInputDataEntity( 0 ) );

		AddOutputToUndoList( );
	}
	coreCatchExceptionsReportAndNoThrowMacro( 
		"CorrectionsProcessor::SetStartingCorrectionPoint" );

	return bReturn;
}

void ManualCorrectionsProcessor::Undo()
{
	try
	{
		if ( m_UndoList.empty() )
		{
			return;
		}

		// Get the last from the list
		Core::vtkPolyDataPtr polyData;
		polyData = m_UndoList.back( );
		m_UndoList.pop_back( );

		// Set the output to the output of this processor to update the view
		Core::vtkPolyDataPtr inputPolyData;
		GetProcessingData( INPUT_SURFACE_MESH, inputPolyData, GetTimeStep() );
		inputPolyData->DeepCopy( polyData );
		GetOutputDataEntity( INPUT_SURFACE_MESH )->Modified();

	}
	coreCatchExceptionsReportAndNoThrowMacro( 
		"CorrectionsProcessor::Update" );
}

long ManualCorrectionsProcessor::GetUndoListSize()
{
	return m_UndoList.size();
}



void ManualCorrectionsProcessor::SetInteractionPlane( vtkPlane *plane )
{
	m_InteractionPlane->SetOrigin( plane->GetOrigin() );
	m_InteractionPlane->SetNormal( plane->GetNormal() );
}

ManualCorrectionsProcessor::NumParametersPtr 
ManualCorrectionsProcessor::GetNumericalParam() const
{
	return m_numericalParamPtr;
}

void ManualCorrectionsProcessor::LoadModelFiles()
{
}

Core::vtkPolyDataPtr ManualCorrectionsProcessor::GetCorrectionPoints() // const
{
	Core::vtkPolyDataPtr	correctionPoints;
	Core::DataEntityHelper::GetProcessingData( 
		//GetCorrectionPointsHolder( ),
		GetInputDataEntityHolder( INPUT_POINTS ),
		correctionPoints,
		GetTimeStep() );
	return correctionPoints;
}

void ManualCorrectionsProcessor::ClearUndoList()
{
	try
	{
		while ( m_UndoList.size() )
		{
			m_UndoList.pop_back( );
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro( 
		"gsp::ManualCorrectionsProcessor::ClearUndoList()" );
}



void ManualCorrectionsProcessor::InitializeCorrectionPoints( )
{
	try
	{
		Core::DataEntity::Pointer dataEntityCorrections;
		dataEntityCorrections = GetInputDataEntity( INPUT_POINTS );
		if ( dataEntityCorrections.IsNull() )
		{
			return;
		}

		for ( unsigned i = 0 ; i < dataEntityCorrections->GetNumberOfTimeSteps() ; i++ )
		{
			Core::vtkPolyDataPtr	correctionPointsPtr;
			dataEntityCorrections->GetProcessingData( correctionPointsPtr, i );
			correctionPointsPtr->DeepCopy( CreateEmptyCorrectionPoints(
				m_numericalParamPtr->m_ManualCorrectionsArrayName.c_str()) );
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro( 
		"gsp::ManualCorrectionsProcessor::InitializeCorrectionPoints()" );

}

const char* ManualCorrectionsProcessor::GetSubpartIDName()
{
	return "boh";
}

void ManualCorrectionsProcessor::SetScalarArray( const char* name )
{
	m_numericalParamPtr->m_ManualCorrectionsArrayName = name;
}

const char* ManualCorrectionsProcessor::GetScalarArray()
{
	return m_numericalParamPtr->m_ManualCorrectionsArrayName.c_str();
}

vtkPolyData* ManualCorrectionsProcessor::FillSubpartRegionID(
	vtkPolyData* correctionPoints, 
	vtkPolyData* paramInputShape )
{

	return correctionPoints;

}

vtkPolyData* ManualCorrectionsProcessor::CreateEmptyCorrectionPoints(const char* arrayName)
{
	vtkPolyData* emptyCorrectionPoints = vtkPolyData::New( );

	// Create points
	vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
	emptyCorrectionPoints->SetPoints( points );

	// Create vertex
	vtkSmartPointer<vtkCellArray> newVertex = vtkSmartPointer<vtkCellArray>::New();
	emptyCorrectionPoints->SetVerts( newVertex );

	// Create arrayName
	vtkSmartPointer<vtkShortArray> newScalars = vtkSmartPointer<vtkShortArray>::New();
	newScalars->SetName( arrayName );
	emptyCorrectionPoints->GetPointData()->AddArray(newScalars);

	return emptyCorrectionPoints;
}

void ManualCorrectionsProcessor::UpdatePointSet()
{
	// auxiliaries
	vnl_vector<double> candidatePoint(3);
	vnl_vector<double> neighPoint(3);
	vnl_vector<double> diffNeigh(3);
	vnl_vector<double> cellPoint(3);
	vnl_vector<double> diffCenter(3);

	int NumIt = 1;
	unsigned int numberOfShapePoints = m_WorkingShape->GetNumberOfPoints();
	curValue.set_size( numberOfShapePoints );
	bllao::Resize(curVector, numberOfShapePoints, 3);
	curValue.fill(0);
	bllao::FillMatrix(curVector, 0.0);

	unsigned int currentIteration;
	for (currentIteration=0; 
		currentIteration<NumIt; 
		currentIteration++)
	{

		vtkPoints * shapePoints = m_WorkingShape->GetPoints();

		// \todo replace this pointlocator with a way of setting the correspondence between 
		// subpart points and shape model points
		vtkSmartPointer<vtkPointLocator> shapePointLocator = 
			vtkSmartPointer<vtkPointLocator>::New();
		shapePointLocator->SetDataSet(m_WorkingShape);
		shapePointLocator->BuildLocator();

		/*		SubpartMapType::const_iterator subpartMapIter;
		for (subpartMapIter = m_SubpartMap.begin();
		subpartMapIter != m_SubpartMap.end(); 
		++subpartMapIter ) 
		{
		if (subpartMapIter->second.subpartManualPt != NULL) // if has corrections
		{*/
		unsigned int numberOfCorrectingPoints = 
			//subpartMapIter->second.subpartManualPt->GetNumberOfPoints();
			GetCorrectionPoints()->GetNumberOfPoints();

		vtkSmartPointer<vtkPointLocator> subpartPointLocator = 
			vtkSmartPointer<vtkPointLocator>::New();
		subpartPointLocator->SetDataSet( m_WorkingShape );//subpartMapIter->second.subpartShapePt);
		subpartPointLocator->BuildLocator();

		// Compute intersection with the plane if provided
		vtkSmartPointer<vtkPolyData> intersectionShape = NULL;
		vtkSmartPointer<vtkCutter> cutter = NULL;
		vtkSmartPointer<vtkCellLocator> subpartCellLocator = NULL;
		vtkSmartPointer<vtkPointLocator> correctionPlanePointLocaltor = NULL;
		if ( m_InteractionPlane != NULL )
		{
			cutter = vtkSmartPointer<vtkCutter>::New();
			cutter->SetInput( m_WorkingShape );//subpartMapIter->second.subpartShapePt );
			cutter->SetCutFunction( m_InteractionPlane );
			cutter->Update();

			intersectionShape = cutter->GetOutput();
			if ( intersectionShape->GetNumberOfPoints() > 0 )
			{
				correctionPlanePointLocaltor = vtkSmartPointer<vtkPointLocator>::New();
				correctionPlanePointLocaltor->SetDataSet( intersectionShape );
				correctionPlanePointLocaltor->BuildLocator();
			}
		}

		// If intersection cannot be computed -> Get the initial mesh
		if ( correctionPlanePointLocaltor == NULL )
		{
			subpartCellLocator = vtkSmartPointer<vtkCellLocator>::New();
			subpartCellLocator->SetDataSet( m_WorkingShape ); //subpartMapIter->second.subpartShapePt );
			subpartCellLocator->BuildLocator();
		}

		unsigned int correctionPointCounter;
		for (correctionPointCounter = 0; 
			correctionPointCounter < numberOfCorrectingPoints; 
			correctionPointCounter++)
		{
			GetCorrectionPoints()->GetPoint(
				correctionPointCounter,
				candidatePoint.data_block());
			vtkIdType cellId = 0;

			int subId = 0;
			double dist = 0;

			// Find closest point to the mesh or outline or use the starting point
			if ( m_numericalParamPtr->m_bEnableStartingPoint )
			{
				cellPoint[ 0 ] = m_numericalParamPtr->m_StartingCorrectionPoint[ 0 ];
				cellPoint[ 1 ] = m_numericalParamPtr->m_StartingCorrectionPoint[ 1 ];
				cellPoint[ 2 ] = m_numericalParamPtr->m_StartingCorrectionPoint[ 2 ];
			}
			else if ( subpartCellLocator )
			{
				subpartCellLocator->FindClosestPoint(
					candidatePoint.data_block(), 
					cellPoint.data_block(), 
					cellId, 
					subId, 
					dist);
			}
			else if ( correctionPlanePointLocaltor )
			{
				vtkIdType pointId = correctionPlanePointLocaltor->FindClosestPoint(
					candidatePoint.data_block() );
				intersectionShape->GetPoint( pointId, cellPoint.data_block() );
			}

			diffCenter = candidatePoint-cellPoint;

			vtkIdList* auxPtIds = vtkIdList::New();
			subpartPointLocator->FindPointsWithinRadius(
				m_numericalParamPtr->m_PointsetRadius,
				cellPoint.data_block(),
				auxPtIds);

			//get the farthest point
			//costa: this works well only when there are many neighbours
			//vtkIdType farthest_pointId = auxPtIds->GetId(m_NumberOfNeighbours-1);
			//subpartMapIter->second.subpartShapePt->GetPoint(farthest_pointId, neighPoint.data_block());
			//double maxNeighDistance = (cellPoint - neighPoint).magnitude();
			//m_PropagationSigma = maxNeighDistance; 

			int neighbourPointCounter;
			for (neighbourPointCounter=0; 
				neighbourPointCounter < auxPtIds->GetNumberOfIds();
				neighbourPointCounter++) 
			{
				vtkIdType subpart_pointId = auxPtIds->GetId(neighbourPointCounter);
				/*subpartMapIter->second.subpartShapePt*/ 
				m_WorkingShape->GetPoint(subpart_pointId, neighPoint.data_block());

				vtkIdType shape_pointId = shapePointLocator->FindClosestPoint(
					neighPoint.data_block());

				diffNeigh = cellPoint-neighPoint;

				double val = blMath::Dot(diffNeigh, diffNeigh);
				//double mag = exp(-val/(2*m_PropagationSigma*m_PropagationSigma));
				//double magNeighbour = exp(-val/(2*m_PropagationSigma*m_PropagationSigma));
				double sigma = m_numericalParamPtr->m_PointsetRadius/ 4;
				double magNeighbour = exp( -val / ( 2 * sigma * sigma ) );
				//double magCandidate = 1/(candidatePoint-neighPoint).magnitude();

				curValue[shape_pointId] ++;
				//curVector(shape_pointId, 0) += mag*diffNeigh[0];
				//curVector(shape_pointId, 1) += mag*diffNeigh[1];
				//curVector(shape_pointId, 2) += mag*diffNeigh[2];

				//curValue[shape_pointId] = __max(curValue[shape_pointId], mag);
				curVector(shape_pointId, 0) += float(diffCenter[0] * magNeighbour);
				curVector(shape_pointId, 1) += float(diffCenter[1] * magNeighbour);
				curVector(shape_pointId, 2) += float(diffCenter[2] * magNeighbour);

			}

			auxPtIds->Delete();
		}	// for each correcting point

		/*	} // if has corrections
		} // for each subpart*/

		// \todo see if this variable should be a class member
		vnl_vector<double> refShapePoints(numberOfShapePoints * 3);

		// todo: make normalization simpler and get rid of this method
		PropagateForces(refShapePoints, currentIteration);

		// fit shape model to cloud of candidate points
		/*SetOutputShape(refShapePoints, m_bPDModelRestricted );*/
		vnl_vector<double> *newShapePoints = &refShapePoints;
		//vtkPoints *shapePoints = polyData->GetPoints();
		vnl_vector<double> aux_pt(3);

		unsigned int index = 0;
		unsigned int shapePointCounter;
		for (shapePointCounter = 0; shapePointCounter < numberOfShapePoints; shapePointCounter++)
		{
			aux_pt[0] = newShapePoints->get( index );
			index++;
			aux_pt[1] = newShapePoints->get( index );
			index++;
			aux_pt[2] = newShapePoints->get( index );
			index++;
			shapePoints->SetPoint(shapePointCounter, aux_pt.data_block());
		}


		// \todo: think how to get rid of this call
		//UpdateSubpartMap();

	} //currentIteration

}

ManualCorrectionsProcessor::CorrectionType 
ManualCorrectionsProcessor::GetCorrectionType()
{
	return m_correctionType;
}

void ManualCorrectionsProcessor::SetCorrectionType( CorrectionType val )
{
	m_correctionType = val;
}

/**
*	Propagates forces
*/
// -------------------------------------------------------------------
void ManualCorrectionsProcessor::PropagateForces( vnl_vector<double>& paramRefShapePoints, int paramIter)
// -------------------------------------------------------------------
{

	vtkFloatArray* pcoords = NULL;

	int index = 0;
	vtkPoints *shapePoints = m_WorkingShape->GetPoints();

	unsigned int numberOfShapePoints = m_WorkingShape->GetNumberOfPoints();

	//	double max_value = vals.max_value();

	vnl_vector<double> displacement(3);

	for (int k=0; k<numberOfShapePoints; k++)
	{	
		if (curValue[k])
		{		
			displacement[0] = curVector(k, 0)/curValue[k];
			displacement[1] = curVector(k, 1)/curValue[k];
			displacement[2] = curVector(k, 2)/curValue[k];
		}
		else
			displacement.fill(0.0);

		double *p = shapePoints -> GetPoint(k);
		p[0] += displacement[0];
		p[1] += displacement[1];
		p[2] += displacement[2];

		shapePoints -> SetPoint(k,p);

		paramRefShapePoints[index] = p[0];
		index ++;
		paramRefShapePoints[index] = p[1];
		index ++;
		paramRefShapePoints[index] = p[2];
		index ++;
	}

}


void ManualCorrectionsProcessor::AddOutputToUndoList()
{
	Core::vtkPolyDataPtr polyData = Core::vtkPolyDataPtr::New();

	//Set input to the Undo list, we need to copy the input because it is reused
	Core::vtkPolyDataPtr processingData;
	GetProcessingData( INPUT_SURFACE_MESH, processingData, GetTimeStep( ) );
	polyData->DeepCopy( processingData );
	m_UndoList.push_back( polyData );

	// If we reach the max -> pop the front
	if ( m_UndoList.size() > m_lUndoMaxSize )
	{
		m_UndoList.pop_front( );
	}
}

}   // namespace GenericSegmentationPlugin
