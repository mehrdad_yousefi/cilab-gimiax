#ifndef OtsuProcessor_H
#define OtsuProcessor_H

/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#include "blOtsuImageFilter.h"

#include "coreBaseProcessor.h"
#include "coreException.h"
#include "coreDataHolder.h"
#include "coreReportExceptionMacros.h"
#include "coreFrontEndPlugin.h"

#include "CILabItkMacros.h"

coreCreateRuntimeExceptionClassMacro(evoOtsuControllerException, "Error in evoOtsuController", /*NOT EXPORTING*/);

namespace gsp{

/**
This class contains the Otsu processor.

\author Maarten Nieber (refactored by Chiara Riccobene)
\date 29 sept 2009
*/

class PLUGIN_EXPORT OtsuProcessor : public Core::BaseProcessor
{
public:

	//!
	coreDeclareSmartPointerClassMacro(OtsuProcessor, Core::SmartPointerObject);
	//!
	typedef itk::Image< float, 3 > InputImageType;
	//!
	typedef Core::DataHolder< InputImageType::Pointer > InputImageHolder;
	
	//!
	void SetInsideValue(float _insideValue);
	//!
	void SetOutsideValue(float _outsideValue);
	//!
	void SetNumberOfHistogramBins(unsigned int _numberOfHistogramBins);
	//!
	void SetThreshold(float _threshold);
	//!
	float GetInsideValue(void);
	//!
	float GetOutsideValue(void);
	//!
	unsigned int GetNumberOfHistogramBins(void);
	//!
	float GetThreshold(void);
	//!
	void Update( );
	
private:
	
	OtsuProcessor();
    //! purposely not implemented
	OtsuProcessor( const Self& );	
	//! purposely not implemented
	void operator = ( const Self& );    

private:
	//!
	OtsuImageFilter::Pointer m_OtsuFilter;
	//!
	float insideValue;
	//!
	float outsideValue;
	//!
	unsigned int numberOfHistogramBins;
	//!
	float threshold;
};

} //gso 

#endif //OtsuProcessor_H
