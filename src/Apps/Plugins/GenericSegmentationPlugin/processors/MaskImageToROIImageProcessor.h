/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _MaskImageToROIImageProcessor_H
#define _MaskImageToROIImageProcessor_H

#include "coreBaseProcessor.h"



#include "vtkSmartPointer.h"


namespace GenericSegmentationPlugin
{

/**
Processor for 

\ingroup GenericSegmentationPlugin
\author David Lucena Gonz�lez
\date 31 Mar 2011
*/
class MaskImageToROIImageProcessor : public Core::BaseProcessor
{
public:

	typedef enum
	{
		INPUT_ROIIMAGE,
		INPUT_MASKIMAGE,
		INPUTS_NUMBER
	} INPUT_TYPE;

	typedef enum
	{
		OUTPUT_ROIIMAGE,
		OUTPUT_MASKIMAGE,
		OUTPUTS_NUMBER
	} OUTPUT_TYPE;

public:
	//!
	coreProcessor(MaskImageToROIImageProcessor, Core::BaseProcessor);
	
	//! Call library to perform operation
	void Update();

	void CreateMaskImageFromROIImage();

	void SetNumberOfLevels( int numberOfLevels );
	int GetNumberOfLevels();

private:
	//!
	MaskImageToROIImageProcessor();

	//!
	~MaskImageToROIImageProcessor();

	//! Purposely not implemented
	MaskImageToROIImageProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );

	Core::DataEntity::Pointer CreateROIImageDataEntityFromImageDataEntity( Core::DataEntity::Pointer inputDataEntity );

	void CreateROIImageVectorFromImageVector( const std::vector<vtkSmartPointer<vtkImageData> >& inputVectorSP,
	                                          std::vector<vtkSmartPointer<vtkImageData> >& outputVectorSP ) const;

	int m_NumberOfLevels;
};

}   // namespace GenericSegmentationPlugin

#endif //_MaskImageToROIImageProcessor_H
