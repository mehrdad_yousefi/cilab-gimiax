/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _gspPropagateLandmarksProcessor_H
#define _gspPropagateLandmarksProcessor_H

#include "coreBaseProcessor.h"


namespace GenericSegmentationPlugin
{

/**
Processor for 

\ingroup GenericSegmentationPlugin
\author Xavi Planes
\date 16 feb 2009
*/
class PropagateLandmarksProcessor : public Core::BaseProcessor
{
public:

	typedef itk::Image<float,3> ImageType;

	typedef enum
	{
		INPUT_0,
		INPUT_1,
		INPUTS_NUMBER
	} INPUT_TYPE;

	typedef enum
	{
		OUTPUT_0,
		OUTPUT_1,
		OUTPUT_2,
		OUTPUTS_NUMBER
	}OUTPUT_TYPE;
public:
	//!
	coreProcessor(PropagateLandmarksProcessor, Core::BaseProcessor);
	
	//! Call library to perform operation
	void Update( );

	Core::IntHolderType::Pointer GetTimeStepHolder();
	void SetTimeStepHolder(Core::IntHolderType::Pointer holder);

private:
	//!
	PropagateLandmarksProcessor();

	//!
	~PropagateLandmarksProcessor();

	//! Purposely not implemented
	PropagateLandmarksProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );


	//!
	Core::IntHolderType::Pointer m_TimeStepHolder;
};

}   // namespace GenericSegmentationPlugin

#endif //_gspPropagateLandmarksProcessor_H
