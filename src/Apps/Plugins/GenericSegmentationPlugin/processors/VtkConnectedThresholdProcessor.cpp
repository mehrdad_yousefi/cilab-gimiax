/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "VtkConnectedThresholdProcessor.h"

#include <string>
#include <iostream>

#include "coreReportExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityHelper.txx"
#include "coreVTKImageDataHolder.h"
#include "coreKernel.h"


#include "vtkSmartPointer.h"
#include "vtkImageSeedConnectivity.h"
#include "vtkImageThreshold.h"
#include "vtkImageCast.h"

#include "meBinImageToMeshFilter.h"

#include "itkVTKImageToImageFilter.h"

gsp::VtkConnectedThresholdProcessor::VtkConnectedThresholdProcessor( )
{
	SetName( "VtkConnectedThresholdProcessor" );
	
	BaseProcessor::SetNumberOfInputs( INPUTS_NUMBER );
	GetInputPort(INPUT_IMAGE)->SetName(  "Input Image" );
	GetInputPort(INPUT_IMAGE)->SetDataEntityType( Core::ImageTypeId);
	GetInputPort(INPUT_SEED_POINT)->SetName(  "Input Point" );
	GetInputPort(INPUT_SEED_POINT)->SetDataEntityType( Core::PointSetTypeId);
	BaseProcessor::SetNumberOfOutputs( OUTPUTS_NUMBER );
	m_parameters = new ConnectedThresholdParameters();

}

gsp::VtkConnectedThresholdProcessor::~VtkConnectedThresholdProcessor()
{
}

void gsp::VtkConnectedThresholdProcessor::Update()
{
	// Get the first image
	ImageType::Pointer itkInputImage;
	GetProcessingData( INPUT_IMAGE,
		itkInputImage );

	Core::vtkPolyDataPtr seedPoint;
	GetProcessingData( INPUT_SEED_POINT,
		seedPoint );

	// Convert from physical point to index.
	itk::Point<double, 3> physicalPoint;
	itk::Index<3> index;
	physicalPoint[0] = seedPoint->GetPoint(0)[0];
	physicalPoint[1] = seedPoint->GetPoint(0)[1];
	physicalPoint[2] = seedPoint->GetPoint(0)[2];
	itkInputImage->TransformPhysicalPointToIndex(physicalPoint, index);

	std::cout << "physicalPoint = (" << physicalPoint[0] << "," << physicalPoint[1] << "," << physicalPoint[2] << ")" << std::endl;
	std::cout << "index = (" << index[0] << "," << index[1] << "," << index[2] << ")" << std::endl;


	Core::vtkImageDataPtr vtkImage;
	GetProcessingData( INPUT_IMAGE ,vtkImage );

	
	vtkSmartPointer<vtkImageThreshold> threshold = vtkSmartPointer<vtkImageThreshold>::New();
	threshold->SetInput(vtkImage);
	threshold->SetInValue(255);
	threshold->SetOutValue(0);
	threshold->ThresholdBetween(m_parameters->lowerThreshold,m_parameters->upperThreshold);
	threshold->Update();

	vtkSmartPointer<vtkImageCast> cast = vtkSmartPointer<vtkImageCast>::New();
	cast->SetInput(threshold->GetOutput());
	cast->SetOutputScalarTypeToUnsignedChar();

	vtkSmartPointer<vtkImageSeedConnectivity> conn = 
		vtkSmartPointer<vtkImageSeedConnectivity>::New();
	conn->SetInput(cast->GetOutput());
	conn->AddSeed(index[0],index[1],index[2]);
	conn->SetInputConnectValue(255);
	conn->SetOutputConnectedValue(255);
	conn->SetOutputUnconnectedValue(0);
	conn->Update();

	double isoValue = 0.5;

	itk::Image< unsigned char, 3 >::Pointer itkImage;
		
	itk::VTKImageToImageFilter<itk::Image< unsigned char, 3 > >::Pointer toItkFilter;
	toItkFilter = itk::VTKImageToImageFilter<itk::Image< unsigned char, 3 > >::New();
	toItkFilter->SetInput( conn->GetOutput() );	
	toItkFilter->Update();
	itkImage = (itk::Image< unsigned char, 3 >*)toItkFilter->GetOutput();

	Core::vtkPolyDataPtr mesh = BinImageToMesh::CreateIsoSurface<itk::Image< unsigned char, 3 > >(
		itkImage, 
		isoValue, 
		false);

	// Set the output mesh of this processor
	UpdateOutput(OUTPUT_MESH, 
		mesh, 
		"Connected threshold mesh",
		false,
		1,
		GetInputDataEntity(0));

	// Set the output to the output of this processor
	//UpdateOutput(OUTPUT_IMAGE, conn->GetOutput(), "Connected threshold segmentation",false,GetInputDataEntity(0));
	UpdateOutput(OUTPUT_IMAGE,
		conn->GetOutput(),
		"Connected threshold segmentation", 
		false, 
		1,
		GetInputDataEntity(0) );

}
