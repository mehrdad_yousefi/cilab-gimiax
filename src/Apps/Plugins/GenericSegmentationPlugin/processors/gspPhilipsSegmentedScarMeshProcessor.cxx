/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "gspPhilipsSegmentedScarMeshProcessor.h"

#include <string>
#include <iostream>
#include <vector>

#include "coreReportExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityHelper.txx"
#include "coreKernel.h"
#include "coreVTKPolyDataHolder.h"
#include "coreVTKImageDataHolder.h"
#include "coreVTKUnstructuredGridHolder.h"
#include "meVTKTetraGenerationFilter.h"

#include "vtkSmartPointer.h"
#include "vtkImageShiftScale.h"
#include "vtkPointData.h"
#include "vtkDataArray.h"
#include "vtkImageGaussianSmooth.h"
#include "vtkMarchingCubes.h"
#include "vtkCleanPolyData.h"
#include "vtkSmoothPolyDataFilter.h"
#include "vtkPolyDataNormals.h"
#include "vtkUnstructuredGrid.h"
#include "vtkProbeFilter.h"
#include "vtkDataSetToUnstructuredGridFilter.h"

using namespace std;

namespace gsp
{

PhilipsSegmentedScarMeshProcessor::PhilipsSegmentedScarMeshProcessor()
{
	SetName( "PhilipsSegmentedScarMeshProcessor" );
	
	BaseProcessor::SetNumberOfInputs( INPUTS_NUMBER );
	GetInputPort( INPUT_PHILIPSLVSEGMENTEDSCAR )->SetName( "Input Image" );
	GetInputPort( INPUT_PHILIPSLVSEGMENTEDSCAR )->SetDataEntityType( Core::ImageTypeId );
	GetInputPort( INPUT_VOLUMEMESH )->SetName( "Input Tetrahedra" );
	GetInputPort( INPUT_VOLUMEMESH )->SetDataEntityType( Core::VolumeMeshTypeId );

	BaseProcessor::SetNumberOfOutputs( OUTPUTS_NUMBER );
	GetOutputPort( OUTPUT_PHILIPSLVSCARMESH )->SetName( "Output Scar Mesh" );
	GetOutputPort( OUTPUT_PHILIPSLVSCARMESH )->SetDataEntityType( Core::SurfaceMeshTypeId );
	GetOutputPort( OUTPUT_ANNOTATEDVOLUMELVMESH )->SetName( "Output Mesh with Scar annotation" );
	//GetOutputPort( OUTPUT_ANNOTATEDVOLUMELVMESH )->SetDataEntityType( Core::SurfaceMeshTypeId );
}

PhilipsSegmentedScarMeshProcessor::~PhilipsSegmentedScarMeshProcessor()
{
}

template <class T> void PhilipsSegmentedScarMeshProcessor::ComputeNonZeroRangeT( T theType, vtkDataArray* dataArray, double outputRange[2] ) const
{
	// Compute range only if there is data
	vtkDataArrayTemplate<T>* concreteDataArray = static_cast<vtkDataArrayTemplate<T>* >( dataArray );
	int comp = 0;
	T* begin = concreteDataArray->GetPointer( 0 ) + comp;
	T* end = concreteDataArray->GetPointer( 0 ) + comp + concreteDataArray->GetMaxId() + 1;
	if( begin == end )
	{
		return;
	}

	// Compute the range of scalar values.
	int numComp = concreteDataArray->GetNumberOfComponents();
	T range[2] = { numeric_limits<T>::max(), numeric_limits<T>::min() };
	T zero = static_cast<T>( 0 );
	for( T* i = begin; i != end; i += numComp )
	{
		T s = *i;
		if( ( s < range[0] ) && ( s != zero ) )
		{
			range[0] = s;
		}
		if(s > range[1])
		{
			range[1] = s;
		}
	}

	// Store the range.
	outputRange[0] = static_cast<double>( range[0] );
	outputRange[1] = static_cast<double>( range[1] );
}

void PhilipsSegmentedScarMeshProcessor::ComputeNonZeroRange( vtkSmartPointer<vtkImageData> imageDataSP, double outputRange[2] ) const
{
	vtkDataArray* scalarsArray = imageDataSP->GetPointData()->GetScalars();
	switch( scalarsArray->GetDataType() )
	{
		vtkTemplateMacro( ComputeNonZeroRangeT( static_cast<VTK_TT>( 0 ), scalarsArray, outputRange ) );
		default:
		{
			//vtkErrorMacro( "Unknown Scalar type " << scalarsArray->GetDataType() );
		}
	}

/*
	switch( scalarsArray->GetDataType() )
	{
		case VTK_DOUBLE:
		{
			ComputeNonZeroRangeT( static_cast<double>( 0 ), scalarsArray, outputRange );
			break;
		}
		case VTK_FLOAT:
		{
			ComputeNonZeroRangeT( static_cast<float>( 0 ), scalarsArray, outputRange );
			break;
		}
		case VTK_LONG_LONG:
		{
			ComputeNonZeroRangeT( static_cast<long long>( 0 ), scalarsArray, outputRange );
			break;
		}
		case VTK_UNSIGNED_LONG_LONG:
		{
			ComputeNonZeroRangeT( static_cast<unsigned long long>( 0 ), scalarsArray, outputRange );
			break;
		}
		case VTK___INT64:
		{
			ComputeNonZeroRangeT( static_cast<__int64>( 0 ), scalarsArray, outputRange );
			break;
		}
		case VTK_UNSIGNED___INT64:
		{
			ComputeNonZeroRangeT( static_cast<unsigned __int64>( 0 ), scalarsArray, outputRange );
			break;
		}
		case VTK_ID_TYPE:
		{
			ComputeNonZeroRangeT( static_cast<vtkIdType>( 0 ), scalarsArray, outputRange );
			break;
		}
		case VTK_LONG:
		{
			ComputeNonZeroRangeT( static_cast<long>( 0 ), scalarsArray, outputRange );
			break;
		}
		case VTK_UNSIGNED_LONG:
		{
			ComputeNonZeroRangeT( static_cast<unsigned long>( 0 ), scalarsArray, outputRange );
			break;
		}
		case VTK_INT:
		{
			ComputeNonZeroRangeT( static_cast<int>( 0 ), scalarsArray, outputRange );
			break;
		}
		case VTK_UNSIGNED_INT:
		{
			ComputeNonZeroRangeT( static_cast<unsigned int>( 0 ), scalarsArray, outputRange );
			break;
		}
		case VTK_SHORT:
		{
			ComputeNonZeroRangeT( static_cast<short>( 0 ), scalarsArray, outputRange );
			break;
		}
		case VTK_UNSIGNED_SHORT:
		{
			ComputeNonZeroRangeT( static_cast<unsigned short>( 0 ), scalarsArray, outputRange );
			break;
		}
		case VTK_CHAR:
		{
			ComputeNonZeroRangeT( static_cast<char>( 0 ), scalarsArray, outputRange );
			break;
		}
		case VTK_SIGNED_CHAR:
		{
			ComputeNonZeroRangeT( static_cast<signed char>( 0 ), scalarsArray, outputRange );
			break;
		}
		case VTK_UNSIGNED_CHAR:
		{
			ComputeNonZeroRangeT( static_cast<unsigned char>( 0 ), scalarsArray, outputRange );
			break;
		}
	}
*/
}

//! Originally copied from 
// suitesres\src\Plugins\gsp\processors\MaskImageToROIImageProcessor.cxx
void PhilipsSegmentedScarMeshProcessor::CreateROIImageVectorFromImageVector(
	const vector<vtkSmartPointer<vtkImageData> >& inputVectorSP,
	vector<vtkSmartPointer<vtkImageData> >& outputVectorSP ) const
{
	outputVectorSP.clear();

	// First get the maximun a minimun pixel values of the mask image
	double minimunScalarValue =  numeric_limits<double>::max();
	double maximunScalarValue =  numeric_limits<double>::min();
	double minimunScalarValueNotZero =  numeric_limits<double>::max();

	vector<vtkSmartPointer<vtkImageData> >::const_iterator imageIterator;
	for( imageIterator = inputVectorSP.begin(); imageIterator != inputVectorSP.end(); imageIterator++ )
	{
		vtkSmartPointer<vtkImageData> currentMaskImageSP = *imageIterator;

		double range[2];
		ComputeNonZeroRange( currentMaskImageSP, range );

		if( minimunScalarValue > range[0] )
		{
			minimunScalarValue = range[0];
		}
		if( maximunScalarValue < range[1] )
		{
			maximunScalarValue = range[1];
		}
	}

	int numberOfLevels = 2;

	double inputRange = maximunScalarValue - minimunScalarValue;
	double outputRange = static_cast<double>( numberOfLevels - 1 );
	double scale = outputRange / inputRange;
	cout << "minimunScalarValue: " << minimunScalarValue << endl;
	cout << "maximunScalarValue: " << maximunScalarValue << endl;
	cout << "inputRange: " << inputRange << endl;
	cout << "outputRange: " << outputRange << endl;
	printf( "scale: %.10f\n", scale );
	printf( "scale*minimunScalarValue: %.10f\n", scale*minimunScalarValue );
	printf( "scale*maximunScalarValue: %.10f\n", scale*maximunScalarValue );
	printf( "scale*( minimunScalarValue - minimunScalarValue ): %.10f\n", scale*( minimunScalarValue - minimunScalarValue ) );
	printf( "scale*( maximunScalarValue - minimunScalarValue ): %.10f\n", scale*( maximunScalarValue - minimunScalarValue ) );

	// Now conver to unsigned char
	vtkSmartPointer<vtkImageShiftScale> imageShiftScaleFilterSP = vtkSmartPointer<vtkImageShiftScale>::New();
	for( imageIterator = inputVectorSP.begin(); imageIterator != inputVectorSP.end(); imageIterator++ )
	{
		vtkSmartPointer<vtkImageData> currentMaskImage = *imageIterator;
		vtkSmartPointer<vtkImageData> currentROIImage = vtkSmartPointer<vtkImageData>::New();
		imageShiftScaleFilterSP->SetInput( currentMaskImage );
		imageShiftScaleFilterSP->SetShift( ( minimunScalarValue - 1 ) );
		imageShiftScaleFilterSP->SetScale( scale );
		imageShiftScaleFilterSP->SetOutputScalarTypeToUnsignedChar();
		imageShiftScaleFilterSP->Update();
		currentROIImage->DeepCopy( imageShiftScaleFilterSP->GetOutput() );
		outputVectorSP.push_back( currentROIImage );
	}
}

vtkPolyData* PhilipsSegmentedScarMeshProcessor::CreateIsoSurfaceFromVTKImage( vtkImageData* inputImage, int isoValue ) const
{
	bool removeInnerSurface = false;

	vtkPolyData* surface = vtkPolyData::New();
	//-------------------------------------------------------------------
	//Smooth the Volume Data
	//-------------------------------------------------------------------
	//Some "binary" volume would produce stepped surfaces if we do not blur
	//it. The Gaussian kernel specified accomplishes the smoothing. The amount
	//of smoothing is controlled by the Gaussian standard deviation that is
	//independently specified for each axis of the volume data.
	vtkSmartPointer<vtkImageGaussianSmooth> gaussian = vtkSmartPointer<vtkImageGaussianSmooth>::New();
	gaussian->SetDimensionality( 3 );
	gaussian->SetStandardDeviation( 0.5, 0.5, 0.5 );   //could be set by user
	gaussian->SetRadiusFactor( 0.5 );
	gaussian->SetInput( inputImage );
	gaussian->Update();

	//-------------------------------------------------------------------
	//Generate Triangles
	//-------------------------------------------------------------------
	//The filter runs faster if gradient and normal calculations are turned off.
	vtkSmartPointer<vtkMarchingCubes> mcubes = vtkSmartPointer<vtkMarchingCubes>::New();
	mcubes->SetInput( gaussian->GetOutput() );
	mcubes->ComputeScalarsOff();
	mcubes->ComputeGradientsOff();
	mcubes->ComputeNormalsOff();
	mcubes->SetValue( 0, isoValue );
	mcubes->Update();

	if( mcubes->GetOutput()->GetNumberOfPolys() == 0 )
	{
		surface->DeepCopy( mcubes->GetOutput() );
		mcubes->Delete();
		return surface;
	}

	vtkSmartPointer<vtkCleanPolyData> cleanMesh = vtkSmartPointer<vtkCleanPolyData>::New();
	cleanMesh->SetInput( mcubes->GetOutput() );
	cleanMesh->ConvertPolysToLinesOff();
	cleanMesh->SetTolerance( 1.0e-10 );
	cleanMesh->SetAbsoluteTolerance( 1.0e-10 );
	cleanMesh->Update();

	/*
	//-------------------------------------------------------------------
	//Reduce the Number of Triangles
	//-------------------------------------------------------------------
	//There are often many more triangles generated by the isosurfacing algorithm
	//than it is needed for rendering. 
	vtkSmartPointer<vtkDecimatePro> decimator = vtkSmartPointer<vtkDecimatePro>::New();
	decimator->SetInput( mcubes->GetOutput() );
	//decimator->SetMaximumError( 1 );
	decimator->SetTargetReduction( 0.5 ); //can be set by user
	decimator->PreserveTopologyOn();
	decimator->Update();
	*/

	//-------------------------------------------------------------------
	//Remove inner surface
	//-------------------------------------------------------------------

	vtkSmartPointer<vtkPolyData> smoothInput; 
/*
	if( removeInnerSurface )
	{
		vtkSmartPointer< RemoveInnerSurfaceFilter<ItkImageType> > removeInnerSurfaceFilter = vtkSmartPointer< RemoveInnerSurfaceFilter<ItkImageType> >::New();
		removeInnerSurfaceFilter->SetInput(cleanMesh->GetOutput());
		//	removeInnerSurfaceFilter->SetInput(decimator->GetOutput());
		removeInnerSurfaceFilter->SetInputDistanceMap(image);
		removeInnerSurfaceFilter->Update();
		smoothInput = removeInnerSurfaceFilter->GetOutput();
	}
	else
*/
		smoothInput = cleanMesh->GetOutput();

	//-------------------------------------------------------------------
	//Smooth the Triangle Vertices
	//-------------------------------------------------------------------
	//Laplacian smoothing is used, and triangle vertices are adjusted as
	//an average of neighbouring vertices. Models that are heavily decimated
	//can sometimes be improved with additional polygonal smoothing.
	vtkSmartPointer<vtkSmoothPolyDataFilter> smoother = vtkSmartPointer<vtkSmoothPolyDataFilter>::New();
	smoother->SetInput( smoothInput ); //use output of new filter
	smoother->SetFeatureAngle( 60.0 );
	smoother->SetRelaxationFactor( 0.2 );	// before: 0.2
	smoother->SetNumberOfIterations( 10 ); // before: 5
	smoother->GenerateErrorVectorsOff();
	smoother->FeatureEdgeSmoothingOff();
	smoother->BoundarySmoothingOff();
	smoother->SetConvergence( 0.0 );
	smoother->Update();

	//-------------------------------------------------------------------
	//Generate Normals
	//-------------------------------------------------------------------
	vtkSmartPointer<vtkPolyDataNormals> normals = vtkSmartPointer<vtkPolyDataNormals>::New();
	normals->SetInput( smoother->GetOutput() );
	normals->SplittingOff(); //to avoid duplicate vertices due to sharp edges
	normals->Update();
	surface->DeepCopy( normals->GetOutput() );

	return surface;
}

void PhilipsSegmentedScarMeshProcessor::CreateIsoSurfaceVectorFromVTKImageVector(
	const std::vector<vtkSmartPointer<vtkImageData> >& inputImageSPVector,
	std::vector<vtkSmartPointer<vtkPolyData> >& outputVectorSP) const
{
	vector<vtkSmartPointer<vtkImageData> >::const_iterator imageSPIterator;
	for( imageSPIterator = inputImageSPVector.begin();
	     imageSPIterator != inputImageSPVector.end();
	     imageSPIterator++ )
	{
		vtkPolyData* surface  = CreateIsoSurfaceFromVTKImage( *imageSPIterator, 2 );
		outputVectorSP.push_back( vtkSmartPointer<vtkPolyData>( surface ) );
	}
}

void PhilipsSegmentedScarMeshProcessor::ExtractScar()
{
	// The code of this processor is mostly copied from	the method GetSurface of
	// gimias130\src\Modules\MeshLib\libmodules\meMeshFilters\include\meBinImageToMeshFilter.h
	// This method is used in the Threshold processor at 
	// suitesres\src\Plugins\gsp\processors\ThresholdProcessor.cpp
	// The issue is that the processor gets the Gimias image (in VTK), and transforms it in
	// ITK using a macro. Then, the processor calls the method that gets an ITK image and
	// transforms it to VTK to extract the polydata. This is a waste of time, so I prefer
	// rather put it directly here all the code. Of course, the good solution should be to
	// have a method that with a VTK image returns the polydata. This method could then be
	// called both from this processor and from the method that uses ITK image as input.

	Core::DataEntity::Pointer inputDE = GetInputDataEntity( INPUT_PHILIPSLVSEGMENTEDSCAR );
	if( inputDE.IsNull() )
	{
		return;
	}
	// Get the image
	vector<vtkSmartPointer<vtkImageData> > inputImageSPVector;
	GetProcessingData<vtkSmartPointer<vtkImageData> >( INPUT_PHILIPSLVSEGMENTEDSCAR, inputImageSPVector );

	vector<vtkSmartPointer<vtkPolyData> > outputPolyDataSPVector;

	vector<vtkSmartPointer<vtkImageData> > inputROIImageSPVector;
	Core::DataEntityType intputType = inputDE->GetType();
	if( intputType == Core::ImageTypeId )
	{
		// Is not ROI image, convert to ROI image
		CreateROIImageVectorFromImageVector( inputImageSPVector, inputROIImageSPVector );
		CreateIsoSurfaceVectorFromVTKImageVector( inputROIImageSPVector, outputPolyDataSPVector );
	}
	else
	{
		CreateIsoSurfaceVectorFromVTKImageVector( inputImageSPVector, outputPolyDataSPVector );
	}

	// Set the output to the output of this processor
	UpdateOutput<vtkSmartPointer<vtkPolyData> >( OUTPUT_PHILIPSLVSCARMESH, outputPolyDataSPVector, "LV_Scar_mesh", false, GetInputDataEntity( INPUT_PHILIPSLVSEGMENTEDSCAR ) );
}

void PhilipsSegmentedScarMeshProcessor::MapScar()
{
	Core::DataEntity::Pointer inputImageDE = GetInputDataEntity( INPUT_PHILIPSLVSEGMENTEDSCAR );
	if( inputImageDE.IsNull() )
	{
		return;
	}
	
	Core::DataEntity::Pointer inputVolumeMeshDE = GetInputDataEntity( OUTPUT_ANNOTATEDVOLUMELVMESH );
	if( inputVolumeMeshDE.IsNull() )
	{
		return;
	}

	// Get the image
	vector<vtkSmartPointer<vtkImageData> > inputImageSPVector;
	GetProcessingData<vtkSmartPointer<vtkImageData> >( INPUT_PHILIPSLVSEGMENTEDSCAR, inputImageSPVector );

	vector<vtkSmartPointer<vtkUnstructuredGrid> > inputVolumeMeshSPVector;
	GetProcessingData<vtkSmartPointer<vtkUnstructuredGrid> >( OUTPUT_ANNOTATEDVOLUMELVMESH, inputVolumeMeshSPVector );


	vector<vtkSmartPointer<vtkImageData> > inputROIImageSPVector;
	Core::DataEntityType intputType = inputImageDE->GetType();
	if( intputType == Core::ImageTypeId )
	{
		// Is not ROI image, convert to ROI image
		CreateROIImageVectorFromImageVector( inputImageSPVector, inputROIImageSPVector );
	}
	else
	{
		inputROIImageSPVector = inputImageSPVector;
	}


	vtkSmartPointer<vtkProbeFilter> probeFilterSP = vtkSmartPointer<vtkProbeFilter>::New();
	probeFilterSP->SetInput( inputVolumeMeshSPVector[0] );
	probeFilterSP->SetSource( inputROIImageSPVector[0] );
	probeFilterSP->SpatialMatchOn();
	probeFilterSP->Update();


	vtkSmartPointer<vtkUnstructuredGrid> probedVolumeMeshSP = vtkSmartPointer<vtkUnstructuredGrid>::New();
	probedVolumeMeshSP->DeepCopy( inputVolumeMeshSPVector[0] );
	probedVolumeMeshSP->GetPointData()->AddArray( probeFilterSP->GetOutput()->GetPointData()->GetArray( "scalars" ) );
	vtkDataArray* scarDataArray = probedVolumeMeshSP->GetPointData()->GetArray( "scalars" );
	scarDataArray->SetName( "scar" );
	vtkUnsignedCharArray* castedScarDataArray = dynamic_cast<vtkUnsignedCharArray*>( scarDataArray );

	if( castedScarDataArray != NULL )
	{
		int currentScalar;
		for( int tupleID = 0; tupleID < castedScarDataArray->GetNumberOfTuples(); tupleID++ )
		{
			currentScalar = castedScarDataArray->GetValue( tupleID );
			if( currentScalar < 1 )
			{
				currentScalar = 1;
			}
			if( currentScalar > 2 )
			{
				currentScalar = 2;
			}
			if( ( currentScalar > 1 ) && ( currentScalar < 2 ) )
			{
cout << "Should not happen" << endl;
			}
			// Decreement by 1 the scalar value, as we want the scar/no scar
			// mapped between 0 and 1, and not between 1 and 2
			castedScarDataArray->SetValue( tupleID, currentScalar - 1 );
		}
	}

/*
	//if( scar != NULL )
	if( false )
	{
		int scalarBetweenValue = 80 + 39 / 2;
		int currentScalar;
		for( int tupleID = 0; tupleID < scar->GetNumberOfTuples(); tupleID++ )
		{
			currentScalar = scar->GetValue( tupleID );
			if( currentScalar < 39 )
			{
				currentScalar = 39;
			}
			if( currentScalar > 80 )
			{
				currentScalar = 80;
			}
			if( ( currentScalar > 39 ) && ( currentScalar < 80 ) )
			{
				if( currentScalar <= scalarBetweenValue )
				{
					currentScalar = 39;
				}
				else
				{
					currentScalar = 80;
				}
			}
			scar->SetValue( tupleID, currentScalar );
		}
	}
*/

	// Set the output to the output of this processor
	UpdateOutput( OUTPUT_ANNOTATEDVOLUMELVMESH, probedVolumeMeshSP, "LV_Anotated_Scar", false, 1, GetInputDataEntity( INPUT_PHILIPSLVSEGMENTEDSCAR ) );
}

void PhilipsSegmentedScarMeshProcessor::Update()
{
	switch( m_Parameters.m_ProcessorMethod )
	{
		case PM_EXTRACTSCAR:
		{
			ExtractScar();
			break;
		}
		case PM_MAPSCAR:
		{
			MapScar();
			break;
		}
	}
}

PhilipsSegmentedScarMeshProcessor::PSSMPParameters& PhilipsSegmentedScarMeshProcessor::GetParameters()
{
	return m_Parameters;
}

} // namespace gsp

