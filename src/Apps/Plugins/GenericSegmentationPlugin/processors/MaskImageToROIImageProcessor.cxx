/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#include "MaskImageToROIImageProcessor.h"

#include <string>
#include <iostream>

#include "coreReportExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityHelper.txx"
#include "coreKernel.h"
#include "coreVTKPolyDataHolder.h"
#include "coreVTKImageDataHolder.h"

// VTK
#include "vtkSmartPointer.h"
#include "vtkImageShiftScale.h"
#include "vtkPointData.h"
#include "vtkDataArray.h"


#include <vector>

#ifdef _MSC_VER
	#define INT64 __int64 // Windows
#else
	// not Windows (linux/unix)
	#include <stdint.h>
	typedef int64_t INT64;// not Windows
#endif

using namespace std;

namespace GenericSegmentationPlugin
{

MaskImageToROIImageProcessor::MaskImageToROIImageProcessor()
{
	SetName( "MaskImageToROIImageProcessor" );
	
	BaseProcessor::SetNumberOfInputs( INPUTS_NUMBER );
	GetInputPort( INPUT_ROIIMAGE )->SetName( "Input ROI Image" );
	GetInputPort( INPUT_ROIIMAGE )->SetDataEntityType( Core::ImageTypeId | Core::ROITypeId );
	GetInputPort( INPUT_MASKIMAGE )->SetName( "Input Mask Image " );
	GetInputPort( INPUT_MASKIMAGE )->SetDataEntityType( Core::ImageTypeId );

	BaseProcessor::SetNumberOfOutputs( OUTPUTS_NUMBER );
	GetOutputPort( OUTPUT_ROIIMAGE )->SetDataEntityType( Core::ImageTypeId | Core::ROITypeId );
	GetOutputPort( OUTPUT_MASKIMAGE )->SetDataEntityType( Core::ImageTypeId );

	m_NumberOfLevels = 1;
}

MaskImageToROIImageProcessor::~MaskImageToROIImageProcessor()
{
}

void MaskImageToROIImageProcessor::SetNumberOfLevels( int numberOfLevels )
{
	m_NumberOfLevels = numberOfLevels;
}

int MaskImageToROIImageProcessor::GetNumberOfLevels()
{
	return m_NumberOfLevels;
}

void MaskImageToROIImageProcessor::CreateMaskImageFromROIImage()
{
	vector<Core::vtkImageDataPtr> inputVector;
	GetProcessingData<Core::vtkImageDataPtr>( INPUT_ROIIMAGE, inputVector );


	// here goes the filter or the functions that determine the processor
	// the output should go in the update functions

	vector<Core::vtkImageDataPtr> outputVector;
	vtkSmartPointer<vtkImageShiftScale> imageShiftScaleFilterSP = vtkSmartPointer<vtkImageShiftScale>::New();
	vector<Core::vtkImageDataPtr>::iterator imageIterator;
	for( imageIterator = inputVector.begin(); imageIterator != inputVector.end(); imageIterator++ )
	{
		Core::vtkImageDataPtr currentMaskImage = *imageIterator;
		Core::vtkImageDataPtr currentROIImage = Core::vtkImageDataPtr::New();
		//currentROIImage->Initialize();
		imageShiftScaleFilterSP->SetInput( currentMaskImage );
		imageShiftScaleFilterSP->SetOutputScalarTypeToUnsignedChar();
		imageShiftScaleFilterSP->Update();
		currentROIImage->DeepCopy( imageShiftScaleFilterSP->GetOutput() );
		//currentROIImage->DeepCopy( currentMaskImage );
		outputVector.push_back( currentROIImage );
	}

	// Set the output to the output of this processor
	UpdateOutput<Core::vtkImageDataPtr>( OUTPUT_MASKIMAGE, outputVector, "Mask_Image", false );
}

template <class T> void ComputeNonZeroRange( T theType, vtkDataArray* dataArray, double outputRange[2] )
{
	// Compute range only if there is data
	vtkDataArrayTemplate<T>* concreteDataArray = static_cast<vtkDataArrayTemplate<T>* >( dataArray );
	int comp = 0;
	T* begin = concreteDataArray->GetPointer( 0 ) + comp;
	T* end = concreteDataArray->GetPointer( 0 ) + comp + concreteDataArray->GetMaxId() + 1;
	if( begin == end )
	{
		return;
	}

	// Compute the range of scalar values.
	int numComp = concreteDataArray->GetNumberOfComponents();
	T range[2] = { numeric_limits<T>::max(), numeric_limits<T>::min() };
	T zero = static_cast<T>( 0 );
	for( T* i = begin; i != end; i += numComp )
	{
		T s = *i;
		if( ( s < range[0] ) && ( s != zero ) )
		{
			range[0] = s;
		}
		if(s > range[1])
		{
			range[1] = s;
		}
	}

	// Store the range.
	outputRange[0] = static_cast<double>( range[0] );
	outputRange[1] = static_cast<double>( range[1] );
}

void MaskImageToROIImageProcessor::CreateROIImageVectorFromImageVector(
    const vector<vtkSmartPointer<vtkImageData> >& inputVectorSP,
    vector<vtkSmartPointer<vtkImageData> >& outputVectorSP ) const
{
	outputVectorSP.clear();

	// First get the maximun a minimun pixel values of the mask image
	double minimunScalarValue =  numeric_limits<double>::max();
	double maximunScalarValue =  numeric_limits<double>::min();
	double minimunScalarValueNotZero =  numeric_limits<double>::max();

	vector<vtkSmartPointer<vtkImageData> >::const_iterator imageIterator;
	for( imageIterator = inputVectorSP.begin(); imageIterator != inputVectorSP.end(); imageIterator++ )
	{
		vtkSmartPointer<vtkImageData> currentMaskImageSP = *imageIterator;

		double range[2];
		//currentMaskImage->GetScalarRange( range );
		vtkDataArray* scalarsArray = currentMaskImageSP->GetPointData()->GetScalars();
		switch( scalarsArray->GetDataType() )
		{
			case VTK_DOUBLE:
			{
				ComputeNonZeroRange( static_cast<double>( 0 ), scalarsArray, range );
				break;
			}
			case VTK_FLOAT:
			{
				ComputeNonZeroRange( static_cast<float>( 0 ), scalarsArray, range );
				break;
			}
			case VTK_LONG_LONG:
			{
				ComputeNonZeroRange( static_cast<long long>( 0 ), scalarsArray, range );
				break;
			}
			case VTK_UNSIGNED_LONG_LONG:
			{
				ComputeNonZeroRange( static_cast<unsigned long long>( 0 ), scalarsArray, range );
				break;
			}
#ifdef VTK_TYPE_USE___INT64
			case VTK___INT64:
			{
				ComputeNonZeroRange( static_cast<INT64>( 0 ), scalarsArray, range );
				break;
			}
			case VTK_UNSIGNED___INT64:
			{
				ComputeNonZeroRange( static_cast<unsigned INT64>( 0 ), scalarsArray, range );
				break;
			}
#endif
			case VTK_ID_TYPE:
			{
				ComputeNonZeroRange( static_cast<vtkIdType>( 0 ), scalarsArray, range );
				break;
			}
			case VTK_LONG:
			{
				ComputeNonZeroRange( static_cast<long>( 0 ), scalarsArray, range );
				break;
			}
			case VTK_UNSIGNED_LONG:
			{
				ComputeNonZeroRange( static_cast<unsigned long>( 0 ), scalarsArray, range );
				break;
			}
			case VTK_INT:
			{
				ComputeNonZeroRange( static_cast<int>( 0 ), scalarsArray, range );
				break;
			}
			case VTK_UNSIGNED_INT:
			{
				ComputeNonZeroRange( static_cast<unsigned int>( 0 ), scalarsArray, range );
				break;
			}
			case VTK_SHORT:
			{
				ComputeNonZeroRange( static_cast<short>( 0 ), scalarsArray, range );
				break;
			}
			case VTK_UNSIGNED_SHORT:
			{
				ComputeNonZeroRange( static_cast<unsigned short>( 0 ), scalarsArray, range );
				break;
			}
			case VTK_CHAR:
			{
				ComputeNonZeroRange( static_cast<char>( 0 ), scalarsArray, range );
				break;
			}
			case VTK_SIGNED_CHAR:
			{
				ComputeNonZeroRange( static_cast<signed char>( 0 ), scalarsArray, range );
				break;
			}
			case VTK_UNSIGNED_CHAR:
			{
				ComputeNonZeroRange( static_cast<unsigned char>( 0 ), scalarsArray, range );
				break;
			}
		}

		if( minimunScalarValue > range[0] )
		{
			minimunScalarValue = range[0];
		}
		if( maximunScalarValue < range[1] )
		{
			maximunScalarValue = range[1];
		}
	}

	double inputRange = maximunScalarValue - minimunScalarValue;
	double outputRange = static_cast<double>( m_NumberOfLevels - 1 );
	double scale = outputRange / inputRange;
cout << "minimunScalarValue: " << minimunScalarValue << endl;
cout << "maximunScalarValue: " << maximunScalarValue << endl;
cout << "inputRange: " << inputRange << endl;
cout << "outputRange: " << outputRange << endl;
printf( "scale: %.10f\n", scale );
printf( "scale*minimunScalarValue: %.10f\n", scale*minimunScalarValue );
printf( "scale*maximunScalarValue: %.10f\n", scale*maximunScalarValue );
printf( "scale*( minimunScalarValue - minimunScalarValue ): %.10f\n", scale*( minimunScalarValue - minimunScalarValue ) );
printf( "scale*( maximunScalarValue - minimunScalarValue ): %.10f\n", scale*( maximunScalarValue - minimunScalarValue ) );

	// Now conver to unsigned char
	vtkSmartPointer<vtkImageShiftScale> imageShiftScaleFilterSP = vtkSmartPointer<vtkImageShiftScale>::New();
	for( imageIterator = inputVectorSP.begin(); imageIterator != inputVectorSP.end(); imageIterator++ )
	{
		vtkSmartPointer<vtkImageData> currentMaskImage = *imageIterator;
		vtkSmartPointer<vtkImageData> currentROIImage = vtkSmartPointer<vtkImageData>::New();
		imageShiftScaleFilterSP->SetInput( currentMaskImage );
		imageShiftScaleFilterSP->SetShift( ( minimunScalarValue - 1 ) );
		imageShiftScaleFilterSP->SetScale( scale );
		imageShiftScaleFilterSP->SetOutputScalarTypeToUnsignedChar();
		imageShiftScaleFilterSP->Update();
		currentROIImage->DeepCopy( imageShiftScaleFilterSP->GetOutput() );
		outputVectorSP.push_back( currentROIImage );
	}
}

void MaskImageToROIImageProcessor::Update()
{
	vector<vtkSmartPointer<vtkImageData> > inputVector;
	GetProcessingData<vtkSmartPointer<vtkImageData> >( INPUT_MASKIMAGE, inputVector );

	// here goes the filter or the functions that determine the processor
	// the output should go in the update functions
	vector<vtkSmartPointer<vtkImageData> > outputVector;
	CreateROIImageVectorFromImageVector( inputVector, outputVector );

	UpdateOutput<vtkSmartPointer<vtkImageData> >( OUTPUT_ROIIMAGE, outputVector, "ROI_Image", false );

	// Set the output t
	// Add levels tag
	blTagMap::Pointer levels;
	blTag::Pointer tag = GetOutputDataEntity( OUTPUT_ROIIMAGE )->GetMetadata()->FindTagByName( "MultiROILevel" );
	if( tag.IsNotNull( ) )
	{
		tag->GetValue( m_NumberOfLevels );
cout << "Setting number of levels: " << m_NumberOfLevels << endl;
	}
	else
	{
cout << "Creating tag MultiROILevel" << endl;
		blTagMap::Pointer ROIlevels = blTagMap::New();
		for( int i = 0; i < m_NumberOfLevels; i++ )
		{
			std::ostringstream  oss;
			oss << "Level_" << i;
			ROIlevels->AddTag( oss.str(), i );
		}
		GetOutputDataEntity( OUTPUT_ROIIMAGE )->GetMetadata()->AddTag( "MultiROILevel", ROIlevels );
	}
}

}   // namespace GenericSegmentationPlugin
