/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _GenericSegmentationPluginTransformationBoxProcessor_H
#define _GenericSegmentationPluginTransformationBoxProcessor_H

#include "coreBaseProcessor.h"

#include "blLinearAlgebraTypes.h"
#include "coreVTKPolyDataHolder.h"

class vtkPlane;


namespace GenericSegmentationPlugin
{
/**
Processor for applying rigid transformation to images and meshes

\ingroup GenericSegmentationPlugin
\author Luigi Carotenuto
\date 11 oct 2012
*/
class TransformationBoxProcessor : public Core::BaseProcessor
{
public:

	typedef enum
	{
		INPUT_0,
		INPUT_NUM
	}INPUT_TYPE;

	typedef enum
	{
		OUTPUT_0,
		OUTPUTS_NUMBER
	}OUTPUT_TYPE;

public:
	//!
	coreProcessor(TransformationBoxProcessor, Core::BaseProcessor);
	
	//! Call library to perform operation
	void Update( );
	
	//! Set the current transformation
	void SetTransform(vtkTransform *t){m_transform = t;};
	
	//! Get the current transformation
	vtkTransform *GetTransform(){return m_transform;};
	
	//! apply the processor to current timesteps are to all (default only current)
	void SetAllTimeSteps(bool all){m_allTimeSteps = all;};
	
	//! return current value of AllTimeSteps
	bool GetAllTimeSteps(){return m_allTimeSteps;};
	
	//!
	void SetAllChildren( bool allChildren );

private:
	//!
	TransformationBoxProcessor();

	//!
	~TransformationBoxProcessor();

	//! Purposely not implemented
	TransformationBoxProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );
	
	//!
	void UpdateDataEntity( Core::DataEntity::Pointer dataEntity );

private:
	//! process all timesteps
	bool m_allTimeSteps;
	
	//!
	bool m_allChildren;

	//!
	vtkTransform* m_transform;
};
    
}   // namespace GenericSegmentationPlugin

#endif //_GenericSegmentationPluginTransformationBoxProcessor_H
