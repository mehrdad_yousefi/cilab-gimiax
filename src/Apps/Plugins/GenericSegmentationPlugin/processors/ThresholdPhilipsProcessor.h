/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _ThresholdPhilipsProcessor_H
#define _ThresholdPhilipsProcessor_H

#include "coreBaseProcessor.h"

#include "itkImage.h"

// Forward declarations
struct CustomFiltersParams;

/**
Processor for 

\ingroup GenericSegmentationPlugin
\date 16 feb 2009
\author David Lucena
\date 03 11 2010 (dd mm yyyy)
*/
class ThresholdPhilipsProcessor : public Core::BaseProcessor
{
public:

	class TPPParameters
	{
		// public methods
	public:
		TPPParameters() : m_bOutputRightVentricle( false )
			{};
		virtual ~TPPParameters() {};
		// public members
	public:
		bool m_bOutputRightVentricle;
	};


	typedef enum
	{
		INPUT_MESH,
		INPUTS_NUMBER
	} INPUT_TYPE;

	typedef enum
	{
		OUTPUT_LV_EPI,
		OUTPUT_LV_EPI_RV,
		OUTPUT_APEX,
		OUTPUT_LV_ENDO,
		OUTPUT_LEFT_ATRIUM,
		OUTPUT_RV_EPI,
		OUTPUT_RV_ENDO,
		OUTPUT_LV_COMPLETE,
		OUTPUT_RV_COMPLETE,
		OUTPUT_LV_RV_COMPLETE,
		OUTPUT_DEBUG,
		OUTPUT_DEBUG_1,
		OUTPUT_DEBUG_2,
		OUTPUTS_NUMBER
	} OUTPUT_TYPE;
public:
	//!
	coreProcessor(ThresholdPhilipsProcessor, Core::BaseProcessor);
	
	//! Call library to perform operation
	void Update();

	TPPParameters& GetParameters();

private:
	//!
	ThresholdPhilipsProcessor();
	//!
	~ThresholdPhilipsProcessor();

	//! Purposely not implemented
	ThresholdPhilipsProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );


	vtkPolyData* ApplyAllCustomFilters( vtkPolyData* pInPolyData, CustomFiltersParams* pCustomFiltersParams ) const;
	vtkPolyData* RemoveFlaps( vtkPolyData* pInPolyData, bool updateOutput = false );

	vtkPolyData* MultiThreshold( const std::vector<int>& orgRegionIDs, const std::vector<int>& dstSubpartsIDs,
	                     const std::vector<int>& processorOuputs, const std::vector<std::string>& processorOutputsNames,
	                     vtkPolyData* pInPolyData, int appendedSetsLoopSubdivisionCount, Core::DataEntity::Pointer fatherDataEntity = NULL );

	TPPParameters m_Parameters;
};

#endif //_CandidateToolsPluginThresholdPhilipsProcessor_H
