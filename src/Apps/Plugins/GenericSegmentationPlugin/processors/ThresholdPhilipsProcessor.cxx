/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "ThresholdPhilipsProcessor.h"

// std
#include <string>
#include <iostream>
#include <sstream>
#include <list>

// Core
#include "coreReportExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityHelper.txx"
#include "coreKernel.h"
#include "coreVTKPolyDataHolder.h"

// VTK
#include "vtkSmartPointer.h"
#include "vtkCellData.h"
#include "vtkPointData.h"
#include "vtkLine.h"
#include "vtkArrayCalculator.h"
#include "vtkProgrammableAttributeDataFilter.h"
#include "vtkMergePoints.h"
#include "vtkFeatureEdges.h"
#include "vtkCleanPolyData.h"
#include "vtkGeometryFilter.h"
#include "vtkAppendPolyData.h"
#include "vtkPolyDataNormals.h"
#include "vtkLinearExtrusionFilter.h"
#include "vtkExtractSelection.h"
#include "vtkExtractSelectedPolyDataIds.h"
#include "vtkSelectionSource.h"
#include "vtkDataSetSurfaceFilter.h"
#include "vtkSelectionNode.h"
#include "vtkDataSetTriangleFilter.h"
#include "vtkTriangleFilter.h"
#include "vtkLoopSubdivisionFilter.h"

// BaseLib
#include "blShapeUtils.h"

using namespace std;

const int PHILIPS_REGION_ID_LV_EPI      = 1;
const int PHILIPS_REGION_ID_APEX        = 15;
const int PHILIPS_REGION_ID_LV_EPI_RV   = 25;
const int PHILIPS_REGION_ID_LEFT_ATRIUM = 35;
const int PHILIPS_REGION_ID_LV_ENDO     = 170;

const int PHILIPS_REGION_ID_RV_EPI      = 90;
const int PHILIPS_REGION_ID_RIGHT_ATRIUM= 45;


const double CRT_SUBPART_ID_ENDOCARDIUM = 1.0;
const double CRT_SUBPART_ID_EPICARDIUM = 2.0;
const double CRT_SUBPART_ID_RV_ENDOCARDIUM = 3.0;
const double CRT_SUBPART_ID_RV_EPICARDIUM = 4.0;


struct CustomFiltersParams
{
	int m_PhilipsRegionID;
	double m_SubPartID;
};



ThresholdPhilipsProcessor::ThresholdPhilipsProcessor( )
{
	SetName( "ThresholdPhilipsProcessor" );
	
	BaseProcessor::SetNumberOfInputs( INPUTS_NUMBER );
	GetInputPort( INPUT_MESH )->SetName( "Input Surface" );
	GetInputPort( INPUT_MESH )->SetDataEntityType( Core::SurfaceMeshTypeId );
	BaseProcessor::SetNumberOfOutputs( OUTPUTS_NUMBER );
}

ThresholdPhilipsProcessor::~ThresholdPhilipsProcessor()
{
}

struct NullAlgorithmTag {};
struct DataSetAlgorithmTag {};
struct PolyDataAlgorithmTag {};

/*
template <typename T, typename S > struct AlgorithmTraits
{
	typedef NullAlgorithmTag AlgorithmCategory;
};
*/

template <typename T, typename S = typename T::Superclass > struct AlgorithmTraits
{
	typedef NullAlgorithmTag AlgorithmCategory;
};

// Specialization for Algorithms that derive from vtkPolyDataAlgorithm
template <typename T> struct AlgorithmTraits<T, vtkPolyDataAlgorithm>
{
	typedef PolyDataAlgorithmTag AlgorithmCategory;
};

// Specialization for Algorithms that derive from vtkDataSetAlgorithm
template <typename T> struct AlgorithmTraits<T, vtkDataSetAlgorithm>
{
	typedef DataSetAlgorithmTag AlgorithmCategory;
};

template <typename S> struct AlgorithmTraits<vtkPolyDataAlgorithm, S>
{
	typedef DataSetAlgorithmTag AlgorithmCategory;
};




vtkPolyData* DoApplyAlgorithm( vtkPolyData* pInPolyData, vtkPolyDataAlgorithm& algorithm, PolyDataAlgorithmTag, bool deleteInput = true )
{
	wxASSERT( pInPolyData );

	vtkPolyData* pOutPolyData = NULL;

	algorithm.SetInput( pInPolyData );
	algorithm.Update();

	if( algorithm.GetOutput()->GetNumberOfPoints() )
	{
		if( deleteInput )
		{
			pInPolyData->Delete();
		}
		pOutPolyData = vtkPolyData::New();
		pOutPolyData->DeepCopy( algorithm.GetOutput() );
	}

	return pOutPolyData;
}

//vtkDataSet* DoApplyAlgorithm( vtkDataSet* pInPolyData, vtkDataSetAlgorithm& algorithm, DataSetAlgorithmTag, bool deleteInput = true )
vtkPolyData* DoApplyAlgorithm( vtkPolyData* pInPolyData, vtkDataSetAlgorithm& algorithm, DataSetAlgorithmTag, bool deleteInput = true )
{
	wxASSERT( pInPolyData );

	vtkPolyData* pOutPolyData = NULL;

	algorithm.SetInput( pInPolyData );
	algorithm.Update();

	if( algorithm.GetOutput()->GetNumberOfPoints() )
	{
		if( deleteInput )
		{
			pInPolyData->Delete();
		}
		pOutPolyData = vtkPolyData::New();
		pOutPolyData->DeepCopy( algorithm.GetOutput() );
	}

	return pOutPolyData;
}

template <typename D,typename A> D* ApplyAlgorithm( D* pInData, A& algorithm, bool deleteInput = true )
{
	wxASSERT( pInData );

	typename AlgorithmTraits<A>::AlgorithmCategory algorithmCategory;

	D* pOutData = NULL;

	pOutData = DoApplyAlgorithm( pInData, algorithm, algorithmCategory, deleteInput );

	return pOutData;
}

template <typename D,typename A> D* ApplyAlgorithm( vtkSmartPointer<D>& pInData, A& algorithm, bool deleteInput = true )
{
	return ApplyAlgorithm( pInData.GetPointer(), algorithm, deleteInput );
}

vtkPolyData* ApplyPolyDataAlgorithm( vtkPolyData* pInPolyData, vtkPolyDataAlgorithm& algorithm, bool deleteInput = true )
{
	wxASSERT( pInPolyData );

	vtkPolyData* pOutPolyData = NULL;

	algorithm.SetInput( pInPolyData );
	algorithm.Update();

	if( algorithm.GetOutput()->GetNumberOfPoints() )
	{
		if( deleteInput )
		{
			pInPolyData->Delete();
		}
		pOutPolyData = vtkPolyData::New();
		pOutPolyData->DeepCopy( algorithm.GetOutput() );
	}

	return pOutPolyData;
}

void SetSubpartID( vtkPolyData* inOutPolyData, int subPartID )
{
	vtkSmartPointer<vtkArrayCalculator> arrayCalculatorFilter = vtkSmartPointer<vtkArrayCalculator>::New();

	stringstream functionValueStringStream;
	functionValueStringStream << subPartID;
	arrayCalculatorFilter->SetFunction( functionValueStringStream.str().c_str() );
	arrayCalculatorFilter->SetReplacementValue( subPartID );
	arrayCalculatorFilter->SetResultArrayName( "subpartID" );
	arrayCalculatorFilter->SetInput( inOutPolyData );
	arrayCalculatorFilter->Update();

	if( arrayCalculatorFilter->GetOutput()->GetNumberOfPoints() )
	{
		inOutPolyData->DeepCopy( arrayCalculatorFilter->GetOutput() );
	}
}

vtkPolyData* ThresholdPhilipsProcessor::ApplyAllCustomFilters( vtkPolyData* pInPolyData, CustomFiltersParams* pCustomFiltersParams ) const
{
	wxASSERT( pInPolyData );

	vtkPolyData* pOutPolyData;
	vtkSmartPointer<vtkCleanPolyData> cleanPolyDataFilter = vtkSmartPointer<vtkCleanPolyData>::New();
	vtkSmartPointer<vtkArrayCalculator> arrayCalculatorFilter = vtkSmartPointer<vtkArrayCalculator>::New();

	vtkPolyData* pTempPolyData = blShapeUtils::ShapeUtils::GetShapeRegion(
		pInPolyData, pCustomFiltersParams->m_PhilipsRegionID,
		pCustomFiltersParams->m_PhilipsRegionID, "SubPartID" );

	// Remove the cell array we do not want
	vtkCellData* pInputCellData = pTempPolyData->GetCellData();
	pInputCellData->RemoveArray( "SubPartID" );

	//pTempPolyData = ApplyAlgorithm( pTempPolyData, *cleanPolyDataFilter );

	cleanPolyDataFilter->SetInput( pTempPolyData );
	cleanPolyDataFilter->Update();

#define REMOVE_FLAPS 0
#if( REMOVE_FLAPS )
	pTempPolyData->Delete();
	pTempPolyData = RemoveFlaps( cleanPolyDataFilter->GetOutput(), 
		pCustomFiltersParams->m_PhilipsRegionID == PHILIPS_REGION_ID_LV_ENDO );
#endif

	stringstream functionValueStringStream;
	functionValueStringStream << pCustomFiltersParams->m_SubPartID;
	arrayCalculatorFilter->SetFunction( functionValueStringStream.str().c_str() );
	arrayCalculatorFilter->SetReplacementValue( pCustomFiltersParams->m_SubPartID );
	//arrayCalculatorFilter->SetResultArrayName( "SubPartID" );
	arrayCalculatorFilter->SetResultArrayName( "subpartID" );
#if( REMOVE_FLAPS )
	arrayCalculatorFilter->SetInput( pTempPolyData );
#else
	arrayCalculatorFilter->SetInputConnection( cleanPolyDataFilter->GetOutputPort() );
#endif
	arrayCalculatorFilter->Update();

	if( arrayCalculatorFilter->GetOutput()->GetNumberOfPoints() )
	{
		pTempPolyData->Delete();
		pOutPolyData = vtkPolyData::New();
		pOutPolyData->DeepCopy( arrayCalculatorFilter->GetOutput() );
	}

	return pOutPolyData;
}

void PrintConflictiveEdges( list<vtkLine*>& edgesList )
{
	list<vtkLine*>::iterator edgesIterator;
	vtkLine* pCurrentEdgeCell;
	vtkIdType currentEdgePointsIds[2];
	vtkIdType previousEdgePointsIds[2];
	previousEdgePointsIds[0] = -1;
	previousEdgePointsIds[1] = -1;
	stringstream outputLineStreamBuffer;
	string outputLine = "";
	for( edgesIterator = edgesList.begin(); edgesIterator != edgesList.end();  edgesIterator++ )
	{
		pCurrentEdgeCell = *edgesIterator;
		currentEdgePointsIds[0] = pCurrentEdgeCell->GetPointIds()->GetId( 0 );
		currentEdgePointsIds[1] = pCurrentEdgeCell->GetPointIds()->GetId( 1 );

		if( currentEdgePointsIds[0] != previousEdgePointsIds[1] )
		{
			outputLineStreamBuffer << endl << currentEdgePointsIds[0];
		}
		outputLineStreamBuffer << "->";
		outputLineStreamBuffer << currentEdgePointsIds[1];

		previousEdgePointsIds[0] = currentEdgePointsIds[0];
		previousEdgePointsIds[1] = currentEdgePointsIds[1];
	}
	outputLine = outputLineStreamBuffer.str();
	cout << outputLine << endl;
}

// Insert edges in order
// Suppose we have an edge A already in the list, with points a0, a1 and
// we want to insert an edge B with points b1, b2, then, we have
// 1) if b0 == a1,                          B is inserted before A
// 2) if b1 == a0,                          B is inserted after A
// 3) if ( b0 != a1 ) &&
//       ( b1 != a0 )                       B is inserted at the end (disjoint edges)
// 4) if ( ( b0 == a0 ) && ( b1 == a1 ) ||
//         ( b0 == a1 ) && ( b1 == a0 ) )   B is not inserted

void InsertEdgeOrdered( vtkIdList*& cellsIds, vtkIdType cellId, vtkIdList*& edgesIds, list<vtkLine*>& edgesList, vtkCellArray*& edges, double edgePointsCoords[2][3], vtkIdType edgePointsIds[2] )
{
	vtkLine* pEdgeCell;

	if( !edgesList.empty() )
	{
		list<vtkLine*>::iterator edgesIterator;
		int insertionLocation = 0;
		bool edgeInserted = false;
		vtkLine* pCurrentEdgeCell;
		vtkIdType currentEdgePointsIds[2];
		for( edgesIterator = edgesList.begin();
			( edgesIterator != edgesList.end() ) && !edgeInserted; insertionLocation++, edgesIterator++ )
		{
			pCurrentEdgeCell = *edgesIterator;
			currentEdgePointsIds[0] = pCurrentEdgeCell->GetPointIds()->GetId( 0 );
			currentEdgePointsIds[1] = pCurrentEdgeCell->GetPointIds()->GetId( 1 );
			if( ( edgePointsIds[0] == currentEdgePointsIds[1] ) &&
			    ( edgePointsIds[1] != currentEdgePointsIds[0] ) )
			{
				// Inserted before
				pEdgeCell = vtkLine::New();
				pEdgeCell->Initialize();
				pEdgeCell->GetPointIds()->InsertId( 0, edgePointsIds[0] );
				pEdgeCell->GetPointIds()->InsertId( 1, edgePointsIds[1] );
				pEdgeCell->GetPoints()->InsertPoint( 0, edgePointsCoords[0] );
				pEdgeCell->GetPoints()->InsertPoint( 1, edgePointsCoords[1] );
				edgesList.insert( ( ++edgesIterator ), pEdgeCell );
				edgeInserted = true;
				break;
			}
			else if( ( edgePointsIds[1] == currentEdgePointsIds[0] ) &&
			         ( edgePointsIds[0] != currentEdgePointsIds[1] ) )
			{
				// Inserted after
				pEdgeCell = vtkLine::New();
				pEdgeCell->Initialize();
				pEdgeCell->GetPointIds()->InsertId( 0, edgePointsIds[0] );
				pEdgeCell->GetPointIds()->InsertId( 1, edgePointsIds[1] );
				pEdgeCell->GetPoints()->InsertPoint( 0, edgePointsCoords[0] );
				pEdgeCell->GetPoints()->InsertPoint( 1, edgePointsCoords[1] );
				edgesList.insert( edgesIterator, pEdgeCell );
				edgeInserted = true;
				break;
			}
			else if( ( edgePointsIds[0] == currentEdgePointsIds[0] ) &&
			         ( edgePointsIds[1] != currentEdgePointsIds[1] ) )
			{
				// Make B' (b'0 = b1, b'1 = b0 )
				// reverse of B
				// Inserted after
				pEdgeCell = vtkLine::New();
				pEdgeCell->Initialize();
				pEdgeCell->GetPointIds()->InsertId( 0, edgePointsIds[1] );
				pEdgeCell->GetPointIds()->InsertId( 1, edgePointsIds[0] );
				pEdgeCell->GetPoints()->InsertPoint( 0, edgePointsCoords[1] );
				pEdgeCell->GetPoints()->InsertPoint( 1, edgePointsCoords[0] );
				edgesList.insert( edgesIterator, pEdgeCell );
				edgeInserted = true;
				break;
			}
			else if( ( edgePointsIds[1] == currentEdgePointsIds[1] ) &&
			         ( edgePointsIds[0] != currentEdgePointsIds[0] ) )
			{
				// Make B' (b'0 = b1, b'1 = b0 )
				// reverse of B
				// Inserted before
				pEdgeCell = vtkLine::New();
				pEdgeCell->Initialize();
				pEdgeCell->GetPointIds()->InsertId( 0, edgePointsIds[1] );
				pEdgeCell->GetPointIds()->InsertId( 1, edgePointsIds[0] );
				pEdgeCell->GetPoints()->InsertPoint( 0, edgePointsCoords[1] );
				pEdgeCell->GetPoints()->InsertPoint( 1, edgePointsCoords[0] );
				edgesList.insert( edgesIterator, pEdgeCell );
				edgeInserted = true;
				break;
			}
		}
		if( !edgeInserted )
		{
			// Inserted after (disjoint)
			pEdgeCell = vtkLine::New();
			pEdgeCell->Initialize();
			pEdgeCell->GetPointIds()->InsertId( 0, edgePointsIds[0] );
			pEdgeCell->GetPointIds()->InsertId( 1, edgePointsIds[1] );
			pEdgeCell->GetPoints()->InsertPoint( 0, edgePointsCoords[0] );
			pEdgeCell->GetPoints()->InsertPoint( 1, edgePointsCoords[1] );
			edgesList.push_back( pEdgeCell );
		}

	}
	else
	{
		pEdgeCell = vtkLine::New();
		pEdgeCell->Initialize();
		pEdgeCell->GetPointIds()->InsertId( 0, edgePointsIds[0] );
		pEdgeCell->GetPointIds()->InsertId( 1, edgePointsIds[1] );
		pEdgeCell->GetPoints()->InsertPoint( 0, edgePointsCoords[0] );
		pEdgeCell->GetPoints()->InsertPoint( 1, edgePointsCoords[1] );
		edgesList.push_back( pEdgeCell );
		cellsIds->InsertNextId( cellId );
	}
}

vtkPolyData* ThresholdPhilipsProcessor::RemoveFlaps( vtkPolyData* pInPolyData, bool updateOutput )
{
	wxASSERT( pInPolyData );

	vtkPolyData* pOutPolyData = NULL;
	vtkSmartPointer<vtkFeatureEdges> featureEdgesFilter = vtkSmartPointer<vtkFeatureEdges>::New();

	vtkPolyData *pTempPolyData;
	pTempPolyData = vtkPolyData::New();
	pTempPolyData->DeepCopy( pInPolyData );


	featureEdgesFilter->SetInput( pTempPolyData );
	featureEdgesFilter->ManifoldEdgesOff();
	featureEdgesFilter->NonManifoldEdgesOff();
	featureEdgesFilter->FeatureEdgesOff();
	featureEdgesFilter->BoundaryEdgesOn();
	featureEdgesFilter->Update();

	if( featureEdgesFilter->GetOutput()->GetNumberOfPoints() && updateOutput )
	{
		UpdateOutput(
			OUTPUT_DEBUG,
			featureEdgesFilter->GetOutput(),
			"Edges", false, 1, GetInputDataEntity( INPUT_MESH ) );
	}

	vtkPolyData* pFeaturedEdges = featureEdgesFilter->GetOutput();
	pFeaturedEdges->BuildLinks();


	//vtkCellArray* pLines = pFeaturedEdges->GetLines();
	pTempPolyData->BuildLinks();
	vtkIdType numberOfPoints = pTempPolyData->GetNumberOfPoints();
	vtkIdType numberOfPolys = pTempPolyData->GetNumberOfPolys();
	vtkCellArray* pInputPolys = pTempPolyData->GetPolys();


	vtkPoints* pEdgePoints = vtkPoints::New();
	pEdgePoints->Allocate( numberOfPoints / 10, numberOfPoints );
	// Create a locator for inserting unique points so that we build the
	// conflictiveEdges list without repeating points
	vtkPointLocator* pPointLocator = vtkMergePoints::New();
	pPointLocator->InitPointInsertion( pEdgePoints, pTempPolyData->GetBounds() );
	// Allocate some memory for the conflictive cells (probably triangles)
	// and the conflictive edges (share only one cell)
	vtkIdList* pConflictiveCellsIds = vtkIdList::New();
	pConflictiveCellsIds->Allocate( numberOfPolys / 10 );
	vtkIdList* pConflictiveEdgesIds = vtkIdList::New();
	pConflictiveEdgesIds->Allocate( numberOfPolys / 10 );
	list<vtkLine*> conflictiveEdgesList( numberOfPolys / 10 );
	conflictiveEdgesList.clear();
	vtkCellArray* pConflictiveEdges = vtkCellArray::New();
	pConflictiveEdges->Allocate( numberOfPolys / 10 );
	vtkIdType numerOfPoints = 0;
	vtkIdType* pointsArray = NULL;
	vtkIdList* edgeNeighbors = vtkIdList::New();
	edgeNeighbors->Allocate( VTK_CELL_SIZE );
	vtkIdType polyCellId;
	for( polyCellId = 0, pInputPolys->InitTraversal();
		pInputPolys->GetNextCell( numerOfPoints, pointsArray ); polyCellId++ )
	{
		for( int pointIndex = 0; pointIndex < numerOfPoints; pointIndex++ )
		{
			vtkIdType firstPointId = pointsArray[pointIndex];
			vtkIdType secondPointId = pointsArray[(pointIndex + 1) % numerOfPoints];
			pTempPolyData->GetCellEdgeNeighbors( polyCellId, firstPointId, secondPointId, edgeNeighbors );
			vtkIdType numberOfNeighbors = edgeNeighbors->GetNumberOfIds();
			if( numberOfNeighbors == 0 )
			{
				// Found an edge with only a polygon
				//vtkCell* pCurrentCell = pTempPolyData->GetCell( polyCellId );
				double conflictiveEdgePointCoords[2][3];
				pTempPolyData->GetPoint( firstPointId, conflictiveEdgePointCoords[0] );
				pTempPolyData->GetPoint( secondPointId, conflictiveEdgePointCoords[1] );
				vtkIdType conflictiveEdge[2];
				pPointLocator->InsertUniquePoint( conflictiveEdgePointCoords[0], conflictiveEdge[0] );
				pPointLocator->InsertUniquePoint( conflictiveEdgePointCoords[1], conflictiveEdge[1] );
				//conflictiveEdge[0] = firstPointId;
				//conflictiveEdge[1] = secondPointId;
				InsertEdgeOrdered( pConflictiveCellsIds, polyCellId, pConflictiveEdgesIds, conflictiveEdgesList, pConflictiveEdges, conflictiveEdgePointCoords, conflictiveEdge );
			}
		}
	}

	PrintConflictiveEdges( conflictiveEdgesList );

	// Delete allocated data
	pPointLocator->Delete();
	pConflictiveCellsIds->Delete();
	pConflictiveEdgesIds->Delete();
	pConflictiveEdges->Delete();

	pOutPolyData = pTempPolyData;

	return pOutPolyData;
}

vtkPolyData* ThresholdPhilipsProcessor::MultiThreshold(
	const vector<int>& orgRegionIDs,
	const vector<int>& dstSubpartsIDs,
	const vector<int>& processorOuputs,
	const vector<string>& processorOutputsNames,
	vtkPolyData* pInPolyData,
	int appendedSetsLoopSubdivisionCount,
	Core::DataEntity::Pointer fatherDataEntity )
{
	CustomFiltersParams* pCustomFiltersParams = new CustomFiltersParams;

	vector<vtkSmartPointer<vtkPolyData> > subpartsPolyData;
	subpartsPolyData.resize( orgRegionIDs.size() );
	int dstMaxSubartID = numeric_limits<int>::min();
	for( int indexRegion = 0; indexRegion < orgRegionIDs.size(); indexRegion++ )
	{
		subpartsPolyData[indexRegion] = vtkSmartPointer<vtkPolyData>::New();

		pCustomFiltersParams->m_PhilipsRegionID = orgRegionIDs[indexRegion];
		pCustomFiltersParams->m_SubPartID = dstSubpartsIDs[indexRegion];

		subpartsPolyData[indexRegion] = ApplyAllCustomFilters( pInPolyData, pCustomFiltersParams );

		// Save the subpart ID with maximum value
		if( dstMaxSubartID < dstSubpartsIDs[indexRegion] )
		{
			dstMaxSubartID = dstSubpartsIDs[indexRegion];
		}

		if( processorOuputs[indexRegion] >= 0 )
		{
			UpdateOutput( processorOuputs[indexRegion],
				subpartsPolyData[indexRegion], 
				processorOutputsNames[indexRegion], false, 1, fatherDataEntity );
		}
	}

	delete pCustomFiltersParams;

	// Now append polydata, organized by dstSubpartID
	vector<vtkSmartPointer<vtkAppendPolyData> > appendFilters;
	appendFilters.resize( dstMaxSubartID );
	for( int indexRegion = 0; indexRegion < orgRegionIDs.size(); indexRegion++ )
	{
		int subpartID = dstSubpartsIDs[indexRegion];
		int subpartIDIndex = subpartID - 1;
		if( appendFilters[subpartIDIndex] == NULL )
		{
			appendFilters[subpartIDIndex] = vtkSmartPointer<vtkAppendPolyData>::New();
		}
		appendFilters[subpartIDIndex]->AddInput( subpartsPolyData[indexRegion] );
	}

	// Update the append filters output, and clean the data
	vtkSmartPointer<vtkAppendPolyData> finalAppendFilter = vtkSmartPointer<vtkAppendPolyData>::New();
	vector<vtkSmartPointer<vtkCleanPolyData> > cleanFilters;
	cleanFilters.resize( dstMaxSubartID );
	for( int indexSubpart = 0; indexSubpart < dstMaxSubartID; indexSubpart++ )
	{
		if( appendFilters[indexSubpart] != NULL )
		{
			appendFilters[indexSubpart]->Update();
			cleanFilters[indexSubpart] = vtkSmartPointer<vtkCleanPolyData>::New();
			cleanFilters[indexSubpart]->SetInput( appendFilters[indexSubpart]->GetOutput() );
			
			//cleanFilters[indexSubpart]->Update();
/*
UpdateOutput( OUTPUT_DEBUG + indexSubpart,
				cleanFilters[indexSubpart]->GetOutput(),
				wxString::Format( "__%d", indexSubpart ).c_str(), false, 1, fatherDataEntity );
*/
			finalAppendFilter->AddInput( cleanFilters[indexSubpart]->GetOutput() );
		}
	}
	finalAppendFilter->Update();

	vtkSmartPointer<vtkCleanPolyData> finalCleanFilter = vtkSmartPointer<vtkCleanPolyData>::New();
	finalCleanFilter->SetInput( finalAppendFilter->GetOutput() );
	finalCleanFilter->Update();

	vtkPolyData* outPolyData = vtkPolyData::New();

	if( appendedSetsLoopSubdivisionCount > 0 )
	{
	vtkSmartPointer<vtkLoopSubdivisionFilter> subdividedPolyDataFilter = vtkSmartPointer<vtkLoopSubdivisionFilter>::New();
	subdividedPolyDataFilter->SetInput( finalCleanFilter->GetOutput() );
	subdividedPolyDataFilter->SetNumberOfSubdivisions( appendedSetsLoopSubdivisionCount );
	subdividedPolyDataFilter->Update();

	outPolyData->DeepCopy( subdividedPolyDataFilter->GetOutput() );
	}
	else
	{
		outPolyData->DeepCopy( finalCleanFilter->GetOutput() );
	}



	// TODO: use dstMaxSubartID
	// Clamp subpartID values to 1 or 2, not values inbetween
	vtkDataArray* subpartIDArray = outPolyData->GetPointData()->GetArray( "subpartID" );
	for( int tupleIndex = 0; tupleIndex < subpartIDArray->GetNumberOfTuples(); tupleIndex++ )
	{
		double tupleValue = subpartIDArray->GetTuple1( tupleIndex );
		if( ( tupleValue > 1.0 ) && ( tupleValue < 2.0 ) )
		{
			if( tupleValue = 1.5 )
			{
				tupleValue = 2.0;
			}
			else
			{
				tupleValue = 1.0;
			}
			subpartIDArray->SetTuple1( tupleIndex, tupleValue );
		}
	}

	return outPolyData;
}

typedef struct
{
	unsigned short currentPointNumberOfCells;
	vtkIdType* currentPointCellsIDs; 
} PointCellsInfo;

vtkIdType FindTopologicallyIdenticCell( vtkPolyData* srcPolyData, vtkIdType srcCellID, vtkPolyData* dstPolyData )
{
	// Get the cell
	vtkCell*currentCell = srcPolyData->GetCell( srcCellID );
	// Get the points of the cell
	vtkPoints* currentCellPoints = currentCell->GetPoints();
	int numberOfPoints = currentCellPoints->GetNumberOfPoints();
	double pointCoords[3];
	vector<PointCellsInfo> allPointsCells;
	allPointsCells.resize( numberOfPoints );
	for( int indexPoint = 0; indexPoint < numberOfPoints; indexPoint++ )
	{
		currentCellPoints->GetPoint( indexPoint, pointCoords );
		// Find the point on the destination poly data
		vtkIdType pointID = dstPolyData->FindPoint( pointCoords );
		// Get the cells that share this point
		dstPolyData->GetPointCells( pointID,
			                        allPointsCells[indexPoint].currentPointNumberOfCells,
			                        allPointsCells[indexPoint].currentPointCellsIDs );
	}
	// Now, for each point of the original polydata cell, we have all the cells that
	// share that points in the destination polyData
	// But only one cell will share all the points
	vtkIdType foundCell = -1;
	bool foundInAll;
	bool found;
	for( int firstPointIndexCell = 0; firstPointIndexCell < allPointsCells[0].currentPointNumberOfCells; firstPointIndexCell++ )
	{
		vtkIdType cellToFind = allPointsCells[0].currentPointCellsIDs[firstPointIndexCell];
		foundInAll = true;
		for( int indexPoint = 1; indexPoint < numberOfPoints; indexPoint++ )
		{
			found = false;
			for( int indexCell = 0; indexCell < allPointsCells[indexPoint].currentPointNumberOfCells; indexCell++ )
			{
				if( cellToFind == allPointsCells[indexPoint].currentPointCellsIDs[indexCell] )
				{
					// Found, not need to go further
					found = true;
					break;
				}
			}
			foundInAll &= found;
			if( !foundInAll )
			{
				// If the cell id is not found in every point cells, then it is not the cell we are interested in
				break;
			}
		}
		if( foundInAll )
		{
			foundCell = cellToFind;
			break;
		}
	}

	return foundCell;
}

void ThresholdPhilipsProcessor::Update()
{
	Core::vtkPolyDataPtr vtkInput;
	Core::DataEntityHelper::GetProcessingData( 
		GetInputDataEntityHolder( INPUT_MESH ),
		vtkInput );

	vtkSmartPointer<vtkCleanPolyData> cleanPolyDataFilter = vtkSmartPointer<vtkCleanPolyData>::New();
	vtkSmartPointer<vtkArrayCalculator> arrayCalculatorFilter = vtkSmartPointer<vtkArrayCalculator>::New();
	vtkSmartPointer<vtkProgrammableAttributeDataFilter> programableAttributeDataFilter = vtkSmartPointer<vtkProgrammableAttributeDataFilter>::New();

	// First of all, get the original mesh (philips segmented mesh), and
	// remove the following cells IDs, because they produce a bad thresholded
	// mesh: 1215, 2722, 2988, 3126, 3847, 4077, 10429, 10440, 10454,
	// 10489, 10490

	vtkSmartPointer<vtkPolyData> polyPhilipsCorrected = vtkSmartPointer<vtkPolyData>::New();
	polyPhilipsCorrected->DeepCopy( vtkInput );
	polyPhilipsCorrected->DeleteCell( 1215 );
	polyPhilipsCorrected->DeleteCell( 2722 );
	polyPhilipsCorrected->DeleteCell( 2988 );
	polyPhilipsCorrected->DeleteCell( 3126 );
	polyPhilipsCorrected->DeleteCell( 3847 );
	polyPhilipsCorrected->DeleteCell( 4077 );
	polyPhilipsCorrected->DeleteCell( 10429 );
	polyPhilipsCorrected->DeleteCell( 10440 );
	polyPhilipsCorrected->DeleteCell( 10454 );
	polyPhilipsCorrected->DeleteCell( 10489 );
	polyPhilipsCorrected->DeleteCell( 10490 );
	polyPhilipsCorrected->RemoveDeletedCells();

	vtkSmartPointer<vtkPolyData> LVCompleteSP;

	vector<int> regionIDs;
	vector<int> subpartIDs;
	vector<int> processorOuputs;
	vector<string> processorOutputsNames;

	regionIDs.push_back( PHILIPS_REGION_ID_LV_EPI );
	subpartIDs.push_back( CRT_SUBPART_ID_EPICARDIUM );
	//processorOuputs.push_back( OUTPUT_LV_EPI );
	processorOuputs.push_back( -1 );
	processorOutputsNames.push_back( "LVMyo_1" );

	regionIDs.push_back( PHILIPS_REGION_ID_LV_EPI_RV );
	subpartIDs.push_back( CRT_SUBPART_ID_EPICARDIUM );
	//processorOuputs.push_back( OUTPUT_LV_EPI_RV );
	processorOuputs.push_back( -1 );
	processorOutputsNames.push_back( "LVMyo_RightVentricle_25" );

	regionIDs.push_back( PHILIPS_REGION_ID_APEX );
	subpartIDs.push_back( CRT_SUBPART_ID_ENDOCARDIUM );
	//processorOuputs.push_back( OUTPUT_APEX );
	processorOuputs.push_back( -1 );
	processorOutputsNames.push_back( "ApexLVEndo_LVMyo_LeftVentricle_15" );

	regionIDs.push_back( PHILIPS_REGION_ID_LV_ENDO );
	subpartIDs.push_back( CRT_SUBPART_ID_ENDOCARDIUM );
	//processorOuputs.push_back( OUTPUT_LV_ENDO );
	processorOuputs.push_back( -1 );
	processorOutputsNames.push_back( "LVMyo_LeftVentricle_170" );


#define ATRIUM_AS_ENDO_ID 0
	regionIDs.push_back( PHILIPS_REGION_ID_LEFT_ATRIUM );
#if( ATRIUM_AS_ENDO_ID )
	subpartIDs.push_back( CRT_SUBPART_ID_ENDOCARDIUM );
#else
	subpartIDs.push_back( CRT_SUBPART_ID_EPICARDIUM );
#endif
	//processorOuputs.push_back( OUTPUT_LEFT_ATRIUM );
	processorOuputs.push_back( -1 );
	processorOutputsNames.push_back( "LVMyo_LeftAtrium_35" );

	if( m_Parameters.m_bOutputRightVentricle )
	{
		regionIDs.push_back( PHILIPS_REGION_ID_RV_EPI );
		subpartIDs.push_back( CRT_SUBPART_ID_EPICARDIUM );
		//processorOuputs.push_back( OUTPUT_LV_ENDO );
		processorOuputs.push_back( -1 );
		processorOutputsNames.push_back( "RVMyo_90" );
	}

	int subdivisions = 2;

	LVCompleteSP = MultiThreshold(
		regionIDs, subpartIDs, processorOuputs, processorOutputsNames,
		polyPhilipsCorrected, subdivisions, GetInputDataEntity( INPUT_MESH ) );

	if( m_Parameters.m_bOutputRightVentricle )
	{
			//UpdateOutput( OUTPUT_LV_COMPLETE, LVCompleteSP, "LeftVentricleComplete" );

			int auxSubdivisions = subdivisions;
			// Don't subdivide until the end
			subdivisions = 0;

		vector<int> regionIDsRV;
		vector<int> subpartIDsRV;
		vector<int> processorOuputsRV;
		vector<string> processorOutputsNamesRV;

		regionIDsRV.push_back( PHILIPS_REGION_ID_RV_EPI );
			subpartIDsRV.push_back( CRT_SUBPART_ID_RV_EPICARDIUM );
		//processorOuputsRV.push_back( OUTPUT_RV_EPI );
		processorOuputsRV.push_back( -1 );
		processorOutputsNamesRV.push_back( "RVMyo_90" );

		// Get the right ventricle epicardium
		vtkSmartPointer<vtkPolyData> RVEpiSP = MultiThreshold(
			regionIDsRV, subpartIDsRV, processorOuputsRV, processorOutputsNamesRV,
			polyPhilipsCorrected, subdivisions, GetInputDataEntity( INPUT_MESH ) );

		regionIDsRV.clear();
		subpartIDsRV.clear();
		processorOuputsRV.clear();
		processorOutputsNamesRV.clear();

		regionIDsRV.push_back( PHILIPS_REGION_ID_LV_EPI_RV );
			subpartIDsRV.push_back( CRT_SUBPART_ID_RV_EPICARDIUM );
		//processorOuputsRV.push_back( OUTPUT_LV_EPI_RV );
		processorOuputsRV.push_back( -1 );
		processorOutputsNamesRV.push_back( "RVMyo_25" );

		regionIDsRV.push_back( PHILIPS_REGION_ID_RV_EPI );
			subpartIDsRV.push_back( CRT_SUBPART_ID_RV_EPICARDIUM );
		//processorOuputsRV.push_back( OUTPUT_RV_EPI );
		processorOuputsRV.push_back( -1 );
		processorOutputsNamesRV.push_back( "RVMyo_90" );

		// Get the right ventricle parts
		vtkSmartPointer<vtkPolyData> outRVSP = MultiThreshold(
			regionIDsRV, subpartIDsRV, processorOuputsRV, processorOutputsNamesRV,
			polyPhilipsCorrected, subdivisions, GetInputDataEntity( INPUT_MESH ) );

		// Create the normals to be used for the extrusion
		vtkPolyData* shapeWithNormalsPt = vtkPolyData::New();
		vtkSmartPointer<vtkPolyDataNormals> polydataNormalsFilter = vtkSmartPointer<vtkPolyDataNormals>::New();
		polydataNormalsFilter->SetInput( outRVSP );
		polydataNormalsFilter->ComputePointNormalsOn();
		polydataNormalsFilter->FlipNormalsOn();
		polydataNormalsFilter->SplittingOff();
		polydataNormalsFilter->ConsistencyOn();
		polydataNormalsFilter->Update();

		vtkSmartPointer<vtkPolyData> epiWithNormalsSP = vtkSmartPointer<vtkPolyData>::New();
		epiWithNormalsSP->DeepCopy( polydataNormalsFilter->GetOutput() );


		vtkSmartPointer<vtkLinearExtrusionFilter> extrudeFilterSP = vtkSmartPointer<vtkLinearExtrusionFilter>::New();
		double m_ExtrusionSize = 0.5;
		// Get only the extrusion
		extrudeFilterSP->SetInput( epiWithNormalsSP );
		extrudeFilterSP->SetExtrusionTypeToNormalExtrusion();
		extrudeFilterSP->CappingOff();
		extrudeFilterSP->SetScaleFactor( m_ExtrusionSize );
		extrudeFilterSP->Update();
		vtkSmartPointer<vtkPolyData> RVEpiExtrusion = vtkSmartPointer<vtkPolyData>::New();
		RVEpiExtrusion->DeepCopy( extrudeFilterSP->GetOutput() );

		// Get the extrusion and caps
		extrudeFilterSP->RemoveAllInputs();
		extrudeFilterSP->SetInput( epiWithNormalsSP );
		extrudeFilterSP->SetExtrusionTypeToNormalExtrusion();
		extrudeFilterSP->CappingOn();
		extrudeFilterSP->SetScaleFactor( m_ExtrusionSize );
		extrudeFilterSP->Update();
		vtkSmartPointer<vtkPolyData> RVEpiWithExtrusionCappingOn = vtkSmartPointer<vtkPolyData>::New();
		RVEpiWithExtrusionCappingOn->DeepCopy( extrudeFilterSP->GetOutput() );


		// Necessary or FindTopologicallyIdenticCell will fail
		RVEpiWithExtrusionCappingOn->BuildLinks();
			// Get the selection of the faces in the extrusion, from the polydata with the extrusion and
		// caps
		vtkSmartPointer<vtkSelectionSource> extrusionSelectionSP = vtkSmartPointer<vtkSelectionSource>::New();
		extrusionSelectionSP->SetContentType( vtkSelectionNode::INDICES );
		extrusionSelectionSP->SetFieldType( vtkSelectionNode::CELL );
		vtkIdType foundCellID;
		for( int i = 0; i< RVEpiExtrusion->GetNumberOfCells(); i++ )
		{
			foundCellID = FindTopologicallyIdenticCell( RVEpiExtrusion, i, RVEpiWithExtrusionCappingOn );
			if( foundCellID >= 0 )
			{
				extrusionSelectionSP->AddID( -1, foundCellID );
			}
			else
			{
				cout << "Cell not found" << endl;
			}
		}
		extrusionSelectionSP->SetInverse( 1 );

		// Get the selecion of the faces in the epiWithNormalsSP, from the polydata with the extrusion and
		// caps. That is, we only leave the endocardial part and the extrusion part
		vtkSmartPointer<vtkSelectionSource> epiSelectionSP = vtkSmartPointer<vtkSelectionSource>::New();
		epiSelectionSP->SetContentType( vtkSelectionNode::INDICES );
		epiSelectionSP->SetFieldType( vtkSelectionNode::CELL );
		epiSelectionSP->SetInverse( 1 );
		for( int i = 0; i< epiWithNormalsSP->GetNumberOfCells(); i++ )
		{
			foundCellID = FindTopologicallyIdenticCell( epiWithNormalsSP, i, RVEpiWithExtrusionCappingOn );
			if( foundCellID >= 0 )
			{
				epiSelectionSP->AddID( -1, foundCellID );
			}
			else
			{
				cout << "Cell not found" << endl;
			}
		}

		vtkSmartPointer<vtkExtractSelection> extractSelectionSP = vtkSmartPointer<vtkExtractSelection >::New();
		vtkSmartPointer<vtkDataSetSurfaceFilter> dataSetSurfaceFilterSP = vtkSmartPointer<vtkDataSetSurfaceFilter>::New();

		// Extract the extruded part
		extractSelectionSP->SetInput( RVEpiWithExtrusionCappingOn );
		extractSelectionSP->SetSelectionConnection( extrusionSelectionSP->GetOutputPort() );
		extractSelectionSP->Update();
		dataSetSurfaceFilterSP->SetInput( extractSelectionSP->GetOutput() );
		dataSetSurfaceFilterSP->Update();
		vtkSmartPointer<vtkPolyData> EndoAndEpi = vtkSmartPointer<vtkPolyData>::New();
		EndoAndEpi->DeepCopy( dataSetSurfaceFilterSP->GetOutput() );


		// Extract the epi part
		extractSelectionSP->SetInput( EndoAndEpi );
		extractSelectionSP->SetSelectionConnection( epiSelectionSP->GetOutputPort() );
		extractSelectionSP->Update();
		dataSetSurfaceFilterSP->SetInput( extractSelectionSP->GetOutput() );
		dataSetSurfaceFilterSP->Update();
		vtkSmartPointer<vtkPolyData> RVEndoSP = vtkSmartPointer<vtkPolyData>::New();
		RVEndoSP->DeepCopy( dataSetSurfaceFilterSP->GetOutput() );

		// Triangularize RVEpiExtrusion
		//vtkSmartPointer<vtkDataSetTriangleFilter> triangleFilterSP = vtkSmartPointer<vtkDataSetTriangleFilter>::New();
		vtkSmartPointer<vtkTriangleFilter> triangleFilterSP = vtkSmartPointer<vtkTriangleFilter>::New();
		triangleFilterSP->SetInput( RVEpiExtrusion );
		triangleFilterSP->Update();
		vtkSmartPointer<vtkPolyData> RVEpiExtrusionTriangulatedSP = vtkSmartPointer<vtkPolyData>::New();
		RVEpiExtrusionTriangulatedSP->DeepCopy( triangleFilterSP->GetOutput() );
			
		//SetSubpartID( RVEpiExtrusionTriangulatedSP, CRT_SUBPART_ID_EPICARDIUM );
			SetSubpartID( RVEpiExtrusionTriangulatedSP, CRT_SUBPART_ID_RV_ENDOCARDIUM );
			SetSubpartID( RVEndoSP, CRT_SUBPART_ID_RV_ENDOCARDIUM );

		// Append Extrusion with endocardium and epicardium
		vtkSmartPointer<vtkAppendPolyData> appendSP = vtkSmartPointer<vtkAppendPolyData>::New();
		appendSP->AddInput( RVEpiExtrusionTriangulatedSP );
		appendSP->AddInput( RVEndoSP );
		appendSP->AddInput( RVEpiSP );
			
		vtkSmartPointer<vtkCleanPolyData> cleanSP = vtkSmartPointer<vtkCleanPolyData>::New();
		cleanSP->SetInputConnection( appendSP->GetOutputPort() );
		cleanSP->Update();

			vtkSmartPointer<vtkPolyData> RVCompleteSP = vtkSmartPointer<vtkPolyData>::New();
			
			subdivisions = auxSubdivisions;
			if( subdivisions > 0 )
			{
				// Subdivide the resulting mesh
			
				vtkSmartPointer<vtkLoopSubdivisionFilter> subdividedPolyDataFilter = vtkSmartPointer<vtkLoopSubdivisionFilter>::New();
				subdividedPolyDataFilter->SetInputConnection( cleanSP->GetOutputPort() );
				subdividedPolyDataFilter->SetNumberOfSubdivisions( subdivisions );
				subdividedPolyDataFilter->Update();
							
				RVCompleteSP->DeepCopy( subdividedPolyDataFilter->GetOutput() );
			}
			else
			{
				RVCompleteSP->DeepCopy( cleanSP->GetOutput() );
			}

			//UpdateOutput( OUTPUT_DEBUG_7, RVCompleteSP, "RVCompleteSP" );

			vtkSmartPointer<vtkAppendPolyData> finalAppendSP = vtkSmartPointer<vtkAppendPolyData>::New();
			finalAppendSP->AddInput( RVCompleteSP );
			finalAppendSP->AddInput( LVCompleteSP );
			
			vtkSmartPointer<vtkCleanPolyData> finalCleanSP = vtkSmartPointer<vtkCleanPolyData>::New();
			finalCleanSP->SetInputConnection( finalAppendSP->GetOutputPort() );
			finalCleanSP->Update();

			UpdateOutput( OUTPUT_LV_RV_COMPLETE, finalCleanSP->GetOutput(), "Ventricles" );
			//UpdateOutput( OUTPUT_LV_RV_COMPLETE, finalAppendSP->GetOutput(), "Ventricles" );
	}
	else
	{
		UpdateOutput( OUTPUT_LV_COMPLETE, LVCompleteSP, "LeftVentricleComplete" );
	}
	Core::DataEntity::Pointer outputLVRV = GetOutputDataEntity( OUTPUT_LV_RV_COMPLETE );
	if( outputLVRV.IsNotNull() )
	{
		Core::DataEntityMetadata::Pointer metadataSP = outputLVRV->GetMetadata();
		blTag::Pointer CRTEntityTagSP = metadataSP->FindTagByName( "CRTWorkflowEntity" );
		if( CRTEntityTagSP.IsNull() )
		{
			metadataSP->AddTag( "CRTWorkflowEntity", string( "PhillipsLVRV" ) );
cout << "Adding metadata OUTPUT_LV_RV_COMPLETE" << endl;
		}
	}
	Core::DataEntity::Pointer outputLV = GetOutputDataEntity( OUTPUT_LV_COMPLETE );
	if( outputLV.IsNotNull() )
	{
		Core::DataEntityMetadata::Pointer metadataSP = outputLV->GetMetadata();
		blTag::Pointer CRTEntityTagSP = metadataSP->FindTagByName( "CRTWorkflowEntity" );
		if( CRTEntityTagSP.IsNull() )
		{
			metadataSP->AddTag( "CRTWorkflowEntity", string( "PhillipsLV" ) );
cout << "Adding metadata OUTPUT_LV_COMPLETE" << endl;
		}
	}

}

ThresholdPhilipsProcessor::TPPParameters& ThresholdPhilipsProcessor::GetParameters()
{
	return m_Parameters;
}

