/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "msMitkMoveImageAndMeshInteractor.h"
#include "mitkSurface.h"
#include "mitkInteractionConst.h"
#include <mitkDataTreeNode.h>
#include "mitkDisplayPositionEvent.h"
#include "mitkStateEvent.h"
#include "mitkProperties.h"

//for an temporary update
#include "mitkRenderingManager.h"


#define MAXUNDO 100

//## Default Constructor
mitk::MoveImageAndMeshInteractor
::MoveImageAndMeshInteractor(const char * type, DataTreeNode* dataNode)
:Interactor(type, dataNode)
{
	mitk::State::Pointer startState = mitk::GlobalInteraction::GetInstance()->GetStartState(type);
	if ( startState.IsNull( ) )
	{
		throw Core::Exceptions::Exception( "MoveImageAndMeshInteractor::MoveImageAndMeshInteractor", 
			"Cannot initialize interactor" );
	}

	m_lastInteractionMove[0] = 0;
	m_lastInteractionMove[1] = 0;
	m_lastInteractionMove[2] = 0;

	m_callback = NULL;
}

mitk::MoveImageAndMeshInteractor::~MoveImageAndMeshInteractor()
{
}


bool mitk::MoveImageAndMeshInteractor::ExecuteAction( Action* action, mitk::StateEvent const* stateEvent )
{
	// Check if we have a DisplayPositionEvent
	const mitk::DisplayPositionEvent *dpe = 
		dynamic_cast< const mitk::DisplayPositionEvent * >( stateEvent->GetEvent() );     
			
	mitk::Point3D currentPickedPoint;
	if ( dpe != NULL )
		currentPickedPoint = dpe->GetWorldPosition();


  bool ok = false;
  
  /*Each case must watch the type of the event!*/
  switch (action->GetActionId())
  {
	case AcDONOTHING:
		ok = true;
		break;

  	case AcINITMOVE:
	{	
		m_initialPickedPoint = currentPickedPoint;
		m_lastPickedPoint = currentPickedPoint;
		ok = true;
		break;
	}
  
	case AcMOVE:
    {
		//modify Geometry from data as given in parameters or in event
		
		m_lastInteractionMove[0] = currentPickedPoint[0] - m_lastPickedPoint[0];
		m_lastInteractionMove[1] = currentPickedPoint[1] - m_lastPickedPoint[1];
		m_lastInteractionMove[2] = currentPickedPoint[2] - m_lastPickedPoint[2];
		m_lastPickedPoint = currentPickedPoint;


		/*
	    //checking corresponding Data; has to be an image (why not a surface also?)
		mitk::Image* image = dynamic_cast<mitk::Image*>(m_DataTreeNode->GetData());
		if ( image == NULL )
		{
			LOG_WARN<<"MoveImageAndMeshInteractor got wrong type of data! Aborting interaction!\n";
			return false;
		}
		*/
		Translate(&m_lastInteractionMove);

		//update the undoList
		if(m_undoList.size()==MAXUNDO)
			m_undoList.pop();

		m_undoList.push(m_lastInteractionMove);


		//update rendering
		mitk::RenderingManager::GetInstance()->RequestUpdateAll();

		ok = true;
		break;
    }
	case AcACCEPT:
	{
		if(m_callback!=NULL)
		{
			double shiftDist [3]=  {m_lastPickedPoint[0]-m_initialPickedPoint[0],
									m_lastPickedPoint[1]-m_initialPickedPoint[1],
									m_lastPickedPoint[2]-m_initialPickedPoint[2]};
			if(m_callback!=NULL)
				m_callback->OnModifiedRenderingData(shiftDist);
		}
		break;
	}

	case AcUNDOUPDATE:
	{
		if(m_undoList.size()>0)
		{
			mitk::Vector3D revertVector = m_undoList.top();
			m_undoList.pop();
			revertVector[0] = -revertVector[0];
			revertVector[1] = -revertVector[1];
			revertVector[2] = -revertVector[2];

			Translate(&revertVector);

			mitk::RenderingManager::GetInstance()->RequestUpdateAll();
		}
		break;
	}

  default:
    return Superclass::ExecuteAction( action, stateEvent );
  }

  return ok;
}


// Overwritten since this class can handle it better!
// Note: Interactor superClass does not handle very well 
// neither mouse dragging events nor keyboard events!
float mitk::MoveImageAndMeshInteractor
::CalculateJurisdiction(StateEvent const* stateEvent) const
{
	float returnValue = 0.5;


	// If it is a key event that can be handled in the current state,
	// then return 0.5
	mitk::DisplayPositionEvent const *disPosEvent =
		dynamic_cast <const mitk::DisplayPositionEvent *> (stateEvent->GetEvent());

	// Key event handling:
	if (disPosEvent == NULL)
	{
		// Check if the current state has a transition waiting for that key event.
		if (this->GetCurrentState()->GetTransition(stateEvent->GetId())!=NULL)
		{
			return 0.5;
		}
		else
		{
			return 0.0;
		}
	}

	//if the event can be understood and if there is a transition waiting for that event
	if (this->GetCurrentState()->GetTransition(stateEvent->GetId())!=NULL)
	{
		returnValue = 0.5;//it can be understood
	}

	int timeStep = disPosEvent->GetSender()->GetTimeStep();
	return returnValue;
}


#include "mitkImageTimeSelector.h"
void mitk::MoveImageAndMeshInteractor::Translate(mitk::Vector3D *translateVect)
{
	mitk::BaseData *data = m_DataTreeNode->GetData();
	Geometry3D* geometry = data->GetUpdatedTimeSlicedGeometry()->GetGeometry3D( m_TimeStep );
	//geometry->Translate(*translateVect);

	mitk::Point3D origin = geometry->GetOrigin();
	origin[0] += (*translateVect)[0];
	origin[1] += (*translateVect)[1];
	origin[2] += (*translateVect)[2];
	geometry->SetOrigin(origin);
	
	//return;
	////do a more serious translation
	//mitk::Image::Pointer image = dynamic_cast<mitk::Image*>(data);
	//if(image.IsNotNull())
	//{
	//	mitk::ImageTimeSelector::Pointer timeSelector=mitk::ImageTimeSelector::New();
	//	timeSelector->SetInput( image );
	//	timeSelector->SetTimeNr( m_TimeStep );
	//	timeSelector->UpdateLargestPossibleRegion();

	//	//AccessByItk_1( timeSelector->GetOutput(), InternalCompute, translateVect );
	//	m_imageTStep =  timeSelector->GetOutput();
	//	//AccessFixedDimensionByItk_1(  timeSelector->GetOutput(), ItkImageSwitch, 3, translateVect );
	//	AccessByItk_1( m_imageTStep, ItkImageSwitch, translateVect );
	//	image->Modified();
	//	m_imageTStep->Modified();
	//}
	//else
	//	m_imageTStep = NULL;


	//m_DataTreeNode->Modified();
}


// basically copied from mitk/Core/Algorithms/mitkImageAccessByItk.h
#define myAccessByItk(mitkImage, itkImageTypeFunction, pixeltype, dimension, itkimage2, param1)            \
	if ( typeId == typeid(pixeltype) )                                                    \
{                                                                                        \
	typedef itk::Image<pixeltype, dimension> ImageType;                                   \
	typedef mitk::ImageToItk<ImageType> ImageToItkType;                                    \
	itk::SmartPointer<ImageToItkType> imagetoitk = ImageToItkType::New();                 \
	imagetoitk->SetInput(mitkImage);                                                     \
	imagetoitk->Update();                                                               \
	itkImageTypeFunction(imagetoitk->GetOutput(), itkimage2, param1);                          \
}                                                              

#define myAccessAllTypesByItk(mitkImage, itkImageTypeFunction, dimension, itkimage2, param1)    \
{                                                                                                                           \
	myAccessByItk(mitkImage, itkImageTypeFunction, double,         dimension, itkimage2, param1) else   \
	myAccessByItk(mitkImage, itkImageTypeFunction, float,          dimension, itkimage2, param1) else    \
	myAccessByItk(mitkImage, itkImageTypeFunction, int,            dimension, itkimage2, param1) else     \
	myAccessByItk(mitkImage, itkImageTypeFunction, unsigned int,   dimension, itkimage2, param1) else      \
	myAccessByItk(mitkImage, itkImageTypeFunction, short,          dimension, itkimage2, param1) else     \
	myAccessByItk(mitkImage, itkImageTypeFunction, unsigned short, dimension, itkimage2, param1) else    \
	myAccessByItk(mitkImage, itkImageTypeFunction, char,           dimension, itkimage2, param1) else   \
	myAccessByItk(mitkImage, itkImageTypeFunction, unsigned char,  dimension, itkimage2, param1)       \
}


template<typename TPixel, unsigned int VImageDimension>
void mitk::MoveImageAndMeshInteractor::ItkImageSwitch( itk::Image<TPixel,VImageDimension>* itkImage, mitk::Vector3D *vector )
{
	const std::type_info& typeId=*(m_imageTStep->GetPixelType().GetTypeId());

	std::cout <<"**********ce prova!=!="<<std::endl;
	myAccessAllTypesByItk( m_imageTStep, InternalCompute, VImageDimension, itkImage, vector );
}


#include <itkPasteImageFilter.h>
#include <itkCropImageFilter.h>

template<typename TPixel1, unsigned int VImageDimension1, typename TPixel2, unsigned int VImageDimension2 >
void mitk::MoveImageAndMeshInteractor::InternalCompute(
					 itk::Image< TPixel1, VImageDimension1 >* itkInputImage, 
					 itk::Image< TPixel2, VImageDimension2 >* itkOuputImage,
					 mitk::Vector3D *translateVect )
{
	std::cout<<"********segundo paso"<<std::endl;
	typedef itk::Image< TPixel1, VImageDimension1 > ItkInputImageType;
	typedef itk::Image< TPixel2, VImageDimension2 > ItkOutputImageType;

	typedef itk::PasteImageFilter <ItkInputImageType, ItkInputImageType, ItkOutputImageType > PasteImageFilterType;
	typename PasteImageFilterType::Pointer pasteFilter = PasteImageFilterType::New();

	typename ItkInputImageType::PointType origin = itkInputImage->GetOrigin();
	origin[0] += (*translateVect)[0];
	origin[1] += (*translateVect)[1];
	origin[2] += (*translateVect)[2];

	itkInputImage->SetRegions(itkInputImage->GetLargestPossibleRegion());
	typename ItkInputImageType::IndexType newOriginIndex;
	newOriginIndex[0]= 100;
	newOriginIndex[1]= 100;
	newOriginIndex[2]= 0;


	typename ItkInputImageType::PointType itkNewOrigin;
	itkNewOrigin[0] = origin[0];
	itkNewOrigin[1] = origin[1];
	itkNewOrigin[2] = origin[2];

	itkInputImage->SetRegions(itkInputImage->GetLargestPossibleRegion());

	typename ItkInputImageType::PointType currOrigin = itkInputImage->GetOrigin();
	typename ItkInputImageType::SizeType destinationSize;
	destinationSize[0] = itkInputImage->GetLargestPossibleRegion().GetSize()[0]-
		std::floor(std::abs(itkNewOrigin[0]-currOrigin[0])/itkInputImage->GetSpacing()[0]);

	destinationSize[1] = itkInputImage->GetLargestPossibleRegion().GetSize()[1]-
		std::floor(std::abs(itkNewOrigin[1]-currOrigin[1])/itkInputImage->GetSpacing()[1]);

	destinationSize[2] = itkInputImage->GetLargestPossibleRegion().GetSize()[2]-
		std::floor(std::abs(itkNewOrigin[2]-currOrigin[2])/itkInputImage->GetSpacing()[2]);	

	if((destinationSize[0]<= 0) ||
		(destinationSize[1]<=0) ||
		(destinationSize[2]<= 0))
		return; //error...you have moved outside the possible area

	typename ItkInputImageType::PointType destinationOrigin;
	destinationOrigin[0] = std::max(currOrigin[0], itkNewOrigin[0]);
	destinationOrigin[1] = std::max(currOrigin[1], itkNewOrigin[1]);
	destinationOrigin[2] = std::max(currOrigin[2], itkNewOrigin[2]);

	typename ItkInputImageType::IndexType destinationIndex;
	itkInputImage->TransformPhysicalPointToIndex(destinationOrigin,destinationIndex);

	typename ItkInputImageType::IndexType cpFromIndex;
	cpFromIndex.Fill(0);

	if(destinationOrigin[0]==currOrigin[0])
		cpFromIndex[0] =  itkInputImage->GetLargestPossibleRegion().GetSize()[0] - destinationSize[0];

	if(destinationOrigin[1]==currOrigin[1])
		cpFromIndex[1] =  itkInputImage->GetLargestPossibleRegion().GetSize()[1] - destinationSize[1];

	if(destinationOrigin[2]==currOrigin[2])
		cpFromIndex[2] =  itkInputImage->GetLargestPossibleRegion().GetSize()[2] - destinationSize[2];

	typename ItkInputImageType::Pointer itkDestImage = ItkInputImageType::New();

	itkDestImage->SetRegions(itkInputImage->GetLargestPossibleRegion());
	itkDestImage->Allocate();
	itkDestImage->SetSpacing(itkInputImage->GetSpacing());
	itkDestImage->SetOrigin(currOrigin);

	typename ItkInputImageType::RegionType sourceRegion;
	sourceRegion.SetIndex(cpFromIndex);
	sourceRegion.SetSize(destinationSize);


	pasteFilter->SetSourceImage(itkInputImage);
	pasteFilter->SetDestinationImage(itkDestImage);
	pasteFilter->SetSourceRegion(sourceRegion);
	pasteFilter->SetDestinationIndex(destinationIndex);
	pasteFilter->Update();

	std::cout<<"********tercer paso"<<std::endl;

	itkOuputImage = pasteFilter->GetOutput();
	itkOuputImage->Modified();

}


