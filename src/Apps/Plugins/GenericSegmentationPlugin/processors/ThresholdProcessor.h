#ifndef ThresholdProcessor_H
#define ThresholdProcessor_H

// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "itkBinaryThresholdImageFilter.h"

#include "coreBaseProcessor.h"
#include "coreException.h"
#include "coreCreateExceptionMacros.h"
#include "corePluginMacros.h"

coreCreateRuntimeExceptionClassMacro(ThresholdProcessorException, "Error in ThresholdProcessor", );

namespace gsp{

/**
This class receives inputs from the ThresholdWidget and runs the itk threshold filter on the
current input image.

\author Maarten Nieber
\date 20 Nov 2007
*/
	
class ThresholdParams
{
	public:
		double m_lowerThreshold;
		double m_upperThreshold;
		double insideValue;
		double outsideValue;
		ThresholdParams()
		{
			m_lowerThreshold = 0;
			m_upperThreshold = 0;
			insideValue = 1;
			outsideValue = 0;
	}
};
	

class PLUGIN_EXPORT ThresholdProcessor : public Core::BaseProcessor
{
public:
	//!
	coreDeclareSmartPointerClassMacro(ThresholdProcessor, Core::SmartPointerObject);
	
	//!
	typedef itk::Image< float, 3 > InputImageType;
	//!
	typedef itk::Image< unsigned char, 3 > OutputImageType;
	//!
	typedef Core::DataHolder< InputImageType::Pointer > InputImageHolder;

	
	//! Runs the threshold
	void Update( void );

	//!
	double GetIsoValue ();

	//!
	void SetIsoValue (double val);

	int m_flag;

	//!
	double GetInsideValue();
	
	//!
	double GetOutsideValue();
	
	//!
	double GetLowerThreshold();
	
	//!
	double GetUpperThreshold();

	//!
	void SetInsideValue(double d);
	
	//!
	void SetOutsideValue(double d);
	
	//!
	void SetLowerThreshold(double d);
	
	//!
	void SetUpperThreshold(double d);


private:
	//!
	ThresholdProcessor( );

    //! purposely not implemented
	ThresholdProcessor( const Self& );	

	//! purposely not implemented
	void operator = ( const Self& );    


	double m_isoValue;
	
	double m_LowerThreshold;
	double m_UpperThreshold;
	double m_InsideValue;
	double m_OutsideValue;

};

} // gsp

#endif //ThresholdProcessor_H
