/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _VtkConnectedThresholdProcessor_H
#define _VtkConnectedThresholdProcessor_H

#include "coreBaseProcessor.h"


class ConnectedThresholdParameters
{ 
	public:
		int lowerThreshold;
		int upperThreshold;
		int numberOfIterations;
		double timeStep;
		double conductance;
		bool useImageSpacing;


	ConnectedThresholdParameters()
	{
		lowerThreshold = 150;
		upperThreshold = 300;
		numberOfIterations = 5;
		timeStep = 0.0125;
		conductance = 1.0;
		useImageSpacing = true;
	}
};

namespace gsp{

/**
Processor for 

\ingroup GenericSegmentationPlugin
\author Chiara Riccobene
\date Sept 2010
*/
class PLUGIN_EXPORT VtkConnectedThresholdProcessor : public Core::BaseProcessor
{
public:

	typedef itk::Image<float,3> ImageType;

	typedef enum
	{
		INPUT_IMAGE,
		INPUT_SEED_POINT,
		INPUTS_NUMBER
	} INPUT_TYPE;

	typedef enum
	{
		OUTPUT_IMAGE,
		OUTPUT_MESH,
		OUTPUTS_NUMBER
	}OUTPUT_TYPE;
public:
	//!
	coreProcessor(VtkConnectedThresholdProcessor, Core::BaseProcessor);

	//! Call library to perform operation
	void Update( );

	void SetParameters(ConnectedThresholdParameters* param){m_parameters = param;}
	ConnectedThresholdParameters* GetParameters( ){return m_parameters;}

protected:
	//!
	VtkConnectedThresholdProcessor();

	//!
	~VtkConnectedThresholdProcessor();

	//! Purposely not implemented
	VtkConnectedThresholdProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );

	ConnectedThresholdParameters* m_parameters;
};
    
} // namespace gsp{

#endif //_VtkConnectedThresholdProcessor_H
