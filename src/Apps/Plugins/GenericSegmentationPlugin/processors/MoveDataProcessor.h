/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef MoveDataProcessor_H
#define MoveDataProcessor_H

#include "coreBaseProcessor.h"


/**
\brief Translate a data object

\ingroup ManualSegmentationPlugin
\author Xavi Planes
\date  Aug 2012
*/

class MoveDataProcessor : public Core::BaseProcessor
{
public:
	//!
	coreProcessor(MoveDataProcessor, Core::BaseProcessor);

	typedef enum
	{
		INPUT_DATA_TO_MOVE,
		NUMBER_OF_INPUTS,
	} INPUT_TYPE;

	typedef enum
	{
		MOVED_DATA,
		NUMBER_OF_OUTPUTS,
	} OUTPUT_TYPE;


	//!
	void Update( );

private:

	//!
	MoveDataProcessor( );

	//!
	~MoveDataProcessor();

	//! Purposely not implemented
	MoveDataProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );

private:

};

#endif //MoveDataProcessor_H

