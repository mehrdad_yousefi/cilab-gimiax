/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "TransformationBoxProcessor.h"

#include <string>
#include <iostream>

#include "coreReportExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityHelper.txx"
#include "coreKernel.h"

#include "vtkTransform.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkImageReslice.h"
#include "vtkImageData.h"
#include "vtkUnstructuredGrid.h"
#include "vtkTransformFilter.h"
#include "vtkImageChangeInformation.h"

#include "coreSettings.h"



namespace GenericSegmentationPlugin
{

TransformationBoxProcessor::TransformationBoxProcessor()
{
	SetName( "TransformationBoxProcessor" );

	BaseProcessor::SetNumberOfInputs( INPUT_NUM );
	GetInputPort(INPUT_0)->SetName(  "Input Data" );

	BaseProcessor::SetNumberOfOutputs( OUTPUTS_NUMBER );
	m_transform = NULL;
	m_allTimeSteps = false;
	m_allChildren = false;
}

TransformationBoxProcessor::~TransformationBoxProcessor()
{
}

void TransformationBoxProcessor::SetAllChildren( bool allChildren )
{
	m_allChildren = allChildren;
}

void TransformationBoxProcessor::Update()
{
	if(m_transform==NULL)
		return;

	Core::DataEntity::Pointer dE = GetInputDataEntity(INPUT_0);
	
	UpdateDataEntity( GetInputDataEntity(INPUT_0) );
}

void TransformationBoxProcessor::UpdateDataEntity( Core::DataEntity::Pointer dE )
{
	SetOutputDataEntity(OUTPUT_0, dE );	

	if(dE->IsSurfaceMesh())
	{
		std::vector<Core::vtkPolyDataPtr> outputVect;
		for(unsigned int i=0; i<dE->GetNumberOfTimeSteps();i++)
		{
			Core::vtkPolyDataPtr polyData;
			dE->GetProcessingData(polyData,i);
				
			if (polyData==NULL) {
				return;
			}
			
			Core::vtkPolyDataPtr vtkOutputMesh = vtkPolyData::New();
			if((m_allTimeSteps)||(i==GetTimeStep()))
			{
				vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter;
				transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
				transformFilter->SetInput( polyData );
				transformFilter->SetTransform( m_transform );
				transformFilter->Update();
				vtkOutputMesh->DeepCopy( transformFilter->GetOutput( ) );
			}
			else
			{
				vtkOutputMesh->DeepCopy( polyData );
			}
			outputVect.push_back( vtkOutputMesh );
				
			//UpdateOutput(OUTPUT_0, vtkOutputMesh,dE->GetMetadata()->GetName(), true, 
			//			 1, dE->GetFather() );			
		}
		UpdateOutput<Core::vtkPolyDataPtr>(OUTPUT_0, outputVect,dE->GetMetadata()->GetName(), 
											true, dE->GetFather() );			

	}

	if (dE->IsVolumeMesh()) {
		std::vector<vtkUnstructuredGrid *> outputVect;
		for(unsigned int i=0; i<dE->GetNumberOfTimeSteps();i++)
		{
			vtkSmartPointer<vtkUnstructuredGrid> gridData;
			dE->GetProcessingData(gridData,i);
				
			if (gridData==NULL) {
				return;
			}
				
			vtkSmartPointer<vtkUnstructuredGrid> vtkOutputMesh = vtkUnstructuredGrid::New();
			if((m_allTimeSteps)||(i==GetTimeStep()))
			{
				vtkSmartPointer<vtkTransformFilter> transformFilter;
				transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
				transformFilter->SetInput( gridData );
				transformFilter->SetTransform( m_transform );
				transformFilter->Update();
				
				vtkOutputMesh->DeepCopy( transformFilter->GetOutput( ) );
			}
			else {
				vtkOutputMesh->DeepCopy( gridData );
			}
			outputVect.push_back( vtkOutputMesh );

			//UpdateOutput(OUTPUT_0, vtkOutputMesh,dE->GetMetadata()->GetName(), true, 
			//			 1, dE->GetFather() );			
		}
		UpdateOutput<vtkUnstructuredGrid *>(OUTPUT_0, outputVect,dE->GetMetadata()->GetName(), 
											true, dE->GetFather() );			
	}
		
		
	if (dE->IsImage()) {
		std::vector<vtkImageData *> outputVect;
		for(unsigned int i=0; i<dE->GetNumberOfTimeSteps();i++)
		{
			vtkSmartPointer<vtkImageData> imageData;
			dE->GetProcessingData(imageData,i);
				
			if (imageData==NULL) {
				return;
			}
				
			vtkSmartPointer<vtkImageData> vtkOutputImage = vtkImageData::New();
			if((m_allTimeSteps)||(i==GetTimeStep()))
			{				
				// Get new origin
				double origin[ 3 ];
				origin[ 0 ] = imageData->GetOrigin( )[ 0 ];
				origin[ 1 ] = imageData->GetOrigin( )[ 1 ];
				origin[ 2 ] = imageData->GetOrigin( )[ 2 ];
				double scale[ 3 ];
				m_transform->GetScale( scale );
				double position[ 3 ];
				m_transform->GetPosition( position );
				double rotation[ 3 ];
				m_transform->GetOrientation( rotation );

				//// Center image to 0
				//vtkSmartPointer<vtkImageChangeInformation> ici;
				//ici = vtkSmartPointer<vtkImageChangeInformation>::New();
				//ici->SetInput ( imageData );
				//ici->CenterImageOn();
				//ici->Update();

				//// Only Rotate image
				//vtkSmartPointer <vtkTransform> t = vtkTransform::New();		
				//t->RotateX( rotation[ 0 ] );
				//t->RotateY( rotation[ 1 ] );
				//t->RotateZ( rotation[ 2 ] );
				//vtkSmartPointer<vtkImageReslice> reslice = vtkImageReslice::New();
				//reslice->SetInput( imageData ); 
				//reslice->SetResliceAxesDirectionCosines( t );
				//reslice->SetInterpolationModeToLinear();
				//reslice->Update();

				// Set origin and scale
				double newOrigin[ 3 ];
				newOrigin[ 0 ] = origin[ 0 ] * scale[ 0 ] + position[ 0 ];
				newOrigin[ 1 ] = origin[ 1 ] * scale[ 1 ] + position[ 1 ];
				newOrigin[ 2 ] = origin[ 2 ] * scale[ 2 ] + position[ 2 ];
				double newSpacing[ 3 ];
				newSpacing[ 0 ] = imageData->GetSpacing( )[ 0 ] * scale[ 0 ];
				newSpacing[ 1 ] = imageData->GetSpacing( )[ 1 ] * scale[ 1 ];
				newSpacing[ 2 ] = imageData->GetSpacing( )[ 2 ] * scale[ 2 ];
				vtkSmartPointer<vtkImageChangeInformation> ici;
				ici = vtkSmartPointer<vtkImageChangeInformation>::New();
				ici->SetInput ( imageData );
				ici->SetOutputOrigin( newOrigin );
				ici->SetOutputSpacing( newSpacing );
				ici->Update();


				vtkOutputImage->DeepCopy( ici->GetOutput( ) );
			}
			else {
				vtkOutputImage->DeepCopy( imageData );
			}
			outputVect.push_back( vtkOutputImage );

//				UpdateOutput(OUTPUT_0, vtkOutputImage,dE->GetMetadata()->GetName(), true, 
//							 de->GetNumberOfInputs(), dE->GetFather() );	
				

		}
		UpdateOutput<vtkImageData *>(OUTPUT_0, outputVect,dE->GetMetadata()->GetName(), 
											true, dE->GetFather() );			
	}
		
	if ( m_allChildren )
	{
		Core::DataEntity::ChildrenListType children = dE->GetChildrenList();
		for ( int i = 0 ; i < children.size( ) ;i++ )
		{
			UpdateDataEntity( children[ i ] );
		}
	}
}

}   // namespace GenericSegmentationPlugin
