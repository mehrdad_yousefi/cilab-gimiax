/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "MoveDataProcessor.h"

MoveDataProcessor::MoveDataProcessor() 
{
	SetNumberOfInputs( NUMBER_OF_INPUTS );
	GetInputPort( INPUT_DATA_TO_MOVE )->SetName( "Input data to be moved" );
	GetInputPort( INPUT_DATA_TO_MOVE )->SetDataEntityType( Core::ROITypeId | 
		Core::ImageTypeId | Core::SurfaceMeshTypeId | Core::VolumeMeshTypeId);
	GetInputPort( INPUT_DATA_TO_MOVE )->SetUpdateMode( Core::BaseFilterInputPort::UPDATE_ACCESS_MULTIPLE_TIME_STEP );


	SetNumberOfOutputs( NUMBER_OF_OUTPUTS );
}


MoveDataProcessor::~MoveDataProcessor() 
{

}

void MoveDataProcessor::Update( )
{
}
