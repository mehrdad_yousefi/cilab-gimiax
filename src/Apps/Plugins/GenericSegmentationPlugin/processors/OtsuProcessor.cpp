/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "OtsuProcessor.h"

#include "coreAssert.h"
#include "coreDataEntity.h"
#include "coreDataEntityUtilities.h"
#include "coreDataEntityUtilities.txx"
#include "coreLoggerHelper.h"

#include "meBinImageToMeshFilter.h"

using namespace gsp;

OtsuProcessor::OtsuProcessor()
{
	//! Set inputs and outpus for the OtsuProcessor
	BaseProcessor::SetNumberOfInputs( 1 );
	GetInputPort( 0 )->SetName( "Input Image" );
	GetInputPort( 0 )->SetDataEntityType( Core::ImageTypeId );
	BaseProcessor::SetNumberOfOutputs( 2 );
	GetOutputPort( 0 )->SetDataEntityType(Core::ImageTypeId|Core::ROITypeId);
	GetOutputPort( 1 )->SetDataEntityType(Core::SurfaceMeshTypeId);

	this->m_OtsuFilter = OtsuImageFilter::New();
//	Core::Initialize(this->m_ParametersHolder, evoOtsuParameters::New());

	insideValue = 1;
	outsideValue = 0;
	numberOfHistogramBins = 128;
	threshold = 0;

	
}

void OtsuProcessor::Update()
{
	InputImageType::Pointer inputImage;
	//The Otsu parameters
	GetProcessingData( 0, inputImage );

	// Running the API filter
	m_OtsuFilter->SetInput( inputImage );
	m_OtsuFilter->Update();
	std::cout << "Otsu ComputationTime -> " << m_OtsuFilter->GetComputationTime() <<
		" seconds" << std::endl;
	threshold = m_OtsuFilter->GetThreshold();

	double isoValue = 0.5 * (insideValue + outsideValue);

	Core::vtkPolyDataPtr mesh = BinImageToMesh::CreateIsoSurface<itk::Image< unsigned char, 3 > >(
									m_OtsuFilter->GetOutput(), 
									isoValue, 
									false);
	// Set the output mesh of this processor
	UpdateOutput(1, 
				mesh, 
				"Otsu mesh",
				false,
				1,
				GetInputDataEntity(0));
		
	// Set the output image of this processor
	UpdateOutput( 0 ,
				m_OtsuFilter->GetOutput(), 
				"Otsu",
				false,
				1,
				GetInputDataEntity(0));	

}

//!
void OtsuProcessor::SetInsideValue(float _insideValue)
{
	insideValue = _insideValue;
}

//!
void OtsuProcessor::SetOutsideValue(float _outsideValue)
{
	outsideValue = _outsideValue;
}

//!
void OtsuProcessor::SetNumberOfHistogramBins(unsigned int _numberOfHistogramBins)
{
	numberOfHistogramBins = _numberOfHistogramBins;
}

//!
void OtsuProcessor::SetThreshold(float _threshold)
{
	threshold = _threshold;
}

//!
float OtsuProcessor::GetInsideValue(void)
{
	return insideValue;
}

//!
float OtsuProcessor::GetOutsideValue(void)
{
	return outsideValue;
}

//!
unsigned int OtsuProcessor::GetNumberOfHistogramBins(void)
{
	return numberOfHistogramBins;
}

//!
float OtsuProcessor::GetThreshold(void)
{
	return threshold;
}