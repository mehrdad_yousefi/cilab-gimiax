// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "ConnectedThresholdProcessor.h"

#include "coreAssert.h"
#include "coreDataEntity.h"
#include "coreLoggerHelper.h"
#include "coreDataEntityUtilities.h"
#include "coreDataEntityUtilities.txx"
#include "meBinImageToMeshFilter.h"

using namespace gsp;

void ConnectedThresholdProcessor::Update()
{
	ConnectedThresholdProcessor::InputImageType::Pointer inputImage;

	//a helper function to adapt the input image to itk image representation
	GetProcessingData( INPUT_IMAGE, inputImage );

	// Convert from physical point to index.
	InputImageType::PointType physicalPoint;
	Core::vtkPolyDataPtr seedPoint;
	Core::DataEntityHelper::GetProcessingData(
		GetInputDataEntityHolder( INPUT_SEED_POINT),
		seedPoint);
	physicalPoint[0] = seedPoint->GetPoint(0)[0];
	physicalPoint[1] = seedPoint->GetPoint(0)[1];
	physicalPoint[2] = seedPoint->GetPoint(0)[2];
	InputImageType::IndexType seed; 
	inputImage->TransformPhysicalPointToIndex(physicalPoint, seed);
	SetSeed(seed);

	// Running the API filter
	m_Filter->SetInput( inputImage );
	m_Filter->Update();
	cout << "Connected Threshold ComputationTime -> " << m_Filter->GetComputationTime() << " seconds" << endl;

	double isoValue = 0.5;

	Core::vtkPolyDataPtr mesh = BinImageToMesh::CreateIsoSurface<OutputImageType>(
		m_Filter->GetOutput(), 
		isoValue, 
		false);

	// Set the output mesh of this processor
	UpdateOutput(1, 
		mesh, 
		"Connected threshold mesh",
		false,
		1,
		GetInputDataEntity(INPUT_IMAGE));

	// Set the output image of this processor
	//UpdateOutputImageAsVtk<OutputImageType>( 0 ,
	UpdateOutput( 0 ,
		m_Filter->GetOutput(), 
		"Connected threshold segmentation",
		false,
		1,
		GetInputDataEntity(INPUT_IMAGE));	
}

ConnectedThresholdProcessor::ConnectedThresholdProcessor()
{
	//! Set inputs and outpus for the OtsuProcessor
	BaseProcessor::SetNumberOfInputs( NUMBER_OF_INPUTS );
	GetInputPort( INPUT_IMAGE )->SetName( "Input Image" );
	GetInputPort( INPUT_IMAGE )->SetDataEntityType( Core::ImageTypeId );
	GetInputPort( INPUT_SEED_POINT )->SetName( "Seed Point" );
	GetInputPort( INPUT_SEED_POINT )->SetDataEntityType( Core::PointSetTypeId );
	BaseProcessor::SetNumberOfOutputs( 2 );

	m_Filter = blConnectedThresholdImageFilter::New();

	this->m_LowerThreshold = 0;
	this->m_UpperThreshold = 0;
	this->m_ReplaceValue = 1;
	this->m_NumberOfIterations = 5;
	this->m_TimeStep = 0.0125;
	this->m_Conductance = 1.0;
	this->m_UseImageSpacing = true;
}


void ConnectedThresholdProcessor::SetLowerThreshold(double d)
{
	m_LowerThreshold = d;
	m_Filter->SetLowerThreshold(d);
}

double ConnectedThresholdProcessor::GetLowerThreshold()
{
	return m_LowerThreshold;
}

void ConnectedThresholdProcessor::SetUpperThreshold(double d)
{
	m_UpperThreshold = d;
	m_Filter->SetUpperThreshold(d);
}

double ConnectedThresholdProcessor::GetUpperThreshold()
{
	return m_UpperThreshold;
}

void ConnectedThresholdProcessor::SetReplaceValue(int i)
{
	m_ReplaceValue = i;
	m_Filter->SetReplaceValue(i);
}

int ConnectedThresholdProcessor::GetReplaceValue()
{
	return m_ReplaceValue;
}

void ConnectedThresholdProcessor::SetSeed(InputImageType::IndexType index)
{
	m_Seed = index;
	m_Filter->SetSeed(index);
}

ConnectedThresholdProcessor::InputImageType::IndexType ConnectedThresholdProcessor::GetSeed()
{
	return m_Seed;
}

void ConnectedThresholdProcessor::SetNumberOfIterations(unsigned int u)
{
	m_NumberOfIterations = u;
	m_Filter->SetNumberOfIterations(u);
}

unsigned int ConnectedThresholdProcessor::GetNumberOfIterations()
{
	return m_NumberOfIterations;
}

void ConnectedThresholdProcessor::SetTimeStep(double d)
{
	m_TimeStep = d;
	m_Filter->SetTimeStep(d);
}

double ConnectedThresholdProcessor::GetTimeStep()
{
	return m_TimeStep;
}

void ConnectedThresholdProcessor::SetConductance(double d)
{
	m_Conductance = d;
	m_Filter->SetConductance(d);
}

double ConnectedThresholdProcessor::GetConductance()
{
	return m_Conductance;
}

void ConnectedThresholdProcessor::SetImageSpacing(bool b)
{
	m_UseImageSpacing = b;
	m_Filter->SetImageSpacing(b);
}

bool ConnectedThresholdProcessor::GetImageSpacing()
{
	return m_UseImageSpacing;
}
