// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "RegionGrowProcessor.h"

#include "coreLoggerHelper.h"
#include "coreImageDataEntityMacros.h"
#include "coreDataEntityUtilities.h"
#include "coreDataEntityUtilities.txx"
#include "coreAssert.h"
#include "coreDataEntity.h"

#include "vtkSmartPointer.h"
#include "vtkPointSet.h"

#include "meBinImageToMeshFilter.h"

using namespace gsp;

namespace gsp{

/**
This is a helper class for RegionGrowProcessor that performs the region growing on an itk image.
It is used in Gimias::RegionGrowProcessor::Update.

\author Maarten Nieber
\date 16 sep 2008
*/

struct RegionGrowProcessorExecute
{
	//! Stores the processor for use in the Execute function
	gsp::RegionGrowProcessor::Pointer processor;

	//! Executes the region grow on \a image.
	template< class ItkImageType > void Execute(typename ItkImageType::Pointer image)
	{
		typedef itk::NeighborhoodConnectedImageFilter<ItkImageType, ItkImageType > //RegionGrowProcessor::OutputImageType > 
			RegionGrowFilterType;
		typename RegionGrowFilterType::Pointer filter = RegionGrowFilterType::New();

		itk::Point<double, 3> physicalPoint;
		Core::vtkPolyDataPtr seedPoints;
		Core::DataEntityHelper::GetProcessingData(
			processor->GetInputDataEntityHolder( RegionGrowProcessor::INPUT_SEED_POINT),
			seedPoints);

		filter->SetInput(image);
		for(int i=0; i<seedPoints->GetNumberOfPoints();i++)
		{
			physicalPoint[0] = seedPoints->GetPoint(i)[0];
			physicalPoint[1] = seedPoints->GetPoint(i)[1];
			physicalPoint[2] = seedPoints->GetPoint(i)[2];

			typename RegionGrowFilterType::IndexType seed;
			image->TransformPhysicalPointToIndex(physicalPoint, seed);

			std::cout << "physicalPoint = (" << physicalPoint[0] << "," << physicalPoint[1] << "," << physicalPoint[2] << ")" << std::endl;
			std::cout << "seed = (" << seed[0] << "," << seed[1] << "," << seed[2] << ")" << std::endl;
			filter->AddSeed(seed);

		}

		filter->SetReplaceValue(processor->GetReplaceValue());
		filter->SetUpper(processor->GetUpperThreshold());
		filter->SetLower(processor->GetLowerThreshold());
		filter->Update();

		//Add image and mesh (at isosurface of 0.5)
		double isoValue = 0.5;
		Core::vtkPolyDataPtr mesh = BinImageToMesh::CreateIsoSurface<ItkImageType>( //RegionGrowProcessor::OutputImageType>(
			filter->GetOutput(), 
			isoValue, 
			false);

		// Set the output mesh of this processor
		processor->UpdateOutput(1, 
			mesh, 
			"Region grow mesh",
			false,
			1,
			processor->GetInputDataEntity(RegionGrowProcessor::INPUT_IMAGE));

		// Set the output image of this processor
		//processor->UpdateOutputImageAsVtk<RegionGrowProcessor::OutputImageType>( 0 ,
		
		typedef itk::ImageToVTKImageFilter< ItkImageType > ITK2VTKType;
		typename ITK2VTKType::Pointer itk2vtk = ITK2VTKType::New();
		itk2vtk->SetInput( filter->GetOutput() );
		itk2vtk->Update();
		processor->UpdateOutput(
			0,
			itk2vtk->GetOutput(),
			"Region grow segmentation",
			false,
			1,
			processor->GetInputDataEntity(RegionGrowProcessor::INPUT_IMAGE));
	}
};

} // gsp

void gsp::RegionGrowProcessor::Update( void )
{
	if (m_LowerThreshold>m_UpperThreshold)
	{
		std::swap(m_LowerThreshold,m_UpperThreshold);
	}

	RegionGrowProcessorExecute execute;
	execute.processor = this;

	bool hasValidInput = GetInputDataEntity(0) &&
		GetInputDataEntity(INPUT_IMAGE)->GetType() == Core::ImageTypeId;
	if( !hasValidInput )
	{
		Core::LoggerHelper::ShowMessage( 
			"Please select a Volume Image input from the Data List.", 
			Core::LoggerHelper::MESSAGE_TYPE_ERROR );
		return;
	}

	try
	{
		coreImageDataEntityItkMacro( this->GetInputDataEntity(INPUT_IMAGE), execute.Execute );
	}
	catch( ... )
	{
		throw;
	}
}

gsp::RegionGrowProcessor::RegionGrowProcessor()
{
	//! Set inputs and outpus for the OtsuProcessor
	BaseProcessor::SetNumberOfInputs( NUMBER_OF_INPUTS );

	GetInputPort( INPUT_IMAGE )->SetName( "Input Image" );
	GetInputPort( INPUT_IMAGE )->SetDataEntityType( Core::ImageTypeId );
	GetInputPort( INPUT_SEED_POINT )->SetName( "Input point" );
	GetInputPort( INPUT_SEED_POINT )->SetDataEntityType( Core::PointSetTypeId );

	BaseProcessor::SetNumberOfOutputs( 2 );
	GetOutputPort( 0 )->SetDataEntityType(Core::ImageTypeId|Core::ROITypeId);
	GetOutputPort( 1 )->SetDataEntityType(Core::SurfaceMeshTypeId);

	// Initialize parameters.
	SetReplaceValue(1.0);
	SetLowerThreshold(200.0);
	SetUpperThreshold(300.0);
}

void gsp::RegionGrowProcessor::SetReplaceValue(double d)
{
	m_ReplaceValue = d;
}

void gsp::RegionGrowProcessor::SetUpperThreshold(double d)
{
	m_UpperThreshold = d;
}

void gsp::RegionGrowProcessor::SetLowerThreshold(double d)
{
	m_LowerThreshold = d;
}

double gsp::RegionGrowProcessor::GetReplaceValue()
{
	return m_ReplaceValue;
}

double gsp::RegionGrowProcessor::GetUpperThreshold()
{
	return m_UpperThreshold;
}

double gsp::RegionGrowProcessor::GetLowerThreshold()
{
	return m_LowerThreshold;
}

