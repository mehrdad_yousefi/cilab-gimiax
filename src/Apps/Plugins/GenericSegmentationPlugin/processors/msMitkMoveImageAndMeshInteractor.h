/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef MITKMoveImageAndMeshInteractor_H_HEADER_INCLUDED
#define MITKMoveImageAndMeshInteractor_H_HEADER_INCLUDED

#include <mitkInteractor.h>
#include "mitkCommon.h"
#include <stack>

namespace mitk
{
  class DataTreeNode;

  class MoveImageAndMeshInteractorCallaback
  {	
  public:
	  virtual void OnModifiedRenderingData(double shiftDist[3]) = 0;

  };
  
  /**
   * \brief Interaction to move an image with mouse move
   *
   * \ingroup GenericSegmentationPlugin
   */
  class MoveImageAndMeshInteractor : public Interactor
  {
  public:
    mitkClassMacro(MoveImageAndMeshInteractor, Interactor);
    mitkNewMacro2Param(Self, const char*, DataTreeNode*);

    /**
     * \brief check how good an event can be handled 
     */
    //virtual float CanHandleEvent(StateEvent const* stateEvent) const; 
    //used from mitkInteractor

    /**
    *@brief Gets called when mitk::DataTreeNode::SetData() is called
    * 
    * No need to use it here, because the pattern won't be complex 
    * and we can take care of unexpected data change
    **/
    //virtual void DataChanged(){};

	MoveImageAndMeshInteractorCallaback * GetCallback() const { return m_callback; }
	void SetCallback(MoveImageAndMeshInteractorCallaback * val) { m_callback = val; }

  protected:
    /**
     * \brief Constructor 
     */
    MoveImageAndMeshInteractor(const char * type, DataTreeNode* node);

    /**
     * \brief Default Destructor
     **/
    virtual ~MoveImageAndMeshInteractor();

    /**
    * @brief Convert the given Actions to Operations and send to data and UndoController
    **/
    virtual bool ExecuteAction( Action* action, mitk::StateEvent const* stateEvent );


	/**
	* @brief rewritten since this class can handle the jurisdiction better!
	* the Interactor superClass does not handle very well neither mouse
	*dragging events nor keyboard events!
	**/
	virtual float CalculateJurisdiction(StateEvent const* stateEvent) const;

	//! do the translation of the current geometry3D
	void Translate(mitk::Vector3D *translateVect);

	//! starting point
	mitk::Point3D m_initialPickedPoint, m_lastPickedPoint;

	mitk::Vector3D m_lastInteractionMove;

	std::stack<mitk::Vector3D> m_undoList;

	MoveImageAndMeshInteractorCallaback *m_callback;

	template<typename TPixel, unsigned int VImageDimension>
	void ItkImageSwitch( itk::Image<TPixel,VImageDimension>* itkImage, mitk::Vector3D *vector );

	template<typename TPixel1, unsigned int VImageDimension1, typename TPixel2, unsigned int VImageDimension2 > 
	void InternalCompute( itk::Image< TPixel1, VImageDimension1 >* itkInputImage, itk::Image< TPixel2, VImageDimension2 >* itkOuputImage, mitk::Vector3D *translateVect );
	mitk::Image::ConstPointer m_imageTStep;
  };
}

#endif /* MITKMoveImageAndMeshInteractor_H_HEADER_INCLUDED */
