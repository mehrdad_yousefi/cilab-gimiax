/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "gspPropagateLandmarksProcessor.h"

#include <string>
#include <iostream>

#include "coreReportExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityHelper.txx"
#include "coreKernel.h"
#include "coreVTKPolyDataHolder.h"
#include "coreVTKImageDataHolder.h"
#include "coreLoggerHelper.h"

#include "vtkSmartPointer.h"
#include "vtkPointData.h"
#include "blShapeUtils.h"

namespace GenericSegmentationPlugin
{

PropagateLandmarksProcessor::PropagateLandmarksProcessor()
{
	SetName( "PropagateLandmarksProcessor" );
	
	BaseProcessor::SetNumberOfInputs( 1 );
	GetInputPort(INPUT_0)->SetName(  "Input Landmarks" );
	GetInputPort(INPUT_0)->SetDataEntityType( Core::PointSetTypeId);
// 	GetInputPort(INPUT_1)->SetName(  "Input Skeleton" );
// 	GetInputPort(INPUT_1)->SetDataEntityType( Core::SkeletonTypeId);
	BaseProcessor::SetNumberOfOutputs( 2);
	GetOutputPort(0)->SetDataEntityType(Core::PointSetTypeId);
	GetOutputPort(1)->SetDataEntityType(Core::SkeletonTypeId);
	
}

PropagateLandmarksProcessor::~PropagateLandmarksProcessor()
{
}

void PropagateLandmarksProcessor::Update()
{
	Core::vtkPolyDataPtr pointsCurrentTimeStep;
	GetProcessingData<Core::vtkPolyDataPtr>(
		INPUT_0,
		pointsCurrentTimeStep ,
		m_TimeStepHolder->GetSubject());

	std::vector<Core::vtkPolyDataPtr> points;
	GetProcessingData<Core::vtkPolyDataPtr>(
		INPUT_0,
		points );

	if (points.size()-1 == m_TimeStepHolder->GetSubject())
	{
		Core::LoggerHelper::ShowMessage(
			"You're on the last timestep",
			Core::LoggerHelper::MESSAGE_TYPE_INFO);
		return;

	}
	points.at(m_TimeStepHolder->GetSubject()+1)->DeepCopy(pointsCurrentTimeStep);
		
	//GetInputDataEntity(INPUT_0)->Modified();
	GetInputDataEntityHolder(INPUT_0)->NotifyObservers();

}

Core::IntHolderType::Pointer PropagateLandmarksProcessor::GetTimeStepHolder()
{
	return m_TimeStepHolder;
}

void PropagateLandmarksProcessor::SetTimeStepHolder( Core::IntHolderType::Pointer holder )
{
	m_TimeStepHolder =holder;
}

}   // namespace GenericSegmentationPlugin
