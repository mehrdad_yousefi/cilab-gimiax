/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "SurfaceMeshToRoiProcessor.h"

#include "vtkPolyDataToImageStencil.h"
#include "vtkImageStencil.h"
#include "vtkImageMathematics.h"
#include "coreVTKImageDataHolder.h"
#include "coreVTKPolyDataHolder.h"


using namespace Core;

//!
SurfaceMeshToRoiProcessor::SurfaceMeshToRoiProcessor(void)
{
	SetNumberOfInputs(2);
	GetInputPort( 0 )->SetDataEntityType( Core::SurfaceMeshTypeId );
	GetInputPort( 0 )->SetName( "Input Surface Mesh" );
	GetInputPort( 1 )->SetDataEntityType( Core::ImageTypeId );
	GetInputPort( 1 )->SetName( "Input Image" );
	SetNumberOfOutputs(1);
	GetOutputPort( 0 )->SetDataEntityType( Core::ImageTypeId | Core::ROITypeId );
	GetOutputPort( 0 )->SetReuseOutput( true );
	GetOutputPort( 0 )->SetName( "Output mask image" );
	GetOutputPort( 0 )->SetDataEntityName( "Output mask image" );
}

//!
SurfaceMeshToRoiProcessor::~SurfaceMeshToRoiProcessor(void)
{
}

//!
void SurfaceMeshToRoiProcessor::Update( )
{

	vtkPolyDataPtr inputMesh;
	GetProcessingData( 0, inputMesh );

	int numberOfTimeSteps;
	numberOfTimeSteps = GetInputDataEntity(1)->GetNumberOfTimeSteps();

	std::vector<Core::vtkImageDataPtr> outputTimeStep;

	for (int i=0; i<numberOfTimeSteps; i++)
	{
		Core::vtkImageDataPtr inputImage;
		GetProcessingData( 1, inputImage, i );

		vtkSmartPointer<vtkPolyDataToImageStencil> stencilFilter;
		stencilFilter = vtkSmartPointer<vtkPolyDataToImageStencil>::New();
		stencilFilter->SetTolerance(1.0);
		stencilFilter->SetInput( inputMesh );
		stencilFilter->SetInformationInput( inputImage );
		stencilFilter->Update();

		// Create an empty image
		vtkSmartPointer<vtkImageMathematics> operation;
		operation = vtkSmartPointer<vtkImageMathematics>::New();
		operation->SetInput1(inputImage);
		operation->SetInput2(inputImage);
		operation->SetOperationToSubtract();
		operation->Update();

		vtkSmartPointer<vtkImageStencil> imageStencil;
		imageStencil = vtkSmartPointer<vtkImageStencil>::New();
		//imageStencil->SetInput( outputImage );
		imageStencil->SetInput( operation->GetOutput() );
		imageStencil->SetStencil( stencilFilter->GetOutput( ) );
		imageStencil->SetBackgroundValue( 1 );
		imageStencil->Update();

		// 0->1 and 1->2
		vtkSmartPointer<vtkImageMathematics> operation1;
		operation1 = vtkSmartPointer<vtkImageMathematics>::New();
		operation1->SetInput1(imageStencil->GetOutput());
		operation1->SetOperationToAddConstant();
		operation1->SetConstantC( 1 );
		operation1->Update();

		// 2->0
		vtkSmartPointer<vtkImageMathematics> operation2;
		operation2 = vtkSmartPointer<vtkImageMathematics>::New();
		operation2->SetInput1(operation1->GetOutput());
		operation2->SetOperationToReplaceCByK();
		operation2->SetConstantC( 2 );
		operation2->SetConstantK( 0 );
		operation2->Update();

		Core::vtkImageDataPtr vTimeStep = Core::vtkImageDataPtr::New();
		vTimeStep->DeepCopy(operation2->GetOutput());
		outputTimeStep.push_back(vTimeStep);

	}
	
	UpdateOutput( 0, outputTimeStep, "Mask image", true, GetInputDataEntity( 0 ) );
}

