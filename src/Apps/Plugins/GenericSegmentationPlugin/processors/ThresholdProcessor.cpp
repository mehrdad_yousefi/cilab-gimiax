// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "ThresholdProcessor.h"

#include "coreDataEntityUtilities.h"
#include "coreDataEntityUtilities.txx"
#include "coreLoggerHelper.h"
#include "coreImageDataEntityMacros.h"

#include "vtkMarchingCubes.h"
#include "vtkPolyDataNormals.h"
#include "vtkCleanPolyData.h"
#include "vtkSmartPointer.h"
#include "vtkSmoothPolyDataFilter.h"
#include "vtkDecimatePro.h"
#include "vtkImageGaussianSmooth.h"
#include "itkImageToVTKImageFilter.h"
#include "itkCastImageFilter.h"

#include "meRemoveInnerSurfaceFilter.h"

#include "meBinImageToMeshFilter.h"

using namespace gsp;

namespace gsp{

/**
This is a helper class for RegionGrowProcessor that performs the region growing on an itk image.
It is used in Gimias::RegionGrowProcessor::Run.

\author Maarten Nieber
\date 16 sep 2008
*/

struct ThresholdProcessorExecute
{
	//! Stores the processor for use in the Execute function
	gsp::ThresholdProcessor::Pointer processor;

	//! Executes the region grow on \a image.
	template< class ItkImageType > void Execute(typename ItkImageType::Pointer image)
	{	
		if (processor->m_flag == 0) //Threshold segmentation button has been pressed
		{
			typedef itk::BinaryThresholdImageFilter<ItkImageType, ThresholdProcessor::OutputImageType> FilterType;
			typename FilterType::Pointer filter = FilterType::New();

			filter->SetInput(image);
			filter->SetInsideValue(processor->GetInsideValue());
			filter->SetOutsideValue(processor->GetOutsideValue());
			filter->SetLowerThreshold(processor->GetLowerThreshold());
			filter->SetUpperThreshold(processor->GetUpperThreshold());
			filter->Update();

			// Set the output image of this processor
			this->processor->UpdateOutput( 
				0,
				filter->GetOutput(), 
				"Threshold segmentation",
				true,
				1,
				this->processor->GetInputDataEntity(0) );
		}
		else //Isosurface slider is moved
		{
			double isoValue = this->processor->GetIsoValue();

			Core::vtkPolyDataPtr mesh = BinImageToMesh::CreateIsoSurface<ItkImageType>(
				image,
				isoValue,
				false);

			// Set the output mesh of this processor
			this->processor->UpdateOutput(1, 
				mesh, 
				"Threshold mesh",
				true,
				1,
				this->processor->GetInputDataEntity(0));
		}
	}
};

} // gsp

void ThresholdProcessor::Update( void )
{	
	ThresholdProcessorExecute execute;
	execute.processor = this;

	Core::DataEntity::Pointer inputDE = GetInputDataEntity( 0 );
	bool hasValidInput =  inputDE.IsNotNull() &&
		( ( inputDE->GetType() == Core::ImageTypeId ) ||
		  ( inputDE->GetType() == Core::ROITypeId ) ||
		  ( inputDE->GetType() == ( Core::ImageTypeId | Core::ROITypeId ) ) );
	if( !hasValidInput )
	{
		Core::LoggerHelper::ShowMessage( 
			"Please select a Volume Image input from the Data List.", 
			Core::LoggerHelper::MESSAGE_TYPE_ERROR );
		return;
	}

	try
	{
		coreImageDataEntityItkMacro(GetInputDataEntity( 0 ), execute.Execute );
	}
	catch( ... )
	{
		throw;
	}
}

ThresholdProcessor::ThresholdProcessor()
{
	//! Set inputs and outpus for the OtsuProcessor
	BaseProcessor::SetNumberOfInputs( 1 );
	GetInputPort( 0 )->SetName( "Input image" );
	GetInputPort( 0 )->SetDataEntityType( Core::ImageTypeId );
	BaseProcessor::SetNumberOfOutputs( 2 );
	GetOutputPort( 0 )->SetDataEntityType(Core::ImageTypeId|Core::ROITypeId);
	GetOutputPort( 1 )->SetDataEntityType(Core::SurfaceMeshTypeId);

	//parameters initialization
	m_LowerThreshold = 0.0;
	m_UpperThreshold = 0.0;
	m_InsideValue = 1.0;
	m_OutsideValue = 0.0;
}



double ThresholdProcessor::GetInsideValue()
{
	return m_InsideValue;
}


double ThresholdProcessor::GetOutsideValue()
{
	return m_OutsideValue;
}


double ThresholdProcessor::GetLowerThreshold()
{
	return m_LowerThreshold;
}


double ThresholdProcessor::GetUpperThreshold()
{
	return m_UpperThreshold;
}


void ThresholdProcessor::SetInsideValue(double d)
{
	m_InsideValue = d;
	SetIsoValue (0.5 * (m_InsideValue + m_OutsideValue));
}


void ThresholdProcessor::SetOutsideValue(double d)
{
	m_OutsideValue = d;
}


void ThresholdProcessor::SetLowerThreshold(double d)
{
	m_LowerThreshold = d;
}


void ThresholdProcessor::SetUpperThreshold(double d)
{
	m_UpperThreshold = d;
}





double ThresholdProcessor::GetIsoValue ()
{
		return m_isoValue;
};

void ThresholdProcessor::SetIsoValue (double val)
{
		m_isoValue = val;
};