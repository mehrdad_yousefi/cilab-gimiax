/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef _SurfaceMeshToRoiProcessor_H
#define _SurfaceMeshToRoiProcessor_H

#include "corePluginMacros.h"
#include "coreBaseProcessor.h"

namespace Core
{
/**
\brief Create a mask image from a surface mesh 

\sa Core::MeshCreation
\ingroup GenericSegmentationPlugin
\author Xavi Planes
\date 07 May 2010
*/
class PLUGIN_EXPORT SurfaceMeshToRoiProcessor : public Core::BaseProcessor
{
public:
	coreDeclareSmartPointerClassMacro(SurfaceMeshToRoiProcessor, Core::BaseFilter);

	//!
	void Update( );

protected:
	SurfaceMeshToRoiProcessor(void);
	virtual ~SurfaceMeshToRoiProcessor(void);

private:
	coreDeclareNoCopyConstructors(SurfaceMeshToRoiProcessor);
};

} // namespace Core

#endif

