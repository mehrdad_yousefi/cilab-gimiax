/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _gspPhilipsSegmentedScarMeshProcessor_H
#define _gspPhilipsSegmentedScarMeshProcessor_H

#include "coreBaseProcessor.h"


#include "vtkSmartPointer.h"
#include "vtkImageData.h"
class vtkPolyData;

namespace gsp
{

/**
Processor for 

\ingroup GenericSegmentationPlugin
\author David Lucena Gonz�lez
\date 06 Jun 2011
*/
class PhilipsSegmentedScarMeshProcessor : public Core::BaseProcessor
{
public:

	typedef enum
	{
		PM_EXTRACTSCAR,
		PM_MAPSCAR,
	} PROCESSOR_METHOD;

	class PSSMPParameters
	{
		// public methods
	public:
		PSSMPParameters() : m_ProcessorMethod( PM_EXTRACTSCAR )
		{};
		virtual ~PSSMPParameters() {};
		// public members
	public:
		PROCESSOR_METHOD m_ProcessorMethod;
	};


	typedef enum
	{
		INPUT_PHILIPSLVSEGMENTEDSCAR,
		INPUT_VOLUMEMESH,
		INPUTS_NUMBER
	} INPUT_TYPE;

	typedef enum
	{
		OUTPUT_PHILIPSLVSCARMESH,
		OUTPUT_ANNOTATEDVOLUMELVMESH,
		OUTPUTS_NUMBER
	} OUTPUT_TYPE;
public:
	//!
	coreProcessor( PhilipsSegmentedScarMeshProcessor, Core::BaseProcessor );
	
	//! Call library to perform operation
	void Update();

	PSSMPParameters& GetParameters();

private:
	//!
	PhilipsSegmentedScarMeshProcessor();

	//!
	~PhilipsSegmentedScarMeshProcessor();

	//! Purposely not implemented
	PhilipsSegmentedScarMeshProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );


	/**\brief Computes the values range of a dataArray
	*
	*  Computes the minimum and maximum values present in the
	*  given dataArray, not counting the zero value as part of the range
	*  This is the template method used by ComputeNonZeroRange
	*
	*  \param T the dataArray enclosed type
	*  \param dataArray the dataArray to calculate the range for
	*  \return outputRange range of values in dataArray
	*  \sa ComputeNonZeroRange()
	*/
	template <class T> void ComputeNonZeroRangeT( T theType, vtkDataArray* dataArray, double outputRange[2] ) const;
	//! 
	/**\brief Computes the values range of a dataArray
	*
	*  Computes the minimum and maximum values present in the
	*  given dataArray, not counting the zero value as part of the range
	*
	*  \param dataArray the dataArray to calculate the range for
	*  \return outputRange range of values in dataArray
	*  \sa ComputeNonZeroRangeT()
	*/
	void ComputeNonZeroRange( vtkSmartPointer<vtkImageData> imageData, double outputRange[2] ) const;
	void CreateROIImageVectorFromImageVector(
		const std::vector<vtkSmartPointer<vtkImageData> >& inputVectorSP,
		std::vector<vtkSmartPointer<vtkImageData> >& outputVectorSP ) const;
	vtkPolyData* CreateIsoSurfaceFromVTKImage( vtkImageData* inputImage, int isoValue ) const;
	void CreateIsoSurfaceVectorFromVTKImageVector(
		const std::vector<vtkSmartPointer<vtkImageData> >& inputImageSPVector,
		std::vector<vtkSmartPointer<vtkPolyData> >& outputVectorSP ) const;

	void ExtractScar();
	void MapScar();

	PSSMPParameters m_Parameters;
};
    
} // namespace gsp

#endif //_gspPhilipsSegmentedScarMeshProcessor_H
