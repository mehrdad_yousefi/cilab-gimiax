/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "msMitkSurfaceDeformationInteractor3D.h"
#include "mitkPointOperation.h"
#include "mitkDisplayPositionEvent.h"
#include "mitkWheelEvent.h"
#include "mitkStatusBar.h"
#include "mitkDataTreeNode.h"
#include "mitkInteractionConst.h"
#include "mitkAction.h"
#include "mitkStateEvent.h"
#include "mitkOperationEvent.h"
#include "mitkUndoController.h"
#include "mitkStateMachineFactory.h"
#include "mitkStateTransitionOperation.h"
#include "mitkBaseRenderer.h"
#include "mitkRenderingManager.h"
#include "mitkSurface.h"
#include "mitkVtkScalarModeProperty.h"
#include "mitkProperties.h"
#include "mitkMaterialProperty.h"
#include "mitkLine.h"


#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyle.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkDataArray.h>
#include <vtkPointSet.h>
#include <vtkSphereSource.h>
#include <vtkPlane.h>
#include <vtkStripper.h>
#include <vtkCutter.h>
#include "vtkShortArray.h"
#include "vtkDoubleArray.h"

#include "blShapeUtils.h"
#include "blMITKUtils.h"

#define SELECTIONNAME "__selectionScalar__"


#define min(a,b) (a<b)?a:b;
#define max(a,b) (a>b)?a:b;



//how precise must the user pick the point
//default value
mitk::SurfDeformationInteractor
::SurfDeformationInteractor(const char * type, mitk::DataTreeNode* dataTreeNode, mitk::DataTreeNode* pointSetNode)
: Interactor( type, dataTreeNode )
{
	m_GaussSigma = GAUSSDEFAULT;
	m_Precision = 6.5;
	m_PickedSurfaceNode = NULL;
	m_PickedSurface = NULL;
	m_SphereRes = 200.0;

	mitk::State::Pointer startState = mitk::GlobalInteraction::GetInstance()->GetStartState(type);
	if ( startState.IsNull( ) )
	{
		throw Core::Exceptions::Exception( "SurfDeformationInteractor::SurfDeformationInteractor", 
			"Cannot initialize interactor" );
	}

	m_OriginalPolyData = vtkSmartPointer<vtkPolyData>::New();

	// Initialize vector arithmetic
	m_ObjectNormal[0] = 1.0/sqrt(3.0);
	m_ObjectNormal[1] = 1.0/sqrt(3.0);
	m_ObjectNormal[2] = 1.0/sqrt(3.0);
	m_pointSetNode = pointSetNode;
	m_createSphereMode = false;
	m_CallBack = NULL;
	m_OriginalTimeStep = -1;
	m_SelectedScalarArray.name = "";
	m_SelectedScalarArray.mode = blShapeUtils::ShapeUtils::SCALAR_ARRAY_NONE;
}

mitk::SurfDeformationInteractor::~SurfDeformationInteractor()
{
}

void mitk::SurfDeformationInteractor::SetPrecision( mitk::ScalarType precision )
{
	m_Precision = precision;
}

// Overwritten since this class can handle it better!
float mitk::SurfDeformationInteractor
::CalculateJurisdiction(StateEvent const* stateEvent) const
{
	float returnValue = 0.5;


	// If it is a key event that can be handled in the current state,
	// then return 0.5
	mitk::DisplayPositionEvent const *disPosEvent =
		dynamic_cast <const mitk::DisplayPositionEvent *> (stateEvent->GetEvent());

	// Key event handling:
	if (disPosEvent == NULL)
	{
		// Check if the current state has a transition waiting for that key event.
		if (this->GetCurrentState()->GetTransition(stateEvent->GetId())!=NULL)
		{
			return 0.5;
		}
		else
		{
			return 0.0;
		}
	}

	//if the event can be understood and if there is a transition waiting for that event
	if (this->GetCurrentState()->GetTransition(stateEvent->GetId())!=NULL)
	{
		returnValue = 0.5;//it can be understood
	}

	int timeStep = disPosEvent->GetSender()->GetTimeStep();
	return returnValue;
}


bool mitk::SurfDeformationInteractor
::ExecuteAction( Action *action, mitk::StateEvent const *stateEvent )
{
	bool ok = false;

	// Get data object
	mitk::BaseData *data = m_DataTreeNode->GetData();
	if ( data == NULL )
	{
		LOG_ERROR << "No data object present!";
		return ok;
	}

	if((m_OriginalTimeStep!=-1) && (m_TimeStep!=m_OriginalTimeStep))
		return ok;

	// Get mitk::Event and extract renderer
	const mitk::Event *event = stateEvent->GetEvent();
	mitk::BaseRenderer *renderer = NULL;
	vtkRenderWindow *renderWindow = NULL;
	vtkRenderWindowInteractor *renderWindowInteractor = NULL;

	if ( event != NULL )
	{
		renderer = event->GetSender();
		if ( renderer != NULL )
		{
			renderWindow = renderer->GetRenderWindow();
			if ( renderWindow != NULL )
			{
				renderWindowInteractor = renderWindow->GetInteractor();
			}
		}
	}

	// Check if we have a DisplayPositionEvent
	const mitk::DisplayPositionEvent *dpe = 
		dynamic_cast< const mitk::DisplayPositionEvent * >( stateEvent->GetEvent() );     
	if ( dpe != NULL )
	{
		m_PickedSurfaceNode = m_DataTreeNode;
		//if(m_createSphereMode)
		//	m_PickedSurfaceNode = m_DataTreeNode;
		//else
		//	m_PickedSurfaceNode = dpe->GetPickedObjectNode();
		m_CurrentPickedPoint = dpe->GetWorldPosition();
		m_CurrentPickedDisplayPoint = dpe->GetDisplayPosition(); //not used
	}


	// Extract surface
	mitk::Surface *mitkSurface;
	mitkSurface = dynamic_cast< Surface * >( data );
	if ( mitkSurface != NULL )
	{
		m_PolyData = mitkSurface->GetVtkPolyData( m_TimeStep );
	}
	else
	{
		m_PolyData = NULL;
	}

	// Get selected scalar array
	vtkDataArray* selectedArray = blShapeUtils::ShapeUtils::GetArray( m_PolyData, m_SelectedScalarArray );

	//if 2dRendering, select the corresponding plane normal; otherwise 1,1,1
	m_PlaneGeometry = NULL;
	m_is2DView = false;
	if ( renderer!= NULL )
	{
		if(renderer->GetMapperID() == BaseRenderer::Standard2D )
		{
			const mitk::PlaneGeometry* planeGeometry = dynamic_cast<const mitk::PlaneGeometry*>(renderer->GetCurrentWorldGeometry2D() );
			if ( planeGeometry )
			{
				m_PlaneGeometry = (mitk::PlaneGeometry*)( planeGeometry );
				m_ObjectNormal = m_PlaneGeometry->GetNormal( );
				m_ObjectNormal.Normalize( );
				m_is2DView = true;
			}

			if ( planeGeometry != m_PlaneGeometry && m_CallBack )
			{
				m_CallBack->OnParametersModified();
			}

		}
		else
		{
			m_ObjectNormal[0] = 1/sqrt(3.0);
			m_ObjectNormal[1] = 1/sqrt(3.0);
			m_ObjectNormal[2] = 1/sqrt(3.0);
			m_is2DView = false;
		}
	}	//otherwise default value

	//std::cout<<"current normal: "<<m_ObjectNormal[0]<<" "<<m_ObjectNormal[1]<<m_ObjectNormal[2]<<endl;

	// Make sure that the data (if time-resolved) has enough entries;
	// if not, create the required extra ones (empty)
	data->Expand( m_TimeStep+1 );


	bool bShowPoint = false;

	switch (action->GetActionId())
	{
	case AcDONOTHING:
		ok = true;
		break;


	case AcCHECKOBJECT:
		{
			// Check if an object is present at the current mouse position
			m_PickedSurface = NULL;
			m_PickedPolyData = NULL;

			if ( m_PickedSurfaceNode != NULL )
			{
				m_PickedSurface = dynamic_cast< mitk::Surface * >( m_PickedSurfaceNode->GetData() );
				if ( m_PickedSurface != NULL )
				{
					m_PickedPolyData = m_PickedSurface->GetVtkPolyData( m_TimeStep );
				}
			}

			mitk::StateEvent *newStateEvent;
			if ( (m_PickedSurfaceNode == m_DataTreeNode) && (m_PickedSurface != NULL) )
			{
				if(m_createSphereMode)
				{
					// a new sphere will be created
					newStateEvent = new mitk::StateEvent( EIDNEW, dpe );
					m_InitialPickedPoint = m_CurrentPickedPoint;
					bShowPoint = true;
				}
				else
					// Yes: object will be selected
					newStateEvent = new mitk::StateEvent( EIDYES, dpe );

				//// Disable VTK interactor until MITK interaction has been completed
				//if ( renderWindowInteractor != NULL )
				//{
				//  renderWindowInteractor->Disable();
				//}
			}
			else
			{
				//TODO: this could be removed: we don't need a checkable object anymore...the
				//code will enter here if you remove the node..but the widget should have already
				//disabled the interactor
				
				// No: back to start state
				newStateEvent = new mitk::StateEvent( EIDNO, dpe );

				//// Re-enable VTK interactor (may have been disabled previously)
				//if ( renderWindowInteractor != NULL )
				//{
				//	renderWindowInteractor->Enable();
				//}
			}
			this->HandleEvent( newStateEvent );

			ok = true;
			break;
		}

	case AcDESELECTOBJECT:
		{
			if(m_createSphereMode)
				m_createSphereMode = false;

			//clear the undolist
			if(m_CallBack!=NULL)
				m_CallBack->OnUndo(true);

			bShowPoint = false;
			mitk::RenderingManager::GetInstance()->RequestUpdateAll();

			ok = true;
			break;
		}

	case AcSELECTPICKEDOBJECT:
		{
			if((m_PolyData->GetNumberOfPoints()>0) && 
				(!m_PolyData->GetPointData()->HasArray(SELECTIONNAME)))
			{
				//create the selection array but don't select it
				vtkSmartPointer<vtkDoubleArray> selectionScalar = vtkSmartPointer<vtkDoubleArray>::New();
				selectionScalar->SetNumberOfTuples(m_PolyData->GetNumberOfPoints());
				selectionScalar->SetNumberOfComponents(1);
				selectionScalar->SetName(SELECTIONNAME);
				for(int i=0;i<m_PolyData->GetNumberOfPoints();i++)
					selectionScalar->SetComponent( i, 0, 0 );
				m_PolyData->GetPointData()->AddArray(selectionScalar);
				m_PolyData->GetPointData()->SetActiveScalars(selectionScalar->GetName());
				m_PolyData->Modified();
				mitkSurface->Modified();
				m_PolyData->GetPointData()->Update();
			}
			
			UpdateMaxDimension();

			//mitk::MaterialProperty::Pointer materialProperty = mitk::MaterialProperty::New( 1.0, 0.0, 0.0, 1.0, m_DataTreeNode );
			//materialProperty->SetDiffuseColor(1.0, 0.0, 0.0);
			//m_DataTreeNode->ReplaceProperty("material", materialProperty);
			mitk::RenderingManager::GetInstance()->RequestUpdateAll();

			bShowPoint = true;
			ok = true;  
			break;
		}

	case AcINITMOVE:
		{

			m_InitialPickedPoint = m_CurrentPickedPoint;
			bShowPoint = false;
			if(m_createSphereMode)
				//nothing more to do
				break;

			
			UpdateMaxDimension();

			//---------------------------------------------------
			// Get position of the closest point on the surface
			//---------------------------------------------------
			// Found a point -> store it in the m_InitialPickedPointWorld
			double pPoint[ 3 ] = { m_CurrentPickedPoint[ 0 ], m_CurrentPickedPoint[ 1 ], m_CurrentPickedPoint[ 2 ] };
			int iPosition = m_PolyData->FindPoint( pPoint );

			if (iPosition>=0)
			{
				m_InitialPickedPoint[ 0 ] = mitk::PointSet::CoordinateType(m_PolyData->GetPoint(iPosition)[ 0 ]);
				m_InitialPickedPoint[ 1 ] = mitk::PointSet::CoordinateType(m_PolyData->GetPoint(iPosition)[ 1 ]);
				m_InitialPickedPoint[ 2 ] = mitk::PointSet::CoordinateType(m_PolyData->GetPoint(iPosition)[ 2 ]);

			}
			m_SurfaceColorizationCenter = m_InitialPickedPoint;
			

			// Colorize surface / wireframe dependend on distance from picked point
			this->ColorizeSurface( m_PolyData,
				m_SurfaceColorizationCenter, COLORIZATION_GAUSS );
			mitk::RenderingManager::GetInstance()->RequestUpdateAll();

			// Make deep copy of vtkPolyData interacted on
			m_OriginalPolyData->DeepCopy( m_PolyData );
			m_OriginalTimeStep = m_TimeStep;

			ok = true;
			break;
		}

	case AcMOVE:
		{
			if(m_createSphereMode)
			{
				UpdateTheSphere(m_InitialPickedPoint,m_CurrentPickedPoint);
							
			}
			else 
			{
				mitk::Vector3D interactionMove;
				interactionMove[0] = m_CurrentPickedPoint[0] - m_InitialPickedPoint[0];
				interactionMove[1] = m_CurrentPickedPoint[1] - m_InitialPickedPoint[1];
				interactionMove[2] = m_CurrentPickedPoint[2] - m_InitialPickedPoint[2];

				// Calculate mouse move in 3D space

				//// Transform mouse move into geometry space
				data->UpdateOutputInformation(); // make sure that the Geometry is up-to-date

				vtkPoints *originalPoints = m_OriginalPolyData->GetPoints();
				vtkPoints *deformedPoints = m_PolyData->GetPoints();

				// Get selected scalar array
				blShapeUtils::ShapeUtils::VTKScalarType selectionArray;
				selectionArray.mode = blShapeUtils::ShapeUtils::SCALAR_ARRAY_POINT_DATA;
				selectionArray.name = SELECTIONNAME;
				vtkDataArray* selectedArray = blShapeUtils::ShapeUtils::GetArray( m_PolyData, selectionArray );

				// Deform points
				double point[3];
				for ( unsigned int i = 0; i < deformedPoints->GetNumberOfPoints(); ++i )
				{
					// Get original point
					vtkFloatingPointType *originalPoint = originalPoints->GetPoint( i );
					mitk::Vector3D t = interactionMove * selectedArray->GetTuple1( i ); 
					point[0] = originalPoint[0] + t[0];
					point[1] = originalPoint[1] + t[1];
					point[2] = originalPoint[2] + t[2];

					deformedPoints->SetPoint( i, point );
				}

				// Make sure that surface is colorized at initial picked position
				// as long as we are in deformation state
				m_SurfaceColorizationCenter =  m_InitialPickedPoint;

				m_PolyData->Modified();
				mitkSurface->Modified();
			}

			//mitk::RenderingManager::GetInstance()->RequestUpdateAll(
			//  mitk::RenderingManager::REQUEST_UPDATE_3DWINDOWS );
			mitk::RenderingManager::GetInstance()->RequestUpdateAll();
			ok = false;
			bShowPoint = true;
			break;
		}

	case AcMODIFY:
		{
			const mitk::KeyEvent *me = 
				dynamic_cast< const mitk::KeyEvent * >( stateEvent->GetEvent() );     
			if ( me == NULL )
			{
				ok = true;
				break;
			}
			
			switch (me->GetKey())
			{
				case 90: //'Z'
					if(!m_createSphereMode)
						m_GaussSigma -=  GAUSSSTEP;
					else
						m_SphereRes -= GAUSSSTEP;
					break;
				case 65: //'A'
					if(!m_createSphereMode)
						m_GaussSigma +=  GAUSSSTEP;
					else
						m_SphereRes += GAUSSSTEP;
					break;
			}

			m_GaussSigma = max(m_GaussSigma,GAUSSMIN);
			m_GaussSigma = min(m_GaussSigma,GAUSSMAX);

			m_SphereRes = max(m_SphereRes,SPHRESMIN);
			m_SphereRes = min(m_SphereRes,SPHRESMAX);

			// Colorize surface dependent on sigma and distance from picked point
			this->ColorizeSurface( m_PolyData,
				m_SurfaceColorizationCenter, COLORIZATION_GAUSS );

			if(m_CallBack!=NULL)
			{
				m_CallBack->OnParametersModified();
			}


			mitk::RenderingManager::GetInstance()->RequestUpdateAll();
			ok = true;
			bShowPoint = true;
			break;
		}
	case AcACCEPT:
		{
			if(m_CallBack!=NULL)
			{
				m_CallBack->OnRenderingDataModified();
			}
			
			m_SurfaceColorizationCenter = m_CurrentPickedPoint;
			bShowPoint = true;
			break;
		}

	case AcFINISHOBJECT:
		{
			mitk::StateEvent *newStateEvent;
			if(m_createSphereMode)
			{
				m_createSphereMode = false;

				// Yes: object will be selected
				newStateEvent = new mitk::StateEvent( EIDYES );
			}
			else
				// Yes: object will be deselected
				newStateEvent = new mitk::StateEvent( EIDNO );

			if(m_CallBack!=NULL)
			{
				m_CallBack->OnRenderingDataModified();
				m_CallBack->OnParametersModified();
			}

			this->HandleEvent( newStateEvent );

			bShowPoint = false;
			break;
		}

	case AcUNDOUPDATE:
		{
			if(m_CallBack!=NULL)
			{
				m_CallBack->OnUndo(false);
			}
			ok = true;
			break;
		}


	default:
		return Superclass::ExecuteAction( action, stateEvent );
	}


	if(m_pointSetNode.IsNotNull())
	{
		if(bShowPoint)
		{
			mitk::PointSet::Pointer pointData 
				= dynamic_cast<mitk::PointSet*>(m_pointSetNode->GetData());
			if(pointData.IsNotNull())
			{
				if(pointData->GetSize(  )>=1)
					pointData->SetPoint(0, m_CurrentPickedPoint, m_TimeStep);
			}
		}
		m_pointSetNode->SetProperty("visible",mitk::BoolProperty::New(bShowPoint));
	}


	return ok;
}


bool mitk::SurfDeformationInteractor::ColorizeSurface( vtkPolyData *polyData, 
															 const Point3D &pickedPoint, int mode, double scalar )
{
	if ( polyData == NULL )
	{
		return false;
	}

	vtkPoints *points = polyData->GetPoints();
	vtkPointData *pointData = polyData->GetPointData();
	if ( pointData == NULL )
	{
		return false;
	}

	vtkDataArray *scalars = pointData->GetArray(SELECTIONNAME);
	if ( scalars == NULL )
	{
		return false;
	}
	pointData->SetActiveScalars(SELECTIONNAME);

	// Get selected scalar array
	vtkDataArray* selectedArray = blShapeUtils::ShapeUtils::GetArray( polyData, m_SelectedScalarArray );

	if ( mode == COLORIZATION_GAUSS )
	{
		// Get picked point and transform into local coordinates
		//mitk::Point3D pickedPointIndex;
		//m_Geometry->WorldToIndex( pickedPoint, pickedPointIndex );
		
		blShapeUtils::ShapeUtils::VTKScalarType scalarType;
		scalarType.mode = blShapeUtils::ShapeUtils::SCALAR_ARRAY_POINT_DATA;
		scalarType.name = SELECTIONNAME;
		blMITKUtils::ApplyLookupTable( true, m_DataTreeNode, 
			blLookupTables::LUT_TYPE_RAINBOW_HUE_INVERTED, scalarType );
		double range[2] = {0, 1};
		blMITKUtils::SetVtkLutScalarRangeForMesh(m_DataTreeNode,range);

		// Get value of picked point
		double valuePickedPoint;
		if ( selectedArray )
		{
			vtkIdType id = polyData->FindPoint( pickedPoint[ 0 ], pickedPoint[ 1 ], pickedPoint[ 2 ] );
			valuePickedPoint = selectedArray->GetTuple1( id );
		}

		// L is the desired factor at minimum distant plane
		double l = 0.01;
		double globalRadius = sqrt(-2 * m_GaussSigma * m_GaussSigma * log(l)) * 2 * m_maxDim;

		// Picked point
		mitk::Vector3D v1 = pickedPoint.GetVectorFromOrigin();

		for ( unsigned int i = 0; i < points->GetNumberOfPoints(); ++i )
		{
			if ( selectedArray && selectedArray->GetTuple1( i ) != valuePickedPoint )
			{
				scalars->SetTuple1( i, 0 );
				continue;
			}

			// Get original point
			vtkFloatingPointType *point = points->GetPoint( i );
			mitk::Vector3D v0;
			v0[0] = point[0];
			v0[1] = point[1];
			v0[2] = point[2];

			// Get distance vector
			mitk::Vector3D directionVector = v1-v0;
			double d = directionVector.GetNorm();

			// Restrict distant points 
			if (d > 1.1 * globalRadius) 
			{
				scalars->SetTuple1( i, 0 );
				continue;
			} 

			double radiusInCurrentDirection = globalRadius;

			// Iterate over all fixed planes to compute minimum distance in current direction
			std::list<mitk::PlaneGeometry::Pointer>::iterator itPlanes;
			for ( itPlanes = m_FixedPlanesList.begin( ) ; itPlanes != m_FixedPlanesList.end( ) ; itPlanes++ )
			{
				// Skip current plane
				if ( *(*itPlanes) == m_PlaneGeometry.GetPointer( ) )
				{
					continue;
				}

				// Line between picked point and current point
				// Direction must be normalized because there's a bug in the intersection function
				directionVector.Normalize( );
				mitk::Line3D line(pickedPoint, directionVector );

				// Intersection of line to plane
				mitk::Point3D intersectionPoint;
				(*itPlanes)->IntersectionPoint( line, intersectionPoint );

				// Check direction of current point
				mitk::Vector3D vectorToIntersectionWithPlane = intersectionPoint.GetVectorFromOrigin() - v1;
				double rayVsIntersectionDirection = (-directionVector) * vectorToIntersectionWithPlane;
				if (rayVsIntersectionDirection >= 0) 
				{
					double currentRadius = vectorToIntersectionWithPlane.GetNorm();
					radiusInCurrentDirection = min(currentRadius, radiusInCurrentDirection);
				}
			}

			// Compute deformation
			double relativeRadius = radiusInCurrentDirection / globalRadius;
			double denom = m_GaussSigma * m_GaussSigma * 2 * relativeRadius;
			d /= (2*m_maxDim);
			double t =  exp( - (d * d / denom )); 

			scalars->SetTuple1( i, t );
		}
	}
	else if ( mode == COLORIZATION_CONSTANT )
	{
		for ( unsigned int i = 0; i < pointData->GetNumberOfTuples(); ++i )
		{
			scalars->SetComponent( i, 0, scalar );
		}
	}
	//std::cout<<"current normal2: "<<m_ObjectNormal[0]<<" "<<m_ObjectNormal[1]<<m_ObjectNormal[2]<<endl;


	polyData->Modified();
	pointData->Update();
	return true;
}



void mitk::SurfDeformationInteractor::UpdateTheSphere( mitk::Point3D center, 
															   mitk::Point3D spherePoint)
{

	if(m_PolyData==NULL)
		return;

	double radius = 0;
	for(int i=0; i<3; i++)
		radius += pow(center[i]-spherePoint[i],2);
	radius = sqrt(radius);

	vtkSmartPointer<vtkSphereSource> source = vtkSphereSource::New();
	source->SetCenter(center[0],center[1],center[2]);
	source->SetRadius(radius);
	source->SetThetaResolution(m_SphereRes);
	source->SetPhiResolution(m_SphereRes);
	source->Update();
	m_PolyData->DeepCopy(source->GetOutput());
	m_PolyData->Modified();

}

void mitk::SurfDeformationInteractor::UpdateMaxDimension()
{
	double bound[6];
	m_PolyData->GetBounds(bound);
	m_maxDim = 1;
	for (int i=0;i<3;i++)
	{
		double currDim = (bound[2*i+1]-bound[2*i])/2;
		(m_maxDim >= currDim)? m_maxDim : m_maxDim = currDim;
	}
}

void mitk::SurfDeformationInteractor::SetGaussSigma( double val ) 
{ 
	m_GaussSigma = val; 
	m_GaussSigma = max( m_GaussSigma, GAUSSMIN );

	// Colorize surface dependent on sigma and distance from picked point
	this->ColorizeSurface( m_PolyData,
		m_SurfaceColorizationCenter, COLORIZATION_GAUSS );

	mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}

double mitk::SurfDeformationInteractor::GetGaussSigma() const 
{ 
	return m_GaussSigma; 
}

void mitk::SurfDeformationInteractor::SetSelectedScalarArray( 
	const blShapeUtils::ShapeUtils::VTKScalarType &selectedScalarArray )
{
	m_SelectedScalarArray = selectedScalarArray;
}

void mitk::SurfDeformationInteractor::SetCreateSphereMode(bool val)
{
	m_createSphereMode = val;
}

bool mitk::SurfDeformationInteractor::GetCreateSphereMode()
{ 
	return m_createSphereMode;
}

void mitk::SurfDeformationInteractor::SetCallback(SurfDeformationCallback *callback)
{
	m_CallBack=callback;
}

vtkPolyData * mitk::SurfDeformationInteractor::GetPreviousPolyData() const 
{ 
	return m_OriginalPolyData; 
}

double mitk::SurfDeformationInteractor::GetSphereRes() const 
{ 
	return m_SphereRes; 
}

void mitk::SurfDeformationInteractor::FixPlane( const mitk::PlaneGeometry* planeGeometry, bool fix )
{
	// Get plane information
	if ( planeGeometry == NULL || fix == IsPlaneFixed( planeGeometry ) )
	{
		return;
	}

	if ( fix )
	{
		mitk::PlaneGeometry::Pointer plane =  dynamic_cast<mitk::PlaneGeometry*> ( planeGeometry->Clone().GetPointer( ) );
		m_FixedPlanesList.push_back( plane );
	}
	else
	{
		std::list<mitk::PlaneGeometry::Pointer>::iterator itPlanes;
		for ( itPlanes = m_FixedPlanesList.begin( ) ; itPlanes != m_FixedPlanesList.end( ) ; itPlanes++ )
		{
			// Skip current plane
			if ( *(*itPlanes) == planeGeometry )
			{
				break;
			}
		}

		if ( itPlanes != m_FixedPlanesList.end( ) )
		{
			m_FixedPlanesList.erase( itPlanes );
		}
	}
}

bool mitk::SurfDeformationInteractor::IsPlaneFixed( const mitk::PlaneGeometry* planeGeometry )
{
	// Get plane information
	if ( planeGeometry == NULL )
	{
		return false;
	}

	std::list<mitk::PlaneGeometry::Pointer>::iterator itPlanes;
	for ( itPlanes = m_FixedPlanesList.begin( ) ; itPlanes != m_FixedPlanesList.end( ) ; itPlanes++ )
	{
		if ( *(*itPlanes) == planeGeometry )
		{
			return true;
		}
	}

	return false;
}

void mitk::SurfDeformationInteractor::UnFixAllPlanes( )
{
	m_FixedPlanesList.clear( );
}
