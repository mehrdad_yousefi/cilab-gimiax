/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "gspPhilipsSegmentedScarMeshPanelWidget.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"
// Core
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"
#include "coreDataTreeHelper.h"
#include "coreReportExceptionMacros.h"

namespace gsp
{

const wxWindowID PhilipsSegmentedScarMeshPanelWidget::ms_WidgetID = wxNewId();
const std::string PhilipsSegmentedScarMeshPanelWidget::ms_WidgetCaption = "PhilipsSegmentedScarMesh Panel Widget";

PhilipsSegmentedScarMeshPanelWidget::PhilipsSegmentedScarMeshPanelWidget(  wxWindow* parent, int id/*= wxID_ANY*/,
													   const wxPoint&  pos /*= wxDefaultPosition*/, 
													   const wxSize&  size /*= wxDefaultSize*/, 
													   long style/* = 0*/ )
: gspPhilipsSegmentedScarMeshPanelWidgetUI(parent, id,pos,size,style)
{
	m_Processor = gsp::PhilipsSegmentedScarMeshProcessor::New();

	SetName( ms_WidgetCaption );
}

PhilipsSegmentedScarMeshPanelWidget::~PhilipsSegmentedScarMeshPanelWidget( )
{
	// We don't need to destroy anything because all the child windows 
	// of this wxWindow are destroyed automatically
}

void PhilipsSegmentedScarMeshPanelWidget::OnInit( )
{
	//------------------------------------------------------
	// Observers to data
	m_Processor->GetOutputDataEntityHolder( 0 )->AddObserver( 
		this, 
		&PhilipsSegmentedScarMeshPanelWidget::OnModifiedOutputDataEntity );

/*
	m_Processor->GetInputDataEntityHolder( PhilipsSegmentedScarMeshProcessor::INPUT_0 )->AddObserver( 
		this, 
		&PhilipsSegmentedScarMeshPanelWidget::OnModifiedInputDataEntity );
*/

	UpdateWidget();
}

void PhilipsSegmentedScarMeshPanelWidget::UpdateWidget()
{
	
	UpdateHelperWidget( );

}

void PhilipsSegmentedScarMeshPanelWidget::UpdateData()
{
	// Set parameters to processor. Pending
}

void PhilipsSegmentedScarMeshPanelWidget::OnMapScarPressed( wxCommandEvent& event )
{
	// Catch the exception from the processor and show the message box
	try
	{
		// Update the scale values from widget to processor
		UpdateData();

		PhilipsSegmentedScarMeshProcessor::PSSMPParameters& parameters = m_Processor->GetParameters();
		//parameters.m_ProcessorMethod = PhilipsSegmentedScarMeshProcessor::PM_EXTRACTSCAR;
		parameters.m_ProcessorMethod = PhilipsSegmentedScarMeshProcessor::PM_MAPSCAR;
		
		UpdateProcessor( true );
	}
	coreCatchExceptionsReportAndNoThrowMacro( "PhilipsSegmentedScarMeshPanelWidget::OnMapScarPressed" );
}


void PhilipsSegmentedScarMeshPanelWidget::OnModifiedOutputDataEntity()
{
	try
	{

	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"CardiacInitializationPanelWidget::OnModifiedOutputDataEntity")

}

void PhilipsSegmentedScarMeshPanelWidget::UpdateHelperWidget()
{
	if ( GetHelperWidget( ) == NULL )
	{
		return;
	}
	GetHelperWidget( )->SetInfo( 
		Core::Widgets::HELPER_INFO_LEFT_BUTTON, 
		" info that is useful in order to use the processor" );

}

bool PhilipsSegmentedScarMeshPanelWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = gspPhilipsSegmentedScarMeshPanelWidgetUI::Enable( enable );

	// If this panel widget is selected -> Update the widget
	if( enable )
	{
		UpdateWidget();
	}

	return bReturn;
}

void PhilipsSegmentedScarMeshPanelWidget::OnModifiedInputDataEntity()
{
	UpdateWidget();
}

Core::BaseProcessor::Pointer PhilipsSegmentedScarMeshPanelWidget::GetProcessor()
{
	return m_Processor.GetPointer();
}

} // namespace gsp
