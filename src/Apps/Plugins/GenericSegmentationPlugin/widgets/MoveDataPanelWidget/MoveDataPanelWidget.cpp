/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "MoveDataPanelWidget.h"

// Core
#include "coreVTKPolyDataHolder.h"
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"
#include "coreRenderingTree.h"
#include "coreDataEntityListBrowser.h"
#include "coreReportExceptionMacros.h"
#include "corePluginTab.h"
#include "coreDataEntity.h"
#include "coreDataEntityList.h"
#include "coreDataEntityList.txx"
#include "coreProcessorInputWidget.h"

// Core
#include "coreDataTreeHelper.h"
#include "vtkImageToImageStencil.h"
#include "wxMitkSelectableGLWidget.h"
#include "coreMultiRenderWindowMITK.h"
#include "coreImageDataEntityMacros.h"
#include "coreVTKUnstructuredGridHolder.h"

#include "vtkImageClip.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkTransform.h"
#include "vtkTransformFilter.h"

using namespace Core::Widgets;

// Event the widget
BEGIN_EVENT_TABLE(MoveDataPanelWidget,MoveDataPanelWidgetUI)
END_EVENT_TABLE()


MoveDataPanelWidget::MoveDataPanelWidget(
						 wxWindow* parent, 
						 int id ,
						 const wxPoint& pos, 
						 const wxSize& size, 
						 long style) :
	MoveDataPanelWidgetUI(parent, id, pos, size, style)
{
	m_processor = MoveDataProcessor::New();

	UpdateWidget( );
}

MoveDataPanelWidget::~MoveDataPanelWidget( ) 
{
}

void MoveDataPanelWidget::OnInit()
{
}

Core::BaseProcessor::Pointer MoveDataPanelWidget::GetProcessor()
{
	return Core::BaseProcessor::Pointer(m_processor);
}

void MoveDataPanelWidget::StartManualMoveInteraction()
{	
	if(m_btnMoveManually->GetValue() == false)
		return;

	if(m_processor->GetInputDataEntity(
		MoveDataProcessor::INPUT_DATA_TO_MOVE).IsNull())
		return;

	if ( !GetMultiRenderWindow() )
	{
		return ;
	}

	GetMultiRenderWindow()->GetMetadata()->AddTag( "AxisLocked", true );
	GetMultiRenderWindow()->GetMetadataHolder()->NotifyObservers();

	// Create interactor
	if(m_interactor.IsNull())
	{
		m_interactor = ms::MoveImageAndMeshInteractorHelper::New( 
			GetRenderingTree(),
			m_processor->GetInputDataEntityHolder(MoveDataProcessor::INPUT_DATA_TO_MOVE));
	}

	try
	{
		m_interactor->ConnectToDataTreeNode();
		m_interactor->SetCallback(this);

		//m_InteractorStateHolder->SetSubject( INTERACTOR_ENABLED );


	}coreCatchExceptionsReportAndNoThrowMacro( MoveDataPanelWidget::StartManualMoveInteraction );

	UpdateWidget( );
}

//!
void MoveDataPanelWidget::StopManualMoveInteraction()
{
	GetMultiRenderWindow()->GetMetadata()->AddTag( "AxisLocked", false );
	GetMultiRenderWindow()->GetMetadataHolder()->NotifyObservers();

	if ( m_interactor.IsNull() )
	{
		return;

	}
	

	m_interactor->DisconnectFromDataTreeNode();

	m_interactor = NULL;

	//m_InteractorStateHolder->SetSubject( INTERACTOR_DISABLED );
}

void MoveDataPanelWidget::OnBtnMoveManually( wxCommandEvent& event )
{
	EnableManualMoveInteraction( m_btnMoveManually->GetValue() == true );
}


void MoveDataPanelWidget::EnableManualMoveInteraction( bool bEnable )
{
	if(bEnable)
		StartManualMoveInteraction();
	else
		StopManualMoveInteraction();
	UpdateWidget();
}
void MoveDataPanelWidget::UpdateWidget()
{
	if (m_btnMoveManually->GetValue())
		m_btnMoveManually->SetLabel("Stop Moving");
	else
		m_btnMoveManually->SetLabel("Move Manually");
}

void MoveDataPanelWidget::OnModifiedRenderingData(double shiftDist[3])
{
	Core::DataEntity::Pointer movedData = 
            m_processor->GetInputDataEntity(MoveDataProcessor::INPUT_DATA_TO_MOVE);
	if( movedData.IsNull())
	{
		return;
	}

	// Get rendering data
	mitk::DataTreeNode::Pointer node;
	Core::CastAnyProcessingData(GetRenderingTree()->GetNode(movedData),node);

	// Get current Rendering data origin
	mitk::Geometry3D* geometry = node->GetData()->GetUpdatedTimeSlicedGeometry()->GetGeometry3D( GetTimeStep( ) );
	mitk::Point3D origin = geometry->GetOrigin();

	// Update the processing data with the new origin information from rendering data
	for (int i = 0; i<movedData->GetNumberOfTimeSteps(); i++)
	{
		// For each processing data type:
		switch(movedData->GetType())
		{
			case Core::ImageTypeId:
			case Core::ImageTypeId | Core::ROITypeId:
			case Core::ROITypeId:
			{
				Core::vtkImageDataPtr vtkImage;
				movedData->GetProcessingData( vtkImage, i, true );
				if ( vtkImage != NULL )
				{
						double originAsDouble[ 3 ] = { origin[ 0 ], origin[ 1 ], origin[ 2 ] };
					vtkImage->SetOrigin(originAsDouble);
					vtkImage->Modified();
				}
				break;
			}
			case Core::SurfaceMeshTypeId:
			case Core::VolumeMeshTypeId:
			{
				vtkSmartPointer<vtkDataSet> dataSet;
				if ( movedData->GetType( ) == Core::SurfaceMeshTypeId )
				{
					Core::vtkPolyDataPtr porssessingMesh;
					movedData->GetProcessingData( porssessingMesh, i, true );
					dataSet = porssessingMesh;
				}
				else if ( movedData->GetType( ) == Core::VolumeMeshTypeId )
				{
					Core::vtkUnstructuredGridPtr volumeMesh;
					movedData->GetProcessingData( volumeMesh, i, true );
					dataSet = volumeMesh;
				}

				// Transform surface mesh and restore origin of rendering geometry to 0
				if( ( dataSet != NULL ) )
				{
					double originAsFloat[ 3 ] = { origin[ 0 ], origin[ 1 ], origin[ 2 ] };
					vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New( );
					transform->Translate( originAsFloat );

					vtkSmartPointer<vtkTransformFilter> transformFilter;
					transformFilter = vtkSmartPointer<vtkTransformFilter>::New( );
					transformFilter->SetInput( dataSet );
					transformFilter->SetTransform( transform );
					transformFilter->Update( );

					dataSet->DeepCopy( transformFilter->GetOutput( ) );
				}
				break;
			}
		}
	}

	origin.Fill( 0 );
	geometry->SetOrigin( origin );

	movedData->Modified( );
}

