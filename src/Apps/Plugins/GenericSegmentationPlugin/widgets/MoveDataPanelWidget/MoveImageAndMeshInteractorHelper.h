/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef msMoveImageAndMeshInteractorHelper_H
#define msMoveImageAndMeshInteractorHelper_H


#include "coreRenderingTree.h"
#include "coreObject.h"
#include "coreDataEntityHolder.h"
#include "coreCommonDataTypes.h"

// MITK
#include "mitkDataTree.h"
#include "mitkImage.h"

//the real mitk style interactor
#include "msMitkMoveImageAndMeshInteractor.h"

//Core
#include "coreDataEntity.h"
#include "coreDataHolder.h"



namespace ms{

/**
\brief 
\ingroup GenericSegmentationPlugin
\helper object to mitk::MoveImageAndMeshInteractor 
\(that is a classical mitk interactor)
\This is a standard Gimias helper to an mitk type Interactor
\In this case msMitkDeformationSurfaceInteractor class
\author Luigi Carotenuto
\date 21-08-11
*/
class MoveImageAndMeshInteractorHelper 
	: public Core::SmartPointerObject
{

// PUBLIC OPERATIONS
public:

	//!
	coreDeclareSmartPointerClassMacro2Param(
		MoveImageAndMeshInteractorHelper,
		Core::SmartPointerObject,
		Core::RenderingTree::Pointer,
		Core::DataEntityHolder::Pointer );

	//! Connect the interactor to the selectedData tree node
	void ConnectToDataTreeNode();

	//! Disconnect the interactor from the rendering tree
	void DisconnectFromDataTreeNode();

	//!
	Core::RenderingTree::Pointer GetRenderingTree() const;

	//!
	void SetRenderingTree(Core::RenderingTree::Pointer val);

	//!
	Core::DataEntityHolder::Pointer GetSelectedDataHolder() const;

	//!
	void SetSelectedDataHolder(Core::DataEntityHolder::Pointer val);

	void SetCallback(mitk::MoveImageAndMeshInteractorCallaback *callback){
		if(m_interactor.IsNotNull())
			m_interactor->SetCallback(callback);};

// PRIVATE OPERATIONS
private:
	//! 
	MoveImageAndMeshInteractorHelper(
		Core::RenderingTree::Pointer renderingTree,
		Core::DataEntityHolder::Pointer selectedImage );

	virtual ~MoveImageAndMeshInteractorHelper();


	//! Connect to the m_renderingTree
	void ConnectNodeToTree();

	//! Disconnect to the m_renderingTree
	void DisconnectNodeFromTree();

	//! Connect the interactors to the global instance
	void ConnectInteractors();

	//! Disconnect all interactors to the global instance
	void DisconnectInteractors();

	//! Redefined
	void CreateInteractor();

	//! Redefined
	void DestroyInteractor();

	//!
	Core::DataEntity::Pointer GetImageDataEntity();

	//!
	mitk::DataTreeNode::Pointer GetSelectedDataNode();


	virtual mitk::Interactor* GetInternalInteractor( ) {return m_interactor;};

// ATTRIBUTES
private:
	MoveImageAndMeshInteractorHelper();
	
	//! Rendering data tree.
	Core::RenderingTree::Pointer m_renderingTree;
	
	//! Current Selected image (if any)
	Core::DataEntityHolder::Pointer m_selectedDataHolder;

	//!
	mitk::MoveImageAndMeshInteractor::Pointer m_interactor;
	
};

} // ms

#endif //msMoveImageAndMeshInteractorHelper
