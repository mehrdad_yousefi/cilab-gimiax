/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _MoveDataPanelWidget_H
#define _MoveDataPanelWidget_H

#include "MoveDataPanelWidgetUI.h"
#include "MoveDataProcessor.h"

// CoreLib
#include "coreRenderingTree.h"
#include "coreFrontEndPlugin.h"
#include "coreCommonDataTypes.h"
#include "coreProcessingWidget.h"

#include "MoveImageAndMeshInteractorHelper.h"

#include <vector>
#include <map>

#include "blMITKUtils.h"
#include <itkPasteImageFilter.h>
#include <itkCropImageFilter.h>
#include <itkVTKImageToImageFilter.h>
#include <itkImageToVTKImageFilter.h>

namespace Core{ namespace Widgets {
	class AcquireDataEntityInputControl;
	class UserHelper;
	class DataEntityListBrowser;
}}

namespace Core{
	namespace Widgets {


#define wxID_MultiLevelROIWidget wxID("wxID_MultiLevelROIWidget")
struct OriginChanger
{
	void SetParameters(double newOrigin[ 3 ], 
		Core::DataEntity::Pointer data, 
		int timeStep,
		bool bSwitch)
	{
		for(int i=0;i<3;i++)
			origin[ i ] = newOrigin[ i ];
		imageDE = data;
		bDoSwitch = bSwitch;
		currentTimestep = timeStep;
	}

	template< class ItkImageType > 
	void ChangeOrigin(typename ItkImageType::Pointer itkImage)
	{
		return;
		if(imageDE.IsNull())
			return;

		//now try to get the image again with at the correct timestep
		//and with the switch facility
		imageDE->GetProcessingData(itkImage,currentTimestep,bDoSwitch);
		if(itkImage.IsNull())
			return;
		//image->SetOrigin(origin);
		
		typedef itk::PasteImageFilter <ItkImageType, ItkImageType > PasteImageFilterType;
		typename PasteImageFilterType::Pointer pasteFilter = PasteImageFilterType::New();

		typename ItkImageType::PointType itkNewOrigin;
		itkNewOrigin[0] = origin[0];
		itkNewOrigin[1] = origin[1];
		itkNewOrigin[2] = origin[2];

		itkImage->SetRegions(itkImage->GetLargestPossibleRegion());

		typename ItkImageType::PointType currOrigin = itkImage->GetOrigin();
		typename ItkImageType::SizeType destinationSize;
		destinationSize[0] = itkImage->GetLargestPossibleRegion().GetSize()[0]-
			std::floor(std::abs(itkNewOrigin[0]-currOrigin[0])/itkImage->GetSpacing()[0]);

		destinationSize[1] = itkImage->GetLargestPossibleRegion().GetSize()[1]-
			std::floor(std::abs(itkNewOrigin[1]-currOrigin[1])/itkImage->GetSpacing()[1]);

		destinationSize[2] = itkImage->GetLargestPossibleRegion().GetSize()[2]-
			std::floor(std::abs(itkNewOrigin[2]-currOrigin[2])/itkImage->GetSpacing()[2]);	
		
		if((destinationSize[0]<= 0) ||
		   (destinationSize[1]<=0) ||
		   (destinationSize[2]<= 0))
				return; //error...you have moved outside the possible area

		typename ItkImageType::PointType destinationOrigin;
		destinationOrigin[0] = std::max(currOrigin[0], itkNewOrigin[0]);
		destinationOrigin[1] = std::max(currOrigin[1], itkNewOrigin[1]);
		destinationOrigin[2] = std::max(currOrigin[2], itkNewOrigin[2]);

		typename ItkImageType::IndexType destinationIndex;
		itkImage->TransformPhysicalPointToIndex(destinationOrigin,destinationIndex);
		
		typename ItkImageType::IndexType cpFromIndex;
		cpFromIndex.Fill(0);

		if(destinationOrigin[0]==currOrigin[0])
			cpFromIndex[0] =  itkImage->GetLargestPossibleRegion().GetSize()[0] - destinationSize[0];
		
		if(destinationOrigin[1]==currOrigin[1])
			cpFromIndex[1] =  itkImage->GetLargestPossibleRegion().GetSize()[1] - destinationSize[1];
		
		if(destinationOrigin[2]==currOrigin[2])
			cpFromIndex[2] =  itkImage->GetLargestPossibleRegion().GetSize()[2] - destinationSize[2];

		typename ItkImageType::Pointer itkDestImage = ItkImageType::New();
		
		itkDestImage->SetRegions(itkImage->GetLargestPossibleRegion());
		itkDestImage->Allocate();
		itkDestImage->SetSpacing(itkImage->GetSpacing());
		itkDestImage->SetOrigin(currOrigin);
			
		typename ItkImageType::RegionType sourceRegion;
		sourceRegion.SetIndex(cpFromIndex);
		sourceRegion.SetSize(destinationSize);


		pasteFilter->SetSourceImage(itkImage);
		pasteFilter->SetDestinationImage(itkDestImage);
		pasteFilter->SetSourceRegion(sourceRegion);
		pasteFilter->SetDestinationIndex(destinationIndex);
		pasteFilter->Update();

		//itkImage = cropFilter->GetOutput();
		imageDE->SetTimeStep(pasteFilter->GetOutput() ,currentTimestep);

		imageDE->Modified();

	}


private :
	double origin[ 3 ];
	Core::DataEntity::Pointer imageDE;
	bool bDoSwitch;
	int currentTimestep;

};

/**
Move data object

\ingroup GenericSegmentationPlugin
\author Xavi Planes
\date Aug 2012
*/
class MoveDataPanelWidget : 
	public MoveDataPanelWidgetUI,
	public Core::Widgets::ProcessingWidget,
	public mitk::MoveImageAndMeshInteractorCallaback
{

	// OPERATIONS
public:
	coreDefineBaseWindowFactory( Core::Widgets::MoveDataPanelWidget )
	//!
	MoveDataPanelWidget(
		wxWindow* parent, 
		int id = wxID_MultiLevelROIWidget,
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=0);

	//!
	~MoveDataPanelWidget( );

	//!
	Core::BaseProcessor::Pointer GetProcessor();

	//!
	void OnInit();

	//!
	void StartManualMoveInteraction( );

	//!
	void StopManualMoveInteraction( );

	
private:
    wxDECLARE_EVENT_TABLE();

	//!
	void EnableManualMoveInteraction( bool bEnable );

	//!
	void OnBtnMoveManually(wxCommandEvent& event);

	//!
	void UpdateWidget();

	//!
	void OnModifiedRenderingData(double shiftDist[3]);

	// ATTRIBUTES
private:

	MoveDataProcessor::Pointer m_processor;

	//! Move Image Interactor Helper
	ms::MoveImageAndMeshInteractorHelper::Pointer m_interactor;

};
} //namespace Widgets
} //namespace Core

#endif //_MoveDataPanelWidget_H
