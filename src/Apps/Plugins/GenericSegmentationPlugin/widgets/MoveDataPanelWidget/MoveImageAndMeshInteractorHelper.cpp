/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// CoreLib
#include "MoveImageAndMeshInteractorHelper.h"
#include "coreAssert.h"
#include "coreException.h"
#include "coreEnvironment.h"
#include "coreLogger.h"
#include "coreKernel.h"
#include "coreReportExceptionMacros.h"
#include "coreDataTreeHelper.h"
#include "coreDataEntityHelper.h"
#include "coreVTKPolyDataHolder.h"
#include "coreVTKImageDataRenDataBuilder.h"
#include "coreRenderingTreeMITK.h"

// Mitk
#include "mitkRenderingManager.h"
#include "mitkSmartPointerProperty.h"
#include "mitkGlobalInteraction.h"
#include "mitkInteractionConst.h"
#include "mitkDataTreeNode.h"
#include "mitkDataTreeHelper.h"
#include "mitkVtkResliceInterpolationProperty.h"
#include "mitkDataStorage.h"
#include "mitkProperties.h"

#include "vtkSmartPointer.h"
#include "vtkImageMathematics.h"
#include "vtkPointData.h"
#include "blMITKUtils.h"

/**
 */
ms::MoveImageAndMeshInteractorHelper::MoveImageAndMeshInteractorHelper(
						Core::RenderingTree::Pointer renderingTree,
						Core::DataEntityHolder::Pointer selectedImage
						)
{
	try{
		SetSelectedDataHolder(selectedImage);
		SetRenderingTree( renderingTree );
	}
		coreCatchExceptionsAddTraceAndThrowMacro(
			"MoveImageAndMeshInteractorHelper::MoveImageAndMeshInteractorHelper");

}

ms::MoveImageAndMeshInteractorHelper::~MoveImageAndMeshInteractorHelper()
{
	DisconnectFromDataTreeNode();
}

/**
 */
ms::MoveImageAndMeshInteractorHelper::MoveImageAndMeshInteractorHelper()
{
	DisconnectFromDataTreeNode();
	DestroyInteractor();
}

void ms::MoveImageAndMeshInteractorHelper::ConnectToDataTreeNode( )
{
	ConnectNodeToTree();

	CreateInteractor();

	ConnectInteractors();
}

void ms::MoveImageAndMeshInteractorHelper::DisconnectFromDataTreeNode()
{
	try
	{
		DisconnectInteractors();

		DestroyInteractor();
	}
	coreCatchExceptionsReportAndNoThrowMacro(
		"MoveImageAndMeshInteractorHelper::DisconnectFromDataTreeNode()");
}

void ms::MoveImageAndMeshInteractorHelper::ConnectNodeToTree()
{
	/*
	if ((GetImageDataEntity()->GetType() != Core::ImageTypeId) &&
		((GetImageDataEntity()->GetType() != Core::ROITypeId)))
	{
		throw Core::Exceptions::Exception( 
			"MoveImageAndMeshInteractorHelper", 
			"Input Data should be an image or a ROI" );
	}
	*/
}

void ms::MoveImageAndMeshInteractorHelper::DisconnectNodeFromTree()
{

}

void ms::MoveImageAndMeshInteractorHelper::ConnectInteractors()
{
	// MITK checks if the interactor is in the list. It will not be added twice
	if( GetInternalInteractor() )
		mitk::GlobalInteraction::GetInstance()->AddInteractor( GetInternalInteractor() );
}

void ms::MoveImageAndMeshInteractorHelper::DisconnectInteractors()
{
	if( GetInternalInteractor() )
	{
		mitk::GlobalInteraction::GetInstance()->RemoveInteractor( GetInternalInteractor() );
	}
}

void ms::MoveImageAndMeshInteractorHelper::CreateInteractor()
{
	if ( m_interactor.IsNull() )
	{
		mitk::DataTreeNode::Pointer node = GetSelectedDataNode();
		//node->ReplaceProperty( "pickable", mitk::BoolProperty::New(true) );
		m_interactor = mitk::MoveImageAndMeshInteractor::New(
			"MoveImageAndMesh", node );
	}
}


void ms::MoveImageAndMeshInteractorHelper::DestroyInteractor()
{
	m_interactor = NULL;
}



Core::DataEntity::Pointer ms::MoveImageAndMeshInteractorHelper::GetImageDataEntity()
{
	return m_selectedDataHolder->GetSubject();
}


mitk::DataTreeNode::Pointer ms::MoveImageAndMeshInteractorHelper::GetSelectedDataNode()
{
	mitk::DataTreeNode::Pointer node = NULL;
	boost::any anyData = GetRenderingTree()->GetNode( GetImageDataEntity( ) );
	Core::CastAnyProcessingData( anyData, node );
	return node;
}

Core::RenderingTree::Pointer ms::MoveImageAndMeshInteractorHelper::GetRenderingTree() const
{
	return m_renderingTree;
}

void ms::MoveImageAndMeshInteractorHelper::SetRenderingTree( Core::RenderingTree::Pointer val )
{
	m_renderingTree = val;
}

Core::DataEntityHolder::Pointer ms::MoveImageAndMeshInteractorHelper::GetSelectedDataHolder() const
{
	return m_selectedDataHolder;
}

void ms::MoveImageAndMeshInteractorHelper::SetSelectedDataHolder( Core::DataEntityHolder::Pointer val )
{
	m_selectedDataHolder = val;
}
