// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "RegionGrowWidget.h"
#include "gblValidate.h"

#include "coreAssert.h"
#include "coreDataTreeHelper.h"
#include "coreDataTreeMITKHelper.h"
#include "coreProcessorInputWidget.h"

#include "gblWxButtonEventProxy.h"
#include "gblWxChoiceValueProxy.h"
#include <limits>
#include "gblWxBridgeLib.h"
#include "gblWxValidate.h"

using namespace gsp;


RegionGrowWidget::RegionGrowWidget( wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style )
: RegionGrowWidgetUI(parent, id, pos, size, style)
{
	m_Processor = gsp::RegionGrowProcessor::New( );

	m_Processor->SetReplaceValue(1.0);
	m_Processor->SetLowerThreshold(200.0);
	m_Processor->SetUpperThreshold(300.0);

	this->m_ChangeInWidgetObserver.SetSlotFunction(this, &RegionGrowWidget::UpdateData);
	//this->m_ChangeInWidgetObserver.Observe(this->ledX);
	//this->m_ChangeInWidgetObserver.Observe(this->ledY);
	//this->m_ChangeInWidgetObserver.Observe(this->ledZ);
	this->m_ChangeInWidgetObserver.Observe(this->ledLowerThreshold);
	this->m_ChangeInWidgetObserver.Observe(this->ledUpperThreshold);

	this->ledLowerThreshold->SetValidator( wxTextValidator ( wxFILTER_NUMERIC ) );
	this->ledUpperThreshold->SetValidator( wxTextValidator ( wxFILTER_NUMERIC ) );
	//this->ledX->SetValidator( wxTextValidator ( wxFILTER_NUMERIC ) );
	//this->ledY->SetValidator( wxTextValidator ( wxFILTER_NUMERIC ) );
	//this->ledZ->SetValidator( wxTextValidator ( wxFILTER_NUMERIC ) );

	dataEntityInputImage = NULL;
}

/**
*/
void RegionGrowWidget::OnInit( )
{
	GetInputWidget( RegionGrowProcessor::INPUT_IMAGE )->Hide( );
	GetInputWidget( RegionGrowProcessor::INPUT_SEED_POINT )->Hide( );

	// Observers to data
	m_Processor->GetInputDataEntityHolder( RegionGrowProcessor::INPUT_SEED_POINT )->AddObserver( 
		this, 
		&RegionGrowWidget::OnModifiedSelectedPoint );

	m_Processor->GetInputDataEntityHolder( RegionGrowProcessor::INPUT_IMAGE )->AddObserver( 
		this, 
		&RegionGrowWidget::OnModifiedInputDataEntity );

	m_Processor->GetOutputDataEntityHolder( 0 )->AddObserver( 
		this, 
		&RegionGrowWidget::OnModifiedOutputDataEntity );

	this->UpdateWidget();

}

void RegionGrowWidget::UpdateWidget()
{
	// For the moment, show maximum number of decimals, for otherwise, the decimal truncation may introduce
	// differences between GIMIAS and the GAR command line executable
	const int nrDecimals = std::numeric_limits<double>::digits10 + 2;
	this->m_ChangeInWidgetObserver.SetEnabled(false);

	itk::Point< double, 3 > seedPoint;
	Core::vtkPolyDataPtr vtkPointsData;

	if ( m_Processor->GetInputDataEntity( RegionGrowProcessor::INPUT_SEED_POINT).IsNotNull())
	{
		Core::DataEntityHelper::GetProcessingData(
			m_Processor->GetInputDataEntityHolder( RegionGrowProcessor::INPUT_SEED_POINT),
			vtkPointsData);
	}
	else
	{
		vtkPointsData = vtkPolyData::New();
		vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
		points->InsertPoint(0,0,0,0);
		vtkPointsData->SetPoints(points);
	}
		

	wxString pointsCoords = "";
	for(int i=0; i<vtkPointsData->GetNumberOfPoints();i++)
	{
		seedPoint[0] = vtkPointsData->GetPoint(i)[0];
		seedPoint[1] = vtkPointsData->GetPoint(i)[1];
		seedPoint[2] = vtkPointsData->GetPoint(i)[2];
		pointsCoords.Append(wxString::Format("%.2f %.2f %.2f\n",
			seedPoint[0], seedPoint[1], seedPoint[2]));
	}
	m_text_seeds->SetValue(pointsCoords);

	//gbl::SetNumber(this->ledX, seedPoint[0], nrDecimals);
	//gbl::SetNumber(this->ledY, seedPoint[1], nrDecimals);
	//gbl::SetNumber(this->ledZ, seedPoint[2], nrDecimals);

	gbl::SetNumber(this->ledLowerThreshold, m_Processor->GetLowerThreshold(), nrDecimals);
	gbl::SetNumber(this->ledUpperThreshold, m_Processor->GetUpperThreshold(), nrDecimals);

	this->m_ChangeInWidgetObserver.SetEnabled(true);
}

bool RegionGrowWidget::Validate()
{
	return true;
}

void RegionGrowWidget::UpdateData()
{
	//if (m_Processor->GetInputDataEntity( RegionGrowProcessor::INPUT_SEED_POINT).IsNotNull())
	//{
	//	Core::vtkPolyDataPtr Point;
	//	Core::DataEntityHelper::GetProcessingData(
	//		m_Processor->GetInputDataEntityHolder( RegionGrowProcessor::INPUT_SEED_POINT),
	//		Point);

	//	Point->GetPoint(0)[0] = gbl::GetNumber( this->ledX );
	//	Point->GetPoint(0)[1] = gbl::GetNumber( this->ledY );
	//	Point->GetPoint(0)[2] = gbl::GetNumber( this->ledZ );
	//}
	m_Processor->SetLowerThreshold( gbl::GetNumber( this->ledLowerThreshold ) );
	m_Processor->SetUpperThreshold( gbl::GetNumber( this->ledUpperThreshold ) );
}

bool RegionGrowWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = RegionGrowWidgetUI::Enable( enable );
	return bReturn;
}


void RegionGrowWidget::OnButtonSelectPoint(wxCommandEvent &event)
{

	ConnectInteractor();
}


bool RegionGrowWidget::CheckInputs()
{
	if( m_Processor->GetInputDataEntity( RegionGrowProcessor::INPUT_IMAGE ) )
	{
		return true;
	}
	return false;
}

void RegionGrowWidget::ConnectInteractor()
{
	Core::Widgets::LandmarkSelectorWidget* lsw = ProcessingWidget::GetSelectionToolWidget<Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" );
	if ( lsw )
	{
		// Connect interactor
		lsw->SetAllowedInputDataTypes( Core::ImageTypeId );
		lsw->SetInteractorType(Core::PointInteractor::POINT_SET);
		lsw->SetDataName( "Selected Points" );
		
		if ( dataEntityInputImage.IsNotNull() )
		{
			lsw->SetInputDataEntity( dataEntityInputImage );
		}
		
		lsw->StartInteractor();
	}
}

void RegionGrowWidget::OnModifiedSelectedPoint()
{
	m_text_seeds->SetValue("");
	if ( m_Processor->GetInputDataEntity( gsp::RegionGrowProcessor::INPUT_SEED_POINT ).IsNull() )
	{
		return;
	}

	Core::vtkPolyDataPtr points;
	Core::DataEntityHelper::GetProcessingData (
		m_Processor->GetInputDataEntityHolder( gsp::RegionGrowProcessor::INPUT_SEED_POINT ),
		points);
		
	int n = points->GetNumberOfPoints();

	if ( n == 0 )
	{
		return;
	}

	Core::DataTreeMITKHelper::ChangeShowLabelsProperty(
		m_Processor->GetInputDataEntity( gsp::RegionGrowProcessor::INPUT_SEED_POINT ),
		GetRenderingTree(),
		false);

	if ( n > 0 )
	{

		UpdateWidget();
	}
}

void RegionGrowWidget::OnButtonRegionGrow( wxCommandEvent &event )
{
	UpdateProcessor( true );
}


RegionGrowWidget::~RegionGrowWidget()
{	
}

void RegionGrowWidget::OnModifiedInputDataEntity()
{
	try
	{
		Core::DataEntity::Pointer inputDataEntity;
		inputDataEntity = m_Processor->GetInputDataEntity( RegionGrowProcessor::INPUT_IMAGE );

		if (inputDataEntity.IsNull())
		{
			UpdateWidget();
		}
		else
		{
			dataEntityInputImage = inputDataEntity;
		}
	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"RegionGrowWidget::OnModifiedInputDataEntity")
}

void RegionGrowWidget::OnModifiedOutputDataEntity()
{
	try
	{
		Core::DataEntity::Pointer inputDataEntity;
		inputDataEntity = m_Processor->GetInputDataEntity( 0 );

		// Add output to the data list and render it
		Core::DataTreeHelper::PublishOutput( 
			m_Processor->GetOutputDataEntityHolder( 0 ), 
			GetRenderingTree(),
			true,
			false);

		Core::DataTreeHelper::PublishOutput( 
			m_Processor->GetOutputDataEntityHolder( 1 ), 
			GetRenderingTree(),
			true,
			false );
	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"RegionGrowWidget::OnModifiedOutputDataEntity")
}

Core::BaseProcessor::Pointer RegionGrowWidget::GetProcessor()
{
	return m_Processor.GetPointer();
}
