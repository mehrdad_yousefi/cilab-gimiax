#ifndef RegionGrowWidget_H
#define RegionGrowWidget_H

// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "RegionGrowWidgetUI.h"
#include "gblWxConnectorOfWidgetChangesToSlotFunction.h"

#include "RegionGrowProcessor.h"

#include "coreFrontEndPlugin.h"
#include "coreRenderingTree.h"
#include "coreDataEntityListBrowser.h"
#include "coreLandmarkSelectorWidget.h"
#include "coreProcessingWidget.h"

/**
 * \brief Mesh processing panel widget
 * \ingroup GenericSegmentationPlugin
 */

#define wxID_Generic_RegionGrow_Widget wxID( "wxID_Generic_RegionGrow_Widget" )

class RegionGrowWidget : 
	public RegionGrowWidgetUI,
	public Core::Widgets::ProcessingWidget
{

public:
	coreDefineBaseWindowFactory( RegionGrowWidget )
	//!
	RegionGrowWidget( wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);
	~RegionGrowWidget();
	//! 
	void OnInit();
	//!
	Core::BaseProcessor::Pointer GetProcessor( );

	//! Update GUI from working data
	void UpdateWidget();
	//! Update working data from GUI
	void UpdateData();
	//! Validate GUI data
	bool Validate();

	//!
	void OnModifiedInputDataEntity();
	//! 
	void OnModifiedOutputDataEntity( );
	
	//!
	//void OnModifiedSeedPoint();

private:
	//!
	void OnButtonRegionGrow(wxCommandEvent &event);

	//!
	void OnButtonSelectPoint(wxCommandEvent &event);

	//!
	void OnModifiedInput( int num ){}

	//!
	void OnModifiedOutput( int num ){}

private:

	//!
	bool Enable( bool enable = true );

	//!
	void ConnectInteractor();

	//!
	bool CheckInputs();

	//!
	void OnModifiedSelectedPoint();


private:
	//!
	gbl::wx::ConnectorOfWidgetChangesToSlotFunction m_ChangeInWidgetObserver;
	//!
	gsp::RegionGrowProcessor::Pointer m_Processor;
	//!
	Core::DataEntity::Pointer dataEntityInputImage;
};

#endif //RegionGrowWidget_H
