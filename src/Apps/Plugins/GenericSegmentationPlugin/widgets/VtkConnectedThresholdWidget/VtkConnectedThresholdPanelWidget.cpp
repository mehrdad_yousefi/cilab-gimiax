/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "VtkConnectedThresholdPanelWidget.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"
// Core
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"

// Core
#include "coreDataTreeHelper.h"
#include "coreDataTreeMITKHelper.h"
#include "coreReportExceptionMacros.h"
#include "coreProcessingWidget.h"
#include "coreProcessorInputWidget.h"
#include "coreLandmarkSelectorWidget.h"

 using namespace gsp;

VtkConnectedThresholdPanelWidget::VtkConnectedThresholdPanelWidget(  wxWindow* parent, int id/*= wxID_ANY*/,
													   const wxPoint&  pos /*= wxDefaultPosition*/, 
													   const wxSize&  size /*= wxDefaultSize*/, 
													   long style/* = 0*/ )
: VtkConnectedThresholdPanelWidgetUI(parent, id,pos,size,style)
{
	m_Processor = VtkConnectedThresholdProcessor::New();

	SetName( "Vtk ConnectedThreshold " );
}

VtkConnectedThresholdPanelWidget::~VtkConnectedThresholdPanelWidget( )
{
	// We don't need to destroy anything because all the child windows 
	// of this wxWindow are destroyed automatically
}

void VtkConnectedThresholdPanelWidget::OnInit( )
{
	GetInputWidget( 0 )->Hide();
	GetInputWidget( 1 )->Hide();

	//------------------------------------------------------
	// Observers to data
	m_Processor->GetOutputDataEntityHolder( 0 )->AddObserver( 
		this, 
		&VtkConnectedThresholdPanelWidget::OnModifiedOutputDataEntity );

	m_Processor->GetInputDataEntityHolder( 1 )->AddObserver( 
		this, 
		&VtkConnectedThresholdPanelWidget::OnModifiedSeedPoint );

	m_Processor->GetInputDataEntityHolder( VtkConnectedThresholdProcessor::INPUT_IMAGE )->AddObserver( 
		this, 
		&VtkConnectedThresholdPanelWidget::OnModifiedInputDataEntity );


	UpdateWidget();
}

void VtkConnectedThresholdPanelWidget::UpdateWidget()
{
	
	UpdateHelperWidget( );
	ConnectedThresholdParameters* cthParameters = m_Processor->GetParameters();
	 
	ledLowerThreshold->SetValue(wxString::Format("%d", cthParameters->lowerThreshold));
	ledUpperThreshold->SetValue(wxString::Format("%d", cthParameters->upperThreshold));

}

void VtkConnectedThresholdPanelWidget::UpdateData()
{
	// Set parameters to processor. Pending
	ConnectedThresholdParameters* cthParameters = new ConnectedThresholdParameters();
	cthParameters->lowerThreshold = std::atoi(ledLowerThreshold->GetValue());
	cthParameters->upperThreshold = std::atoi(ledUpperThreshold->GetValue());
	m_Processor->SetParameters(cthParameters);

}

void VtkConnectedThresholdPanelWidget::OnButtonConnectedThreshold(wxCommandEvent& event)
{
	// Catch the exception from the processor and show the message box
	try
	{
		// Update the scale values from widget to processor
		UpdateData();

		UpdateProcessor( true );
	}
	coreCatchExceptionsReportAndNoThrowMacro( "VtkConnectedThresholdPanelWidget::OnBtnApply" );
}


void VtkConnectedThresholdPanelWidget::OnButtonSelectPoint(wxCommandEvent &event)
{
	Core::Widgets::LandmarkSelectorWidget* lsw = ProcessingWidget::GetSelectionToolWidget<Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" );
	if( lsw )
	{
		lsw->SetAllowedInputDataTypes( Core::ImageTypeId );
		lsw->SetInteractorType(Core::PointInteractor::POINT_SET);
		lsw->SetDataName( "Selected Point" );
		if ( dataEntityInputImage.IsNotNull() )
		{
			lsw->SetInputDataEntity( dataEntityInputImage );
		}
		lsw->StartInteractor();
	}
}

void VtkConnectedThresholdPanelWidget::OnModifiedSeedPoint()
{
	if ( m_Processor->GetInputDataEntity( 1 ).IsNull() )
	{
		return;
	}

	Core::vtkPolyDataPtr points;
	Core::DataEntityHelper::GetProcessingData (
		m_Processor->GetInputDataEntityHolder( 1 ),
		points);

	int n = points->GetNumberOfPoints();

	if ( n == 0 )
	{
		return;
	}

	Core::DataTreeMITKHelper::ChangeShowLabelsProperty(
		m_Processor->GetInputDataEntity( 1 ),
		GetRenderingTree(),
		false);

	if ( n == 1 )
	{
		UpdateWidget();
	}

}

void VtkConnectedThresholdPanelWidget::OnModifiedOutputDataEntity()
{
	try{

		Core::DataEntity::Pointer inputDataEntity;
		inputDataEntity = m_Processor->GetInputDataEntity( VtkConnectedThresholdProcessor::INPUT_IMAGE );

		// Hide input if is different from output and output is not empty
		if ( m_Processor->GetOutputDataEntity( 0 ).IsNotNull() && 
			 m_Processor->GetOutputDataEntity( 0 ) != inputDataEntity )
		{
			GetRenderingTree( )->Show( inputDataEntity, false );
		}

		// Add output to the data list and render it
		// After adding the output, the input will automatically be changed to
		// this one
		Core::DataTreeHelper::PublishOutput( 
			m_Processor->GetOutputDataEntityHolder( 0 ), 
			GetRenderingTree( ) );
	
	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"CardiacInitializationPanelWidget::OnModifiedOutputDataEntity")

}

void VtkConnectedThresholdPanelWidget::UpdateHelperWidget()
{
	if ( GetHelperWidget( ) == NULL )
	{
		return;
	}
		GetHelperWidget( )->SetInfo( 
			Core::Widgets::HELPER_INFO_LEFT_BUTTON, 
			" info that is useful in order to use the processor" );

}

bool VtkConnectedThresholdPanelWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = VtkConnectedThresholdPanelWidgetUI::Enable( enable );

	// If this panel widget is selected -> Update the widget
	if ( enable )
	{
		UpdateWidget();
	}

	return bReturn;
}

void VtkConnectedThresholdPanelWidget::OnModifiedInputDataEntity()
{
	UpdateWidget();
}

Core::BaseProcessor::Pointer VtkConnectedThresholdPanelWidget::GetProcessor()
{
	return m_Processor.GetPointer( );
}
