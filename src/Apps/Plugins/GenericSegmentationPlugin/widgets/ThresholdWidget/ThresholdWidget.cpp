// Copyright 2006 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "ThresholdWidget.h"

#include "gblWxValidate.h"
#include "gblWxButtonEventProxy.h"
#include "gblWxChoiceValueProxy.h"
#include <limits>
#include "gblWxBridgeLib.h"

#include "coreAssert.h"
#include "coreDataTreeHelper.h"
#include "coreImageDataEntityMacros.h"
#include "coreProcessorInputWidget.h"
#include "coreWxMitkGraphicalInterface.h"

#include "itkOtsuThresholdImageCalculator.h"

using namespace itk;


template <class ItkImageType> 
double DoComputeOtsuThreshold(typename ItkImageType::Pointer image)
{
	typename OtsuThresholdImageCalculator<ItkImageType>::Pointer otsu;
	otsu = OtsuThresholdImageCalculator<ItkImageType>::New();
	otsu->SetImage ( image );
	otsu->Compute();
	return otsu->GetThreshold();
}

ThresholdWidget::ThresholdWidget( wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style )
: ThresholdWidgetUI(parent, id, pos, size, style)
{
	m_Processor = gsp::ThresholdProcessor::New( );

	m_Processor->SetInputDataEntity( 0,
		Core::Runtime::Kernel::GetDataContainer()->GetDataEntityList( )->GetSelectedDataEntity());

	//m_Processor->GetOutputPort( 0 )->SetDataEntityType( Core::DataEntityType::SurfaceMeshTypeId);
	//m_Processor->GetOutputPort( 0 )->SetDataEntityType( Core::SurfaceMeshTypeId );

	m_Processor->SetLowerThreshold(0.0);
	m_Processor->SetUpperThreshold(0.0);


	//Set observer function for data update
	this->changeInWidgetObserver.SetSlotFunction(this, &ThresholdWidget::UpdateData);
	this->changeInWidgetObserver.Observe(this->ledLowerThreshold);
	this->changeInWidgetObserver.Observe(this->ledUpperThreshold);


	this->ledLowerThreshold->SetValidator( wxTextValidator ( wxFILTER_NUMERIC ) );
	this->ledUpperThreshold->SetValidator( wxTextValidator ( wxFILTER_NUMERIC ) );

}

void ThresholdWidget::SetThresholdMaxMin ()
{
	Core::vtkImageDataPtr imageData;
	Core::DataEntity::Pointer dataEntity; //= m_Processor->GetInputDataEntity(0);
	Core::DataEntityHolder::Pointer dHolder= m_Processor->GetInputDataEntityHolder(0);
	dataEntity = dHolder->GetSubject();

	double valLow = 0;
	double valUp = 0;
	double mean = 0;

	if (dataEntity.IsNotNull())
	{
		// Generate a new instance of type vtkImageData
		dataEntity->GetProcessingData( imageData, 0, false, true);	
	}
	if(imageData!=NULL)
	{
		//Set sliders limits to min and max image intensity values
		double max = imageData->GetScalarRange()[1]; 
		double min = imageData->GetScalarRange()[0]; 
		std::cout << "(" << min << "," << max << ")" << std::endl;
		this->sliderLowerThreshold->SetRange(min,max);
		this->sliderUpperThreshold->SetRange(min,max);
		this->isosurfSlider->SetRange (min,max);

		valLow = min;
		valUp = max;
		mean = (max+min)/2+1;

		std::string imageModality;
		//Core::ModalityType mod = CT;
		Core::ModalityType mod = dataEntity->GetMetadata()->GetModality();
		
		if (mod == Core::CT)
		{
			//For CT images set default values:
			//lower threshold = 100
			valLow = 100;	
			//upper threshold = 400
			valUp = 400;	
		}
		if ((mod == Core::THREE_D_RA)||(mod == Core::XA))
		{
			//Set default values for 3DRA data 
	
			//default upper threshold= max image intensity value 
			//default lower threshold = threshold calculated by Otsu Filter
			
			// Compute the Otsu Threshold for the input image
			coreImageDataEntityItkMacro(
				dHolder->GetSubject( ),
				valLow = DoComputeOtsuThreshold );
		}
	}
	//Set default values
	sliderLowerThreshold->SetValue(valLow);
	sliderUpperThreshold->SetValue(valUp);
	ledLowerThreshold->SetValue(wxString::Format("%.0f", valLow));
	ledUpperThreshold->SetValue(wxString::Format("%.0f", valUp));

	if (IsBinary(valLow,valUp))
	{
		m_SliderDoubleIsoSurface->Show(true);
		isosurfSlider->Show(false);
		m_IsBinary = true;
		FitInside();
	}
	else
	{
		m_SliderDoubleIsoSurface->Show(false);
		isosurfSlider->Show(true);
		m_IsBinary = false;
		FitInside();
	}

	isosurfSlider->SetValue (mean);	
}
void ThresholdWidget::OnInit( )
{
	// Image
	GetProcessorOutputObserver( 0 )->SetHideInput( false );
	GetProcessorOutputObserver( 0 )->SelectDataEntity( false );
	// Mesh
	GetProcessorOutputObserver( 1 )->SetHideInput( false );
	GetProcessorOutputObserver( 1 )->SelectDataEntity( false );

	//-------------------------------------------------------
	// Observers to data
	m_Processor->GetInputDataEntityHolder( 0 )->AddObserver( 
		this, 
		&ThresholdWidget::OnModifiedInputDataEntity );



	this->UpdateWidget();
}

void ThresholdWidget::OnChangedMetadata (blTagMap* tagMap,const std::string &tagId)
{
	this->SetThresholdMaxMin();
}


bool ThresholdWidget::IsBinary(double a, double b)
{
	return (a == 0 && b == 1);
}

void ThresholdWidget::UpdateWidget()
{
	bool hasParameters = m_Processor.IsNotNull();
	if ( !hasParameters )
	{
		this->Enable( false );
		return;
	}

	// For the moment, show maximum number of decimals, for otherwise, the decimal truncation may introduce
	// differences between GIMIAS and the GAR command line executable
	const int nrDecimals = std::numeric_limits<double>::digits10 + 2;
	this->changeInWidgetObserver.SetEnabled(false);

	gbl::SetNumber(this->ledLowerThreshold, m_Processor->GetLowerThreshold(), nrDecimals);
	gbl::SetNumber(this->ledUpperThreshold, m_Processor->GetUpperThreshold(), nrDecimals);

	this->changeInWidgetObserver.SetEnabled(true);
	this->Validate();
}

bool ThresholdWidget::Validate()
{
	if( m_Processor.IsNull() )
		return false;

	bool okay = true;

	// validate widget contents
	using boost::get;
	using namespace gbl::wx;
	Validator validator;

	// Check thresholds
	boost::tuple<bool, double> lowerThreshold = validator.GetNumber(this->ledLowerThreshold, gbl::VC_REAL);
	boost::tuple<bool, double> upperThreshold = validator.GetNumber(this->ledUpperThreshold, gbl::VC_REAL);
	bool orderOkay = get<1>(lowerThreshold) <= get<1>(upperThreshold);
	okay = get<0>(lowerThreshold) && get<0>(upperThreshold) && orderOkay;
	gbl::wx::SetAppearance(this->ledLowerThreshold, okay);
	gbl::wx::SetAppearance(this->ledUpperThreshold, okay);
	ledLowerThreshold->Update( );
	ledUpperThreshold->Update( );

	this->btnThreshold->Enable(okay);
	return okay;
}

void ThresholdWidget::UpdateData()
{
	if( !this->Validate() )
		return;

	m_Processor->SetLowerThreshold(gbl::GetNumber(this->ledLowerThreshold));
	m_Processor->SetUpperThreshold(gbl::GetNumber(this->ledUpperThreshold));
}

void ThresholdWidget::OnButtonThreshold( wxCommandEvent &event )
{
	m_Processor->m_flag = 0;

	UpdateProcessor( true );
}

void ThresholdWidget::OnSliderIsosurf(wxScrollEvent &event)
{
	// Only apply filter when scroll is finished
	if( event.GetEventType() == wxEVT_SCROLL_CHANGED )
	{
		try
		{
			if( m_Processor->GetInputDataEntityHolder(0)->GetSubject().IsNotNull() )
			{
				m_Processor->m_flag = 1;
				m_Processor->SetIsoValue( this->isosurfSlider->GetValue() );
			}
			else
			{
				// Warning: no data selected
				Core::Runtime::wxMitkGraphicalInterface::Pointer gIface = Core::Runtime::Kernel::GetGraphicalInterface();
				coreAssertMacro(gIface.IsNotNull());
				gIface->GetMainWindow()->ReportError("Error: no data selected.", true);
			}
		}
		coreCatchExceptionsReportAndNoThrowMacro(ThresholdWidget::OnSliderIsosurf)

		}
}

void ThresholdWidget::OnSliderLower(wxScrollEvent &event)
{
	try
	{
		double val = this->sliderLowerThreshold->GetValue();
		this->ledLowerThreshold->SetValue(wxString::Format("%.0f", val));
		this->UpdateData();
	}
	coreCatchExceptionsReportAndNoThrowMacro(ThresholdWidget::OnSliderLower)
}

void ThresholdWidget::OnSliderUpper(wxScrollEvent &event)
{
	try
	{
		double val = this->sliderUpperThreshold->GetValue();
		this->ledUpperThreshold->SetValue(wxString::Format("%.0f", val));
		this->UpdateData();
	}
	coreCatchExceptionsReportAndNoThrowMacro(ThresholdWidget::OnSliderUpper)
}

void ThresholdWidget::OnTxtUpper(wxCommandEvent &event)
{
	try
	{
		double val = atoi(this->ledUpperThreshold->GetValue());
		this->sliderUpperThreshold->SetValue(val);
		this->UpdateData();
	}
	coreCatchExceptionsReportAndNoThrowMacro(ThresholdWidget::OnTxtUpper)
}

void ThresholdWidget::OnTxtLower(wxCommandEvent &event)
{
	try
	{
		double val = atoi(this->ledLowerThreshold->GetValue());
		this->sliderLowerThreshold->SetValue(val);
		this->UpdateData();
	}
	coreCatchExceptionsReportAndNoThrowMacro(ThresholdWidget::OnTxtLower)
}

void ThresholdWidget::OnModifiedInputDataEntity()
{
	try{

		/* Add an observer in order to update all the data entity list of 
		all the plugins when a data entity is renamed
		*/
		if(m_Processor->GetInputDataEntityHolder(0)->GetSubject().IsNotNull())
		{
			m_Processor->GetInputDataEntityHolder(0)->GetSubject()->GetMetadata()->AddObserverOnChangedTag<ThresholdWidget>(
				this, 
				&ThresholdWidget::OnChangedMetadata);
		}
		SetThresholdMaxMin ();
		}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"ThresholdWidget::OnModifiedInputDataEntity")

}

Core::BaseProcessor::Pointer ThresholdWidget::GetProcessor()
{
	return m_Processor.GetPointer();
}


//!
void ThresholdWidget::OnButtonComputeMesh(wxCommandEvent &event)
{
	m_Processor->m_flag = 1;
		
	if (m_IsBinary)
	{
		m_Processor->SetIsoValue( m_SliderDoubleIsoSurface->GetValue() );
	}
	else
	{
		m_Processor->SetIsoValue( isosurfSlider->GetValue() );
	}
		

	m_Processor->Update();
}

//!
bool ThresholdWidget::Enable(bool enable /*=true*/)
{
	bool rEnable = ThresholdWidgetUI::Enable(enable);
	if(enable)
	{
		//check if the processor already have input
		if(m_Processor.IsNotNull())
		{
			OnModifiedInputDataEntity();
			blTagMap *tagMap =NULL;
			std::string tagId; //unused
			OnChangedMetadata (tagMap,tagId);
		}
	}
	return rEnable;
}
