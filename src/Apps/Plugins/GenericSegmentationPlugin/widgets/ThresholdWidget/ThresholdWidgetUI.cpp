// -*- C++ -*- generated by wxGlade 0.6.5 (standalone edition) on Mon Jan 21 10:13:02 2013

#include "ThresholdWidgetUI.h"

// begin wxGlade: ::extracode

// end wxGlade


ThresholdWidgetUI::ThresholdWidgetUI(wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    wxScrolledWindow(parent, id, pos, size, style)
{
    // begin wxGlade: ThresholdWidgetUI::ThresholdWidgetUI
    sizer_2_staticbox = new wxStaticBox(this, -1, wxT("Isosurface"));
    txtLowerThreshold = new wxStaticText(this, wxID_ANY, wxT("Lower threshold"));
    ledLowerThreshold = new wxTextCtrl(this, wxID_txtLower, wxEmptyString);
    sliderLowerThreshold = new wxSlider(this, wxID_sldLower, 0, 0, 10000, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL|wxSL_LABELS|wxSL_LEFT|wxSL_RIGHT|wxSL_TOP);
    txtUpperThreshold = new wxStaticText(this, wxID_ANY, wxT("Upper threshold"));
    ledUpperThreshold = new wxTextCtrl(this, wxID_txtUpper, wxEmptyString);
    sliderUpperThreshold = new wxSlider(this, wxID_sldUpper, 0, 0, 10000, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL|wxSL_LABELS|wxSL_LEFT|wxSL_RIGHT|wxSL_TOP);
    btnThreshold = new wxButton(this, wxID_btnThreshold, wxT("Threshold segmentation"));
    isosurfSlider = new wxSlider(this, wxID_sldIsosurf, 0, 0, 10000, wxDefaultPosition, wxDefaultSize, wxSL_LABELS|wxSL_LEFT|wxSL_RIGHT);
    m_SliderDoubleIsoSurface = new mitk::wxMitkSliderDouble(this, wxID_ANY);
    btnComputeMesh = new wxButton(this, wxID_btnComputeMesh, wxT("Compute Isosurface mesh"));

    set_properties();
    do_layout();
    // end wxGlade
}


BEGIN_EVENT_TABLE(ThresholdWidgetUI, wxScrolledWindow)
    // begin wxGlade: ThresholdWidgetUI::event_table
    EVT_TEXT(wxID_txtLower, ThresholdWidgetUI::OnTxtLower)
    EVT_COMMAND_SCROLL_CHANGED(wxID_sldLower, ThresholdWidgetUI::OnSliderLower)
    EVT_TEXT(wxID_txtUpper, ThresholdWidgetUI::OnTxtUpper)
    EVT_COMMAND_SCROLL_CHANGED(wxID_sldUpper, ThresholdWidgetUI::OnSliderUpper)
    EVT_BUTTON(wxID_btnThreshold, ThresholdWidgetUI::OnButtonThreshold)
    EVT_COMMAND_SCROLL_CHANGED(wxID_sldIsosurf, ThresholdWidgetUI::OnSliderIsosurf)
    EVT_BUTTON(wxID_btnComputeMesh, ThresholdWidgetUI::OnButtonComputeMesh)
    // end wxGlade
END_EVENT_TABLE();


void ThresholdWidgetUI::OnTxtLower(wxCommandEvent &event)
{
    event.Skip();
    wxLogDebug(wxT("Event handler (ThresholdWidgetUI::OnTxtLower) not implemented yet")); //notify the user that he hasn't implemented the event handler yet
}


void ThresholdWidgetUI::OnSliderLower(wxScrollEvent &event)
{
    event.Skip();
    wxLogDebug(wxT("Event handler (ThresholdWidgetUI::OnSliderLower) not implemented yet")); //notify the user that he hasn't implemented the event handler yet
}


void ThresholdWidgetUI::OnTxtUpper(wxCommandEvent &event)
{
    event.Skip();
    wxLogDebug(wxT("Event handler (ThresholdWidgetUI::OnTxtUpper) not implemented yet")); //notify the user that he hasn't implemented the event handler yet
}


void ThresholdWidgetUI::OnSliderUpper(wxScrollEvent &event)
{
    event.Skip();
    wxLogDebug(wxT("Event handler (ThresholdWidgetUI::OnSliderUpper) not implemented yet")); //notify the user that he hasn't implemented the event handler yet
}


void ThresholdWidgetUI::OnButtonThreshold(wxCommandEvent &event)
{
    event.Skip();
    wxLogDebug(wxT("Event handler (ThresholdWidgetUI::OnButtonThreshold) not implemented yet")); //notify the user that he hasn't implemented the event handler yet
}


void ThresholdWidgetUI::OnSliderIsosurf(wxScrollEvent &event)
{
    event.Skip();
    wxLogDebug(wxT("Event handler (ThresholdWidgetUI::OnSliderIsosurf) not implemented yet")); //notify the user that he hasn't implemented the event handler yet
}


void ThresholdWidgetUI::OnButtonComputeMesh(wxCommandEvent &event)
{
    event.Skip();
    wxLogDebug(wxT("Event handler (ThresholdWidgetUI::OnButtonComputeMesh) not implemented yet")); //notify the user that he hasn't implemented the event handler yet
}


// wxGlade: add ThresholdWidgetUI event handlers


void ThresholdWidgetUI::set_properties()
{
    // begin wxGlade: ThresholdWidgetUI::set_properties
    SetScrollRate(10, 10);
    ledLowerThreshold->SetMinSize(wxSize(50, 20));
    ledUpperThreshold->SetMinSize(wxSize(50, 20));
    // end wxGlade
}


void ThresholdWidgetUI::do_layout()
{
    // begin wxGlade: ThresholdWidgetUI::do_layout
    wxBoxSizer* sizer_6 = new wxBoxSizer(wxVERTICAL);
    wxStaticBoxSizer* sizer_2 = new wxStaticBoxSizer(sizer_2_staticbox, wxVERTICAL);
    wxBoxSizer* sizer_7 = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* sizer_9 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* sizer_8 = new wxBoxSizer(wxHORIZONTAL);
    sizer_8->Add(txtLowerThreshold, 0, wxLEFT|wxRIGHT|wxALIGN_BOTTOM, 5);
    sizer_8->Add(ledLowerThreshold, 0, wxLEFT|wxRIGHT|wxALIGN_BOTTOM|wxALIGN_CENTER_VERTICAL, 5);
    sizer_8->Add(sliderLowerThreshold, 1, wxLEFT|wxRIGHT|wxEXPAND, 5);
    sizer_7->Add(sizer_8, 0, wxEXPAND, 0);
    sizer_9->Add(txtUpperThreshold, 0, wxLEFT|wxRIGHT|wxTOP|wxALIGN_BOTTOM, 5);
    sizer_9->Add(ledUpperThreshold, 0, wxLEFT|wxRIGHT|wxTOP|wxALIGN_BOTTOM, 5);
    sizer_9->Add(sliderUpperThreshold, 1, wxLEFT|wxRIGHT|wxTOP|wxEXPAND, 5);
    sizer_7->Add(sizer_9, 0, wxEXPAND, 0);
    sizer_7->Add(btnThreshold, 0, wxALL|wxEXPAND, 5);
    sizer_6->Add(sizer_7, 0, wxEXPAND, 0);
    sizer_2->Add(isosurfSlider, 0, wxALL|wxEXPAND, 5);
    sizer_2->Add(m_SliderDoubleIsoSurface, 1, wxALL|wxEXPAND, 5);
    sizer_2->Add(btnComputeMesh, 0, wxALL|wxEXPAND, 5);
    sizer_6->Add(sizer_2, 0, wxALL|wxEXPAND, 5);
    SetSizer(sizer_6);
    sizer_6->Fit(this);
    // end wxGlade
}

