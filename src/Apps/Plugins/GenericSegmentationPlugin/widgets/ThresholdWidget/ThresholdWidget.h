#ifndef ThresholdWidget_H
#define ThresholdWidget_H

// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "ThresholdWidgetUI.h"

#include "ThresholdProcessor.h"
#include "gblWxConnectorOfWidgetChangesToSlotFunction.h"

#include "coreRenderingTree.h"
#include "coreDataEntityListBrowser.h"
#include "coreProcessingWidget.h"

/**
 * \brief Mesh processing panel widget
 * \ingroup GenericSegmentationPlugin
 */

#define wxID_Generic_Treshold_Widget wxID( "wxID_Generic_Treshold_Widget" )

class ThresholdWidget : 
	public ThresholdWidgetUI,
	public Core::Widgets::ProcessingWidget
{
public:
	coreDefineBaseWindowFactory( ThresholdWidget )
	ThresholdWidget(wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

	//! Initialize this class after construction.
	void OnInit();
	//!
	Core::BaseProcessor::Pointer GetProcessor( );

	//! Update GUI from working data
	void UpdateWidget();
	//! Update working data from GUI
	void UpdateData();
	//! Validate GUI data
	bool Validate();
	//!
	void OnButtonThreshold(wxCommandEvent &event);
	//!
	void OnButtonComputeMesh(wxCommandEvent &event);
	//!
	void OnSliderIsosurf(wxScrollEvent &event);
	//!
	void OnSliderLower(wxScrollEvent &event);
	//!
	void OnSliderUpper(wxScrollEvent &event);
	//!
	void OnTxtUpper(wxCommandEvent &event);
	//! 
	void OnTxtLower(wxCommandEvent &event);
	//! Referesh min/max
	void OnModifiedInputDataEntity( );
	//!
	void OnChangedMetadata (blTagMap* tagMap,const std::string &tagId);

	//!
	bool Enable(bool enable = true);
	
//! private methods
private:
	//!
	void SetThresholdMaxMin ();

	//!
	void OnModifiedInput( int num ){}

	//!
	void OnModifiedOutput( int num ){}

	//
	bool IsBinary(double a, double b);

//! private members
private:
	//!
	gbl::wx::ConnectorOfWidgetChangesToSlotFunction changeInWidgetObserver;
	//!
	gsp::ThresholdProcessor::Pointer m_Processor;
	//!
	bool m_IsBinary;

};

#endif //ThresholdWidget_H
