#ifndef ConnectedThresholdWidget_H
#define ConnectedThresholdWidget_H

// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "coreLandmarkSelectorWidget.h"
#include "coreRenderingTree.h"
#include "coreDataEntityListBrowser.h"
#include "coreProcessingWidget.h"


#include "ConnectedThresholdWidgetUI.h"
#include "ConnectedThresholdProcessor.h"
#include "gblWxConnectorOfWidgetChangesToSlotFunction.h"


/**
 * \brief Connected Threshold Segmentation panel widget
 * \ingroup GenericSegmentationPlugin
 */

#define wxID_Generic_ConnectedThreshold_Widget wxID( "wxID_Generic_ConnectedThreshold_Widget" )

class ConnectedThresholdWidget : 
	public ConnectedThresholdWidgetUI,
	public Core::Widgets::ProcessingWidget
{

public:
	coreDefineBaseWindowFactory( ConnectedThresholdWidget )
	ConnectedThresholdWidget( wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);
	//! Initialize this class after construction.
	void OnInit();

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

private:

	//! Update GUI from working data
	void UpdateWidget();
	//! Update working data from GUI
	void UpdateData();
	//! Validate GUI data
	bool Validate();
	//!
	void OnButtonConnectedThreshold(wxCommandEvent &event); 
	//!
	void OnButtonSelectPoint(wxCommandEvent &event);
	//!
	void OnModifiedInputDataEntity();
	//! 
	void OnModifiedOutputDataEntity( );
	//!
	void OnModifiedSeedPoint();
	//!
	bool Enable( bool enable = true );
	//!
	void OnModifiedInput( int num ){}
	//!
	void OnModifiedOutput( int num ){}
	//!
	void ConnectInteractor();

// ATTRIBUTES
private:
	//!
	gbl::wx::ConnectorOfWidgetChangesToSlotFunction m_changeInWidgetObserver;
	//!
	gsp::ConnectedThresholdProcessor::Pointer m_Processor;
	//!
	Core::DataEntity::Pointer dataEntityInputImage;
};

#endif //ConnectedThresholdWidget_H
