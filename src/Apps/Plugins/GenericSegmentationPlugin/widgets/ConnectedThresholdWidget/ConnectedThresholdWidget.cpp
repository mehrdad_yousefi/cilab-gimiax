// Copyright 2006 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "ConnectedThresholdWidget.h"

#include "coreAssert.h"
#include "coreDataTreeHelper.h"
#include "coreDataTreeMITKHelper.h"
#include "coreDataEntityHelper.h"
#include "coreProcessorInputWidget.h"

#include "gblWxButtonEventProxy.h"
#include "gblWxChoiceValueProxy.h"
#include <limits>
#include "gblWxBridgeLib.h"
#include "gblWxValidate.h"


using namespace gsp;

ConnectedThresholdWidget::ConnectedThresholdWidget( wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style )
: ConnectedThresholdWidgetUI(parent, id, pos, size, style)
{
	m_Processor = gsp::ConnectedThresholdProcessor::New( );
	
	m_Processor->SetLowerThreshold(150.0);
	m_Processor->SetUpperThreshold(300.0);
	m_Processor->SetNumberOfIterations(5);
	m_Processor->SetTimeStep(0.0125);
	m_Processor->SetConductance(1.0);
	m_Processor->SetImageSpacing(true);

	this->m_changeInWidgetObserver.SetSlotFunction(this, &ConnectedThresholdWidget::UpdateData);
	this->m_changeInWidgetObserver.Observe(this->ledLowerThreshold);
	this->m_changeInWidgetObserver.Observe(this->ledUpperThreshold);
	this->m_changeInWidgetObserver.Observe(this->ledNrOfIterations);
	this->m_changeInWidgetObserver.Observe(this->ledTimeStep);
	this->m_changeInWidgetObserver.Observe(this->ledConductance);
	this->m_changeInWidgetObserver.Observe(this->chbUseImageSpacing);
	this->m_changeInWidgetObserver.Observe(this->ledX);
	this->m_changeInWidgetObserver.Observe(this->ledY);
	this->m_changeInWidgetObserver.Observe(this->ledZ);

	this->ledLowerThreshold->SetValidator( wxTextValidator ( wxFILTER_NUMERIC ) );
	this->ledUpperThreshold->SetValidator( wxTextValidator ( wxFILTER_NUMERIC ) );
	this->ledNrOfIterations->SetValidator( wxTextValidator ( wxFILTER_NUMERIC ) );
	this->ledTimeStep->SetValidator( wxTextValidator ( wxFILTER_NUMERIC ) );
	this->ledConductance->SetValidator( wxTextValidator ( wxFILTER_NUMERIC ) );
	this->ledX->SetValidator( wxTextValidator ( wxFILTER_NUMERIC ) );
	this->ledY->SetValidator( wxTextValidator ( wxFILTER_NUMERIC ) );
	this->ledZ->SetValidator( wxTextValidator ( wxFILTER_NUMERIC ) );
}

void ConnectedThresholdWidget::OnInit( )
{
	GetInputWidget( ConnectedThresholdProcessor::INPUT_IMAGE )->Hide();
	GetInputWidget( ConnectedThresholdProcessor::INPUT_SEED_POINT )->Hide();

	// Observers to data
	m_Processor->GetInputDataEntityHolder( ConnectedThresholdProcessor::INPUT_SEED_POINT )->AddObserver( 
		this, 
		&ConnectedThresholdWidget::OnModifiedSeedPoint );

	m_Processor->GetInputDataEntityHolder( ConnectedThresholdProcessor::INPUT_IMAGE )->AddObserver( 
		this, 
		&ConnectedThresholdWidget::OnModifiedInputDataEntity );

	m_Processor->GetOutputDataEntityHolder( 0 )->AddObserver( 
		this, 
		&ConnectedThresholdWidget::OnModifiedOutputDataEntity );

	this->UpdateWidget();
}

void ConnectedThresholdWidget::UpdateWidget()
{
	this->m_changeInWidgetObserver.SetEnabled(false);
	// For the moment, show maximum number of decimals, for otherwise, the decimal truncation may introduce
	// differences between GIMIAS and the GAR command line executable
	const int nrDecimals = std::numeric_limits<double>::digits10 + 2;

	// Set into the widget the seed point value
	itk::Point< double, 3 > seedPoint;
	Core::vtkPolyDataPtr vtkPoint;
	if ( m_Processor->GetInputDataEntity( ConnectedThresholdProcessor::INPUT_SEED_POINT).IsNotNull())
	{

		Core::DataEntityHelper::GetProcessingData(
			m_Processor->GetInputDataEntityHolder( ConnectedThresholdProcessor::INPUT_SEED_POINT),
			vtkPoint);

	}
	else
	{
		vtkPoint = vtkPolyData::New();
		vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
		points->InsertPoint(0,0,0,0);
		vtkPoint->SetPoints(points);
	}
	seedPoint[0] = vtkPoint->GetPoint(0)[0];
	seedPoint[1] = vtkPoint->GetPoint(0)[1];
	seedPoint[2] = vtkPoint->GetPoint(0)[2];

	gbl::SetNumber(this->ledX, seedPoint[0], nrDecimals);
	gbl::SetNumber(this->ledY, seedPoint[1], nrDecimals);
	gbl::SetNumber(this->ledZ, seedPoint[2], nrDecimals);

	// Set the values into the widget	
	gbl::SetNumber(this->ledLowerThreshold, m_Processor->GetLowerThreshold(), nrDecimals);
	gbl::SetNumber(this->ledUpperThreshold, m_Processor->GetUpperThreshold(), nrDecimals);
	gbl::SetNumber(this->ledNrOfIterations, m_Processor->GetNumberOfIterations(), nrDecimals);
	gbl::SetNumber(this->ledTimeStep, m_Processor->GetTimeStep(), nrDecimals);
	gbl::SetNumber(this->ledConductance, m_Processor->GetConductance(), nrDecimals);
	gbl::SetFlag(this->chbUseImageSpacing, m_Processor->GetImageSpacing());

	this->m_changeInWidgetObserver.SetEnabled(true);
	this->Validate();
}

bool ConnectedThresholdWidget::Validate()
{
	bool okay = true;
	using boost::get;
	using namespace gbl::wx;
	Validator validator;

	boost::tuple<bool, double> lowerThreshold = validator.GetNumber(this->ledLowerThreshold, gbl::VC_REAL);
	boost::tuple<bool, double> upperThreshold = validator.GetNumber(this->ledUpperThreshold, gbl::VC_REAL);
	bool orderOkay = get<1>(lowerThreshold) <= get<1>(upperThreshold);
	okay = get<0>(lowerThreshold) && get<0>(upperThreshold) && orderOkay;
	SetAppearance(this->ledLowerThreshold, okay);
	SetAppearance(this->ledUpperThreshold, okay);

	okay = validator.IsNumber(this->ledX, gbl::VC_REAL) && okay;
	okay = validator.IsNumber(this->ledY, gbl::VC_REAL) && okay;
	okay = validator.IsNumber(this->ledZ, gbl::VC_REAL) && okay;
	okay = validator.IsNumber(this->ledNrOfIterations, gbl::VC_NONNEGATIVE_INTEGER) && okay;
	okay = validator.IsNumber(this->ledTimeStep, gbl::VC_NONNEGATIVE_REAL) && okay;
	okay = validator.IsNumber(this->ledConductance, gbl::VC_REAL) && okay;

	this->btnConnectedThreshold->Enable(okay);
	return okay;
}

void ConnectedThresholdWidget::UpdateData()
{
	if( !this->Validate() )
		return;
	if (m_Processor->GetInputDataEntity( ConnectedThresholdProcessor::INPUT_SEED_POINT).IsNotNull())
	{
		Core::vtkPolyDataPtr Point;
		Core::DataEntityHelper::GetProcessingData(
			m_Processor->GetInputDataEntityHolder( ConnectedThresholdProcessor::INPUT_SEED_POINT),
			Point);
		Point->GetPoint(0)[0] = gbl::GetNumber( this->ledX );
		Point->GetPoint(0)[1] = gbl::GetNumber( this->ledY );
		Point->GetPoint(0)[2] = gbl::GetNumber( this->ledZ );
	}
    //store the values in the data holder
	m_Processor->SetLowerThreshold(gbl::GetNumber( this->ledLowerThreshold ));
	m_Processor->SetUpperThreshold(gbl::GetNumber( this->ledUpperThreshold ));
	m_Processor->SetNumberOfIterations(gbl::GetNumber( this->ledNrOfIterations ));
	m_Processor->SetTimeStep(gbl::GetNumber( this->ledTimeStep ));
	m_Processor->SetConductance(gbl::GetNumber( this->ledConductance ));
	m_Processor->SetImageSpacing(gbl::GetFlag( this->chbUseImageSpacing ));
}

void ConnectedThresholdWidget::OnButtonConnectedThreshold( wxCommandEvent &event )
{
	UpdateProcessor( true );
}

bool ConnectedThresholdWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = ConnectedThresholdWidgetUI::Enable( enable );
	return bReturn;
}

void ConnectedThresholdWidget::OnModifiedInputDataEntity()
{
	try{
		Core::DataEntity::Pointer inputDataEntity;
		inputDataEntity = m_Processor->GetInputDataEntity( ConnectedThresholdProcessor::INPUT_IMAGE );
		if (inputDataEntity.IsNull())
			UpdateWidget();
	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"ConnectedThresholdWidget::OnModifiedInputDataEntity")
}


void ConnectedThresholdWidget::OnModifiedOutputDataEntity()
{
	try{
		// Add output to the data list and render it
		Core::DataTreeHelper::PublishOutput( 
			m_Processor->GetOutputDataEntityHolder( 0 ), 
			GetRenderingTree(),
			true,
			false);

		Core::DataTreeHelper::PublishOutput( 
			m_Processor->GetOutputDataEntityHolder( 1 ), 
			GetRenderingTree(),
			true,
			false );

	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"ConnectedThresholdWidget::OnModifiedOutputDataEntity")

}

void ConnectedThresholdWidget::OnModifiedSeedPoint()
{
	if ( m_Processor->GetInputDataEntity( gsp::ConnectedThresholdProcessor::INPUT_SEED_POINT ).IsNull() )
	{
		return;
	}

	Core::vtkPolyDataPtr points;
	Core::DataEntityHelper::GetProcessingData (
		m_Processor->GetInputDataEntityHolder( gsp::ConnectedThresholdProcessor::INPUT_SEED_POINT ),
		points);

	int n = points->GetNumberOfPoints();

	if ( n == 0 )
	{
		return;
	}

	Core::DataTreeMITKHelper::ChangeShowLabelsProperty(
		m_Processor->GetInputDataEntity( gsp::ConnectedThresholdProcessor::INPUT_SEED_POINT ),
		GetRenderingTree(),
		false);

	if ( n == 1 )
	{
		UpdateWidget();
	}

}

void ConnectedThresholdWidget::OnButtonSelectPoint(wxCommandEvent &event)
{
	ConnectInteractor();
}


Core::BaseProcessor::Pointer ConnectedThresholdWidget::GetProcessor()
{
	return m_Processor.GetPointer();
}

void ConnectedThresholdWidget::ConnectInteractor()
{
	Core::Widgets::LandmarkSelectorWidget* lsw = ProcessingWidget::GetSelectionToolWidget<Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" );
	if ( lsw )
	{
		lsw->SetAllowedInputDataTypes( Core::ImageTypeId );
		lsw->SetInteractorType(Core::PointInteractor::POINT_SET);
		lsw->SetDataName( "Selected Point" );
		if ( dataEntityInputImage.IsNotNull() )
		{
			lsw->SetInputDataEntity( dataEntityInputImage );
		}
		lsw->StartInteractor();
	}
}