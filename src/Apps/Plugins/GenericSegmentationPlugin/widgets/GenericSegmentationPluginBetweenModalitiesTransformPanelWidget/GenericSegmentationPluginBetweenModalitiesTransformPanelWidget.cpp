/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "GenericSegmentationPluginBetweenModalitiesTransformPanelWidget.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"
// Core
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"

// Core
#include "coreDataTreeHelper.h"
#include "coreReportExceptionMacros.h"

namespace GenericSegmentationPlugin
{

const wxWindowID BetweenModalitiesTransformPanelWidget::ms_WidgetID = wxNewId();
const std::string BetweenModalitiesTransformPanelWidget::ms_WidgetCaption = "BetweenModalitiesTransform Panel Widget";

BetweenModalitiesTransformPanelWidget::BetweenModalitiesTransformPanelWidget(  wxWindow* parent, int id/*= wxID_ANY*/,
													   const wxPoint&  pos /*= wxDefaultPosition*/, 
													   const wxSize&  size /*= wxDefaultSize*/, 
													   long style/* = 0*/ )
: GenericSegmentationPluginBetweenModalitiesTransformPanelWidgetUI(parent, id,pos,size,style)
{
	m_Processor = GenericSegmentationPlugin::BetweenModalitiesTransformProcessor::New();

	SetName( "BetweenModalitiesTransform Panel Widget" );
}

BetweenModalitiesTransformPanelWidget::~BetweenModalitiesTransformPanelWidget( )
{
	// We don't need to destroy anything because all the child windows 
	// of this wxWindow are destroyed automatically
}

void BetweenModalitiesTransformPanelWidget::OnInit( )
{
	//------------------------------------------------------
	// Observers to data
	m_Processor->GetOutputDataEntityHolder( 0 )->AddObserver( this, 
		&BetweenModalitiesTransformPanelWidget::OnModifiedOutputDataEntity );

	m_Processor->GetInputDataEntityHolder( 0 )->AddObserver( this, 
		&BetweenModalitiesTransformPanelWidget::OnModifiedInputDataEntity );

	UpdateWidget();
}

void BetweenModalitiesTransformPanelWidget::UpdateWidget()
{
	UpdateHelperWidget();
}

void BetweenModalitiesTransformPanelWidget::UpdateData()
{
	// Set parameters to processor. Pending
}

void BetweenModalitiesTransformPanelWidget::OnReorientPressed( wxCommandEvent& event )
{
	// Catch the exception from the processor and show the message box
	try
	{
		// Update the scale values from widget to processor
		UpdateData();

		m_Processor->Update();
	}
	coreCatchExceptionsReportAndNoThrowMacro( "BetweenModalitiesTransformPanelWidget::OnReorientPressed" );
}


void BetweenModalitiesTransformPanelWidget::OnModifiedOutputDataEntity()
{
	try
	{
		Core::DataEntity::Pointer inputDataEntity;
		inputDataEntity = m_Processor->GetInputDataEntity( 0 );

		// Hide input if is different from output and output is not empty
		if( m_Processor->GetOutputDataEntity( 0 ).IsNotNull() && 
			 m_Processor->GetOutputDataEntity( 0 ) != inputDataEntity )
		{
			GetRenderingTree( )->Show( inputDataEntity, false );
		}
	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"CardiacInitializationPanelWidget::OnModifiedOutputDataEntity")
}

void BetweenModalitiesTransformPanelWidget::UpdateHelperWidget()
{
	if( GetHelperWidget( ) == NULL )
	{
		return;
	}
	GetHelperWidget( )->SetInfo( 
		Core::Widgets::HELPER_INFO_LEFT_BUTTON, 
		" info that is useful in order to use the processor" );

}

bool BetweenModalitiesTransformPanelWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = GenericSegmentationPluginBetweenModalitiesTransformPanelWidgetUI::Enable( enable );

	// If this panel widget is selected -> Update the widget
	if( enable )
	{
		UpdateWidget();
	}

	return bReturn;
}

void BetweenModalitiesTransformPanelWidget::OnModifiedInputDataEntity()
{
	UpdateWidget();
}

Core::BaseProcessor::Pointer BetweenModalitiesTransformPanelWidget::GetProcessor()
{
	return m_Processor.GetPointer();
}

}	// namespace GenericSegmentationPlugin
