/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "MaskImageToROIImageWidget.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"

// Core
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"
#include "coreProcessorInputWidget.h"
#include "coreDataTreeHelper.h"
#include "coreReportExceptionMacros.h"


using namespace Core::Widgets;

namespace GenericSegmentationPlugin
{

MaskImageToROIImageWidget::MaskImageToROIImageWidget(  wxWindow* parent, int id/*= wxID_ANY*/,
													   const wxPoint&  pos /*= wxDefaultPosition*/, 
													   const wxSize&  size /*= wxDefaultSize*/, 
													   long style/* = 0*/ )
: MaskImageToROIImageWidgetUI(parent, id,pos,size,style)
{
	m_Processor = MaskImageToROIImageProcessor::New();

	SetName( "MaskImageToROIImage Panel Widget" );
}

MaskImageToROIImageWidget::~MaskImageToROIImageWidget()
{
	// We don't need to destroy anything because all the child windows 
	// of this wxWindow are destroyed automatically
}

void MaskImageToROIImageWidget::OnInit()
{
	//------------------------------------------------------
	// Observers to data
	m_Processor->GetOutputDataEntityHolder( MaskImageToROIImageProcessor::OUTPUT_ROIIMAGE )->AddObserver( 
		this, 
		&MaskImageToROIImageWidget::OnModifiedOutputDataEntity );

	m_Processor->GetInputDataEntityHolder( MaskImageToROIImageProcessor::INPUT_MASKIMAGE )->AddObserver( 
		this, 
		&MaskImageToROIImageWidget::OnModifiedInputDataEntity );

	GetProcessorOutputObserver( MaskImageToROIImageProcessor::OUTPUT_ROIIMAGE )->SetHideInput( false );
	GetProcessorOutputObserver( MaskImageToROIImageProcessor::OUTPUT_ROIIMAGE )->SelectDataEntity( false );

	m_txtNumberOfLevels->SetValue( wxT( "2" ) );

	// Redistribute UI
	wxSizer* pGlobalSizer = GetSizer();
	ProcessorInputWidget* pInputWidgets[MaskImageToROIImageProcessor::INPUTS_NUMBER];
	// First retrieve and detach every input widget, to set later when we want
	for( int indexInput = 0; indexInput < MaskImageToROIImageProcessor::INPUTS_NUMBER; indexInput++ )
	{
		pInputWidgets[indexInput] = GetInputWidget( indexInput );
		pGlobalSizer->Detach( pInputWidgets[indexInput] );
	}

	//wxSizer* pHorizontalMarginSizer = pGlobalSizer->GetItem( (size_t)0 )->GetSizer();
	wxSizer* pHorizontalMarginSizer = pGlobalSizer;

	pHorizontalMarginSizer->Insert( 1, pInputWidgets[MaskImageToROIImageProcessor::INPUT_ROIIMAGE],
		0, wxEXPAND | wxTOP, 5 );

	// Now begin to add from last to first, so that is easy to know the insert position
	pHorizontalMarginSizer->Insert( 0, pInputWidgets[MaskImageToROIImageProcessor::INPUT_MASKIMAGE],
		0, wxEXPAND | wxTOP, 5 );

	GetSizer()->FitInside( this );


	UpdateWidget();
}

void MaskImageToROIImageWidget::UpdateWidget()
{
	UpdateHelperWidget();
}

void MaskImageToROIImageWidget::UpdateData()
{
	// Set parameters to processor. Pending
}

void MaskImageToROIImageWidget::OnMaskToROIPressed( wxCommandEvent &event )
{
	// Catch the exception from the processor and show the message box
	try
	{
		// Update the scale values from widget to processor
		UpdateData();

		long numberOfLevels = 0;
		if( m_txtNumberOfLevels->GetValue().ToLong( &numberOfLevels ) && ( numberOfLevels > 0 ) && ( numberOfLevels < 256 ) )
		{
			m_Processor->SetNumberOfLevels( numberOfLevels );
			
			UpdateProcessor( true );
		}
		else
		{
			// Incorrect levels!!!
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro( "MaskImageToROIImagePanelWidget::OnMaskToROIPressed" );
}

void MaskImageToROIImageWidget::OnROIToMaskPressed( wxCommandEvent &event )
{
	m_Processor->CreateMaskImageFromROIImage();
}


void MaskImageToROIImageWidget::OnModifiedOutputDataEntity()
{
	try
	{
		Core::DataEntity::Pointer maskImageDE = m_Processor->GetInputDataEntity( MaskImageToROIImageProcessor::INPUT_MASKIMAGE );
		if( maskImageDE.IsNotNull() )
		{
			mitk::DataTreeNode::Pointer imageMaskNode;
			Core::CastAnyProcessingData( GetRenderingTree()->GetNode( maskImageDE ), imageMaskNode );
			if( imageMaskNode.IsNotNull() )
			{
				GetRenderingTree()->Show( maskImageDE, false, true );
			}
		}
	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"CardiacInitializationPanelWidget::OnModifiedOutputDataEntity")
}

void MaskImageToROIImageWidget::UpdateHelperWidget()
{
	if ( GetHelperWidget( ) == NULL )
	{
		return;
	}
		GetHelperWidget( )->SetInfo( 
			Core::Widgets::HELPER_INFO_LEFT_BUTTON, 
			" info that is useful in order to use the processor" );

}

bool MaskImageToROIImageWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = MaskImageToROIImageWidgetUI::Enable( enable );

	// If this panel widget is selected -> Update the widget
	if ( enable )
	{
		UpdateWidget();
	}

	return bReturn;
}

void MaskImageToROIImageWidget::OnModifiedInputDataEntity()
{
	UpdateWidget();
}

Core::BaseProcessor::Pointer MaskImageToROIImageWidget::GetProcessor()
{
	return m_Processor.GetPointer( );
}


}   // GenericSegmentationPlugin
