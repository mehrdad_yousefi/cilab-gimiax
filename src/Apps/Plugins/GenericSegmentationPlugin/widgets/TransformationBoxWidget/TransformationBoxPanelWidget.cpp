/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "TransformationBoxPanelWidget.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"

// Core
#include "coreUserHelperWidget.h"
#include "coreDataTreeHelper.h"
#include "coreReportExceptionMacros.h"
#include "coreMultiRenderWindowMITK.h"

//Mitk
#include "mitkBaseVtkMapper3D.h"
#include "mitkBaseRenderer.h"

#include <vtkTransform.h>
#include <vtkProp3D.h>

namespace GenericSegmentationPlugin
{

	class vtkApplyTransformCallback : public vtkCommand
	{
		public:
			static vtkApplyTransformCallback *New()
			{ 
				return new vtkApplyTransformCallback; 
			}
			
			virtual void Execute(vtkObject *caller, unsigned long, void*)
			{
//				vtkTransform *t = vtkTransform::New();
//				vtkBoxWidget *widget = reinterpret_cast<vtkBoxWidget*>(caller);
//				widget->GetTransform(t);
				//Workaround: this won't work because of MITK
				//widget->GetProp3D()->SetUserTransform(t);
				if(m_widget)
					m_widget->UpdateTransformTable();
				 //t->Delete();
			}
		
			void SetWidget(TransformationBoxPanelWidget *widget)
			{
				m_widget = widget;
			}
		private:
			
			vtkApplyTransformCallback(){m_widget = NULL;};
			TransformationBoxPanelWidget *m_widget;
		
	};	
	

TransformationBoxPanelWidget::TransformationBoxPanelWidget(  wxWindow* parent, int id/*= wxID_ANY*/,
													   const wxPoint&  pos /*= wxDefaultPosition*/, 
													   const wxSize&  size /*= wxDefaultSize*/, 
													   long style/* = 0*/ )
:TransformationBoxPanelWidgetUI(parent, id,pos,size,style)
{
	m_Processor = TransformationBoxProcessor::New();

	SetName( "TransformationBox Panel Widget" );
}

TransformationBoxPanelWidget::~TransformationBoxPanelWidget( )
{
	// We don't need to destroy anything because all the child windows 
	// of this wxWindow are destroyed automatically
	if(m_callback!=NULL)
		m_callback->Delete();
	
	if(m_boxWidget!=NULL)
		m_boxWidget->Delete();
}

void TransformationBoxPanelWidget::OnInit()
{

	//------------------------------------------------------
	// Observers to data
	m_Processor->GetInputDataEntityHolder( TransformationBoxProcessor::INPUT_0 )->AddObserver( 
		this, 
		&TransformationBoxPanelWidget::OnModifiedInputDataEntity );

	
	// The box widget observes the events invoked by the render window
	// interactor.  These events come from user interaction in the render
	// window.
	m_boxWidget = vtkBoxWidget::New();
	
	
	Core::Widgets::MultiRenderWindowMITK* mitkRenderWindow =
		dynamic_cast<Core::Widgets::MultiRenderWindowMITK*> ( GetMultiRenderWindow() );
	vtkRenderWindowInteractor *iren = mitkRenderWindow->Get3D( )->GetVtkRenderWindow()->GetInteractor();

	m_boxWidget->SetInteractor(iren);
	m_boxWidget->SetPlaceFactor(1.25);
	m_callback = vtkApplyTransformCallback::New();
	m_callback->SetWidget(this);
	m_boxWidget->AddObserver(vtkCommand::InteractionEvent, m_callback);

	UpdateWidget();
}

void TransformationBoxPanelWidget::UpdateWidget()
{
	wxString label;
	(m_btnShowBox->GetValue())?label = wxString("Hide"):label = wxString("Show");
	m_btnShowBox->SetLabel(label);
	UpdateHelperWidget( );
}

void TransformationBoxPanelWidget::UpdateData()
{
}

void TransformationBoxPanelWidget::ResetBoxWidget()
{
	if(m_boxWidget==NULL)
		return;
	
	vtkSmartPointer <vtkTransform> t = GetTransform(); 
	t->Identity();
	m_boxWidget->SetTransform(t);
}
	
void TransformationBoxPanelWidget::OnModifiedInputDataEntity()
{
	try{
		Core::DataEntity::Pointer data = 
			m_Processor->GetInputDataEntity(TransformationBoxProcessor::INPUT_0);

		if(data.IsNull())
		{
			UpdateTransformTable();
			m_btnShowBox->SetValue(0);
			if(m_boxWidget!=NULL)
				m_boxWidget->Off();
			UpdateWidget();
		}
		else {
			ShowBox(m_btnShowBox->GetValue());
		}

	
	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"TransformationBoxPanelWidget::OnModifiedInputDataEntity")

}

void TransformationBoxPanelWidget::UpdateHelperWidget()
{
}

bool TransformationBoxPanelWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = TransformationBoxPanelWidgetUI::Enable( enable );

	// If this panel widget is selected -> Update the widget
	if ( enable )
	{
		UpdateWidget();
	}

	return bReturn;
}


Core::BaseProcessor::Pointer TransformationBoxPanelWidget::GetProcessor()
{
	return m_Processor.GetPointer( );
}


void TransformationBoxPanelWidget::ShowBox(bool show)
{
	try
	{
		if(m_boxWidget==NULL)
			return;

		if(show)
		{
			Core::DataEntity::Pointer dE = 
				m_Processor->GetInputDataEntity(TransformationBoxProcessor::INPUT_0);
		
			mitk::BaseVtkMapper3D *mapper3D = NULL;
		
			Core::Widgets::MultiRenderWindowMITK* mitkRenderWindow =
			dynamic_cast<Core::Widgets::MultiRenderWindowMITK*> ( GetMultiRenderWindow() );
		
			mitk::BaseRenderer * renderer = mitk::BaseRenderer::GetInstance(
																		mitkRenderWindow->Get3D( )->GetVtkRenderWindow());
		
			mitk::DataTreeNode::Pointer node;
			Core::CastAnyProcessingData( GetRenderingTree()->GetNode(dE), node );
			if ( node.IsNull() )
				return;
		
			mapper3D =static_cast<mitk::BaseVtkMapper3D *> (node->GetMapper(mitk::BaseRenderer::Standard3D));
			if(mapper3D==NULL)
				return;
			
			vtkProp3D *prop3D = NULL;
			prop3D = static_cast<vtkProp3D *> ( mapper3D->GetVtkProp(renderer) );
		
			if((prop3D==NULL))
			{
				// Place the interactor initially. The actor is used to place and scale
				// the interactor. An observer isadded to the box widget to watch for
				// interaction events. This event is captured and used to set the
				// transformation matrix of the actor.
				m_boxWidget->SetProp3D(prop3D);
				m_boxWidget->PlaceWidget();
			}
			else {
				//prop3d not found..probably not a volume
				double bounds[6]={-50, 50, -50, 50, -50, 50};
				if(dE->IsImage() || dE->IsROI())
				{
					vtkSmartPointer<vtkImageData> imageData = NULL;
					dE->GetProcessingData(imageData);
					if(imageData!=NULL)
						imageData->GetBounds(bounds);
				}
				else if(dE->IsSurfaceMesh())
				{
					vtkSmartPointer<vtkPolyData> surfaceData = NULL;
					dE->GetProcessingData(surfaceData);
					if(surfaceData!=NULL)
						surfaceData->GetBounds(bounds);
				}
				else if(dE->IsVolumeMesh())
				{
					vtkSmartPointer<vtkUnstructuredGrid> surfaceData = NULL;
					dE->GetProcessingData(surfaceData);
					if(surfaceData!=NULL)
						surfaceData->GetBounds(bounds);					
				}
				else if(dE->IsPointSet())
				{
					vtkSmartPointer<vtkPointSet> pointData = NULL;
					dE->GetProcessingData(pointData);
					if(pointData!=NULL)
						pointData->GetBounds(bounds);					
				}
				m_boxWidget->PlaceWidget(bounds);
			}

			//normally the widget is shown with 'i' key...here we need to force it
			m_boxWidget->On();
		}
		else {
			m_boxWidget->Off();
		}
	}

	coreCatchExceptionsReportAndNoThrowMacro( TransformationBoxPanelWidget::ShowBox );
	
}
	
void TransformationBoxPanelWidget::OnBtnShowBox( wxCommandEvent& event )
{
	ShowBox(m_btnShowBox->GetValue());
	UpdateWidget();
}
	
void TransformationBoxPanelWidget::OnBtnReset( wxCommandEvent& event )
{
	ResetBoxWidget( );
	UpdateWidget();
}

void TransformationBoxPanelWidget::OnBtnApply( wxCommandEvent& event )
{
	try{
		if(m_boxWidget==NULL)
			return;
		
		vtkSmartPointer <vtkTransform> t = GetTransform(); 
		m_Processor->SetTransform(t);
		m_Processor->SetTimeStep(GetTimeStep());
		m_Processor->SetAllTimeSteps(m_chkAllTimeSteps->GetValue());
		m_Processor->SetAllChildren( m_chkAllChildren->GetValue() );
		m_Processor->Update();
		UpdateTransformTable();
		m_btnShowBox->SetValue(0);
		if(m_boxWidget!=NULL)
			m_boxWidget->Off();
		UpdateWidget();
	}
	coreCatchExceptionsReportAndNoThrowMacro( TransformationBoxPanelWidget::OnShowBox );

}	
	
	void TransformationBoxPanelWidget::UpdateTransformTable()
	{
		vtkSmartPointer <vtkTransform> t = vtkTransform::New();
		m_boxWidget->GetTransform(t);
		double pos[3], scale[3], rot[3];
		t->GetPosition(pos);
		t->GetScale(scale);
		t->GetOrientation(rot);

		m_transX->SetValue(wxString::Format("%.3f",pos[0]));
		m_transY->SetValue(wxString::Format("%.3f",pos[1]));
		m_transZ->SetValue(wxString::Format("%.3f",pos[2]));
		
		m_scaleX->SetValue(wxString::Format("%.3f",scale[0]));
		m_scaleY->SetValue(wxString::Format("%.3f",scale[1]));
		m_scaleZ->SetValue(wxString::Format("%.3f",scale[2]));
		
		m_rotX->SetValue(wxString::Format("%.3f",rot[0]));
		m_rotY->SetValue(wxString::Format("%.3f",rot[1]));
		m_rotZ->SetValue(wxString::Format("%.3f",rot[2]));		
	}
	
	vtkSmartPointer<vtkTransform> TransformationBoxPanelWidget::GetTransform()
	{
		
		vtkSmartPointer <vtkTransform> t = vtkTransform::New();		
		
		double trans[3];
		wxTextCtrl *transCtrl[3]={m_transX,m_transY,m_transZ};		
		for(int i=0;i<3;i++)
			if(!transCtrl[i]->GetValue().ToDouble(&trans[i]))
			{
				trans[i]=0;
				transCtrl[i]->SetValue("0");
			}

		double scale[3];
		wxTextCtrl *scaleCtrl[3]={m_scaleX,m_scaleY,m_scaleZ};		
		for(int i=0;i<3;i++)
			if(!scaleCtrl[i]->GetValue().ToDouble(&scale[i]))
			{
				scale[i]=0;
				scaleCtrl[i]->SetValue("0");
			}
		
		double rot[3];
		wxTextCtrl *rotCtrl[3]={m_rotX,m_rotY,m_rotZ};		
		for(int i=0;i<3;i++)
			if(!rotCtrl[i]->GetValue().ToDouble(&rot[i]))
			{
				rot[i]=0;
				rotCtrl[i]->SetValue("0");
			}
		
		t->Translate(trans);
		t->Scale(scale);
		t->RotateZ(rot[2]);	
		t->RotateX(rot[0]);
		t->RotateY(rot[1]);	
		return t;
	}

	
}   // namespace GenericSegmentationPlugin
