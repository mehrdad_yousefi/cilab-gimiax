// Copyright 2006 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "OtsuWidget.h"
#include "gblValidate.h"
#include "coreAssert.h"
#include "coreDataTreeHelper.h"
#include "coreProcessorInputWidget.h"

#include "gblWxButtonEventProxy.h"
#include "gblWxChoiceValueProxy.h"
#include <limits>
#include "gblWxBridgeLib.h"

OtsuWidget::OtsuWidget( wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style )
: OtsuWidgetUI(parent, id, pos, size, style)
{
	m_Processor = gsp::OtsuProcessor::New( );
	//this->m_OtsuProcessor->SetOutputDEH(this->m_OutputDEH);

	m_Processor->SetInputDataEntity( 0,
		Core::Runtime::Kernel::GetDataContainer()->GetDataEntityList( )->GetSelectedDataEntity());
}

void OtsuWidget::OnInit( )
{
	GetInputWidget( 0 )->Hide( );

	//-------------------------------------------------------
	// Observers to data
	m_Processor->GetOutputDataEntityHolder( 0 )->AddObserver( 
		this, 
		&OtsuWidget::OnModifiedOutputDataEntity );
}

void OtsuWidget::UpdateWidget()
{
}

bool OtsuWidget::Validate()
{
	return true;
}

void OtsuWidget::UpdateData()
{
	if( !this->Validate() )
		return;
}

void OtsuWidget::OnButtonOtsuSegmentation( wxCommandEvent &event )
{
	UpdateProcessor( true );
}

void OtsuWidget::OnModifiedOutputDataEntity()
{
	try{

		Core::DataEntity::Pointer inputDataEntity;
		inputDataEntity = m_Processor->GetInputDataEntity( 0 );

		// Add output to the data list and render it
		Core::DataTreeHelper::PublishOutput( 
			m_Processor->GetOutputDataEntityHolder( 0 ), 
			GetRenderingTree(),
			true,
			false);
		
		Core::DataTreeHelper::PublishOutput( 
			m_Processor->GetOutputDataEntityHolder( 1 ), 
			GetRenderingTree(),
			true,
			false );

	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"OtsuWidget::OnModifiedOutputDataEntity")

}

Core::BaseProcessor::Pointer OtsuWidget::GetProcessor()
{
	return m_Processor.GetPointer();
}
