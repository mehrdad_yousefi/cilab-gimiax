#ifndef OtsuWidget_H
#define OtsuWidget_H

// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "CILabNamespaceMacros.h"

#include "gblWxConnectorOfWidgetChangesToSlotFunction.h"

#include "OtsuWidgetUI.h"
#include "OtsuProcessor.h"

#include "coreRenderingTree.h"
#include "coreDataEntityListBrowser.h"
#include "coreProcessingWidget.h"

#define wxID_Generic_Otsu_Widget wxID( "wxID_Generic_Otsu_Widget" )

class OtsuWidget : 
	public OtsuWidgetUI,
	public Core::Widgets::ProcessingWidget
{
public:
	coreDefineBaseWindowFactory( OtsuWidget )
	OtsuWidget(
		wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

	//! Initialize this class after construction.
	void OnInit();
	//!
	Core::BaseProcessor::Pointer GetProcessor( );

	//! Update GUI from working data
	void UpdateWidget();
	//! Update working data from GUI
	void UpdateData();
	//! Validate GUI data
	bool Validate();
	//!
	virtual void OnButtonOtsuSegmentation(wxCommandEvent &event);

	//! 
	void OnModifiedOutputDataEntity( );

	//!
	void OnModifiedInput( int num ){}

	//!
	void OnModifiedOutput( int num ){}

private:
	//!
	gsp::OtsuProcessor::Pointer m_Processor;

};

#endif //OtsuWidget_H
