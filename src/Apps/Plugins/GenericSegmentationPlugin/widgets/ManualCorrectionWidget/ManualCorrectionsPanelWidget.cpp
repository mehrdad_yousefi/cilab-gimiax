/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "ManualCorrectionsPanelWidget.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"
// Core
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"

// Core
#include "coreDataTreeHelper.h"
#include "coreReportExceptionMacros.h"
#include "coreLandmarkSelectorWidget.h"

const int MAX_NUM_POINTS = 1;

#include "vtkPlane.h"

namespace GenericSegmentationPlugin
{


ManualCorrectionsPanelWidget::ManualCorrectionsPanelWidget(  wxWindow* parent, int id/*= wxID_ANY*/,
													   const wxPoint&  pos /*= wxDefaultPosition*/, 
													   const wxSize&  size /*= wxDefaultSize*/, 
													   long style/* = 0*/ )
:ManualCorrectionsPanelWidgetUI(parent, id,pos,size,style)
{
	m_Processor = ManualCorrectionsProcessor::New();

	SetName( "ManualCorrections Panel Widget" );
}

ManualCorrectionsPanelWidget::~ManualCorrectionsPanelWidget( )
{
	// We don't need to destroy anything because all the child windows 
	// of this wxWindow are destroyed automatically
}

void ManualCorrectionsPanelWidget::OnInit()
{

	//------------------------------------------------------
	// Observers to data
	// Observers to data
	m_Processor->GetOutputDataEntityHolder( ManualCorrectionsProcessor::OUTPUT_0 )->AddObserver( 
		this, 
		&ManualCorrectionsPanelWidget::OnModifiedOutputDataEntity );

	m_Processor->GetInputDataEntityHolder( ManualCorrectionsProcessor::INPUT_SURFACE_MESH )->AddObserver( 
		this, 
		&ManualCorrectionsPanelWidget::OnModifiedInputDataEntity,
		Core::DH_NEW_SUBJECT );

	m_Processor->GetInputDataEntityHolder( ManualCorrectionsProcessor::INPUT_POINTS )->AddObserver(this, 
		&ManualCorrectionsPanelWidget::OnModifiedCorrectionsPoints );

	GetProcessorOutputObserver( 0 )->SetHideInput( false );
	GetProcessorOutputObserver( 0 )->SetEnable( false );

	m_radius->SetValue(wxString::Format("%.2f",double(DEFAULTRADIUS)));
	m_ConnectingInteractor  = false;
	UpdateWidget();
}

void ManualCorrectionsPanelWidget::UpdateWidget()
{
	
	//const int maxNrDecimals = 1;

	m_btnUndo->Enable( m_Processor->GetUndoListSize() > 0 && 
		m_btnEnableInteraction->GetValue() );

	if ( m_Processor->GetUndoListSize() )
	{
		m_btnUndo->SetLabel( 
			wxString::Format( "Undo (%d)", m_Processor->GetUndoListSize() ) );
	}
	else
	{
		m_btnUndo->SetLabel( wxString::Format( "Undo" ) );
	}

	m_radioCorrectionType->SetSelection(m_Processor->GetCorrectionType());
	m_endTimeStep = m_Processor->GetInputDataNumberOfTimeSteps( 0 ) ;
	m_txtEndTimeStep->SetValue( wxString::Format("%.d", 
		m_endTimeStep ) );

	LayoutWidget();

	UpdateHelperWidget( );

}
void ManualCorrectionsPanelWidget::LayoutWidget()
{

	m_radioCorrectionType->Enable(m_btnEnableInteraction->GetValue());
	m_btnPropagate->Enable(m_btnEnableInteraction->GetValue());


	GetSizer( )->Show(m_btnPropagate, m_chkAdvanced->IsChecked( ), true );
	GetSizer( )->Show(m_labelEndTimeStep->GetContainingSizer(),m_chkAdvanced->IsChecked( ), true );
	GetSizer( )->Show(m_txtEndTimeStep->GetContainingSizer(),m_chkAdvanced->IsChecked( ), true );


	FitInside( );
}

void ManualCorrectionsPanelWidget::UpdateData()
{
	m_Processor->SetCorrectionType(
		(ManualCorrectionsProcessor::CorrectionType)
		m_radioCorrectionType->GetSelection());
	m_txtEndTimeStep->GetValue().ToULong(&m_endTimeStep);
}

void ManualCorrectionsPanelWidget::OnModifiedOutputDataEntity()
{
	try{



		if ( m_Processor->GetOutputDataEntity( 
			ManualCorrectionsProcessor::OUTPUT_0 ).IsNull() )
		{
			return;
		}


		// Publish output
		Core::DataEntityHelper::AddDataEntityToList( 
			m_Processor->GetOutputDataEntityHolder( 
				ManualCorrectionsProcessor::OUTPUT_0 ), 
			true );

		mitk::DataTreeNode::Pointer node;
		Core::CastAnyProcessingData(
			GetRenderingTree()->GetNode( m_Processor->GetOutputDataEntity( 
			ManualCorrectionsProcessor::OUTPUT_0 )  ),
			node);
		if ( node.IsNull() )
		{
			Core::CastAnyProcessingData(
				GetRenderingTree()->Add( m_Processor->GetOutputDataEntity( 
					ManualCorrectionsProcessor::OUTPUT_0 ), false, false ),
				node);
		}

		if ( node.IsNotNull() )
		{
			node->SetOpacity(1.0 );
		}

		// Show the node
		GetRenderingTree()->Show( m_Processor->GetOutputDataEntity( 
			ManualCorrectionsProcessor::OUTPUT_0 ), true );
		UpdateWidget();
	
	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"ManualCorrectionsPanelWidget::OnModifiedOutputDataEntity")

}

void ManualCorrectionsPanelWidget::UpdateHelperWidget()
{
}

bool ManualCorrectionsPanelWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = ManualCorrectionsPanelWidgetUI::Enable( enable );

	// If this panel widget is selected -> Update the widget
	ConnectInteractor(false);
	if ( enable )
	{
		UpdateWidget();
	}

	return bReturn;
}

void ManualCorrectionsPanelWidget::OnModifiedInputDataEntity()
{
	UpdateWidget();
}

Core::BaseProcessor::Pointer ManualCorrectionsPanelWidget::GetProcessor()
{
	return m_Processor.GetPointer( );
}


void ManualCorrectionsPanelWidget::OnModifiedCorrectionsPoints()
{
	if(m_ConnectingInteractor)
		return;

	Core::Widgets::LandmarkSelectorWidget *lsw = 
		ProcessingWidget::GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" );

	if(lsw==NULL)
		return;

	Core::PointInteractorPointSet* pointSetInteractor = 
		static_cast<Core::PointInteractorPointSet*> (lsw->GetPointInteractor( ).GetPointer( ));

	if ( pointSetInteractor ==NULL )
	{
		return;
	}

	if(GetRenderingTree())
	{
		mitk::DataTreeNode::Pointer nodeLandmarks;
		Core::CastAnyProcessingData(
			GetRenderingTree()->GetNode(m_Processor->GetInputDataEntity( ManualCorrectionsProcessor::INPUT_POINTS)),
			nodeLandmarks);
		if(nodeLandmarks.IsNotNull())
			nodeLandmarks->ReplaceProperty("includeInBoundingBox", mitk::BoolProperty::New(false));
	}


	if ( pointSetInteractor->IsCurrentActionAddPoint() )
	{

		if ( !m_Processor->SetStartingCorrectionPoint( 0 ) )
		{
			return;
		}

		// Show current output data
		GetRenderingTree()->Show( m_Processor->GetOutputDataEntity( ManualCorrectionsProcessor::OUTPUT_0 ), true );

		// Hide input data
		GetRenderingTree()->Show( m_Processor->GetInputDataEntity( 
			ManualCorrectionsProcessor::INPUT_SURFACE_MESH ), false );

		// Show final outputdata
		GetRenderingTree()->Show( m_Processor->GetOutputDataEntity( 
			ManualCorrectionsProcessor::INPUT_SURFACE_MESH  ), true );

		// Get the plane origin and orientation of the mouse click
		GetInteractionPlane( );

		if ( GetMultiRenderWindow( ) != NULL )
		{
			GetMultiRenderWindow( )->GetMetadata()->AddTag("EnableMouseWheel", false );
			GetMultiRenderWindow( )->GetMetadataHolder()->NotifyObservers();
		}

	}
	else
	{
		// CheckMovingPoint
		if(pointSetInteractor->IsCurrentActionMovePoint() )
		{
			////UpdateProcessor
			//m_Processor->GetNumericalParam()->m_PointsetRadius = 
			//	50 + m_PointInteractor->GetScalarValue() * 3;
			double radius = double(DEFAULTRADIUS);
			m_radius->GetValue().ToDouble(&radius);
			m_radius->SetValue(wxString::Format("%.2f",radius));
			m_Processor->GetNumericalParam()->m_PointsetRadius = radius;
			m_Processor->Update( );
		}
		else
		{
			//FinishMovingPoint
			if((m_Processor->GetCorrectionPoints()==NULL) ||
				(m_Processor->GetCorrectionPoints()->GetNumberOfPoints()==0))
				return;

			if ( GetMultiRenderWindow( ) != NULL ) 
			{
				GetMultiRenderWindow( )->GetMetadata()->AddTag("EnableMouseWheel", true );
				GetMultiRenderWindow( )->GetMetadataHolder()->NotifyObservers();
			}
		}
	}

}

void ManualCorrectionsPanelWidget::OnModifiedTimeStepper()
{
}

void ManualCorrectionsPanelWidget::OnBtnUndo( wxCommandEvent& event )
{
	m_Processor->Undo( );

	m_Processor->InitializeCorrectionPoints( );

}

void ManualCorrectionsPanelWidget::OnChkAdvanced( wxCommandEvent& event )
{
		UpdateWidget();
}

void ManualCorrectionsPanelWidget::OnBtnPropagate( wxCommandEvent& event )
{
	try
	{
	
		Core::DataEntity::Pointer dataEntity;
		dataEntity = m_Processor->GetInputDataEntity( 0 );
		Core::DataEntity::Pointer newDataEntity;
		newDataEntity = m_Processor->GetOutputDataEntity( 0 );

		Core::vtkPolyDataPtr poly;
		Core::DataEntityHelper::GetProcessingData(m_Processor->GetOutputDataEntityHolder( 0 ),
			poly,
			m_Processor->GetTimeStep());



		// first copy for the time steps at the beginning if the given end step
		// is lower than the max time step
		if ( m_endTimeStep < m_Processor->GetTimeStep( ))
		{
			for (int it = 0; it < m_endTimeStep; it++)
			{
				Core::vtkPolyDataPtr polyTemp;
				Core::DataEntityHelper::GetProcessingData(
					m_Processor->GetInputDataEntityHolder( 0 ),
					polyTemp,
					it);
				polyTemp->DeepCopy(poly);
				dataEntity->SetTimeStep( polyTemp, it  );
			}
			m_endTimeStep = m_Processor->GetInputDataNumberOfTimeSteps( 0 ) ;
		}

		// then copy the time steps from current one till the end
		for ( unsigned i = m_Processor->GetTimeStep( ) +1 ; 
			i < m_endTimeStep ; 
			i++ )
		{
			Core::vtkPolyDataPtr polyTemp;
			Core::DataEntityHelper::GetProcessingData(
				m_Processor->GetInputDataEntityHolder( 0 ),
				polyTemp,
				i);
			polyTemp->DeepCopy(poly);
			dataEntity->SetTimeStep( polyTemp, i  );
		}


		m_Processor->GetInputDataEntityHolder(0)->NotifyObservers();
	}
	coreCatchExceptionsReportAndNoThrowMacro( ManualCorrectionsPanelWidget::OnBtnPropagate );

}

void ManualCorrectionsPanelWidget::OnEnableInteractor( wxCommandEvent& event ) 
{
	if ( m_btnEnableInteraction->GetValue() )
	{

		ConnectInteractor(true);
		// ResetInteraction
		// Initialize correction points to the number of time steps
		m_Processor->InitializeCorrectionPoints( );
		m_Processor->ClearUndoList( );

		m_btnEnableInteraction->SetLabel("Disable Interaction");


	}
	else
	{
		//m_PointInteractor->DisconnectFromDataTreeNode(  );
		ConnectInteractor(false);
		m_Processor->ClearUndoList();

		// Remove current output from rendering tree
		GetRenderingTree()->Remove( m_Processor->GetOutputDataEntity( 
			ManualCorrectionsProcessor::OUTPUT_0), false );

		//GetRenderingTree()->Remove(m_Processor->GetCorrectionPointsHolder()->GetSubject(),false);
		Core::Runtime::Kernel::GetDataContainer()->GetDataEntityList( )->Remove( 
			m_Processor->GetInputDataEntity( ManualCorrectionsProcessor::INPUT_POINTS ) );

		/*	GetHelperWidget()->SetInfo( 
		Core::Widgets::HELPER_INFO_ONLY_TEXT, "" );*/
		m_btnEnableInteraction->SetLabel("Enable Interaction");

	}
	UpdateWidget();
}

void ManualCorrectionsPanelWidget::GetInteractionPlane()
{
	Core::PointInteractorPointSet* pointSetInteractor;

	Core::Widgets::LandmarkSelectorWidget *lsw = 
		ProcessingWidget::GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" );
	if(lsw==NULL)
		return;

	pointSetInteractor = static_cast<Core::PointInteractorPointSet*> (lsw->
		GetPointInteractor( ).GetPointer( ));

	mitk::Vector3D normal;
	mitk::Point3D origin;
	bool success = pointSetInteractor->GetLastClickedPlane( normal, origin );
	if ( success )
	{
		vtkPlane *plane = vtkPlane::New();
		plane->SetOrigin( origin[ 0 ], origin[ 1 ], origin[ 2 ] );
		plane->SetNormal( normal[ 0 ], normal[ 1 ], normal[ 2 ] );
		m_Processor->SetInteractionPlane( plane );
		plane->Delete();
	}
}

void ManualCorrectionsPanelWidget::OnRadioBoxSelection( wxCommandEvent& event )
{
	m_Processor->SetCorrectionType(
		(ManualCorrectionsProcessor::CorrectionType)m_radioCorrectionType->GetSelection());
}

//void gsp::ManualCorrectionsPanelWidget::CreateInteractor()
//{
//	//------------------------------------------------------
//	//// Interactor. Use a empty holder -> Root
//	//Core::DataEntityHolder::Pointer dataEntity = Core::DataEntityHolder::New();
//	//m_PointInteractor = Core::PointInteractorPointSet::New( 
//	//	GetRenderingTree(),
//	//	m_Processor->GetCorrectionPointsHolder( ),
//	//	dataEntity );
//
//	//m_PointInteractor->SetMaxPoints( MAX_NUM_POINTS );
//	//m_PointInteractor->SetEnableContinuousMoveEvent( true );
//	//m_PointInteractor->SetIncrementalAdd( false );
//	//m_PointInteractor->SetClickAndDragLeftMouseButton( true );
//
//}

void ManualCorrectionsPanelWidget::ConnectInteractor(bool bConnect)
{

	Core::Widgets::LandmarkSelectorWidget *lsw = 
		ProcessingWidget::GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" );
	
	if(lsw==NULL)
		return;

	if(bConnect)
	{
		bool bInputIsOk = false;
		bInputIsOk = 
			Core::DataEntityHelper::IsSurfaceMesh( m_Processor->GetInputDataEntityHolder( ManualCorrectionsProcessor::INPUT_SURFACE_MESH ) );
		if ( !bInputIsOk )
		{
			return;
		}
		lsw->SetAllowedInputDataTypes( Core::SurfaceMeshTypeId );
		lsw->SetInteractorType( Core::PointInteractor::POINT_SET );
		lsw->SetInputDataEntity( m_Processor->GetInputDataEntity( ManualCorrectionsProcessor::INPUT_SURFACE_MESH ) );
		lsw->SetDataName( "Correction Landmarks" );
		m_ConnectingInteractor = true;
		lsw->StartInteractor();	

		Core::PointInteractorPointSet* pointSetInteractor;
		pointSetInteractor = static_cast<Core::PointInteractorPointSet*> (
			lsw->GetPointInteractor( ).GetPointer( ));

		pointSetInteractor->SetMaxPoints( 1 );
		pointSetInteractor->SetEnableContinuousMoveEvent( true );
		pointSetInteractor->SetIncrementalAdd( false );
		pointSetInteractor->SetClickAndDragLeftMouseButton( true );

		m_Processor->GetInputDataEntityHolder( ManualCorrectionsProcessor::INPUT_POINTS )->SetSubject( 
			lsw->GetPointInteractor( )->GetSelectedPointsDataEntity( ) );

		// Update the current time step
		m_Processor->SetTimeStep( GetTimeStep() );
		m_ConnectingInteractor = false;
	}
	else
	{
		lsw->StopInteraction();
		lsw->SetDefaultAllowedInputDataTypes( );
	}
}
}   // namespace GenericSegmentationPlugin
