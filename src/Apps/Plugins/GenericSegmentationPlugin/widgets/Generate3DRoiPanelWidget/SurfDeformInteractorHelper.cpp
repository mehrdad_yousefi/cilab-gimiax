/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// CoreLib
#include "SurfDeformInteractorHelper.h"
#include "coreAssert.h"
#include "coreException.h"
#include "coreEnvironment.h"
#include "coreLogger.h"
#include "coreKernel.h"
#include "coreReportExceptionMacros.h"
#include "coreDataTreeHelper.h"
#include "coreDataEntityHelper.h"
#include "coreVTKPolyDataHolder.h"
#include "coreVTKImageDataRenDataBuilder.h"
#include "coreRenderingTreeMITK.h"

// Mitk
#include "mitkRenderingManager.h"
#include "mitkSmartPointerProperty.h"
#include "mitkGlobalInteraction.h"
#include "mitkInteractionConst.h"
#include "mitkDataTreeNode.h"
#include "mitkDataTreeHelper.h"
#include "mitkVtkResliceInterpolationProperty.h"
#include "mitkDataStorage.h"
#include "mitkProperties.h"

#include "vtkSmartPointer.h"
#include "vtkImageMathematics.h"
#include "vtkPointData.h"
#include "blMITKUtils.h"

/**
 */
ms::SurfDeformInteractorHelper::SurfDeformInteractorHelper(
						Core::RenderingTree::Pointer renderingTree,
						Core::DataEntityHolder::Pointer selectedMesh,
						Core::DataEntityHolder::Pointer selectionPoint,
						Core::DataEntityHolder::Pointer selectedImage
						)
{
	try{
		SetSelectedSphereHolder( selectedMesh );
		SetImageDataHolder(selectedImage);
		SetPointDataHolder(selectionPoint);
		SetRenderingTree( renderingTree );
		m_bCreateSphereMode = false;
		m_bOrthoFactorEnabled = true;
	}
		coreCatchExceptionsAddTraceAndThrowMacro(
			"SurfDeformInteractorInterface::SurfDeformInteractorInterface");

}

ms::SurfDeformInteractorHelper::~SurfDeformInteractorHelper()
{
	DisconnectFromDataTreeNode();
}

ms::SurfDeformInteractorHelper::SurfDeformInteractorHelper()
{
	//if ( m_selectedSphereHolder.IsNotNull() )
	//{
	//	m_selectedSphereHolder->RemoveObserver<SurfDeformInteractorInterface>(
	//		this, 
	//		&Self::OnSphereModified);
	//}

	DisconnectFromDataTreeNode();

	DestroyInteractor();
	m_bCreateSphereMode = false;
	m_bOrthoFactorEnabled = true;
}

mitk::SurfDeformationInteractor* ms::SurfDeformInteractorHelper::GetInternalInteractor( ) 
{
	return m_surfaceInteractor;
}

void ms::SurfDeformInteractorHelper::ConnectToDataTreeNode( )
{
	if ( !IsConnectedToRenderingTree (GetSelectedSphereHolder()->GetSubject()))
		ConnectNodeToTree();

	CreateInteractor();

	ConnectInteractors();
}

void ms::SurfDeformInteractorHelper::DisconnectFromDataTreeNode()
{
	try
	{
		DisconnectInteractors();

		//Commented out. We don't want to disconnect the countour from the rendering node when the interactor is distructed. MB
		//DisconnectNodeFromTree();
		DestroyInteractor();
	}
	coreCatchExceptionsReportAndNoThrowMacro(
		"SurfDeformInteractorInterface::DisconnectFromDataTreeNode()");
}

void ms::SurfDeformInteractorHelper::ConnectNodeToTree()
{
	if (GetImageDataEntity()->GetType() != Core::ImageTypeId)
	{
		throw Core::Exceptions::Exception( 
			"SurfDeformInteractorInterface", 
			"Input Data should be an image" );
	}

	//! add a check before doing the connections of the interactor
	if (GetSurfaceDataEntity().IsNull() ) return;

	// will be set by the interactor class
	m_renderingTree->Add( 
		GetSurfaceDataEntity( ), 
		false, 
		false );
}

void ms::SurfDeformInteractorHelper::DisconnectNodeFromTree()
{
	if ( GetSurfaceDataEntity().IsNotNull() )
			m_renderingTree->Remove( 
				GetSurfaceDataEntity( ),
				false );

}

void ms::SurfDeformInteractorHelper::ConnectInteractors()
{
	// MITK checks if the interactor is in the list. It will not be added twice
	if( GetInternalInteractor() )
		mitk::GlobalInteraction::GetInstance()->AddInteractor( GetInternalInteractor() );
	
}

void ms::SurfDeformInteractorHelper::DisconnectInteractors()
{
	if( GetInternalInteractor() )
	{
		mitk::GlobalInteraction::GetInstance()->RemoveInteractor( GetInternalInteractor() );
	}
}

void ms::SurfDeformInteractorHelper::CreateInteractor()
{
	if ( m_surfaceInteractor.IsNull() )
	{
		mitk::DataTreeNode::Pointer nodeSphere  = GetSurfaceNode();
		mitk::DataTreeNode::Pointer nodePointSet = GetPointNode();
		nodeSphere->ReplaceProperty( "pickable", mitk::BoolProperty::New(true) );
		m_surfaceInteractor = mitk::SurfDeformationInteractor::New(
			"SurfDeformationInteractor", nodeSphere, nodePointSet );
	}
	m_surfaceInteractor->SetCreateSphereMode(m_bCreateSphereMode);
}

bool ms::SurfDeformInteractorHelper::IsConnectedToRenderingTree(Core::DataEntity::Pointer dataEntity)
{
	mitk::DataTreeIteratorClone	itFound;
	bool bDataIsInRenderingTree = false;
	
	if( GetRenderingTree().IsNull() )
		return false;

	bDataIsInRenderingTree = GetRenderingTree()->IsDataEntityRendered( dataEntity );

	return bDataIsInRenderingTree;
}

void ms::SurfDeformInteractorHelper::DestroyInteractor()
{
	//m_toolManager = NULL ;
	m_surfaceInteractor = NULL;
}


Core::DataEntity::Pointer ms::SurfDeformInteractorHelper::GetSurfaceDataEntity()
{
	if ( m_surfaceHolder.IsNull( ) )
	{
		return NULL;
	}

	return m_surfaceHolder->GetSubject();
}

Core::DataEntity::Pointer ms::SurfDeformInteractorHelper::GetImageDataEntity()
{
	return m_imageHolder->GetSubject();
}

Core::DataEntity::Pointer ms::SurfDeformInteractorHelper::GetPointDataEntity()
{
	return m_pointHolder->GetSubject();
}


mitk::DataTreeNode::Pointer ms::SurfDeformInteractorHelper::GetSurfaceNode()
{
	mitk::DataTreeNode::Pointer node;
	boost::any anyData = GetRenderingTree()->GetNode( GetSurfaceDataEntity( ) );
	Core::CastAnyProcessingData( anyData, node );
	return node;
}


mitk::DataTreeNode::Pointer ms::SurfDeformInteractorHelper::GetPointNode()
{
	mitk::DataTreeNode::Pointer node = NULL;
	
	boost::any anyData = GetRenderingTree()->GetNode( GetPointDataEntity() );
	Core::CastAnyProcessingData( anyData, node );
	return node;
}


mitk::DataTreeNode::Pointer ms::SurfDeformInteractorHelper::GetImageNode()
{
	mitk::DataTreeNode::Pointer node = NULL;
	boost::any anyData = GetRenderingTree()->GetNode( GetImageDataEntity( ) );
	Core::CastAnyProcessingData( anyData, node );
	return node;
}

mitk::Surface::Pointer ms::SurfDeformInteractorHelper::GetSurfaceRenderingData()
{
	if ( GetSurfaceDataEntity( ).IsNull() )
	{
		throw Core::Exceptions::Exception( 
			"SurfDeformInteractorInterface", 
			"Input Sphere is NULL" );
	}
	mitk::BaseData::Pointer renderingData;
	boost::any anyData = GetSurfaceDataEntity( )->GetRenderingData( "MITK" );
	Core::CastAnyProcessingData( anyData, renderingData );
	mitk::Surface* sphere = NULL;
	if ( renderingData.IsNull() )
	{
		throw Core::Exceptions::Exception( 
			"SurfDeformInteractorInterface", 
			"Input Sphere rendering data is NULL" );
	}
	sphere = dynamic_cast<mitk::Surface*>(renderingData.GetPointer()); 
	if ( sphere == NULL )
	{
		throw Core::Exceptions::Exception( 
			"SurfDeformInteractorInterface", 
			"Input sphere rendering data is not correct" );
	}
	return sphere;
}

Core::RenderingTree::Pointer ms::SurfDeformInteractorHelper::GetRenderingTree() const
{
	return m_renderingTree;
}

void ms::SurfDeformInteractorHelper::SetRenderingTree( Core::RenderingTree::Pointer val )
{
	m_renderingTree = val;
}

Core::DataEntityHolder::Pointer ms::SurfDeformInteractorHelper::GetSelectedSphereHolder() const
{
	return m_surfaceHolder;
}

void ms::SurfDeformInteractorHelper::SetSelectedSphereHolder( Core::DataEntityHolder::Pointer val )
{
	//if ( m_selectedSphereHolder.IsNotNull() )
	//{
	//	m_selectedSphereHolder->RemoveObserver<SurfDeformInteractorInterface>(
	//		this, 
	//		&Self::OnSphereModified );
	//}

	// Observers to rendering data
	mitk::Surface::Pointer mitkData = NULL;
	try
	{
		mitkData = GetSurfaceRenderingData( );
	}
	catch ( ... )
	{
	}

	m_surfaceHolder = val;

	//// Observers to processing data
	//m_selectedSphereHolder->AddObserver<SurfDeformInteractorInterface>(
	//	this, 
	//	&Self::OnSphereModified );
}


//void ms::SurfDeformInteractorInterface::OnSphereModified( )
//{
//	std::cout << "on sphere modified....."<<endl;
//	if (GetSelectedSphereHolder().IsNull() ) return;
//	if (GetSelectedSphereHolder()->GetSubject().IsNull() ) return;
//    
//	//Core::DataEntityHelper::AddDataEntityToList( GetSelectedSphereHolder() , false);
//}

Core::DataEntityHolder::Pointer ms::SurfDeformInteractorHelper::GetImageDataHolder() const
{
	return m_imageHolder;
}

void ms::SurfDeformInteractorHelper::SetImageDataHolder( Core::DataEntityHolder::Pointer val )
{
	m_imageHolder = val;
}


void ms::SurfDeformInteractorHelper::SetPointDataHolder(Core::DataEntityHolder::Pointer val)
{
	m_pointHolder = val;
}