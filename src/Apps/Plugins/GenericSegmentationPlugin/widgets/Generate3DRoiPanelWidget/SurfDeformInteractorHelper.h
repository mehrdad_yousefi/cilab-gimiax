/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef msSurfDeformInteractorInterface_H
#define msSurfDeformInteractorInterface_H


#include "coreRenderingTree.h"
#include "coreObject.h"
#include "coreDataEntityHolder.h"
#include "coreCommonDataTypes.h"

// MITK
#include "mitkDataTree.h"
#include "mitkImage.h"
#include "msMitkSurfaceDeformationInteractor3D.h"

//Core
#include "coreDataEntity.h"
#include "coreDataHolder.h"



namespace ms{

/**
\brief 
\ingroup GenericSegmentationPlugin
\helper for msMitkSurfDefomationInteractor3D 
\(that is a classical mitk style interactor)
\This is a standard Gimias interface to an mitk type Interactor
\author Luigi Carotenuto
\date 21-08-11
*/
class SurfDeformInteractorHelper 
	: public Core::SmartPointerObject
{

// PUBLIC OPERATIONS
public:

	//!
	coreDeclareSmartPointerClassMacro4Param(
		SurfDeformInteractorHelper,
		Core::SmartPointerObject,
		Core::RenderingTree::Pointer,
		Core::DataEntityHolder::Pointer,
		Core::DataEntityHolder::Pointer,
		Core::DataEntityHolder::Pointer );

	//! Connect the interactor to the selectedData tree node
	void ConnectToDataTreeNode();

	//! Disconnect the interactor from the rendering tree
	void DisconnectFromDataTreeNode();

	//!
	Core::RenderingTree::Pointer GetRenderingTree() const;

	//!
	void SetRenderingTree(Core::RenderingTree::Pointer val);

	//!
	Core::DataEntityHolder::Pointer GetSelectedSphereHolder() const;

	//!
	void SetSelectedSphereHolder(Core::DataEntityHolder::Pointer val);

	//!
	Core::DataEntityHolder::Pointer GetImageDataHolder() const;

	//!
	void SetImageDataHolder(Core::DataEntityHolder::Pointer val);

	//!
	void SetPointDataHolder(Core::DataEntityHolder::Pointer val);

	bool IsConnectedToRenderingTree(Core::DataEntity::Pointer dataEntity);

	//!default off
	void SetCreateSphereMode(bool val){m_bCreateSphereMode = val;};

	bool GetCreateSphereMode(){ if(m_surfaceInteractor.IsNotNull())
									m_bCreateSphereMode = m_surfaceInteractor->GetCreateSphereMode();
								return m_bCreateSphereMode;};

	void SetCallback(SurfDeformationCallback *callback){
		if(m_surfaceInteractor.IsNotNull())
			m_surfaceInteractor->SetCallback(callback);};


	vtkPolyData * GetPreviousPolyData() const { if(m_surfaceInteractor.IsNotNull()) 
													return m_surfaceInteractor->GetPreviousPolyData();
												else
													return NULL;};

	double GetSphereRes() const {if(m_surfaceInteractor.IsNotNull())
									return m_surfaceInteractor->GetSphereRes();
								 else return 0;}


	void SetOrthoFactorEnabled(bool val){m_bOrthoFactorEnabled=val;};

	virtual mitk::SurfDeformationInteractor* GetInternalInteractor( );

// PRIVATE OPERATIONS
private:
	//! 
	SurfDeformInteractorHelper(
		Core::RenderingTree::Pointer renderingTree,
		Core::DataEntityHolder::Pointer selectedMesh,
		Core::DataEntityHolder::Pointer selectionPoint,
		Core::DataEntityHolder::Pointer selectedImage );

	virtual ~SurfDeformInteractorHelper();


	//! Connect to the m_renderingTree
	void ConnectNodeToTree();

	//! Disconnect to the m_renderingTree
	void DisconnectNodeFromTree();

	//! Connect the interactors to the global instance
	void ConnectInteractors();

	//! Disconnect all interactors to the global instance
	void DisconnectInteractors();

	//! Redefined
	void CreateInteractor();

	//! Redefined
	void DestroyInteractor();

	//!
	Core::DataEntity::Pointer GetSurfaceDataEntity();

	//!
	Core::DataEntity::Pointer GetImageDataEntity();

	//!
	Core::DataEntity::Pointer GetPointDataEntity();

	//!
	mitk::DataTreeNode::Pointer GetSurfaceNode();

	//!
	mitk::Surface::Pointer GetSurfaceRenderingData();

	//!
	mitk::DataTreeNode::Pointer GetImageNode();

	//!
	mitk::DataTreeNode::Pointer GetPointNode();

// ATTRIBUTES
private:
	SurfDeformInteractorHelper();
	//! Rendering data tree.
	Core::RenderingTree::Pointer m_renderingTree;
	
	//! create an empty point set dentity
	void CreateNewPointSetDataEntity();

	//! Current selected mesh
	Core::DataEntityHolder::Pointer m_surfaceHolder;

	//! Current Selected image (if any)
	Core::DataEntityHolder::Pointer m_imageHolder;

	//! Selected point
	Core::DataEntityHolder::Pointer m_pointHolder;

	//! the internal interactor
	mitk::SurfDeformationInteractor::Pointer m_surfaceInteractor;

	//! set the interactor to create sphere mode
	bool m_bCreateSphereMode;

	//! enable the not simmetric deformation along orthognal directions
	bool m_bOrthoFactorEnabled;

};

} // ms

#endif //msSurfDeformInteractorInterface
