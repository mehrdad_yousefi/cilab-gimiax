/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "GenerateSpherePanelWidget.h"

// Core
#include "coreVTKPolyDataHolder.h"
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"
#include "coreRenderingTree.h"
#include "coreDataEntityListBrowser.h"
#include "coreReportExceptionMacros.h"
#include "corePluginTab.h"
#include "coreDataEntity.h"
#include "coreDataEntityList.h"
#include "coreDataEntityList.txx"
#include "coreProcessorInputWidget.h"
#include "coreVTKImageDataHolder.h"

// STD
#include <limits>
#include <cmath>
#include <algorithm>
#include <sstream>

// Core
#include "coreDataTreeHelper.h"

#include "mitkLookupTableProperty.h"
#include "vtkPointData.h"
#include "vtkStringArray.h"
#include "vtkSphereSource.h"


using namespace Core::Widgets;

// Event the widget
BEGIN_EVENT_TABLE(GenerateSpherePanelWidget,GenerateSpherePanelWidgetUI)
END_EVENT_TABLE()

GenerateSpherePanelWidget::Processor::Processor( )
{
	SetNumberOfInputs( NUMBER_OF_INPUTS );

	GetInputPort( INPUT_POINT )->SetName( "Input Point" );
	GetInputPort( INPUT_POINT )->SetDataEntityType( Core::PointSetTypeId );

	GetInputPort( INPUT_MESH )->SetName( "Mesh to be modified" );
	GetInputPort( INPUT_MESH )->SetDataEntityType( Core::SurfaceMeshTypeId );
}

GenerateSpherePanelWidget::GenerateSpherePanelWidget(
							  wxWindow* parent, 
							  int id,
							  const wxPoint& pos, 
							  const wxSize& size, 
							  long style) :
	GenerateSpherePanelWidgetUI(parent, id, pos, size, style)
{
	try 
	{
		m_processor = Processor::New();		

		ClearEditBoxes();
	}
	coreCatchExceptionsReportAndNoThrowMacro( 
		"GenerateSpherePanelWidget::GenerateSpherePanelWidget" );

}

GenerateSpherePanelWidget::~GenerateSpherePanelWidget( ) 
{
}

void GenerateSpherePanelWidget::OnInit()
{
	GetInputWidget(Processor::INPUT_POINT)->SetAutomaticSelection(false);
	GetInputWidget(Processor::INPUT_POINT)->SetDefaultDataEntityFlag(false);
	GetInputWidget(Processor::INPUT_POINT)->Hide();

	GetInputWidget(Processor::INPUT_MESH)->SetAutomaticSelection(false);
	GetInputWidget(Processor::INPUT_MESH)->SetDefaultDataEntityFlag(false);
	GetInputWidget(Processor::INPUT_MESH)->Hide( );
}


void GenerateSpherePanelWidget::OnModifiedInput( int num )
{
	if((!IsShown()) || (!/*IsEnabled()*/IsThisEnabled()))
		return;

	if ( num == Processor::INPUT_MESH )
	{
		ClearEditBoxes();
	}
}

void GenerateSpherePanelWidget::ClearEditBoxes()
{
	m_edtSphereRes->SetValue("");
}

void GenerateSpherePanelWidget::OnRenderingDataModified()
{
	if((!IsShown()) || (!/*IsEnabled()*/IsThisEnabled()))
		return;

	if(m_processor->GetInputDataEntity(Processor::INPUT_MESH).IsNotNull())
		GetRenderingTree()->Show(
			m_processor->GetInputDataEntity(Processor::INPUT_MESH),true,true);

	
	UpdateWidget();
}


void GenerateSpherePanelWidget::OnParametersModified()
{
	if(m_interactor.IsNotNull())
	{
		m_edtSphereRes->SetValue( wxString::Format("%.2f",m_interactor->GetSphereRes()) );
	}
}

void GenerateSpherePanelWidget::UpdateWidget() 
{
	if(m_btnEnableInteraction->GetValue())
		m_btnEnableInteraction->SetLabel("Disable Interaction");
	else
		m_btnEnableInteraction->SetLabel("Enable Interaction");
}

//!
Core::BaseProcessor::Pointer GenerateSpherePanelWidget::GetProcessor()
{
	return m_processor.GetPointer( );
}

//!
void GenerateSpherePanelWidget::StartInteractor()
{
	if(m_btnEnableInteraction->GetValue() == false)
		return;
	
	m_workingTimeStep = GetTimeStep();

	CreateSphereDataEntity();

	CreateSelectionPointDataEntity();

	// Create interactor
	if(m_interactor.IsNull())
		m_interactor = ms::SurfDeformInteractorHelper::New( 
			GetRenderingTree(),
			m_processor->GetInputDataEntityHolder(Processor::INPUT_MESH),
			m_processor->GetInputDataEntityHolder(Processor::INPUT_POINT),
			Core::DataEntityHolder::New( ) );

	m_interactor->SetCreateSphereMode(true);
	m_interactor->SetOrthoFactorEnabled(false);
	try
	{
		m_interactor->ConnectToDataTreeNode();
		m_interactor->SetCallback(this);
		OnParametersModified();

	}coreCatchExceptionsReportAndNoThrowMacro( GenerateSpherePanelWidget::StartInteraction );
	
	UpdateWidget( );
}

//!
void GenerateSpherePanelWidget::StopInteraction()
{
	ClearEditBoxes();
	if ( m_interactor.IsNull() )
	{
		return;
	}
	
	Core::DataEntity::Pointer pointDE = 		
		m_processor->GetInputDataEntity(Processor::INPUT_POINT);
	if(pointDE.IsNotNull())
	{
		GetRenderingTree()->Remove(pointDE);
		m_processor->GetInputDataEntityHolder(Processor::INPUT_POINT)->SetSubject(NULL);

		//Remove the point from the list datalist also:
		if(GetListBrowser()->GetDataEntityList()->IsInTheList(pointDE->GetId()))
			GetListBrowser()->GetDataEntityList()->Remove(pointDE);
	}

	m_interactor->DisconnectFromDataTreeNode();
	
	m_interactor = NULL;
}

//!
bool GenerateSpherePanelWidget::IsSelectionEnabled( )
{
	return false;
}


void Core::Widgets::GenerateSpherePanelWidget::OnEnableInteraction( wxCommandEvent& event )
{
	EnableInteraction( m_btnEnableInteraction->GetValue() == true );
}


void Core::Widgets::GenerateSpherePanelWidget::EnableInteraction( bool bEnable )
{
	if(bEnable)
		StartInteractor();
	else
		StopInteraction();
	UpdateWidget();
}


void Core::Widgets::GenerateSpherePanelWidget::SetRenderingTree( RenderingTree::Pointer tree )
{
	Core::BaseWindow::SetRenderingTree( tree );
	if(m_interactor.IsNotNull())
		m_interactor->SetRenderingTree( tree );
}



bool GenerateSpherePanelWidget::Enable( bool enable ) 
{
	bool bReturn = GenerateSpherePanelWidgetUI::Enable( enable );
	if ( !enable ) 
	{
		if(m_interactor.IsNotNull())
			StopInteraction( );
	}

	return bReturn;
}


void GenerateSpherePanelWidget::CreateSelectionPointDataEntity()
{
	Core::DataEntity::Pointer meshDE = m_processor->GetInputDataEntity( Processor::INPUT_MESH);

	if(meshDE.IsNull())
		return;

	int nTimeSteps = meshDE->GetNumberOfTimeSteps();

	Core::DataEntity::Pointer selectionPointDE = Core::DataEntity::New(Core::PointSetTypeId);
	selectionPointDE->GetMetadata()->SetName( "Correction Point" );
	selectionPointDE->Resize( nTimeSteps, typeid( Core::vtkPolyDataPtr ) );

	//Just put a point somewhere..why not at the origin (0,0,0)
	Core::vtkPolyDataPtr pointSet = vtkPolyData::New();
	vtkSmartPointer<vtkPoints> points = vtkPoints::New();
	points->Allocate(1);
	points->InsertPoint(0,0,0,0);
	pointSet->SetPoints(points);
	pointSet->Update();

	int timeStep = GetTimeStep();
	for(int i=0; i<nTimeSteps; i++)
	{
		if(i==timeStep)
			selectionPointDE->SetTimeStep(pointSet,i);
		else
			selectionPointDE->SetTimeStep(vtkPolyDataPtr::New(),i);
	}

	vtkSmartPointer<vtkStringArray> stringData;
	stringData = vtkSmartPointer<vtkStringArray>::New();
	stringData->SetName( "LandmarksName" );
	stringData->SetNumberOfTuples(1);
	stringData->InsertValue(0, "Point");

	pointSet->GetPointData()->AddArray( stringData );
	selectionPointDE->SetFather(m_processor->GetInputDataEntity(Processor::INPUT_MESH));
	Core::DataEntityHelper::AddDataEntityToList(selectionPointDE);
	GetRenderingTree()->Add(selectionPointDE,false,false);

	m_processor->GetInputDataEntityHolder(Processor::INPUT_POINT)->SetSubject(selectionPointDE);
}


void GenerateSpherePanelWidget::CreateSphereDataEntity()
{
	int nTimeSteps = 1;

	Core::DataEntity::Pointer sphereDE = Core::DataEntity::New(Core::SurfaceMeshTypeId);
	sphereDE->GetMetadata()->SetName( "Sphere" );
	sphereDE->Resize( nTimeSteps, typeid( Core::vtkPolyDataPtr ) );
	sphereDE->Modified();

	double center[3] = {0, 0, 0};
	double width = 5.0;

	vtkSmartPointer<vtkSphereSource> source = vtkSphereSource::New();
	source->SetCenter(center[0],center[1],center[2]);
	source->SetRadius(width/10.0);
	source->Update();

	vtkSmartPointer<vtkPolyData> sphere = vtkPolyData::New();
	sphere->DeepCopy(source->GetOutput());

	sphereDE->SetTimeStep(sphere);

	m_processor->GetInputDataEntityHolder(Processor::INPUT_MESH)->SetSubject(sphereDE);
	
	Core::DataEntityHelper::AddDataEntityToList(sphereDE,false);
	GetRenderingTree()->Add(sphereDE, false, false); //true, false);
}
