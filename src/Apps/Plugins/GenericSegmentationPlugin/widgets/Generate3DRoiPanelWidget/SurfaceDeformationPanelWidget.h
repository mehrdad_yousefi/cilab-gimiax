/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _SurfaceDeformationPanelWidget_H
#define _SurfaceDeformationPanelWidget_H

#include "SurfaceDeformationPanelWidgetUI.h"

#include "SurfDeformInteractorHelper.h"
#include "coreProcessingWidget.h"

// CoreLib
#include "coreRenderingTree.h"
#include "coreVTKPolyDataHolder.h"

#include <vector>
#include <map>

#include "blMITKUtils.h"
#include "wxMitkScalarsArrayControl.h"
#include "wxMitkSelectableGLWidget.h"

struct UndoListType
{
	Core::vtkPolyDataPtr prevPolyData;
	bool prevSphereModeState;
};


namespace Core{
namespace Widgets {


/**
\ingroup GenericSegmentationPlugin
\author Luigi Carotenuto
\date 23 08 2011
*/
class SurfaceDeformationPanelWidget : 
	public SurfaceDeformationPanelWidgetUI,
	public Core::Widgets::ProcessingWidget,
	public SurfDeformationCallback
{

	// OPERATIONS
public:
	coreDefineBaseWindowFactory( SurfaceDeformationPanelWidget )

	//!
	cilabDeclareExceptionMacro( Exception, std::exception );

	SurfaceDeformationPanelWidget(
		wxWindow* parent, 
		int id = wxID_ANY,
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=0);

	//!
	~SurfaceDeformationPanelWidget( );

	//!
	void OnInit();

	//!
	bool Enable( bool enable /*= true */ );

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

	//!
	void StartInteractor( );

	//!
	void StopInteraction( );

	//!
	bool IsSelectionEnabled( );

	//!
	void FocusChange();
private:
    wxDECLARE_EVENT_TABLE();

	void EnableInteraction( bool bEnable );

	void OnEnableInteraction(wxCommandEvent& event);

	void OnUseArray(wxCommandEvent &event);

	void OnSliderGaussian(wxCommandEvent &event);

	void OnFixPlane(wxCommandEvent &event);

	void OnScalarArraySelected(wxCommandEvent& event);

	void OnBtnUndo(wxCommandEvent &event);

	void OnUnfixAll(wxCommandEvent &event);

	virtual void OnRenderingDataModified(); //callback from interactor

	virtual void OnUndo(bool clearAll = false); //callback from interactor

	virtual void OnParametersModified(); //callback from the interactor

	void OnModifiedInput( int val );

	//!
	void UpdateWidget();

	//!
	void UpdateData();

	//!
	void SetRenderingTree( RenderingTree::Pointer tree );

	void CreateSelectionPointDataEntity();

	//!
	void OnStepperChanged();

	//!
	void SetView( mitk::wxMitkSelectableGLWidget* view );
	// ATTRIBUTES
private:
	/**
	\ingroup GenericSegmentationPlugin
	\author Xavi Planes
	\date Feb 2013
	*/
	class Processor : public Core::BaseProcessor
	{
	public:
		typedef enum
		{
			INPUT_POINT,
			INPUT_MESH,
			NUMBER_OF_INPUTS,
		} INPUT_TYPE;
		//!
		coreProcessor(Processor, Core::BaseProcessor);
		//!
		Processor( );
	};
	
	//! Processor (just to have a proc)
	Processor::Pointer m_processor;

	//! Surface Deformation Interactor Interface
	ms::SurfDeformInteractorHelper::Pointer m_interactor;

	std::deque<UndoListType> m_undoList;

	//! corresponds to GetTimeStep at the moment of the StartInteraction call
	int m_workingTimeStep;
	//!
	mitk::wxMitkScalarsArrayControl* m_ScalarArrayControl;

	//!
	typedef itk::SimpleMemberCommand< SurfaceDeformationPanelWidget > MemberCommand;
	MemberCommand::Pointer m_FocusManagerCallback;

	//!
	unsigned long m_FocusManagerObserverTag;

	//!
	unsigned long m_ObserverTagStepper;

	//! Active view
	mitk::wxMitkSelectableGLWidget* m_View;
};

} //namespace Widgets
} //namespace Core

#endif //_SurfaceDeformationPanelWidget_H
