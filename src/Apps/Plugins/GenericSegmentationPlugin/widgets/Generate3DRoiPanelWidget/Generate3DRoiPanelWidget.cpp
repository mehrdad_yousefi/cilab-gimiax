/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "Generate3DRoiPanelWidget.h"

#include "coreProcessorInputWidget.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreBaseWindowFactories.h"

#include "wx/wupdlock.h"

#include "sphere_blue.xpm"
#include "Deformation.xpm"
#include "CreateMask.xpm"

using namespace Core::Widgets;

// Event the widget
BEGIN_EVENT_TABLE(Generate3DRoiPanelWidget,Generate3DRoiPanelWidgetUI)
//	EVT_TOGGLEBUTTON(wxID_EnableInteraction, Core::Widgets::Generate3DRoiPanelWidget::OnEnableInteraction)
END_EVENT_TABLE()


Generate3DRoiPanelWidget::Generate3DRoiPanelWidget(
							  wxWindow* parent, 
							  int id,
							  const wxPoint& pos, 
							  const wxSize& size, 
							  long style) :
	Generate3DRoiPanelWidgetUI(parent, id, pos, size, style)
{
	m_ToolWindow = NULL;

	m_bmpBtnGenerateSphere->SetBitmapLabel( wxBitmap( sphere_blue_xpm ) );
	m_bmpBtnDeformation->SetBitmapLabel( wxBitmap( Deformation_xpm ) );
	m_bmpBtnCreateMaskImage->SetBitmapLabel( wxBitmap( CreateMask_xpm ) );
}

Generate3DRoiPanelWidget::~Generate3DRoiPanelWidget( ) 
{
}

void Generate3DRoiPanelWidget::OnInit()
{
}

void Generate3DRoiPanelWidget::UpdateWidget() 
{
}

void Generate3DRoiPanelWidget::OnBtnCreateMaskImage(wxCommandEvent &event) 
{
	ShowWindow( "Core::Widgets::SimpleProcessingWidget<SurfaceMeshToRoiProcessor>Factory" );
}

void Generate3DRoiPanelWidget::OnBtnGenerateSphere(wxCommandEvent &event)
{
	ShowWindow( "GenerateSpherePanelWidgetFactory" );
}

void Generate3DRoiPanelWidget::OnBtnDeformation(wxCommandEvent &event)
{
	ShowWindow( "SurfaceDeformationPanelWidgetFactory" );
}

void Generate3DRoiPanelWidget::ShowWindow( const std::string &factoryName )
{
	// Lock this window
	wxWindowUpdateLocker lock( this );

	// Remove old window
	if ( m_ToolWindow )
	{
		m_ToolWindow->Destroy( );
		m_ToolWindow = NULL;
	}

	// Find Factory
	if ( !factoryName.empty( ) )
	{
		// Create widget
		Core::BaseWindow* baseWindow;
		Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
		graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
		baseWindow = graphicalIface->GetBaseWindowFactory( )->CreateBaseWindow( factoryName, this );
		m_ToolWindow = dynamic_cast<wxWindow*> (baseWindow);

		// Init processor observers
		baseWindow->InitProcessorObservers( true );

		// OnInit can change the processor observers
		GetPluginTab( )->InitBaseWindow( baseWindow );

		// Add to this window 
		GetSizer( )->Add(m_ToolWindow, 1, wxTOP|wxEXPAND, 5);
		GetSizer( )->Layout( );
	}

	// Send a resize event to update the size of window
	wxSizeEvent resEvent(GetBestSize(), GetId());
	resEvent.SetEventObject(this);
	GetEventHandler()->ProcessEvent(resEvent);
}
