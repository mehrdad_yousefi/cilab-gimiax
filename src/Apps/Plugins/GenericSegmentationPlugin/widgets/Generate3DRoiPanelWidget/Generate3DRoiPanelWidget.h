/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _Generate3DRoiPanelWidget_H
#define _Generate3DRoiPanelWidget_H

#include "Generate3DRoiPanelWidgetUI.h"

#include "SurfDeformInteractorHelper.h"
#include "coreProcessingWidget.h"

// CoreLib
#include "coreRenderingTree.h"
#include "coreFrontEndPlugin.h"
#include "coreCommonDataTypes.h"
#include "coreVTKPolyDataHolder.h"

#include <vector>
#include <map>

namespace Core{
	namespace Widgets {


/**
\ingroup GenericSegmentationPlugin
\author Luigi Carotenuto
\date 23 08 2011
*/
class Generate3DRoiPanelWidget : 
	public Generate3DRoiPanelWidgetUI,
	public Core::Widgets::ProcessingWidget
{

	// OPERATIONS
public:
	coreDefineBaseWindowFactory( Generate3DRoiPanelWidget )

	//!
	cilabDeclareExceptionMacro( Exception, std::exception );

	Generate3DRoiPanelWidget(
		wxWindow* parent, 
		int id = wxID_ANY,
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=0);

	//!
	~Generate3DRoiPanelWidget( );

	//!
	void OnInit();

private:
    wxDECLARE_EVENT_TABLE();

	//!
	void OnBtnCreateMaskImage(wxCommandEvent &event); 

	//!
    void OnBtnGenerateSphere(wxCommandEvent &event);

	//!
    void OnBtnDeformation(wxCommandEvent &event);

	//!
	void ShowWindow( const std::string &factoryName );

	//!
	void UpdateWidget();

	// ATTRIBUTES
private:
	//!
	wxWindow* m_ToolWindow;
};

} //namespace Widgets
} //namespace Core

#endif //_Generate3DRoiPanelWidget_H
