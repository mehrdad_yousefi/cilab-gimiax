/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "SurfaceDeformationPanelWidget.h"

// Core
#include "coreVTKPolyDataHolder.h"
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"
#include "coreRenderingTree.h"
#include "coreDataEntityListBrowser.h"
#include "coreReportExceptionMacros.h"
#include "corePluginTab.h"
#include "coreDataEntity.h"
#include "coreDataEntityList.h"
#include "coreDataEntityList.txx"
#include "coreProcessorInputWidget.h"
#include "coreVTKImageDataHolder.h"

#include "wxMitkMultiRenderWindow.h"

// STD
#include <limits>
#include <cmath>
#include <algorithm>
#include <sstream>

// Core
#include "coreDataTreeHelper.h"

#include "mitkLookupTableProperty.h"
#include "vtkPointData.h"
#include "vtkStringArray.h"
#include "vtkCellData.h"

#include <wx/wupdlock.h>

#define MAXUNDOSIZE 100


using namespace Core::Widgets;

// Event the widget
BEGIN_EVENT_TABLE(SurfaceDeformationPanelWidget,SurfaceDeformationPanelWidgetUI)
  EVT_CHOICE(ID_cmbScalarArray, SurfaceDeformationPanelWidget::OnScalarArraySelected)
  EVT_TEXT(wxID_ANY, SurfaceDeformationPanelWidget::OnSliderGaussian)
END_EVENT_TABLE()


SurfaceDeformationPanelWidget::Processor::Processor( )
{
	SetNumberOfInputs( NUMBER_OF_INPUTS );

	GetInputPort( INPUT_POINT )->SetName( "Input Point" );
	GetInputPort( INPUT_POINT )->SetDataEntityType( Core::PointSetTypeId );

	GetInputPort( INPUT_MESH )->SetName( "Mesh to be modified" );
	GetInputPort( INPUT_MESH )->SetDataEntityType( Core::SurfaceMeshTypeId );
}


SurfaceDeformationPanelWidget::SurfaceDeformationPanelWidget(
							  wxWindow* parent, 
							  int id,
							  const wxPoint& pos, 
							  const wxSize& size, 
							  long style) :
	SurfaceDeformationPanelWidgetUI(parent, id, pos, size, style)
{
	try 
	{
		m_processor = Processor::New();		

		m_ScalarArrayControl = new mitk::wxMitkScalarsArrayControl(this, wxID_ANY);
		m_ScalarArrayControl->ShowAdvancedControls( false );
		m_chkEnableArray->GetContainingSizer( )->Add( m_ScalarArrayControl, 0, wxALL | wxEXPAND, 5 );
		Layout( );

		m_FocusManagerCallback = MemberCommand::New();
		m_FocusManagerCallback->SetCallbackFunction( this, &SurfaceDeformationPanelWidget::FocusChange );

		m_View = NULL;
	}
	coreCatchExceptionsReportAndNoThrowMacro( 
		"SurfaceDeformationPanelWidget::SurfaceDeformationPanelWidget" );

}

SurfaceDeformationPanelWidget::~SurfaceDeformationPanelWidget( ) 
{
	StopInteraction();
	m_undoList.clear();
	SetView( NULL );
}

void SurfaceDeformationPanelWidget::OnInit()
{
	GetInputWidget(Processor::INPUT_POINT)->SetAutomaticSelection(false);
	GetInputWidget(Processor::INPUT_POINT)->SetDefaultDataEntityFlag(false);
	GetInputWidget(Processor::INPUT_POINT)->Hide();

	GetInputWidget(Processor::INPUT_MESH)->SetAutomaticSelection(false);
	//GetInputWidget(Processor::INPUT_MESH)->SetDefaultDataEntityFlag(false);

	UpdateWidget( );
}

void SurfaceDeformationPanelWidget::OnModifiedInput( int val )
{
	if ( val == Processor::INPUT_MESH )
	{
		try
		{
			Core::vtkPolyDataPtr mesh;
			m_processor->GetProcessingData( Processor::INPUT_MESH, mesh );

			std::vector<blShapeUtils::ShapeUtils::VTKScalarType> scalarnames;
			blShapeUtils::ShapeUtils::GetScalarsVector( mesh, scalarnames );

			// Check if interactor has an already selected array
			blShapeUtils::ShapeUtils::VTKScalarType scalarName;
			scalarName = m_ScalarArrayControl->GetCurrentSelectedScalarType( );
			bool found = false;
			int i = 0;
			while ( !found && i < scalarnames.size( ) )
			{
				if ( scalarnames[ i ].name == scalarName.name )
				{
					found = true;
				}
				i++;
			}

			if ( !found && scalarName.name != "SolidColor" )
			{
				// Update scalar arrays and select first one
				m_ScalarArrayControl->SetScalarArrayTypes( scalarnames, true );

				// Get active array
				std::string arrayName;
				if ( mesh->GetPointData( ) && mesh->GetPointData( )->GetScalars( ) )
				{
					arrayName = mesh->GetPointData( )->GetScalars( )->GetName( );
				}
				else if ( mesh->GetCellData( ) && mesh->GetCellData( )->GetScalars( ) )
				{
					arrayName = mesh->GetCellData( )->GetScalars( )->GetName( );
				}
				
				// Select default one
				if ( scalarnames.size( ) && arrayName.empty( ) )
				{
					arrayName = scalarnames[ 0 ].name;
				}

				if ( !arrayName.empty( ) )
				{
					m_ScalarArrayControl->SetSelectedArray( arrayName.c_str( ) );
				}
			}
		}
		catch(...)
		{
			std::vector<blShapeUtils::ShapeUtils::VTKScalarType> scalarnames;
			m_ScalarArrayControl->SetScalarArrayTypes( scalarnames, true );
		}

		m_undoList.clear();
	}
}

void SurfaceDeformationPanelWidget::OnRenderingDataModified()
{
	if((!IsShown()) || (!/*IsEnabled()*/IsThisEnabled()))
		return;

	if(m_processor->GetInputDataEntity(Processor::INPUT_MESH).IsNotNull())
		GetRenderingTree()->Show(
			m_processor->GetInputDataEntity(Processor::INPUT_MESH),true,true);

	
	if(m_interactor.IsNotNull())
	{
		if( !m_interactor->GetCreateSphereMode() )
		{
			//Add the original polydata to the list:
			if(m_interactor->GetPreviousPolyData()!=NULL)
			{
				UndoListType prevElement;
				prevElement.prevPolyData = vtkPolyData::New();
				prevElement.prevPolyData->DeepCopy( m_interactor->GetPreviousPolyData() );
				prevElement.prevSphereModeState = m_interactor->GetCreateSphereMode();

				if(m_undoList.size()==MAXUNDOSIZE)
					m_undoList.pop_back(); //eliminate last element of the list

				m_undoList.push_front(prevElement); //put the new element as the front el. of the list
			}
		}
	}
	UpdateWidget();
}


void SurfaceDeformationPanelWidget::OnParametersModified()
{
	UpdateWidget( );
}

void SurfaceDeformationPanelWidget::UpdateWidget() 
{
	// Enable button
	m_btnEnableInteraction->SetValue( m_interactor.IsNotNull() );
	if(m_btnEnableInteraction->GetValue())
		m_btnEnableInteraction->SetLabel("Stop");
	else
		m_btnEnableInteraction->SetLabel("Start");

	// Undo
	m_btnUndo->SetLabel(  wxString::Format( "Undo (%d)", (int)m_undoList.size( ) ) );
	m_btnUndo->Enable( m_undoList.size( ) );

	// Parameters
	if ( m_interactor.IsNotNull() && m_interactor->GetInternalInteractor( ) )
	{
		m_sldGaussian->SetValue( m_interactor->GetInternalInteractor( )->GetGaussSigma() * 100 );
		
		
		const mitk::PlaneGeometry* planeGeometry = NULL;
		mitk::BaseRenderer* renderer = mitk::GlobalInteraction::GetInstance()->GetFocus();
		if (renderer && renderer->GetMapperID() == mitk::BaseRenderer::Standard2D)
		{
			planeGeometry = dynamic_cast<const mitk::PlaneGeometry*>(renderer->GetCurrentWorldGeometry2D() );
		}

		m_btnFixPlane->SetValue( planeGeometry && m_interactor->GetInternalInteractor( )->IsPlaneFixed( planeGeometry ) );
		m_btnFixPlane->SetLabel( m_btnFixPlane->GetValue( ) ? "UnFix Plane" : "Fix Plane" );
	}

	// Show parameters
	bool shown = m_sldGaussian->IsShown( );
	bool show = m_interactor.IsNotNull() && m_interactor->GetInternalInteractor( );
	if ( show != shown )
	{
		wxWindowUpdateLocker noUpdates( this );
		wxSizer* parametersSizer = m_sldGaussian->GetContainingSizer( );
		GetSizer( )->Show( parametersSizer, show );

		GetSizer( )->Show( m_btnUndo, show );

		// Cast a resize event
		wxSizeEvent resEvent(GetBestSize(), GetId());
		resEvent.SetEventObject(this);
		GetEventHandler()->ProcessEvent(resEvent);
	}
}

void SurfaceDeformationPanelWidget::UpdateData( )
{
	if(m_interactor.IsNotNull() && m_interactor->GetInternalInteractor( ) )
	{
		m_interactor->GetInternalInteractor( )->SetGaussSigma( float( m_sldGaussian->GetValue( ) ) / 100 );
	}
}

//!
Core::BaseProcessor::Pointer SurfaceDeformationPanelWidget::GetProcessor()
{
	return m_processor.GetPointer( );
}

//!
void SurfaceDeformationPanelWidget::StartInteractor()
{
	//clear the undo list
	m_undoList.clear();
	
	if(m_interactor.IsNotNull())
		return;

	m_workingTimeStep = GetTimeStep();
	if(m_processor->GetInputDataEntity( Processor::INPUT_MESH ).IsNull())
	{
		throw Core::Exceptions::Exception( "StartInteractor", "Please select an input data" );
		return;
	}

	CreateSelectionPointDataEntity();

	// Backup rendering properties
	GetRenderingTree( )->UpdateMetadata( m_processor->GetInputDataEntity(Processor::INPUT_MESH) );

	// Create interactor
	if(m_interactor.IsNull())
		m_interactor = ms::SurfDeformInteractorHelper::New( 
			GetRenderingTree(),
			m_processor->GetInputDataEntityHolder(Processor::INPUT_MESH),
			m_processor->GetInputDataEntityHolder(Processor::INPUT_POINT),
			Core::DataEntityHolder::New( ) );

	m_interactor->SetCreateSphereMode(false);
	m_interactor->SetOrthoFactorEnabled(true);
	
	try
	{
		m_interactor->ConnectToDataTreeNode();

		blShapeUtils::ShapeUtils::VTKScalarType scalarName;
		if ( m_ScalarArrayControl->/*IsEnabled()*/IsThisEnabled())
		{
			scalarName = m_ScalarArrayControl->GetCurrentSelectedScalarType( );
		}
		m_interactor->GetInternalInteractor( )->SetSelectedScalarArray( scalarName );

		m_interactor->SetCallback(this);
		
		OnParametersModified();

		m_FocusManagerObserverTag = mitk::GlobalInteraction::GetInstance()->GetFocusManager()->AddObserver( mitk::FocusEvent(), m_FocusManagerCallback);
  
	}
	coreCatchExceptionsReportAndNoThrowMacro( SurfaceDeformationPanelWidget::StartInteraction );
	
	UpdateWidget( );
}

//!
void SurfaceDeformationPanelWidget::StopInteraction()
{
	//in any case clear the undo list
	m_undoList.clear();

	if ( m_interactor.IsNull() || GetRenderingTree( ).IsNull() )
	{
		return;
	}

	// Restore rendering properties
	GetRenderingTree( )->UpdateRenderingProperties( m_processor->GetInputDataEntity(Processor::INPUT_MESH) );

	mitk::GlobalInteraction::GetInstance()->GetFocusManager()->RemoveObserver( m_FocusManagerObserverTag ); // remove (if tag is invalid, nothing is removed)

	Core::DataEntity::Pointer pointDE = m_processor->GetInputDataEntity(Processor::INPUT_POINT);
	if(pointDE.IsNotNull())
	{
		GetRenderingTree()->Remove(pointDE);
		m_processor->GetInputDataEntityHolder(Processor::INPUT_POINT)->SetSubject(NULL);

		//Remove the point from the list datalist also:
		if(GetListBrowser()->GetDataEntityList()->IsInTheList(pointDE->GetId()))
			GetListBrowser()->GetDataEntityList()->Remove(pointDE);
	}

	m_interactor->DisconnectFromDataTreeNode();

	m_interactor = NULL;
}

//!
bool SurfaceDeformationPanelWidget::IsSelectionEnabled( )
{
	return false;
}


void Core::Widgets::SurfaceDeformationPanelWidget::OnEnableInteraction( wxCommandEvent& event )
{
	EnableInteraction( m_btnEnableInteraction->GetValue() == true );
}


void Core::Widgets::SurfaceDeformationPanelWidget::EnableInteraction( bool bEnable )
{
	try
	{
		if(bEnable)
			StartInteractor();
		else
			StopInteraction();
	}
	coreCatchExceptionsReportAndNoThrowMacro( 
		"SurfaceDeformationPanelWidget::EnableInteraction" );

	UpdateWidget();
}


void Core::Widgets::SurfaceDeformationPanelWidget::SetRenderingTree( RenderingTree::Pointer tree )
{
	Core::BaseWindow::SetRenderingTree( tree );
	if(m_interactor.IsNotNull())
		m_interactor->SetRenderingTree( tree );
}



bool SurfaceDeformationPanelWidget::Enable( bool enable ) 
{
	bool bReturn = SurfaceDeformationPanelWidgetUI::Enable( enable );
	if ( !enable ) 
	{
		if(m_interactor.IsNotNull())
			StopInteraction( );
	}

	return bReturn;
}


void SurfaceDeformationPanelWidget::CreateSelectionPointDataEntity()
{
	Core::DataEntity::Pointer meshDE = m_processor->GetInputDataEntity(
		Processor::INPUT_MESH);

	if(meshDE.IsNull())
		return;

	int nTimeSteps = meshDE->GetNumberOfTimeSteps();

	Core::DataEntity::Pointer selectionPointDE = Core::DataEntity::New(Core::PointSetTypeId);
	selectionPointDE->GetMetadata()->SetName( "Correction Point" );
	selectionPointDE->Resize( nTimeSteps, typeid( Core::vtkPolyDataPtr ) );

	//Just put a point somewhere..why not at the origin (0,0,0)
	Core::vtkPolyDataPtr pointSet = vtkPolyData::New();
	vtkSmartPointer<vtkPoints> points = vtkPoints::New();
	points->Allocate(1);
	points->InsertPoint(0,0,0,0);
	pointSet->SetPoints(points);
	pointSet->Update();

	int timeStep = GetTimeStep();
	for(int i=0; i<nTimeSteps; i++)
	{
		if(i==timeStep)
			selectionPointDE->SetTimeStep(pointSet,i);
		else
			selectionPointDE->SetTimeStep(vtkPolyDataPtr::New(),i);
	}

	vtkSmartPointer<vtkStringArray> stringData;
	stringData = vtkSmartPointer<vtkStringArray>::New();
	stringData->SetName( "LandmarksName" );
	stringData->SetNumberOfTuples(1);
	stringData->InsertValue(0, "Point");

	pointSet->GetPointData()->AddArray( stringData );
	selectionPointDE->SetFather(m_processor->GetInputDataEntity(Processor::INPUT_MESH));
	Core::DataEntityHelper::AddDataEntityToList(selectionPointDE);
	GetRenderingTree()->Add(selectionPointDE,false,false);

	m_processor->GetInputDataEntityHolder(Processor::INPUT_POINT)->SetSubject(selectionPointDE);
}

void SurfaceDeformationPanelWidget::OnBtnUndo(wxCommandEvent &event)
{
	OnUndo( false );
}

void SurfaceDeformationPanelWidget::OnUndo(bool clearAll)
{
	if(m_undoList.size()>0)
	{
		if(clearAll)
			m_undoList.clear();
		else
		{
			UndoListType previousElement;
			previousElement.prevPolyData = m_undoList.at(0).prevPolyData;
			previousElement.prevSphereModeState = m_undoList.at(0).prevSphereModeState;
			m_undoList.pop_front();
			Core::DataEntity::Pointer inputMeshDE = 
				m_processor->GetInputDataEntity(Processor::INPUT_MESH);

			Core::vtkPolyDataPtr meshVtk;
			inputMeshDE->GetProcessingData(meshVtk, GetTimeStep());
			if(meshVtk!=NULL)
			{
				meshVtk->DeepCopy(previousElement.prevPolyData);
				m_interactor->SetCreateSphereMode(previousElement.prevSphereModeState);
			}

		}

	}

	UpdateWidget();

	//now you can update the renderization
	mitk::RenderingManager::GetInstance()->RequestUpdateAll();
}

void SurfaceDeformationPanelWidget::OnScalarArraySelected(wxCommandEvent& event)
{ 
	if ( m_interactor.IsNull( ) || m_interactor->GetInternalInteractor( ) == NULL )
	{
		return;
	}

	blShapeUtils::ShapeUtils::VTKScalarType scalarName;
	scalarName = m_ScalarArrayControl->GetCurrentSelectedScalarType( );
	m_interactor->GetInternalInteractor( )->SetSelectedScalarArray( scalarName );

	event.Skip( );
}

void SurfaceDeformationPanelWidget::OnUseArray(wxCommandEvent &event)
{
	m_ScalarArrayControl->Enable( event.GetInt( ) );

	blShapeUtils::ShapeUtils::VTKScalarType scalarName;
	if ( m_ScalarArrayControl->/*IsEnabled()*/IsThisEnabled() )
	{
		scalarName = m_ScalarArrayControl->GetCurrentSelectedScalarType( );
	}
	
	if ( m_interactor.IsNotNull( ) && m_interactor->GetInternalInteractor( ) )
	{
		m_interactor->GetInternalInteractor( )->SetSelectedScalarArray( scalarName );
	}
}

void SurfaceDeformationPanelWidget::OnSliderGaussian(wxCommandEvent &event)
{
	if ( event.GetEventObject( ) != m_sldGaussian->GetSpinCtrl( ) )
	{
		event.Skip();
		return;
	}

	UpdateData( );
}

void SurfaceDeformationPanelWidget::OnFixPlane(wxCommandEvent &event)
{
	// Get Plane geometry
	const mitk::PlaneGeometry* planeGeometry = NULL;
	mitk::BaseRenderer* renderer = mitk::GlobalInteraction::GetInstance()->GetFocus();
	if (renderer && renderer->GetMapperID() == mitk::BaseRenderer::Standard2D)
	{
		planeGeometry = dynamic_cast<const mitk::PlaneGeometry*>(renderer->GetCurrentWorldGeometry2D() );
	}

	if ( !planeGeometry )
	{
		return;
	}

	// Fix/Unfix plane
	if ( m_interactor.IsNotNull( ) && m_interactor->GetInternalInteractor( ) )
	{
		m_interactor->GetInternalInteractor( )->FixPlane( planeGeometry, event.GetInt( ) );
	}

	UpdateWidget( );
}

void SurfaceDeformationPanelWidget::FocusChange()
{
	mitk::wxMitkMultiRenderWindow* renderWindow = dynamic_cast<mitk::wxMitkMultiRenderWindow*> ( GetMultiRenderWindow( ) );
	if ( !renderWindow )
	{
		return;
	}

	// Find active window
	mitk::BaseRenderer* renderer = mitk::GlobalInteraction::GetInstance()->GetFocus();
	mitk::wxMitkMultiRenderWindow::WidgetListType widgets = renderWindow->GetLayout()->GetSliceViews( );
	mitk::wxMitkMultiRenderWindow::WidgetListType::iterator it;
	for ( it = widgets.begin() ; it != widgets.end() ; it++ )
	{
		if ( (*it)->GetRenderer( ) == renderer )
		{
			SetView( *it );
		}
	}
	
	// Update active plane
	UpdateWidget( );
}

void SurfaceDeformationPanelWidget::OnStepperChanged()
{
	// Update active plane
	UpdateWidget( );
}

void SurfaceDeformationPanelWidget::SetView( mitk::wxMitkSelectableGLWidget* view )
{
	if ( m_View )
	{
		m_View->GetSliceNavigationController()->GetSlice()->RemoveObserver( m_ObserverTagStepper );
	}

	m_View = view;

	if ( m_View )
	{
		itk::SimpleMemberCommand<SurfaceDeformationPanelWidget>::Pointer command;
		command = itk::SimpleMemberCommand<SurfaceDeformationPanelWidget>::New();
		command->SetCallbackFunction(this, &SurfaceDeformationPanelWidget::OnStepperChanged);
		m_ObserverTagStepper = m_View->GetSliceNavigationController()->GetSlice()->AddObserver( 
			itk::ModifiedEvent( ), command );
	}
}

void SurfaceDeformationPanelWidget::OnUnfixAll(wxCommandEvent &event)
{
	if ( m_interactor.IsNotNull( ) && m_interactor->GetInternalInteractor( ) )
	{
		m_interactor->GetInternalInteractor( )->UnFixAllPlanes(  );
	}

	UpdateWidget( );
}
