/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _GenerateSpherePanelWidget_H
#define _GenerateSpherePanelWidget_H

#include "GenerateSpherePanelWidgetUI.h"

#include "SurfDeformInteractorHelper.h"
#include "coreProcessingWidget.h"

// CoreLib
#include "coreRenderingTree.h"
#include "coreFrontEndPlugin.h"
#include "coreCommonDataTypes.h"
#include "coreVTKPolyDataHolder.h"

#include <vector>
#include <map>

namespace Core{
namespace Widgets {


/**
\ingroup GenericSegmentationPlugin
\author Luigi Carotenuto
\date 23 08 2011
*/
class GenerateSpherePanelWidget : 
	public GenerateSpherePanelWidgetUI,
	public Core::Widgets::ProcessingWidget,
	public SurfDeformationCallback
{

	// OPERATIONS
public:
	coreDefineBaseWindowFactory( GenerateSpherePanelWidget )

	//!
	cilabDeclareExceptionMacro( Exception, std::exception );

	GenerateSpherePanelWidget(
		wxWindow* parent, 
		int id = wxID_ANY,
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=0);

	//!
	~GenerateSpherePanelWidget( );

	//!
	void OnInit();

	//!
	bool Enable( bool enable /*= true */ );

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

	//!
	void StartInteractor( );

	//!
	void StopInteraction( );

	//!
	bool IsSelectionEnabled( );


private:
    wxDECLARE_EVENT_TABLE();

	void EnableInteraction( bool bEnable );

	void OnEnableInteraction(wxCommandEvent& event);

	virtual void OnRenderingDataModified(); //callback from interactor

	virtual void OnParametersModified(); //callback from the interactor

	void OnModifiedInput( int val );

	//!
	void UpdateWidget();

	//!
	void SetRenderingTree( RenderingTree::Pointer tree );



	void CreateSelectionPointDataEntity();

	void CreateSphereDataEntity();

	void ClearEditBoxes();

	// ATTRIBUTES
private:
	/**
	\ingroup GenericSegmentationPlugin
	\author Xavi Planes
	\date Feb 2013
	*/
	class Processor : public Core::BaseProcessor
	{
	public:
		typedef enum
		{
			INPUT_POINT,
			INPUT_MESH,
			NUMBER_OF_INPUTS,
		} INPUT_TYPE;
		//!
		coreProcessor(Processor, Core::BaseProcessor);
		//!
		Processor( );
	};

	//! Processor (just to have a proc)
	Processor::Pointer m_processor;

	//! Surface Deformation Interactor Interface
	ms::SurfDeformInteractorHelper::Pointer m_interactor;

	//! corresponds to GetTimeStep at the moment of the StartInteraction call
	int m_workingTimeStep;
};

} //namespace Widgets
} //namespace Core

#endif //_GenerateSpherePanelWidget_H
