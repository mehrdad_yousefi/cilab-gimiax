/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "ThresholdPhilipsPanelWidget.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"
// Core
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"

// Core
#include "coreDataTreeHelper.h"
#include "coreReportExceptionMacros.h"
#include "coreProcessorInputWidget.h"


const wxWindowID ThresholdPhilipsPanelWidget::ms_WidgetID = wxNewId();
const std::string ThresholdPhilipsPanelWidget::ms_WidgetCaption = "ThresholdPhilips Panel Widget";

ThresholdPhilipsPanelWidget::ThresholdPhilipsPanelWidget(  wxWindow* parent, int id/*= wxID_ANY*/,
													   const wxPoint&  pos /*= wxDefaultPosition*/, 
													   const wxSize&  size /*= wxDefaultSize*/, 
													   long style/* = 0*/ )
: ThresholdPhilipsPanelWidgetUI(parent, id,pos,size,style)
{
	m_Processor = ThresholdPhilipsProcessor::New();

	SetName( ms_WidgetCaption );
}

ThresholdPhilipsPanelWidget::~ThresholdPhilipsPanelWidget( )
{
	// We don't need to destroy anything because all the child windows 
	// of this wxWindow are destroyed automatically
}

void ThresholdPhilipsPanelWidget::OnInit( )
{
	//GetInputWidget( 0 )->SetAutomaticSelection( false );
	GetInputWidget( 0 )->SetAutomaticSelection( true );
	GetInputWidget( 0 )->SetDefaultDataEntityFlag( false );
	//------------------------------------------------------
	// Observers to data
	m_Processor->GetOutputDataEntityHolder( ThresholdPhilipsProcessor::OUTPUT_LV_COMPLETE )->AddObserver( 
		this, 
		&ThresholdPhilipsPanelWidget::OnModifiedOutputDataEntity );

	m_Processor->GetInputDataEntityHolder( ThresholdPhilipsProcessor::INPUT_MESH )->AddObserver( 
		this, 
		&ThresholdPhilipsPanelWidget::OnModifiedInputDataEntity );


	UpdateWidget();
}

void ThresholdPhilipsPanelWidget::UpdateWidget()
{
	
	UpdateHelperWidget( );

}

void ThresholdPhilipsPanelWidget::UpdateData()
{
	// Set parameters to processor. Pending
}

void ThresholdPhilipsPanelWidget::OnBtnApply(wxCommandEvent& event)
{
	// Catch the exception from the processor and show the message box
	try
	{
		// Update the scale values from widget to processor
		UpdateData();

		ThresholdPhilipsProcessor::TPPParameters& parameters = m_Processor->GetParameters();
		if( m_chkOutputRV->IsChecked() )
		{
			parameters.m_bOutputRightVentricle = true;
		}
		else
		{
			parameters.m_bOutputRightVentricle = false;
		}

		UpdateProcessor( true );
	}
	coreCatchExceptionsReportAndNoThrowMacro( "ThresholdPhilipsPanelWidget::OnBtnApply" );
}


void ThresholdPhilipsPanelWidget::OnModifiedOutputDataEntity()
{
	try
	{
		Core::DataEntity::Pointer inputDataEntity;
		inputDataEntity = m_Processor->GetInputDataEntity( ThresholdPhilipsProcessor::INPUT_MESH );

		// Hide input if is different from output and output is not empty
		if ( m_Processor->GetOutputDataEntity( 0 ).IsNotNull() && 
			 m_Processor->GetOutputDataEntity( 0 ) != inputDataEntity )
		{
			GetRenderingTree( )->Show( inputDataEntity, false );
		}

		// Add output to the data list and render it
		// After adding the output, the input will automatically be changed to
		// this one
		for( int outputIndex = 0; outputIndex < ThresholdPhilipsProcessor::OUTPUTS_NUMBER; outputIndex++ )
		{
			Core::DataTreeHelper::PublishOutput(
				m_Processor->GetOutputDataEntityHolder( outputIndex ),
				GetRenderingTree(), false,
				outputIndex == ThresholdPhilipsProcessor::OUTPUT_LV_COMPLETE,
				outputIndex == ThresholdPhilipsProcessor::OUTPUT_LV_COMPLETE );
		}
	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"CardiacInitializationPanelWidget::OnModifiedOutputDataEntity")

}

void ThresholdPhilipsPanelWidget::UpdateHelperWidget()
{
	if ( GetHelperWidget( ) == NULL )
	{
		return;
	}
		GetHelperWidget( )->SetInfo( 
			Core::Widgets::HELPER_INFO_LEFT_BUTTON, 
			" info that is useful in order to use the processor" );

}

bool ThresholdPhilipsPanelWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = ThresholdPhilipsPanelWidgetUI::Enable( enable );

	// If this panel widget is selected -> Update the widget
	if ( enable )
	{
		UpdateWidget();
	}

	return bReturn;
}

void ThresholdPhilipsPanelWidget::OnModifiedInputDataEntity()
{
	UpdateWidget();
}

Core::BaseProcessor::Pointer ThresholdPhilipsPanelWidget::GetProcessor()
{
	return m_Processor.GetPointer( );
}

