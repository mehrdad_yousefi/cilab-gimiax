/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "GenericSegmentationPluginPropagateLandmarksPanelWidget.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"
// Core
#include "coreDataEntityHelper.h"
#include "coreUserHelperWidget.h"

// Core
#include "coreDataTreeHelper.h"
#include "coreReportExceptionMacros.h"
#include "coreProcessorInputWidget.h"

#include "coreMovieToolbar.h"


namespace GenericSegmentationPlugin
{

PropagateLandmarksPanelWidget::PropagateLandmarksPanelWidget(  wxWindow* parent, int id/*= wxID_ANY*/,
													   const wxPoint&  pos /*= wxDefaultPosition*/, 
													   const wxSize&  size /*= wxDefaultSize*/, 
													   long style/* = 0*/ )
: GenericSegmentationPluginPropagateLandmarksPanelWidgetUI(parent, id,pos,size,style)
{
	m_Processor = PropagateLandmarksProcessor::New();

	SetName( "PropagateLandmarks Panel Widget" );
}

PropagateLandmarksPanelWidget::~PropagateLandmarksPanelWidget( )
{
	// We don't need to destroy anything because all the child windows 
	// of this wxWindow are destroyed automatically
}

void PropagateLandmarksPanelWidget::OnInit( )
{

	GetInputWidget(0)->SetAutomaticSelection(false);
	GetInputWidget(0)->SetDefaultDataEntityFlag(false);

	Core::Widgets::MovieToolbar* movieToolbar;
	GetPluginTab()->GetWidget( wxID_MovieToolbar, movieToolbar );
	if(movieToolbar)
		m_Processor->SetTimeStepHolder(movieToolbar->GetCurrentTimeStep());


	m_Processor->GetInputDataEntityHolder(0)->AddObserver(
		this,
		&PropagateLandmarksPanelWidget::OnModifiedInputDataEntity);

	UpdateWidget();
}

void PropagateLandmarksPanelWidget::UpdateWidget()
{
	
	UpdateHelperWidget();

}

void PropagateLandmarksPanelWidget::UpdateData()
{
	// Set parameters to processor. Pending
}

void PropagateLandmarksPanelWidget::OnBtnApply(wxCommandEvent& event)
{
	// Catch the exception from the processor and show the message box
	try
	{
		// Update the scale values from widget to processor
		UpdateData();

		m_Processor->Update( );
	}
	coreCatchExceptionsReportAndNoThrowMacro( "PropagateLandmarksPanelWidget::OnBtnApply" );
}


void PropagateLandmarksPanelWidget::OnModifiedOutputDataEntity()
{
	try{

	
	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"CardiacInitializationPanelWidget::OnModifiedOutputDataEntity")

}

void PropagateLandmarksPanelWidget::UpdateHelperWidget()
{
	if ( GetHelperWidget( ) == NULL )
	{
		return;
	}
		GetHelperWidget( )->SetInfo( 
			Core::Widgets::HELPER_INFO_LEFT_BUTTON, 
			" info that is useful in order to use the processor" );

}

bool PropagateLandmarksPanelWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = GenericSegmentationPluginPropagateLandmarksPanelWidgetUI::Enable( enable );

	// If this panel widget is selected -> Update the widget
	if ( enable )
	{
		UpdateWidget();
	}

	return bReturn;
}

void PropagateLandmarksPanelWidget::OnModifiedInputDataEntity()
{
	Core::DataTreeHelper::PublishOutput( 
		m_Processor->GetInputDataEntityHolder( 0 ), 
		GetRenderingTree(),false,false);
	UpdateWidget();
}

Core::BaseProcessor::Pointer PropagateLandmarksPanelWidget::GetProcessor()
{
	return m_Processor.GetPointer( );
}

}   // namespace GenericSegmentationPlugin
