/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _GenericSegmentationPluginPropagateLandmarksPanelWidget_H
#define _GenericSegmentationPluginPropagateLandmarksPanelWidget_H

#include "GenericSegmentationPluginPropagateLandmarksProcessor.h"
#include "GenericSegmentationPluginPropagateLandmarksPanelWidgetUI.h"

// CoreLib
#include "coreRenderingTree.h"
#include "corePointInteractorPointSelect.h"
#include "coreProcessingWidget.h"

#define wxID_PropagateLandmarksPW wxID("Propagate Landmarks Panel Widget")

namespace Core
{
	namespace Widgets
	{
		class UserHelper;
		class DataEntityListBrowser;
	}
}
	
namespace GenericSegmentationPlugin
{

/*
\ingroup PropagateLandmarksate Plugin
\author Chiara Riccobene
\date 13 Dec 2009
*/
class PropagateLandmarksPanelWidget : 
public GenericSegmentationPluginPropagateLandmarksPanelWidgetUI,
public Core::Widgets::ProcessingWidget 
{

// OPERATIONS
public:
	//!
	coreDefineBaseWindowFactory( PropagateLandmarksPanelWidget );
	
	//!
	PropagateLandmarksPanelWidget(wxWindow* parent, int id= wxID_ANY,
		const wxPoint&  pos = wxDefaultPosition, 
		const wxSize&  size = wxDefaultSize, 
		long style = 0);

	//!
	~PropagateLandmarksPanelWidget( );

	//! Add button events to the bridge and call UpdateWidget()
	void OnInit(  );
	
	//!
	bool Enable( bool enable /*= true */ );
	
	//!
	Core::BaseProcessor::Pointer GetProcessor( );

private:
	//! Update GUI from working data
	void UpdateWidget();

	//! Update working data from GUI
	void UpdateData();

	//! Button has been pressed
	void OnBtnApply(wxCommandEvent& event);

	//!
	void UpdateHelperWidget( );

	//!
	void OnModifiedInputDataEntity();

	//!
	void OnModifiedOutputDataEntity();
	
// ATTRIBUTES
private:
	//! Working data of the processor
	PropagateLandmarksProcessor::Pointer m_Processor;

};

}   //namespace GenericSegmentationPlugin

#endif //_GenericSegmentationPluginPropagateLandmarksPanelWidget_H
