#ifndef GENERICSEGMENTATIONPLUGINWIDGETCOLLECTIVE_H
#define GENERICSEGMENTATIONPLUGINWIDGETCOLLECTIVE_H

// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "CILabNamespaceMacros.h"
#include "coreWidgetCollective.h"
#include "GenericSegmentationPluginProcessorCollective.h"

// Forward declarations
namespace Core
{
namespace Plugins
{
	class AngioSegmentationPlugin;
}
namespace Widgets 
{ 
	//class MultiRenderWindow;
	class AppearanceSuitcase;
} 
}

namespace gsp{

class WidgetCollective : public Core::WidgetCollective
{
public:
	//!
	coreDeclareSmartPointerClassMacro(
		WidgetCollective, 
		Core::WidgetCollective );

private:
	WidgetCollective( );

};

} // gsp

#endif //GENERICSEGMENTATIONPLUGINWIDGETCOLLECTIVE_H
