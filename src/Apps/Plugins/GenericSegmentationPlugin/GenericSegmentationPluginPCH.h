#ifndef GENERICSEGMENTATIONPLUGINWXPCH_H
#define GENERICSEGMENTATIONPLUGINWXPCH_H

// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#if defined( _MSC_VER )
#define WX_HIDE_MODE_T 1
typedef unsigned short mode_t;
#endif

#ifdef WX_PRECOMP
  #include <wx/wxprec.h>
#endif
#include <wx/wx.h>

#include <CILabAssertMacros.h>
#include <CILabItkMacros.h>
#include <CILabNamespaceMacros.h>
#include <coreDataEntityUtilities.h>
#include <blMath.h>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

#include <coreAssert.h>
#include <coreDataEntity.h>
#include <coreDataEntityHelper.h>
#include <coreDataEntityUtilities.h>
#include <coreDataHolder.h>
#include <coreDataHolderConnection.h>
#include <coreEnvironment.h>
#include <coreException.h>
#include <coreFrontEndPlugin.h>
#include <coreImageInfoWidget.h>
#include <coreKernel.h>
#include <coreLogger.h>
#include <coreLoggerHelper.h>
#include <coreReportExceptionMacros.h>
#include <coreSmartPointerMacros.h>
#include <coreAssert.h>
#include <coreDataEntity.h>
#include <coreDataHolder.h>
#include <coreException.h>
#include <coreFrontEndPlugin.h>
#include <coreMultiRenderWindow.h>
#include <coreObject.h>
#include <coreRenderingTree.h>
#include <coreRenderingTree.txx>
#include <coreReportExceptionMacros.h>

#include <gblBridge.h>
#include <gblValidate.h>
#include <gblWxBridgeLib.h>
#include <gblWxButtonEventProxy.h>
#include <gblWxChoiceValueProxy.h>
#include <gblWxConnectorOfWidgetChangesToSlotFunction.h>
#include <gblWxValidate.h>
#include <itkBinaryThresholdImageFilter.h>
#include <itkImage.h>
#include <itkNeighborhoodConnectedImageFilter.h>
#include <itkPoint.h>
#include <mitkCuboid.h>
#include <mitkDataTree.h>
#include <mitkDataTreeNode.h>
#include <mitkGlobalInteraction.h>
#include <mitkInteractionConst.h>
#include <mitkPointOperation.h>
#include <mitkPointSet.h>
#include <mitkPointSetInteractor.h>
#include <mitkRenderingManager.h>
#include <vtkImageData.h>
#include <vtkPolyData.h>
//#include <wxMitkToolBoxControl.h>
//#include <wxMitkToolBoxItem.h>

#include <iostream>
#include <itkIndent.h>
#include <limits>
#include <mitkDataTreeHelper.h>
#include <mitkDataTreeNode.h>
#include <mitkGlobalInteraction.h>
#include <mitkProperties.h>
#include <set>
#include <string>
#include <vtkCornerAnnotation.h>
#include <vtkPolyData.h>
#include <wx/choicebk.h>
#include <wx/frame.h>
#include <wx/image.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>


#endif //GENERICSEGMENTATIONPLUGINWXPCH_H
