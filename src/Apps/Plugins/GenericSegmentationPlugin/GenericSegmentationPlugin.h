// Copyright 2006 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#ifndef GenericSegmentationPlugin_H
#define GenericSegmentationPlugin_H

#include <coreFrontEndPlugin.h>
#include "GenericSegmentationPluginProcessorCollective.h"
#include "GenericSegmentationPluginWidgetCollective.h"

namespace Core
{
namespace Plugins
{

/** 
\brief Plugin for Generic Segmentation
This class is only for initialization, not part of the model.

\ingroup GenericSegmentationPlugin
\author Albert Sanchez
\date 22 Sep 2010
*/

class PLUGIN_EXPORT GenericSegmentationPlugin 
	: public Core::FrontEndPlugin::FrontEndPlugin
{
public:
	coreDeclareSmartPointerClassMacro(
		GenericSegmentationPlugin, 
		Core::FrontEndPlugin::FrontEndPlugin);

protected:
	GenericSegmentationPlugin(void);
	virtual ~GenericSegmentationPlugin(void);

private:

private:
	//! Contains all the processors for the plugin
	gsp::ProcessorCollective::Pointer m_Processors;
	//! Contains all the widgets
	gsp::WidgetCollective::Pointer m_WidgetCollective;

	coreDeclareNoCopyConstructors(GenericSegmentationPlugin);
};

}
}

#endif // GenericSegmentationPlugin_H
