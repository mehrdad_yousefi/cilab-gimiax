// Copyright 2006 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "GenericSegmentationPluginWidgetCollective.h"

#include "RegionGrowWidget.h"

#include <corePluginTab.h>
#include <coreException.h>
#include "coreWxMitkGraphicalInterface.h"
#include "coreImageInfoWidget.h"
#include "corePluginTabFactory.h"
#include "coreVisualProperties.h"
#include "coreSimpleProcessingWidget.h"

#include "ThresholdWidget.h"
#include "OtsuWidget.h"
#include "ConnectedThresholdWidget.h"
#include "VtkConnectedThresholdPanelWidget.h"
#include "ManualCorrectionsPanelWidget.h"
#include "TransformationBoxPanelWidget.h"
#include "MaskImageToROIImageWidget.h"
#include "gspPropagateLandmarksPanelWidget.h"
#include "ThresholdPhilipsPanelWidget.h"
#include "gspPhilipsSegmentedScarMeshPanelWidget.h"
#include "SurfaceMeshToRoiProcessor.h"
#include "Generate3DRoiPanelWidget.h"
#include "MoveDataPanelWidget.h"
#include "GenerateSpherePanelWidget.h"
#include "SurfaceDeformationPanelWidget.h"

const long wxID_SurfaceMeshToRoi = wxNewId();

#include "SurfaceDeform3D.xpm"
#include "Deformation.xpm"

using namespace Core;
using namespace gsp;

gsp::WidgetCollective::WidgetCollective()
{

	Core::WindowConfig config = Core::WindowConfig().ProcessingTool( );
	config.Category("Segmentation");

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory( 
		ThresholdWidget::Factory::NewBase( ), 
		config.Id( wxID_Generic_Treshold_Widget ).Caption( "Threshold Segmentation" ) );

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory( 
		OtsuWidget::Factory::NewBase( ), 
		config.Id( wxID_Generic_Otsu_Widget ).Caption( "Otsu Segmentation" ) );

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory( 
		RegionGrowWidget::Factory::NewBase( ), 
		config.Id( wxID_Generic_RegionGrow_Widget ).Caption( "Region Grow Segmentation" ) );

	//Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory( 
	//	ConnectedThresholdWidget::Factory::NewBase( ), 
	//	config.Id( wxID_Generic_ConnectedThreshold_Widget ).Caption( "Connected Threshold Segmentation" ) );

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory( 
		VtkConnectedThresholdPanelWidget::Factory::NewBase( ), 
		config.Id( wxID_Generic_VtkConnectedThreshold_Widget ).Caption( "Vtk Connected Threshold Segmentation" ) );

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory( 
		GenericSegmentationPlugin::ManualCorrectionsPanelWidget::Factory::NewBase( ), 
		config.Id( wxID_Generic_ManualCorrection_Widget ).Caption( "Manual Correction of meshes" ) );

	
	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory( 
		GenericSegmentationPlugin::TransformationBoxPanelWidget::Factory::NewBase( ), 
		config.Id( wxID_Generic_TransformationBox_Widget ).Caption( "Transformation Box" ) );	
	
	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
		GenericSegmentationPlugin::MaskImageToROIImageWidget::Factory::NewBase(),
		config.ProcessorObservers().Id( wxID_MaskImageToROI ).Caption( "Mask Image To ROI Image" ) );

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
		GenericSegmentationPlugin::PropagateLandmarksPanelWidget::Factory::NewBase(),
		config.ProcessorObservers().Id(wxID_PropagateLandmarksPW).Caption( "Propagate Landmarks" ) );

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
		ThresholdPhilipsPanelWidget::Factory::NewBase(), 
		config.Id( ThresholdPhilipsPanelWidget::ms_WidgetID ).Caption( ThresholdPhilipsPanelWidget::ms_WidgetCaption ) );

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
		PhilipsSegmentedScarMeshPanelWidget::Factory::NewBase(),
		config.Id( PhilipsSegmentedScarMeshPanelWidget::ms_WidgetID ).Caption( PhilipsSegmentedScarMeshPanelWidget::ms_WidgetCaption ) );

	typedef Core::Widgets::SimpleProcessingWidget<Core::SurfaceMeshToRoiProcessor> 
		SurfaceToRoiWidget;
	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory( 
		SurfaceToRoiWidget::Factory::NewBase(),
		config.Id(wxID_SurfaceMeshToRoi).Caption( "Surface mesh to ROI image" ) );

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
		Core::Widgets::MoveDataPanelWidget::Factory::NewBase(),
		Core::WindowConfig( ).ProcessingTool().Category( "Filtering" )
		.Caption("Translate Data") );

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
		Core::Widgets::Generate3DRoiPanelWidget::Factory::NewBase(),
		Core::WindowConfig( ).ProcessingTool().Category( "Segmentation" )
		.Caption("Generate 3D ROI").Bitmap( SurfaceDeform3D_xpm ) );

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
		Core::Widgets::GenerateSpherePanelWidget::Factory::NewBase(),
		Core::WindowConfig( ).ProcessingTool().Category( "Segmentation" )
		.Caption("Generate Sphere") );

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
		Core::Widgets::SurfaceDeformationPanelWidget::Factory::NewBase(),
		Core::WindowConfig( ).ProcessingTool().Category( "Segmentation" )
		.Caption("Surface Deformation").Bitmap( Deformation_xpm ) );

}


