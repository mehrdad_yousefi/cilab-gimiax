/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreSignalTimePropagationProcessor.h"
#include "coreDataEntityHelper.h"
#include "coreEnvironment.h"
#include "blSignalCollective.h"

Core::SignalTimePropagationProcessor::SignalTimePropagationProcessor( )
{
	SetNumberOfInputs( 2 );
	GetInputPort( 0 )->SetName( "Input data entity" );
	GetInputPort( 0 )->SetDataEntityType( Core::ImageTypeId );
	GetInputPort( 0 )->SetUpdateMode( BaseFilterInputPort::UPDATE_ACCESS_MULTIPLE_TIME_STEP );

	GetInputPort( 1 )->SetName( "Input signal" );
	GetInputPort( 1 )->SetDataEntityType( Core::SignalTypeId );
	GetInputPort( 1 )->SetUpdateMode( BaseFilterInputPort::UPDATE_ACCESS_MULTIPLE_TIME_STEP );

	SetNumberOfOutputs( 0 );
	
	SetName( "SignalTimePropagationProcessor" );

	m_MarkersPerScan = 1;
	m_TR = 1;
	m_PropagationMethod = PROPAGATION_METHOD_USE_ANNOTATIONS;
}

void Core::SignalTimePropagationProcessor::Update()
{
	int numOfTimeSteps = GetInputDataEntity(0)->GetNumberOfTimeSteps();

	blSignalCollective::Pointer signal ;
	GetProcessingData(1, signal);

	if ( m_UseStartTime )
	{
		// Add start time tag
		GetInputDataEntity(0)->GetMetadata()->AddTag( "StartTime", m_StartTime );

		// Set start time to 0
		for ( int i = 0 ; i < signal->GetNumberOfSignals() ; i++ )
		{
			signal->GetSignal( i )->SetStartTime( 0 );
		}
	}
	else
	{
		// Reset previous value
		std::string time = "00:00:00";
		GetInputDataEntity(0)->GetMetadata()->AddTag( "StartTime", time );
	}


	switch ( m_PropagationMethod )
	{
	case PROPAGATION_METHOD_USE_ANNOTATIONS: 
		{
			// Get last annotations vector
			std::vector< blSignalAnnotation::Pointer > annots;
			for ( int i = 0 ; i < signal->GetNumberOfSignals() ; i++ )
			{
				if ( signal->GetSignal(i)->GetAnnotation( 0 ).IsNotNull() )
				{
					annots = signal->GetSignal(i)->GetAnnotationsVector();
				}
			}

			if ( annots.empty() )
			{
				throw Core::Exceptions::Exception( 
					"SignalTimePropagationProcessor::Update", "Cannot find annotations" );
			}

			// Propagate time information
			int annotationIndex = 0;
			int timeSt = 0;
			while (annotationIndex < annots.size() && timeSt < numOfTimeSteps )
			{
				blSignal::SampleType time;
				time = signal->GetSignal(0)->GetValueX( annots.at(annotationIndex)->GetXPos() );
				GetInputDataEntity(0)->SetTimeAtTimeStep( timeSt, time);
				annotationIndex += m_MarkersPerScan;
				timeSt++;
			}
		}
		break;
	case PROPAGATION_METHOD_USE_DATA_SIZE: 
		{
			// Use number of samples divided by number of time steps
			int numOfSamples = signal->GetSignal(0)->GetNumberOfValues();
			for (int it = 0; it < numOfTimeSteps; it++)
			{
				blSignal::SampleType time;
				time = signal->GetSignal(0)->GetValueX( int(it*numOfSamples/numOfTimeSteps) );
				GetInputDataEntity(0)->SetTimeAtTimeStep(it, time );
			}
		}
		break;
	case PROPAGATION_METHOD_USE_TR: 
		{
			// Get start time and increase by TR
			blSignal::SampleType time;
			time = signal->GetSignal(0)->GetValueX( 0 );
			for (int it = 0; it < numOfTimeSteps; it++)
			{
				GetInputDataEntity(0)->SetTimeAtTimeStep(it, time );
				time += m_TR;
			}
		}
		break;
	}


}

int Core::SignalTimePropagationProcessor::GetMarkersPerScan() const
{
	return m_MarkersPerScan;
}

void Core::SignalTimePropagationProcessor::SetMarkersPerScan( int val )
{
	m_MarkersPerScan = val;
}

bool Core::SignalTimePropagationProcessor::GetUseStartTime() const
{
	return m_UseStartTime;
}

void Core::SignalTimePropagationProcessor::SetUseStartTime( bool val )
{
	m_UseStartTime = val;
}

std::string Core::SignalTimePropagationProcessor::GetStartTime() const
{
	return m_StartTime;
}

void Core::SignalTimePropagationProcessor::SetStartTime( std::string val )
{
	m_StartTime = val;
}

float Core::SignalTimePropagationProcessor::GetTR() const
{
	return m_TR;
}

void Core::SignalTimePropagationProcessor::SetTR( float val )
{
	m_TR = val;
}

Core::SignalTimePropagationProcessor::PROPAGATION_METHOD Core::SignalTimePropagationProcessor::GetPropagationMethod() const
{
	return m_PropagationMethod;
}

void Core::SignalTimePropagationProcessor::SetPropagationMethod( PROPAGATION_METHOD val )
{
	m_PropagationMethod = val;
}