/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreSignalTimePropagationProcessor_H
#define _coreSignalTimePropagationProcessor_H

#include "corePluginMacros.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreBaseFilter.h"
#include "coreBaseProcessor.h"

namespace Core{

/**
Propagates time information from signal to connected data entity (3d +t).

\ingroup SignalViewerPlugin
\author Chiara Riccobene
\date 9 dec 2009
*/

class PLUGIN_EXPORT SignalTimePropagationProcessor : public Core::BaseProcessor
{
public:
	enum PROPAGATION_METHOD
	{
		PROPAGATION_METHOD_USE_ANNOTATIONS,
		PROPAGATION_METHOD_USE_DATA_SIZE,
		PROPAGATION_METHOD_USE_TR
	};

public:
	coreDeclareSmartPointerClassMacro(Core::SignalTimePropagationProcessor, Core::BaseProcessor);
	//!
	void Update();
	//!
	bool CheckInputs();
	//!
	int GetMarkersPerScan() const;
	void SetMarkersPerScan(int val);

	//!
	bool GetUseStartTime() const;
	void SetUseStartTime(bool val);

	//!
	std::string GetStartTime() const;
	void SetStartTime(std::string val);

	//!
	float GetTR() const;
	void SetTR(float val);

	//!
	PROPAGATION_METHOD GetPropagationMethod() const;
	void SetPropagationMethod(PROPAGATION_METHOD val);

private:
	//!
	SignalTimePropagationProcessor( );	

	//!
	float m_TR;
	//!
	int m_MarkersPerScan;
	//!
	bool m_UseStartTime;
	//!
	std::string m_StartTime;
	//!
	PROPAGATION_METHOD m_PropagationMethod;
};

} // Core

#endif //_coreSignalTimePropagationProcessor_H
