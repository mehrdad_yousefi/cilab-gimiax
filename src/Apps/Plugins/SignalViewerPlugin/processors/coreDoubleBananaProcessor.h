/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreDoubleBananaProcessor_H
#define _coreDoubleBananaProcessor_H

#include "corePluginMacros.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreBaseFilter.h"
#include "coreBaseProcessor.h"

namespace Core{

/**
Compute double bananna from EEG signals

Definition: http://eegatlas-online.com/index.php/montages/bipolar/double-banana-2

\ingroup SignalViewerPlugin
\author Xavi Planes
\date 9 dec 2009
*/

class PLUGIN_EXPORT DoubleBananaProcessor : public Core::BaseProcessor
{
public:
	coreDeclareSmartPointerClassMacro(Core::DoubleBananaProcessor, Core::BaseProcessor);
	//!
	void Update();
private:
	/**
	*/
	DoubleBananaProcessor( );	

};

} // Core

#endif //_coreDoubleBananaProcessor_H
