/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _SignalViewPluginPCH_H
#define _SignalViewPluginPCH_H

// CoreLib
#include "coreDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreFrontEndPlugin.h"
#include "coreReportExceptionMacros.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"

// GUIBridgeLib
#include "gblBridge.h"
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"
#include "gblWxConnectorOfWidgetChangesToSlotFunction.h"

// VTK
#include "vtkPolyData.h"

// STD
#include <limits>
#include <string>

// wxWidgets
#ifdef WX_PRECOMP
  #include <wx/wxprec.h>
#endif
#include <wx/wx.h>
#include <wx/image.h>
#include <wx/spinbutt.h>


// BaseLib
#include "blVTKHelperTools.h"

// CoreLib
#include "coreDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityHelper.txx"
#include "coreDataTreeHelper.h"
#include "coreFrontEndPlugin.h"
#include "coreKernel.h"
#include "coreMultiRenderWindow.h"
#include "coreObject.h"
#include "corePluginTab.h"
#include "coreBaseProcessor.h"
#include "coreRenderingTree.h"
#include "coreReportExceptionMacros.h"
#include "coreSmartPointerMacros.h"
#include "coreUserHelperWidget.h"
#include "coreVTKPolyDataHolder.h"
#include "coreWxMitkCoreMainWindow.h"

// GUIBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"
#include "gblWxConnectorOfWidgetChangesToSlotFunction.h"

// ITK
#include "itkImage.h"
#include "itkResampleImageFilter.h"
#include "itkScaleTransform.h"
#include "itkSubtractImageFilter.h"

// VTK
#include "vtkPolyData.h"

// BaseLib
#include "wxID.h"

// STD
#include <iostream>
#include <limits>
#include <string>

#endif //_SignalViewPluginPCH_H
