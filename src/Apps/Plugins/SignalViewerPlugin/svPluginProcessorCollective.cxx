/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "svPluginProcessorCollective.h"

#include "coreDataEntityReader.h"
#include "coreDataEntityWriter.h"
#include "coreSignalReader.h"
#include "coreSignalWriter.h"

#include "coreWxMitkGraphicalInterface.h"


svProcessorCollective::svProcessorCollective()
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
	gIface = Core::Runtime::Kernel::GetGraphicalInterface();

	// Signal data entity
	gIface->RegisterReader( Core::IO::BaseDataEntityReader::Pointer( Core::IO::SignalReader::New() ) );
	gIface->RegisterWriter( Core::IO::BaseDataEntityWriter::Pointer( Core::IO::SignalWriter::New() ) );

}

svProcessorCollective::~svProcessorCollective()
{
}
