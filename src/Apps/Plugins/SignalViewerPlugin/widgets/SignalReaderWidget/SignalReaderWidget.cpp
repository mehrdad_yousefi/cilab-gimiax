/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "SignalReaderWidget.h"


SignalReaderWidget::SignalReaderWidget(wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    SignalReaderWidgetUI(parent, id, pos, size, style)
{

}

Core::BaseProcessor::Pointer SignalReaderWidget::GetProcessor()
{
	return NULL;
}

void SignalReaderWidget::OnSpinFactor( wxSpinEvent &event )
{
	UpdateWidget( );
}

void SignalReaderWidget::SetProperties( blTagMap::Pointer tagMap )
{
	m_TagMap = tagMap;

	m_PropertiesWidget->UpdateWidget( m_TagMap );

	Fit();

	UpdateWidget();
}

void SignalReaderWidget::GetProperties( blTagMap::Pointer tagMap )
{
	UpdateData( );

	tagMap->AddTags( m_TagMap );
}

void SignalReaderWidget::UpdateWidget()
{
	blTag::Pointer tag;
	float sampleRate = 0;
	tag = m_TagMap->FindTagByName( "SampleRate" );
	if ( tag.IsNotNull() )
	{
		tag->GetValue( sampleRate );
	}

	size_t fileSize = 0;
	tag = m_TagMap->FindTagByName( "FileSize (Mb)" );
	if ( tag.IsNotNull() )
	{
		tag->GetValue( fileSize );
	}

	float newSampleRate = sampleRate / m_SpinFactor->GetValue();
	float newSize = fileSize / m_SpinFactor->GetValue();
	m_txtOutputFreq->SetValue( wxString::Format( "%f", newSampleRate ) );
	m_txtOutputSize->SetValue( wxString::Format( "%f", newSize ) );
}

void SignalReaderWidget::UpdateData()
{
	m_TagMap->AddTag( "SampleRateFactor", int ( m_SpinFactor->GetValue() ) );
}
