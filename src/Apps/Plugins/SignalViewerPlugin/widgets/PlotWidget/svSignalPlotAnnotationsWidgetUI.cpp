// -*- C++ -*- generated by wxGlade 0.6.3 on Fri Nov 04 17:02:29 2011

#include "svSignalPlotAnnotationsWidgetUI.h"

// begin wxGlade: ::extracode

// end wxGlade


svSignalPlotAnnotationsWidgetUI::svSignalPlotAnnotationsWidgetUI(wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    wxPanel(parent, id, pos, size, wxTAB_TRAVERSAL)
{
    // begin wxGlade: svSignalPlotAnnotationsWidgetUI::svSignalPlotAnnotationsWidgetUI
    sizer_1_staticbox = new wxStaticBox(this, -1, wxT("Signal Annotations"));
    sizer_5_staticbox = new wxStaticBox(this, -1, wxT("Parameters"));
    const wxString *m_listBoxAnnotations_choices = NULL;
    m_listBoxAnnotations = new wxListBox(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, m_listBoxAnnotations_choices, 0);
    button_1 = new wxButton(this, wxID_REMOVE, wxT("Remove"));
    label_2 = new wxStaticText(this, wxID_ANY, wxT("Type"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE);
    const wxString *m_cmbType_choices = NULL;
    m_cmbType = new wxComboBox(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0, m_cmbType_choices, wxCB_DROPDOWN);
    label_3_copy = new wxStaticText(this, wxID_ANY, wxT("Start"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE);
    m_txtStart = new wxTextCtrl(this, wxID_TXT_START, wxEmptyString);
    label_3 = new wxStaticText(this, wxID_ANY, wxT("Duration"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE);
    m_txtDuration = new wxTextCtrl(this, wxID_TXT_DURATION, wxEmptyString);
    m_btnApply = new wxButton(this, wxID_APPLY, wxT("Apply"));

    set_properties();
    do_layout();
    // end wxGlade
}


BEGIN_EVENT_TABLE(svSignalPlotAnnotationsWidgetUI, wxPanel)
    // begin wxGlade: svSignalPlotAnnotationsWidgetUI::event_table
    EVT_LISTBOX(wxID_ANY, svSignalPlotAnnotationsWidgetUI::OnSelectedAnnotation)
    EVT_BUTTON(wxID_REMOVE, svSignalPlotAnnotationsWidgetUI::OnRemoveButton)
    EVT_COMBOBOX(wxID_ANY, svSignalPlotAnnotationsWidgetUI::OnComboSelected)
    EVT_BUTTON(wxID_APPLY, svSignalPlotAnnotationsWidgetUI::OnApplyButton)
    // end wxGlade
END_EVENT_TABLE();


void svSignalPlotAnnotationsWidgetUI::OnSelectedAnnotation(wxCommandEvent &event)
{
    event.Skip();
    wxLogDebug(wxT("Event handler (svSignalPlotAnnotationsWidgetUI::OnSelectedAnnotation) not implemented yet")); //notify the user that he hasn't implemented the event handler yet
}


void svSignalPlotAnnotationsWidgetUI::OnRemoveButton(wxCommandEvent &event)
{
    event.Skip();
    wxLogDebug(wxT("Event handler (svSignalPlotAnnotationsWidgetUI::OnRemoveButton) not implemented yet")); //notify the user that he hasn't implemented the event handler yet
}


void svSignalPlotAnnotationsWidgetUI::OnComboSelected(wxCommandEvent &event)
{
    event.Skip();
    wxLogDebug(wxT("Event handler (svSignalPlotAnnotationsWidgetUI::OnComboSelected) not implemented yet")); //notify the user that he hasn't implemented the event handler yet
}


void svSignalPlotAnnotationsWidgetUI::OnApplyButton(wxCommandEvent &event)
{
    event.Skip();
    wxLogDebug(wxT("Event handler (svSignalPlotAnnotationsWidgetUI::OnApplyButton) not implemented yet")); //notify the user that he hasn't implemented the event handler yet
}


// wxGlade: add svSignalPlotAnnotationsWidgetUI event handlers


void svSignalPlotAnnotationsWidgetUI::set_properties()
{
    // begin wxGlade: svSignalPlotAnnotationsWidgetUI::set_properties
    // end wxGlade
}


void svSignalPlotAnnotationsWidgetUI::do_layout()
{
    // begin wxGlade: svSignalPlotAnnotationsWidgetUI::do_layout
    wxStaticBoxSizer* sizer_1 = new wxStaticBoxSizer(sizer_1_staticbox, wxVERTICAL);
    wxBoxSizer* sizer_6 = new wxBoxSizer(wxHORIZONTAL);
    wxStaticBoxSizer* sizer_5 = new wxStaticBoxSizer(sizer_5_staticbox, wxVERTICAL);
    wxBoxSizer* sizer_4 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* sizer_4_copy = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* sizer_3 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* sizer_2 = new wxBoxSizer(wxHORIZONTAL);
    sizer_2->Add(m_listBoxAnnotations, 1, wxEXPAND, 0);
    sizer_1->Add(sizer_2, 1, wxEXPAND, 0);
    sizer_1->Add(button_1, 0, wxALL|wxALIGN_RIGHT, 5);
    sizer_3->Add(label_2, 1, wxEXPAND, 0);
    sizer_3->Add(m_cmbType, 1, 0, 0);
    sizer_5->Add(sizer_3, 0, wxEXPAND, 0);
    sizer_4_copy->Add(label_3_copy, 1, wxEXPAND, 0);
    sizer_4_copy->Add(m_txtStart, 1, 0, 0);
    sizer_5->Add(sizer_4_copy, 0, wxEXPAND, 0);
    sizer_4->Add(label_3, 1, wxEXPAND, 0);
    sizer_4->Add(m_txtDuration, 1, 0, 0);
    sizer_5->Add(sizer_4, 0, wxEXPAND, 0);
    sizer_1->Add(sizer_5, 0, wxEXPAND, 0);
    sizer_6->Add(20, 20, 1, wxEXPAND, 0);
    sizer_6->Add(m_btnApply, 0, wxALL|wxALIGN_RIGHT, 5);
    sizer_1->Add(sizer_6, 0, wxEXPAND, 0);
    SetSizer(sizer_1);
    sizer_1->Fit(this);
    // end wxGlade
}

