/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _svSignalPlotMapper_H
#define _svSignalPlotMapper_H

// Core
#include "coreSmartPointerMacros.h"
#include "coreObject.h"
#include "coreDataEntity.h"
#include "coreDataEntityHolder.h"
#include "coreCommonDataTypes.h"
#include "svSignalPlotWindow.h"

#include "mathplot.h"



/**
\brief Parameters to configure plot widget
\ingroup SignalViewerPlugin
\author Xavi Planes
\date 27 April 2010
*/
class PLUGIN_EXPORT svPlotParameters : public Core::SmartPointerObject
{
public:
	coreDeclareSmartPointerClassMacro( svPlotParameters, Core::SmartPointerObject );
	double GetWindowSize() const { return m_WindowSize; }
	void SetWindowSize(double val) { m_WindowSize = val; }
	double GetWindowOffset() const { return m_WindowOffset; }
	void SetWindowOffset(double val) { m_WindowOffset = val; }
private:
	svPlotParameters( ){
		m_WindowSize = 10;
		m_WindowOffset = 0;
	};
	double m_WindowSize;
	double m_WindowOffset;
};
typedef Core::DataHolder<svPlotParameters::Pointer> svPlotParametersHolderType;

/**
Mediator for the plot widget. 
Holds a list of svSignalPlotWindow and a list of svSignalLayer and decides
on what window will be each curve plotted

Create a new plot window
\param parent The parent ID: should be the plot widget
\param dataEntity used to get the number of leads in the dataEntity in 
order to resize the number of windows
\return The list of windows that will be set in the plot widget

\ingroup SignalViewerPlugin
\author Chiara Riccobene
\date may  2009 
*/

class PLUGIN_EXPORT svSignalPlotMapper : public Core::BaseProcessor
{

	//! Map signals to windows
	class SignalMapInfo
	{
	public:
		std::string windowName;
		blSignal::Pointer signal;
		svSignalPlotWindow* signalPlotWindow;
	};

public:
	//!
	coreDeclareSmartPointerClassMacro(svSignalPlotMapper, Core::BaseProcessor);
	
	typedef svSignalPlotWindow SignalWindowType;
	typedef std::vector<SignalWindowType*> SignalWindowVectorType;

	//!
	void SetParentWindow(wxWindow* val);

	//!
	void Update( );

	//!
	void Clear();

	//!
	void SynchronizeAll( bool val );

	//!
	bool IsSyncEnabled( );

	//!
	void ZoomInYAll( );

	//!
	void ZoomOutYAll( );

	//!
	void FitAll( );

	//!
	std::vector<mpWindow*> GetsvSignalPlotWindowList() const;

	//!
	void SetTimeStepHolder(Core::IntHolderType::Pointer val);

	//!
	svPlotParametersHolderType::Pointer GetParamHolder() const;

	//!
	void UpdateRenderingProperties( );

	//! Get Metadata rendering tags for a signal. If it's null, create an empty one
	static blTagMap::Pointer GetSignalMetadataRendering(
		Core::DataEntity::Pointer dataEntity,
		const std::string &signalName );

private:
	//! Default constructor
	svSignalPlotMapper();
	
	//!
	~svSignalPlotMapper();

	//! Plot the signals in the previously created window
	void PlotSignalValues( );

	//!
	void OnModifiedParams( );

	/** Update rendering properties from signal metadata to window
	Available rendering properties for each signal (using name as key) are:
	- "color": std::string, like "0.3,0.5,0.9"
	- "width": int
	- "group": std::string, group name
	- "Legend": blTagMap::Pointer
	  - "x position", int, position in pixels
	- "continuity":bool: draw points with continue line
	- "Draw Annotations Markers": bool: draw circles at the top of the annotations
	*/
	void UpdateRenderingProperties( blSignal::Pointer signal, svSignalPlotWindow* window );

	//!
	svSignalPlotWindow* GetPlotWindow( const std::string &name );

	//! Add all windows to parent window
	void DoLayout( );

	/** Check if we need to update the number of windows
	\param [out] createdWindowsMap the windows that are already created and attached 
	to an input signal
	*/
	bool CheckUpdateWindows(
		std::map<blSignal::Pointer,SignalMapInfo> &createdWindowsMap );

	//!
	void CleanWindowList( );

	//!
	void UpdateSignals( const std::map<blSignal::Pointer,SignalMapInfo> &createdWindowsMap );

private:
	
	//! The Plot windows
	std::vector<mpWindow*> m_svSignalPlotWindowList;

	//! Parent window of all windows
	wxWindow* m_ParentWindow;

	//!
	Core::IntHolderType::Pointer m_timeStepHolder;

	//!
	svPlotParametersHolderType::Pointer m_ParamHolder;
};

#endif //_svSignalPlotMapper_H
