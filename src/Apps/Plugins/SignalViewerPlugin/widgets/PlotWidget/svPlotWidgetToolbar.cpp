/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "svPlotWidgetToolbar.h"
#include "svPlotManagerWidget.h"
#include "svPlotWidget.h"
#include "svSignalPlotAnnotationsWidget.h"
#include "svPlotFilterAnnotationsWidget.h"
#include "svConfigurationWidget.h"

#include "Annotations.xpm"
#include "Synchronize.xpm"
#include "Filter.xpm"
#include "Config.xpm"

#define wxID_SYNCHRONIZE wxID( "wxID_SYNCHRONIZE" )

BEGIN_EVENT_TABLE(svPlotWidgetToolbar, Core::Widgets::ToolbarBase)
  EVT_TOOL(wxID_ANNOTATIONS, svPlotWidgetToolbar::OnBtnAnnotations)
  EVT_TOOL(wxID_SYNCHRONIZE, svPlotWidgetToolbar::OnBtnSynchronize)
  EVT_TOOL(wxID_FILTER_ANNOTATIONS, svPlotWidgetToolbar::OnBtnFilterAnnotations)
  EVT_TOOL(wxID_SIGNAL_VIEWER_CONFIGURATION, svPlotWidgetToolbar::OnBtnConfiguration)
END_EVENT_TABLE()


svPlotWidgetToolbar::svPlotWidgetToolbar(wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
    Core::Widgets::ToolbarBase(parent, id, pos, size, style, name)
{
	wxBitmap bitmap = wxBitmap( annotations_xpm );
	AddTool(wxID_ANNOTATIONS, _T("Annotations"),
		bitmap, _T("Show Annotations widget"), wxITEM_CHECK);

	bitmap = wxBitmap( synchronize_xpm );
	AddTool(wxID_SYNCHRONIZE, _T("Synchronize"),
		bitmap, _T("Synchronize with parent data"), wxITEM_CHECK);

	bitmap = wxBitmap( filter_xpm );
	AddTool(wxID_FILTER_ANNOTATIONS, _T("Filter annotations"),
		bitmap, _T("Filter annotations"), wxITEM_CHECK);

	bitmap = wxBitmap( config_xpm );
	AddTool(wxID_SIGNAL_VIEWER_CONFIGURATION, _T("Signal Viewer Configuration"),
		bitmap, _T("Signal Viewer Configuration"), wxITEM_CHECK);
	
	Realize();
}


svPlotManagerWidget* svPlotWidgetToolbar::GetsvPlotManagerWidget() const
{
	return m_svPlotManagerWidget;
}

void svPlotWidgetToolbar::SetsvPlotManagerWidget( svPlotManagerWidget* val )
{
	m_svPlotManagerWidget = val;
}

void svPlotWidgetToolbar::OnBtnAnnotations(wxCommandEvent &event)
{
	try
	{
		m_svPlotManagerWidget->Show( 
			wxID_ANNOTATIONS,
			GetToolToggled( wxID_ANNOTATIONS ) );
	}
	coreCatchExceptionsReportAndNoThrowMacro( svPlotWidgetToolbar::OnBtnAnnotations )
}

void svPlotWidgetToolbar::OnBtnSynchronize( wxCommandEvent &event )
{
	try
	{
		m_svPlotManagerWidget->GetPlotWidget()->SynchronizeAll(
			GetToolToggled( wxID_SYNCHRONIZE ) );
	}
	coreCatchExceptionsReportAndNoThrowMacro( svPlotWidgetToolbar::OnBtnSynchronize )

	// Check if synchronization is enabled
	ToggleTool( wxID_SYNCHRONIZE, m_svPlotManagerWidget->GetPlotWidget()->IsSynchronized( ) );
}

void svPlotWidgetToolbar::OnBtnFilterAnnotations( wxCommandEvent &event )
{
	try
	{
		m_svPlotManagerWidget->Show( 
			wxID_FILTER_ANNOTATIONS,
			GetToolToggled( wxID_FILTER_ANNOTATIONS ) );
	}
	coreCatchExceptionsReportAndNoThrowMacro( svPlotWidgetToolbar::OnBtnFilterAnnotations )
}

void svPlotWidgetToolbar::OnBtnConfiguration( wxCommandEvent &event )
{
	try
	{
		m_svPlotManagerWidget->Show( 
			wxID_SIGNAL_VIEWER_CONFIGURATION,
			GetToolToggled( wxID_SIGNAL_VIEWER_CONFIGURATION ) );
	}
	coreCatchExceptionsReportAndNoThrowMacro( svPlotWidgetToolbar::OnBtnConfiguration )
}
