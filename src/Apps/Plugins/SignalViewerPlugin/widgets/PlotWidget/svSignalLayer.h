/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _svSignalLayer_H
#define _svSignalLayer_H

// Core
#include "mathplot.h"
#include "blSignal.h"

class svSignalAnnotationsLayer;
class svScaleXLayer;
class svScaleYLayer;

/**
Signal, annotations, axis and legend layers for each blSignal

\ingroup SignalViewerPlugin
\author Xavi Planes
\date 14 Jan 2010
*/
class PLUGIN_EXPORT svSignalLayer
{
public:
	//!
	svSignalLayer( );

	//! Return the signal
	blSignal::Pointer GetSignal() const;

	//! Set a new signal and create the layer m_svSignalLayer
	void SetSignal( blSignal::Pointer signal );

	//! Create the layers for annotations, axis and legend
	void CreateAllLayers( );

	//! Add layers to the window
	void AddLayers( mpWindow *window );

	//! Remove layers from the window
	void DelLayers( mpWindow *window );

	//! Update signal pointer to m_svSignalLayer
	void UpdateSignal( blSignal::Pointer signal );

	//! m_svSignalAnnotationsLayer
	svSignalAnnotationsLayer* GetsvSignalAnnotationsLayer() const;

	//! m_ls_x_axis
	svScaleXLayer* GetsvScaleXLayer() const;

	//! m_ls_y_axis
	svScaleYLayer* GetsvScaleYLayer() const;

	//!
	mpLayer* GetSignalLayer() const;
	mpInfoLegend* GetLegendLayer() const;

protected:
	//!
	mpLayer* m_svSignalLayer;

	//!
	svSignalAnnotationsLayer* m_svSignalAnnotationsLayer;

	//!
	mpInfoLegend* m_LegendLayer;

	//!
	svScaleXLayer* m_ls_x_axis;

	//!
	svScaleYLayer* m_ls_y_axis;
};

#endif //_svSignalLayer_H
