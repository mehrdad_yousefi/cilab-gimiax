/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _svSignalEventData_H
#define _svSignalEventData_H

#include <wx/listbox.h>

#include "blSignalEvent.h"


/**
Client data of list box or combo box for a blSignalEvent

\ingroup SignalViewerPlugin
\author Xavi Planes
\date 25 Jan 2010 
*/
class PLUGIN_EXPORT svSignalEventData : public wxClientData
{
public:
	//!
	svSignalEventData( blSignalEvent& event );

	//!
	blSignalEvent &GetSignalEvent( );

protected:
	//! 
	blSignalEvent m_SignalEvent;
};

#endif //_svSignalEventData_H
