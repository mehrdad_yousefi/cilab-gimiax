/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreToolbarBase.h"

#ifndef _svPlotWidgetToolbar_H
#define _svPlotWidgetToolbar_H

class svPlotManagerWidget;

/**
\brief Layout for plot widgets
\ingroup SignalViewerPlugin
\author Xavi Planes
\date 4 Jan 2010
*/
class PLUGIN_EXPORT svPlotWidgetToolbar: public Core::Widgets::ToolbarBase {
public:

	svPlotWidgetToolbar(wxWindow* parent, wxWindowID id, 
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = wxAUI_TB_DEFAULT_STYLE, 
		const wxString& name = wxPanelNameStr);

	//!
	svPlotManagerWidget* GetsvPlotManagerWidget() const;
	void SetsvPlotManagerWidget(svPlotManagerWidget* val);

protected:

	//!
    void OnBtnAnnotations(wxCommandEvent &event);

	//!
	void OnBtnSynchronize(wxCommandEvent &event);
	
	//!
	void OnBtnFilterAnnotations(wxCommandEvent &event);

	//!
	void OnBtnConfiguration(wxCommandEvent &event);
	
    wxDECLARE_EVENT_TABLE();
private:

	//!
	svPlotManagerWidget* m_svPlotManagerWidget;
};

#endif // _svPlotWidgetToolbar_H
