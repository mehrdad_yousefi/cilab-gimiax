/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _svSignalFreeAnnotationDialog_H
#define _svSignalFreeAnnotationDialog_H


#include "svSignalFreeAnnotationDialogUI.h"

/**
Modal dialog to show a text control

\ingroup SignalViewerPlugin
\author Xavi Planes
\date 9 June 2010 
*/
class PLUGIN_EXPORT svSignalFreeAnnotationDialog: public svSignalFreeAnnotationDialogUI {
public:
    svSignalFreeAnnotationDialog(wxWindow* parent, int id, const wxString& title, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=wxDEFAULT_DIALOG_STYLE);

	//!
	wxTextCtrl* GetTxtName() const;
private:

	//!
	void OnTextEnter(wxCommandEvent &event);

protected:
};


#endif // _svSignalFreeAnnotationDialog_H
