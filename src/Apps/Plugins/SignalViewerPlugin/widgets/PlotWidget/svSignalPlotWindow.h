/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _svSignalPlotWindow_H
#define _svSignalPlotWindow_H

// Core
#include "mathplot.h"
#include "blSignal.h"
#include "blSignalEvent.h"
#include "coreDataEntityHolder.h"
#include "coreCommonDataTypes.h"
#include "svSignalLayer.h"

class svSignalAnnotationsLayer;
class svSignalAnnotationDialog;

#define wxID_ADD_PLOT_ANNOTATION wxID( "wxID_ADD_PLOT_ANNOTATION" )
#define wxID_ADD_FREE_ANNOTATION wxID( "wxID_ADD_FREE_ANNOTATION" )
#define wxID_SYNCHRONIZE_WITH_DATA wxID( "wxID_SYNCHRONIZE_WITH_DATA")
#define wxID_EDIT_PLOT_ANNOTATION wxID( "wxID_EDIT_PLOT_ANNOTATION" )
#define wxID_ZOOM_IN_Y wxID( "wxID_ZOOM_IN_Y" )
#define wxID_ZOOM_OUT_Y wxID( "wxID_ZOOM_OUT_Y" )
#define wxID_CONTINUOUS wxID( "wxID_CONTINUOUS" )

/**
Vertical line layer for time position

\ingroup SignalViewerPlugin
\author Martin Bianculli
\date 21 July 2009 
*/
class PLUGIN_EXPORT svVerticalLineLayer : public mpFY
{

public:
	svVerticalLineLayer(double x) : mpFY(wxT("Time")) {this->m_X = x;}
	virtual double GetX( double y ) { return this->m_X; }
	//void SetX(double x){this->m_X = x;}
	virtual double GetMinX() { return this->m_X; }
	virtual double GetMaxX() { return this->m_X; }
	virtual double GetMinY() { return 0; }
	virtual double GetMaxY() { return 0; }

private:
	double m_X;
};

/**
Plot window to manage blSignal objects

\ingroup SignalViewerPlugin
\author Xavi Planes
\date 21 July 2009 
*/
class PLUGIN_EXPORT svSignalPlotWindow : public mpWindow
{
public:
	typedef std::vector<svSignalLayer> svSignalLayersVectorType;

public:
	//! Default constructor
	/*!
	 \param signalDataHolder The holder of the signal. 
	*/
	svSignalPlotWindow( wxWindow *parent, wxWindowID id);

	//!
	~svSignalPlotWindow();

	//! Set time holder to update time vertical line m_VerticalLine
	void SetTimeStepHolder(Core::IntHolderType::Pointer timeStepHolder);

	//! Set input data holder to update time vertical line m_VerticalLine using father of signal
	Core::DataEntityHolder::Pointer GetInputSignalDataHolder() const;
	void SetInputSignalDataHolder(Core::DataEntityHolder::Pointer val);

	//! To notify observers of Data entity list that the signal has changed
	Core::DataEntityHolder::Pointer GetOutputSignalDataHolder() const;
	void SetOutputSignalDataHolder(Core::DataEntityHolder::Pointer val);

	/** Create a svSignalLayer, add it to m_svSignalLayers and call Fit( ) and UpdateView( )
	\note if layer is already created, don't do anything
	*/
	void AddSignal( blSignal::Pointer signal );

	//! Update data of a already used signal
	void UpdateSignal( blSignal::Pointer signal );

	//! Remove all layers and clean m_svSignalLayers
	void ResetSignals( );

	//! Find svSignalLayer for a concrete signal
	svSignalLayer* GetSignalLayer( blSignal::Pointer signal );

	//!
	svSignalPlotWindow::svSignalLayersVectorType& GetSignalLayers();

	//!
	void OnMouseLeftRelease(wxMouseEvent &event);
	
	//!
	void OnMouseMoved(wxMouseEvent &event);

	//!
	void OnSyncPlotWithParent(wxCommandEvent& event);

	//! Update m_VerticalLine position
	void OnTimeStepModified();

	//!
	bool GetIsSyncEnable() const;
	void Synchronize(bool val);

	//!
	void ZoomInY();

	//!
	void ZoomOutY();

	//!
	void SetWindowOffset( double offset );

	//!
	void SetWindowSize( double size );

	//! Fit the current view depending on m_IsSyncEnable
	void UpdateView( );

private:
	
    wxDECLARE_EVENT_TABLE();

	//!
	void OnAddPlotAnnotation(wxCommandEvent& event );

	//!
	void OnAddFreeAnnotation(wxCommandEvent& event );
	
	//!
	void OnZoomInY( wxCommandEvent& event );

	//!
	void OnZoomOutY( wxCommandEvent& event );

	//!
	void OnContinuous( wxCommandEvent& event );

	// Synchronize current time step with plot
	void Sync();

	//!
	void StartDrawDuration( );

	//!
	void EndDrawDuration( int releaseX, int releaseY );

	//!
	bool ShowAnnotationList( blSignalEvent &selectedEvent );

	//!
	std::string AskFreeAnnotationName( );

private:

	//! Curves
	svSignalLayersVectorType m_svSignalLayers;

	//! Use to synchronize the time
	Core::DataEntityHolder::Pointer m_InputSignalDataHolder;

	//! To notify observers of Data entity list that the signal has changed
	Core::DataEntityHolder::Pointer m_OutputSignalDataHolder;

	//! Holds the current time step, used to synchronize with data when needed
	Core::IntHolderType::Pointer m_timeStepHolder;

	//!
	boost::signals::connection m_timeStepHolderConnection;

	//! If true, synchronize the plot window time step with the data
	bool m_IsSyncEnable;

	//!
	svVerticalLineLayer* m_VerticalLine;

	//! An input for storing the annotation text inserted by the user
	svSignalAnnotationDialog* m_AnnotationDialog; 

	//! In sec.
	double m_WindowOffset;

	//! In sec.
	double m_WindowSize;
};

#endif //_svSignalPlotWindow_H
