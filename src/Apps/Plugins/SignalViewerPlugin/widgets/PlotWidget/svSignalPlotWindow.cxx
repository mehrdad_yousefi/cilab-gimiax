// Copyright 2006 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "svSignalPlotWindow.h"
#include "svSignalAnnotationsLayer.h"
#include "wxID.h"
#include <wx/textctrl.h>
#include "blMitkUnicode.h"
#include "coreDataEntityHelper.h"

#include "coreKernel.h"
#include "svSignalDataLayer.h"
#include "svSignalAnnotationDialog.h"
#include "svSignalAnnotationListBox.h"
#include "svSignalFreeAnnotationDialog.h"
#include "svSignalPlotMapper.h"

#include <blSignalEvent.h>
#include "blSignalCollective.h"

BEGIN_EVENT_TABLE(svSignalPlotWindow, mpWindow)
  EVT_MENU( wxID_ADD_PLOT_ANNOTATION,    svSignalPlotWindow::OnAddPlotAnnotation)
  EVT_MENU( wxID_ADD_FREE_ANNOTATION,    svSignalPlotWindow::OnAddFreeAnnotation)
  EVT_MENU( wxID_SYNCHRONIZE_WITH_DATA, svSignalPlotWindow::OnSyncPlotWithParent)
  EVT_MENU( wxID_ZOOM_IN_Y, svSignalPlotWindow::OnZoomInY)
  EVT_MENU( wxID_ZOOM_OUT_Y, svSignalPlotWindow::OnZoomOutY)
  EVT_MENU( wxID_CONTINUOUS, svSignalPlotWindow::OnContinuous)
  EVT_LEFT_UP( svSignalPlotWindow::OnMouseLeftRelease)
  EVT_MOTION(svSignalPlotWindow::OnMouseMoved)
END_EVENT_TABLE();



svSignalPlotWindow::svSignalPlotWindow( wxWindow *parent, wxWindowID id ) : mpWindow( parent, id )
{
	SetMinSize( wxDefaultSize );
	SetInitialSize( wxDefaultSize );

	// Eliminate the flicker
	EnableDoubleBuffer( true );

	m_IsSyncEnable = false;

	m_VerticalLine = NULL;

	// Add pop up menu
	wxMenu* popupMenu = GetPopupMenu( );
	popupMenu->Append( 
		wxID_SYNCHRONIZE_WITH_DATA, 
		_("Synchronize with parent data"), 
		_("Synchronize with parent data"),
		wxITEM_CHECK);

	popupMenu->Append( wxID_ZOOM_IN_Y,_("Zoom in Y"), _("Zoom in Y"));
	popupMenu->Append( wxID_ZOOM_OUT_Y,_("Zoom out Y"), _("Zoom out Y"));
	popupMenu->Append( 
		wxID_ADD_PLOT_ANNOTATION,
		_("Add annotation"), 
		_("Add annotation to the current position"));
	popupMenu->Append( 
		wxID_ADD_FREE_ANNOTATION,
		_("Add Free annotation"), 
		_("Add Free annotation to the current position"));
	wxMenuItem* item = popupMenu->AppendCheckItem( 
		wxID_CONTINUOUS,
		_("Continuity"), 
		_("Draws a continuous line") );
	item->Check( true );


	m_AnnotationDialog = NULL;

	m_WindowOffset = 0;
	m_WindowSize = 10;
}

svSignalPlotWindow::~svSignalPlotWindow()
{
	if ( m_timeStepHolderConnection.connected() )
	{
		m_timeStepHolderConnection.disconnect();
	}
}


void svSignalPlotWindow::SetTimeStepHolder(Core::IntHolderType::Pointer timeStepHolder)
{
	m_timeStepHolderConnection.disconnect( );
	m_timeStepHolder = timeStepHolder;
	m_timeStepHolderConnection = m_timeStepHolder->AddObserver(
		this, 
		&svSignalPlotWindow::OnTimeStepModified );
}	



void svSignalPlotWindow::ResetSignals()
{
	for( svSignalLayersVectorType::iterator it = m_svSignalLayers.begin();
		it != m_svSignalLayers.end(); it++)
	{
		it->DelLayers( this );
	}

	while ( m_svSignalLayers.size() )
	{
		m_svSignalLayers.erase( m_svSignalLayers.begin() );
	}
}

void svSignalPlotWindow::AddSignal( blSignal::Pointer signal )
{
	if ( GetSignalLayer( signal) )
	{
		UpdateSignal( signal );
		return;
	}

	svSignalLayer svSignalLayers;
	svSignalLayers.SetSignal( signal );

	// Only create all layers for the first signal
	if ( m_svSignalLayers.empty() )
	{
		svSignalLayers.CreateAllLayers();
	}

	svSignalLayers.AddLayers( this );
	m_svSignalLayers.push_back( svSignalLayers );

	// Compute initial bounding box
	Fit( );
	UpdateView();
}


void svSignalPlotWindow::OnSyncPlotWithParent(wxCommandEvent& event)
{
	try
	{
		Synchronize( GetPopupMenu( )->IsChecked( wxID_SYNCHRONIZE_WITH_DATA ) );
	}
	coreCatchExceptionsReportAndNoThrowMacro( svSignalPlotWindow::OnSyncPlotWithParent )
}

void svSignalPlotWindow::OnTimeStepModified()
{
	try
	{

		// clear all the lines
		if (m_VerticalLine!= NULL)
			this->DelLayer(m_VerticalLine);

		int currentTimeStep = m_timeStepHolder->GetSubject( );

		Core::DataEntity::Pointer father = m_InputSignalDataHolder->GetSubject()->GetFather();
		if ( father.IsNotNull() )
		{
			float currentTime = father->GetTimeAtTimeStep(currentTimeStep);		
			if ( currentTime != Core::DataEntityImpl::CONSTANT_IN_TIME )
			{
				m_VerticalLine = new svVerticalLineLayer( currentTime );
				m_VerticalLine->SetDrawOutsideMargins(false);
				this->AddLayer(m_VerticalLine);
			}
		}

		UpdateView();
	}
	coreCatchExceptionsLogAndNoThrowMacro( svSignalPlotWindow::OnTimeStepModified )
}

void svSignalPlotWindow::Sync()
{
	if ( m_InputSignalDataHolder->GetSubject().IsNull() )
	{
		return;
	}

	if ( m_InputSignalDataHolder->GetSubject()->GetFather().IsNull() )
	{
		throw Core::Exceptions::Exception( 
			"svSignalPlotWindow::Sync",
			"Please set a parent data to Synchronize the signal" );
	}

	int numberOfTimeSteps = m_InputSignalDataHolder->GetSubject()->GetFather()->GetNumberOfTimeSteps();
	int currentTimeStep = m_timeStepHolder->GetSubject();
	Core::DataEntity::Pointer fatherData = m_InputSignalDataHolder->GetSubject()->GetFather();

	if ( fatherData.IsNull( ) )
	{
		return;
	}

	double currentImageTime;
	currentImageTime = fatherData->GetTimeAtTimeStep( currentTimeStep );

	// In sec.
	currentImageTime += m_WindowOffset;

	// In sec.
	double windowSize = 10;
	double time1 = currentImageTime - m_WindowSize / 2;
	double time2 = currentImageTime + m_WindowSize / 2;

	blSignalCollective::Pointer signal;
	Core::DataEntityHelper::GetProcessingData(m_InputSignalDataHolder,  signal);

	Fit(time1, time2 , m_desiredYmin, m_desiredYmax );

	// Hide Time label
	if ( m_VerticalLine )
	{
		m_VerticalLine->SetName( "" );
	}
}

bool svSignalPlotWindow::GetIsSyncEnable() const
{
	return m_IsSyncEnable;
}

void svSignalPlotWindow::Synchronize( bool val )
{
	bool oldSyncValue = m_IsSyncEnable;
	m_IsSyncEnable = val;

	// Reset bounding box
	Fit( );

	try
	{
		// Throws esxception if synchronization cannot be enabled
		UpdateView();
	}
	catch( ... )
	{
		// Restore previous value
		m_IsSyncEnable = oldSyncValue;
		throw;
	}
}

void svSignalPlotWindow::OnAddPlotAnnotation( 
	wxCommandEvent& event )
{
	// select the first curve, later, add a mark for every curve 
	svSignalLayersVectorType::iterator it = m_svSignalLayers.begin(); 
	unsigned pos;
	bool proceed = it->GetSignal( )->FindXValuePosition(p2x(m_clickedX), pos);

	// break if there user clicked outisde the range of the graph
	if (!proceed)
	{
		return;
	}

	blSignalEvent selectedEvent;
	bool selected = ShowAnnotationList( selectedEvent );
	if ( !selected )
	{
		return;
	}

	m_svSignalLayers[ 0 ].GetsvSignalAnnotationsLayer( )->StartAddAnnotation( 
		selectedEvent, 
		m_clickedX,
		m_clickedY,
		p2x(m_clickedX) );

	if ( m_svSignalLayers[ 0 ].GetsvSignalAnnotationsLayer( )->GetDrawingInterval() )
	{
		this->SetCursor(wxCURSOR_SIZEWE);
	}
	else
	{
		m_OutputSignalDataHolder->GetSubject()->Modified( );
		m_OutputSignalDataHolder->NotifyObservers( );
	}

	this->Refresh();

}

void svSignalPlotWindow::OnMouseLeftRelease( wxMouseEvent &event )
{
	if ( m_svSignalLayers.empty() ||
		 !m_svSignalLayers[ 0 ].GetsvSignalAnnotationsLayer( )->GetDrawingInterval() )
	{
		event.Skip();
		return;
	}

	m_svSignalLayers[ 0 ].GetsvSignalAnnotationsLayer( )->EndAddAnnotation( 
		p2x( event.GetX() ) );

	// Update ES And ED time steps
	if ( m_OutputSignalDataHolder.IsNotNull( ) )
	{
		m_OutputSignalDataHolder->GetSubject()->Modified( );
		m_OutputSignalDataHolder->NotifyObservers( );
	}

	this->SetCursor(wxCursor(wxCURSOR_ARROW));
}

void svSignalPlotWindow::OnMouseMoved( wxMouseEvent &event )
{
	if ( m_svSignalLayers.size( ) == 0 || 
		 !m_svSignalLayers[ 0 ].GetsvSignalAnnotationsLayer( )->GetDrawingInterval() )
	{
		event.Skip();
		return;
	}

	m_svSignalLayers[ 0 ].GetsvSignalAnnotationsLayer( )->OnMouseMove( p2x( event.GetX() ) );

	Refresh( );
}

bool svSignalPlotWindow::ShowAnnotationList( blSignalEvent &selectedEvent )
{
	wxPoint screenPoint;
	screenPoint = ClientToScreen( wxPoint(m_clickedX, m_clickedY) );

	m_AnnotationDialog = new svSignalAnnotationDialog( 
		this, 
		wxID_ANY,
		"Select type", 
		screenPoint, 
		wxSize(200,100),
		wxRESIZE_BORDER );
	m_AnnotationDialog->SetSize( wxSize( 200, 100 ) );
	int ret = m_AnnotationDialog->ShowModal( );

	bool selected = false;
	if ( ret == wxID_OK )
	{
		selected = m_AnnotationDialog->GetAnnotationList( )->GetSelectedEvent( selectedEvent );
	}

	m_AnnotationDialog->Destroy();
	m_AnnotationDialog= NULL;

	return selected;
}

void svSignalPlotWindow::UpdateSignal( blSignal::Pointer signal )
{
	for( svSignalLayersVectorType::iterator it = m_svSignalLayers.begin();
		it != m_svSignalLayers.end(); it++)
	{
		if ( it->GetSignal() == signal )
		{
			it->UpdateSignal( signal );
		}
	}

	UpdateView( );
}

void svSignalPlotWindow::OnZoomInY( wxCommandEvent& event )
{
  svSignalPlotWindow::ZoomInY();
}

void svSignalPlotWindow::OnZoomOutY( wxCommandEvent& event )
{
  svSignalPlotWindow::ZoomOutY( );
}

void svSignalPlotWindow::ZoomInY()
{
	Fit( 
		m_desiredXmin, 
		m_desiredXmax, 
		m_desiredYmin / zoomIncrementalFactor ,
		m_desiredYmax / zoomIncrementalFactor );
}

void svSignalPlotWindow::ZoomOutY()
{
	Fit( 
		m_desiredXmin, 
		m_desiredXmax, 
		m_desiredYmin * zoomIncrementalFactor ,
		m_desiredYmax * zoomIncrementalFactor );
}

void svSignalPlotWindow::SetWindowOffset( double offset )
{
	m_WindowOffset = offset;
	UpdateView( );
}

void svSignalPlotWindow::SetWindowSize( double size )
{
	m_WindowSize = size;
	UpdateView( );
}

void svSignalPlotWindow::UpdateView()
{
	if ( m_IsSyncEnable )
		Sync();
	else 
	{
		if ( m_VerticalLine )
		{
			m_VerticalLine->SetName( "Time" );
		}
		Fit( m_desiredXmin, 
			m_desiredXmax, 
			m_desiredYmin,
			m_desiredYmax );
	}
}

void svSignalPlotWindow::OnAddFreeAnnotation( wxCommandEvent& event )
{
	// select the first curve, later, add a mark for every curve 
	svSignalLayersVectorType::iterator it = m_svSignalLayers.begin(); 
	unsigned pos;
	bool proceed = it->GetSignal( )->FindXValuePosition(p2x(m_clickedX), pos);

	// break if there user clicked outside the range of the graph
	if (!proceed)
	{
		return;
	}

	blSignalEvent selectedEvent;
	std::string name = AskFreeAnnotationName( );
	if ( name.empty() )
	{
		return;
	}

	m_svSignalLayers[ 0 ].GetsvSignalAnnotationsLayer( )->StartAddAnnotation( 
		selectedEvent, 
		m_clickedX,
		m_clickedY,
		p2x(m_clickedX) );

	m_svSignalLayers[ 0 ].GetsvSignalAnnotationsLayer( )->GetCurrAnnotation( )->SetFreeName( name );

	if ( m_svSignalLayers[ 0 ].GetsvSignalAnnotationsLayer( )->GetDrawingInterval() )
	{
		this->SetCursor(wxCURSOR_SIZEWE);
	}
	else
	{
		m_OutputSignalDataHolder->GetSubject()->Modified( );
		m_OutputSignalDataHolder->NotifyObservers( );
	}

	this->Refresh();
}

std::string svSignalPlotWindow::AskFreeAnnotationName( )
{
	wxPoint screenPoint;
	screenPoint = ClientToScreen( wxPoint(m_clickedX, m_clickedY) );

	std::string name;
	svSignalFreeAnnotationDialog* dialog = new svSignalFreeAnnotationDialog(
		this, 
		wxID_ANY,
		"New annotation", 
		screenPoint, 
		wxSize(200,100),
		wxRESIZE_BORDER );
	dialog->SetSize( wxSize( 200, 35 ) );
	int ret = dialog->ShowModal( );
	if ( ret == wxID_OK )
	{
		name = dialog->GetTxtName( )->GetValue();
	}

	dialog->Destroy();

	return name;
}

svSignalPlotWindow::svSignalLayersVectorType& svSignalPlotWindow::GetSignalLayers()
{
	return m_svSignalLayers;
}

svSignalLayer* svSignalPlotWindow::GetSignalLayer( blSignal::Pointer signal )
{
	for( svSignalLayersVectorType::iterator it = m_svSignalLayers.begin();
		it != m_svSignalLayers.end(); it++)
	{
		if ( it->GetSignal() == signal )
		{
			return &(*it);
		}
	}

	return NULL;
}

Core::DataEntityHolder::Pointer svSignalPlotWindow::GetInputSignalDataHolder() const
{
	return m_InputSignalDataHolder;
}

void svSignalPlotWindow::SetInputSignalDataHolder( Core::DataEntityHolder::Pointer val )
{
	m_InputSignalDataHolder = val;
}

Core::DataEntityHolder::Pointer svSignalPlotWindow::GetOutputSignalDataHolder() const
{
	return m_OutputSignalDataHolder;
}

void svSignalPlotWindow::SetOutputSignalDataHolder( Core::DataEntityHolder::Pointer val )
{
	m_OutputSignalDataHolder = val;
}

void svSignalPlotWindow::OnContinuous( wxCommandEvent& event )
{
	for( svSignalLayersVectorType::iterator it = m_svSignalLayers.begin();
		it != m_svSignalLayers.end(); it++)
	{
		it->GetSignalLayer( )->SetContinuity( event.GetInt() );
		
		// Add tag to metadata
		blTagMap::Pointer signalRendering;
		signalRendering = svSignalPlotMapper::GetSignalMetadataRendering( 
			m_OutputSignalDataHolder->GetSubject(), 
			it->GetSignal( )->GetName( ) );
		
		if ( signalRendering.IsNotNull() )
		{
			signalRendering->AddTag( "continuity", bool( event.GetInt() ) );
		}
	}
}
