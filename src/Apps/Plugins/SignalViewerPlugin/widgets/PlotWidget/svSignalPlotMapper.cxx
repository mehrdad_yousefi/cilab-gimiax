// Copyright 2006 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "svSignalPlotMapper.h"
#include "coreKernel.h"
#include "coreDataEntityHelper.h"
#include "svSignalPlotWindow.h"
#include "svSignalNotebookPage.h"
#include "svSignalAnnotationsLayer.h"

#include "blSignalCollective.h"

#include "mathplot.h"

#include "blTextUtils.h"

#include "wx/wupdlock.h"

svSignalPlotMapper::svSignalPlotMapper()
{
	SetName( "SignalPlotMapper" );
	SetNumberOfInputs( 1 );
	GetInputPort( 0 )->SetName( "Input signal" );
	GetInputPort( 0 )->SetDataEntityType( Core::SignalTypeId );
	SetNumberOfOutputs( 1 );
	GetOutputPort( 0 )->SetName( "Output signal" );
	GetOutputPort( 0 )->SetDataEntityType( Core::SignalTypeId );

	m_ParentWindow = NULL;

	m_ParamHolder = svPlotParametersHolderType::New();
	m_ParamHolder->SetSubject( svPlotParameters::New() );
	m_ParamHolder->AddObserver(
		this,
		&svSignalPlotMapper::OnModifiedParams );
}

/**
*/
svSignalPlotMapper::~svSignalPlotMapper()
{
}

/**
*/
void svSignalPlotMapper::Clear()
{
}

/**
*/
void svSignalPlotMapper::PlotSignalValues( )
{
	blSignalCollective::Pointer signalCollective;
	GetProcessingData( 0, signalCollective );

	// For each svSignalPlotWindow, add the signal data
	for( int signalPos = 0 ; signalPos < signalCollective->GetNumberOfSignals(); signalPos++ )
	{
		blSignal::Pointer signal = signalCollective->GetSignal(signalPos);
		blTagMap::Pointer signalRendering;
		signalRendering = GetSignalMetadataRendering( GetInputDataEntity( 0 ), signal->GetName() );

		// Get group name
		std::string groupName;
		if ( signalRendering.IsNotNull() && signalRendering->GetTag( "group" ).IsNotNull() )
		{
			groupName = signalRendering->GetTag( "group" )->GetValueAsString();
		}

		// Find if window was already created
		svSignalPlotWindow* window = NULL;
		if ( !groupName.empty() )
		{
			window = GetPlotWindow( groupName );
		}

		// Create window
		if ( window == NULL )
		{
			window = new svSignalPlotWindow( m_ParentWindow, wxID_ANY );
			window->SetInputSignalDataHolder( GetInputDataEntityHolder( 0 ) );
			window->SetOutputSignalDataHolder( GetOutputDataEntityHolder( 0 ) );
			window->SetTimeStepHolder( m_timeStepHolder );
			window->ResetSignals( );
			if ( !groupName.empty( ) )
			{
				window->SetName( groupName );
			}
			else
			{
				window->SetName( signal->GetName( ) );
			}
			m_svSignalPlotWindowList.push_back( window );
		}

		// Add signal
		window->AddSignal( signal );
	}	
}

void svSignalPlotMapper::SynchronizeAll( bool val )
{
	std::vector<mpWindow*>::iterator it;
	for (it = m_svSignalPlotWindowList.begin();
		it != m_svSignalPlotWindowList.end();
		it++)
	{
		svSignalPlotWindow* signalPlot;
		signalPlot = dynamic_cast<svSignalPlotWindow*> ( *it );
		if ( signalPlot != NULL )
		{
			signalPlot->Synchronize( val );
		}
	}
}

void svSignalPlotMapper::UpdateSignals(
	const std::map<blSignal::Pointer,SignalMapInfo> &createdWindowsMap )
{
	wxWindowUpdateLocker lock( m_ParentWindow );

	// Reset all signals
	std::vector<mpWindow*>::iterator it;
	for (it = m_svSignalPlotWindowList.begin();
		it != m_svSignalPlotWindowList.end();
		it++)
	{
		svSignalPlotWindow* window;
		window = dynamic_cast< svSignalPlotWindow* > ( (*it) );

		// Signal layers can change. We need to reset them
		window->ResetSignals( );
	}


	// For each signal, update the window
	std::map<blSignal::Pointer,SignalMapInfo>::const_iterator itSignalInfo;
	for ( itSignalInfo = createdWindowsMap.begin( ); 
		  itSignalInfo != createdWindowsMap.end( ); 
		  itSignalInfo++ )
	{
		// Add signal
		itSignalInfo->second.signalPlotWindow->AddSignal( itSignalInfo->second.signal );
	}
}


void svSignalPlotMapper::ZoomInYAll( )
{
	std::vector<mpWindow*>::iterator it;
	for (it = m_svSignalPlotWindowList.begin();
		it != m_svSignalPlotWindowList.end();
		it++)
	{
		svSignalPlotWindow* window;
		window = dynamic_cast< svSignalPlotWindow* > ( (*it) );

		window->ZoomInY();
	}
}

void svSignalPlotMapper::ZoomOutYAll( )
{
	std::vector<mpWindow*>::iterator it;
	for (it = m_svSignalPlotWindowList.begin();
		it != m_svSignalPlotWindowList.end();
		it++)
	{
		svSignalPlotWindow* window;
		window = dynamic_cast< svSignalPlotWindow* > ( (*it) );

		window->ZoomOutY();
	}
}

void svSignalPlotMapper::FitAll()
{
	std::vector<mpWindow*>::iterator it;
	for (it = m_svSignalPlotWindowList.begin();
		it != m_svSignalPlotWindowList.end();
		it++)
	{
		svSignalPlotWindow* window;
		window = dynamic_cast< svSignalPlotWindow* > ( (*it) );

		window->Fit();
	}
}

void svSignalPlotMapper::SetParentWindow( wxWindow* val )
{
	m_ParentWindow = val;
}

void svSignalPlotMapper::Update()
{
	Core::DataEntity::Pointer dataEntity;
	dataEntity = GetInputDataEntityHolder( 0 )->GetSubject( );

	// If input is NULL -> Removed from Data List
	if ( dataEntity.IsNull() )
	{
		return;
	}

	// Group signals
	std::map<blSignal::Pointer,SignalMapInfo> createdWindowsMap;
	bool updateWindows = CheckUpdateWindows( createdWindowsMap );

	// If same number of signals, try to update faster
	if ( updateWindows )
	{
		wxWindowUpdateLocker lock( m_ParentWindow );

		CleanWindowList( );

		PlotSignalValues( );

		DoLayout( );
	}
	else
	{
		UpdateSignals( createdWindowsMap );
	}

	// Update rendering properties like legend centered after do layout
	UpdateRenderingProperties();
}

void svSignalPlotMapper::CleanWindowList()
{
	std::vector<mpWindow*>::iterator it;
	for ( it = m_svSignalPlotWindowList.begin( ) ; 
		  it != m_svSignalPlotWindowList.end();
		  it++ )
	{
		svSignalPlotWindow* signalPlot = dynamic_cast<svSignalPlotWindow*> ( *it );
		signalPlot->Destroy( );
	}

	m_svSignalPlotWindowList.resize( 0 );
}

bool svSignalPlotMapper::CheckUpdateWindows(
	std::map<blSignal::Pointer,SignalMapInfo> &createdWindowsMap )
{
	// Check if number of signals has changed
	// and new plot windows needs to be created
	blSignalCollective::Pointer signalCollective;
	GetProcessingData( 0, signalCollective);

	// For each signal, group it
	std::map<blSignal::Pointer,SignalMapInfo> windowsToCreateMap;
	for( int signalPos = 0 ; signalPos < signalCollective->GetNumberOfSignals(); signalPos++ )
	{
		blSignal::Pointer signal = signalCollective->GetSignal(signalPos);
		blTagMap::Pointer signalRendering;
		signalRendering = GetSignalMetadataRendering( GetInputDataEntity( 0 ), signal->GetName() );

		// Get group name
		SignalMapInfo info;
		if ( signalRendering.IsNotNull() && signalRendering->GetTag( "group" ).IsNotNull() )
		{
			info.windowName = signalRendering->GetTag( "group" )->GetValueAsString();
		}
		else
		{
			info.windowName = signal->GetName( );
		}
		info.signal = signal;
		windowsToCreateMap[ signal ] = info;
	}

	// Remove windows that are already created
	std::vector<mpWindow*>::iterator it;
	for (it = m_svSignalPlotWindowList.begin();
		it != m_svSignalPlotWindowList.end();
		it++)
	{
		svSignalPlotWindow* signalPlot;
		signalPlot = dynamic_cast<svSignalPlotWindow*> ( *it );

		// Find signal info
		std::map<blSignal::Pointer,SignalMapInfo>::iterator itSignalInfo;
		itSignalInfo = windowsToCreateMap.begin( );
		bool found = false;
		while ( !found && itSignalInfo != windowsToCreateMap.end( ) )
		{
			if ( itSignalInfo->second.windowName == signalPlot->GetName( ).c_str( ) )
			{
				found = true;
			}
			else
			{
				itSignalInfo++;
			}
		}

		// Move signal info to createdWindowsMap
		if ( found )
		{
			itSignalInfo->second.signalPlotWindow = signalPlot;
			createdWindowsMap[ itSignalInfo->second.signal ] = itSignalInfo->second;
			windowsToCreateMap.erase( itSignalInfo );
		}
	}

	return !windowsToCreateMap.empty( ) || createdWindowsMap.size( ) != m_svSignalPlotWindowList.size( );
}

std::vector<mpWindow*> svSignalPlotMapper::GetsvSignalPlotWindowList() const
{
	return m_svSignalPlotWindowList;
}

void svSignalPlotMapper::SetTimeStepHolder( Core::IntHolderType::Pointer val )
{
	m_timeStepHolder = val;
}

void svSignalPlotMapper::OnModifiedParams()
{
	svPlotParameters::Pointer plotParameters;
	plotParameters = m_ParamHolder->GetSubject( );

	std::vector<mpWindow*>::iterator it;
	for (it = m_svSignalPlotWindowList.begin();
		it != m_svSignalPlotWindowList.end();
		it++)
	{
		svSignalPlotWindow* signalPlot;
		signalPlot = dynamic_cast<svSignalPlotWindow*> ( *it );
		if ( signalPlot != NULL )
		{
			signalPlot->SetWindowOffset( plotParameters->GetWindowOffset( ) );
			signalPlot->SetWindowSize( plotParameters->GetWindowSize( ) );
		}
	}
}

svPlotParametersHolderType::Pointer svSignalPlotMapper::GetParamHolder() const
{
	return m_ParamHolder;
}

void svSignalPlotMapper::UpdateRenderingProperties( )
{
	std::vector<mpWindow*>::iterator it;
	for (it = m_svSignalPlotWindowList.begin();
		it != m_svSignalPlotWindowList.end();
		it++)
	{
		svSignalPlotWindow* window;
		window = dynamic_cast< svSignalPlotWindow* > ( (*it) );

		svSignalPlotWindow::svSignalLayersVectorType signalLayers;
		signalLayers = window->GetSignalLayers();

		svSignalPlotWindow::svSignalLayersVectorType::iterator it;
		for ( it = signalLayers.begin() ; it != signalLayers.end() ; it++ )
		{
			blSignal::Pointer signal = it->GetSignal( );

			// Update rendering properties
			UpdateRenderingProperties( signal, window );
		}
	}
}

void svSignalPlotMapper::UpdateRenderingProperties(
	blSignal::Pointer signal, svSignalPlotWindow* window )
{
	// Update rendering tags
	blTagMap::Pointer signalRendering = GetSignalMetadataRendering( GetInputDataEntity( 0 ), signal->GetName() );
	if ( signalRendering.IsNull( ) )
	{
		return;
	}

	svSignalLayer* layer = window->GetSignalLayer( signal );
	if ( layer == NULL )
	{
		return;
	}

	wxPen pen;

	// Update color
	blTag::Pointer tagColor = signalRendering->GetTag( "color" );
	std::string colorValue;
	if ( tagColor.IsNotNull() )
	{
		colorValue = tagColor->GetValueCasted<std::string>( );
		std::list<std::string> colorValues;
		std::list<std::string>::iterator it;
		blTextUtils::ParseLine( colorValue, ',', colorValues );
		double rgb[3];
		int count = 0;
		for ( it = colorValues.begin() ; it != colorValues.end() && count < 3 ; it++ )
		{
			rgb[ count ] = atof( it->c_str() );
			count++;
		}
		pen.SetColour( wxColour( rgb[ 0 ] * 255, rgb[ 1 ] * 255, rgb[ 2 ] * 255 ) );
	}

	// Update width
	int width = std::max( 1, signalRendering->GetTagValue<int>("width") );
	pen.SetWidth( width );

	// Set pen
	layer->GetSignalLayer( )->SetPen( pen );

	// Update legend
	blTagMap::Pointer legend = signalRendering->GetTagValue<blTagMap::Pointer>( "Legend" );
	if ( legend.IsNotNull() && layer->GetLegendLayer() )
	{
		int xPos = legend->GetTagValue<int>( "x position" );
		if ( xPos != 0 )
		{
			wxPoint delta( 0, 0 );
			// This doesn't work for the first time
			//delta.x = window->GetXScreen( ) / 2;
			delta.x = xPos;
			layer->GetLegendLayer()->Move( delta );
		}
	}

	// Continuity
	blTag::Pointer tagContinuity = signalRendering->GetTag( "continuity" );
	if ( tagContinuity.IsNotNull() )
	{
		bool continuity = tagContinuity->GetValueCasted<bool>( );
		layer->GetSignalLayer()->SetContinuity( continuity );
		wxMenuItem *menuItem = window->GetPopupMenu()->FindItem( wxID_CONTINUOUS );
		if ( menuItem )
		{
			menuItem->Check( continuity );
		}
	}

	// Annotations
	blTag::Pointer tagAnnotations = signalRendering->GetTag( "Draw Annotations Markers" );
	if ( tagAnnotations.IsNotNull() && layer->GetsvSignalAnnotationsLayer() )
	{
		bool drawMarkers = tagAnnotations->GetValueCasted<bool>( );
		layer->GetsvSignalAnnotationsLayer()->SetDrawMarkers( drawMarkers );
	}
}

blTagMap::Pointer svSignalPlotMapper::GetSignalMetadataRendering( 
	Core::DataEntity::Pointer dataEntity,
	const std::string &signalName )
{
	if ( dataEntity.IsNull() )
	{
		return NULL;
	}

	blTagMap::Pointer metadata(dataEntity->GetMetadata());
	blTagMap::Pointer rendering = metadata->GetTagValue<blTagMap::Pointer>( "Rendering" );
	if ( rendering.IsNull())
	{
		rendering = blTagMap::New( );
		metadata->AddTag( "Rendering", rendering );
	}

	blTagMap::Pointer signalRendering = rendering->GetTagValue<blTagMap::Pointer>( signalName );
	if ( signalRendering.IsNull() )
	{
		signalRendering = blTagMap::New( );
		rendering->AddTag( signalName, signalRendering );
	}

	return signalRendering;
}

svSignalPlotWindow* svSignalPlotMapper::GetPlotWindow( const std::string &name )
{
	std::vector<mpWindow*>::iterator it;
	for (it = m_svSignalPlotWindowList.begin();
		it != m_svSignalPlotWindowList.end();
		it++)
	{
		svSignalPlotWindow* signalPlot;
		signalPlot = dynamic_cast<svSignalPlotWindow*> ( *it );

		if ( signalPlot->GetName() == name)
		{
			return signalPlot;
		}
	}

	return NULL;
}

void svSignalPlotMapper::DoLayout()
{
	// If number of windows != number of signals -> Clear all old windows
	// and create new ones
	if ( m_ParentWindow->GetSizer() )
	{
		m_ParentWindow->GetSizer()->Clear( true );
	}

	wxBoxSizer* sizerVert = new wxBoxSizer(wxVERTICAL);
	std::vector<mpWindow*>::iterator it;
	for (it = m_svSignalPlotWindowList.begin(); it != m_svSignalPlotWindowList.end(); it++)
	{
		wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
		sizer->Add((*it), 1, wxEXPAND | wxALL, 2);
		sizerVert->Add(sizer,1, wxEXPAND, 0);
		wxSize size = sizerVert->GetMinSize( );
		size = size;
	}

	m_ParentWindow->SetSizer(sizerVert);

	// Resize parent
	// Cast a resize event
	wxSizeEvent resEvent( m_ParentWindow->GetBestSize(), m_ParentWindow->GetId() );
	resEvent.SetEventObject( m_ParentWindow );
	m_ParentWindow->GetEventHandler()->ProcessEvent(resEvent);
}

bool svSignalPlotMapper::IsSyncEnabled()
{
	if ( m_svSignalPlotWindowList.empty() )
	{
		return false;
	}

	svSignalPlotWindow* signalPlot;
	signalPlot = dynamic_cast<svSignalPlotWindow*> ( *m_svSignalPlotWindowList.begin() );
	if ( signalPlot != NULL )
	{
		return signalPlot->GetIsSyncEnable( );
	}

	return false;
}
