// -*- C++ -*- generated by wxGlade 0.6.3 on Wed Jun 15 12:55:06 2011

#include <wx/wx.h>
#include <wx/image.h>

#ifndef PTSIGNALTIMEPROPAGATIONPANELWIDGETUI_H
#define PTSIGNALTIMEPROPAGATIONPANELWIDGETUI_H

// begin wxGlade: ::dependencies
// end wxGlade

// begin wxGlade: ::extracode
#include "wxID.h"

#define wxID_btnSignalTimePropagation wxID( "wxID_btnSignalTimePropagation" )
#define wxID_txtMarkersPerScan wxID( "wxID_txtMarkersPerScan" )
#define wxID_chkStartTime wxID( "wxID_chkStartTime" )
#define wxID_txtTR wxID( "wxID_txtTR" )
#define wxID_chkAnnotations wxID( "wxID_chkAnnotations" )
#define wxID_chkInputData wxID( "wxID_chkInputData" )
#define wxID_chkUseTR wxID( "wxID_chkUseTR" )

// end wxGlade


class ptSignalTimePropagationPanelWidgetUI: public wxPanel {
public:
    // begin wxGlade: ptSignalTimePropagationPanelWidgetUI::ids
    // end wxGlade

    ptSignalTimePropagationPanelWidgetUI(wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

private:
    // begin wxGlade: ptSignalTimePropagationPanelWidgetUI::methods
    void set_properties();
    void do_layout();
    // end wxGlade

protected:
    // begin wxGlade: ptSignalTimePropagationPanelWidgetUI::attributes
    wxStaticBox* sizer_7_staticbox;
    wxRadioButton* m_rdbUseAnnotations;
    wxStaticText* label_1;
    wxTextCtrl* m_txtMarkersPerScan;
    wxRadioButton* m_rdbUseDataSize;
    wxRadioButton* m_rdbUseTR;
    wxStaticText* label_1_copy;
    wxTextCtrl* m_txtTR;
    wxCheckBox* m_chkStartTime;
    wxTextCtrl* m_txtStartTime;
    wxButton* m_btnSignalTimePropagation;
    // end wxGlade

    wxDECLARE_EVENT_TABLE();

public:
    virtual void OnUseAnnotations(wxCommandEvent &event); // wxGlade: <event_handler>
    virtual void OnUseInputDataSize(wxCommandEvent &event); // wxGlade: <event_handler>
    virtual void OnUseRepetitionTime(wxCommandEvent &event); // wxGlade: <event_handler>
    virtual void OnCheckStartTime(wxCommandEvent &event); // wxGlade: <event_handler>
    virtual void OnButtonSignalTimePropagation(wxCommandEvent &event); // wxGlade: <event_handler>
}; // wxGlade: end class


#endif // PTSIGNALTIMEPROPAGATIONPANELWIDGETUI_H
