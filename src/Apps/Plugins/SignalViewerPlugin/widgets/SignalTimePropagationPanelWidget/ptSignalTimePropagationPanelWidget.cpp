// Copyright 2009 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "ptSignalTimePropagationPanelWidget.h"

ptSignalTimePropagationPanelWidget::ptSignalTimePropagationPanelWidget(
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    ptSignalTimePropagationPanelWidgetUI(parent, id, pos, size, wxTAB_TRAVERSAL)
{
	m_processor = Core::SignalTimePropagationProcessor::New();

	// Update initial values using processor data
	long markers = m_processor->GetMarkersPerScan( );
	m_txtMarkersPerScan->SetValue( wxString::Format( "%d", markers ) );

	UpdateWidget();
}

Core::BaseProcessor::Pointer ptSignalTimePropagationPanelWidget::GetProcessor()
{
	return m_processor.GetPointer();
}

bool ptSignalTimePropagationPanelWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = wxPanel::Enable( enable );

	try
	{
		const std::string helpStr = \
			"\n\nUsage: set markers per scan and push the button.";
		SetInfoUserHelperWidget( helpStr );
	}
	coreCatchExceptionsReportAndNoThrowMacro("ptSignalTimePropagationPanelWidget::Enable");

	return bReturn;
}

void ptSignalTimePropagationPanelWidget::OnButtonSignalTimePropagation(wxCommandEvent &event)
{
	try
	{
		UpdateData();

		UpdateProcessor( );

		wxMessageBox( "Time propagated!", "Result" );
	}
	coreCatchExceptionsReportAndNoThrowMacro( OnButtonSignalTimePropagation )
}

void ptSignalTimePropagationPanelWidget::UpdateData()
{
	long markers = 1;
	m_txtMarkersPerScan->GetValue( ).ToLong( &markers );
	m_processor->SetMarkersPerScan( markers );

	std::string startTime = std::string( m_txtStartTime->GetValue( ).c_str( ) );
	m_processor->SetStartTime( startTime );

	m_processor->SetUseStartTime( m_chkStartTime->GetValue( ) );

	double TR;
	m_txtTR->GetValue().ToDouble( &TR );
	m_processor->SetTR( TR );
	
	if ( m_rdbUseAnnotations->GetValue() )
	{
		m_processor->SetPropagationMethod( Core::SignalTimePropagationProcessor::PROPAGATION_METHOD_USE_ANNOTATIONS );
	}
	else if ( m_rdbUseDataSize->GetValue() )
	{
		m_processor->SetPropagationMethod( Core::SignalTimePropagationProcessor::PROPAGATION_METHOD_USE_DATA_SIZE );
	}
	else if ( m_rdbUseTR->GetValue() )
	{
		m_processor->SetPropagationMethod( Core::SignalTimePropagationProcessor::PROPAGATION_METHOD_USE_TR );
	}
}

void ptSignalTimePropagationPanelWidget::UpdateWidget()
{
	m_txtStartTime->Enable( m_chkStartTime->GetValue() );
	m_txtMarkersPerScan->Enable( m_rdbUseAnnotations->GetValue( ) );
	m_txtTR->Enable( m_rdbUseTR->GetValue( ) );
}

void ptSignalTimePropagationPanelWidget::OnCheckStartTime( wxCommandEvent &event )
{
	UpdateWidget();
}

void ptSignalTimePropagationPanelWidget::OnUseAnnotations( wxCommandEvent &event )
{
	UpdateWidget();
}

void ptSignalTimePropagationPanelWidget::OnUseInputDataSize( wxCommandEvent &event )
{
	UpdateWidget();
}

void ptSignalTimePropagationPanelWidget::OnUseRepetitionTime( wxCommandEvent &event )
{
	UpdateWidget();
}
