/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "SignalViewerPlugin.h"

// CoreLib
#include "coreReportExceptionMacros.h"
#include "corePluginMacros.h"
#include "coreProfile.h"

// Declaration of the plugin
coreBeginDefinePluginMacro(SignalViewerPlugin)
coreEndDefinePluginMacro()

SignalViewerPlugin::SignalViewerPlugin(void) : FrontEndPlugin()
{
	try
	{
		m_Processors = svProcessorCollective::New();

		m_Widgets = svWidgetCollective::New();
		m_Widgets->Init( );
	}
	coreCatchExceptionsReportAndNoThrowMacro(SignalViewerPlugin::SignalViewerPlugin)
}

SignalViewerPlugin::~SignalViewerPlugin(void)
{
}
