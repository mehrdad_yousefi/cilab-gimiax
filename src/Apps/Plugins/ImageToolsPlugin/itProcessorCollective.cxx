/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "itProcessorCollective.h"
#include "coreChangeOrientationProcessor.h"

#include "coreFactoryManager.h"

itProcessorCollective::itProcessorCollective()
{
	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory( 
		Core::BaseProcessor::GetNameClass( ), 
		Core::ChangeOrientationProcessor::Factory::New( ) );
}

itProcessorCollective::~itProcessorCollective()
{
}
