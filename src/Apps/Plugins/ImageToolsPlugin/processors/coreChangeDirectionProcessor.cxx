﻿/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreChangeDirectionProcessor.h"
#include "coreDataEntityHelper.h"

#include "vtkImageReslice.h"
#include "vtkAbstractTransform.h"

ChangeDirectionProcessor::ChangeDirectionProcessor( )
{
	SetName( "ChangeOrientation" );

	SetNumberOfInputs( 1 );
	GetInputPort( 0 )->SetName( "Input volume" );
	GetInputPort( 0 )->SetDataEntityType( 
		Core::DataEntityType( Core::ImageTypeId | Core::ROITypeId ) );
	SetNumberOfOutputs( 1 );
	GetOutputPort( 0 )->SetDataEntityType( Core::ImageTypeId );
	GetOutputPort( 0 )->SetReuseOutput( true );
	GetOutputPort( 0 )->SetName( "Oriented image" );
	GetOutputPort( 0 )->SetDataEntityName( "Output image" );

	m_ComputeOriginalImage = false;
	m_OutputSpacingFactor = 1.0;
}

void ChangeDirectionProcessor::SetComputeOriginalImage( bool val )
{
	m_ComputeOriginalImage = val;
}

bool ChangeDirectionProcessor::GetComputeOriginalImage( )
{
	return m_ComputeOriginalImage;
}

void ChangeDirectionProcessor::SetOutputSpacingFactor( double val )
{
	m_OutputSpacingFactor = val;
}

double ChangeDirectionProcessor::GetOutputSpacingFactor( )
{
	return m_OutputSpacingFactor;
}

void ChangeDirectionProcessor::ComputeOutputSpacing( 
	vtkSmartPointer<vtkImageReslice> imageReslice )
{
	Core::vtkImageDataPtr inputImage;
	GetProcessingData( 0, inputImage );

	double inSpacing[3], inOrigin[3];
	int inWholeExt[6];
	double outSpacing[3];
	inputImage->GetSpacing( inSpacing );
	inputImage->GetOrigin( inOrigin );
	inputImage->GetExtent( inWholeExt );


	//////////////////////////////////////////////////////////////////
	// Code copied from vtkImageReslice
	int i,j;
	double matrix[4][4];
	double imatrix[4][4];
    vtkMatrix4x4::DeepCopy(*matrix, imageReslice->GetResliceAxes( ) );
    vtkMatrix4x4::Invert(*matrix,*imatrix);

	// pass the center of the volume through the inverse of the
	// 3x3 direction cosines matrix
	double inCenter[3];
	for (i = 0; i < 3; i++)
	{
		inCenter[i] = inOrigin[i] + \
			0.5*(inWholeExt[2*i] + inWholeExt[2*i+1])*inSpacing[i];
	}

	// the default spacing, extent and origin are the input spacing, extent
	// and origin,  transformed by the direction cosines of the ResliceAxes
	// if requested (note that the transformed output spacing will always
	// be positive)
	for (i = 0; i < 3; i++)
	{
		double s = 0;  // default output spacing
		double d = 0;  // default linear dimension
		double e = 0;  // default extent start
		double c = 0;  // transformed center-of-volume

		double r = 0.0;
		for (j = 0; j < 3; j++)
		{
			c += imatrix[i][j]*(inCenter[j] - matrix[j][3]);
			double tmp = matrix[j][i]*matrix[j][i];
			s += tmp*fabs(inSpacing[j]);
			d += tmp*(inWholeExt[2*j+1] - inWholeExt[2*j])*fabs(inSpacing[j]);
			e += tmp*inWholeExt[2*j];
			r += tmp;
		}
		s /= r;
		d /= r*sqrt(r);
		e /= r;

		outSpacing[i] = s * m_OutputSpacingFactor;
	}

	imageReslice->SetOutputSpacing( outSpacing[0], outSpacing[1], outSpacing[2] );
}

void ChangeDirectionProcessor::Update()
{
	Core::vtkImageDataPtr inputImage;
	GetProcessingData( 0, inputImage );

	vtkSmartPointer<vtkImageReslice> imageReslice = vtkSmartPointer<vtkImageReslice>::New();
	imageReslice->SetInput( inputImage );
	imageReslice->SetResliceAxesDirectionCosines(m_xAxesDirectionCosines, m_yAxesDirectionCosines, m_zAxesDirectionCosines);
	imageReslice->SetBackgroundLevel( 0 );
	imageReslice->SetInterpolationModeToCubic( );

	//// compute output spacing just after setting image and cosines
	//ComputeOutputSpacing( imageReslice );
	
	// Get transformation matrix
	vtkSmartPointer<vtkMatrix4x4> transform = imageReslice->GetResliceAxes( );

	// Update
	imageReslice->Update();
	UpdateOutput( 0, imageReslice->GetOutput(), "Oriented image", true, 1, GetInputDataEntity( 0 ) );

	// Compute original image
	if ( m_ComputeOriginalImage )
	{
		Core::vtkImageDataPtr newImage = Core::vtkImageDataPtr::New( );
		newImage->DeepCopy( imageReslice->GetOutput() );
		imageReslice->SetInput( newImage );
		
		// Invert transform
		transform->Invert( );
		imageReslice->SetResliceAxes( transform );
		imageReslice->SetInterpolationModeToCubic( );
		imageReslice->SetOutputSpacingToDefault( );
		imageReslice->Update();
		UpdateOutput( 0, imageReslice->GetOutput(), "Original image", false, 1, GetInputDataEntity( 0 ) );
	}
}

const double* ChangeDirectionProcessor::GetXAxesDirectionCosines() const
{
	return &m_xAxesDirectionCosines[ 0 ];
}

const double* ChangeDirectionProcessor::GetYAxesDirectionCosines() const
{
	return &m_yAxesDirectionCosines[ 0 ];
}

const double *ChangeDirectionProcessor::GetZAxesDirectionCosines() const
{
	return &m_zAxesDirectionCosines[ 0 ];
}

void ChangeDirectionProcessor::SetXAxesDirectionCosines( mitk::Vector3D val )
{
	m_xAxesDirectionCosines[ 0 ] = val[ 0 ];
	m_xAxesDirectionCosines[ 1 ] = val[ 1 ];
	m_xAxesDirectionCosines[ 2 ] = val[ 2 ];
}

void ChangeDirectionProcessor::SetYAxesDirectionCosines( mitk::Vector3D val )
{
	m_yAxesDirectionCosines[ 0 ] = val[ 0 ];
	m_yAxesDirectionCosines[ 1 ] = val[ 1 ];
	m_yAxesDirectionCosines[ 2 ] = val[ 2 ];
}

void ChangeDirectionProcessor::SetZAxesDirectionCosines( mitk::Vector3D val )
{
	m_zAxesDirectionCosines[ 0 ] = val[ 0 ];
	m_zAxesDirectionCosines[ 1 ] = val[ 1 ];
	m_zAxesDirectionCosines[ 2 ] = val[ 2 ];
}
