/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreSetSpacingProcessor_H
#define _coreSetSpacingProcessor_H

#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreBoundingBox.h"
#include "coreBaseProcessor.h"

namespace Core{

/**
Crops an image or contour using a bounding box.

\ingroup imageTools
\author Luigi Carotenuto
\date 01 feb 2011
*/

class PLUGIN_EXPORT SetSpacingProcessor : public Core::BaseProcessor
{
public:

public:
	coreDeclareSmartPointerClassMacro(SetSpacingProcessor, Core::BaseFilter);
	//!
	void Update();

	void SetSpacing( double spacing[3] );

	double *GetSpacing();

private:
	/**
	*/
	SetSpacingProcessor( );

private:

	double m_spacing [3];
};

} // Core

#endif //_coreSetSpacingProcessor_H
