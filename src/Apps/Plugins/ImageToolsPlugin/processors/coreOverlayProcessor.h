/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreOverlayProcessor_H
#define _coreOverlayProcessor_H

#include "corePluginMacros.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreBaseProcessor.h"

namespace Core{

/**
Overlay two images changing the origin and spacing of the scond one

\ingroup ImageToolsPlugin
\author Chiara Riccobene
\date 6 nov 2009
*/

class PLUGIN_EXPORT OverlayProcessor : public Core::BaseProcessor
{
public:
	coreDeclareSmartPointerClassMacro(Core::OverlayProcessor, Core::BaseProcessor);

	//!
	void Update();

private:
	//!
	OverlayProcessor( );

private:

};

} // Core

#endif //_coreOverlayProcessor_H
