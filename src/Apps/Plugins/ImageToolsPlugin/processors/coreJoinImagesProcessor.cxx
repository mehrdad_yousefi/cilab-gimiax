/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreJoinImagesProcessor.h"
#include "itkImageToVTKImageFilter.h"
#include "itkRegionOfInterestImageFilter.h"
#include "coreImageDataEntityMacros.h"
#include "itkImageBase.h"
#include "itkImageRegion.h"


Core::JoinImagesProcessor::JoinImagesProcessor( )
{
	SetNumberOfInputs( 0 );
	SetNumberOfOutputs( 1 );
	GetOutputPort( 0 )->SetName( "Output image" );

	SetName( "JoinImagesProcessor" );

}


bool cmpImageProp(const Core::JoinImagesProcessor::ImageProperties &a,const  Core::JoinImagesProcessor::ImageProperties &b)
{
	return a.xdimesion*a.ydimension*a.zdimension < b.xdimesion*b.ydimension*b.zdimension;
}

void Core::JoinImagesProcessor::Update(std::vector<Core::DataEntity::Pointer> inputs)
{
	Core::ModalityType modality;
	modality = Core::UnknownModality;


	std::vector<Core::JoinImagesProcessor::ImageProperties> imagesProperties;
	std::vector<Core::JoinImagesProcessor::ImageProperties> imagesPropertiesNotSorted;

	for (int i=0; i<inputs.size(); i++)
	{
		Core::vtkImageDataPtr image;
		Core::CastAnyProcessingData(inputs[i]->GetProcessingData(), image);

		JoinImagesProcessor::ImageProperties imageProperties;

		imageProperties.xdimesion = image->GetDimensions()[0];
		imageProperties.ydimension = image->GetDimensions()[1];
		imageProperties.zdimension = image->GetDimensions()[2];
		imageProperties.xspacing = image->GetSpacing()[0];
		imageProperties.yspacing = image->GetSpacing()[1];
		imageProperties.zspacing = image->GetSpacing()[2];
		imageProperties.xorigin = image->GetOrigin()[0];
		imageProperties.yorigin = image->GetOrigin()[1];
		imageProperties.zorigin = image->GetOrigin()[2];

		imagesProperties.push_back(imageProperties);
		imagesPropertiesNotSorted.push_back(imageProperties);

		std::cout << "Dimensions: (" << imageProperties.xdimesion << "," << imageProperties.ydimension << "," << imageProperties.zdimension << ")" << std::endl;
		std::cout << "Spacing: (" << imageProperties.xspacing << "," << imageProperties.yspacing << "," << imageProperties.zspacing << ")" << std::endl;
		std::cout << "Origin: (" << imageProperties.xorigin << "," << imageProperties.yorigin << "," << imageProperties.zorigin << ")" << std::endl;

	}

	std::cout << "====" << std::endl;

	std::sort(imagesProperties.begin(),imagesProperties.end(),cmpImageProp);

	JoinImagesProcessor::ImageProperties minImgProp = imagesProperties[imagesProperties.size()-1];

	Core::DataEntity::Pointer dataEntity = Core::DataEntity::New( Core::ImageTypeId );

	for (int i=0; i<inputs.size(); i++)
	{
		if ( i == 0 )
		{
			modality = inputs[0]->GetMetadata()->GetModality();
		}

		Core::vtkImageDataPtr image;
		Core::CastAnyProcessingData(inputs[i]->GetProcessingData(), image);
		
		std::cout << "SetDimensions: (" << minImgProp.xdimesion << "," << minImgProp.ydimension << "," << minImgProp.zdimension << ")" << std::endl;

		dataEntity->AddTimeStep( image );

	}

	dataEntity->GetMetadata()->SetName( "Images joined" );
	dataEntity->GetMetadata( )->SetModality(modality);
	SetOutputDataEntity( 0, dataEntity);
}

