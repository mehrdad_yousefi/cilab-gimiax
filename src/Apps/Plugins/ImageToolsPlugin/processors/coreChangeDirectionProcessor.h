﻿/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreChangeDirectionProcessor_H
#define _coreChangeDirectionProcessor_H

#include "corePluginMacros.h"
#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreBaseProcessor.h"
#include "vtkMatrix4x4.h"

/**
Change direction of image using cosine direction (axis)

Output spacing can be magnified by a factor

\ingroup ImageToolsPlugin
\author Xavi Planes
\date Oct 2011
*/

class PLUGIN_EXPORT ChangeDirectionProcessor : public Core::BaseProcessor
{
public:
	coreDeclareSmartPointerClassMacro(ChangeDirectionProcessor, Core::BaseProcessor);

	//!
	const double* GetXAxesDirectionCosines() const;
	void SetXAxesDirectionCosines( mitk::Vector3D normal );
	const double* GetYAxesDirectionCosines() const;
	void SetYAxesDirectionCosines( mitk::Vector3D normal );
	const double* GetZAxesDirectionCosines() const;
	void SetZAxesDirectionCosines( mitk::Vector3D normal );

	//!
	void SetComputeOriginalImage( bool val );
	bool GetComputeOriginalImage( );

	//!
	void SetOutputSpacingFactor( double val );
	double GetOutputSpacingFactor( );

	//!
	void Update();


private:
	//!
	ChangeDirectionProcessor( );

	//! Compute output spacing using axis and factor m_OutputSpacingFactor
	void ComputeOutputSpacing( vtkSmartPointer<vtkImageReslice> imageReslice );

private:
	//!
	double m_xAxesDirectionCosines[3];
	//!
	double m_yAxesDirectionCosines[3];
	//!
	double m_zAxesDirectionCosines[3];
	//!
	bool m_ComputeOriginalImage;
	//!
	double m_OutputSpacingFactor;
};

#endif //_coreChangeDirectionProcessor_H
