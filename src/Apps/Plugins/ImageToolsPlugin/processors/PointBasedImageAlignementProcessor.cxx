/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "PointBasedImageAlignementProcessor.h"

#include <string>
#include <iostream>

#include "coreReportExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityHelper.txx"
#include "coreKernel.h"
#include "coreVTKPolyDataHolder.h"

#include <vtkSmartPointer.h>
#include "vtkImageData.h"
#include <vtkTransform.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkIterativeClosestPointTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkLandmarkTransform.h>
#include <vtkMath.h>
#include <vtkMatrix4x4.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkProperty.h>
#include <vtkImageReslice.h>
#include <vtkImageChangeInformation.h>
#include <vtkIterativeClosestPointTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkImageCast.h>
#include <itkImageToVTKImageFilter.h>

#include <itkResampleImageFilter.h>
#include "coreImageDataEntityMacros.h"

#include "mitkBaseData.h"
#include "mitkImage.h"



namespace gsp{
	struct ExecuteImageTransformation
	{
		gsp::PointBasedImageAlignementProcessor::Pointer processor;
		vtkSmartPointer<vtkMatrix4x4> lTrans_m;

		//! Executes the transformation on an image
		template< class ItkImageType > void Execute(typename ItkImageType::Pointer image)
		{
			if((processor.IsNull()) ||  (lTrans_m==NULL))
				return;

			typedef itk::ResampleImageFilter<
				ItkImageType, ItkImageType >  FilterType;

			typename FilterType::Pointer resampleFilter = FilterType::New();

			typedef itk::AffineTransform< double, 3 >  TransformType;
			TransformType::Pointer transform = TransformType::New();
			itk::Matrix<double,3,3> matrix;
			itk::Vector<double,3> vector;

			vtkSmartPointer<vtkMatrix4x4> invMat = vtkMatrix4x4::New();
			// Get the inverse matrix of the transformation to apply to the image
			lTrans_m ->Invert(*lTrans_m ,*invMat);
			std::cout << "The resulting inverse matrix from is: " << *invMat << std::endl;

			for( int i = 0 ; i < 3 ; i++ )
			{
				for( int j = 0 ; j < 3 ; j++ )
				{
					matrix[ i ][ j ] = (*invMat)[ i ][ j ] ;
				}
				vector[ i ] = (*invMat)[ i ][ 3 ] ;
			}
			transform->SetMatrix( matrix ) ;
			transform->SetTranslation( vector ) ;


			typedef itk::LinearInterpolateImageFunction<ItkImageType, double >  InterpolatorType;
			typename InterpolatorType::Pointer interpolator = InterpolatorType::New();
			resampleFilter->SetInterpolator( interpolator );
			resampleFilter->SetDefaultPixelValue( 0 );
			resampleFilter->SetTransform( transform );


			typename ItkImageType::SpacingType spacing = image->GetSpacing();
			typename ItkImageType::PointType  origin  = image->GetOrigin();
			typename ItkImageType::SizeType size = image->GetLargestPossibleRegion().GetSize();
			resampleFilter->SetOutputOrigin( origin );
			resampleFilter->SetOutputSpacing( spacing );
			resampleFilter->SetSize( size );
			resampleFilter->SetInput( image);
			resampleFilter->Update();

			//now use the
			typename ItkImageType::Pointer outputMovingImageItk = resampleFilter->GetOutput();	

			if(outputMovingImageItk.IsNotNull())
			{
				//		UpdateOutput(OUTPUT_IMAGE_ITK, outputMovingImageItk, "Registered Image ITK", true, 1, GetInputDataEntity(INPUT_MOVING_IMAGE));

				//since there are problems in saving itk images in gimias 1.3.0, it is better to store the image in vtk
				typedef itk::ImageToVTKImageFilter< ItkImageType > ITK2VTKType;
				typename ITK2VTKType::Pointer itk2vtk = ITK2VTKType::New();
				itk2vtk->SetInput( outputMovingImageItk );
				itk2vtk->Update();
				vtkSmartPointer<vtkImageData> outputMovingImageVtk = vtkSmartPointer<vtkImageData>::New( );
				outputMovingImageVtk->DeepCopy( itk2vtk->GetOutput() );


				processor->UpdateOutput(processor->OUTPUT_IMAGE_VTK, outputMovingImageVtk, "Registered Image VTK", true, 1, 
					processor->GetInputDataEntity(processor->INPUT_MOVING_IMAGE));

			}
		}
	};
}



gsp::PointBasedImageAlignementProcessor::PointBasedImageAlignementProcessor( )
{
	SetName( "PointBasedImageAlignementProcessor" );

	BaseProcessor::SetNumberOfInputs( NUMBEROFINPUTS );
	GetInputPort( INPUT_FIXED_IMAGE )->SetName( "Input Fixed Image" );
	GetInputPort( INPUT_FIXED_IMAGE )->SetDataEntityType( Core::ImageTypeId );
	GetInputPort( INPUT_MOVING_IMAGE )->SetName( "Input Moving Image" );
	GetInputPort( INPUT_MOVING_IMAGE )->SetDataEntityType( Core::ImageTypeId );
	GetInputPort( INPUT_FIXED_POINTSET )->SetName( "Input Point" );
	GetInputPort( INPUT_FIXED_POINTSET )->SetDataEntityType( Core::PointSetTypeId );
	GetInputPort( INPUT_MOVING_POINTSET )->SetName( "Input Point" );
	GetInputPort( INPUT_MOVING_POINTSET )->SetDataEntityType( Core::PointSetTypeId );
	GetInputPort( INPUT_MOVING_MESH )->SetName( "Input Moving mesh" );
	GetInputPort( INPUT_MOVING_MESH )->SetDataEntityType( Core::SurfaceMeshTypeId );
	GetInputPort( INPUT_FIXED_MESH )->SetName( "Input Moving mesh" );
	GetInputPort( INPUT_FIXED_MESH )->SetDataEntityType( Core::SurfaceMeshTypeId );

	BaseProcessor::SetNumberOfOutputs( NUMBEROFOUTPUTS );
	//GetOutputPort( OUTPUT_IMAGE_ITK )->SetDataEntityType( Core::ImageTypeId );
	GetOutputPort( OUTPUT_IMAGE_VTK )->SetDataEntityType( Core::ImageTypeId );
	GetOutputPort( OUTPUT_POINTS )->SetDataEntityType( Core::PointSetTypeId );
}

gsp::PointBasedImageAlignementProcessor::~PointBasedImageAlignementProcessor()
{
}



void gsp::PointBasedImageAlignementProcessor::Update()
{
	//Here this terms are synonymous:
	//target = fixed
	//source = moving

	// Get the fixed image
	vtkSmartPointer <vtkImageData> fixedImage;
	GetProcessingData( INPUT_FIXED_IMAGE, fixedImage);

	// Get the moving image
	vtkSmartPointer <vtkImageData> movingImage;
	GetProcessingData( INPUT_MOVING_IMAGE, movingImage);
	
	
	// Get the fixed pointset
	Core::vtkPolyDataPtr fixedPointset;
	GetProcessingData( INPUT_FIXED_POINTSET, fixedPointset );

	// Get the fixed pointset
	Core::vtkPolyDataPtr movingPointset;
	GetProcessingData( INPUT_MOVING_POINTSET, movingPointset );
	
	if ( fixedPointset->GetNumberOfPoints() != movingPointset->GetNumberOfPoints()  )
	{
		throw Core::Exceptions::Exception(
			"PointBasedImageAlignementProcessor::Update",
			"fixed points and moving points number is different" );
	}

	if (( fixedPointset->GetNumberOfPoints() == 0 ) ||   (movingPointset->GetNumberOfPoints() == 0 ))
	{
		throw Core::Exceptions::Exception(
			"PointBasedImageAlignementProcessor::Update",
			"fixed points or moving points not specified" );
	}

	if ((fixedImage==NULL) || (movingImage==NULL) || (fixedPointset==NULL) || (movingPointset==NULL))
	{
		throw Core::Exceptions::Exception(
			"PointBasedImageAlignementProcessor::Update",
			"fixed image or moving image are not correctly set" );
	}


	vtkSmartPointer<vtkPoints> fixedPoints = fixedPointset->GetPoints();
	vtkSmartPointer<vtkPoints> movingPoints = movingPointset->GetPoints();

	vtkSmartPointer<vtkLandmarkTransform> lTransf = vtkLandmarkTransform::New();

	switch(m_params.getTransformationType())
	{
	case(AlignementParams::TR_RIGID):
		lTransf->SetModeToRigidBody();
		break;
	case (AlignementParams::TR_SIMILARITY):
		lTransf->SetModeToSimilarity();
		break;
	case (AlignementParams::TR_AFFINE):
		lTransf->SetModeToAffine();
		break;

	default:
		return;
	}

	lTransf->SetSourceLandmarks(movingPoints);
	lTransf->SetTargetLandmarks(fixedPoints);
	lTransf->Update();
	
	// Get the resulting transformation matrix (this matrix takes the source points to the target points)
	vtkSmartPointer<vtkMatrix4x4> m = lTransf->GetMatrix();
	std::cout << "The resulting matrix from the landmark transform is: " << *m << std::endl;

	ExecuteImageTransformation executor;
	executor.processor = this;
	executor.lTrans_m = m;
	try
	{
		coreImageDataEntityItkMacro( this->GetInputDataEntity(INPUT_MOVING_IMAGE), executor.Execute );
	}
	catch( ... )
	{
		throw;
	}

	//apply the transformation also to the points
	if(GetOutputDataEntity(OUTPUT_IMAGE_VTK).IsNotNull())
	{
		Core::vtkPolyDataPtr outputPolyData = vtkPolyData::New();
		vtkSmartPointer<vtkPoints> outputPoints = vtkPoints::New();
		lTransf->TransformPoints(movingPoints,outputPoints);
		outputPolyData->SetPoints(outputPoints);
		UpdateOutput(OUTPUT_POINTS, outputPolyData, "Registered Points", true, 1, GetOutputDataEntity(OUTPUT_IMAGE_VTK));
	}

	//if mesh input is present, also transform the mesh
	if(GetInputDataEntity(INPUT_MOVING_MESH))
	{
		Core::vtkPolyDataPtr outputMesh = vtkPolyData::New();

		Core::vtkPolyDataPtr movingMesh = NULL;
		GetProcessingData( INPUT_MOVING_MESH, movingMesh);

		if(movingMesh!=NULL)
		{
			bool bUseICP = false;

			if(GetInputDataEntity(INPUT_FIXED_MESH))
			{
				Core::vtkPolyDataPtr fixedMesh = NULL;
				GetProcessingData( INPUT_FIXED_MESH, fixedMesh);
				if(fixedMesh!=NULL)
				{
					bUseICP= true;
					//use an icp:
					vtkSmartPointer<vtkIterativeClosestPointTransform>  icp =
						vtkIterativeClosestPointTransform::New();
					icp->SetSource(movingMesh);
					icp->SetTarget(fixedMesh);
					icp->SetMaximumNumberOfIterations(20);
					icp->GetLandmarkTransform()->SetModeToRigidBody();
					//icp->StartByMatchingCentroidsOn();
					icp->Modified();
					icp->Update();

					// Get the resulting transformation matrix (this matrix takes the source points to the target points)
					//	vtkSmartPointer<vtkMatrix4x4> m = icp->GetMatrix();
					//	std::cout << "The resulting matrix is: " << *m << std::endl;

					// Transform the source points by the ICP solution
					vtkSmartPointer<vtkTransformPolyDataFilter> icpTransformFilter =
						vtkSmartPointer<vtkTransformPolyDataFilter>::New();
					icpTransformFilter->SetInput(movingMesh);
					icpTransformFilter->SetTransform(icp);
					icpTransformFilter->Update();
					outputMesh->DeepCopy(icpTransformFilter->GetOutput());
					std::cout << "********ICP used"<<std::endl;

				}

			}
			if(!bUseICP)
			{
				std::cout << "********ICP not used"<<std::endl;
				vtkTransformPolyDataFilter *transfFilter = vtkTransformPolyDataFilter::New();
				transfFilter->SetTransform(lTransf);
				transfFilter->SetInput(movingMesh);
				transfFilter->Update();
				outputMesh->DeepCopy(transfFilter->GetOutput());
			}
		

			UpdateOutput(OUTPUT_MESH, outputMesh, "Registered Mesh", true, 1, GetOutputDataEntity(OUTPUT_IMAGE_VTK));

		}

	}

}
