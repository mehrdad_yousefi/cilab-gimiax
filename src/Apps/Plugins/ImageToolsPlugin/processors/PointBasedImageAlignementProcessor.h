/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _PointBasedImageAlignementProcessor_H
#define _PointBasedImageAlignementProcessor_H

#include "coreBaseProcessor.h"

#include "itkImage.h"
#include "itkScaleTransform.h"

namespace gsp{

/**
Processor for point based image alignement

\ingroup gsp
\author Luigi Carotenuto
\date 31 mar 2011
*/

class AlignementParams
{
public:
	typedef enum
	{
		TR_RIGID = 0,
		TR_SIMILARITY,
		TR_AFFINE,
		NUMBEROFTRANSFTYPE
	} TRANSFORMATION_TYPE;

	
public:
	AlignementParams() {m_transformationType = TR_RIGID; };
	int getTransformationType(){return m_transformationType;};
	void setTransformationType(int tType){if ((tType>=0) && (tType<NUMBEROFTRANSFTYPE)) m_transformationType=tType;};

private:
	int m_transformationType;
};


class PointBasedImageAlignementProcessor : public Core::BaseProcessor
{
public:
	typedef enum
	{
		INPUT_FIXED_IMAGE,
		INPUT_MOVING_IMAGE,
		INPUT_MOVING_MESH, //optional
		INPUT_FIXED_MESH, //optional
		INPUT_FIXED_POINTSET,
		INPUT_MOVING_POINTSET,
		NUMBEROFINPUTS
	} INPUT_TYPE;
	
	typedef enum
	{
		OUTPUT_POINTS,
//		OUTPUT_IMAGE_ITK,
		OUTPUT_IMAGE_VTK,
		OUTPUT_MESH, //optional
		NUMBEROFOUTPUTS
	} OUTPUT_TYPE;

public:
	//!
	coreProcessor(PointBasedImageAlignementProcessor, Core::BaseProcessor);

	//! Call library to perform operation
	void Update( );


public:
	AlignementParams m_params;
private:
	//!
	PointBasedImageAlignementProcessor();

	//!
	~PointBasedImageAlignementProcessor();

	//! Purposely not implemented
	PointBasedImageAlignementProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );

	
};
    
} // namespace gsp{

#endif //_PointBasedImageAlignementProcessor_H
