/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreSetSpacingProcessor.h"
#include "itkImageToVTKImageFilter.h"
#include "itkRegionOfInterestImageFilter.h"
#include "coreImageDataEntityMacros.h"
#include "itkImageBase.h"
#include "itkImageRegion.h"



Core::SetSpacingProcessor::SetSpacingProcessor( )
{
	SetNumberOfInputs( 1 );
	GetInputPort( 0 )->SetName( "Input image" );
	GetInputPort( 0 )->SetDataEntityType( Core::ImageTypeId );
	SetNumberOfOutputs( 1 );
	GetOutputPort( 0 )->SetName( "Output image" );

	SetName( "SetSpacingProcessor" );

}


void Core::SetSpacingProcessor::SetSpacing(double spacing[3])
{
	m_spacing[0] = spacing [0];
	m_spacing[1] = spacing [1];
	m_spacing[2] = spacing [2];

}

void Core::SetSpacingProcessor::Update()
{
	if( !GetInputDataEntity( 0 ) )
		return;

	if( GetInputDataEntity( 0 )->IsImage() )
	{
		int nTimeSteps = GetInputDataEntity( 0 )->GetNumberOfTimeSteps();
		if(nTimeSteps<=0)
			return;
		
		std::vector<vtkImageData *> outVtkImageVect;
		for(int i= 0; i<nTimeSteps; i++)
		{
			
			SetTimeStep(i);
			Core::vtkImageDataPtr vtkImage;
			GetProcessingData( 0 ,vtkImage, i );

			vtkSmartPointer<vtkImageData> imageOut = NULL;
			if(vtkImage!=NULL)
			{
				
				imageOut = vtkImageData::New();
				imageOut->DeepCopy(vtkImage);
				imageOut->SetSpacing(m_spacing);
				imageOut->Update();
				outVtkImageVect.push_back(imageOut);
			}

		}
		UpdateOutput <vtkImageData *>(0,outVtkImageVect,"Image ReSpaced", true, 
			GetInputDataEntity(0));
	

	}
	
}


double *Core::SetSpacingProcessor::GetSpacing()
{
	if( !GetInputDataEntity( 0 ) )
		return NULL;

	if( GetInputDataEntity( 0 )->IsImage() )
	{
		Core::vtkImageDataPtr vtkImage;
		GetProcessingData( 0 ,vtkImage );
		if(vtkImage==NULL)
			return NULL;

		return vtkImage->GetSpacing();
	}
	else
		return NULL;
}
