/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreJoinImagesProcessor_H
#define _coreJoinImagesProcessor_H

#include "coreDataEntityHolder.h"
#include "coreSmartPointerMacros.h"
#include "coreBoundingBox.h"
#include "coreBaseProcessor.h"

namespace Core{

/**
From N images generates 1 image with N timesteps

\ingroup gmInteractors
\author Albert Sanchez
\date 08 jul 2011
*/

class PLUGIN_EXPORT JoinImagesProcessor : public Core::BaseProcessor
{
public:
	struct ImageProperties
	{
		int xdimesion;
		int ydimension;
		int zdimension;
		double xspacing;
		double yspacing;
		double zspacing;
		double xorigin;
		double yorigin;
		double zorigin;
	};

public:
	coreDeclareSmartPointerClassMacro(JoinImagesProcessor, Core::BaseFilter);
	//!
	void Update(std::vector<Core::DataEntity::Pointer> inputs);


private:
	/**
	*/
	JoinImagesProcessor( );


};

} // Core

#endif //_coreJoinImagesProcessor_H
