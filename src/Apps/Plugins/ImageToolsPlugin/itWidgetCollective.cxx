/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "itWidgetCollective.h"

#include "wxID.h"

#include "coreFrontEndPlugin.h"
#include "corePluginTabFactory.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreSimpleProcessingWidget.h"
#include "coreKernel.h"

#include "coreOverlayProcessor.h"
#include "ptChangeOrientationWidget.h"
#include "coreImageInfoWidget.h"
#include "ptSetSpacingWidget.h"
#include "PointBasedImageAlignementPanelWidget.h"
#include "JoinImagesWidget.h"
#include "ChangeDirectionPanelWidget.h"
#include "coreImageContrastWidget.h"

itWidgetCollective::itWidgetCollective( ) 
{
}

void itWidgetCollective::Init(  )
{
	Core::Runtime::Kernel::RuntimeGraphicalInterfacePointer gIface;
	gIface = Core::Runtime::Kernel::GetGraphicalInterface();


	Core::WindowConfig windowConfig = Core::WindowConfig( ).ProcessingTool( );
	windowConfig.Category("Image processing");
	gIface->RegisterFactory( 
		ptChangeOrientationWidget::Factory::NewBase(),
		windowConfig.Caption( "Change Orientation" ) );
	gIface->RegisterFactory( 
		JoinImagesWidget::Factory::NewBase(),
		windowConfig.Caption( "Join images" ) );		
	typedef Core::Widgets::SimpleProcessingWidget<Core::OverlayProcessor> 
		OverlayWidget;
	gIface->RegisterFactory( 
		OverlayWidget::Factory::NewBase(),
		windowConfig.Caption( "Overlay Images" ) );
	gIface->RegisterFactory( 
		ptSetSpacingWidget::Factory::NewBase(),
		windowConfig.Caption( "Set Image Spacing" ) );

	gIface->RegisterFactory( 
		PointBasedImageAlignementPanelWidget::Factory::NewBase( ), 
		windowConfig.Caption( "Point Based Image Alignement" ) );

	gIface->RegisterFactory( 
		ChangeDirectionPanelWidget::Factory::NewBase( ), 
		windowConfig.Caption( "Change Image Direction by AXIS" ) );

	gIface->RegisterFactory(
		Core::Widgets::ImageInfoWidget::Factory::NewBase( ),
		Core::WindowConfig( ).VerticalLayout( )
		.Id( wxID_ImageInformationWidget ).Category("Windows").Caption( "Image information" ) );

	gIface->RegisterFactory(
		Core::Widgets::ImageContrastWidget::Factory::NewBase(),
		Core::WindowConfig( ).VerticalLayout()
		.Caption( "Image Contrast Widget" ).Category( "Visualization" ) );
}

