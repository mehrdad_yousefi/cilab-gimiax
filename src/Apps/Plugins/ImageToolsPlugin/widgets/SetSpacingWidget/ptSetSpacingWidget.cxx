/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "ptSetSpacingWidget.h"

#include <wx/wupdlock.h>

#include "coreKernel.h"
#include "coreDataTreeHelper.h"
#include "coreDataEntityListBrowser.h"
#include "coreReportExceptionMacros.h"
#include "boost/filesystem.hpp"
#include <blMitkUnicode.h>
#include "coreDataTreeHelper.h"
#include "coreSelectionToolboxWidget.h"

using namespace mitk;
using namespace Core::Widgets;

BEGIN_EVENT_TABLE(ptSetSpacingWidget, wxPanel)
	EVT_BUTTON(wxID_btnSetSpacing, ptSetSpacingWidget::OnButtonSetSpacing)
END_EVENT_TABLE();


ptSetSpacingWidget::ptSetSpacingWidget(
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
		wxPanel(parent, id, pos, size, wxTAB_TRAVERSAL)
{
	m_ptSetSpacingWidgetStaticBox = new wxStaticBox(this, -1, wxT("Set Spacing Tool"));
	m_buttonSetSpacing = new wxButton(this, wxID_btnSetSpacing, wxT("Set Spacing"));

	m_Processor = Core::SetSpacingProcessor::New();

	emptylabel = new wxStaticText(this, wxID_ANY, wxEmptyString);
    labelOrig = new wxStaticText(this, wxID_ANY, wxT("Orig. Spacing (mm)"));
	labelNew = new wxStaticText(this, wxID_ANY, wxT("New Spacing (mm)"));
    labelX = new wxStaticText(this, wxID_ANY, wxT("X") );
	labelY = new wxStaticText(this, wxID_ANY, wxT("Y"));
	labelZ = new wxStaticText(this, wxID_ANY, wxT("Z"));
    m_xOrig = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
	m_yOrig = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
	m_zOrig = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
    m_xNew = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
	m_yNew = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
	m_zNew = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
	
	do_layout();
}

void ptSetSpacingWidget::OnInit( )
{
	// Observers to data
	m_Processor->GetInputDataEntityHolder( 0 )->AddObserver( 
		this, 
		&ptSetSpacingWidget::OnModifiedInputImage );
}

void ptSetSpacingWidget::OnModifiedInputImage()
{
	if( !m_Processor->GetInputDataEntity( 0 ) )
		return;

	if( !m_Processor->GetInputDataEntity( 0 )->IsImage() )
		return;


	double *spc = m_Processor->GetSpacing();

	m_xOrig->SetValue(wxString::Format("%f",spc[0]));
	m_yOrig->SetValue(wxString::Format("%f",spc[1]));
	m_zOrig->SetValue(wxString::Format("%f",spc[2]));

	m_xNew->SetValue(wxT(""));
	m_yNew->SetValue(wxT(""));
	m_zNew->SetValue(wxT(""));

}

void ptSetSpacingWidget::do_layout()
{
    wxBoxSizer* GlobalSizer = new wxBoxSizer(wxVERTICAL);
	wxStaticBoxSizer* ptSetSpacingWidget = new wxStaticBoxSizer(m_ptSetSpacingWidgetStaticBox, wxVERTICAL);
    wxGridSizer* grid_sizer = new wxGridSizer(4, 3, 0, 0);
	grid_sizer->Add(emptylabel, 0, 0, 0);
    grid_sizer->Add(labelOrig, 0, 0, 0);
	grid_sizer->Add(labelNew, 0, 0, 0);
	grid_sizer->Add(labelX, 0, 0, 0);
	grid_sizer->Add(m_xOrig, 0, 0, 0);
	grid_sizer->Add(m_xNew, 0, 0, 0);
	grid_sizer->Add(labelY, 0, 0, 0);
	grid_sizer->Add(m_yOrig, 0, 0, 0);
	grid_sizer->Add(m_yNew, 0, 0, 0);
	grid_sizer->Add(labelZ, 0, 0, 0);
	grid_sizer->Add(m_zOrig, 0, 0, 0);
	grid_sizer->Add(m_zNew, 0, 0, 0);
	
	wxBoxSizer* button_sizer = new wxBoxSizer(wxHORIZONTAL);
	button_sizer->Add(m_buttonSetSpacing, 1,  wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL);
	ptSetSpacingWidget->Add(grid_sizer, 1, wxEXPAND, 0);
	ptSetSpacingWidget->Add(button_sizer, 1, wxEXPAND, 0);
    GlobalSizer->Add(ptSetSpacingWidget);
	
    SetSizer(GlobalSizer);
    GlobalSizer->Fit(this);
}

Core::BaseProcessor::Pointer ptSetSpacingWidget::GetProcessor()
{
	return m_Processor.GetPointer();
}


void ptSetSpacingWidget::OnButtonSetSpacing(wxCommandEvent& event)
{
	if(m_Processor->GetInputDataEntity(0).IsNull())
		return;

	double spc[3];
	bool ok;
	
	ok = m_xNew->GetValue().ToDouble(&spc[0]);
	if(!ok)
		return;

	ok = m_yNew->GetValue().ToDouble(&spc[1]);
	if(!ok)
		return;

	ok = m_zNew->GetValue().ToDouble(&spc[2]);
	if(!ok)
		return;

	m_Processor->SetSpacing(spc); 
	m_Processor->Update() ;

	m_xNew->SetValue(wxT(""));
	m_yNew->SetValue(wxT(""));
	m_zNew->SetValue(wxT(""));

}

