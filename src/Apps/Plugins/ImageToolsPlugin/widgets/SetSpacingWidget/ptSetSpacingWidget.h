/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef COREptSetSpacingWidget_H
#define COREptSetSpacingWidget_H

#include "corePluginMacros.h"
#include "CILabNamespaceMacros.h"
#include "coreBBoxWidget.h"
#include "coreSetSpacingProcessor.h"
#include <wx/wx.h>
#include "wx/tglbtn.h"

// GuiBridgeLib
#include "gblWxConnectorOfWidgetChangesToSlotFunction.h"

namespace Core{
namespace Widgets{
		class DataEntityListBrowser;
}
}
	
#define wxID_btnSetSpacing wxID("Set Spacing")

/** 
\brief Change image spacing
\ingroup ImageToolsPlugin
\author Luigi Carotenuto
*/
class PLUGIN_EXPORT ptSetSpacingWidget : 
	public wxPanel,
	public Core::Widgets::ProcessingWidget
{
public:
	coreDefineBaseWindowFactory( ptSetSpacingWidget )

	//!
	ptSetSpacingWidget(wxWindow* parent, 
		int id = wxID_ANY, 
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=0);

private:
	//!
	void OnButtonSetSpacing(wxCommandEvent& event);

	//!
	void OnInit();

	void OnModifiedInputImage();

	void do_layout();

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

private:
	wxButton* m_buttonSetSpacing;
	
	wxStaticText* emptylabel;
	wxStaticText* labelOrig;
	wxStaticText* labelNew;
    wxStaticText* labelX;
	wxStaticText* labelY;
	wxStaticText* labelZ;
	wxTextCtrl* m_xOrig;
	wxTextCtrl* m_yOrig;
	wxTextCtrl* m_zOrig;
    wxTextCtrl* m_xNew;
	wxTextCtrl* m_yNew;
	wxTextCtrl* m_zNew;

	wxStaticBox* m_ptSetSpacingWidgetStaticBox;

    wxDECLARE_EVENT_TABLE();

private:

	//!
	Core::SetSpacingProcessor::Pointer m_Processor;

};

#endif //COREptSetSpacingWidget_H
