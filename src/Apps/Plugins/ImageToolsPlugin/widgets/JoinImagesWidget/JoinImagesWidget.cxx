/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "JoinImagesWidget.h"

#include <wx/wupdlock.h>

#include "coreKernel.h"
#include "coreDataTreeHelper.h"
#include "coreDataEntityListBrowser.h"
#include "coreReportExceptionMacros.h"
#include "boost/filesystem.hpp"
#include <blMitkUnicode.h>
#include "coreDataTreeHelper.h"
#include "coreSelectionToolboxWidget.h"
#include "coreBBoxWidget.h"

using namespace mitk;
using namespace Core::Widgets;


BEGIN_EVENT_TABLE(JoinImagesWidget, wxPanel)
	EVT_BUTTON(wxID_btnJoin, JoinImagesWidget::OnButtonJoin)
	EVT_BUTTON(wxID_btnAdd, JoinImagesWidget::OnButtonAdd)
	EVT_BUTTON(wxID_btnClear, JoinImagesWidget::OnButtonClear)
END_EVENT_TABLE();


JoinImagesWidget::JoinImagesWidget(
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
		wxPanel(parent, id, pos, size, wxTAB_TRAVERSAL)
{
	m_JoinImagesWidgetStaticBox = new wxStaticBox(this, -1, wxT("Join Images Tool"));
	m_buttonJoin = new wxButton(this, wxID_btnJoin, wxT("Join"));
	m_buttonAdd = new wxButton(this, wxID_btnAdd, wxT("Add"));
	m_buttonClear = new wxButton(this, wxID_btnClear, wxT("Clear"));
	const wxString *m_listChoices = NULL;
	m_list = new wxListBox(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, m_listChoices, 0);

	m_SelectedDataEntities.clear();
	m_Processor = Core::JoinImagesProcessor::New();
	do_layout();
}

void JoinImagesWidget::OnInit( )
{
}

void JoinImagesWidget::OnButtonJoin( wxCommandEvent& event )
{
	std::vector<Core::DataEntity::Pointer> inputs;

	Core::DataEntityList::Pointer dataEntityList;
	dataEntityList = Core::Runtime::Kernel::GetDataContainer()->GetDataEntityList( );

	for (int i=0; i<m_SelectedDataEntities.size(); i++)
	{
		Core::DataEntity::Pointer dataEntity;
		dataEntity = dataEntityList->GetDataEntity(m_SelectedDataEntities[i]);
		inputs.push_back(dataEntity);
	}

	m_Processor->Update(inputs);

	inputs.clear();
}


void JoinImagesWidget::OnButtonClear( wxCommandEvent& event )
{
	m_SelectedDataEntities.clear();
	m_list->Clear();
}


void JoinImagesWidget::OnButtonAdd( wxCommandEvent& event )
{
	Core::DataEntity::Pointer dataEntity;
	dataEntity = Core::Runtime::Kernel::GetDataContainer()->GetDataEntityList( )->GetSelectedDataEntity();

	int id = dataEntity->GetId();

	if (std::find(m_SelectedDataEntities.begin(),m_SelectedDataEntities.end(),id) == m_SelectedDataEntities.end()) //not found
	{
		m_SelectedDataEntities.push_back(id);
		m_list->Append(dataEntity->GetMetadata()->GetName());
	}
}


void JoinImagesWidget::do_layout()
{
    wxBoxSizer* GlobalSizer = new wxBoxSizer(wxVERTICAL);
	wxStaticBoxSizer* JoinImagesWidget = new wxStaticBoxSizer(m_JoinImagesWidgetStaticBox, wxVERTICAL);
	wxBoxSizer* sizer_8 = new wxBoxSizer(wxVERTICAL);

	sizer_8->Add(m_buttonAdd, 0, wxEXPAND, 0);
	sizer_8->Add(m_list, 1, wxEXPAND, 0);
	sizer_8->Add(m_buttonClear, 0, wxEXPAND, 0);
	sizer_8->Add(m_buttonJoin, 0, wxEXPAND, 0);

	JoinImagesWidget->Add(sizer_8, 1, wxEXPAND, 2);
	
	GlobalSizer->Add(JoinImagesWidget, 0, wxEXPAND, 0);
	SetSizer(GlobalSizer);
	GlobalSizer->Fit(this);

}

Core::BaseProcessor::Pointer JoinImagesWidget::GetProcessor()
{
	return m_Processor.GetPointer();
}
