/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef COREJoinImagesWidget_H
#define COREJoinImagesWidget_H

#include "corePluginMacros.h"
#include "CILabNamespaceMacros.h"
#include "coreBBoxWidget.h"
#include "coreJoinImagesProcessor.h"
#include <wx/wx.h>
#include "wx/tglbtn.h"

// GuiBridgeLib
#include "gblWxConnectorOfWidgetChangesToSlotFunction.h"

namespace Core{
namespace Widgets{
		class DataEntityListBrowser;
}
}
	
#define wxID_btnJoin wxID_HIGHEST + 3
#define wxID_btnAdd wxID_HIGHEST + 2
#define wxID_btnClear wxID_HIGHEST + 1

/** 
\brief Join N images into 1 image with N timepoints.
\ingroup ImageToolsPlugin
\author Albert Sanchez
*/
class PLUGIN_EXPORT JoinImagesWidget : 
	public wxPanel,
	public Core::Widgets::ProcessingWidget
{
public:
	coreDefineBaseWindowFactory( JoinImagesWidget )

	//!
	JoinImagesWidget(wxWindow* parent, 
		int id = wxID_ANY, 
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=0);

	//!
	itk::ImageRegion<3> GetBox();


private:
	//!
	void OnButtonJoin(wxCommandEvent& event);
	//!
	void OnButtonAdd(wxCommandEvent& event);
	//!
	void OnButtonClear(wxCommandEvent& event);

	//!
	void OnInit();

	void do_layout();

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

private:
	wxStaticBox* m_JoinImagesWidgetStaticBox;
	wxButton* m_buttonJoin;
	wxButton* m_buttonAdd;
	wxButton* m_buttonClear;
	wxListBox* m_list;

    wxDECLARE_EVENT_TABLE();

private:

	//!
	Core::JoinImagesProcessor::Pointer m_Processor;

	std::vector<int> m_SelectedDataEntities;

};

#endif //COREJoinImagesWidget_H
