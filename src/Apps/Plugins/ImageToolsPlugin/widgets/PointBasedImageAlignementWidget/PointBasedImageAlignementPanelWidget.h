/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _PointBasedImageAlignementPanelWidget_H
#define _PointBasedImageAlignementPanelWidget_H

#include "PointBasedImageAlignementProcessor.h"
#include "PointBasedImageAlignementPanelWidgetUI.h"
#include "coreProcessingWidget.h"

// CoreLib
#include "coreProcessorOutputObserver.h"

namespace Core{ namespace Widgets {
	class UserHelper;
	class DataEntityListBrowser;
}}

#define wxID_PBImageAlignement_Widget wxID( "wxID_PBImageAlignement_Widget" )

/**
PanelWidget for interacting with GenericSegmentationPlugin::PointBasedImageAlignementProcessor.

The parent class PointBasedImageAlignementPanelWidgetUI contains all the code for 
buttons, labels, layouts, etc.

The PointBasedImageAlignementPanelWidget provides the functions that handle the GUI events 
generated by PointBasedImageAlignementPanelWidgetUI

PointBasedImageAlignementPanelWidgetUI is created with wxGlade, a graphical 
GUI editor. It is encouraged to use wxGlade for designing your widgets.

\ingroup GenericSegmentationPlugin
\author Luigi Carotenuto
\date 31 mar 2011
*/
class PointBasedImageAlignementPanelWidget : 
	public PointBasedImageAlignementPanelWidgetUI,
	public Core::Widgets::ProcessingWidget 
{

// OPERATIONS
public:
	//!
	coreDefineBaseWindowFactory( PointBasedImageAlignementPanelWidget )

	//!
	PointBasedImageAlignementPanelWidget( wxWindow* parent, int id = wxID_ANY, const wxPoint&  pos = wxDefaultPosition, const wxSize&  size = wxDefaultSize, long style = 0);

	//!
	~PointBasedImageAlignementPanelWidget( );

	//! Add button events to the bridge and call UpdateWidget()
	void OnInit(  );
	
	//!
	bool Enable( bool enable /*= true */ );

	//! Update GUI from working data
	void UpdateWidget();

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

	//!
	void ConnectInteractor(int whichInputImage);
	
	//!
	void DisconnectInteractor();
	
private:
    wxDECLARE_EVENT_TABLE();

	//! Button has been pressed
	void OnBtnFixedImagePts(wxCommandEvent& event);

	//! Button has been pressed
	void OnBtnMovingImagePts(wxCommandEvent& event);

	//!
	void UpdateHelperWidget( );

	void OnBtnUpdate(wxCommandEvent &event);

	void SetDisplaySideBySide(bool onOff);
// ATTRIBUTES
private:
	//! Working data of the processor
	gsp::PointBasedImageAlignementProcessor::Pointer m_Processor;

	bool m_ConnectingInteractor;

};


#endif //_PointBasedImageAlignementPanelWidget_H
