/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "PointBasedImageAlignementPanelWidget.h"

// GuiBridgeLib
#include "gblWxBridgeLib.h"
#include "gblWxButtonEventProxy.h"

// Core
#include "coreUserHelperWidget.h"
#include "coreReportExceptionMacros.h"
#include "coreProcessorInputWidget.h"
#include "coreRenderWindowContainer.h"

#include "coreLandmarkSelectorWidget.h"

#include "wxMitkMultiRenderWindowLayout.h"

// Event the widget
BEGIN_EVENT_TABLE(PointBasedImageAlignementPanelWidget, PointBasedImageAlignementPanelWidgetUI)
	EVT_BUTTON(wxID_BTN_FIXEDIMAGEPOINTS, PointBasedImageAlignementPanelWidget::OnBtnFixedImagePts)
	EVT_BUTTON(wxID_BTN_MOVINGIMAGEPOINTS, PointBasedImageAlignementPanelWidget::OnBtnMovingImagePts)
END_EVENT_TABLE()


PointBasedImageAlignementPanelWidget::PointBasedImageAlignementPanelWidget( 
	wxWindow* parent, int id, 
	const wxPoint& pos /*= wxDefaultPosition*/, 
	const wxSize& size /*= wxDefaultSize*/, long style /*= 0*/ )
: PointBasedImageAlignementPanelWidgetUI(parent, id, pos, size, style)
{
	m_Processor = gsp::PointBasedImageAlignementProcessor::New( );

	SetName( "Point based image alignement" );
	m_ConnectingInteractor = false;
}

PointBasedImageAlignementPanelWidget::~PointBasedImageAlignementPanelWidget( )
{
	// We don't need to destroy anything because all the child windows 
	// of this wxWindow are destroyed automatically
}

void PointBasedImageAlignementPanelWidget::OnInit( )
{
	GetInputWidget( gsp::PointBasedImageAlignementProcessor::INPUT_FIXED_IMAGE )->SetLabel("Fixed Image");
	GetInputWidget( gsp::PointBasedImageAlignementProcessor::INPUT_FIXED_IMAGE )->SetAutomaticSelection( false );
	GetInputWidget( gsp::PointBasedImageAlignementProcessor::INPUT_FIXED_IMAGE )->SetDefaultDataEntityFlag( false );
	GetInputWidget( gsp::PointBasedImageAlignementProcessor::INPUT_MOVING_IMAGE )->SetLabel("Moving Image");
	GetInputWidget( gsp::PointBasedImageAlignementProcessor::INPUT_MOVING_IMAGE )->SetAutomaticSelection( false );
	GetInputWidget(	gsp::PointBasedImageAlignementProcessor::INPUT_MOVING_IMAGE)->SetDefaultDataEntityFlag( false );
	GetInputWidget( gsp::PointBasedImageAlignementProcessor::INPUT_MOVING_MESH )->SetLabel("Moving Mesh");
	GetInputWidget( gsp::PointBasedImageAlignementProcessor::INPUT_MOVING_MESH )->SetAutomaticSelection( false );
	GetInputWidget(	gsp::PointBasedImageAlignementProcessor::INPUT_MOVING_MESH)->SetDefaultDataEntityFlag( false );
	GetInputWidget( gsp::PointBasedImageAlignementProcessor::INPUT_FIXED_MESH )->SetLabel("Fixed Mesh");
	GetInputWidget( gsp::PointBasedImageAlignementProcessor::INPUT_FIXED_MESH )->SetAutomaticSelection( false );
	GetInputWidget(	gsp::PointBasedImageAlignementProcessor::INPUT_FIXED_MESH)->SetDefaultDataEntityFlag( false );

	GetInputWidget(	gsp::PointBasedImageAlignementProcessor::INPUT_FIXED_POINTSET)->Hide();
	GetInputWidget(	gsp::PointBasedImageAlignementProcessor::INPUT_MOVING_POINTSET)->Hide();

	UpdateWidget();
}

void PointBasedImageAlignementPanelWidget::UpdateWidget()
{

	if (m_btnFixedImagePts->GetValue()) 
		m_btnFixedImagePts->SetLabel(wxString("Stop Fixed Points"));
	else
		m_btnFixedImagePts->SetLabel(wxString("Set Fixed Points"));

	if (m_btnMovingImagePts->GetValue()) 
		m_btnMovingImagePts->SetLabel(wxString("Stop Moving Points"));
	else
		m_btnMovingImagePts->SetLabel(wxString("Set Moving Points"));
}


void PointBasedImageAlignementPanelWidget::OnBtnFixedImagePts(wxCommandEvent& event)
{
	if ( event.IsChecked() )
	{
		m_btnMovingImagePts->SetValue(false);
		ConnectInteractor(gsp::PointBasedImageAlignementProcessor::INPUT_FIXED_IMAGE);
	}
	else
	{
		DisconnectInteractor();
	}
	UpdateWidget();
}


void PointBasedImageAlignementPanelWidget::OnBtnMovingImagePts(wxCommandEvent& event)
{
	if ( event.IsChecked() )
	{
		m_btnFixedImagePts->SetValue(false);
		ConnectInteractor(gsp::PointBasedImageAlignementProcessor::INPUT_MOVING_IMAGE);
	}
	else
	{
		DisconnectInteractor();
	}
	UpdateWidget();
}



bool PointBasedImageAlignementPanelWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = PointBasedImageAlignementPanelWidgetUI::Enable( enable );
	
	if ((enable) && (IsShown()))
			SetDisplaySideBySide(true);
	else
		SetDisplaySideBySide(false);

	DisconnectInteractor();
	m_btnFixedImagePts->SetValue(false);
	m_btnMovingImagePts->SetValue(false);
	UpdateWidget();
		
	return bReturn;
}


void PointBasedImageAlignementPanelWidget::ConnectInteractor(int whichInputImage)
{

	int whichInputPointSet;
	int whichImageToHide;
	int whereToDisplay;
	std::string pointSetName;
	if(( whichInputImage != gsp::PointBasedImageAlignementProcessor::INPUT_FIXED_IMAGE ) && 
	( whichInputImage != gsp::PointBasedImageAlignementProcessor::INPUT_MOVING_IMAGE ))
		return;
	
	if((m_Processor->GetInputDataEntity(gsp::PointBasedImageAlignementProcessor::INPUT_FIXED_IMAGE).IsNull()) || 
		(m_Processor->GetInputDataEntity(gsp::PointBasedImageAlignementProcessor::INPUT_MOVING_IMAGE).IsNull()))
		return;

	if( whichInputImage == gsp::PointBasedImageAlignementProcessor::INPUT_FIXED_IMAGE )
	{
		whichInputPointSet = gsp::PointBasedImageAlignementProcessor::INPUT_FIXED_POINTSET;
		whichImageToHide = gsp::PointBasedImageAlignementProcessor::INPUT_MOVING_IMAGE;
		pointSetName = "Fixed Image Landmarks";
		whereToDisplay = 0;
	}
	else
	{
		whichInputPointSet = gsp::PointBasedImageAlignementProcessor::INPUT_MOVING_POINTSET;
		whichImageToHide = gsp::PointBasedImageAlignementProcessor::INPUT_FIXED_IMAGE;
		pointSetName = "Moving Image Landmarks";
		whereToDisplay = 1;
	}

	m_ConnectingInteractor = true;
	try
	{
		Core::Widgets::RenderWindowContainer* mrwc 
			= dynamic_cast<Core::Widgets::RenderWindowContainer*>(GetPluginTab()->
			GetCurrentWorkingArea());

		mrwc->SetActiveWindow(whereToDisplay);
		//here hide the unwanted data enitity
		
		Core::RenderingTree::Pointer rendTree = GetRenderingTree();
		if(rendTree.IsNull())
			return;
		GetRenderingTree()->Show(m_Processor->GetInputDataEntity(whichImageToHide),false);

		DisconnectInteractor();
		ProcessingWidget::GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" )->
			SetAllowedInputDataTypes( Core::ImageTypeId );
		ProcessingWidget::GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" )->
			SetInteractorType( Core::PointInteractor::POINT_SET );
		ProcessingWidget::GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" )->
			SetInputDataEntity( m_Processor->GetInputDataEntity( whichInputImage ) );
		ProcessingWidget::GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" )->
			SetDataName( pointSetName );
		ProcessingWidget::GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" )->
			StartInteractor();	
		

		Core::PointInteractorPointSet* pointSetInteractor;
		pointSetInteractor = static_cast<Core::PointInteractorPointSet*> (
			ProcessingWidget::GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" )->
			GetPointInteractor( ).GetPointer( ));
		
		m_Processor->GetInputDataEntityHolder( whichInputPointSet )->SetSubject( 
			ProcessingWidget::GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" )->
			GetPointInteractor( )->GetSelectedPointsDataEntity( ) );

	}
	coreCatchExceptionsReportAndNoThrowMacro( "PointBasedImageAlignementPanelWidget::ConnectInteractor" );

	m_ConnectingInteractor = false;
}

void PointBasedImageAlignementPanelWidget::DisconnectInteractor()
{
	
	if ( ProcessingWidget::GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" ) )
	{
		ProcessingWidget::GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" )->
			StopInteraction( );
		ProcessingWidget::GetSelectionToolWidget< Core::Widgets::LandmarkSelectorWidget>( "Landmark selector" )->
			SetDefaultAllowedInputDataTypes( );
	}
	
}



void PointBasedImageAlignementPanelWidget::UpdateHelperWidget()
{
	/*
	if ( GetHelperWidget( ) == NULL )
	{
		return;
	}

	bool bInputPointsSelected = CheckInputPointSelected( );

	if (m_Processor->GetInputDataEntity( ResampleProcessor::INPUT_IMAGE ).IsNull( ) )
	{
		GetHelperWidget( )->SetInfo( 
			Core::Widgets::HELPER_INFO_ONLY_TEXT, 
			"Please select input data" );
	}
	else if ( !bInputPointsSelected )
	{
		GetHelperWidget( )->SetInfo( 
			Core::Widgets::HELPER_INFO_LEFT_BUTTON, 
			"Select input point using Toolbar button" );
	}
	else
	{
		GetHelperWidget( )->SetInfo( 
			Core::Widgets::HELPER_INFO_ONLY_TEXT, 
			"Please press Apply button" );
	}
	 */
}

void PointBasedImageAlignementPanelWidget::OnBtnUpdate(wxCommandEvent &event)
{
	m_Processor->m_params.setTransformationType(m_mode->GetSelection());
	m_Processor->Update();
	m_btnMovingImagePts->SetValue(false);
	m_btnFixedImagePts->SetValue(false);
	DisconnectInteractor();
}

Core::BaseProcessor::Pointer PointBasedImageAlignementPanelWidget::GetProcessor()
{
	return m_Processor.GetPointer( );
}

void PointBasedImageAlignementPanelWidget::SetDisplaySideBySide(bool onOff)
{
	if (GetPluginTab() && GetPluginTab()->GetWorkingAreaManager() && GetPluginTab()->GetWorkingAreaManager()->GetActiveMultiRenderWindow() )
	{
		if(onOff)
			GetPluginTab()->ShowWindow("SideBySideWorkingArea",true);
		else
			GetPluginTab()->ShowWindow("OrthoSliceWorkingArea",true);

		Core::Widgets::RenderWindowContainer* mrwc = 
			dynamic_cast<Core::Widgets::RenderWindowContainer*>(GetPluginTab()->GetCurrentWorkingArea());

		for(int i=0;i<mrwc->GetNumberOfWindows();i++)
		{
			mrwc->GetNumberOfWindows();
			mrwc->SetActiveWindow(i);
			GetMultiRenderWindow()->GetMetadata()->AddTag( "LayoutType", int( mitk::Default2x2_YXZ3D ) );
			GetMultiRenderWindow()->GetMetadata()->AddTag( "SlicesPlanesEnabled", true );
			GetMultiRenderWindow()->GetMetadataHolder()->NotifyObservers();
		}

	}
}
