/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "ChangeDirectionPanelWidget.h"
#include "coreProcessorInputWidget.h"
#include "coreMultiRenderWindowMITK.h"

ChangeDirectionPanelWidget::ChangeDirectionPanelWidget( 
	wxWindow* parent, int id,const wxPoint& pos /*= wxDefaultPosition*/, 
	const wxSize& size /*= wxDefaultSize*/, long style /*= 0*/ )
: ChangeDirectionPanelWidgetUI(parent, id, pos, size, style)
{
	m_Processor = ChangeDirectionProcessor::New();
}

ChangeDirectionPanelWidget::~ChangeDirectionPanelWidget( )
{
	// We don't need to destroy anything because all the child windows 
	// of this wxWindow are destroyed automatically
}

void ChangeDirectionPanelWidget::OnInit(  )
{
	GetInputWidget( 0 )->SetAutomaticSelection( false );
}

void ChangeDirectionPanelWidget::UpdateWidget()
{
}

void ChangeDirectionPanelWidget::UpdateData()
{

}

void ChangeDirectionPanelWidget::OnBtnApply(wxCommandEvent& event)
{
	try
	{
		// Get normal vector from each axis plane geometry
		Core::Widgets::RenderWindowBase* base = GetMultiRenderWindow();
		if ( !base )
		{
			throw Core::Exceptions::Exception( 
				"ChangeDirectionPanelWidget::OnBtnApply", "Render window is NULL" );
		}

		Core::Widgets::MultiRenderWindowMITK* renderWindow;
		renderWindow = dynamic_cast<Core::Widgets::MultiRenderWindowMITK*> ( base );
		if ( !renderWindow )
		{
			throw Core::Exceptions::Exception( 
				"ChangeDirectionPanelWidget::OnBtnApply", "Render window is not MITK" );
		}

		mitk::wxMitkMultiRenderWindow::WidgetListType sliceViews;
		sliceViews = renderWindow->GetWidgets();
		mitk::wxMitkMultiRenderWindow::WidgetListType::iterator it;
		int normals[ 4 ];

		// Initialize normals
		normals[ mitk::SliceNavigationController::Sagittal ] = -1;
		normals[ mitk::SliceNavigationController::Frontal ] = 1;
		normals[ mitk::SliceNavigationController::Transversal ] = -1;


		// Get normal
		for ( it = sliceViews.begin() ; it != sliceViews.end() ; it++ )
		{
			const mitk::PlaneGeometry* planeGeometry = (*it)->GetGeometry2D();

			mitk::SliceNavigationController::ViewDirection direction;
			direction = (*it)->GetSliceNavigationController()->GetViewDirection();

			double magnitude = planeGeometry->GetNormalVnl().magnitude();

			mitk::Vector3D normal;
			normal[ 0 ] = planeGeometry->GetNormalVnl()[ 0 ] / magnitude;
			normal[ 1 ] = planeGeometry->GetNormalVnl()[ 1 ] / magnitude;
			normal[ 2 ] = planeGeometry->GetNormalVnl()[ 2 ] / magnitude;

			if ( (*it)->GetSliceNavigationController()->GetFrontSide( ) )
			{
					normals[ direction ] *= -1;
			}

			normal *= normals[direction];


			switch ( direction )
			{
			case mitk::SliceNavigationController::Sagittal: 
				m_Processor->SetXAxesDirectionCosines( normal );
				break;
			case mitk::SliceNavigationController::Frontal: 
				m_Processor->SetYAxesDirectionCosines( normal );
				break;
			case mitk::SliceNavigationController::Transversal: 
				m_Processor->SetZAxesDirectionCosines( normal );
				break;
			}
		}

		UpdateProcessor();
	}
	coreCatchExceptionsReportAndNoThrowMacro( ChangeDirectionPanelWidget::OnBtnApply )
}

Core::BaseProcessor::Pointer ChangeDirectionPanelWidget::GetProcessor()
{
	return m_Processor.GetPointer( );
}

