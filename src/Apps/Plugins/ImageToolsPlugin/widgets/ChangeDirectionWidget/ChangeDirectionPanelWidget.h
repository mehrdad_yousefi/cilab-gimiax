/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _ChangeDirectionPanelWidget_H
#define _ChangeDirectionPanelWidget_H

#include "coreChangeDirectionProcessor.h"
#include "ChangeDirectionPanelWidgetUI.h"
#include "coreProcessingWidget.h"


/**
Change direction of image using axis

\ingroup ImageToolsPlugin
\author Xavi Planes
\date Oct 2011
*/
class ChangeDirectionPanelWidget : 
	public ChangeDirectionPanelWidgetUI,
	public Core::Widgets::ProcessingWidget
{

// OPERATIONS
public:
	//!
	coreDefineBaseWindowFactory( ChangeDirectionPanelWidget )

	//!
	ChangeDirectionPanelWidget( wxWindow* parent, int id = wxID_ANY, const wxPoint&  pos = wxDefaultPosition, const wxSize&  size = wxDefaultSize, long style = 0);

	//!
	~ChangeDirectionPanelWidget( );

	//! Add button events to the bridge and call UpdateWidget()
	void OnInit( );

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

private:
	//! Update GUI from working data
	void UpdateWidget();

	//! Update working data from GUI
	void UpdateData();

	//! Button has been pressed
	void OnBtnApply(wxCommandEvent& event);

// ATTRIBUTES
private:
	//! Working data of the processor
	ChangeDirectionProcessor::Pointer m_Processor;
};


#endif //_ChangeDirectionPanelWidget_H
