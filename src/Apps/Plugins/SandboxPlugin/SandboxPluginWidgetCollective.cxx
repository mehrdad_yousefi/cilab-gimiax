/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "SandboxPluginWidgetCollective.h"
#include "SandboxPluginShapeScalePanelWidget.h"
#include "SandboxPluginSubtractPanelWidget.h"
#include "SandboxPluginResamplePanelWidget.h"

#include "wxID.h"

#include "coreFrontEndPlugin.h"
#include "corePluginTab.h"
#include "coreWxMitkGraphicalInterface.h"

SandboxPlugin::WidgetCollective::WidgetCollective( ) 
{
	// Panel widgets
	Core::WindowConfig config;
	config.ProcessingTool( ).ProcessorObservers();

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
		ShapeScalePanelWidget::Factory::NewBase(), config.Category("Basic Surface Editing").Caption( "Scale Shape" ) );

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
		SubtractPanelWidget::Factory::NewBase(), config.Category("Image processing").Caption( "Substract Images" ) );

	Core::Runtime::Kernel::GetGraphicalInterface()->RegisterFactory(
		ResamplePanelWidget::Factory::NewBase(), config.Category("Image processing").Caption( "Resample an image" ) );

}

