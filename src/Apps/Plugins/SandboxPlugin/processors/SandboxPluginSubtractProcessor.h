/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _SandboxPluginSubtractProcessor_H
#define _SandboxPluginSubtractProcessor_H

#include "coreBaseProcessor.h"


#include "itkImage.h"

namespace SandboxPlugin{

/**
Processor for subtracting two images

\ingroup SandboxPlugin
\author Berta Marti
\date 24 oct 2008
*/
class SubtractProcessor : public Core::BaseProcessor
{
public:
	//!
	coreProcessor(SubtractProcessor, Core::BaseProcessor);
	typedef itk::Image<float,3> ImageType;
		
	//! Call library to perform operation
	void Update( );

private:
	//!
	SubtractProcessor();

	//!
	~SubtractProcessor();

	//! Purposely not implemented
	SubtractProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );

	void UpdateProgress(
		itk::Object *caller,
		const itk::EventObject& event);

	//!
	bool InternalCopy( BaseFilter* filter );

private:
	
};
    
} // namespace sdp{

#endif //_SandboxPluginSubtractProcessor_H
