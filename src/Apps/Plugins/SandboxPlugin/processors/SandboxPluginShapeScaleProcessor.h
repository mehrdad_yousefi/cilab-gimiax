/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _SandboxPluginShapeScaleProcessor_H
#define _SandboxPluginShapeScaleProcessor_H

#include "coreBaseProcessor.h"
#include "coreSmartPointerMacros.h"
#include "corePluginMacros.h"


namespace SandboxPlugin{

/**
Processor for scaling a shape

\ingroup SandboxPlugin
\author Maarten Nieber
\date 22 jun 2008
*/
class PLUGIN_EXPORT ShapeScaleProcessor : public Core::BaseProcessor
{
public:
	//!
	coreProcessor(ShapeScaleProcessor, Core::BaseProcessor);

	//! Call library to perform operation
	void Update( );

	//!
	float GetScale() const;
	void SetScale(float val);

private:
	//!
	ShapeScaleProcessor( );

	//!
	~ShapeScaleProcessor();

	//! Purposely not implemented
	ShapeScaleProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );

	//!
	bool InternalCopy( BaseFilter* filter );

	//!
	static void ProgressFunction(
		vtkObject* caller, long unsigned int eventId, void* clientData, void* callData);

	//!
	std::vector<vtkPointSet*> GetPointSet( );

private:
	//!
	float m_Scale;
};

} // namespace SandboxPlugin{

#endif //_SandboxPluginShapeScaleProcessor_H
