/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "SandboxPluginShapeScaleProcessor.h"

#include <string>

#include "vtkPolyData.h"

#include "coreReportExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreKernel.h"
#include "coreVTKPolyDataHolder.h"
#include "coreVTKUnstructuredGridHolder.h"

#include "vtkTransform.h"
#include "vtkTransformFilter.h"
#include "vtkCallbackCommand.h"

SandboxPlugin::ShapeScaleProcessor::ShapeScaleProcessor( )
{
	SetName( "ShapeScaleProcessor" );

	BaseProcessor::SetNumberOfInputs( 1 );
	GetInputPort( 0 )->SetName( "Mesh" );
	GetInputPort( 0 )->SetDataEntityType( Core::SurfaceMeshTypeId | Core::VolumeMeshTypeId );

	BaseProcessor::SetNumberOfOutputs( 1 );
	GetOutputPort( 0 )->SetDataEntityType( Core::SurfaceMeshTypeId );

	m_Scale = 0.5;
}

SandboxPlugin::ShapeScaleProcessor::~ShapeScaleProcessor()
{
}

void SandboxPlugin::ShapeScaleProcessor::Update()
{
	Core::DataEntity::Pointer	dataEntity = this->GetInputDataEntity( 0 );
	if ( dataEntity.IsNull( ) )
	{
		throw Core::Exceptions::Exception("ShapeScaleProcessor::GetPointSet", 
			"You must select an input data from the Processing Browser and set it as input" 
			);
	}

	std::vector<vtkPointSet*> pointSet = GetPointSet( );

	std::vector<Core::vtkPolyDataPtr> outputPolyData;
	std::vector<Core::vtkUnstructuredGridPtr> outputUnstructuredGrid;
	for ( size_t i = 0 ; i < pointSet.size() ; i++ )
	{
		// Configure transform
		vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
		transform->Scale( m_Scale, m_Scale, m_Scale );

		// Call the filter
		vtkSmartPointer<vtkTransformFilter> transformFilter;
		transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
		transformFilter->SetInput( pointSet[ i ] );
		transformFilter->SetTransform( transform );
		
		// Configure callback
		vtkSmartPointer<vtkCallbackCommand> progressCallback;
		progressCallback = vtkSmartPointer<vtkCallbackCommand>::New();
		progressCallback->SetCallback(ProgressFunction);
		progressCallback->SetClientData( GetUpdateCallback().GetPointer() );
		transformFilter->AddObserver( vtkCommand::ProgressEvent, progressCallback);

		// Update
		transformFilter->Update();

		if ( pointSet[ i ]->IsA( "vtkPolyData" ) )
		{
			outputPolyData.push_back( transformFilter->GetPolyDataOutput() );
		}
		else if ( pointSet[ i ]->IsA( "vtkUnstructuredGrid" ) )
		{
			outputUnstructuredGrid.push_back( transformFilter->GetUnstructuredGridOutput() );
		}
	}

	if ( outputPolyData.size() )
	{
		GetOutputPort( 0 )->SetDataEntityType( Core::SurfaceMeshTypeId );
		// Set the output to the output of this processor
		UpdateOutput( 
			0, 
			outputPolyData, 
			"ScaleMesh",
			true, 
			GetInputDataEntity( 0 ) );
	}
	else if ( outputUnstructuredGrid.size() )
	{
		GetOutputPort( 0 )->SetDataEntityType( Core::VolumeMeshTypeId );
		// Set the output to the output of this processor
		UpdateOutput( 
			0, 
			outputUnstructuredGrid, 
			"ScaleMesh",
			true, 
			GetInputDataEntity( 0 ) );
	}

}

float SandboxPlugin::ShapeScaleProcessor::GetScale() const
{
	return m_Scale;
}

void SandboxPlugin::ShapeScaleProcessor::SetScale( float val )
{
	m_Scale = val;
}

bool SandboxPlugin::ShapeScaleProcessor::InternalCopy( BaseFilter* filter )
{
	ShapeScaleProcessor* processor = static_cast<ShapeScaleProcessor*> ( filter );
	m_Scale = processor->GetScale();
	return true;
}

void SandboxPlugin::ShapeScaleProcessor::ProgressFunction(
	vtkObject* caller, long unsigned int eventId, void* clientData, void* callData)
{
	vtkAlgorithm* filter = static_cast<vtkAlgorithm*>(caller);
	Core::UpdateCallback* callback = static_cast<Core::UpdateCallback*> (clientData);
	callback->SetProgress( filter->GetProgress() );
	callback->Modified();

	filter->SetAbortExecute( callback->GetAbortProcessing() );
}

std::vector<vtkPointSet*> SandboxPlugin::ShapeScaleProcessor::GetPointSet()
{
	Core::DataEntity::Pointer	dataEntity = this->GetInputDataEntity( 0 );

	std::vector<vtkPointSet*> dataVector;
	try
	{
		std::vector<Core::vtkPolyDataPtr> vtkInputMeshVector;
		GetProcessingData( 0, vtkInputMeshVector );
		for ( size_t i = 0 ; i < vtkInputMeshVector.size() ; i++ )
			dataVector.push_back( vtkInputMeshVector[ i ] );
	}
	catch( ... )
	{
		std::vector<Core::vtkUnstructuredGridPtr> vtkInputMeshVector;
		GetProcessingData( 0, vtkInputMeshVector );
		for ( size_t i = 0 ; i < vtkInputMeshVector.size() ; i++ )
			dataVector.push_back( vtkInputMeshVector[ i ] );
	}

	return dataVector;
}

