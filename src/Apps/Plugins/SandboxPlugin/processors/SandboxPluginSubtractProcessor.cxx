/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "SandboxPluginSubtractProcessor.h"

#include <string>
#include <iostream>

#include "coreReportExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreDataEntityHelper.h"
#include "coreDataEntityHelper.txx"
#include "coreKernel.h"

#include "itkSubtractImageFilter.h"
#include "itkCommand.h"

SandboxPlugin::SubtractProcessor::SubtractProcessor( )
{
	SetName( "SubtractProcessor" );

	BaseProcessor::SetNumberOfInputs( 2 );
	GetInputPort( 0 )->SetName( "Input Image" );
	GetInputPort( 0 )->SetDataEntityType( Core::ImageTypeId );
	GetInputPort( 1 )->SetName( "Input Image" );
	GetInputPort( 1 )->SetDataEntityType( Core::ImageTypeId );

	BaseProcessor::SetNumberOfOutputs( 1 );
	GetOutputPort( 0 )->SetDataEntityType( Core::ImageTypeId );
}

SandboxPlugin::SubtractProcessor::~SubtractProcessor()
{
}

void SandboxPlugin::SubtractProcessor::Update()
{
	// Get the first image and switch implementation to ImageType
	ImageType::Pointer itkInputImage;
	GetProcessingData( 0, itkInputImage );
	
	// Get the second image and retrieve a new instance of processing data
	ImageType::Pointer itkInputImage2;
	GetProcessingData( 1, itkInputImage2, -1, true );

	// Call the function 
	typedef itk::SubtractImageFilter<ImageType> SubtractType;
	SubtractType::Pointer filter = SubtractType::New();
	filter->SetInput1( itkInputImage );
	filter->SetInput2( itkInputImage2 );

	// Add progress
	itk::MemberCommand<SandboxPlugin::SubtractProcessor>::Pointer command;
	command = itk::MemberCommand<SandboxPlugin::SubtractProcessor>::New();
	command->SetCallbackFunction( this, &SubtractProcessor::UpdateProgress );
	filter->AddObserver( itk::ProgressEvent(), command );

	// Update
	filter->Update();

	ImageType::Pointer itkOutputImage = filter->GetOutput();

	UpdateOutput(  
		0 , 
		filter->GetOutput(), 
		"SubtractProcessor",
		true, 
		1,
		GetInputDataEntity( 0 ) );

}

void SandboxPlugin::SubtractProcessor::UpdateProgress(
	itk::Object *caller,
	const itk::EventObject& event)
{
	itk::ProcessObject *processObject = (itk::ProcessObject*)caller;
	if (typeid(event) == typeid(itk::ProgressEvent)) 
	{
		GetUpdateCallback()->SetProgress( processObject->GetProgress() );
		GetUpdateCallback()->Modified();
	}

	if ( GetUpdateCallback()->GetAbortProcessing() )
	{
		processObject->SetAbortGenerateData( true );
	}

}

bool SandboxPlugin::SubtractProcessor::InternalCopy( BaseFilter* filter )
{
	// There are no parameters to copy
	return true;
}
