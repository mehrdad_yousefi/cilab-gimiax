/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _SandboxPluginResampleProcessor_H
#define _SandboxPluginResampleProcessor_H

#include "coreBaseProcessor.h"


#include "itkImage.h"
#include "itkScaleTransform.h"

namespace SandboxPlugin{

/**
Processor for Resampleing an image

\ingroup SandboxPlugin
\author Xavi Planes
\date 16 feb 2009
*/
class ResampleProcessor : public Core::BaseProcessor
{
public:
	typedef itk::Image<float,3> ImageType;
	typedef itk::ScaleTransform<double, 3> ScaleTransformType;

	typedef enum
	{
		INPUT_IMAGE,
		INPUT_POINT
	} INPUT_TYPE;

public:
	//!
	coreProcessor(ResampleProcessor, Core::BaseProcessor);

	//! Call library to perform operation
	void Update( );

	//!
	ScaleTransformType::Pointer GetScaleTransform() const;

	//!
	void UpdateProgress( itk::Object *caller, const itk::EventObject& event );

private:
	//!
	ResampleProcessor();

	//!
	~ResampleProcessor();

	//! Purposely not implemented
	ResampleProcessor( const Self& );

	//! Purposely not implemented
	void operator = ( const Self& );
	//!
	bool InternalCopy( BaseFilter* filter );

private:

	ScaleTransformType::Pointer m_ScaleTransform;
};
    
} // namespace SandboxPlugin{

#endif //_SandboxPluginResampleProcessor_H
