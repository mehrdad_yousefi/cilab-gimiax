/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "SandboxPluginResamplePanelWidget.h"

// Core
#include "coreUserHelperWidget.h"
#include "coreReportExceptionMacros.h"


SandboxPlugin::ResamplePanelWidget::ResamplePanelWidget( 
	wxWindow* parent, int id, 
	const wxPoint& pos /*= wxDefaultPosition*/, 
	const wxSize& size /*= wxDefaultSize*/, long style /*= 0*/ )
: SandboxPluginResamplePanelWidgetUI(parent, id, pos, size, style)
{
	m_Processor = ResampleProcessor::New( );
}

SandboxPlugin::ResamplePanelWidget::~ResamplePanelWidget( )
{
	// We don't need to destroy anything because all the child windows 
	// of this wxWindow are destroyed automatically
}

void SandboxPlugin::ResamplePanelWidget::OnInit( )
{
	UpdateWidget();
}

void SandboxPlugin::ResamplePanelWidget::UpdateWidget()
{
	SandboxPlugin::ResampleProcessor::ScaleTransformType::ScaleType scale;
	scale  = m_Processor->GetScaleTransform( )->GetScale( );

	// Update each text control with the parameters of the WorkingData
	m_SpinCtrlX->SetValue( scale[ 0 ] * 100 );
	m_SpinCtrlY->SetValue( scale[ 1 ] * 100 );
	m_SpinCtrlZ->SetValue( scale[ 2 ] * 100 );

	// Enable Apply button
	bool bInputPointsSelected = CheckInputPointSelected();

	m_btnResample->Enable( bInputPointsSelected );
}

void SandboxPlugin::ResamplePanelWidget::UpdateData()
{
	SandboxPlugin::ResampleProcessor::ScaleTransformType::ScaleType scale;

	// Update each paramterer of the WorkingData
	scale[ 0 ] = double( m_SpinCtrlX->GetValue( ) ) / 100;
	scale[ 1 ] = double( m_SpinCtrlY->GetValue( ) ) / 100;
	scale[ 2 ] = double( m_SpinCtrlZ->GetValue( ) ) / 100;
	m_Processor->GetScaleTransform( )->SetScale( scale );
}

void SandboxPlugin::ResamplePanelWidget::OnBtnApply(wxCommandEvent& event)
{
	// Update the scale values from widget to processor
	UpdateData();

	UpdateProcessor( );
}


bool SandboxPlugin::ResamplePanelWidget::Enable( bool enable /*= true */ )
{
	bool bReturn = SandboxPluginResamplePanelWidgetUI::Enable( enable );

	// If this panel widget is selected -> Update the widget
	if ( enable )
	{
		UpdateWidget();
	}

	return bReturn;
}

bool SandboxPlugin::ResamplePanelWidget::CheckInputPointSelected()
{
	if ( m_Processor->GetInputDataEntity( ResampleProcessor::INPUT_POINT ).IsNull( ) )
	{
		return false;
	}

	// Enable Apply button
	bool bInputPointsSelected = false;
	try{
		Core::vtkPolyDataPtr vtkInputPoint;
		m_Processor->GetProcessingData( ResampleProcessor::INPUT_POINT, vtkInputPoint );
		bInputPointsSelected = vtkInputPoint->GetNumberOfPoints() != 0;
	}catch(...)
	{

	}

	return bInputPointsSelected;
}

Core::BaseProcessor::Pointer SandboxPlugin::ResamplePanelWidget::GetProcessor()
{
	return m_Processor.GetPointer( );
}

void SandboxPlugin::ResamplePanelWidget::OnModifiedInput( int num )
{
	try
	{
		switch ( num )
		{
		case ResampleProcessor::INPUT_POINT: UpdateWidget(); break;
		case ResampleProcessor::INPUT_IMAGE: UpdateWidget( ); break;
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro( "ResamplePanelWidget::OnModifiedInput" );
}
