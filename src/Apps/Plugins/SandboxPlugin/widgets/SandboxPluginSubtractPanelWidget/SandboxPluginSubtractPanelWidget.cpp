/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "SandboxPluginSubtractPanelWidget.h"
#include "coreProcessorInputWidget.h"
#include "wxMitkMultiRenderWindowLayout.h"

SandboxPlugin::SubtractPanelWidget::SubtractPanelWidget( 
	wxWindow* parent, int id,const wxPoint& pos /*= wxDefaultPosition*/, 
	const wxSize& size /*= wxDefaultSize*/, long style /*= 0*/ )
: SandboxPluginSubtractPanelWidgetUI(parent, id, pos, size, style)
{
	m_Processor = SubtractProcessor::New();
}

SandboxPlugin::SubtractPanelWidget::~SubtractPanelWidget( )
{
	// We don't need to destroy anything because all the child windows 
	// of this wxWindow are destroyed automatically
}

void SandboxPlugin::SubtractPanelWidget::OnInit(  )
{
	GetInputWidget( 0 )->SetAutomaticSelection( false );
	GetInputWidget( 1 )->SetAutomaticSelection( false );

	GetProcessorOutputObserver(0)->PushProperty(blTag::New( "opacity", 0.5 ));
	UpdateWidget();
}

void SandboxPlugin::SubtractPanelWidget::UpdateWidget()
{
}

void SandboxPlugin::SubtractPanelWidget::UpdateData()
{

}

void SandboxPlugin::SubtractPanelWidget::OnBtnApply(wxCommandEvent& event)
{
	UpdateProcessor();
}

Core::BaseProcessor::Pointer SandboxPlugin::SubtractPanelWidget::GetProcessor()
{
	return m_Processor.GetPointer( );
}

void SandboxPlugin::SubtractPanelWidget::OnModifiedOutput( int num )
{
	GetProcessorOutputObserver(num)->AddProperty(blTag::New( "opacity", 0.8 ));
}
