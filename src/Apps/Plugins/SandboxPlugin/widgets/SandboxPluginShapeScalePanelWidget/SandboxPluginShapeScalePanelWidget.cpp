/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "SandboxPluginShapeScalePanelWidget.h"


// Event the widget
BEGIN_EVENT_TABLE(SandboxPlugin::ShapeScalePanelWidget, SandboxPluginShapeScalePanelWidgetUI)
	EVT_BUTTON(wxID_BTN_SCALE, SandboxPlugin::ShapeScalePanelWidget::OnBtnScale)
END_EVENT_TABLE()

SandboxPlugin::ShapeScalePanelWidget::ShapeScalePanelWidget( 
	wxWindow* parent, int id, const wxPoint& pos /*= wxDefaultPosition*/, 
	const wxSize& size /*= wxDefaultSize*/, long style /*= 0*/ )
: SandboxPluginShapeScalePanelWidgetUI(parent, id, pos, size, style )
{
	m_Processor = ShapeScaleProcessor::New();
}

SandboxPlugin::ShapeScalePanelWidget::~ShapeScalePanelWidget( )
{
	// We don't need to destroy anything because all the child windows 
	// of this wxWindow are destroyed automatically
}

void SandboxPlugin::ShapeScalePanelWidget::OnInit(  )
{
	UpdateWidget();
}

void SandboxPlugin::ShapeScalePanelWidget::UpdateWidget()
{
	m_textScale->SetValue( wxString::Format( "%.2f", m_Processor->GetScale() ) );
}

void SandboxPlugin::ShapeScalePanelWidget::UpdateData()
{
	// Update each paramterer of the WorkingData
	double scale = 0;
	m_textScale->GetValue( ).ToDouble( &scale );
	m_Processor->SetScale( scale );
}

void SandboxPlugin::ShapeScalePanelWidget::OnBtnScale(wxCommandEvent& event)
{
	UpdateData();

	UpdateProcessor();
}

Core::BaseProcessor::Pointer SandboxPlugin::ShapeScalePanelWidget::GetProcessor()
{
	return m_Processor.GetPointer( );
}

