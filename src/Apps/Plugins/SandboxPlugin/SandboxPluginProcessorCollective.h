/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _SandboxPluginProcessorCollective_H
#define _SandboxPluginProcessorCollective_H

#include "coreSmartPointerMacros.h"
#include "coreObject.h"

#include "SandboxPluginShapeScaleProcessor.h"
#include "SandboxPluginSubtractProcessor.h"
#include "SandboxPluginResampleProcessor.h"

namespace SandboxPlugin{

/**
This class instantiates all processors used in the plugin and registers them.

\ingroup SandboxPlugin
\author Maarten Nieber
\date 18 jun 2008
*/

class ProcessorCollective : public Core::SmartPointerObject
{
public:
	//!
	coreDeclareSmartPointerClassMacro(SandboxPlugin::ProcessorCollective, Core::SmartPointerObject);

private:
	//! The constructor instantiates all the processors and connects them.
	ProcessorCollective();

};

} // namespace SandboxPlugin{

#endif //_SandboxPluginProcessorCollective_H
