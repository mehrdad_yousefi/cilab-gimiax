/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDynDataTransferCLPShared.h"
#include "itksys/SystemTools.hxx"


Core::DynDataTransferCLPShared::DynDataTransferCLPShared()
{
}

Core::DynDataTransferCLPShared::~DynDataTransferCLPShared()
{
}


void Core::DynDataTransferCLPShared::UpdateModuleParameter( 
	ModuleParameter* param, 
	int num )
{
	// Share data entity pointer with images and use itk::DataEntityIO
	// to get and set the memory pointer
	if ( m_DynProcessor->GetModuleExecution()->GetModuleType( ) == "SharedObjectModule"
		 && param->GetTag() == "image" 
		 && param->GetFileExtensions().size( ) == 0 )
	{
		if ( param->GetChannel() == "input" )
		{
			std::ostringstream fnameString;
			fnameString << "DataEntity:"
				<< m_DynProcessor->GetInputPort( num )->GetDataEntity().GetPointer() 
				<< ":" << m_DynProcessor->GetInputPort( num )->GetSelectedTimeStep();
			param->SetDefault( fnameString.str() );
		}
		else if ( param->GetChannel() == "output" )
		{
			// Create new DataEntity
			Core::DataEntity::Pointer dataEntity;
			dataEntity = Core::DataEntity::New( ImageTypeId );
			dataEntity->GetMetadata()->SetName( param->GetName() );
			m_OutputDataEntities[ num ] = dataEntity;
			
			// Set Pointer to DataEntity
			std::ostringstream fnameString;
			fnameString << "DataEntity:" << dataEntity.GetPointer()
				<< ":" << m_DynProcessor->GetOutputPort( num )->GetSelectedTimeStep();
			param->SetDefault( fnameString.str() );
		}
	}
}

void Core::DynDataTransferCLPShared::UpdateProcessorOutput( ModuleParameter* param, int num )
{
	if ( param->GetTag() == "image" && 
		 m_DynProcessor->GetModuleExecution()->GetModuleType( ) == "SharedObjectModule" &&
		 param->GetFileExtensions().size() == 0 &&
		 m_OutputDataEntities.find( num ) != m_OutputDataEntities.end( ) )
	{
		Core::DataEntity::Pointer parentData = FindInputReferenceData( param );

		std::string name = m_DynProcessor->GetModule( )->GetTitle( ) + "_" + param->GetName();

		// For current iteration, only is valid the current time step, the rest is reset to 0
		int timeStep = m_DynProcessor->GetOutputPort( num )->GetSelectedTimeStep();

		m_DynProcessor->UpdateOutput( num, 
			m_OutputDataEntities[ num ]->GetProcessingData( TimeStepIndex( timeStep ) ), 
			name, !GetGenerateNewOutputData( ), 1, parentData );
	}
	else
	{
		DynDataTransferCLP::UpdateProcessorOutput( param, num );
	}

}

void Core::DynDataTransferCLPShared::CleanTemporaryFiles()
{
	DynDataTransferCLP::CleanTemporaryFiles();

	m_OutputDataEntities.clear();
}

void Core::DynDataTransferCLPShared::PreProcessData( ModuleParameter* param, int num )
{
	// If input is already set:
	if ( !param->GetDefault().empty() )
	{
		if ( param->GetChannel() == "input" )
		{
			if ( itksys::SystemTools::FileExists( param->GetDefault().c_str() ) )
			{
				// If file is already in disk, 
				// do nothing
			}
			else
			{
				// TODO: Try to read the file from remote path
				// and pass a pointer
			}
		}
		else
		{
			// Use desired output path 
		}
	}
	else 
	{
		// Create values of in/out
		UpdateModuleParameter( param, num );

		// If DataEntity cannot be passed, save it to disk
		if ( param->GetDefault().empty() )
		{
			DynDataTransferCLP::PreProcessData( param, num );
		}
	}

}

