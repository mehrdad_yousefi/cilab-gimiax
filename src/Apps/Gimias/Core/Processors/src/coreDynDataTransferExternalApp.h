/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreDynDataTransferExternalApp_H
#define _coreDynDataTransferExternalApp_H

// CoreLib
#include "gmProcessorsWin32Header.h"
#include "coreDynDataTransferCLP.h"
#include "dynModuleExecution.h"
#include <set>

namespace Core{

/**
\brief Used by ExternalApp to transfer data

The inputs/outputs passed to the plugin through disk.

\ingroup gmKernel
\author Xavi Planes
\date 14 07 2010
*/
class GMPROCESSORS_EXPORT DynDataTransferExternalApp : public Core::DynDataTransferCLP
{
	//!
	typedef std::map<int, std::string> DataEntityToFileNameMap;
public:
	//!
	coreDeclareSmartPointerClassMacro(Core::DynDataTransferExternalApp, Core::DynDataTransferBase);
	coreDefineFactoryClass( DynDataTransferExternalApp )
	coreDefineFactoryTagsBegin( )
	coreDefineFactoryAddTag( "ModuleType", std::string( "ExternalApp" ) )
	coreDefineFactoryTagsEnd( )
	coreDefineFactoryClassEnd( )

protected:
	//!
	DynDataTransferExternalApp(void);

	//!
	virtual ~DynDataTransferExternalApp(void);

private:
	//!
	DynDataTransferExternalApp( const Self& );

	//!
	void operator=(const Self&);

protected:
};

} // namespace Core{


#endif // _coreDynDataTransferBaseCLP_H


