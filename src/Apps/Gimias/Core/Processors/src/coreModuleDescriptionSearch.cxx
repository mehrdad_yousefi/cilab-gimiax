/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreModuleDescriptionSearch.h"
#include "coreWxMitkGraphicalInterface.h"
#include "corePluginProviderManager.h"
#include "coreWorkflowManager.h"

#include "dynModuleHelper.h"

#include "blTextUtils.h"


ModuleDescriptionSearch::ModuleDescriptionSearch( )
{
	m_OnlyEnabled = false;
}

ModuleDescriptionSearch::~ModuleDescriptionSearch()
{
}

void ModuleDescriptionSearch::Update()
{
	ScannAllPluginProviders( );

	m_FoundModuleList.clear( );
	std::list<ModuleDescription>::iterator itModule;
	for ( itModule = m_AllModuleList.begin() ; itModule != m_AllModuleList.end() ; itModule++ )
	{

		bool matchesType = true;
		if ( !m_TypeList.empty() )
		{
			matchesType = 
				std::find( m_TypeList.begin( ), m_TypeList.end( ), itModule->GetType() ) != m_TypeList.end() || 
				std::find( m_TypeList.begin( ), m_TypeList.end( ), itModule->GetAlternativeType() ) != m_TypeList.end();
		}

		bool matchesHierarchy = m_Hierarchy.empty() || m_Hierarchy == itModule->GetHierarchy();
		bool matchesTitle = m_ModuleTitle.empty() || m_ModuleTitle == itModule->GetTitle();
		bool matchesEnabled = !m_OnlyEnabled || m_ModuleEnabled[ itModule->GetTitle( ) ] == true;

		if ( matchesType && matchesHierarchy && matchesTitle && matchesEnabled )
		{
			m_FoundModuleList.push_back( *itModule );
		}
	}

}


ModuleDescription ModuleDescriptionSearch::FindModuleDescription( const std::string factoryName )
{
	// Remove "Factory" word at the end to get class name
	std::string hyerarchyName = factoryName;
	blTextUtils::StrSub( hyerarchyName, "Factory", "" );

	ModuleDescriptionSearch::Pointer search = ModuleDescriptionSearch::New( );
	search->SetHierarchy( hyerarchyName );
	search->Update();
	std::list<ModuleDescription> list = search->GetFoundModuleList();
	if ( !list.empty() )
	{
		return *list.begin();
	}

	return ModuleDescription();
}

void ModuleDescriptionSearch::AddType( std::string val )
{
	m_TypeList.push_back( val );
}

std::list<ModuleDescription> ModuleDescriptionSearch::GetFoundModuleList() const
{
	return m_FoundModuleList;
}

std::string ModuleDescriptionSearch::FindProvider( const std::string &moduleTitle )
{
	std::string pluginProviderName;

	// Get ModuleDescription from factory of CLP
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	Core::Runtime::PluginProviderManager::Pointer pluginProviderManager;
	pluginProviderManager = graphicalIface->GetPluginProviderManager();

	Core::Runtime::PluginProviderManager::PluginProvidersType providers;
	providers = pluginProviderManager->GetPluginProviders( );
	Core::Runtime::PluginProviderManager::PluginProvidersType::iterator it;
	for ( it = providers.begin() ; it != providers.end() ; it++ )
	{
		ModuleFactory *factory = (*it)->GetModuleFactory( );
		if ( factory == NULL )
		{
			continue;
		}

		ModuleDescription module = factory->GetModuleDescription( moduleTitle );
		if ( module.GetTitle() != "Unknown" )
		{
			pluginProviderName = (*it)->GetName( );
			break;
		}
	}

	return pluginProviderName;
}

void ModuleDescriptionSearch::CreateWorkflowModules()
{
	// Scan workflows
	Core::WorkflowManager::WorkflowListType list;
	list = Core::Runtime::Kernel::GetWorkflowManager()->GetWorkflowList( );
	Core::WorkflowManager::WorkflowListType::iterator itWorkflow;
	for ( itWorkflow = list.begin() ; itWorkflow != list.end( ) ; itWorkflow++ )
	{
		Core::Workflow::StepListType steps = (*itWorkflow)->GetStepVector();
		if ( steps.empty() )
		{
			continue;
		}

		Core::WorkflowStep::Pointer firstStep = *steps.begin();
		Core::WorkflowStep::Pointer lastStep = *steps.rbegin();

		// Find ModuleDescription for first step
		Core::WorkflowSubStep::Pointer subStep;
		subStep = firstStep->Get( 0 );
		if ( subStep.IsNull() )
		{
			continue;
		}
		ModuleDescription firstModule = FindModuleDescription( subStep->GetName() );
		if ( firstModule.GetTitle() == "Unknown" )
		{
			continue;
		}

		// Find ModuleDescription for last step
		size_t lastSubStepPos = lastStep->GetSubStepVector( ).size() - 1;
		subStep = lastStep->Get( int( lastSubStepPos ) );
		if ( subStep.IsNull() )
		{
			continue;
		}
		ModuleDescription lastModule = FindModuleDescription( subStep->GetName() );
		if ( lastModule.GetTitle() == "Unknown" )
		{
			continue;
		}


		// Use inputs of first module and use outputs using last module
		ModuleDescription module = firstModule;

		// Remove All outputs
		while ( ModuleParameter* param = dynModuleHelper::GetOutput( &module, 0 ) )
		{
			dynModuleHelper::RemoveParameter( &module, param );
		}

		// Copy outputs from last to module
		ModuleParameterGroup outputsGroup;
		outputsGroup.SetLabel( "Outputs" );
		for ( int i = 0 ; i < dynModuleHelper::GetNumberOfOutputs( &lastModule ) ; i++ )
		{
			ModuleParameter* param = dynModuleHelper::GetOutput( &lastModule, i );
			outputsGroup.AddParameter( *param );
		}

		module.AddParameterGroup( outputsGroup );
		module.SetType( "WorkflowModule" );
		module.SetTitle( (*itWorkflow)->GetName() );


		m_ModuleEnabled[ module.GetTitle( ) ] = true;
		m_AllModuleList.push_back( module );
	}
}

void ModuleDescriptionSearch::SetHierarchy( std::string val )
{
	m_Hierarchy = val;
}

void ModuleDescriptionSearch::SetModuleTitle( std::string val )
{
	m_ModuleTitle = val;
}

void ModuleDescriptionSearch::ScanOnlyEnabled( bool value )
{
	m_OnlyEnabled = value;
}

void ModuleDescriptionSearch::ScannAllPluginProviders()
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	// GetPlugin provider manager
	Core::Runtime::PluginProviderManager::Pointer pluginProviderManager;
	pluginProviderManager = graphicalIface->GetPluginProviderManager();

	Core::Runtime::PluginProviderManager::PluginProvidersType providers;
	providers = pluginProviderManager->GetPluginProviders( );
	Core::Runtime::PluginProviderManager::PluginProvidersType::iterator it;
	for ( it = providers.begin() ; it != providers.end() ; it++ )
	{
		if ( (*it)->GetModuleFactory( ) == NULL )
		{
			continue;
		}

		blTagMap::Pointer plugins;
		plugins = (*it)->GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );

		// Get all modules from ModuleFactory. Some modules are not registered 
		// into the framework se the only way is using ModuleFactory
		std::vector<std::string> moduleNames = (*it)->GetModuleFactory( )->GetModuleNames( );
		for ( size_t i = 0 ; i < moduleNames.size() ; i++ )
		{
			ModuleDescription module;
			module = (*it)->GetModuleFactory( )->GetModuleDescription( moduleNames[ i ] );

			// Get if the module is enabled. This only is valid for some types of modules
			if ( plugins && plugins->FindTagByName( moduleNames[ i ] ).IsNotNull( ) )
			{
				blTag::Pointer tag = plugins->FindTagByName( moduleNames[ i ] );

				// If category has the same name as a plugin -> search inside the category tag
				if ( tag.IsNotNull() && tag->GetTypeName( ) != blTag::GetTypeName( typeid( bool ) ) )
				{
					tag = tag->GetValueCasted<blTagMap::Pointer>( )->FindTagByName( moduleNames[ i ] );
				}

				// Get value of the plugin
				if ( tag.IsNotNull( ) )
				{
					m_ModuleEnabled[ module.GetTitle( ) ] = tag->GetValueCasted<bool>();
				}
				else
				{
					m_ModuleEnabled[ module.GetTitle( ) ] = false;
				}
			}
			else
			{
				// If tag is not found -> the module is always enabled
				m_ModuleEnabled[ module.GetTitle( ) ] = true;
			}

			m_AllModuleList.push_back( module );
		}
	}
}

void ModuleDescriptionSearch::ClearAllModuleList()
{
	m_AllModuleList.clear();
}
