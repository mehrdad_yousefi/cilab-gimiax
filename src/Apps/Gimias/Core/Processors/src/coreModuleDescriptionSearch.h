/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _ModuleDescriptionSearch_H
#define _ModuleDescriptionSearch_H

#include "gmProcessorsWin32Header.h"
#include "ModuleDescription.h"


/**
Search for ModuleDescription instances on PluginProviders and 
creates ModuleDescription for Workflows

Scan all available ModuleDescription from all Plugin providers and fill m_ModuleList.

\ingroup gmProcessors
\author Xavi Planes
\date Oct 2011
*/
class GMPROCESSORS_EXPORT ModuleDescriptionSearch : public Core::SmartPointerObject
{
public:
	coreDeclareSmartPointerClassMacro(ModuleDescriptionSearch, Core::SmartPointerObject);

	//! If only enabled is true, search only enabled types
	void AddType(std::string val );

	//!
	void SetHierarchy(std::string val);

	//!
	void SetModuleTitle(std::string val);

	//! For command line, find modules that are enabled
	void ScanOnlyEnabled( bool value );

	//!
	virtual void Update( );

	//!
	std::list<ModuleDescription> GetFoundModuleList() const;

	//!
	void ClearAllModuleList( );

	/**
	Create specific ModuleDescription for Workflows, using the first step 
	inputs and last step outputs. The type of ModuleDescription is "WorfklowModule".
	*/
	void CreateWorkflowModules( );

	/** Find plugin provider name for a concrete ModuleDescription
	searching over all Plugin Providers
	*/
	static std::string FindProvider( const std::string &moduleTitle );

protected:
	//!
	ModuleDescriptionSearch( );

	//!
	virtual ~ModuleDescriptionSearch( );

	/** Find Module description over all Plugin Providers comparing factory name
	with hierarchy.
	*/
	ModuleDescription FindModuleDescription( const std::string factoryName );

	//!
	void ScannAllPluginProviders( );

private:
	//!
	std::list<ModuleDescription> m_AllModuleList;

	//! Enabled property for each module
	std::map<std::string,bool> m_ModuleEnabled;

	//!
	std::list<ModuleDescription> m_FoundModuleList;

	//!
	std::list<std::string> m_TypeList;

	//!
	std::string m_Hierarchy;

	//!
	std::string m_ModuleTitle;

	//!
	bool m_OnlyEnabled;
};

#endif // _ModuleDescriptionSearch_H

