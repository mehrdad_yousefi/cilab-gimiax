/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreDynDataTransferBaseDLL_H
#define _coreDynDataTransferBaseDLL_H

// CoreLib
#include "gmProcessorsWin32Header.h"
#include "coreDynDataTransferBase.h"
#include "dynModuleExecution.h"

namespace Core{

/**
\brief Use ModuleDescription to call a filter of a dynamic library.

Use the tag "DataType" of the XML description to check the data type
of the processing data. 

\ingroup gmProcessors
\author Xavi Planes
\date 14 07 2010
*/
class GMPROCESSORS_EXPORT DynDataTransferDLL : public Core::DynDataTransferBase
{
public:
	//!
	coreDeclareSmartPointerClassMacro(Core::DynDataTransferDLL, Core::DynDataTransferBase)
	coreDefineFactoryClass( DynDataTransferDLL )
	coreDefineFactoryTagsBegin( )
	coreDefineFactoryAddTag( "ModuleType", std::string( "RawDynLibModule" ) )
	coreDefineFactoryTagsEnd( )
	coreDefineFactoryClassEnd( )

	//!
	virtual void PreProcessData( ModuleParameter* param, int num );

	//!
	virtual void UpdateProcessorOutput( ModuleParameter* param, int num );

protected:
	//!
	DynDataTransferDLL(void);

	//!
	virtual ~DynDataTransferDLL(void);

private:
	//!
	DynDataTransferDLL( const Self& );

	//!
	void operator=(const Self&);

protected:
};

} // namespace Core{


#endif // _coreDynDataTransferBaseDLL_H


