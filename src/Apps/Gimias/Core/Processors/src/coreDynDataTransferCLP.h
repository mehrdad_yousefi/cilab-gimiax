/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreDynDataTransferBaseCLP_H
#define _coreDynDataTransferBaseCLP_H

// CoreLib
#include "gmProcessorsWin32Header.h"
#include "coreDynDataTransferBase.h"
#include "dynModuleExecution.h"
#include <set>

namespace Core{

/**
\brief Use ModuleDescription to call a filter of a Command Line Plugin
as an executable.

The inputs/outputs passed to the plugin through disk.

\ingroup gmProcessors
\author Xavi Planes
\date 14 07 2010
*/
class GMPROCESSORS_EXPORT DynDataTransferCLP : public Core::DynDataTransferBase
{
	//!
	typedef std::map<int, std::string> DataEntityToFileNameMap;
public:
	//!
	coreDeclareSmartPointerClassMacro(Core::DynDataTransferCLP, Core::DynDataTransferBase);
	coreDefineFactoryClass( DynDataTransferCLP )
	coreDefineFactoryTagsBegin( )
	coreDefineFactoryAddTag( "ModuleType", std::string( "CommandLineModule" ) )
	coreDefineFactoryTagsEnd( )
	coreDefineFactoryClassEnd( )

	/** Use m_UseCaseDirectory to create a default filename for inputs/outputs
	*/
	void UpdateModuleParameter( ModuleParameter* param, int num );

	//!
	virtual void UpdateProcessorOutput( ModuleParameter* param, int num );

	/** Get extension of a parameter, using the extension attribute if it's not empty 
	or the 	default extension of the data type depending if it's an image, geometry, ...
	*/
	static std::string GetExtension( ModuleParameter* param );

protected:
	//!
	DynDataTransferCLP(void);

	//!
	virtual ~DynDataTransferCLP(void);

	//!
	virtual void CleanTemporaryFiles();

	/** Search files that doesn't exist but the multiple time steps files exist
	Return a file list separated by ;
	*/
	std::string SearchMultipleTimeSteps( const std::string &filename );

	/** Save input data to disk using Default value
	If default value is empty, don't write it.
	If input DataEntity is empty, don't write it.
	*/
	virtual void PreProcessData( ModuleParameter* param, int num );

	/** Save default parameter to disk using m_UseCaseDirectory and param->GetName( ) 

	Read data into memory and save it.
	\note If the file already exists, doens't do anything
	*/
	void SaveInputToDisk( ModuleParameter* param );

private:
	//!
	DynDataTransferCLP( const Self& );

	//!
	void operator=(const Self&);

protected:
};

} // namespace Core{


#endif // _coreDynDataTransferBaseCLP_H


