/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDynProcessor.h"
#include "coreDynDataTransferBase.h"
#include "coreDynDataTransferCLP.h"
#include "coreDynDataTransferCLPShared.h"
#include "coreDynDataTransferDLL.h"
#include "coreDynDataTransferProcessor.h"
#include "coreDynDataTransferExternalApp.h"
#include "coreStringHelper.h"
#include "dynModuleHelper.h"
#include "coreWxMitkGraphicalInterface.h"
#include "corePluginProviderManager.h"
#include "coreSettings.h"

#include "coreModuleExecutionProcessor.h"
#include "coreDataEntityInfoHelper.h"

#include <time.h>
#include "itksys/SystemTools.hxx"

Core::DynProcessor::DynProcessor()
{
	m_ModuleExecution = dynModuleExecution::New();

	FactoryManager::Register( DynDataTransferBase::GetNameClass(), DynDataTransferCLP::Factory::New() );
	FactoryManager::Register( DynDataTransferBase::GetNameClass(), DynDataTransferCLPShared::Factory::New() );
	FactoryManager::Register( DynDataTransferBase::GetNameClass(), DynDataTransferDLL::Factory::New() );
	FactoryManager::Register( DynDataTransferBase::GetNameClass(), DynDataTransferProcessor::Factory::New() );
	FactoryManager::Register( DynDataTransferBase::GetNameClass(), DynDataTransferExternalApp::Factory::New() );

	dynModuleExecution::RegisterImpl( "ProcessorModule", ModuleExecutionProcessor::Factory::New( ) );

	GenerateUseCase();

	m_AttachedProcess = false;
	m_IOTimeout = 0;
}
	 

Core::DynProcessor::~DynProcessor()
{
}

void Core::DynProcessor::GenerateUseCase() 
{
	std::ostringstream fnameString;
	fnameString << this << time( NULL );
	std::string fname = StringHelper::ConvertDigitsToCharacters( fnameString );
	m_UseCase = fname;
}

void Core::DynProcessor::SetModule( ModuleDescription* module )
{
	m_ModuleExecution->SetModule( module );
	
	SetName( module->GetTitle() );

	// Set inputs, names and optional configuration parameter
	SetNumberOfInputs( dynModuleHelper::GetNumberOfInputs( module ) );
	for ( size_t i = 0 ; i < GetNumberOfInputs() ; i++ )
	{
		ModuleParameter* param;
		param = dynModuleHelper::GetInput( module, i );
		GetInputPort( i )->SetName( param->GetLabel() );

		// Is a optional parameter
		bool isOptional = param->GetIndex().empty();
		GetInputPort( i )->SetOptional( isOptional );

		Core::DataEntityType type;
		type = DataEntityInfoHelper::DataTypeToInputId( DataType( param->GetTag(), param->GetType() ) );
		GetInputPort( i )->SetDataEntityType( type );

		// Set update mode
		if ( param->GetAccessMode( ) == "multiple" )
		{
			GetInputPort( i )->SetUpdateMode( BaseFilterInputPort::UPDATE_ACCESS_MULTIPLE_TIME_STEP );
		}
		else
		{
			// By default is using single time step to allow executing a single CLP for multiple time steps
			GetInputPort( i )->SetUpdateMode( BaseFilterInputPort::UPDATE_ACCESS_SINGLE_TIME_STEP );
		}
	}

	// Set outputs
	SetNumberOfOutputs( dynModuleHelper::GetNumberOfOutputs( module ) );
	for ( size_t i = 0 ; i < GetNumberOfOutputs() ; i++ )
	{
		ModuleParameter* param;
		param = dynModuleHelper::GetOutput( module, i );
		bool isOptional = param->GetIndex().empty();
		GetOutputPort( i )->SetName( param->GetName() );
		GetOutputPort( i )->SetDataEntityName( param->GetLabel() );
		GetOutputPort( i )->SetOptional( isOptional );

		Core::DataEntityType type;
		type = DataEntityInfoHelper::DataTypeToId( DataType( param->GetTag(), param->GetType() ) );
		GetOutputPort( i )->SetDataEntityType( type );
	}
}

ModuleDescription * Core::DynProcessor::GetModule()
{
	return m_ModuleExecution->GetModule();
}

void Core::DynProcessor::Update()
{
	DynDataTransferBase::Pointer dataTransfer;
	dataTransfer = DynDataTransferBase::NewDataTransfer( m_ModuleExecution->GetModuleType() );

	try
	{
		dataTransfer->SetDynProcessor( this );
		dataTransfer->SetTemporaryDirectory( m_TemporaryDirectory );
		dataTransfer->SetIOTimeout( m_IOTimeout );

		// Check if use case directory already exists
		bool exists = true;
		do
		{
			dataTransfer->BuildUseCaseDirectory( );
			exists = dataTransfer->ExistsUseCaseDirectory( );
			// If directory already exists and is not an already running process -> generate a new use case
			if ( exists && !GetAttachedProcess( ) )
			{
				GenerateUseCase( );
				itksys::SystemTools::Delay( 100 );
			}
		} while ( exists && !GetAttachedProcess( ) );

		// Pre Process data
		dataTransfer->PreProcessData();

		// This process can be aborted before executing the command line plugin
		bool processAbortedBeforeExecuting = GetUpdateCallback()->GetAbortProcessing();
		if ( !processAbortedBeforeExecuting )
		{
			m_DynUpdateCallback = DynUpdateCallback::New();
			m_DynUpdateCallback->SetUpdateCallback( GetUpdateCallback() );
			m_DynUpdateCallback->SetModule( GetModule() );

			GetModuleExecution()->SetUseCaseDirectory( dataTransfer->GetUseCaseDirectory() );
			GetModuleExecution()->SetUpdateCallback( m_DynUpdateCallback.GetPointer() );
			GetModuleExecution()->Update(  );
		}

		// If user has not aborted processing, update outputs
		if ( !GetUpdateCallback()->GetAbortProcessing() )
		{
			dataTransfer->PostProcessData();
		}

		// Clean temporary files if process is not detached
		if ( !GetUpdateCallback()->GetDetachProcessing() ||
			   processAbortedBeforeExecuting )
		{
			dataTransfer->CleanTemporaryFiles( );
		}
		GetModuleExecution()->Clean();
	}
	catch ( ... )
	{
		try
		{
			dataTransfer->CleanTemporaryFiles( );
			GetModuleExecution()->Clean();
		}
		catch(...)
		{

		}
		throw;
	}
}

dynModuleExecution::Pointer Core::DynProcessor::GetModuleExecution() const
{
	return m_ModuleExecution;
}

std::string Core::DynProcessor::GetTemporaryDirectory() const
{
	return m_TemporaryDirectory;
}

void Core::DynProcessor::SetTemporaryDirectory( std::string val )
{
	m_TemporaryDirectory = val;
}

void Core::DynProcessor::SetWorkingDirectory( std::string val )
{
	m_ModuleExecution->SetWorkingDirectory( val );
}

bool Core::DynProcessor::InternalCopy( BaseFilter* filter )
{
	DynProcessor* processor = dynamic_cast<DynProcessor*> ( filter );
	m_TemporaryDirectory = processor->GetTemporaryDirectory();
	m_ModuleExecution->Copy( processor->GetModuleExecution() );
	m_ActivePluginProviderName = processor->m_ActivePluginProviderName;
	m_AttachedProcess = processor->GetAttachedProcess();
	if ( m_AttachedProcess )
	{
		m_UseCase = processor->GetUseCase();
	}
	return true;
}

void Core::DynProcessor::SetActivePluginProviderName( std::string val )
{
	m_ActivePluginProviderName = val;
}

std::string Core::DynProcessor::GetActivePluginProviderName() const
{
	return m_ActivePluginProviderName;
}

std::string Core::DynProcessor::GetUseCase() const
{
	return m_UseCase;
}

void Core::DynProcessor::SetUseCase( const std::string &val )
{
	m_UseCase = val;
}

void Core::DynProcessor::AttachProcess( std::string val )
{
	m_UseCase = val;
	m_AttachedProcess = true;
}

bool Core::DynProcessor::GetAttachedProcess() const
{
	return m_AttachedProcess;
}

bool Core::DynProcessor::IsExecutedLocally()
{
	return GetModuleExecution()->IsExecutedLocally();
}

void Core::DynProcessor::UpdateProcessorDirectories( std::string activePluginProvider )
{
	if ( activePluginProvider.empty() )
	{
		return;
	}

	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	// GetPlugin provider
	Runtime::PluginProviderManager::Pointer pluginProviderManager;
	pluginProviderManager = graphicalIface->GetPluginProviderManager();

	Core::Runtime::PluginProvider::Pointer provider;
	provider = pluginProviderManager->GetPluginProvider( activePluginProvider );
	if ( provider.IsNull() )
	{
		throw Core::Exceptions::Exception( 
			"DynProcessingWidget::SetPluginProviderConfiguration",
			"Cannot find the plugin provider" );
	}

	// GetPluginProvider configuration
	if ( provider->GetProperties()->FindTagByName( "Working directory" ).IsNull() )
	{
		return;
	}

	std::string workingDir = provider->GetProperties()->FindTagByName( "Working directory" )->GetValueAsString();
	std::string tempDir;
	if ( provider->GetProperties()->FindTagByName( "Temporary directory" ).IsNotNull() )
	{
		tempDir = provider->GetProperties()->FindTagByName( "Temporary directory" )->GetValueAsString();
	}
	else
	{
		tempDir = workingDir;
	}


	Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
	settings->ReplaceGimiasPath( workingDir );
	settings->ReplaceGimiasPath( tempDir );

	// Set working and temporary directory
	SetTemporaryDirectory( tempDir );
	SetWorkingDirectory( workingDir );
}

void Core::DynProcessor::SetIOTimeout( size_t timeout )
{
	m_IOTimeout = timeout;
}

