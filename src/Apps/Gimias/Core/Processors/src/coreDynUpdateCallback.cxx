/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDynUpdateCallback.h"
#include "ModuleDescription.h"

Core::DynUpdateCallback::DynUpdateCallback()
{
	m_Module = NULL;
}

Core::DynUpdateCallback::~DynUpdateCallback()
{
}

void Core::DynUpdateCallback::Modified()
{
	m_UpdateCallback->SetProgress( m_Module->GetProcessInformation()->Progress );
	m_UpdateCallback->SetStatusMessage( m_Module->GetProcessInformation()->ProgressMessage );
	m_UpdateCallback->SetInformationMessage( GetInformationMessage( ) );
	m_UpdateCallback->Modified();

	if ( m_UpdateCallback->GetAbortProcessing( ) )
	{
		m_Module->GetProcessInformation()->Abort = 1;
	}
}

void Core::DynUpdateCallback::SetUpdateCallback( UpdateCallback::Pointer val )
{
	m_UpdateCallback = val;
}

Core::UpdateCallback::Pointer Core::DynUpdateCallback::GetUpdateCallback() const
{
	return m_UpdateCallback;
}

ModuleDescription* Core::DynUpdateCallback::GetModule() const
{
	return m_Module;
}

void Core::DynUpdateCallback::SetModule( ModuleDescription* val )
{
	m_Module = val;
}

bool Core::DynUpdateCallback::GetDetachProcessing()
{
	return m_UpdateCallback->GetDetachProcessing();
}
