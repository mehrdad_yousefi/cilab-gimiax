/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDynDataTransferCLP.h"
#include "coreDataEntityWriter.h"
#include "coreDataEntityReader.h"
#include "coreFile.h"
#include "itksys/SystemTools.hxx"
#include "blTextUtils.h"
#include "coreDirectory.h"
#include "coreDataEntityInfoHelper.h"

Core::DynDataTransferCLP::DynDataTransferCLP()
{
}

Core::DynDataTransferCLP::~DynDataTransferCLP()
{
}

void Core::DynDataTransferCLP::SaveInputToDisk( ModuleParameter* param )
{
	Core::IO::DataEntityReader::Pointer reader;
	reader = Core::IO::DataEntityReader::New( );
	reader->SetFileName( param->GetDefault() );
	Core::IO::BaseIO::TRANSFER_TYPE transferType = reader->GetTransferType( );

	// Check if file exists, using timeout or read data into memory from remote sources
	double startTime = itksys::SystemTools::GetTime( );
	double currentTime = itksys::SystemTools::GetTime( );
	bool fileExists = false;
	bool fileRead = false;
	bool timeout = false;
	bool aborted = false;

	// Check until m_IOTimeout is passed
	do {

		// Check if file exists
		switch ( transferType )
		{
		case Core::IO::BaseIO::LOCAL_TRANSFER_TYPE:
			fileExists = itksys::SystemTools::FileExists( param->GetDefault().c_str() );

			// Check if the file is 3D+T. the file is in the form image00.vtk, image01.vtk, ...
			if ( !fileExists )
			{
				std::vector< std::string > multipleFilenames;
				Core::IO::File::CheckDataFilenames( param->GetDefault(), multipleFilenames );
				fileExists = multipleFilenames.size( );
			}
			break;
		case Core::IO::BaseIO::REMOTE_TRANSFER_TYPE:
			try
			{
				reader->Update( );
				fileRead = reader->GetNumberOfOutputs() != 0;
			}
			catch(...)
			{
			}
		}

		// Delay 10 sec
		if ( !fileExists && !fileRead && m_IOTimeout != 0 )
		{
			itksys::SystemTools::Delay( 10000 );
		}

		// Get current time
		currentTime = itksys::SystemTools::GetTime( );
		timeout = currentTime - startTime > m_IOTimeout;

		// aborted?
		aborted = m_DynProcessor->GetUpdateCallback( )->GetAbortProcessing( );

	} while( !fileExists && !fileRead && !timeout && !aborted );

	// If file is local -> Return
	if ( !fileExists && !fileRead )
	{
		std::stringstream stream;
		stream << "Cannot read input filename: " << param->GetDefault();
		throw Core::Exceptions::Exception("DynDataTransferCLP::SaveInputToDisk", 
				stream.str().c_str() );
	}

	// Write data into disk
	if ( fileRead )
	{
		// Update module parameter using Use Case Diectory
		UpdateModuleParameter( param, 0 );

		// Save it to disk
		Core::IO::DataEntityWriter::Pointer writer;
		writer = Core::IO::DataEntityWriter::New( );
		writer->SetFileName( param->GetDefault( ) );
		writer->SetInputDataEntity( 0, reader->GetOutputDataEntity( 0 ) );
		writer->Update( );

		// Update GetDefault() value
		std::vector< std::string > writenFilenames = writer->GetWrittenFilenames();
		if ( writenFilenames.size() )
		{
			param->SetDefault( *writenFilenames.begin() );
		}
	}

}

void Core::DynDataTransferCLP::PreProcessData( ModuleParameter* param, int num )
{
	// If input is already set:
	if ( !param->GetDefault().empty() )
	{
		if ( param->GetChannel() == "input" )
		{
			SaveInputToDisk( param );
		}
		else
		{
			Core::IO::DataEntityReader::Pointer reader;
			reader = Core::IO::DataEntityReader::New( );
			reader->SetFileName( param->GetDefault() );
			Core::IO::BaseIO::TRANSFER_TYPE transferType = reader->GetTransferType( );
			if ( transferType == Core::IO::BaseIO::REMOTE_TRANSFER_TYPE )
			{
				// Use local file path for remote data.
				// When the execution is finished, it will always read the output and write
				// it using the desired output
				UpdateModuleParameter( param, num );
			}
		}
	}
	else 
	{
		// Create default filename
		UpdateModuleParameter( param, num );

		// Write input data to disk when DataEntity is in memory
		if ( param->GetChannel() == "input" && m_DynProcessor->GetInputPort( num )->GetDataEntity().IsNotNull( ) )
		{
			std::string path = itksys::SystemTools::GetFilenamePath( param->GetDefault() );
			std::string fileName = itksys::SystemTools::GetFilenameWithoutExtension( param->GetDefault() );
			std::string ext = itksys::SystemTools::GetFilenameExtension( param->GetDefault() );

			// Write selected time step
			Core::IO::DataEntityWriter::Pointer writer = Core::IO::DataEntityWriter::New();
			writer->SetFileName( param->GetDefault() );
			writer->GetInputPort( 0 )->SetSelectedTimeStep( m_DynProcessor->GetInputPort( num )->GetSelectedTimeStep() );
			writer->GetInputPort( 0 )->SetUpdateMode( m_DynProcessor->GetInputPort( num )->GetUpdateMode() );
			writer->SetInputDataEntity( 0, m_DynProcessor->GetInputPort( num )->GetDataEntity() );
			writer->Update( );

			if ( m_DynProcessor->GetInputPort( num )->GetUpdateMode() == 
				Core::BaseFilterInputPort::UPDATE_ACCESS_SINGLE_TIME_STEP )
			{
				// Update GetDefault() value
				std::vector< std::string > writenFilenames = writer->GetWrittenFilenames();
				if ( writenFilenames.size() )
				{
					param->SetDefault( *writenFilenames.begin() );
				}
			}
		}
	}

}

std::string Core::DynDataTransferCLP::GetExtension( ModuleParameter* param )
{
	std::string extension;
	if ( param->GetFileExtensions().size( ) == 0 )
	{
		Core::DataEntityType type = Core::UnknownTypeId;
		type = DataEntityInfoHelper::DataTypeToId( DataType( param->GetTag(), param->GetType() ) );
		extension = Core::IO::DataEntityWriter::GetDefaultFileTypeForWrite( type );
		if ( extension.empty() )
		{
			std::stringstream stream;
			stream << "Cannot find a proper extension to save data of parameter: " << param->GetName();
			throw Core::Exceptions::Exception("DynProcessor::GetInputFilename", 
				 stream.str().c_str() );
		}
	}
	else
	{
		extension = param->GetFileExtensions()[ 0 ];
	}

	return extension;
}

void Core::DynDataTransferCLP::UpdateModuleParameter( 
	ModuleParameter* param, 
	int num )
{
	// By default, the filename is based on the temporary directory and parameter name
	std::string fname = this->m_UseCaseDirectory + "/" + param->GetName();

	std::string extension = GetExtension( param );
	std::string fullFilename = fname + extension;
	param->SetDefault( fullFilename );
}

void Core::DynDataTransferCLP::UpdateProcessorOutput( ModuleParameter* param, int num )
{
	if ( m_DynProcessor->GetModuleExecution()->GetSaveScript() )
	{
		return;
	}

	// Check if data is already written into final destination location
	if ( !m_MapInitialValues[ param->GetName() ].empty() &&
		 m_MapInitialValues[ param->GetName() ] == param->GetDefault() )
	{
		return;
	}

	// Load into memory
	std::string filenames = SearchMultipleTimeSteps( param->GetDefault() );

	// Split default into single files using ";" as separator
	std::list<std::string> files;
	blTextUtils::ParseLine( filenames, ';', files );
	std::vector< std::string > filesVector;
	std::list<std::string>::iterator it;
	for ( it = files.begin() ; it != files.end() ; it++ )
	{
		filesVector.push_back( *it );
	}

	// Read data
	Core::IO::DataEntityReader::Pointer reader = Core::IO::DataEntityReader::New( );
	reader->SetFileNames( filesVector );
	reader->Update();
	if ( reader->GetNumberOfOutputs() == 0 )
	{
		throw Core::Exceptions::Exception("DynDataTransferCLP::UpdateProcessorOutput", 
			"Cannot load output data" );
	}

	Core::DataEntity::Pointer output = reader->GetOutputDataEntity( 0 );
	if ( output.IsNull() )
	{
		throw Core::Exceptions::Exception("DynDataTransferCLP::UpdateProcessorOutput", 
			"Cannot load output data" );
	}

	// Find input parent data
	Core::DataEntity::Pointer parentData = FindInputReferenceData( param );

	// Set name
	std::string name = m_DynProcessor->GetModule( )->GetTitle( ) + "_" + param->GetName();
	output->GetMetadata( )->SetName( name );

	// If this is not the first iteration and output has 1 time step, reuse previous output
	if ( m_DynProcessor->GetOutputDataEntity( num ).IsNotNull() && 
		 output->GetNumberOfTimeSteps( ) == 1 )
	{
		m_DynProcessor->UpdateOutput( 
			num, output->GetProcessingData( ), name, !GetGenerateNewOutputData( ), 1, parentData );
	}
	else
	{
		m_DynProcessor->UpdateOutput( num, output, parentData );
	}

	// If initial value was NOT empty -> 
	// Write temporary output using initial default value
	if ( !m_MapInitialValues[ param->GetName() ].empty() )
	{
		Core::IO::DataEntityWriter::Pointer writer;
		writer = Core::IO::DataEntityWriter::New( );
		writer->SetFileName( m_MapInitialValues[ param->GetName() ] );
		writer->SetInputDataEntity( 0, m_DynProcessor->GetOutputDataEntity( num ) );
		writer->Update( );
	}



}

void Core::DynDataTransferCLP::CleanTemporaryFiles()
{
	Core::DynDataTransferBase::CleanTemporaryFiles();

	if ( m_DynProcessor->GetModuleExecution()->GetSaveScript() )
	{
		return;
	}
}

std::string Core::DynDataTransferCLP::SearchMultipleTimeSteps( const std::string &filename )
{
	std::string addedFileNames;

	if ( filename.find( ";" ) != std::string::npos )
	{
		return filename;
	}

	// If file doesn't exist -> Check multiple time steps adding the 
	// number of the time step at the end
	if ( !itksys::SystemTools::FileExists( filename.c_str() ) )
	{
		std::string filePath = itksys::SystemTools::GetFilenamePath( filename );
		std::string fileName = itksys::SystemTools::GetFilenameWithoutExtension( filename );
		std::string ext = itksys::SystemTools::GetFilenameExtension( filename );

		// Read all files of the folder
		Core::IO::Directory::Pointer directory = Core::IO::Directory::New( );
		directory->SetDirNameFullPath( filePath );
		Core::IO::FileNameList list = directory->GetContents();
		Core::IO::FileNameList::iterator it;

		// Check for files with the same file name and extension
		bool found = false;
		for ( it = list.begin() ; it != list.end() ; it++ )
		{
			std::string curFileName = itksys::SystemTools::GetFilenameWithoutExtension( *it );
			curFileName = Core::IO::BaseIO::RemoveExtensionAndLastDigits( curFileName );
			std::string curExt = itksys::SystemTools::GetFilenameExtension( *it );
			if ( fileName == curFileName && ext == curExt )
			{
				addedFileNames.append( *it + ";" );
			}
		}
	}
	else
	{
		addedFileNames = filename;
	}

	return addedFileNames;
}
