/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreDynProcessor_H
#define _coreDynProcessor_H

// CoreLib
#include "gmProcessorsWin32Header.h"
#include "coreBaseProcessor.h"
#include "dynModuleExecution.h"
#include "coreDynUpdateCallback.h"

namespace Core{

/**
\brief Use ModuleDescription to call a processing filter.

The main responsibility of this class is to pass DataEntities to the filter. 
Depending on the subclass, the data can be passed as:
- Raw pointer to data type (like vtkPolyData*)
- File written into Disk
- DataEntity pointer. There's a specific DataEntityIO class responsible of 
extracting the itk::Image from DataEntity

\ingroup gmProcessors
\author Xavi Planes
\date 14 07 2010
*/
class GMPROCESSORS_EXPORT DynProcessor : public Core::BaseProcessor
{
public:
	//!
	coreDeclareSmartPointerClassMacro(Core::DynProcessor,Core::BaseProcessor)

	//!
	void SetModule( ModuleDescription* module );

	//!
	ModuleDescription *GetModule( );

	//!
	virtual void Update( );

	//!
	dynModuleExecution::Pointer GetModuleExecution() const;

	//!
	std::string GetTemporaryDirectory() const;
	void SetTemporaryDirectory(std::string val);

	//!
	void SetWorkingDirectory(std::string val);

	//!
	std::string GetActivePluginProviderName() const;
	void SetActivePluginProviderName(std::string val);

	//! Use case like "usecase_12345" using this as pointer
	std::string GetUseCase() const;
	void SetUseCase( const std::string &val );

	//! Attach a already running process, using this use case
	void AttachProcess(std::string useCase);
	bool GetAttachedProcess() const;

	//! Redefined
	virtual bool IsExecutedLocally( );

	//! Update temporary and working directories that are stored in the plugin provider
	void UpdateProcessorDirectories( std::string activePluginProvider );

	//! Set time out for input/output operations
	void SetIOTimeout( size_t timeout );

protected:
	//!
	DynProcessor(void);

	//!
	virtual ~DynProcessor(void);

	//!
	virtual bool InternalCopy( BaseFilter* filter );

	//! Generate use case using "this" pointer and current time
	void GenerateUseCase( );

private:
	//!
	DynProcessor( const Self& );

	//!
	void operator=(const Self&);



protected:
	//!
	dynModuleExecution::Pointer m_ModuleExecution;
	//! 
	std::string m_TemporaryDirectory;
	//!
	DynUpdateCallback::Pointer m_DynUpdateCallback;
	//!
	std::string m_ActivePluginProviderName;
	//!
	std::string m_UseCase;
	//! true if process has been attached from a already running one
	bool m_AttachedProcess;
	//! Time out for input/output operations
	size_t m_IOTimeout;
};

} // namespace Core{


#endif // _coreDynProcessor_H


