/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDynDataTransferBase.h"
#include "dynModuleHelper.h"
#include "itksys/SystemTools.hxx"
#include <fstream>
#include "coreWxMitkGraphicalInterface.h"
#include "coreSettings.h"

std::string Core::DynDataTransferBase::m_TemporarySubDir = "/usecase/";

Core::DynDataTransferBase::DynDataTransferBase()
{
}

Core::DynDataTransferBase::~DynDataTransferBase()
{
}


void Core::DynDataTransferBase::UpdateProcessorOutput( ModuleParameter* param, int num )
{

}

void Core::DynDataTransferBase::CleanTemporaryFiles()
{
	if ( !m_DynProcessor->GetModuleExecution()->GetSaveScript() )
	{
		itksys::SystemTools::RemoveADirectory( m_UseCaseDirectory.c_str() );

		if ( itksys::SystemTools::FileExists( m_UseCaseDirectory.c_str() ) )
		{
			std::string error;
			error = "Use case directory cannot be removed: ";
			error += m_UseCaseDirectory;
			Core::Runtime::Kernel::GetGraphicalInterface()->LogMessage(error);
		}
	}
}

Core::DynProcessor::Pointer Core::DynDataTransferBase::GetDynProcessor() const
{
	return m_DynProcessor;
}

void Core::DynDataTransferBase::SetDynProcessor( DynProcessor::Pointer val )
{
	m_DynProcessor = val;
}

void Core::DynDataTransferBase::PreProcessData()
{
	MakeUseCaseDirectory();

	m_MapInitialValues.clear();

	// PreProcess data
	for ( size_t i = 0 ; i < m_DynProcessor->GetNumberOfInputs() ; i++ )
	{
		ModuleParameter* param = dynModuleHelper::GetInput( m_DynProcessor->GetModule(), i );
		bool isOptional = m_DynProcessor->GetInputPort( i )->GetOptional();
		bool isActive = m_DynProcessor->GetInputPort( i )->GetActive();

		m_MapInitialValues[ param->GetName() ] = param->GetDefault();

		// Input value can be true or false indicating if are active or not
		// If is not active, it will be ignored
		if ( isOptional && !isActive )
		{
			// do nothing
		}
		else
		{
			PreProcessData( param, int( i ) );
		}
	}

	for ( size_t i = 0 ; i < m_DynProcessor->GetNumberOfOutputs() ; i++ )
	{
		ModuleParameter* param = dynModuleHelper::GetOutput( m_DynProcessor->GetModule( ), i );
		bool isOptional = m_DynProcessor->GetOutputPort( i )->GetOptional();
		
		m_MapInitialValues[ param->GetName() ] = param->GetDefault();

		// Outputs values are true or false indicating if are active or not
		// If value is true, it will be replaced by the output file name path
		// If value is fale, we will ignore it
		if ( isOptional && param->GetDefault() == "false" )
		{
			// Will be ignored
		}
		else
		{
			PreProcessData( param, int( i ) );
		}
	}

}

void Core::DynDataTransferBase::PreProcessData( ModuleParameter* param, int num )
{
}

void Core::DynDataTransferBase::PostProcessData()
{
	if ( m_DynProcessor->GetModuleExecution()->GetSaveScript( ) )
	{
		return;
	}

	for ( size_t i = 0 ; i < m_DynProcessor->GetNumberOfOutputs() ; i++ )
	{
		ModuleParameter* param = dynModuleHelper::GetOutput( m_DynProcessor->GetModule(), i );
		bool isOptional = m_DynProcessor->GetOutputPort( i )->GetOptional();

		// When execution finishes, and output is optional:
		// If is not active, value will be false
		// Is is active, value will be the output file path
		if ( isOptional && param->GetDefault() == "false" )
		{
			// Do nothing
		}
		else
		{
			UpdateProcessorOutput( param, int( i ) );
		}
	}

	// Clean input values used in previous execution because 
	// new values need to be created when calling it several time steps
	for ( int i = 0 ; i < m_DynProcessor->GetNumberOfInputs() ; i++ )
	{
		ModuleParameter* param;
		param = dynModuleHelper::GetInput( m_DynProcessor->GetModule(), i );
		param->SetDefault( m_MapInitialValues[ param->GetName( ) ] );
	}

	for ( size_t i = 0 ; i < m_DynProcessor->GetNumberOfOutputs() ; i++ )
	{
		ModuleParameter* param = dynModuleHelper::GetOutput( m_DynProcessor->GetModule(), i );
		param->SetDefault( m_MapInitialValues[ param->GetName( ) ] );
	}

}

std::string Core::DynDataTransferBase::GetTemporaryDirectory() const
{
	return m_TemporaryDirectory;
}

void Core::DynDataTransferBase::SetTemporaryDirectory( std::string val )
{
	m_TemporaryDirectory = val;
}

Core::DataEntity::Pointer Core::DynDataTransferBase::FindInputReferenceData( 
	ModuleParameter* param )
{
	// Find input parent data
	Core::DataEntity::Pointer parentData;
	size_t inputIndex = 0;
	if ( !param->GetReference().empty() )
	{
		for ( size_t i = 0 ; i < m_DynProcessor->GetNumberOfInputs() ; i++ )
		{
			ModuleParameter* inputParam;
			inputParam = dynModuleHelper::GetInput( m_DynProcessor->GetModule( ), i );
			if ( inputParam->GetName() == param->GetReference() )
			{
				inputIndex = i;
			}
		}
	}
	if ( m_DynProcessor->GetNumberOfInputs() > 0 )
	{
		parentData = m_DynProcessor->GetInputDataEntity( inputIndex );
	}

	return parentData;

}


Core::DynDataTransferBase::Pointer Core::DynDataTransferBase::NewDataTransfer( const std::string &name )
{
	blTagMap::Pointer properties = blTagMap::New( );
	properties->AddTag( "ModuleType", name );
	BaseFactory::Pointer factory;
	factory = FactoryManager::Find( DynDataTransferBase::GetNameClass(), properties );

	if ( factory.IsNull() )
	{
		throw Core::Exceptions::Exception( "DynDataTransferBase", "Data Transfer not registered for this name" );
	}

	Core::DynDataTransferBase::Pointer newInstance;
	newInstance = DynDataTransferBase::SafeDownCast( factory->CreateInstance() );
	return newInstance;
}

std::string Core::DynDataTransferBase::GetUseCaseDirectory() const
{
	return m_UseCaseDirectory;
}

void Core::DynDataTransferBase::MakeUseCaseDirectory( )
{
	BuildUseCaseDirectory( );

	if ( ExistsUseCaseDirectory( ) )
	{
		std::string error;
		error = "Use case directory already created: ";
		error += m_UseCaseDirectory;
		throw Core::Exceptions::Exception( "DynDataTransferBase::BuildUseCaseDirectory", error.c_str( ) );
	}

	itksys::SystemTools::MakeDirectory( m_UseCaseDirectory.c_str() );
	//! Create an empty file to compute processing time using SSH
	std::string emptyFileName = m_UseCaseDirectory + "/" + "emptyLocal.txt";
	std::ofstream emptyFile( emptyFileName.c_str() );
	emptyFile << "Empty";
}

void Core::DynDataTransferBase::BuildUseCaseDirectory( )
{
	std::string useCaseSubDir = m_TemporarySubDir + m_DynProcessor->GetUseCase( );
	m_UseCaseDirectory = m_TemporaryDirectory + useCaseSubDir;
}

//! check if use case directory exists
bool Core::DynDataTransferBase::ExistsUseCaseDirectory( )
{
	return itksys::SystemTools::FileExists( m_UseCaseDirectory.c_str() );
}

void Core::DynDataTransferBase::SetIOTimeout( size_t timeout )
{
	m_IOTimeout = timeout;
}

std::string Core::DynDataTransferBase::GetTemporarySubDir( )
{
	return m_TemporarySubDir;
}

//!
bool Core::DynDataTransferBase::GetGenerateNewOutputData( )
{
	// Get preferences reuse output configuration
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();

	std::string generateNewOutputCLPDataStr;
	settings->GetPluginProperty( "GIMIAS", "GenerateNewOutputCLPData", generateNewOutputCLPDataStr );
	bool generateNewOutputCLPData = generateNewOutputCLPDataStr == "true" ? true : false;

	return generateNewOutputCLPData;
}


