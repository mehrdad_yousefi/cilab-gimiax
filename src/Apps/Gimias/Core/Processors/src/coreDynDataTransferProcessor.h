/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreDynDataTransferProcessor_H
#define _coreDynDataTransferProcessor_H

// CoreLib
#include "gmProcessorsWin32Header.h"
#include "coreDynDataTransferBase.h"
#include "dynModuleExecution.h"
#include <set>

namespace Core{

/**
\brief Transfer data for Processor execution.

The inputs/outputs passed to the Processor through DataEntity.

\ingroup gmProcessors
\author Xavi Planes
\date 14 07 2010
*/
class GMPROCESSORS_EXPORT DynDataTransferProcessor : public Core::DynDataTransferBase
{
	//!
	typedef std::map<std::string, Core::DataEntity::Pointer> DataEntityMap;
public:
	//!
	coreDeclareSmartPointerClassMacro(Core::DynDataTransferProcessor, Core::DynDataTransferBase);
	coreDefineFactoryClass( DynDataTransferProcessor )
	coreDefineFactoryTagsBegin( )
	coreDefineFactoryAddTag( "ModuleType", std::string( "ProcessorModule" ) )
	coreDefineFactoryTagsEnd( )
	coreDefineFactoryClassEnd( )

	//!
	virtual void UpdateProcessorOutput( ModuleParameter* param, int num );

	//!
	virtual void CleanTemporaryFiles();

protected:
	//!
	DynDataTransferProcessor(void);

	//!
	virtual ~DynDataTransferProcessor(void);

	/** Save input data to disk using Default value
	If default value is empty, don't write it.
	If input DataEntity is empty, don't write it.
	*/
	virtual void PreProcessData( ModuleParameter* param, int num );

private:
	//!
	DynDataTransferProcessor( const Self& );

	//!
	void operator=(const Self&);

protected:
	//!
	DataEntityMap m_DataEntities;
};

} // namespace Core{


#endif // _coreDynDataTransferBaseCLP_H


