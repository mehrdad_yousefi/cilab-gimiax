/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreDynDataTransferBase_H
#define _coreDynDataTransferBase_H

// CoreLib
#include "gmProcessorsWin32Header.h"
#include "coreDynProcessor.h"

namespace Core{

/**
\brief Base class for transfer in/out data when executing a ModuleDescription instance.

This are the steps:
- PreProcessData()
- Execute ModuleDescription
- PostProcessData()
- CleanTemporaryFiles( )

All intermediate data is written into the use case directory.

If default values of in/out parameters are empty, then use input data from memory
and load output data into memory.

If default parameters are not empty, use the input file path and save outputs
into the desired path.

\ingroup gmProcessors
\author Xavi Planes
\date July 2010
*/
class GMPROCESSORS_EXPORT DynDataTransferBase : public Core::SmartPointerObject
{
	typedef std::map<std::string, DynDataTransferBase::Pointer> DataTransferMapType;

public:
	//!
	coreDeclareSmartPointerTypesMacro(Core::DynDataTransferBase,Core::SmartPointerObject)
	coreClassNameMacro(Core::DynDataTransferBase)
	coreTypeMacro(Core::DynDataTransferBase,Core::SmartPointerObject)

	//!
	static DynDataTransferBase::Pointer NewDataTransfer( const std::string &name );

	/** Preprocess data before execution of ModuleDescription.

	For example, when calling a CLP, and the the input data is in memory,
	it needs to be written into disk. Output default values needs to be set
	to the output file path in disk.
	
	The called functions are: MakeUseCaseDirectory( ) and PreProcessData( ) 
	for all in/out parameters that are not optional or are optional but not active.

	*/
	virtual void PreProcessData();

	/** Process data when the execution is finished.

	For example, when CLP finishes, output data needs to be loaded from disk.
	*/
	virtual void PostProcessData();

	/** Remove m_UseCaseDirectory
	
	This function is called when the execution is finished, to clean
	use case directory.
	*/
	virtual void CleanTemporaryFiles( );

	//!
	DynProcessor::Pointer GetDynProcessor() const;
	void SetDynProcessor(DynProcessor::Pointer val);

	//!
	std::string GetTemporaryDirectory() const;
	void SetTemporaryDirectory(std::string val);

	//!
	std::string GetUseCaseDirectory() const;

	//! Set time out for input/output operations
	void SetIOTimeout( size_t timeout );

	/** Create the directory m_UseCaseDirectory
	If directory already exists, throw an exception
	*/
	void MakeUseCaseDirectory( );

	//! Use m_TemporaryPath to create a sub directory string using Processor::GetUseCase( )
	void BuildUseCaseDirectory( );

	//! check if use case directory exists
	bool ExistsUseCaseDirectory( );

	//!
	static std::string GetTemporarySubDir( );

	//!
	bool GetGenerateNewOutputData( );

protected:
	//!
	DynDataTransferBase(void);

	//!
	virtual ~DynDataTransferBase(void);

	/** Process data before execution
	\param [in] param Parameter to modify
	\param [in] num Number of input/output relative to this processor
	*/
	virtual void PreProcessData( ModuleParameter* param, int num ) = 0;

	//! Update output data after execution
	virtual void UpdateProcessorOutput( ModuleParameter* param, int num ) = 0;

	//! Find input data entity with reference tag name
	Core::DataEntity::Pointer FindInputReferenceData( ModuleParameter* param );

private:
	//!
	DynDataTransferBase( const Self& );

	//!
	void operator=(const Self&);

protected:

	//!
	DynProcessor::Pointer m_DynProcessor;
	//! Working directory where use case directory will be created
	std::string m_TemporaryDirectory;
	/** Use case directory will be created inside the Working directory
	when needed. */
	std::string m_UseCaseDirectory;
	/** Store initial values of input / output parameters.
	The values may be changed during execution
	*/
	std::map<std::string, std::string> m_MapInitialValues;
	//! Time out for input/output operations
	size_t m_IOTimeout;
	//!
	static std::string m_TemporarySubDir;
};

} // namespace Core{


#endif // _coreDynDataTransferBase_H


