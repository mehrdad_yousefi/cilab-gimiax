/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _ModuleExecutionProcessor_H
#define _ModuleExecutionProcessor_H

#include "gmProcessorsWin32Header.h"
#include "dynModuleExecutionImpl.h"
#include "ModuleDescription.h"


/**
Executes a ModuleDescription using a Processor

\ingroup gmProcessors
\author Xavi Planes
\date Oct 2011
*/
class GMPROCESSORS_EXPORT ModuleExecutionProcessor : public dynModuleExecutionImpl
{
public:
	typedef ModuleExecutionProcessor Self;
	typedef blSmartPointer<Self> Pointer;
	blNewMacro(Self);
	defineModuleFactory( ModuleExecutionProcessor );
	virtual dynModuleExecutionImpl::Pointer CreateAnother(void) const
	{
		dynModuleExecutionImpl::Pointer smartPtr;
		smartPtr = New().GetPointer();
		return smartPtr;
	}

	//!
	virtual void Update( );

	/** Convert ModuleDescription to blTagMap
	Valid types are:
	- int
	- unsigned int
	- float
	- double
	- std::string
	- bool
	- std::vector<int>
	- std::vector<float>
	- std::vector<std::string>
	- std::vector<double>
	*/
	static blTagMap::Pointer ModuleToTagMap( ModuleDescription& module );

	//!
	static Core::DataEntity::Pointer GetDataEntity( const std::string &value, int &timeStep );

protected:
	//! Set a parameter value to m_Filter
	virtual void SetParameterValue( ModuleParameter *param );

	//! Get a parameter value from m_Filter
	virtual void GetParameterValue( ModuleParameter *param );

	//!
	ModuleExecutionProcessor( );

	//!
	virtual ~ModuleExecutionProcessor( );

private:
};

#endif // _ModuleExecutionProcessor_H

