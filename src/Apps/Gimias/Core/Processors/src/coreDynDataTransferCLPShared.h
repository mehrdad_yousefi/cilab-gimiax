/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreDynDataTransferCLPCLPShared_H
#define _coreDynDataTransferCLPCLPShared_H

// CoreLib
#include "gmProcessorsWin32Header.h"
#include "coreDynDataTransferCLP.h"
#include "dynModuleExecution.h"

namespace Core{

/**
\brief Use ModuleDescription to call a filter of a Command Line Plugin
as a shared library.

To pass images to the plugin, it uses as filename "DataEntity:%p:%d" with a 
pointer to the input DataEntity and selected time step. 
The itk::DataEntityIO will be used to extract the itk::Image needed by the plugin.

To retrieve images from the plugin, it uses m_OutputDataEntities that
is created before calling the filter. After the filter finishes, the 
processing data is set as output of this processor.

For the rest of data types, it uses the parent class DynDataTransferCLP.

\ingroup gmProcessors
\author Xavi Planes
\date 14 07 2010
*/
class GMPROCESSORS_EXPORT DynDataTransferCLPShared : public Core::DynDataTransferCLP
{
	//!
	typedef std::map<int, Core::DataEntity::Pointer> NumToDataEntityMap;

public:
	//!
	coreDeclareSmartPointerClassMacro(Core::DynDataTransferCLPShared, Core::DynDataTransferCLP)
	coreDefineFactoryClass( DynDataTransferCLPShared )
	coreDefineFactoryTagsBegin( )
	coreDefineFactoryAddTag( "ModuleType", std::string( "SharedObjectModule" ) )
	coreDefineFactoryTagsEnd( )
	coreDefineFactoryClassEnd( )

	//!
	void UpdateModuleParameter( ModuleParameter* param, int num );

	//!
	virtual void UpdateProcessorOutput( ModuleParameter* param, int num );

protected:
	//!
	DynDataTransferCLPShared(void);

	//!
	virtual ~DynDataTransferCLPShared(void);

	//!
	void CleanTemporaryFiles();

	//! Save input data to disk using Default value
	virtual void PreProcessData( ModuleParameter* param, int num );

private:
	//!
	DynDataTransferCLPShared( const Self& );

	//!
	void operator=(const Self&);

protected:

	//!
	NumToDataEntityMap m_OutputDataEntities;
};

} // namespace Core{


#endif // _coreDynProcessorCLPShared_H


