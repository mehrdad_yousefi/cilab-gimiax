/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDynDataTransferProcessor.h"
#include "coreDataEntityWriter.h"
#include "coreDataEntityReader.h"
#include "coreFile.h"
#include "itksys/SystemTools.hxx"
#include "blTextUtils.h"
#include "coreDirectory.h"
#include "coreDataEntityInfoHelper.h"

Core::DynDataTransferProcessor::DynDataTransferProcessor()
{
}

Core::DynDataTransferProcessor::~DynDataTransferProcessor()
{
}

void Core::DynDataTransferProcessor::PreProcessData( ModuleParameter* param, int num )
{
	if ( param->GetChannel() == "input" )
	{
		// If input is already set:
		if ( !param->GetDefault().empty() )
		{

			Core::IO::DataEntityReader::Pointer reader;
			reader = Core::IO::DataEntityReader::New( );
			reader->SetFileName( param->GetDefault() );
			bool fileRead = false;
			bool timeout = false;
			bool aborted = false;
			double startTime = itksys::SystemTools::GetTime( );
			double currentTime = itksys::SystemTools::GetTime( );

			// Read data into memory
			do {

				try
				{
					reader->Update( );
					fileRead = reader->GetNumberOfOutputs() != 0;
				}
				catch(...)
				{
				}
			
				// Delay 10 sec
				if ( !fileRead && m_IOTimeout != 0 )
				{
					itksys::SystemTools::Delay( 10000 );
				}

				// Get current time
				currentTime = itksys::SystemTools::GetTime( );
				timeout = currentTime - startTime > m_IOTimeout;

				// aborted?
				aborted = m_DynProcessor->GetUpdateCallback( )->GetAbortProcessing( );

			} while( !fileRead && !timeout && !aborted );

			// If file is local -> Return
			if ( !fileRead )
			{
				std::stringstream stream;
				stream << "Cannot read input filename: " << param->GetDefault();
				throw Core::Exceptions::Exception("DynDataTransferProcessor::PreProcessData", 
						stream.str().c_str() );
			}


			if ( fileRead )
			{
				// Store a pointer
				m_DataEntities[ param->GetName() ] = reader->GetOutputDataEntity( 0 );

				std::ostringstream fnameString;
				fnameString << "DataEntity:"
					<< m_DataEntities[ param->GetName() ].GetPointer() 
					<< ":" << m_DynProcessor->GetInputPort( num )->GetSelectedTimeStep();
				param->SetDefault( fnameString.str() );
			}
		}
		else 
		{
			// If data is in memory -> Use DataEntity
			if ( param->GetChannel() == "input" )
			{
				std::ostringstream fnameString;
				fnameString << "DataEntity:"
					<< m_DynProcessor->GetInputPort( num )->GetDataEntity().GetPointer() 
					<< ":" << m_DynProcessor->GetInputPort( num )->GetSelectedTimeStep();
				param->SetDefault( fnameString.str() );
			}
		}

	}
	else if ( param->GetChannel() == "output" )
	{
		// Create new DataEntity
		Core::DataEntity::Pointer dataEntity;
		dataEntity = Core::DataEntity::New( UnknownTypeId );
		dataEntity->GetMetadata()->SetName( param->GetName() );

		Core::DataEntityType type;
		type = DataEntityInfoHelper::DataTypeToId( DataType( param->GetTag(), param->GetType() ) );
		dataEntity->SetType( type );

		m_DataEntities[ param->GetName() ] = dataEntity;

		// Set Pointer to DataEntity
		std::ostringstream fnameString;
		fnameString << "DataEntity:" << dataEntity.GetPointer()
			<< ":" << m_DynProcessor->GetOutputPort( num )->GetSelectedTimeStep();
		param->SetDefault( fnameString.str() );
	}

}

void Core::DynDataTransferProcessor::UpdateProcessorOutput( ModuleParameter* param, int num )
{
	if ( m_DynProcessor->GetModuleExecution()->GetSaveScript() )
	{
		return;
	}

	if ( m_DataEntities[ param->GetName() ].IsNull( ) ||
		 m_DataEntities[ param->GetName() ]->GetNumberOfTimeSteps( ) == 0 )
	{
		throw Core::Exceptions::Exception( "UpdateProcessorOutput", "Output Data is emtpy" );  
	}

	// If initial value was NOT empty -> save data from memory to disk or remote storage
	if ( !m_MapInitialValues[ param->GetName() ].empty() )
	{
		Core::IO::DataEntityWriter::Pointer writer;
		writer = Core::IO::DataEntityWriter::New( );
		writer->SetFileName( m_MapInitialValues[ param->GetName() ] );
		writer->SetInputDataEntity( 0, m_DataEntities[ param->GetName() ] );
		writer->Update( );

		// Update GetDefault() value with initial value because when it's 3D+T
		// the written filenames will take only the first time step
		param->SetDefault( m_MapInitialValues[ param->GetName() ] );
		//std::vector< std::string > writenFilenames = writer->GetWrittenFilenames();
		//if ( writenFilenames.size() )
		//{
		//	param->SetDefault( *writenFilenames.begin() );
		//}
	}
	else // Pass DataEntity to the output of DynProcessor
	{
		Core::DataEntity::Pointer parentData = FindInputReferenceData( param );

		std::string name = m_DynProcessor->GetModule( )->GetTitle( ) + "_" + param->GetName();

		// For current iteration, only is valid the current time step, the rest is reset to 0
		int timeStep = m_DynProcessor->GetOutputPort( num )->GetSelectedTimeStep();

		m_DynProcessor->UpdateOutput( num, 
			m_DataEntities[ param->GetName() ]->GetProcessingData( TimeStepIndex( timeStep ) ), 
			name, !GetGenerateNewOutputData( ), 1, parentData );
	}
}

void Core::DynDataTransferProcessor::CleanTemporaryFiles()
{
	Core::DynDataTransferBase::CleanTemporaryFiles();

	m_DataEntities.clear();
}
