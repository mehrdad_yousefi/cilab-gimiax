/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDynDataTransferDLL.h"
#include "coreDataEntityWriter.h"
#include "dynModuleHelper.h"

Core::DynDataTransferDLL::DynDataTransferDLL()
{
}

Core::DynDataTransferDLL::~DynDataTransferDLL()
{
}


void Core::DynDataTransferDLL::PreProcessData( 
	ModuleParameter* param, int num )
{
	if ( param->GetChannel() != "input" )
	{
		return;
	}

	Core::DataEntity::Pointer dataEntity;
	dataEntity = m_DynProcessor->GetInputPort( num )->GetDataEntityHolder( )->GetSubject();
	if ( dataEntity.IsNull( ) )
	{
		throw Core::Exceptions::Exception("DynDataTransferDLL::PreProcessData", 
			"You must select an input data from the Processing Browser and set it as input" 
			);
	}

	BaseFactory::Pointer factory;
	factory = DataEntityImplFactoryHelper::FindFactoryBySimilarType( param->GetDataType() );
	if ( factory.IsNull() )
	{
		throw Core::Exceptions::Exception("DynDataTransferDLL::PreProcessData", 
			"Is not possible to convert selected input data to the input data type" 
			);
	}

	// Get time step
	int timeStep;
	if ( factory->GetProperties()->FindTagByName( "single" ) )
	{
		timeStep = 0;
	}
	else if ( factory->GetProperties()->FindTagByName( "multiple" ) )
	{
		timeStep = -1;
	}

	// Update input data type
	boost::any anyData = m_DynProcessor->GetInputPort( num )->UpdateInput( param->GetDataType(), timeStep );
	if ( anyData.empty() )
	{
		throw Core::Exceptions::Exception("DynDataTransferDLL::PreProcessData", 
			"Is not possible to convert selected input data to the input data type" 
			);
	}

	// Get pointer
	void* ptr = NULL;
	ptr = dataEntity->GetTimeStep( timeStep )->GetVoidPtr( );
	param->SetDefault( dynModuleHelper::PointerToString( ptr ) );

}

void Core::DynDataTransferDLL::UpdateProcessorOutput( ModuleParameter* param, int num )
{
	void *ptr = dynModuleHelper::StringToPointer( param->GetDefault( ) );

	Core::DataEntity::Pointer parentData = FindInputReferenceData( param );

	BaseFactory::Pointer factory;
	factory = DataEntityImplFactoryHelper::FindFactoryBySimilarType( param->GetDataType() );
	if ( factory.IsNotNull() )
	{
		DataEntityImpl::Pointer data;
		data = DataEntityImpl::SafeDownCast( factory->CreateInstance() );
		data->SetVoidPtr( ptr );
		std::string name = m_DynProcessor->GetModule( )->GetTitle( ) + "_" + param->GetName();
		m_DynProcessor->UpdateOutput( 
			num, data->GetDataPtr(), name, !GetGenerateNewOutputData( ), 1, parentData );
	}
	else
	{
		std::ostringstream strError;
		strError <<"Failed get output of the filter. You should specify a valid output <dataType> tag";
		throw Core::Exceptions::Exception("DynDataTransferDLL::Update", 
			strError.str().c_str() );
	}

}
