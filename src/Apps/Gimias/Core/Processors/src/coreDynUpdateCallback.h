/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreDynUpdateCallback_H
#define _coreDynUpdateCallback_H

// CoreLib
#include "gmProcessorsWin32Header.h"
#include "dynUpdateCallback.h"
#include "coreUpdateCallback.h"

class ModuleDescription;

namespace Core{

/**
\brief Update callback for DynProcessor

Modified function will be called when the ProcessInformation of 
ModuleDescription is changed. The function will update Core::UpdateCallback
and call Modified( )

When GetAbortProcessing() is true, it sets the ProcessInformation::Abort 
to true

\ingroup gmProcessors
\author Xavi Planes
\date Jan 2011
*/
class GMPROCESSORS_EXPORT DynUpdateCallback 
	: public dynUpdateCallback

{
public:
	//!
	coreDeclareSmartPointerTypesMacro(Core::DynUpdateCallback,dynUpdateCallback)
	coreFactorylessNewMacro(Core::DynUpdateCallback)
	coreClassNameMacro(Core::DynUpdateCallback)

	//! 
	void Modified( );

	//! 
	bool GetDetachProcessing( );

	//!
	UpdateCallback::Pointer GetUpdateCallback() const;
	void SetUpdateCallback(UpdateCallback::Pointer val);

	//!
	ModuleDescription* GetModule() const;
	void SetModule(ModuleDescription* val);

protected:
	//!
	DynUpdateCallback(void);

	//!
	virtual ~DynUpdateCallback(void);

private:
	//!
	DynUpdateCallback( const Self& );

	//!
	void operator=(const Self&);



protected:
	//!
	UpdateCallback::Pointer m_UpdateCallback;
	//!
	ModuleDescription* m_Module;
};

} // namespace Core{


#endif // _coreDynUpdateCallback_H


