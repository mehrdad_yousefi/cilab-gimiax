/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreModuleExecutionProcessor.h"
#include "dynModuleExecution.h"

#include "coreFactoryManager.h"
#include "coreBaseProcessor.h"

#include "dynModuleHelper.h"

ModuleExecutionProcessor::ModuleExecutionProcessor( )
{
}

ModuleExecutionProcessor::~ModuleExecutionProcessor()
{
}

void ModuleExecutionProcessor::Update()
{
	Core::BaseFactory::Pointer factory;
	factory = Core::FactoryManager::FindByInstanceClassName( GetModule( )->GetHierarchy() );
	if ( factory.IsNull() )
	{
		std::stringstream stream;
		stream << "Cannot find Processor: " << GetModule( )->GetHierarchy();
		throw Core::Exceptions::Exception("ModuleExecutionProcessor::Update", 
			stream.str().c_str() );
	}


	Core::BaseProcessor::Pointer processor;
	processor = Core::BaseProcessor::SafeDownCast( factory->CreateInstance() );

	// Retrieve inputs
	for ( size_t i = 0 ; i < processor->GetNumberOfInputs() ; i++ )
	{
		ModuleParameter* param = dynModuleHelper::GetInput( GetModule(), i );

		// Retrieve input data entity
		int timeStep = 0;
		Core::DataEntity::Pointer dataEntity = GetDataEntity(param->GetDefault(), timeStep);
		if (dataEntity.IsNull())
		{
			// not a valid pointer
			continue;
		}

		processor->SetInputDataEntity( i, dataEntity );
	}


	// Set parameters
	blTagMap::Pointer parameters = ModuleToTagMap( *GetModule() );
	processor->SetParameters( parameters );

	// Update
	processor->Update();

	// Update outputs
	for ( size_t i = 0 ; i < processor->GetNumberOfOutputs() ; i++ )
	{
		ModuleParameter* param = dynModuleHelper::GetOutput( GetModule(), i );

		if ( param->GetDefault() == "false" )
		{
			continue;
		}

		// Retrieve output data entity
		int timeStep = 0;
		Core::DataEntity::Pointer dataEntity = GetDataEntity(param->GetDefault(), timeStep);
		if ( dataEntity.IsNull() )
		{
			// not a valid pointer
			continue;
		}

		dataEntity->Copy( processor->GetOutputDataEntity( i ) );
	}

}

void ModuleExecutionProcessor::SetParameterValue( ModuleParameter *param )
{

}

void ModuleExecutionProcessor::GetParameterValue( ModuleParameter *param )
{

}

blTagMap::Pointer ModuleExecutionProcessor::ModuleToTagMap( ModuleDescription& module )
{
	blTagMap::Pointer tagmap = blTagMap::New( );
	std::vector<ModuleParameterGroup> parameterGroups = module.GetParameterGroups();
	std::vector<ModuleParameterGroup>::iterator itGroup;
	itGroup = module.GetParameterGroups().begin();
	while (itGroup != module.GetParameterGroups().end())
	{
		std::vector<ModuleParameter>::iterator itParam;
		itParam = itGroup->GetParameters().begin();
		while (itParam != itGroup->GetParameters().end())
		{
			blTag::Pointer tag = blTag::New( itParam->GetName(), 0 );
			if ( itParam->GetCPPType() == "int" )
			{
				tag->SetValueAsString( blTag::GetTypeName( typeid( int ) ), itParam->GetDefault( ) );
				tagmap->AddTag( tag );
			}
			else if ( itParam->GetCPPType() == "unsigned int" )
			{
				tag->SetValueAsString( blTag::GetTypeName( typeid( unsigned int ) ), itParam->GetDefault( ) );
				tagmap->AddTag( tag );
			}
			else if ( itParam->GetCPPType() == "float" )
			{
				tag->SetValueAsString( blTag::GetTypeName( typeid( float ) ), itParam->GetDefault( ) );
				tagmap->AddTag( tag );
			}
			else if ( itParam->GetCPPType() == "double" )
			{
				tag->SetValueAsString( blTag::GetTypeName( typeid( double ) ), itParam->GetDefault( ) );
				tagmap->AddTag( tag );
			}
			else if ( itParam->GetCPPType() == "std::string" )
			{
				tag->SetValueAsString( blTag::GetTypeName( typeid( std::string ) ), itParam->GetDefault( ) );
				tagmap->AddTag( tag );
			}
			else if ( itParam->GetCPPType() == "bool" )
			{
				// SetValueAsString doesn't work with "true" / "false"
				tag->SetValue( itParam->GetDefault( ) == "true" ? true : false );
				tagmap->AddTag( tag );
			}
			else if ( 
				itParam->GetCPPType() == "std::vector<int>" ||
				itParam->GetCPPType() == "std::vector<float>" ||
				itParam->GetCPPType() == "std::vector<std::string>" ||
				itParam->GetCPPType() == "std::vector<double>" )
			{
				tag->SetValueAsString( blTag::GetTypeName( typeid( std::string ) ), itParam->GetDefault( ) );
				tagmap->AddTag( tag );
			}
			
			++itParam;
		}

		++itGroup;
	}

	return tagmap;
}

Core::DataEntity::Pointer ModuleExecutionProcessor::GetDataEntity( const std::string &value, int &timeStep )
{
	// Retrieve input data entity
	Core::DataEntity* ptr = NULL;
	timeStep = 0;
	sscanf(value.c_str(), "DataEntity:%p:%d", &ptr, &timeStep);
	return ptr;
}

