/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreSessionWriter.h"
#include "coreDataEntityWriter.h"
#include "coreDataEntityHelper.h"

#include "blXMLTagMapWriter.h"

#include "itksys/SystemTools.hxx"

#include <fstream>

using namespace Core::IO;

SessionWriter::SessionWriter()
{
	m_TreeManager = NULL;
	m_ValidExtensionsList.push_back( ".gses" );
	m_ValidTypesList.push_back( SessionTypeId );
}

SessionWriter::~SessionWriter()
{
}

void SessionWriter::WriteData()
{	
	if( GetFileName( ).empty( ) )
		return;

	Core::DataEntityList::Pointer dataList = Core::Runtime::Kernel::GetDataContainer()->GetDataEntityList();
	if(dataList.IsNull())
		return;

	//get directory path
	std::string dirPath = itksys::SystemTools::GetFilenamePath( GetFileName( ) );

	// Create data tag
	blTagMap::Pointer session = blTagMap::New();
	blTagMap::Pointer dataTag = blTagMap::New( );
	session->AddTag( "Data", dataTag );

	// Save selected data
	if ( GetInputDataEntity( 0 ).IsNotNull() )
	{
		// Save children of selected DataEntity
		Core::DataEntity::ChildrenListType children = GetInputDataEntity( 0 )->GetChildrenList();
		for(size_t i=0; i<children.size(); i++)
		{
			blTagMap::Pointer child = SaveDataEntity(children.at(i), dirPath);
			dataTag->AddTag(children.at(i)->GetMetadata()->GetName( ),child);
		}

		// Save views of selected node if any
		SaveViews( GetInputDataEntity( 0 ), dirPath );
	}
	else
	{
		// Save all DataEntity
		Core::DataEntityList::iterator it; 
		for(it = dataList->Begin(); it != dataList->End(); it++)
		{
			Core::DataEntity::Pointer dataEntity = dataList->Get(it);
			if ( dataEntity->GetFather().IsNotNull( ) && 
				 dataEntity->GetFather()->GetId( ) == 1 )
			{
				blTagMap::Pointer child = SaveDataEntity(dataEntity, dirPath);
				dataTag->AddTag(dataEntity->GetMetadata()->GetName( ),child);
			}
		}

		// Save views of root if any
		SaveViews( dataList->Get( dataList->Begin() ), dirPath );
	}

	// Write XML file
	blXMLTagMapWriter::Pointer writer = blXMLTagMapWriter::New( );
	writer->SetFilename( GetFileName( ).c_str( ) );
	writer->SetInput( session );
	writer->Update( );
}

blTagMap::Pointer SessionWriter::SaveDataEntity(
	Core::DataEntity::Pointer dataEntity,
	const std::string &dirPath)
{
	blTagMap::Pointer lightData = blTagMap::New();

	// Get name
	std::string name = dataEntity->GetMetadata()->GetName();
	lightData->AddTag("Name", dataEntity->GetMetadata()->GetName());


	// Number of time steps
	size_t numberOfTimeSteps = dataEntity->GetNumberOfTimeSteps();
	// If DataEntity not loaded, use metadata tag
	if ( dataEntity->GetNumberOfTimeSteps( ) == 0 )
	{
		// set number of TimeSteps
		if ( dataEntity->GetMetadata()->GetTag( "NumberOfTimeSteps" ).IsNotNull( ) )
		{
			numberOfTimeSteps = dataEntity->GetMetadata()->GetTag( "NumberOfTimeSteps" )->GetValueCasted<int>( );
		}
	}
	lightData->AddTag("NumberOfTimeSteps", int( numberOfTimeSteps ) );	


	// File extension: use metadata or default writer
	std::string fileExtension;
	if ( dataEntity->GetMetadata()->GetTag( "Extension" ).IsNotNull( ) )
	{
		fileExtension = dataEntity->GetMetadata()->GetTag( "Extension" )->GetValueAsString( );
	}
	
	// Extension is empty when reading a directory
	if ( fileExtension.empty( ) )
	{
		fileExtension = Core::IO::DataEntityWriter::GetDefaultFileTypeForWrite(dataEntity->GetType());
	}


	// For session type or multiple time steps, use subfolder
	std::string subFolder;
	if ( numberOfTimeSteps > 1 || dataEntity->GetType( ) == Core::SessionTypeId )
	{
		subFolder = "/" + name;
	}

	// If data is loaded -> Try to save the file
	if ( dataEntity->GetNumberOfTimeSteps( ) > 0 || dataEntity->GetType( ) == Core::SessionTypeId )
	{
		// Get count for this name
		std::ostringstream id_str;
		if ( m_NameCount.find( dataEntity->GetMetadata()->GetName() ) == m_NameCount.end() )
		{
			m_NameCount[ dataEntity->GetMetadata()->GetName() ] = 0;
		}
		else
		{
			m_NameCount[ dataEntity->GetMetadata()->GetName() ]++;
			id_str << "_" << m_NameCount[ dataEntity->GetMetadata()->GetName() ]; 
		}

		// Use default file name + id + and extension
		std::string relativePath = subFolder + "/" + name + id_str.str() + fileExtension;
		std::string filepath = dirPath + relativePath;
	
		lightData->AddTag( "Filepath", filepath);
		lightData->AddTag( "RelativePath", relativePath );

		// Create folder
		std::string path = itksys::SystemTools::GetFilenamePath( filepath );
		itksys::SystemTools::MakeDirectory( path.c_str( ) );

		//save data entity
		try
		{
			Core::DataEntityHelper::SaveDataEntity(filepath, dataEntity, m_TreeManager);
		}
		catch(...){}
	}
	else
	{
		std::string relativePath = subFolder + "/" + name + fileExtension;
		std::string filepath = dirPath + relativePath;

		// Set file path
		lightData->AddTag( "Filepath", filepath);
		lightData->AddTag( "RelativePath", relativePath );
	}


	// Save children of DataEntity if not a session
	if ( dataEntity->GetType( ) != Core::SessionTypeId )
	{
		blTagMap::Pointer childrenTags = blTagMap::New();
		for(size_t i=0; i<dataEntity->GetChildrenList().size(); i++)
		{
			blTagMap::Pointer child = SaveDataEntity(dataEntity->GetChildrenList().at(i), dirPath);
			childrenTags->AddTag(dataEntity->GetChildrenList().at(i)->GetMetadata()->GetName( ),child);
		}
		if ( !dataEntity->GetChildrenList().empty() )
		{
			lightData->AddTag( "Children", childrenTags );
		}
	}

	return lightData;
}

void Core::IO::SessionWriter::SetTreeManager( Core::RenderingTreeManager* val )
{
	m_TreeManager = val;
}

void Core::IO::SessionWriter::SaveViews( 
	Core::DataEntity::Pointer dataEntity, 
	const std::string &dirPath )
{
	if ( dataEntity.IsNull( ) )
		return;

	// Get session node
	Core::DataEntity::Pointer session = dataEntity;
	while( session->GetType( ) != Core::SessionTypeId && session->GetFather( ) )
	{
		session = session->GetFather( );
	}
	if ( session.IsNull( ) )
		return;

	// Get tagmap
	blTagMap::Pointer sessionTagMap;
	session->GetProcessingData( sessionTagMap );
	if ( sessionTagMap.IsNull( ) )
		return ;

	// Get views tags
	blTagMap::Pointer views = sessionTagMap->GetTagValue<blTagMap::Pointer>( "Views" );
	if ( views.IsNull( ) )
		return;

	// Create Views subdir
	std::string viewsDir = dirPath + "/Views";
	itksys::SystemTools::RemoveADirectory( viewsDir.c_str( ) );
	itksys::SystemTools::MakeDirectory( viewsDir.c_str( ) );

	// Iterate over all views
	blTagMap::Iterator itView = views->GetIteratorBegin( );
	while ( itView != views->GetIteratorEnd( ) )
	{
		blTagMap::Pointer view = itView->second->GetValueCasted<blTagMap::Pointer>( );

		// View name
		std::string viewName;
		if ( view->GetTag( "Name" ).IsNotNull( ) )
		{
			viewName = view->GetTag( "Name" )->GetValueAsString( );
		}

		// Create subfolder
		std::string viewDir = viewsDir + "/" + viewName;
		itksys::SystemTools::MakeDirectory( viewDir.c_str( ) );

		// Write XML file
		std::string viewFile = viewDir + "/" + viewName + ".gvw";
		blXMLTagMapWriter::Pointer writer = blXMLTagMapWriter::New( );
		writer->SetFilename( viewFile.c_str( ) );
		writer->SetInput( view );
		writer->Update( );

		// Create preview
		Core::DataEntityPreview::Pointer preview;
		BaseFactory::Pointer factory = FactoryManager::Find( DataEntityPreview::GetNameClass() );
		if ( factory.IsNotNull( ) )
			preview = dynamic_cast<DataEntityPreview*> ( factory->CreateInstance( ).GetPointer( ) );
		if ( preview.IsNull( ) )
			return;

		// Save preview image
		preview->SetImage( view->GetTag( "Image" )->GetValue( ) );
		preview->SetName( viewName );
		preview->SetFolder( viewDir );
		preview->Save( );

		itView++;
	}
}


