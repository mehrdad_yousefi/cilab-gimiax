/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreBaseWindowFactory.h"


void Core::BaseWindowFactory::SetParent( wxWindow* parent )
{
	m_Parent = parent;
}

wxWindow* Core::BaseWindowFactory::GetParent() const
{
	return m_Parent;
}

void Core::BaseWindowFactory::SetWindowId( int id )
{
	m_WindowId = id;
}

int Core::BaseWindowFactory::GetWindowId() const
{
	return m_WindowId;
}

std::string Core::BaseWindowFactory::GetWindowName() const
{
	return m_WindowName;
}

void Core::BaseWindowFactory::SetWindowName( std::string val )
{
	m_WindowName = val;
}

Core::BaseWindowFactory::BaseWindowFactory()
{
	m_Parent = NULL;
	m_WindowId = -1;
}

std::string Core::BaseWindowFactory::GetBitmapFilename() const
{
	return m_BitmapFilename;
}

void Core::BaseWindowFactory::SetBitmapFilename( std::string val )
{
	m_BitmapFilename = val;
}

ModuleDescription &Core::BaseWindowFactory::GetModule( const std::string &key )
{
	return m_Modules[ key ];
}

bool Core::BaseWindowFactory::FindModule( const std::string &key )
{
	ModuleMapType::iterator it = m_Modules.find( key );
	return ( it != m_Modules.end( ) );
}

void Core::BaseWindowFactory::RemoveModule( const std::string &key )
{
	ModuleMapType::iterator it = m_Modules.find( key );
	if ( it != m_Modules.end( ) )
	{
		m_Modules.erase( it );
	}
}

void Core::BaseWindowFactory::SetModule( const std::string &key, ModuleDescription val )
{
	m_Modules[ key ] = val;
	if ( m_DefaultModuleKey.empty() )
	{
		m_DefaultModuleKey = key;
	}
}

ModuleDescription & Core::BaseWindowFactory::GetDefaultModule()
{
	if ( !m_Modules.empty() )
	{
		return m_Modules[ m_DefaultModuleKey ];
	}

	// Return an empty module
	return GetModule( "" );
}

Core::BaseWindowFactory::ModuleMapType& Core::BaseWindowFactory::GetModules() 
{
	return m_Modules;
}

std::string Core::BaseWindowFactory::GetDefaultModuleKey() const
{
	return m_DefaultModuleKey;
}

void Core::BaseWindowFactory::SetDefaultModuleKey( std::string val )
{
	m_DefaultModuleKey = val;
}

Core::BaseWindowIOFactory::BaseWindowIOFactory()
{
	m_DataEntitytype = Core::UnknownTypeId;
	m_Extension = "";
}

bool Core::BaseWindowIOFactory::CheckType( 
	Core::DataEntityType type, 
	const std::string &ext,
	bool isReading,
	Core::DataEntity::Pointer dataEntity )
{
	return isReading && type == m_DataEntitytype && ext == m_Extension;
}

