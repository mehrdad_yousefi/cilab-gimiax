/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "corePluginProviderManager.h"

#include "coreFrontEndPluginProvider.h"
#include "coreCommandLinePluginProvider.h"
#include "coreKernel.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreSettings.h"
#include "coreBaseMainWindow.h"
#include "coreWorkflowManager.h"
#include "coreCompatibility.h"
#include "coreReportExceptionMacros.h"

using namespace Core::Runtime;
using namespace Core::FrontEndPlugin;

PluginProviderManager::PluginProviderManager(void)
{
	m_StateHolder = Core::DataHolder<STATE>::New( );
	m_StateHolder->SetSubject( STATE_IDLE );

	m_PluginProvidersUpdater = PluginProvidersUpdater::New( );
}

PluginProviderManager::~PluginProviderManager(void)
{
	RemoveAll( );
}

Core::PluginProvidersUpdater::Pointer Core::Runtime::PluginProviderManager::GetProvidersUpdater( )
{
	return m_PluginProvidersUpdater;
}

std::string Core::Runtime::PluginProviderManager::GetCurrentPluginName() const
{
	return m_PluginProvidersUpdater->GetCurrentPluginName( );
}

void Core::Runtime::PluginProviderManager::SetCurrentPluginName( const std::string &name ) 
{
	m_PluginProvidersUpdater->SetCurrentPluginName( name );
}

Core::Runtime::PluginProviderManager::PluginProvidersType 
Core::Runtime::PluginProviderManager::GetPluginProviders() const
{
	return m_PluginProviders;
}

PluginProvider::Pointer 
Core::Runtime::PluginProviderManager::GetPluginProvider( std::string name ) const
{
	PluginProvidersType::const_iterator it;
	for ( it = m_PluginProviders.begin() ; it != m_PluginProviders.end() ; it++ )
	{
		if ( (*it)->GetName() == name )
		{
			return (*it);
		}
	}	

	return NULL;
}

void Core::Runtime::PluginProviderManager::RegisterDefaultProviders()
{
	FactoryManager::Register( PluginProvider::GetNameClass(), CommandLinePluginProvider::Factory::New( ) );
	FactoryManager::Register( PluginProvider::GetNameClass(), FrontEndPluginProvider::Factory::New( ) );
}

void Core::Runtime::PluginProviderManager::Add( PluginProvider* provider )
{
	if ( GetPluginProvider( provider->GetName() ).IsNotNull() )
	{
		return;
	}

	m_PluginProviders.push_back( provider );
	m_PluginProvidersUpdater->AddProvider( provider );
	Modified( );
}

void Core::Runtime::PluginProviderManager::LoadConfiguration( bool restoreConfiguration )
{
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();

	blTagMap::Pointer gimiasTAG = settings->GetPluginProperties( "GIMIAS" );
	blTagMap::Pointer pluginProvidersTag;
	pluginProvidersTag = gimiasTAG->GetTagValue<blTagMap::Pointer>( "PluginProviders" );
	if ( pluginProvidersTag.IsNull() )
	{
		AddDefaultProviders( );
	}
	else
	{

		// Order by priority because GIMIAS FrontEnd plugins can register
		// new PluginProviders and must be added first
		std::list<std::string> providerNamesList = OrderProvidersByPriorityTag();

		// Add provider
		std::list<std::string>::iterator itNames;
		for( itNames = providerNamesList.begin() ; itNames != providerNamesList.end() ; itNames++ )
		{
			blTagMap::Pointer properties = pluginProvidersTag->GetTagValue<blTagMap::Pointer>( *itNames );

			// If provider not found -> Create a new one
			PluginProvider::Pointer provider = GetPluginProvider( *itNames );
			if ( provider.IsNull( ) )
			{
				std::string classname;
				if ( properties->GetTag( "NameOfClass" ).IsNull() )
				{
					continue;
				}
				properties->GetTag( "NameOfClass" )->GetValue( classname );

				BaseFactory::Pointer baseFactory;
				baseFactory = FactoryManager::FindByInstanceClassName( classname );
				if ( baseFactory.IsNull() )
				{
					continue;
				}

				provider = PluginProvider::SafeDownCast( baseFactory->CreateInstance() );
				if ( provider.IsNull() )
				{
					continue;
				}
				
				// Add properties
				provider->GetProperties()->AddTags( properties );

				// Add it after adding the tags
				Add( provider );

			}
			else
			{
				// Restore plugin configuration from disk when changing from workflow to plugin perspective
				// Be careful: SSHPlugin will call LoadConfiguration() and the FrontEndPluginProvider
				// configuration should not be restored
				if ( restoreConfiguration )
				{
					provider->GetProperties()->AddTags( properties );
				}
			}

		}

		// Remove old plugin providers
		PluginProvidersType::const_iterator it;
		PluginProvidersType toRemoveList;
		for ( it = m_PluginProviders.begin() ; it != m_PluginProviders.end() ; it++ )
		{
			BaseFactory::Pointer baseFactory;
			baseFactory = FactoryManager::FindByInstanceClassName( (*it)->GetNameOfClass() );
			if ( baseFactory.IsNull() )
			{
				toRemoveList.push_back( *it );
			}
		}	

		while ( !toRemoveList.empty( ) )
		{
			Remove( *toRemoveList.begin( ) );
			toRemoveList.pop_front( );
		}

	}

}

void Core::Runtime::PluginProviderManager::SaveConfiguration()
{
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();

	blTagMap::Pointer gimiasTAG = settings->GetPluginProperties( "GIMIAS" );
	blTagMap::Pointer pluginProvidersTag = blTagMap::New( );
	gimiasTAG->AddTag( "PluginProviders", pluginProvidersTag );

	PluginProvidersType::const_iterator it;
	for ( it = m_PluginProviders.begin() ; it != m_PluginProviders.end() ; it++ )
	{
		pluginProvidersTag->AddTag( (*it)->GetName(), (*it)->GetProperties() );
	}

	settings->SetPluginProperties( "GIMIAS", gimiasTAG );
}

void Core::Runtime::PluginProviderManager::Remove( PluginProvider* provider )
{
	// Unload all plugins
	provider->UnselectAllPlugins( );
	LoadPlugins( provider->GetName( ), false );

	// Remove references
	m_PluginProviders.remove( provider );
	m_PluginProvidersUpdater->RemoveProvider( provider );
	Modified( );
}

void Core::Runtime::PluginProviderManager::RemoveAll( )
{
	// Remove from last because GIMIAS plugins should be the last one
	while ( m_PluginProviders.size() ) 
	{
		Remove( m_PluginProviders.back() );
	}
}

void Core::Runtime::PluginProviderManager::AddDefaultProviders()
{
	// Front end plugins
	BaseFactory::Pointer baseFactory;
	PluginProvider::Pointer provider;
	baseFactory = FactoryManager::FindByInstanceClassName( FrontEndPluginProvider::GetNameClass() );
	provider = PluginProvider::SafeDownCast( baseFactory->CreateInstance() );
	Add( provider );

	// Command line plugins
	baseFactory = FactoryManager::FindByInstanceClassName( CommandLinePluginProvider::GetNameClass() );
	provider = PluginProvider::SafeDownCast( baseFactory->CreateInstance() );
	Add( provider );

	// XML descriptions of external applications
	baseFactory = FactoryManager::FindByInstanceClassName( CommandLinePluginProvider::GetNameClass() );
	provider = PluginProvider::SafeDownCast( baseFactory->CreateInstance() );
	blTagMap::Pointer configuration;
	configuration = provider->GetProperties()->GetTagValue<blTagMap::Pointer>( "Configuration" );
	configuration->GetTag( "Scan folder" )->SetValue( std::string( "$(AppData)/Descriptions" ) );
	Add( provider );
}

std::list<std::string> Core::Runtime::PluginProviderManager::OrderProvidersByPriorityTag()
{
	std::list<std::string> providerNamesList;

	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();

	blTagMap::Pointer gimiasTAG = settings->GetPluginProperties( "GIMIAS" );
	blTagMap::Pointer pluginProvidersTag;
	pluginProvidersTag = gimiasTAG->GetTagValue<blTagMap::Pointer>( "PluginProviders" );
	if ( pluginProvidersTag.IsNull() )
	{
		return providerNamesList;
	}

	blTagMap::Iterator it;
	for ( it = pluginProvidersTag->GetIteratorBegin() ; 
		it != pluginProvidersTag->GetIteratorEnd() ; 
		it++)
	{
		blTagMap::Pointer properties;
		it->second->GetValue( properties );
		if ( properties.IsNull() )
		{
			continue;
		}

		if ( properties->GetTag( "Priority" ).IsNull() )
		{
			continue;
		}
		int priority = properties->GetTagValue<int>( "Priority" );

		// Find the provider with equal or more priority
		std::list<std::string>::iterator itNames = providerNamesList.begin();
		bool found = false;
		while (  itNames != providerNamesList.end() && !found )
		{
			blTagMap::Pointer prop = pluginProvidersTag->GetTagValue<blTagMap::Pointer>( *itNames );
			int prio = prop->GetTagValue<int>( "Priority" );
			if ( priority <= prio )
			{
				found = true;
			}
			else
			{
				itNames++;
			}
		}

		providerNamesList.insert( itNames, 1, it->first );
	}

	return providerNamesList;
}

Core::DataHolder<Core::Runtime::PluginProviderManager::STATE>::Pointer 
	Core::Runtime::PluginProviderManager::GetStateHolder( )
{
	return m_StateHolder;
}

std::list<std::string> Core::Runtime::PluginProviderManager::GetPluginsToUnload(
	const std::string providerName /*= ""*/ )
{
	std::list<std::string> pluginsToUnload;

	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();

	// Get loaded plugins
	std::list<std::string> loadedPlugins;
	PluginProvidersType::const_iterator itProvider;
	for ( itProvider = m_PluginProviders.begin() ; itProvider != m_PluginProviders.end() ; itProvider++ )
	{
		// Get lodaded plugins
		std::list<std::string> plugins = (*itProvider)->GetLoadedPlugins( );
		if ( providerName.empty( ) || providerName == (*itProvider)->GetName( ) )
		{
			loadedPlugins.insert( loadedPlugins.end( ), plugins.begin( ), plugins.end( ) );
		}
	}


	// Get all selected plugins
	std::list<std::string> selectedPlugins;
	if ( settings->GetPerspective() == PERSPECTIVE_PLUGIN )
	{
		PluginProvidersType::const_iterator it;
		for ( it = m_PluginProviders.begin() ; it != m_PluginProviders.end() ; it++ )
		{
			if ( (*it)->GetProperties( ).IsNull( ) )
			{
				continue;
			}

			std::list<std::string> pluginsList;
			blTagMap::Pointer plugins = (*it)->GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );
			pluginsList = (*it)->GetSelectedPlugins( plugins );
			selectedPlugins.insert( selectedPlugins.end( ), pluginsList.begin( ), pluginsList.end( ) );
		}
	}
	else if ( settings->GetPerspective() == PERSPECTIVE_WORKFLOW )
	{
		// Get workflow plugins list
		Core::Workflow::Pointer workflow;
		workflow = Core::Runtime::Kernel::GetWorkflowManager()->GetActiveWorkflow( );
		if ( workflow.IsNull() )
		{
			return pluginsToUnload;
		}

		selectedPlugins = workflow->GetPluginNamesList();
		Workflow::PluginNamesListType extra = workflow->GetExtraPluginNamesList();
		selectedPlugins.insert(  selectedPlugins.end(), extra.begin(), extra.end( ) );
	}

	// Check plugins to unload
	std::list<std::string>::iterator itPlugin;
	for ( itPlugin = loadedPlugins.begin( ) ; itPlugin != loadedPlugins.end( ) ; itPlugin++ )
	{
		std::list<std::string>::iterator itFound;
		itFound = std::find( selectedPlugins.begin( ), selectedPlugins.end( ), *itPlugin );
		if ( itFound == selectedPlugins.end( ) )
		{
			pluginsToUnload.push_back( *itPlugin );
		}
	}

	return pluginsToUnload;
}

void Core::Runtime::PluginProviderManager::ScanPlugins( 
	const std::string name /*= ""*/, bool multithreading /*= false*/ )
{
	if ( name.empty( ) )
		GetProvidersUpdater()->SelectAllProviders( );
	else
		GetProvidersUpdater()->SelectProvider( name );
	
	GetProvidersUpdater()->EnableScanPlugins( true );
	GetProvidersUpdater()->EnableLoadPlugins( false );
	GetProvidersUpdater()->EnableMultithreading( multithreading );
	GetProvidersUpdater()->Update( );
}

void Core::Runtime::PluginProviderManager::LoadPlugins( 
	const std::string name /*= ""*/, bool multithreading /*= false*/ )
{
	UnloadProviders( name );

	if ( name.empty( ) )
		GetProvidersUpdater()->SelectAllProviders( );
	else
		GetProvidersUpdater()->SelectProvider( name );
	
	GetProvidersUpdater()->EnableScanPlugins( false );
	GetProvidersUpdater()->EnableLoadPlugins( true );
	GetProvidersUpdater()->EnableMultithreading( multithreading );
	GetProvidersUpdater()->Update( );
}

void Core::Runtime::PluginProviderManager::UpdateProviders( 
	const std::string name /*= ""*/, bool multithreading /*= false*/ )
{
	UnloadProviders( name );

	if ( name.empty( ) )
		GetProvidersUpdater()->SelectAllProviders( );
	else
		GetProvidersUpdater()->SelectProvider( name );
	
	GetProvidersUpdater()->EnableScanPlugins( true );
	GetProvidersUpdater()->EnableLoadPlugins( true );
	GetProvidersUpdater()->EnableMultithreading( multithreading );
	GetProvidersUpdater()->Update( );
}

void Core::Runtime::PluginProviderManager::UnloadProviders(
	const std::string providerName /*= ""*/ )
{
	// Check if the plugin to be unloaded has registered a plugin provider
	std::list<std::string> pluginsToUnload = GetPluginsToUnload( providerName );
	if ( pluginsToUnload.empty( ) )
		return;

	// Get list of registered plugin proivders factories
	Core::FactoryManager::FactoryListType providerFactories;
	providerFactories = FactoryManager::FindAll( Core::Runtime::PluginProvider::GetNameClass() );

	// Check if a plugin provider factory has been registered by a plugin to unload
	std::list<PluginProvider*> providersToRemove;
	Core::FactoryManager::FactoryListType::iterator itFactory;
	for ( itFactory = providerFactories.begin( ) ; itFactory != providerFactories.end( ) ; itFactory++ )
	{
		// Get plugin name
		blTag::Pointer pluginNameTag = (*itFactory)->GetProperties( )->GetTag( "PluginName" );
		if ( pluginNameTag.IsNull( ) )
		{
			continue;
		}

		// Check list of plugins to unload
		std::list<std::string>::iterator itFound;
		itFound = std::find( pluginsToUnload.begin( ), pluginsToUnload.end( ), pluginNameTag->GetValueAsString( ) );
		if ( itFound == pluginsToUnload.end( ) )
		{
			continue;
		}

		// Check if a provider has been created
		PluginProvidersType::const_iterator itProvider;
		for ( itProvider = m_PluginProviders.begin() ; itProvider != m_PluginProviders.end() ; itProvider++ )
		{
			if ( (*itFactory)->GetInstanceClassName( ) == (*itProvider)->GetNameOfClass( ) )
			{
				providersToRemove.push_back( itProvider->GetPointer( ) );
			}
		}
	}

	// Remove plugin providers
	std::list<PluginProvider*>::iterator itProvider;
	for ( itProvider = providersToRemove.begin( ) ; itProvider != providersToRemove.end( ) ; itProvider++ )
	{
		Remove( *itProvider );
	}
}
