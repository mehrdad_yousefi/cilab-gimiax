/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreSettings_H
#define coreSettings_H

#include "gmKernelWin32Header.h"
#include "coreObject.h"
#include "coreConfigVars.h"
#include "coreSettingsIO.h"
#include <string>
#include <boost/thread/mutex.hpp>

namespace Core
{

	class Compatibility;

namespace Runtime
{

	enum PERSPECTIVE_TYPE
	{
		PERSPECTIVE_PLUGIN,
		PERSPECTIVE_WORKFLOW
	};

/** 
\brief Store and retrieve configuration data in a platform-independent style. 

The settings also stores the user profile, so reading it determines all 
the actor types he pertains. Profile only will work when running in 
graphical mode

\sa Core::Profile
\ingroup gmKernel
\author Juan Antonio Moya
\date 20 May 2006
*/

class GMKERNEL_EXPORT Settings : public Core::SmartPointerObject
{
public:
	coreDeclareSmartPointerTypesMacro(Core::Runtime::Settings,Core::SmartPointerObject)
	coreClassNameMacro(Core::Runtime::Settings)

	/** Factory method for creating a new instance.
	 \param argv0 - The first argument of the GIMIAS main function 
	 (the filename of the GIMIAS executable). 
	 */
	static Pointer New(const std::string& argv0);
	
	std::string GetApplicationTitleAndVersion() const;
	std::string GetApplicationTitle() const;
	std::string GetYear() const;
	std::string GetCorpLogoFileName() const;
	std::string GetCompanyName() const;
	std::string GetProjectHomeFolderName() const;
	std::string GetApplicationPath() const;
	//! Returns a \a subpath from the application path.
	std::string GetApplicationPathSubPath(const std::string& subPath);
	std::string GetScriptVersion() const;
	std::string GetKernelVersion() const;
	std::string GetKernelMainVersion() const;
	std::string GetStudioName() const;
	std::string GetProjectName() const;
	std::string GetPluginsPath() const;
	std::string GetResourcePath() const;
	std::string GetModulesPath() const;
	std::string GetDataSourcePath() const;
	//! ApplicationData + ProjectHomeFolderName + KernelVersion
	std::string GetProjectHomePath() const;
	std::string GetConfigFileFullPath() const;
	//! See m_ConfigDTDSourceFileFullPath
	std::string GetConfigDTDSourceFileFullPath() const;
	//! See m_ConfigDTDTargetFileFullPath
	std::string GetConfigDTDTargetFileFullPath() const;
	std::string GetLogFileFullPath() const;
	std::string GetDefaultConfigFileName() const;

	//! Replace $(GimiasPath) by GetApplicationPath()
	void ReplaceGimiasPath( std::string &path ) const; 

	bool LoadSettings( const char* configFileFullPath = NULL );
	void SaveSettings(void);

	bool IsFirstTimeStart(void) const;

	std::string GetPluginResourceForFile(const 
		std::string& pluginName, 
		const std::string& filename) const;
	std::string GetCoreResourceForFile(const std::string& filename) const;

	/** Calls TestCreateConfigFile on this->m_SettingsIO (MN: I have no 
	idea why this is needed, and would
	 love to work on refactoring this asap :-( )
	 */
	void TestCreateConfigFile();

	//!
	std::vector<std::string> GetMRUList( );

	//!
	void SetMRUList( std::vector<std::string> list );

	//! Get GetLastOpenedPath( ) if its not empty, else GetDataSourcePath( )
	std::string GetCurrentDataPath( );

	//! Get Called AE information
	std::string GetPacsCalledAE( );

	//! Get Calling AE information
	std::string GetPacsCallingAE( );

	//! Set Called AE information strCalledAE must be of the type AETitle:IPAdress:TCPPort
	void SetPacsCalledAE( std::string strCalledAE);

	//! Set Calling AE information strCallingAE must be of the type AETitle:IPAdress:TCPPort
	void SetPacsCallingAE( std::string strCallingAE);

	//! pathtype: 0 -> data file, 1 -> dicom dir file, 2-> dicom file 
	std::string GetLastOpenedPath( int pathType = 0);

	//! 
	std::string GetLastSavePath( );

	//! 
	void SetLastSavePath( const std::string path );

	//! pathtype: 0 -> data file, 1 -> dicom dir file, 2-> dicom file
	void SetLastOpenedPath( std::string strPath, int pathType = 0 );

	void SetMainAppName(const std::string& value);
	void SetFirstTimeStart(bool value);

	//!
	PERSPECTIVE_TYPE GetPerspective( );
	void SetPerspective( PERSPECTIVE_TYPE type );

	//!
	std::string GetActiveWorkflow( );
	void SetActiveWorkflow( const std::string &name );

	//!
	void SetImportConfiguration( bool val );
	bool GetImportConfiguration( );

	//!
	bool GetShowRegistrationForm( );
	void SetShowRegistrationForm( bool val );

	//!
	bool GetUserRegistered( );
	void SetUserRegistered( bool val );

	//! Generic property for a plugin
	void SetPluginProperty( 
		const char *pluginName,
		const std::string &name,
		const std::string &value );

	//! Generic property for a plugin
	bool GetPluginProperty( 
		const char *pluginName,
		const std::string &name,
		std::string &value );

	/** Generic properties for a plugin
	\note Be carefull to add a property from the plugin because
	the memory will be destroyed when the plugin is destroyed
	\note If there are no properties, create an empty one
	*/
	blTagMap::Pointer GetPluginProperties( const char *pluginName );

	/**
	Removes all tags and add the properties tags as string
	\note All tags need to be convertible to string
	*/
	void SetPluginProperties( const char *pluginName, blTagMap::Pointer properties );

	/**
	Add the tags to the current tags
	\note All tags need to be convertible to string
	*/
	void AddPluginProperties( const char *pluginName, blTagMap::Pointer properties );

protected:
	/**
	\brief Initializer for the class Settings.
	 When creating also tests creating configuration file 
	*/
	Settings(const std::string& argv0);

	/**
	\brief Destructor for the class Settings.
	\todo Mutex XMLWriter or de-block it on destruction time. By now: do 
	not Save settings on application destruction, or XML system will not 
	be able to parse the file because it could be blocked by the XMLWriter.
	*/
	virtual ~Settings(void);

private:
	// Member constants
	//! 1.5
	std::string m_KernelMainVersion;
	//! 1.5.a1
	std::string m_KernelVersion;
	std::string m_ScriptVersion;
	std::string m_StudioName;
	std::string m_ProjectName;
	std::string m_ApplicationTitle;
	std::string m_ApplicationTitleAndVersion;
	std::string m_Year;
	std::string m_CorpLogoFileName;
	std::string m_CompanyName;
	std::string m_DefaultLogFileName;
	std::string m_DefaultConfigFileName;
	std::string m_DefaultConfigDTDFileName;
	std::string m_DefaultCorpLogoFileName;
	std::string m_DefaultPluginsDirName;
	std::string m_DefaultResourceDirName;
	std::string m_DefaultModulesDirName;
	std::string m_DefaultDataDirName;

	// Computable values
	std::string m_ProjectHomeFolderName;
	std::string m_ApplicationPath;
	std::string m_PluginsPath;
	std::string m_ResourcePath;
	std::string m_ModulesPath;
	std::string m_DataSourcePath;
	std::string m_ProjectHomePath;
	std::string m_ConfigFileFullPath;
	//! This is the dtd file that is part of the GIMIAS resources.
	std::string m_ConfigDTDSourceFileFullPath;
	/** This is the dtd file that is copied to the users folder (and is 
	used to verify the user profile).
	*/
	std::string m_ConfigDTDTargetFileFullPath;
	std::string m_LogFileFullPath;

	//! Config vars got from the IO
	Core::IO::ConfigVars m_configVars;
	//! Read and save ConfigVars
	Core::IO::SettingsIO::Pointer m_SettingsIO;

	friend class Core::Compatibility;

	coreDeclareNoCopyConstructors(Settings);

	//! Protect access to plugin properties when using multiple readers
	boost::mutex m_Mutex;
};
}
}


#endif
