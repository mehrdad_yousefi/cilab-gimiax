/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

//#include "corePythonScriptingInterface.h"
#include "coreKernel.h"
#include "coreException.h"
#include "coreReportExceptionMacros.h"
#include "coreDataEntityBuildersRegistration.h"
#include "coreFilteringFactoriesRegistration.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreDataEntityIORegistration.h"
#include "corePluginProviderManager.h"

#include "coreLogger.h"
#include "coreSettings.h"
#include "coreDataContainer.h"
#include "coreWorkflowManager.h"
#include "coreProcessorManager.h"
#include "coreSessionReader.h"
#include "coreSessionWriter.h"

using namespace Core::Runtime;

class Kernel::Impl
{
public:
	//! Application Settings
	ApplicationSettingsPointer m_ApplicationSettings;
	//! Log manager 
	RuntimeLoggerPointer m_LogManager;
	//! Application runtime
	ApplicationRuntimePointer m_ApplicationRuntime;
	//! Graphical Interface layer
	RuntimeGraphicalInterfacePointer m_GraphicalInterface;
	//! Data members of the model
	DataContainerPointer m_DataContainer;
	//! Scripting Interface layer 
	void* m_ScriptingInterface;
	//!
	WorkflowManagerPointer m_WorkflowManager;
	//!
	ProcessorManagerPointer m_ProcessorManager;

	// When application is closed with exit(), the statis member variables all destroyed
	// and they should be destroyed in order
	~Impl( )
	{
		Destroy();
	}

	void Destroy( )
	{
		// First delete the data container.
		// When a plug in allocates a blSignal object and builds a
		// data entity, the memory of this processing data is in the
		// memory space of the plug in, and when the plug ins are unloaded
		// the memory will be invalid
		m_Impl.m_DataContainer = NULL;
		m_Impl.m_ScriptingInterface = NULL;
		
		// Stop all running tasks before unloading all plugins
		// Some plugins could have a processor running without multi threading, 
		// so first we need to stop all running threads
		if ( m_Impl.m_ProcessorManager.IsNotNull( ) )
		{
			m_Impl.m_ProcessorManager->StopAllExecutionQueues( );
		}

		// Unload all the plug ins before destroying graphical interface
		if ( m_Impl.m_GraphicalInterface.IsNotNull( ) &&
			 m_Impl.m_GraphicalInterface->GetPluginProviderManager( ) )
		{
			m_Impl.m_GraphicalInterface->GetPluginProviderManager( )->RemoveAll();
		}
		
		m_Impl.m_GraphicalInterface = NULL;
		m_Impl.m_LogManager = NULL;

		// Destroy WorkflowManager before applications settings
		m_Impl.m_WorkflowManager = NULL;

		// Destroy processor manager before applications settings 
		m_Impl.m_ProcessorManager = NULL;

		m_Impl.m_ApplicationSettings = NULL;
	}
};

bool Kernel::m_isInitialized = false;
Kernel::Impl Kernel::m_Impl;



Core::Runtime::Kernel::~Kernel()
{
	Core::Runtime::Kernel::Terminate();
}

/** Initializer for the class Domain */
void Kernel::Initialize(
	Core::Runtime::Environment* environment ) 
{
	try
	{
		if(isInitialized())
			Terminate();
	}
	coreCatchExceptionsCastToNewTypeAndThrowMacro(Core::Exceptions::OnAppInitException, Kernel::Initialize)

	try
	{
		// Initialize the m_Impl.m_ApplicationRuntime environment
		m_Impl.m_ApplicationRuntime = environment;
		if(m_Impl.m_ApplicationRuntime.IsNull())
		{
			Core::Exceptions::RuntimeEnvironmentNotSetException e("Kernel::Initialize");
			throw e;
		}

		std::string applicationPath;
		if ( !environment->GetArgv().empty() &&  !environment->GetArgv()[0].empty() ) 
		{
			applicationPath = environment->GetArgv()[0];
		}

		// Create settings before changing state of kernel
		m_Impl.m_ApplicationSettings = Core::Runtime::Settings::New(applicationPath);

		// Check kernel state from previous execution
		environment->ReadPreviousKernelState();
		if ( environment->GetPreviousKernelState() == KERNEL_STATE_LOADING_CONFIGURATION )
		{
			// There was an error when starting the application last time
			m_Impl.m_ApplicationSettings->SetFirstTimeStart( false );
			m_Impl.m_ApplicationSettings->SetShowRegistrationForm( false );
		}
		else
		{
			// Load settings from disk
			InitializeSettings( applicationPath );
		}

		Core::Runtime::Kernel::GetApplicationRuntime()->SetKernelState(
			Core::Runtime::KERNEL_STATE_INITIALIZING_KERNEL );

		// Initialize the Logger object for interfacing with system console and all other reporting tools
		m_Impl.m_LogManager = Logger::New();
		if ( m_Impl.m_LogManager.IsNull() )
		{
			Core::Exceptions::LoggerNotSetException e("Kernel::Initialize");
			throw e;
		}
		m_Impl.m_LogManager->SetFileName(m_Impl.m_ApplicationSettings->GetLogFileFullPath());

		// Register kernel factories
		InitializeData();

		// After settings
		m_Impl.m_WorkflowManager = WorkflowManager::New();
		m_Impl.m_ProcessorManager = ProcessorManager::New();

		// Initialize the Scripting Interface
		//m_Impl.m_ScriptingInterface = new Core::Runtime::PythonScriptingInterface();

	}
	coreCatchExceptionsAddTraceAndThrowMacro(Kernel::Initialize);

	m_isInitialized = true;
}

/** Destructor for the class Domain */
void Kernel::Terminate(void)
{
	try
	{
		// When calling UpdateOutput( ), DataEntity is created and a preview
		// A plugin can register a new class for preview
		FactoryManager::FactoryListType factories;
		factories = Core::FactoryManager::FindAll( Core::DataEntityPreview::GetNameClass() );
		FactoryManager::FactoryListType::iterator itFactory;
		for ( itFactory = factories.begin( ) ; itFactory != factories.end( ) ; itFactory++ )
		{
			Core::FactoryManager::UnRegister( *itFactory );
		}
		factories.clear( );

		// Destroy plugin providers before unregistering DataEntityImpl
		// to allow calling UpdateOutput( )
		m_Impl.Destroy();

		// Unregister data entity builders
		Core::DataEntityBuildersRegistration::UnRegisterDataEntityBuilders();

		// Unregister all readers/writers
		Core::IO::DataEntityReader::UnRegisterAllFormats();
		Core::IO::DataEntityWriter::UnRegisterAllFormats();

		// Port invocation is used when executing a processor to unload plugins
		FilteringFactoriesRegistration::UnRegister();

		m_isInitialized = false;
	}
	coreCatchExceptionsAddTraceAndThrowMacro(Kernel::Terminate)
}


/**
Returns true if a previous call to Kernel::Initialize worked perfectly. 
Therefore, true would mean that the kernel was initialized properly 
*/
bool Kernel::isInitialized(void)
{
	return m_isInitialized;
}


/**
 */
Core::Runtime::Kernel::ApplicationSettingsPointer Core::Runtime::Kernel::GetApplicationSettings()
{
	return m_Impl.m_ApplicationSettings;
}

/** 
 */
Core::Runtime::Kernel::RuntimeLoggerPointer Core::Runtime::Kernel::GetLogManager()
{
	return m_Impl.m_LogManager;
}

/**
 */
Core::Runtime::Kernel::ApplicationRuntimePointer Core::Runtime::Kernel::GetApplicationRuntime()
{
	return m_Impl.m_ApplicationRuntime;
}

/**
 */
Core::Runtime::Kernel::RuntimeGraphicalInterfacePointer Core::Runtime::Kernel::GetGraphicalInterface()
{
	return m_Impl.m_GraphicalInterface;
}

/**
 */
Core::Runtime::Kernel::DataContainerPointer Core::Runtime::Kernel::GetDataContainer()
{
	return m_Impl.m_DataContainer;
}

/**
 */
void Core::Runtime::Kernel::InitializeData()
{
	// register required data entity builders (before DataEntityList, to create root node)
	DataEntityBuildersRegistration::RegisterDataEntityBuilders();

	// Initialize the data structures of the model
	m_Impl.m_DataContainer = Core::DataContainer::New();
	if(m_Impl.m_DataContainer.IsNull())
	{
		Core::Exceptions::DataContainerNotSetException e("Kernel::Initialize");
		throw e;
	}


	// register required data entity IO
	IO::DataEntityIORegistration::RegisterDefaultFormats( 
		m_Impl.m_ApplicationSettings->GetResourcePath( ) );

	// Register session reader
	IO::DataEntityReader::RegisterFormatReader( IO::BaseDataEntityReader::Pointer(IO::SessionReader::New()) );
	IO::DataEntityWriter::RegisterFormatWriter( IO::BaseDataEntityWriter::Pointer(IO::SessionWriter::New()) );

	// Register DataHandling factories
	FilteringFactoriesRegistration::Register();
}

void Core::Runtime::Kernel::InitializeSettings( 
						std::string appName )
{
	Core::Runtime::Kernel::GetApplicationRuntime()->SetKernelState(
		Core::Runtime::KERNEL_STATE_LOADING_CONFIGURATION );
	m_Impl.m_ApplicationSettings->TestCreateConfigFile();

	if ( m_Impl.m_ApplicationSettings.IsNull() )
	{
		Core::Exceptions::SettingsNotSetException e("Kernel::Initialize");
		throw e;
	}
	
	if ( !m_Impl.m_ApplicationSettings->LoadSettings( ) )
	{
		Core::Exceptions::SettingsNotSetException e("Kernel::Initialize");
		throw e;
	}
}

void Core::Runtime::Kernel::InitializeGraphicalInterface( 
	Core::Widgets::BaseMainWindow* mainWindow,
	Core::Runtime::AppRunMode mode )
{
	// Initialize from settings
	m_Impl.m_WorkflowManager->Initialize( );
	m_Impl.m_ProcessorManager->Initialize( );

	switch(mode)
	{
	case Core::Runtime::Graphical:
		{
			// Create the Graphical Interface
			m_Impl.m_GraphicalInterface = wxMitkGraphicalInterface::New();

			if(m_Impl.m_GraphicalInterface.IsNull())
			{
				Core::Exceptions::GraphicalInterfaceNotSetException e("Kernel::Initialize");
				throw e;
			}

			// Start the Graphical Interface
			m_Impl.m_GraphicalInterface->Start( mainWindow );
		}

	default:
		break;
	}
}

void Core::Runtime::Kernel::SetApplicationSettings( ApplicationSettingsPointer settings )
{
	Core::Runtime::Kernel::m_Impl.m_ApplicationSettings = settings;
}

void* Core::Runtime::Kernel::GetScriptingInterface( void )
{
	return m_Impl.m_ScriptingInterface;
}

Core::Runtime::Kernel::WorkflowManagerPointer Core::Runtime::Kernel::GetWorkflowManager()
{
	return m_Impl.m_WorkflowManager;
}

Core::Runtime::Kernel::ProcessorManagerPointer Core::Runtime::Kernel::GetProcessorManager()
{
	return m_Impl.m_ProcessorManager;
}

