/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreBaseMainWindow_H
#define coreBaseMainWindow_H

#include <string>
#include "coreWindowConfig.h"
#include "coreWorkflow.h"
#include "coreBasePluginTab.h"

class ModuleDescription;

namespace Core
{
	class BaseWindow;

namespace Widgets 
{ 

	/** 
	\brief This is an abstract class for the main window. It is created by the 
	main app.

	\ingroup gmKernel
	\author Xavi Planes
	\date 19 Jun 2009
	*/
	class BaseMainWindow
	{
	public:
		virtual void Initialize( ) = 0;
		virtual void SetTitle( const std::string &title ) = 0;
		virtual bool Show( bool show = true ) = 0;
		virtual void ShowSplashScreen( bool show ) = 0;
		virtual void ShowLogFileBrowser(void) = 0;
		virtual void ShowProfileManager(void) = 0;
		virtual void ShowImportConfigurationWizard(void) = 0;
		virtual void ShowAboutScreen(void) = 0;
		virtual void ShowRegistrationForm(void) = 0;
		virtual void ShowWebUpdater( bool show = true ) = 0;
		virtual void ReportWarning(const std::string& message, bool showAlsoDialog) = 0;
		virtual void ReportError(const std::string& message, bool showAlsoDialog) = 0;
		virtual void ReportMessage(const std::string& message, bool showAlsoDialog) = 0;
		//! Displays \a message in the splash window list of messages, if it is present.
		virtual void ReportSplashMessage(const std::string& message) = 0;
		virtual Core::BasePluginTab* GetCurrentPluginTab(void) = 0;
		virtual Core::BasePluginTab* GetLastPluginTab(void) = 0;
		virtual std::list<Core::BasePluginTab*> GetAllPluginTabs( ) = 0;
		virtual bool AttachPluginGUI(Core::BasePluginTab* page) = 0;
		virtual void DetachPluginGUI(Core::BasePluginTab* page, bool destroy) = 0;
		virtual void CloseWindow( ) = 0;
		virtual void CreatePluginTab( const std::string &caption ) = 0;
		virtual void RegisterWindow( 
			Core::BaseWindow *baseWindow,
			WindowConfig config = WindowConfig( ).CommandPanel() ) = 0;

		/**
		Add an instance of a widget to all the already created plugin tabs
		*/
		virtual void RegisterWindow( 
			const std::string &name,
			WindowConfig config = WindowConfig( ) ) = 0;

		//! Unregister a window
		virtual void UnRegisterWindow( const std::string &name ) = 0;

		//! Attach a new workflow to main window
		virtual void AttachWorkflow(Workflow::Pointer workflow) = 0;

		//! Show WorkflowEditor dialog
		virtual void ShowWorkflowEditor( bool show = true ) = 0;

		//! Show WorkflowManager dialog
		virtual void ShowWorkflowManager( bool show = true ) = 0;

		//! Update active workflow with the current tab pages layout
		virtual void UpdateActiveWorkflowLayout() = 0;

		//! Restart application
		virtual void RestartApplication( ) = 0;

		//! Update layout for all plugin tabs
		virtual void UpdateLayout( ) = 0;

		//! Initialize plugin pages before loading plugins
		virtual void InitializePerspective( ) = 0;

		//! Things to do when all is initialized
		virtual void OnGraphicalInterfaceInitialized( ) = 0;

		//! Register a ModuleDescription discovered by a plugin provider
		virtual void RegisterModule( const std::string &pluginProvider, ModuleDescription *module ) = 0;

		//! UnRegister a ModuleDescription discovered by a plugin provider
		virtual void UnRegisterModule( const std::string &pluginProvider, ModuleDescription *module ) = 0;

		//! UnRegister a ModuleDescription discovered by a plugin provider
		virtual std::string FindFactoryNameByModule( ModuleDescription *module ) = 0;

		//! Returns false if the user pressed Cancel in the wizard.
		virtual bool ShowMissingDataEntityFieldsWizard(
			Core::DataEntity::Pointer dataEntity) = 0;

		//! Show preferences dialog
		virtual void ShowPreferencesdialog(void) = 0;

		//! Show custom application manager
		virtual void ShowCustomApplicationManager(void) = 0;

		//!
		virtual void CheckForUpdates( ) = 0;

		//! Lock all GUI to avoid refreshing when modifying multiple components
		virtual void Lock( bool lock = true ) = 0;

		/** Update selected plugins and perspective
		- Ask user to restart if enabled
		- Check if data needs to be unloaded
		- Call PluginProviderManager to load plugins
		*/
		virtual void UpdatePluginConfiguration( ) = 0;
	};


}
}

#endif
