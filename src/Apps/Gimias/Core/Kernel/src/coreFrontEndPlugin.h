/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#ifndef coreFrontEndPlugin_H
#define coreFrontEndPlugin_H

#include "gmKernelWin32Header.h"
#include "coreObject.h"
#include "corePluginMacros.h"
#include "coreBasePlugin.h"

namespace Core{
namespace FrontEndPlugin {

/**
\brief This class is the base class for Core plug-ins.
A plug-in is a computer program that interacts with a main 
application to provide a certain, usually very specific, function.

The main application provides services which the plug-ins can use, 
including a way for plug-ins to register themselves with the main 
application and a protocol by which data is exchanged with plug-ins. 
Plug-ins are dependent on these services provided by the main application and do 
not usually work by themselves. Conversely, the main application is 
independent of the plug-ins, making it possible for plug-ins to be
added and updated dynamically without changes to the main application.

Plug-ins are implemented as shared objects or libraries that are not 
explicitly linked to the libraries of the main application, but are 
created and attached to it during runtime.

The    can provide a GUI widget that can be 
inserted in the Core graphical interface dynamically.
In that sense, a FrontEndPlugin derived class can provide a command
panel, a working area and/or button toolbar.

You must use the <i>coreBeginDefinePluginMacro()</i>, a set of 
<i>coreDefinePluginAddProfileMacro(className, caption)</i>,
and close it by using coreEndDefinePluginMacro() that will construct 
the required definitions for you. 
Place the macros at your plugin cxx file.

\ingroup gmKernel
\author Juan A. Moya
\date 10 Jan 2008
*/
class GMKERNEL_EXPORT FrontEndPlugin : public Core::SmartPointerObject
{
public:
	//!
	coreDeclareSmartPointerClassMacro( Core::FrontEndPlugin::FrontEndPlugin , Core::SmartPointerObject);

protected:
	//!
    FrontEndPlugin();

	//!
	virtual ~FrontEndPlugin(void);

private:

	coreDeclareNoCopyConstructors(FrontEndPlugin);

private:

};

} // FrontEndPlugin  )
} // Core

#endif

