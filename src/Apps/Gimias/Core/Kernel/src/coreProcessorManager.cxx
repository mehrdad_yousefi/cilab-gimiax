/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreProcessorManager.h"
#include "coreReportExceptionMacros.h"
#include "coreProcessorThread.h"
#include "coreDataEntityHolderConnection.h"
#include "coreKernel.h"
#include "coreSettings.h"

using namespace Core;

Core::ProcessorManager::ProcessorManager()
{
	m_EnableMultiThreading = true;

	AddExecutionQueue( "Default" );
	GetExecutionQueue( "Default" )->SetNumberOfThreads( 2 );
}

Core::ProcessorManager::~ProcessorManager()
{
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();

	std::string multithreading = m_EnableMultiThreading ? "true" : "false";
	settings->SetPluginProperty( "GIMIAS", "MultiThreading", multithreading );
	
	if ( GetExecutionQueue( "Default" ).IsNotNull( ) )
	{
		std::ostringstream str;
		str << GetExecutionQueue( "Default" )->GetNumOfThreads();
		std::string numThreads = str.str( ).c_str();
		settings->SetPluginProperty( "GIMIAS", "NumberOfThreads", numThreads );
	}
}

void Core::ProcessorManager::Initialize( )
{
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();
	
	std::string multithreading;
	if ( settings->GetPluginProperty( "GIMIAS", "MultiThreading", multithreading ) )
	{
		m_EnableMultiThreading = ( multithreading == "true" );
	}

	std::string numThreads;
	if ( settings->GetPluginProperty( "GIMIAS", "NumberOfThreads", numThreads ) )
	{
		GetExecutionQueue( "Default" )->SetNumberOfThreads( atoi( numThreads.c_str() ) );
	}
}

ProcessorThread::Pointer Core::ProcessorManager::Execute( 
	BaseProcessor::Pointer processor, const std::string &queueName )
{
	bool enabledOld = processor->GetMultithreading();
	if ( !m_EnableMultiThreading )
	{
		processor->SetMultithreading( false );
	}

	ProcessorThread::Pointer processorThread;
	if ( processor->GetMultithreading() )
	{
		processorThread = ExecuteMultiThreading( processor, queueName );
	}
	else
	{
		ExecuteCurrentThread( processor );
	}

	processor->SetMultithreading( enabledOld );

	return processorThread;
}

Core::ProcessorThread::Pointer Core::ProcessorManager::GetProcessorThread( 
	long uid, int state, BaseProcessor::Pointer processor )
{
	ProcessorThread::Pointer processorThread;
	ProcessorThreadListType::iterator it = m_ProcessorList.begin();
	while ( processorThread.IsNull() && it != m_ProcessorList.end() )
	{
		bool UIDIsOk = uid == -1 || (*it)->GetUID( ) == uid;
		bool stateIsOk = state == -1 || (*it)->GetState( ) & state;
		bool processorIsOk = processor.IsNull() || (*it)->GetProcessor() == processor;
		if ( UIDIsOk && stateIsOk && processorIsOk )
		{
			processorThread = *it;
		}
		it++;
	}

	return processorThread;
}

ProcessorThread::Pointer Core::ProcessorManager::CreateProcessorThread( 
	BaseProcessor::Pointer processor )
{
	ProcessorThread::Pointer processorThread;
	processorThread = GetProcessorThread( -1, -1, processor );
	if ( processorThread.IsNull( ) )
	{
		processorThread = ProcessorThread::New( processor );
		m_ProcessorList.push_back( processorThread );
	}
	return processorThread;
}

Core::ProcessorManager::ProcessorThreadListType Core::ProcessorManager::GetProcessorThreadList() const
{
	return m_ProcessorList;
}

ProcessorThread::Pointer Core::ProcessorManager::ExecuteMultiThreading( 
	BaseProcessor::Pointer processor, const std::string &queueName )
{
	// Create a copy of the processor with current parameters
	BaseProcessor::Pointer instance;
	// Some classes does not have the CreateAnother() function implemented,
	// so the base function of the smart pointer will be called and the
	// dynamic_cast will return NULL
	instance = dynamic_cast<BaseProcessor*> (processor->CreateAnother().GetPointer( ));
	if ( instance.IsNotNull( ) && instance->Copy( processor ) )
	{
		// Connect new outputs to the original processor outputs
		for ( size_t i = 0 ; i < processor->GetNumberOfOutputs() ; i++ )
		{
			DataEntityHolderConnection::Connect( 
				instance->GetOutputDataEntityHolder( i ),
				processor->GetOutputDataEntityHolder( i ),
				CONNECTION_PROCESSOR_OUT_AND_PROCESSOR_OUT );
		}
	}
	else
	{
		// if instance cannot be created, check if there's another processor in the list
		ProcessorThread::Pointer thread;
		thread = GetProcessorThread( -1, ProcessorThread::STATE_ACTIVE | ProcessorThread::STATE_PENDING, processor );
		if ( thread.IsNotNull() )
		{
			throw Exceptions::Exception( 
				"ProcessorManager::ExecuteMultiThreading",
				"Cannot execute this processor twice" );
		}

		instance = processor;

	}

	// Create thread context
	ProcessorThread::Pointer processorThread = CreateProcessorThread( instance );

	// Add to the thread pool
	ProcessorExecutionQueue::Pointer queue = GetExecutionQueue( queueName );
	if ( queue.IsNull() )
	{
		AddExecutionQueue( queueName );
		queue = GetExecutionQueue( queueName );
	}
	queue->Schedule( processorThread );

	return processorThread;
}

void Core::ProcessorManager::ExecuteCurrentThread( BaseProcessor::Pointer processor )
{
	// Create the thread context
	ProcessorThread::Pointer processorThread = CreateProcessorThread( processor );

	// Show processing message
	if ( processorThread->GetShowProcessingMessage( ) )
	{
		Core::Runtime::Kernel::GetApplicationRuntime( )->SetAppState( 
			Core::Runtime::APP_STATE_PROCESSING );
	}

	// Execute it
	processorThread->WorkerFunc( );

	// Hide processing message
	if ( processorThread->GetShowProcessingMessage( ) )
	{
		Core::Runtime::Kernel::GetApplicationRuntime( )->SetAppState( 
			Core::Runtime::APP_STATE_IDLE );
	}
}

void Core::ProcessorManager::EnableMultithreading( bool val )
{
	m_EnableMultiThreading = val;
}

bool Core::ProcessorManager::GetEnableMultiThreading() const
{
	return m_EnableMultiThreading;
}

bool Core::ProcessorManager::AbortAll()
{
	bool success = true;
	ProcessorThreadListType::iterator it;
	for ( it = m_ProcessorList.begin() ; it != m_ProcessorList.end() ; it++ )
	{
		if ( (*it)->GetState() != ProcessorThread::STATE_FINISHED )
		{
			(*it)->GetUpdateCallback( )->SetAbortProcessing( true );
			success = false;
		}
	}

	return success;
}

bool Core::ProcessorManager::DetachAll()
{
	bool success = true;
	ProcessorThreadListType::iterator it;
	for ( it = m_ProcessorList.begin() ; it != m_ProcessorList.end() ; it++ )
	{
		if ( (*it)->GetState() != ProcessorThread::STATE_FINISHED )
		{
			(*it)->GetUpdateCallback( )->SetDetachProcessing( true );
			(*it)->GetUpdateCallback( )->SetAbortProcessing( true );
			success = false;
		}
	}

	return success;
}

void Core::ProcessorManager::AddExecutionQueue( const std::string &name )
{
	if ( GetExecutionQueue( name ).IsNull( ) )
	{
		m_ExecutionQueueMap[ name ] = ProcessorExecutionQueue::New( );
	}
}

ProcessorExecutionQueue::Pointer 
Core::ProcessorManager::GetExecutionQueue( const std::string &name )
{
	ProcessorExecutionQueueMapType::iterator it;
	it = m_ExecutionQueueMap.find( name );
	if ( it == m_ExecutionQueueMap.end() )
	{
		return NULL;
	}


	return it->second;
}

void Core::ProcessorManager::StopAllExecutionQueues( )
{
	ProcessorExecutionQueueMapType::iterator it;
	for ( it = m_ExecutionQueueMap.begin( ); it != m_ExecutionQueueMap.end( ) ; it++ )
	{
		it->second->StopAllThreads( );
	}
}