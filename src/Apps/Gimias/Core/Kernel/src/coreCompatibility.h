/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreCompatibility_H
#define coreCompatibility_H

#include "gmKernelWin32Header.h"
#include "coreObject.h"
#include "coreWorkflow.h"
#include "coreSettings.h"
#include "corePluginProviderManager.h"

namespace Core
{

/**
\brief Backwards compatibility general functions

\ingroup gmFrontEndPlugin
\author Xavi Planes
\date 1 June 2010
*/
class GMKERNEL_EXPORT Compatibility : public Core::Object
{
public:

	/**
	Old class names where using typeid( <type> ).name( ). Now it uses
	the macro coreClassNameMacro( ). We need to convert old class names
	to new ones, when reading files
	*/
	static std::string UpdateOldClassName( const std::string &name );


	/** Update a workflow from older version
	Remove UserHelper and update class names
	*/
	static void UpdateOldWorkflow( Core::Workflow::Pointer workflow );

	/**
	Update name of window class
	*/
	static void UpdateWorkingAreaProperties( blTagMap::Pointer properties );

	//! Update selected plugins from profile to plugin provider
	static void UpdateProfile( 
		Core::Runtime::Settings::Pointer settings, 
		Runtime::PluginProviderManager::PluginProvidersType pluginProviders );

	/** Update MultiROILevel tag from 1.3 to 1.4, because the pixel value 
	was starting from 0 **/
	static void UpdateDataEntityMetadata( Core::DataEntity::Pointer dataEntity );
};

} // namespace Core

#endif // coreCompatibility_H

