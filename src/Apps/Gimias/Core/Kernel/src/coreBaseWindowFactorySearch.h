/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreBaseWindowFactorySearch_H
#define _coreBaseWindowFactorySearch_H

#include "coreObject.h"
#include "coreBaseWindow.h"
#include "coreWindowConfig.h"
#include "coreBaseWindowFactory.h"

namespace Core
{


/**
\brief Search a Factory usgin specific parameters

\ingroup gmKernel
\author Xavi Planes
\date Oct 2011
*/
class GMKERNEL_EXPORT BaseWindowFactorySearch : public Core::SmartPointerObject
{
public:

public:

	coreDeclareSmartPointerClassMacro(
		Core::BaseWindowFactorySearch, 
		Core::SmartPointerObject);

	//!
	void SetType(WIDGET_TYPE val);
	void SetCaption(std::string val);
	void SetCategory(std::string val);
	void SetState(WIDGET_STATE val);
	void SetId(int val);
	void SetInstanceClassName(std::string val);

	//!
	void Update( );

	//!
	std::list<std::string> GetFactoriesNames() const;
	

protected:
	//!
	BaseWindowFactorySearch( );

protected:

	//! Parameters for search
	WIDGET_TYPE m_Type;
	std::string m_Caption;
	std::string m_Category;
	int m_Id;
	WIDGET_STATE m_State;
	std::string m_InstanceClassName;

	//!
	std::list<std::string> m_FactoriesNames;
};

} // namespace Core

#endif // _coreBaseWindowFactorySearch_H

