/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#ifndef coreBasePlugin_H
#define coreBasePlugin_H

#include "gmKernelWin32Header.h"
#include "coreObject.h"

namespace Core
{
namespace Runtime
{

/**
\brief Base plugin class with generic information and XML IO

\ingroup gmKernel
\author Xavi Planes
\date Oct 2012
*/
class GMKERNEL_EXPORT BasePlugin : public SmartPointerObject
{
public:
	coreDeclareSmartPointerClassMacro(Core::Runtime::BasePlugin,SmartPointerObject)

	//!
	std::string GetName() const;
	void SetName( const std::string &name );

	//!
	std::string GetCaption() const;
	void SetCaption(std::string val);

	//!
	std::string GetExpireDate( ) const;
	void SetExpireDate( const std::string &val );

	//!
	std::string GetSignature( ) const;
	void SetSignature( const std::string &val );

	//!
	std::list<std::string> GetDependencies() const;

	//!
	std::string GetLibraryPath() const;

	//!
	void ReadXML( );

	//!
	void WriteXML( );

protected:
	//!
	BasePlugin(void);
	//!
	virtual ~BasePlugin(void);

private:

	coreDeclareNoCopyConstructors(BasePlugin);

protected:
	//! "plugins/SceneViewPlugin/lib/Debug/"
	std::string m_LibraryPath;

	//! Create a default unique name 
	static int m_PluginNameIndex;

	//!
	std::string m_Name;

	//!
	std::string m_Caption;

	//!
	std::string m_ExpireDate;

	//!
	std::string m_Signature;

	//!
	std::list<std::string> m_Dependencies;

};

} // namespace Runtime
} // namespace Core

#endif // coreBasePlugin_H

