/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreCompatibility.h"

std::string Core::Compatibility::UpdateOldClassName( const std::string &name )
{
	std::string outName = name;

	// Check if it an old name like: "class Core::Widgets::MovieToolbar"
	if ( outName.find( "class " ) == 0 )
	{
		// Remove class
		while ( outName.find( "class " ) != std::string::npos )
		{
			blTextUtils::StrSub( outName, "class ", "" );
		}

		// Add "Factory"
		outName += "Factory";
	}

	if ( name == "Binary Threshold" )
	{
		outName = "Binary Threshold Segmentation";
	}

	if ( name == "ptVolumeClosingPanelWidgetFactory" )
	{
		outName = "VolumeClosingPanelWidgetFactory";
	}

	if ( name == "Core::Widgets::MultiRenderWindowConfigFactory" )
	{
		outName = "Core::Widgets::WorkingAreaConfigFactory";
	}

	return outName;
}


void Core::Compatibility::UpdateOldWorkflow( Core::Workflow::Pointer workflow )
{
	if ( workflow.IsNull() )
	{
		return;
	}

	// Remove old plugins
	Core::Workflow::PluginNamesListType plugins = workflow->GetPluginNamesList( );
	plugins.remove( "NeuroToolsPlugin" );
	workflow->SetPluginNamesList( plugins );

	// Process each step
	Core::Workflow::StepListType steps = workflow->GetStepVector();
	Core::Workflow::StepListType::iterator it;
	for ( it = steps.begin() ; it != steps.end( ) ; it++ )
	{
		// Update window list
		WorkflowStep::WindowListType::iterator itWin;
		for ( itWin = (*it)->GetWindowList().begin();
			itWin != (*it)->GetWindowList().end();
			itWin++ )
		{
			*itWin = UpdateOldClassName( *itWin );
		}

		// Remove old toolbars
		(*it)->GetWindowList().remove( "Core::Widgets::ToolbarLayoutFactory" );
		(*it)->GetWindowList().remove( "Core::Widgets::ToolbarAppearanceFactory" );
		(*it)->GetWindowList().remove( "class Core::Widgets::UserHelper" );

		// Add Toolbox
		WorkflowStep::WindowListType::iterator itWinToolbox;
		itWinToolbox = std::find( (*it)->GetWindowList().begin( ), (*it)->GetWindowList().end( ), 
			"Core::Widgets::ToolboxWidgetFactory" );
		if ( itWinToolbox == (*it)->GetWindowList().end( ) )
		{
			(*it)->GetWindowList().push_back( "Core::Widgets::ToolboxWidgetFactory" );
		}

		// Update step vector
		WorkflowStep::SubStepListType subStepList = (*it)->GetSubStepVector();
		WorkflowStep::SubStepListType::iterator itSubStep;
		for ( itSubStep = subStepList.begin(); itSubStep != subStepList.end(); itSubStep++ )
		{
			WorkflowSubStep::AlternativesListType::iterator itAlt;
			for ( itAlt = (*itSubStep)->GetAlternatives().begin(); 
				itAlt != (*itSubStep)->GetAlternatives().end(); 
				itAlt++ )
			{
				*itAlt = UpdateOldClassName( *itAlt );
			}

			(*itSubStep)->SetName( UpdateOldClassName( (*itSubStep)->GetName() ) );
		}

		// Update layout
		blTag::Pointer tag = (*it)->GetProperties( )->FindTagByName( "Layout" );
		if ( tag.IsNotNull() )
		{
			// Rename Workflow Navigation to Toolbox
			std::string layout;
			tag->GetValue( layout );
			blTextUtils::StrSub( layout, "Workflow Navigation", "Toolbox" );
			tag->SetValue( layout );
		}
		
	}
}

void Core::Compatibility::UpdateWorkingAreaProperties( blTagMap::Pointer properties )
{
	blTag::Pointer tag = properties->FindTagByName( "Size" );
	if ( tag.IsNull() )
	{
		return;
	}

	unsigned int size = tag->GetValueCasted<unsigned int>( );
	for ( size_t i = 0 ; i < size ; i++ )
	{
		std::ostringstream stream;
		stream << "View" << i;

		blTagMap::Pointer tagMapView;
		tagMapView = properties->GetTagValue<blTagMap::Pointer>( stream.str() );
		if ( tagMapView.IsNull() )
		{
			continue;
		}

		// Find if factory exists
		blTag::Pointer tag = tagMapView->FindTagByName( "Factory" );
		std::string factoryName;
		if ( tag.IsNull() )
		{
			continue;
		}

		// Update old class name
		tag->GetValue( factoryName );
		factoryName = UpdateOldClassName( factoryName );
		tag->SetValue( factoryName );

	}
}



void Core::Compatibility::UpdateProfile( 
	Core::Runtime::Settings::Pointer settings,
	Runtime::PluginProviderManager::PluginProvidersType pluginProviders )
{
	Profile::ProfileSet profileSet;
	profileSet = settings->m_configVars.m_profile->GetProfileAsStringSet( );

	if ( ( profileSet.size() == 1 && *profileSet.begin() == "Common Functionalities" )
		 || profileSet.size() == 0 )
	{
		return;
	}

	Runtime::PluginProviderManager::PluginProvidersType::iterator itProvider;
	for ( itProvider = pluginProviders.begin() ;itProvider != pluginProviders.end() ; itProvider++ )
	{
		blTagMap::Pointer plugins;
		plugins = (*itProvider)->GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );

		blTagMap::Iterator it;
		for ( it = plugins->GetIteratorBegin() ; it != plugins->GetIteratorEnd() ; it++ )
		{
			Profile::ProfileSet::iterator itFound;
			if ( plugins->GetTag( it )->GetValue().type() == typeid( bool ) )
			{
				itFound = std::find( profileSet.begin(), profileSet.end(), plugins->GetTag( it )->GetName() );
				plugins->GetTag( it )->SetValue( itFound != profileSet.end() );
			}
		}
	}
}

void Core::Compatibility::UpdateDataEntityMetadata( Core::DataEntity::Pointer dataEntity )
{
	if ( dataEntity.IsNull() )
	{
		return;
	}

	// Get MultiROILevel tag
	blTagMap::Pointer labelMap;
	DataEntityTag::Pointer tag = dataEntity->GetMetadata()->FindTagByName( "MultiROILevel" );
	if ( tag.IsNull( ) )
	{
		return ;
	}

	// Get tagmap
	tag->GetValue( labelMap );
	if ( labelMap.IsNull() )
	{
		return ;
	}

	// Find the minimum level
	int minLevel = INT_MAX;
	blTagMap::Iterator it = labelMap->GetIteratorBegin();
	for ( ; it!= labelMap->GetIteratorEnd(); it++ )
	{
		int value;
		it->second->GetValue<int>( value );
		minLevel = std::min( value, minLevel);
	}

	// If min level is 0, increase all levels by 1
	if ( minLevel != 0 )
	{
		return;
	}

	it = labelMap->GetIteratorBegin();
	for ( ; it!= labelMap->GetIteratorEnd(); it++ )
	{
		int value;
		it->second->GetValue<int>( value );
		it->second->SetValue( value + 1 );
	}
}

