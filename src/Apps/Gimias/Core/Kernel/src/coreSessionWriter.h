/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreSessionWriter_H
#define coreSessionWriter_H


#include "gmKernelWin32Header.h"
#include "blTagMap.h"
#include "coreObject.h"
#include "coreDataEntityList.h"
#include "coreBaseDataEntityWriter.h"

namespace Core 
{
	class RenderingTreeManager;

namespace IO 
{

/** 
\brief The SessionWriter will save all data in Core::DataList on disc and its
parent-children relation in session xml file 

\ingroup gmKernel
\author Jakub Lyko
\date 10 Dec 2009
*/
class GMKERNEL_EXPORT SessionWriter : public BaseDataEntityWriter
{
public:
	coreDeclareSmartPointerClassMacro(Core::IO::SessionWriter, BaseDataEntityWriter);	

	//! 
	void WriteData( );

	//!
	void SetTreeManager(Core::RenderingTreeManager* val);

protected:
	//! 
	SessionWriter();

	//! 
	virtual ~SessionWriter();

private:
	/**
	\brief Save data entity in default format and returns LightData which 
	will be used for saving session. LightData will be stored in xml and
	will provide information to load DataEntity. 
	*/
	blTagMap::Pointer SaveDataEntity(
		Core::DataEntity::Pointer dataEntity,
		const std::string &dirPath);

	//! Save views tag in Views subfolder
	void SaveViews( 
		Core::DataEntity::Pointer dataEntity, 
		const std::string &dirPath );

private:
	//!
	Core::RenderingTreeManager* m_TreeManager;

	//! Count DataEntity instances with the same name to use for writing to disk
	std::map<std::string,int> m_NameCount;
private:
	coreDeclareNoCopyConstructors(SessionWriter);

};

} // namespace IO
} // namespace Core

#endif
