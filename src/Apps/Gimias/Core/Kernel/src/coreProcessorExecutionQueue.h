/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreProcessorExecutionQueue_H
#define _coreProcessorExecutionQueue_H

// CoreLib
#include "gmKernelWin32Header.h"
#include "coreObject.h"
#include "coreProcessorThread.h"

namespace Core{

/**
\brief A queue that executes Processors using thread pool pattern.

\ingroup gmKernel
\author Xavi Planes
\date 17 01 2011
*/
class GMKERNEL_EXPORT ProcessorExecutionQueue : public Core::SmartPointerObject
{
public:
	//!
	coreDeclareSmartPointerClassMacro(Core::ProcessorExecutionQueue, Core::SmartPointerObject);

	/** Execute this processor in background
	\throw exception if there's an error
	*/
	void Schedule( ProcessorThread::Pointer processorThread );

	//!
	void SetNumberOfThreads( long num );
	long GetNumOfThreads() const;

	//!
	void StopAllThreads( );

protected:
	//!
	ProcessorExecutionQueue(void);

	//!
	virtual ~ProcessorExecutionQueue(void);

private:
	//!
	ProcessorExecutionQueue( const Self& );

	//!
	void operator=(const Self&);


protected:
	//!
	class Impl;
	//!
	Impl* m_Impl;
	//!
	long m_NumOfThreads;
};

} // namespace Core{

#endif // _coreProcessorExecutionQueue_H

