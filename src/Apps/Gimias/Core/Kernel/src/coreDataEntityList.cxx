/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDataEntityList.h"
#include "coreAssert.h"
#include "coreReportExceptionMacros.h"
#include "CILabBoostMacros.h"
#include "coreDataEntityHolderConnection.h"

#include <boost/thread/mutex.hpp>

using namespace Core;

const bool ENABLE_DATAENTITYLIST_DEBUG = false;

class DataEntityList::Impl
{
public:
	boost::mutex m_Mutex;
};

/**
*/
DataEntityList::DataEntityList(void)
{
	m_SelectedDataEntityHolder = Core::DataEntityHolder::New();
	m_SelectedDataEntityHolder->SetName( "DataEntityList: Selected Holder" );

	m_Impl = new Impl( );

	// Add root node
	Core::DataEntity::Pointer root = Core::DataEntity::New( Core::SessionTypeId );
	root->AddTimeStep( blTagMap::New( ) );
	root->GetMetadata( )->SetName( "Root" );
	Add( root );
}

/**
*/
DataEntityList::~DataEntityList(void)
{
	RemoveAll( );

	delete m_Impl;
}

/**
*/
bool DataEntityList::Add(DataEntity::Pointer dataEntity)
{
	coreAssertMacro(dataEntity.IsNotNull() && 
		"The requested DataEntity cannot be NULL, if you plan to add it to the DataEntityList");

	bool inserted = false;
	if ( Find( dataEntity->GetId( ) ) == m_InternalList.end( ) )
	{
		// If dataEntity has no father, set root item
		if ( dataEntity->GetFather( ).IsNull( ) && !m_InternalList.empty( ) )
			dataEntity->SetFather( Get( m_InternalList.begin( ) ) );

		// Add to the list
		DataEntityList::InternalListElement elem;
		elem = Core::DataEntityHolder::New( );
		elem->SetSubject( dataEntity );
		elem->SetName( "DataEntityList: In/Out holder" );
		m_InternalList.push_back(elem);
		NotifyObserversOnAdd(dataEntity);
		inserted = true;
	}

	return inserted;
}

/**
*/
void DataEntityList::Remove(DataEntity::Pointer dataEntity)
{
	try
	{
		if ( dataEntity.IsNull() )
		{
			throw Core::Exceptions::Exception( 
				"DataEntityList::Remove", 
				"The requested DataEntity cannot be NULL, for removing" \
				"it from the DataEntityList" );
		}

		// Root node
		if ( dataEntity->GetId( ) == 1 )
		{
			throw Core::Exceptions::Exception( 
				"DataEntityList::Remove", 
				"The requested DataEntity cannot be removed" );
		}

		if ( IsLocked( dataEntity->GetId() ) )
		{
			std::ostringstream str;
			str << "Data " << dataEntity->GetId() << " is locked by task " 
				<< m_LockedDataEntityMap[ dataEntity->GetId() ];
			throw Core::Exceptions::Exception( 
				"DataEntityList::Remove", 
				str.str().c_str() );
		}

		if ( ENABLE_DATAENTITYLIST_DEBUG )
		{
			std::cout 
				<< "Remove DataEntity: " << dataEntity->GetId()
				<< " Children: " << dataEntity->GetChildrenList().size() 
				<< std::endl;
		}

		// Remove Children first
		if( !dataEntity->GetChildrenList().empty() )
		{
			// Create a temporal list because it will change during the iteration
			Core::DataEntity::ChildrenListType childrenList;
			childrenList = dataEntity->GetChildrenList();

			Core::DataEntity::ChildrenListType::iterator it;
			it = childrenList.begin();
			while ( it != childrenList.end() )
			{
				int dataEntityID = (*it)->GetId();

				if ( ENABLE_DATAENTITYLIST_DEBUG )
				{
					std::cout << "  Removing children: " << dataEntityID << std::endl;
				}

				// When the data entity is removed from the list
				// reference count could be different of 0 because there is
				// some reference somewhere
				Remove( dataEntityID );

				it++;
			}
		}

		// Remove father reference
		dataEntity->SetFather( NULL );

		// Get the previous data entity to select it if needed
		Core::DataEntity::Pointer previousDataEntity;
		if ( dataEntity == GetSelectedDataEntity( ) )
		{
			previousDataEntity = GetPreviousDataEntity( dataEntity );
		}

		// Find data entity
		InternalListType::iterator it = Find( dataEntity->GetId() );

		// Set the holder to NULL -> Notify connected holders
		if ( GetDataEntity( dataEntity->GetId() ).IsNotNull() )
		{
			GetDataEntityHolder( dataEntity->GetId() )->SetSubject( NULL );
		}

		// Unselect the current data entity
		if ( dataEntity == GetSelectedDataEntity( ) )
		{
			m_SelectedDataEntityHolder->SetSubject( NULL );
		}

		if ( it != m_InternalList.end( ) )
		{
			// Remove data entity from the list
			m_InternalList.erase( it );

			// Notify Observers
			NotifyObserversOnRemove(dataEntity);
		}

		// Here everything should be clean
		// Select the new data entity if it was selected
		if ( previousDataEntity.IsNotNull() )
		{
			m_SelectedDataEntityHolder->SetSubject( previousDataEntity );
		}

	}
	coreCatchExceptionsAddTraceAndThrowMacro(DataEntityList::Remove)
}

/**
*/
void DataEntityList::Remove(unsigned int dataEntityId)
{
	try
	{
		DataEntity::Pointer dataEntity = GetDataEntity(dataEntityId);
		if(dataEntity.IsNotNull())
			Remove(dataEntity);
	}
	coreCatchExceptionsAddTraceAndThrowMacro(DataEntityList::Remove)
}

/**
*/
void DataEntityList::RemoveAll()
{
	try
	{
		// Get all ID except ID 1 (root)
		std::list<int> idVector;
		for ( iterator it = Begin( ) ; it != End( ) ; it++ )
		{
			if ( GetId( it ) != 1 )
				idVector.push_back( GetId( it ) );
		}

		// Remove all data using ID
		std::list<int>::iterator idIterator;
		for ( idIterator = idVector.begin( ) ; idIterator != idVector.end( ) ; idIterator++ )
		{
			if ( IsInTheList( *idIterator ) )
				Remove( *idIterator );
		}
	}
	coreCatchExceptionsAddTraceAndThrowMacro(DataEntityList::RemoveAll)
}

/**
*/
DataEntity::Pointer DataEntityList::GetDataEntity(unsigned int dataEntityId)
{
	return GetDataEntityHolder( dataEntityId )->GetSubject( );
}


/**
*/
DataEntityHolder::Pointer Core::DataEntityList::GetDataEntityHolder( unsigned int dataEntityId )
{
	InternalListType::const_iterator it = Find( dataEntityId );

	if( it == m_InternalList.end( ) )
	{
		Core::Exceptions::DataEntityIdNotFound e("DataEntityList::GetDataEntityHolder");
		cilabBoostFormatMacro(" Unknown id %d", dataEntityId, message);
		e.Append(message.c_str());
		throw e;
	}
	return *it;
}

void DataEntityList::NotifyObserversOnAdd(DataEntity::Pointer dataEntity) const
{
	// Calling the function call operator may invoke undefined behavior if no slots are connected
	// to the signal, depending on the combiner used, so check it was connected to a slot beforehand
	if(!m_OnAddSignal.empty())
		m_OnAddSignal(dataEntity);
}

//!
void DataEntityList::NotifyObserversOnRemove(DataEntity::Pointer dataEntity) const
{
	// Calling the function call operator may invoke undefined behavior if no slots are connected
	// to the signal, depending on the combiner used, so check it was connected to a slot beforehand
	if(!m_OnRemoveSignal.empty())
		m_OnRemoveSignal(dataEntity);
}


/**
*/
DataEntityList::iterator DataEntityList::Begin(void)
{ 
	return m_InternalList.begin(); 
}

/**
*/
DataEntityList::iterator DataEntityList::End(void) 
{ 
	return m_InternalList.end(); 
}

/**
*/
void DataEntityList::Next(Self::iterator& it) 
{
	++it; 
}

/**
*/
unsigned int DataEntityList::GetId(Self::iterator it) 
{
	if ( (*it).IsNull( ) || (*it)->GetSubject( ).IsNull( ) )
	{
		throw Core::Exceptions::Exception( 
			"DataEntityList::GetId", 
			"The requested element is NULL" );
	}

	return (*it)->GetSubject( )->GetId( ); 
}

/**
*/
DataEntity::Pointer DataEntityList::Get(Self::iterator it)
{
	return (*it)->GetSubject( ); 
}


size_t Core::DataEntityList::GetCount() const
{
	return m_InternalList.size();
}

bool Core::DataEntityList::IsInTheList( unsigned int dataEntityId )
{
	bool foundInInternalList;
	
	InternalListType::iterator it = Find( dataEntityId );
	foundInInternalList = it != m_InternalList.end();

	// Check that the holder is correctly configured
	if ( foundInInternalList )
	{
		Core::DataEntityHolder::Pointer  holder;
		holder = GetDataEntityHolder( dataEntityId );
		foundInInternalList = holder.IsNotNull() && holder->GetSubject().IsNotNull( ) ;
	}

	return foundInInternalList;
}


int  Core::DataEntityList::GetPositionInList(unsigned int dataEntityId)
{
	if(  Find( dataEntityId ) == m_InternalList.end() )
		return -1;
	else
	{
		Self::iterator it = m_InternalList.begin();
		//! the position to be returned. As Items are pushed from top,
		// the first item is the one with the highest id
		size_t position = m_InternalList.size()-1;

		while ( it != m_InternalList.end() )
		{	
			if( GetId(it) == dataEntityId )
				return int( position );
			else
			{
				it++;
				position--;
			}
		}
	}

	return -1;
}

Core::DataEntityList::iterator Core::DataEntityList::Find( 
	unsigned int dataEntityId )
{
	InternalListType::iterator it = m_InternalList.begin( );
	while ( it != m_InternalList.end( ) )
	{
		if ( (*it)->GetSubject( ) && (*it)->GetSubject( )->GetId( ) == dataEntityId )
		{
			return it;
		}

		it++;
	}

	return m_InternalList.end();
}

DataEntity::Pointer Core::DataEntityList::GetPreviousDataEntity( 
	DataEntity::Pointer dataEntity )
{
	if ( GetCount( ) > 1 )
	{
		Core::DataEntityList::iterator it;
		Core::DataEntityList::iterator begin = Begin( );
		it = Find( dataEntity->GetId( ) );

		// Select the previous one
		if ( it != begin )
		{
			it--;
			return Get( it );
		}
		else 
		{
			it++;
			return Get( it );
		}
	}

	return NULL;
}

Core::DataEntityHolder::Pointer 
Core::DataEntityList::GetSelectedDataEntityHolder() const
{
	return m_SelectedDataEntityHolder;
}

Core::DataEntity::Pointer Core::DataEntityList::GetSelectedDataEntity() const
{
	return m_SelectedDataEntityHolder->GetSubject();
}

void Core::DataEntityList::RemoveSelectedDataEntity()
{
	Remove( GetSelectedDataEntity() );
}

Core::DataEntity::Pointer Core::DataEntityList::FindChild( 
	Core::DataEntity::Pointer fatherDataEntity, 
	Core::DataEntityType type,
	Core::DataEntityMetadata::Pointer metadata )
{
	Core::DataEntity::Pointer childDataEntity = NULL;
	if(fatherDataEntity.IsNull())
	{
		return NULL;
	}

	Core::DataEntity::ChildrenListType list = fatherDataEntity->GetChildrenList();
	Core::DataEntity::ChildrenListType::iterator it = list.begin();
	while ( it != list.end() && childDataEntity.IsNull() )
	{
		Core::DataEntity::Pointer child = (*it);
		bool checkType = child->GetType() & type;
		bool checkIsInList = IsInTheList( child->GetId( ) );
		bool checkMetadata = true;
		if ( metadata.IsNotNull( ) )
		{
			Core::DataEntityMetadata::Pointer childMetadata;
			childMetadata = child->GetMetadata();
			blTagMap::Iterator it;
			for ( it = metadata->GetIteratorBegin( ); 
				  it != metadata->GetIteratorEnd( ); 
				  it++ )
			{
				blTag::Pointer childTag = childMetadata->GetTag( it->first );
				checkMetadata &= childTag->GetValueAsString() == it->second->GetValueAsString();
			}
		}
		if ( checkType && checkIsInList && checkMetadata )
		{
			childDataEntity = (*it);
		}
		it++;
	}

	return childDataEntity;
}

Core::DataEntity::Pointer Core::DataEntityList::FindRelative(
	DataEntity::Pointer dataEntity,
	Core::DataEntityType type,
	Core::DataEntityMetadata::Pointer metadata /*= NULL*/ )
{
	while( dataEntity.IsNotNull( ) && dataEntity->GetType( ) != type )
	{
		dataEntity = dataEntity->GetFather( );
	}

	return dataEntity;
}

void Core::DataEntityList::ConnectInputHolder( 
	Core::DataEntityHolder::Pointer processorHolder )
{
	Core::DataEntity::Pointer dataEntity;
	dataEntity = processorHolder->GetSubject( );
	if ( dataEntity.IsNull() )
	{
		return;
	}

	Core::DataEntityHolder::Pointer listHolder;
	listHolder = GetDataEntityHolder( dataEntity->GetId( ) );

	DataEntityHolderConnection::Connect( 
		listHolder, 
		processorHolder,
		CONNECTION_DATALIST_AND_PROCESSOR_IN );
}

void Core::DataEntityList::ConnectOutputHolder( 
	Core::DataEntityHolder::Pointer processorHolder )
{
	Core::DataEntity::Pointer dataEntity;
	dataEntity = processorHolder->GetSubject( );
	if ( dataEntity.IsNull() )
	{
		return;
	}

	Core::DataEntityHolder::Pointer listHolder;
	listHolder = GetDataEntityHolder( dataEntity->GetId( ) );

	DataEntityHolderConnection::Connect( 
		listHolder, 
		processorHolder,
		CONNECTION_DATALIST_AND_PROCESSOR_OUT );
}

void Core::DataEntityList::Lock( long uid, DataEntityGroupIDType dataEntityGroup )
{
	boost::lock_guard<boost::mutex> lock_g(m_Impl->m_Mutex); 
	
	DataEntityGroupIDType::iterator it;
	for ( it = dataEntityGroup.begin() ; it != dataEntityGroup.end() ; it++ )
	{
		if ( IsLocked( *it ) )
		{
			std::ostringstream str;
			str << "Data " << *it << " is locked by task " << m_LockedDataEntityMap[ *it ];
			throw Core::Exceptions::Exception( 
				"DataEntityList::Lock", 
				str.str().c_str() );
		}
	}

	for ( it = dataEntityGroup.begin() ; it != dataEntityGroup.end() ; it++ )
	{
		m_LockedDataEntityMap[ *it ] = uid;
	}
}

void Core::DataEntityList::UnLock( long uid )
{
	boost::lock_guard<boost::mutex> lock_g(m_Impl->m_Mutex); 

	DataEntityMapType::iterator it = m_LockedDataEntityMap.begin();
	while( it != m_LockedDataEntityMap.end() )
	{
		if ( it->second == uid )
		{
			m_LockedDataEntityMap.erase( it );
			it = m_LockedDataEntityMap.begin();
		}
		else
		{
			it++;
		}
	}
}

bool Core::DataEntityList::IsLocked( long dataEntityID )
{
	DataEntityMapType::iterator itDataEntity;
	itDataEntity = m_LockedDataEntityMap.find( dataEntityID );
	if ( itDataEntity != m_LockedDataEntityMap.end() )
	{
		return true;
	}

	return false;
}
