/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreKernel_H
#define coreKernel_H

#include "coreObject.h"
#include "coreEnvironment.h"
#include "coreBaseFactory.h"
#include "coreBaseMainWindow.h"

namespace Core
{
class DataContainer;
class WorkflowManager;
class ProcessorManager;

namespace Runtime
{

class Logger;
class Settings;
class wxMitkGraphicalInterface;

/** 
\brief The Kernel class provides access to all static functionalities the 
application is capable of. It essentially works as an broker for the 
programmer who wants to extend any functionality or to get access to the 
high level managers initialized by the kernel.

Is responsible of initializing, keeping alive and terminating the 
whole environment for the platform.

To initialize it you need to call:
Initialize( ) and InitializeGraphicalInterface( )

\ingroup gmKernel
\author Juan Antonio Moya
\date 31 May 2007
*/
class GMKERNEL_EXPORT Kernel : Core::Object
{
public:
	coreClassNameMacro(Core::Runtime::Kernel)

	//! Define virtual destructor to avoid warning
	virtual ~Kernel( );

	/**
	Initialize the Kernel
	\param [in] environment Environment for managing runtime execution
	*/
	static void Initialize(
		Core::Runtime::Environment* environment );

	//!
	static void Terminate(void);

	typedef coreDeclareSwigWrappableSmartPointerTypeMacro(Core::Runtime::Environment) 
		ApplicationRuntimePointer;
	typedef coreDeclareSwigWrappableSmartPointerTypeMacro(Core::Runtime::Logger) 
		RuntimeLoggerPointer;
	typedef coreDeclareSwigWrappableSmartPointerTypeMacro(Core::Runtime::Settings) 
		ApplicationSettingsPointer;
	typedef coreDeclareSwigWrappableSmartPointerTypeMacro(Core::Runtime::wxMitkGraphicalInterface) 
		RuntimeGraphicalInterfacePointer;
	typedef coreDeclareSwigWrappableSmartPointerTypeMacro(Core::DataContainer) 
		DataContainerPointer;
	typedef coreDeclareSwigWrappableSmartPointerTypeMacro(Core::WorkflowManager) 
		WorkflowManagerPointer;
	typedef coreDeclareSwigWrappableSmartPointerTypeMacro(Core::ProcessorManager) 
		ProcessorManagerPointer;

	static ApplicationRuntimePointer GetApplicationRuntime();
	static RuntimeLoggerPointer GetLogManager();
	static ApplicationSettingsPointer GetApplicationSettings();
	static RuntimeGraphicalInterfacePointer GetGraphicalInterface();
	static DataContainerPointer GetDataContainer();
	static WorkflowManagerPointer GetWorkflowManager();
	static ProcessorManagerPointer GetProcessorManager();
	
	//! Function to override the default instance of the application settings
	static void SetApplicationSettings(ApplicationSettingsPointer settings);

	// Allow to retrieve Scripting interface only when not wrapping swig
	#ifndef SWIG_WRAPPING  
		 static void* GetScriptingInterface(void);;
	#endif

	static bool isInitialized(void);

	/**
	Initialize m_GraphicalInterface

	\param [in] mode Can be graphical or console. In graphical mode, the 
	wxMitkGraphicalInterface member variable will be created and initialized. In
	console mode this variable will not be created.
	\param [in] mainWindow Main window interface
	*/
	static void InitializeGraphicalInterface( 
		Core::Widgets::BaseMainWindow* mainWindow,
		Core::Runtime::AppRunMode mode );

private:
	/**
	\brief Initialize m_DataContainer, 
	DataEntityIORegistration and DataEntityBuildersRegistration
	*/
	static void InitializeData( );

	//! m_ApplicationSettings, wxApp, m_LogManager,
	static void InitializeSettings( 
		std::string appName );

private:			

	class Impl;

	static Impl m_Impl;

	//!
	static bool m_isInitialized;

};

} // namespace Runtime
} // namespace Core

#endif
