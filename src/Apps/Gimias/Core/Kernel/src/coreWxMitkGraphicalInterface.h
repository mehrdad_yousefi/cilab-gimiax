/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreWxMitkGraphicalInterface_H
#define coreWxMitkGraphicalInterface_H

#include "coreObject.h"
#include "coreWindowConfig.h"
#include "dynModuleExecutionImpl.h"

#include <map>
#include <string>

class wxWindow;

namespace Core
{

	class BaseWindowFactory;
	class BaseWindowFactories;
	class BaseWorkingAreaStorage;

// Forward declarations
namespace Exceptions { class Exception; }
namespace Widgets 
{ 
	class SplashScreen;
	class BaseMainWindow;
}

namespace IO
{
	class BaseDataEntityReader;
	class BaseDataEntityWriter;
}

namespace Runtime
{

	class StyleManager;
	class PluginProviderManager;

/** 
\brief This class is the main class in the presentation layer.
It implements the ways to access the Graphical interface done in wxMitk.
The graphical interface has members for interacting with the presentation 
layer. 
It dynamically loads all the plug-ins that are selected for the chosen user 
profile, and provides access to other managers.

\note You should access and get the instance for this class through the 
runtime high-end object: Kernel

\note The main window BaseMainWindow should be the top window and 
the parent of all windows because when this window is destroyed, all other 
windows will be destroyed automatically. Otherwise, the application will not 
finish correctly.

\sa Core::Runtime::Kernel, Core::Runtime::PluginProviderManager

\ingroup gmKernel
\author Juan Antonio Moya
\date 03 Jan 2008
*/

class GMKERNEL_EXPORT wxMitkGraphicalInterface 
	: public Core::SmartPointerObject
{
public:
	coreDeclareSmartPointerClassMacro(
		Core::Runtime::wxMitkGraphicalInterface, 
		Core::SmartPointerObject);
	
	Core::Widgets::BaseMainWindow* GetMainWindow() const;
	Core::Runtime::PluginProviderManager* GetPluginProviderManager() const;
	Core::BaseWindowFactories* GetBaseWindowFactory( ) const;
	Core::BaseWorkingAreaStorage* GetWorkingAreaStorage() const;
	void SetWorkingAreaStorage(BaseWorkingAreaStorage* val);

	void Start( Core::Widgets::BaseMainWindow* mainWindow );
	void ShowSplashScreen(bool show = true);

	void ShowAboutScreen(void);
	void ShowLogFileBrowser(void);
	void ShowProfileManager(void);

	//! Displays \a message in the splash window list of messages, if it is present.
	void LogMessage(const std::string& message);
	void LogException(Core::Exceptions::Exception& e);
	void ReportWarning( const std::string& message,  bool showAlsoDialog = true);
	void ReportError( const std::string& message,  bool showAlsoDialog = true);
	void ReportMessage( const std::string& message, bool showAlsoDialog = true);
	
	bool IsRunning(void);

	//! Create a default plugin tab and set it as the active one
	void CreatePluginTab( const std::string &caption );

	/** Register a widget class that will be used to create an instance and
	add it to the new plugin tabs. Add an instance to all the already created 
	plugin tabs
	*/
	void RegisterFactory( 
		BaseWindowFactory* factory,
		WindowConfig config = WindowConfig( ) );

	//! Unregister a factory
	void UnRegisterFactory( 
		const std::string &factoryName );

	/**
	Register a generic factory to FactoryManager
	This function will automatically keep track of the current plugin name
	and the framework will unload the factory automatically when unloading the plugin
	*/
	void RegisterFactory( 
		const std::string &name, BaseFactory* factory );

	/** Register a reader
	\note This function will automatically keep track of the current plugin name
	and the framework will unload the factory automatically when unloading the plugin
	*/
	void RegisterReader( Core::IO::BaseDataEntityReader* reader );
		
	/** Register a writer
	\note This function will automatically keep track of the current plugin name
	and the framework will unload the factory automatically when unloading the plugin
	*/
	void RegisterWriter( Core::IO::BaseDataEntityWriter* writer );
	
	/** Register a dynModuleExecutionImpl
	\note This function will automatically keep track of the current plugin name
	and the framework will unload the factory automatically when unloading the plugin
	*/
	void RegisterImpl( const std::string &name, dynModuleExecutionImpl::Factory::Pointer );
		
	//! Init the Main Window 
	void InitializeMainWindow( Core::Widgets::BaseMainWindow* mainWindow );

protected:
	wxMitkGraphicalInterface(void);
	~wxMitkGraphicalInterface(void);

	//! Create plugin manager
	void CreatePluginManager( );

	//! Show main window
	void ShowMainWindow( );

	//! Hide splash screen
	void HideSplashScreen( );

	//! Things to do when all is initialized
	void OnGraphicalInterfaceInitialized( );

private:

	/** As long as we're using itk::SmartPointers, it is not possible to 
	define copy-constructors. They're listed here in order to remind it. 
	It is recommended to do the same on child classes
	*/
	coreDeclareNoCopyConstructors(wxMitkGraphicalInterface)

private:
	//! Bridge pattern
	class Impl;
	Impl* m_Impl;
};

} // namespace Runtime
} // namespace Core

#endif
