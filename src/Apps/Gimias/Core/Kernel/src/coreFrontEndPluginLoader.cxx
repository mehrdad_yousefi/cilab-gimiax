/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreFrontEndPluginLoader.h"
#include "coreSettings.h"
#include "coreKernel.h"
#include "dynLib.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreLogger.h"
#include "coreBaseWindowFactories.h"
#include "dynModuleExecution.h"

#include "tinyxml.h"

using namespace Core::Runtime;

FrontEndPluginLoader::FrontEndPluginLoader(void)
{
	m_LibraryHandle = NULL;
	// Create a default unique name
	char name[128];
	sprintf( name, "Plugin%d", m_PluginNameIndex++ );
	m_Name = name;
	m_Caption = m_Name;
}

FrontEndPluginLoader::~FrontEndPluginLoader(void)
{
	CloseLibrary();
}

Core::FrontEndPlugin::FrontEndPlugin::Pointer 
Core::Runtime::FrontEndPluginLoader::GetFrontEndPlugin() 
{
	return m_FrontEndPlugin;
}

void Core::Runtime::FrontEndPluginLoader::SetPath( const std::string path )
{
	m_Path = path;
}

itksys::DynamicLoader::LibraryHandle 
Core::Runtime::FrontEndPluginLoader::GetLibraryHandle() 
{
	return m_LibraryHandle;
}

void Core::Runtime::FrontEndPluginLoader::ResetFrontEndPlugin()
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface; 
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	if ( graphicalIface.IsNotNull( ) && m_FrontEndPlugin.IsNotNull() )
	{
		try
		{
			// Unregister all window factories for the unselected plugin
			BaseWindowFactories::Pointer baseWindowFactory;
			baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );
			std::list<std::string> windowsList = baseWindowFactory->GetFactoryNames( );
			std::list<std::string>::iterator itWin;
			for ( itWin = windowsList.begin( ) ; itWin != windowsList.end() ; itWin++ )
			{
				Core::WindowConfig config;
				if ( ! baseWindowFactory->GetWindowConfig( *itWin, config ) )
				{
					continue;
				}

				if ( config.GetPluginName() == GetName() )
				{
					graphicalIface->UnRegisterFactory( *itWin );
				}
			}

			// Unregister from FactoryManager
			blTagMap::Pointer properties = blTagMap::New( );
			properties->AddTag( "PluginName", GetName( ) );
			Core::FactoryManager::FactoryListType factories = FactoryManager::FindAll( "", properties );
			Core::FactoryManager::FactoryListType::iterator itFactory;
			for ( itFactory = factories.begin( ) ; itFactory != factories.end( ) ; itFactory++ )
			{
				FactoryManager::UnRegister( itFactory->GetPointer( ) );
			}
			// Clean smart pointer reference
			factories.clear( );

			// Readers
			Core::IO::DataEntityReader::UnRegisterPluginReaders( GetName( ) );

			// Writers
			Core::IO::DataEntityWriter::UnRegisterPluginWriters( GetName( ) );

			// dynModuleExecution
			dynModuleExecution::UnRegisterPluginImpl( GetName( ) );

			// Remove plugin tabs
			if ( graphicalIface->GetMainWindow() != NULL )
			{
				std::list<Core::BasePluginTab*> list;
				list = graphicalIface->GetMainWindow()->GetAllPluginTabs();
				std::list<Core::BasePluginTab*>::iterator it;
				for ( it = list.begin() ; it != list.end( ) ; it++ )
				{
					blTag::Pointer tag = (*it)->GetProperties( )->GetTag( "PluginName" );
					if ( tag.IsNull( ) )
					{
						continue;
					}

					if ( tag->GetValueAsString() == GetName( ) )
					{
						graphicalIface->GetMainWindow()->DetachPluginGUI( *it, true );
					}
				}
			}
		}
		catch (...)
		{
		}

	}

	m_FrontEndPlugin = NULL;
}

void Core::Runtime::FrontEndPluginLoader::LoadLibrary()
{
	LoadLibraryHandle( m_LibraryFilename );
}

void Core::Runtime::FrontEndPluginLoader::LoadLibraryHandle( 
	const std::string &filePath )
{
	if ( m_LibraryHandle == NULL )
	{
		if( !(boost::ends_with(filePath, ".dll") || boost::ends_with(filePath, ".so")
			  || boost::ends_with(filePath, ".dylib"))
			  || boost::contains(filePath, "dephelp"))
			return ;

		// Init the Library resource
		m_LibraryHandle = itksys::DynamicLoader::OpenLibrary( filePath.c_str() );

		if (m_LibraryHandle == NULL)
		{
			// Check if the problem is related to missing dependencies
			std::string dllArrayNames;
#ifdef _WIN32
			Core::Runtime::Settings::Pointer settings;
			settings = Core::Runtime::Kernel::GetApplicationSettings();
			dynLib lib;
			lib.CheckMissingDependencies( m_LibraryFilename.c_str(), settings->GetApplicationPath().c_str(), dllArrayNames );
#endif // _WIN32

			if ( !dllArrayNames.empty() )
			{
				Core::Exceptions::OnLoadPluginException eError("FrontEndPluginManager::LoadPlugins");
				eError.Append("Plug-in was: ");
				eError.Append(filePath.c_str());
				eError.Append(" Missing DLLs: ");
				eError.Append( dllArrayNames.c_str() );
				throw eError;
			}
			else
			{
				// If the library was locked already, display an error
				Core::Exceptions::OnLoadPluginObjectAlreadyLockedException eLocked("FrontEndPluginManager::LoadPlugins");
				eLocked.Append("Plug-in was: ");
				eLocked.Append(filePath.c_str());
				throw eLocked;
			}

		}

	}
}

void Core::Runtime::FrontEndPluginLoader::CreateFrontEndPlugin()
{
	if ( m_LibraryHandle == NULL )
		return;

	itksys::DynamicLoader::SymbolPointer symbol;
	FrontEndPluginConstructorFunc pluginConstructor;

	if ( m_FrontEndPlugin.IsNull( ) )
	{

		symbol = itksys::DynamicLoader::GetSymbolAddress(m_LibraryHandle, "newFrontEndPlugin");
		if (symbol != NULL)
		{
			try
			{  
				// Get the Profile object from the library, describing the profile of the plugin
				pluginConstructor = reinterpret_cast<FrontEndPluginConstructorFunc>(symbol);
				m_FrontEndPlugin = pluginConstructor();

				if(m_FrontEndPlugin.IsNull())
				{
					// If the plugin could not be loaded, display error
					Core::Exceptions::OnLoadPluginBadObjectException eBadObj("FrontEndPluginManager::LoadPlugins");
					throw eBadObj;
				}
			}
			catch(...)
			{
				// If the plugin could not be loaded, display error
				Core::Exceptions::OnLoadPluginException eErr("FrontEndPluginManager::LoadPlugins");
				throw eErr;
			}
		}
		else
		{
			// If the symbol was not found, display error
			Core::Exceptions::OnLoadPluginProcedureNotFoundException eNotFound("FrontEndPluginManager::LoadPlugins");
			throw eNotFound;
		}
	}
}

void Core::Runtime::FrontEndPluginLoader::FindLibraryPath()
{
	// If was a directory, build the <pluginName>/lib path to access the shared library
	if(m_Path.compare(".") == 0 && m_Path.compare("..") == 0)
	{
		return;
	}

	std::vector<std::string> modes;
	modes.push_back("Debug");
	modes.push_back("Release");
	// also look in the current folder, for backward compatibility with previous code.
	modes.push_back("."); 

	std::vector<std::string>::const_iterator itMode;
	for( itMode = modes.begin(); itMode != modes.end(); ++itMode )
	{
		std::string pluginSharedLibraryDir;
		pluginSharedLibraryDir = m_Path;
		pluginSharedLibraryDir.append("/lib/" + *itMode );

		// Prepare the dir for grabbing files
		Core::IO::Directory::Pointer dir = Core::IO::Directory::New();
		// Prepare the directory for grabbing files
		dir->GetFilter()->SetMode(Core::IO::DirEntryFilter::FilesOnly);
		dir->GetFilter()->ResetGlobbingExpressions();
		dir->GetFilter()->AddGlobbingExpression("*.dll");
		dir->GetFilter()->AddGlobbingExpression("*.so");
		dir->GetFilter()->AddGlobbingExpression("*.dylib");
		dir->SetDirNameFullPath(pluginSharedLibraryDir);

		Core::IO::FileNameList libraryList;
		libraryList = dir->GetContents();

		// Load all the libraries in the plugins/<pluginName>/lib
		Core::IO::FileNameList::iterator itLib;
		for ( itLib = libraryList.begin(); itLib != libraryList.end(); ++itLib)				
		{
			if( (boost::ends_with(*itLib, ".dll") || boost::ends_with(*itLib, ".so") 
				 || boost::ends_with(*itLib, ".dylib"))
				 && !boost::contains(*itLib, "dephelp"))
			{
				m_LibraryPath = pluginSharedLibraryDir;
				m_LibraryFilename = *itLib;
			}

		} // for each shared library

	} // for each mode
}

void Core::Runtime::FrontEndPluginLoader::LoadXML()
{
	FindLibraryPath( );

	ReadXML();
}

std::string Core::Runtime::FrontEndPluginLoader::GetLibraryFilename() const
{
	return m_LibraryFilename;
}

void Core::Runtime::FrontEndPluginLoader::CloseLibrary()
{
	ResetFrontEndPlugin( );

	// Close shared library handle
	try
	{
		itksys::DynamicLoader::CloseLibrary( m_LibraryHandle );
		m_LibraryHandle = NULL;
	}
	catch(...) { ; }
}
