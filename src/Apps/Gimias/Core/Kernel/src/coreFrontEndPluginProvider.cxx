/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreFrontEndPluginProvider.h"
#include "coreKernel.h"
#include "coreLogger.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreSettings.h"
#include "coreException.h"
#include "coreReportExceptionMacros.h"
#include "coreDirectory.h"
#include "coreFile.h"
#include "coreAssert.h"
#include "coreBaseMainWindow.h"
#include "corePluginProviderManager.h"
#include "coreBaseWindowFactories.h"
#include "coreCommandLinePluginProvider.h"

#include <boost/algorithm/string.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topological_sort.hpp>

using namespace Core::Runtime;
using namespace Core::FrontEndPlugin;

FrontEndPluginProvider::FrontEndPluginProvider(void)
{
	blTagMap::Pointer configuration = blTagMap::New( );
	configuration->AddTag( "Scan folder", std::string( "$(GimiasPath)/plugins" ) );
	GetProperties()->AddTag( "Configuration", configuration );

	// Load Scene view by default
	blTagMap::Pointer plugins = blTagMap::New( );
	plugins->AddTag( "Scene View", true );
	GetProperties()->AddTag( "Plugins", plugins );
	GetProperties()->AddTag( "Priority", 1 );

	m_ModuleFactory = new gmModuleFactory( );
}

FrontEndPluginProvider::~FrontEndPluginProvider(void)
{
}

void FrontEndPluginProvider::PreLoadPluginLibraries(void)
{
	try
	{
		if ( !m_PluginLoader.empty() )
		{
			return;
		}

		// Get the required high level managers
		Core::Runtime::Settings::Pointer settings;
		settings = Core::Runtime::Kernel::GetApplicationSettings();
		coreAssertMacro(settings.IsNotNull() && "The settings manager is not initialized");

		blTagMap::Pointer configuration;
		configuration = GetProperties()->GetTagValue<blTagMap::Pointer>( "Configuration" );

		std::string folder = configuration->GetTag( "Scan folder" )->GetValueAsString();
		settings->ReplaceGimiasPath( folder );
		ScanPluginFolder( folder );

		OrderPluginsDependencies( );

	}
	coreCatchExceptionsAddTraceAndThrowMacro(FrontEndPluginProvider::PreLoadPluginLibraries);
}

void FrontEndPluginProvider::InferAvailableProfilesFromPlugins(void)
{
	try
	{
		blTagMap::Pointer plugins;
		plugins = GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );

		blTagMap::Pointer newPlugins = blTagMap::New( );

		// Add tags for new discovered plugins
		PluginLoaderList::iterator itLib;
		for (itLib = m_PluginLoader.begin();itLib != m_PluginLoader.end();++itLib)				
		{
			blTag::Pointer tag = plugins->GetTag( (*itLib)->GetCaption() );
			if ( tag.IsNull() )
			{
				newPlugins->AddTag( (*itLib)->GetCaption(), false );
			}
			else
			{
				newPlugins->AddTag( tag );
			}
		}
		
		// Replace new plugins tag
		GetProperties()->AddTag( "Plugins", newPlugins );

	}
	coreCatchExceptionsAddTraceAndThrowMacro(FrontEndPluginProvider::InferAvailableProfilesFromPlugins);
}

void FrontEndPluginProvider::LoadPlugins( )
{
	try
	{
		// Add dependencies
		AddPluginsDependencies( );

		// Unload not selected plugins
		UnLoadAllPlugins( );

		blTagMap::Pointer plugins;
		plugins = GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );

		// Set number of outputs. Reset data from UnloadAllPlugins
		SetNumberOfOutputs( 0 );
		SetNumberOfOutputs( m_PluginLoader.size( ) );

		// for each loaded library
		PluginLoaderList::iterator itLib;
		int pluginCount = 0;
		for ( itLib = m_PluginLoader.begin();itLib != m_PluginLoader.end();++itLib)				
		{
			// Create the FrontEndPlugin object from the library
			std::string caption = (*itLib)->GetCaption( );

			// Check if plugin should be loaded
			blTag::Pointer tag = plugins->FindTagByName( caption );
			if ( tag.IsNotNull() && tag->GetValueCasted<bool>() == true && (*itLib)->GetLibraryHandle( ) == NULL )
			{
				try
				{
					(*itLib)->LoadLibrary( );
				}
				catch (Core::Exceptions::Exception& e)
				{
					GetUpdateCallback( )->AddExceptionMessage( e.what( ) );
					GetUpdateCallback( )->Modified( );
					continue;
				}

				// Add this plugin as pending to process
				AddPendingPlugin( (*itLib)->GetName() );

				// Don't wait to process it
				GetOutputPort( pluginCount )->SetWaitPortUpdate( false );

				Core::DataEntity::Pointer dataEntity = Core::DataEntity::New( );
				dataEntity->GetMetadata( )->SetName( GetName( ) );

				// Set clean preview for output DataEntity to clean instances from MITKPlugin class MITKPReview
				dataEntity->SetPreview( NULL );

				// Send UpdateOutput event to observers that will load the plugin
				blTagMap::Pointer pluginInfo = blTagMap::New( );
				pluginInfo->AddTag( (*itLib)->GetName(), true );
				dataEntity->SetTimeStep( pluginInfo );

				// Notify observers
				UpdateOutput( pluginCount, dataEntity );
			}  

			pluginCount++;
		} // for each shared library
	}
	coreCatchExceptionsAddTraceAndThrowMacro(FrontEndPluginProvider::LoadPlugins);

	// Wait until all plugins are processed
	WaitPendingPlugins( );

	// Refresh GUI
	SetNumberOfOutputs( 0 );
}

void FrontEndPluginProvider::LoadPlugin( const std::string &name )
{
	if ( !GetPluginLoader( name ) )
		return;

	try
	{  
		std::string message = "Loading plugin " + name + "\n";
		GetUpdateCallback( )->AddInformationMessage( message );
		GetUpdateCallback( )->Modified( );

		GetPluginLoader( name )->CreateFrontEndPlugin( );
	}
	catch(Core::Exceptions::Exception& e)
	{
		e.AddTrace("FrontEndPluginProvider::LoadPlugins");
		Core::Runtime::Kernel::GetGraphicalInterface()->LogException(e);
		Core::Runtime::Kernel::GetLogManager()->LogException(e);
	}
	catch(...)
	{
		// If the symbol was not found, display error
		Core::Exceptions::CannotReadProfileException eBadProf("FrontEndPluginProvider::LoadPlugins");
		Core::Runtime::Kernel::GetGraphicalInterface()->LogException(eBadProf);
		Core::Runtime::Kernel::GetLogManager()->LogException(eBadProf);
	}

	LoadXMLDescriptions( GetPluginLoader( name )->GetLibraryPath() + "/Filters/" );

	m_LoadedPlugins.push_back( name );
	RemovePendingPlugin( name );
}

void FrontEndPluginProvider::UnLoadPlugin( const std::string &name )
{
	if ( !GetPluginLoader( name ) )
		return;

	try
	{
		std::string message = "UnLoading plugin " + name + "\n";
		GetUpdateCallback( )->AddInformationMessage( message );
		GetUpdateCallback( )->Modified( );


		// Destroy plugin instance
		GetPluginLoader( name )->ResetFrontEndPlugin( );

		// Free memory
		GetPluginLoader( name )->CloseLibrary( );
	}
	catch( ... )
	{
	}

	m_LoadedPlugins.remove( name );
	RemovePendingPlugin( name );
}

void FrontEndPluginProvider::AddPluginsDependencies( )
{
	try
	{

		PluginLoaderList::iterator itLib;
		for ( itLib = m_PluginLoader.begin();itLib != m_PluginLoader.end();++itLib)				
		{
			// Add plugin dependencies
			AddPluginDependencies( *itLib );
		}

		// Add always MITKPlugin for backwards compatibility
		Core::Runtime::Settings::Pointer settings;
		settings = Core::Runtime::Kernel::GetApplicationSettings();

		std::string loadMITKPluginStr;
		settings->GetPluginProperty( "GIMIAS", "LoadMITKPlugin", loadMITKPluginStr );
		bool loadMITKPlugin = loadMITKPluginStr == "false" ? false : true;
		if ( loadMITKPlugin )
		{
			blTagMap::Pointer plugins;
			plugins = GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );
			plugins->AddTag( "MITKPlugin", true );
			if ( settings->GetPerspective() == PERSPECTIVE_PLUGIN )
			{
				plugins->AddTag( "Scene View", true );
			}
		}
	}
	coreCatchExceptionsAddTraceAndThrowMacro(FrontEndPluginProvider::AddPluginsDependencies);
}

void FrontEndPluginProvider::AddPluginDependencies( Core::Runtime::FrontEndPluginLoader::Pointer loader )
{
	blTagMap::Pointer plugins;
	plugins = GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );

	std::string caption = loader->GetCaption( );

	// Grab the plugin's profile and check if matches with the user's profile
	blTag::Pointer tag = plugins->FindTagByName( caption );
	if ( tag.IsNull() || tag->GetValueCasted<bool>() == false )
	{
		return;
	}

	// Enable all depenencies
	std::list<std::string> dependencies = loader->GetDependencies();
	std::list<std::string>::iterator itDepen;
	for ( itDepen = dependencies.begin();itDepen != dependencies.end() ; ++itDepen)				
	{
		Core::Runtime::FrontEndPluginLoader::Pointer loader;
		loader = GetPluginLoader( *itDepen );
		if ( loader.IsNotNull( ) )
		{
			blTag::Pointer dependencyTag = plugins->FindTagByName( loader->GetCaption( ), true );
			if ( dependencyTag.IsNotNull( ) && dependencyTag->GetValueCasted<bool>() == false )
			{
				dependencyTag->SetValue( true );

				std::string message = "Adding dependency..." + loader->GetName() + "\n";
				GetUpdateCallback( )->AddInformationMessage( message );
				GetUpdateCallback( )->Modified( );

				AddPluginDependencies( loader );
			}
		}
	}
}

void FrontEndPluginProvider::UnLoadAllPlugins()
{
	blTagMap::Pointer plugins;
	plugins = GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );

	// Set number of outputs
	SetNumberOfOutputs( 0 );
	SetNumberOfOutputs( m_PluginLoader.size( ) );

	// Unload all plugins
	PluginLoaderList::reverse_iterator it;
	int pluginCount = 0;
	for ( it = m_PluginLoader.rbegin() ; it != m_PluginLoader.rend() ; it++ )
	{
		try
		{
			blTag::Pointer tag = plugins->FindTagByName( (*it)->GetCaption() );
			if ( tag.IsNotNull() && tag->GetValueCasted<bool>() == false && (*it)->GetLibraryHandle( ) != NULL )
			{
				// Add this plugin as pending to process
				AddPendingPlugin( (*it)->GetName() );

				Core::DataEntity::Pointer dataEntity = Core::DataEntity::New( );
				dataEntity->GetMetadata( )->SetName( GetName( ) );

				// Set clean preview for output DataEntity to clean instances from MITKPlugin class MITKPReview
				dataEntity->SetPreview( NULL );

				// Send UpdateOutput event to observers that will load the plugin
				blTagMap::Pointer pluginInfo = blTagMap::New( );
				pluginInfo->AddTag( (*it)->GetName(), false );
				dataEntity->SetTimeStep( pluginInfo );

				UpdateOutput( pluginCount, dataEntity );

			}
		}
		catch(...) { }

		pluginCount++;
	}

}

/**
Dicom plugin will be put as the first plugin
*/
void Core::Runtime::FrontEndPluginProvider::OrderPlugins()
{

	// Put DICOMPlugin at the beginning for backwards compatibility
	PluginLoaderList::iterator itLib;
	for (itLib = m_PluginLoader.begin();itLib != m_PluginLoader.end();++itLib)				
	{
		if ( (*itLib)->GetName( ) == "DICOMPlugin" )
		{
			FrontEndPluginLoader::Pointer loader = (*itLib);
			m_PluginLoader.erase( itLib );
			m_PluginLoader.push_front( loader );
			break;
		}
	}

	// Put MITKPlugin at the beginning for backwards compatibility
	for (itLib = m_PluginLoader.begin();itLib != m_PluginLoader.end();++itLib)				
	{
		if ( (*itLib)->GetName( ) == "MITKPlugin" )
		{
			FrontEndPluginLoader::Pointer loader = (*itLib);
			m_PluginLoader.erase( itLib );
			m_PluginLoader.push_front( loader );
			break;
		}
	}


}

void Core::Runtime::FrontEndPluginProvider::OrderPluginsDependencies()
{
	try
	{

		// Vertex are identified by ints
		// So we need to create associations between plugins and ints
		std::map<std::string, int> pluginIndexMap;
		std::vector<std::string> pluginNamesVector;

		// Fill structures 
		PluginLoaderList::iterator it;
		int pos = 0;
		for ( it = m_PluginLoader.begin();it != m_PluginLoader.end() ; ++it)
		{
			pluginIndexMap[ (*it)->GetName() ] = pos;
			pluginNamesVector.push_back( (*it)->GetName() );
			pos++;
		}

		// Fill plugin dependencies 
		// For each dependency, create an edge
		typedef std::pair<int, int> Edge;
		std::vector<Edge> used_by;
		for ( it = m_PluginLoader.begin();it != m_PluginLoader.end() ; ++it)
		{
			// Fill plugin dependencies
			std::list<std::string> dependencies = (*it)->GetDependencies();
			std::list<std::string>::iterator itDepen;
			for ( itDepen = dependencies.begin();itDepen != dependencies.end() ; ++itDepen)				
			{

				std::map<std::string, int>::iterator itMap;
				itMap = pluginIndexMap.find( *itDepen );
				if ( itMap != pluginIndexMap.end() )
				{
					Edge edge( 
						pluginIndexMap[ *itDepen ], 
						pluginIndexMap[ (*it)->GetName() ] );
					used_by.push_back( edge );
				}
			}
		}

		// Create graph
		typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS> Graph;
		Graph g(used_by.begin(), used_by.end(), m_PluginLoader.size());
		typedef boost::graph_traits<Graph>::vertex_descriptor Vertex;

		// Sort graph
		typedef std::list<Vertex> MakeOrder;
		MakeOrder make_order;
		boost::topological_sort(g, std::front_inserter(make_order));

		// Reorder plugin list
		PluginLoaderList newList;
		for (MakeOrder::iterator i = make_order.begin();
			i != make_order.end(); ++i)
		{
			newList.push_back( GetPluginLoader( pluginNamesVector[ (*i )] ) );
		}
		m_PluginLoader = newList;

		OrderPlugins();

		std::stringstream sstream;
		sstream << "Plugin list: ";
		PluginLoaderList::iterator itLib;
		for (itLib = m_PluginLoader.begin();itLib != m_PluginLoader.end();++itLib)				
		{
			sstream << (*itLib)->GetName( ) << " ";
		}
		sstream << std::endl;
		GetUpdateCallback( )->AddInformationMessage( sstream.str( ) );
		GetUpdateCallback( )->Modified( );

	}
	coreCatchExceptionsLogAndNoThrowMacro(
		FrontEndPluginProvider::OrderPluginsDependencies);

}

Core::Runtime::FrontEndPluginLoader::Pointer 
Core::Runtime::FrontEndPluginProvider::GetPluginLoader( 
	const std::string &pluginName )
{
	PluginLoaderList::iterator it;
	for ( it = m_PluginLoader.begin();it != m_PluginLoader.end() ; ++it)
	{
		if ( (*it)->GetName() == pluginName )
		{
			return (*it);
		}
	}

	return NULL;
}

void Core::Runtime::FrontEndPluginProvider::ScanPluginFolder( const std::string &folder )
{

	// Build the search dir for find plugin libraries (subdirs of ./plugins)
	std::string pluginsDirectories = folder;
	Core::IO::Directory::Pointer dir = Core::IO::Directory::New();
	dir->SetDirNameFullPath(pluginsDirectories);
	dir->GetFilter()->SetMode(Core::IO::DirEntryFilter::SubdirsOnly);
	Core::IO::FileNameList pluginList = dir->GetContents();

	// Report the number of available plugins
	std::stringstream sstream;
	sstream << "Found " << pluginList.size() << " item(s) in plugins directory\n" << std::endl;
	GetUpdateCallback( )->AddInformationMessage( sstream.str( ) );
	GetUpdateCallback( )->Modified( );
	//Core::Runtime::Kernel::GetGraphicalInterface()->LogMessage(message);
	//Core::Runtime::Kernel::GetLogManager()->LogMessage(message);

	// For each directory, try to load XML plugin
	for (Core::IO::FileNameList::iterator it = pluginList.begin(); it != pluginList.end(); ++it) 
	{
		Core::Runtime::FrontEndPluginLoader::Pointer loader;
		loader = Core::Runtime::FrontEndPluginLoader::New( );
		loader->SetPath( (*it) );
		loader->LoadXML( );
		if ( !loader->GetLibraryFilename().empty() )
		{
			// Push the library in the opened shared libraries list for later close
			m_PluginLoader.push_back( loader );
		}

		ScanXMLDescriptions( loader->GetLibraryPath() + "/Filters/" );
	}
}

void Core::Runtime::FrontEndPluginProvider::ScanPlugins()
{
	try
	{
		PreLoadPluginLibraries();
		
		InferAvailableProfilesFromPlugins();
	}
	coreCatchExceptionsReportAndNoThrowMacro(FrontEndPluginProvider::FrontEndPluginProvider)
}

void Core::Runtime::FrontEndPluginProvider::LoadXMLDescriptions( const std::string& path )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	if ( graphicalIface->GetMainWindow() == NULL )
	{
		return;
	}

	// For each XML of ModuleFactory -> Register a factory
	gmModuleFactory* factory = dynamic_cast<gmModuleFactory*> ( m_ModuleFactory );
	ModuleDescriptionMap* modules = factory->GetModuleDescriptionMap( );

	ModuleDescriptionMap::iterator it;
	for ( it = modules->begin() ; it != modules->end() ; it++ )
	{
		ModuleDescription& module = it->second;

		// Avoid modules from other plugins
		if ( module.GetLocation( ).find( path ) == std::string::npos )
		{
			continue;
		}

		// Check if module is related to a Processor
		Core::BaseFactory::Pointer factory;
		factory = FactoryManager::FindByInstanceClassName( module.GetHierarchy() );
		if ( factory.IsNotNull() )
		{
			// Set correct type
			module.SetType( "ProcessorModule" );
			continue;
		}

		// Check if module is associated to a widget or to a DLL ("RawDynLibModule")
		std::string factoryName = graphicalIface->GetMainWindow()->FindFactoryNameByModule( &module );
		if ( !factoryName.empty() )
		{
			module.SetType( "WidgetModule" );
		}

		// Avoid regitering XML files with empty hierarchy. They are just for 
		// showing components in plugin selector
		if ( module.GetType( ) == "RawDynLibModule" && module.GetHierarchy( ).empty( ) )
		{
			continue;
		}

		// Register it
		graphicalIface->GetMainWindow()->RegisterModule( GetName( ), &module );
	}
}

void Core::Runtime::FrontEndPluginProvider::ScanXMLDescriptions( const std::string& path )
{
	// Read the XML filters
	m_ModuleFactory->SetSearchPaths( path.c_str( ) );
	m_ModuleFactory->Scan();
}

std::string Core::Runtime::FrontEndPluginProvider::GetName() const
{
	blTagMap::Pointer configuration;
	configuration = m_Properties->GetTagValue<blTagMap::Pointer>( "Configuration" );

	std::string folder = configuration->GetTag( "Scan folder" )->GetValueAsString();
	return "GIMIAS at " + folder;
}

std::string Core::Runtime::FrontEndPluginProvider::GetPluginName( const std::string &caption )
{
	if ( m_PluginLoader.empty( ) )
	{
		PreLoadPluginLibraries( );
	}

	PluginLoaderList::iterator it;
	for ( it = m_PluginLoader.begin();it != m_PluginLoader.end() ; ++it)
	{
		if ( (*it)->GetCaption( ) == caption )
		{
			return (*it)->GetName( );
		}
	}	

	return caption;
}

Core::Runtime::BasePlugin::Pointer 
Core::Runtime::FrontEndPluginProvider::GetPlugin( const std::string &name )
{
	return GetPluginLoader( name ).GetPointer( );
}

blTagMap::Pointer Core::Runtime::FrontEndPluginProvider::GetVirtualChildPlugins( 
	const std::string &name )
{
	blTagMap::Pointer virtualPlugins = blTagMap::New( );
	if ( GetPluginLoader( name ).IsNull( ) )
		return virtualPlugins;

	gmModuleFactory* factory = dynamic_cast<gmModuleFactory*> ( m_ModuleFactory );
	ModuleDescriptionMap* modules = factory->GetModuleDescriptionMap( );

	// Get plugin path
	std::string pluginPath = GetPluginLoader( name )->GetLibraryPath();

	// Check if plugin is selected
	blTagMap::Pointer plugins;
	plugins = GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );
	std::list<std::string> pluginsList = GetSelectedPlugins( plugins );
	bool isPluginSelected = std::find( pluginsList.begin( ), pluginsList.end( ), name ) != pluginsList.end( );

	// Get all virtual plugins
	ModuleDescriptionMap::iterator it;
	for ( it = modules->begin() ; it != modules->end() ; it++ )
	{
		ModuleDescription& module = it->second;

		// Avoid modules from other plugins
		if ( module.GetLocation( ).find( pluginPath ) == std::string::npos )
		{
			continue;
		}

		// Avoid adding the same plugin name
		if ( module.GetTitle( ) != name )
		{
			virtualPlugins->AddTag( module.GetTitle( ), isPluginSelected );
		}
	}

	return virtualPlugins;
}