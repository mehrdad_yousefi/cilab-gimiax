/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreReportExceptionMacros.h"
#include "coreKernel.h"
#include "coreLogger.h"
#include "coreEnvironment.h"
#include "coreWxMitkGraphicalInterface.h"

void Core::LogExceptionWithLoggerAndGraphicalInterface(Core::Exceptions::Exception& e)
{
	Core::Runtime::Logger::Pointer logger = Core::Runtime::Kernel::GetLogManager();
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface = 
		Core::Runtime::Kernel::GetGraphicalInterface();
	if(logger.IsNotNull()) 
		logger->LogException(e);
	if(graphicalIface.IsNotNull()) 
		graphicalIface->LogException(e);
}

void Core::LogException(Core::Exceptions::Exception& e)
{
	Core::Runtime::Logger::Pointer logger = Core::Runtime::Kernel::GetLogManager();
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface = 
		Core::Runtime::Kernel::GetGraphicalInterface();
	if(logger.IsNotNull()) 
		logger->LogException(e);
	if(graphicalIface.IsNotNull()) 
		graphicalIface->LogMessage(e.what());
}

