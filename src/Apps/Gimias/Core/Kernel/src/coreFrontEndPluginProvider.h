/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#ifndef coreFrontEndPluginProvider_H
#define coreFrontEndPluginProvider_H

#include "gmKernelWin32Header.h"
#include "coreObject.h"
#include "coreFrontEndPluginLoader.h"
#include "corePluginProvider.h"

namespace Core
{
namespace Runtime
{

/**
\brief Loads Front End plugin objects dinamically.

Plugin Provider searches for shared objects (or dll libraries under 
Windows) in the ./plugins/bin directory and tries to load them if they 
share the profile configuration with the current user. 

\ingroup gmKernel
\author Xavi Planes
\date Feb 2011
*/
class GMKERNEL_EXPORT FrontEndPluginProvider : public PluginProvider
{
public:
	typedef std::list<Core::Runtime::FrontEndPluginLoader::Pointer> 
		PluginLoaderList;

	coreDeclareSmartPointerClassMacro( Core::Runtime::FrontEndPluginProvider, PluginProvider);
	coreDefineFactoryClass( FrontEndPluginProvider );
	coreDefineFactoryClassEnd( );

	//!
	std::string GetName( ) const;

	//! Create BaseFrontEndPlugin instances and attach them to GUI
	void LoadPlugins( );

	//! Unload all plugins and detach them from GUI
	void UnLoadAllPlugins();

	/** Load the plugins from the available plugin libraries retrieved from a 
	previous call to PreLoadPluginLibraries.
	*/
	void LoadPlugins( std::list<std::string> pluginsList );

	//!
	Core::Runtime::FrontEndPluginLoader::Pointer GetPluginLoader( 
		const std::string &pluginName); 

	//! Redefined
	virtual BasePlugin::Pointer GetPlugin( const std::string &name );

	//! Redefined
	void ScanPlugins( );

	//! Redefined
	std::string GetPluginName( const std::string &caption );

	//! Redefined
	virtual void LoadPlugin( const std::string &name );

	//! Redefined
	virtual void UnLoadPlugin( const std::string &name );
	
	//! Redefined
	virtual blTagMap::Pointer GetVirtualChildPlugins( const std::string &name );

protected:
	//!
	FrontEndPluginProvider(void);
	//!
	virtual ~FrontEndPluginProvider(void);
	//! Set DICOM plugin at position 0
	void OrderPlugins( );
	//! Compute available profiles from loaded libraries
	void InferAvailableProfilesFromPlugins(void);
	//! Load libraries from folders
	void PreLoadPluginLibraries(void);
	//!
	void OrderPluginsDependencies( );
	//! Add dependent plugins of profile plugins to the userProfile
	void AddPluginsDependencies( );

	//!
	void AddPluginDependencies( Core::Runtime::FrontEndPluginLoader::Pointer );

	//!
	void ScanPluginFolder( const std::string &folder );

	//!
	void ScanXMLDescriptions( const std::string& path );

	//! Load XML files for GIMIAS widgets descriptions
	void LoadXMLDescriptions( const std::string& path );

private:

	//! GIMIAS Plugins
	PluginLoaderList m_PluginLoader;
	//!
	coreDeclareNoCopyConstructors(FrontEndPluginProvider);
};

} // namespace Runtime
} // namespace Core

#endif

