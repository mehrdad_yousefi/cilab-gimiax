/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreBaseWorkingAreaStorage_H
#define _coreBaseWorkingAreaStorage_H

#include "gmKernelWin32Header.h"

namespace Core
{

/**
\brief Singleton class that manages configuration of stored working
areas. 

\ingroup gmWidgets
\author Xavi Planes
\date 30 Aug 2010
*/
class GMKERNEL_EXPORT BaseWorkingAreaStorage : public Core::SmartPointerObject
{
public:

	//!
	coreDeclareSmartPointerTypesMacro(
		Core::BaseWorkingAreaStorage, 
		Core::SmartPointerObject);
	coreClassNameMacro(Core::BaseWorkingAreaStorage);

	//! Scan projectPath + "WorkingAreas" and open all working areas
	virtual void ScanDirectory( const std::string &projectPath ) = 0;

	//! Save a working area
	virtual void Save( BaseWindow* workingArea ) = 0;

	//! Save all working areas and icons to projectHomePath + "WorkingAreas"
	virtual void SaveAll( ) = 0;

	//! Delete a working area
	virtual void Delete( BaseWindow* workingArea ) = 0;

	//!
	virtual std::string GetDefaultWorkingAreaFactoryName( ) = 0;

protected:	

	//!
	BaseWorkingAreaStorage(  ){};

	//!
	~BaseWorkingAreaStorage( ){};

private:
};

} // namespace Core

#endif // _coreBaseWorkingAreaStorage_H
