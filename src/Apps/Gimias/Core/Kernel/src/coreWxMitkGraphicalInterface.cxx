/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreKernel.h"
#include "coreAssert.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreException.h"
#include "coreReportExceptionMacros.h"
#include "coreSettings.h"
#include "coreException.h"
#include "coreWorkflowManager.h"

#include "corePluginProviderManager.h"
#include "coreBaseMainWindow.h"
#include "coreBaseWindowFactories.h"
#include "coreBaseWorkingAreaStorage.h"
#include "coreFactoryManager.h"

#include "dynModuleExecution.h"

using namespace Core::Runtime;

class wxMitkGraphicalInterface::Impl
{
public:
	//! A plugin manager class that loads widgets at runtime
	PluginProviderManager::Pointer m_PluginProviderManager;	
	//! Main Window of the graphical interface is type of (wxWidgets)wxTopLevelWindow
	Core::Widgets::BaseMainWindow* m_MainWindow;
	//! Factory for BaseWindow objects
	Core::BaseWindowFactories::Pointer m_BaseWindowFactory;
	//!
	BaseWorkingAreaStorage::Pointer m_BaseWorkingAreaStorage;
	//! True if application has initialized the graphical interface
	bool isRunning;	
};

//!
wxMitkGraphicalInterface::wxMitkGraphicalInterface(void) 
{
	m_Impl = new Impl( );
	m_Impl->m_MainWindow = NULL;
	m_Impl->isRunning = false;
}

//!
wxMitkGraphicalInterface::~wxMitkGraphicalInterface()
{
	// First clean registered factories
	m_Impl->m_BaseWindowFactory = NULL;
	// Unload all plugins
	m_Impl->m_PluginProviderManager = NULL;
	
	delete m_Impl;
	m_Impl = NULL;
}

void wxMitkGraphicalInterface::Start( Core::Widgets::BaseMainWindow* mainWindow )
{
	
	try
	{
		m_Impl->m_BaseWindowFactory = Core::BaseWindowFactories::New( );

		Core::Runtime::Environment::Pointer runtime = Core::Runtime::Kernel::GetApplicationRuntime();

		// Create the Main Window (it will configure itself)
		InitializeMainWindow( mainWindow );

		// Show splash screen
		if ( !runtime->OnAppExit( ) )
		{
			ShowSplashScreen( true );
		}

		// Create plugin manager
		CreatePluginManager();

		if ( !runtime->OnAppExit( ) )
		{
			OnGraphicalInterfaceInitialized( );
		}
	}
	coreCatchExceptionsAddTraceAndThrowMacro(wxMitkGraphicalInterface::Start);
}

/** Shows the graphical interface to the user and forces its repaint */
void wxMitkGraphicalInterface::ShowSplashScreen(bool show)
{
	if(m_Impl->m_MainWindow != NULL) 
	{
		m_Impl->m_MainWindow->ShowSplashScreen(show);
	}
}

void wxMitkGraphicalInterface::LogMessage(const std::string& message)
{
	try
	{
		if ( GetMainWindow() )
		{
			GetMainWindow()->ReportSplashMessage( message );
		}
	}
	coreCatchExceptionsAddTraceAndThrowMacro(wxMitkGraphicalInterface::LogMessage);
}

/** 
Prompts the Exception to the Logger (superclass does it), and also displays it in the splash window
list of messages of the Splash Screen, if it is present.
*/
void wxMitkGraphicalInterface::LogException(Core::Exceptions::Exception& e)
{
	try
	{
		this->LogMessage(e.what());
		this->ReportMessage(e.what());
	}
	coreCatchExceptionsAddTraceAndThrowMacro(wxMitkGraphicalInterface::LogException);
}

/** Returns true if the application has initialized the graphical interface,
* so it is not running in console mode only
*/
bool wxMitkGraphicalInterface::IsRunning(void)
{
	return m_Impl->isRunning;
}


/** Displays the About Screen of the application */
void wxMitkGraphicalInterface::ShowAboutScreen(void)
{
	try
	{
		if ( GetMainWindow() )
		{
			GetMainWindow()->ShowAboutScreen();
		}
	}
	coreCatchExceptionsCastToNewTypeAndNoThrowMacro(Core::Exceptions::SplashWindowBitmapIsNotSetException, wxMitkGraphicalInterface::wxMitkGraphicalInterface);

}

//!
void wxMitkGraphicalInterface::ShowLogFileBrowser(void)
{
	this->GetMainWindow()->ShowLogFileBrowser();
}

//!
void wxMitkGraphicalInterface::ShowProfileManager(void)
{
	this->GetMainWindow()->ShowProfileManager();
}


/** 
Sends a warning message to be displayed to the user at the information panel, and optionally as a dialog. 
Also formats accordingly the widget that originates the warning.

\param showAlsoDialog set to true by default. It tells the GUI to display a dialog also.
\param message The text to report to the user
\sa ReportMessage, ReportError, m_StyleManager
*/
void wxMitkGraphicalInterface::ReportWarning(const std::string& message, bool showAlsoDialog)
{
	if(m_Impl->m_MainWindow != NULL) 
		m_Impl->m_MainWindow->ReportWarning(message, showAlsoDialog);
}

/** 
Sends a error message to be displayed to the user at the information panel, and optionally as a dialog.
that originates the error.

\param showAlsoDialog set to true by default. It tells the GUI to display a dialog also.
\param message The text to report to the user
\sa ReportMessage, ReportWarning, m_StyleManager
*/
void wxMitkGraphicalInterface::ReportError(const std::string& message, bool showAlsoDialog)
{
	if(m_Impl->m_MainWindow != NULL) 
		m_Impl->m_MainWindow->ReportError(message, showAlsoDialog);
}

/** 
Displays a message to be displayed to the user at the information panel, and optionally as a dialog.
\param showAlsoDialog set to true by default. It tells the GUI to display a dialog also.
\param message The text to report to the user
\sa ReportError, ReportWarning, m_StyleManager
*/
void wxMitkGraphicalInterface::ReportMessage(const std::string& message, bool showAlsoDialog)
{
	if(m_Impl->m_MainWindow != NULL) 
		m_Impl->m_MainWindow->ReportMessage(message, showAlsoDialog);
}

/**
 */
Core::Widgets::BaseMainWindow* Core::Runtime::wxMitkGraphicalInterface::GetMainWindow() const
{
	return m_Impl->m_MainWindow;
}

/**
 */
PluginProviderManager* Core::Runtime::wxMitkGraphicalInterface::GetPluginProviderManager() const
{
	return m_Impl->m_PluginProviderManager;
}

void Core::Runtime::wxMitkGraphicalInterface::CreatePluginManager()
{
	m_Impl->m_PluginProviderManager = PluginProviderManager::New();
	m_Impl->m_PluginProviderManager->RegisterDefaultProviders();
}

void Core::Runtime::wxMitkGraphicalInterface::InitializeMainWindow( 
	Core::Widgets::BaseMainWindow* mainWindow )
{
	m_Impl->m_MainWindow = mainWindow;
	if ( m_Impl->m_MainWindow )
	{
		Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
		std::string title = settings->GetApplicationTitle();
		m_Impl->m_MainWindow->SetTitle(  title );
		m_Impl->m_MainWindow->Initialize();
	}
}

void Core::Runtime::wxMitkGraphicalInterface::ShowMainWindow()
{
	if ( m_Impl->m_MainWindow )
	{
		m_Impl->m_MainWindow->Show();
	}
}

void Core::Runtime::wxMitkGraphicalInterface::HideSplashScreen()
{
	if ( GetMainWindow() )
	{
		GetMainWindow()->ShowSplashScreen( false );
	}
}

void Core::Runtime::wxMitkGraphicalInterface::OnGraphicalInterfaceInitialized()
{
	Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();

	// Save settings
	settings->SaveSettings();

	// Mark it as already initialized and running
	m_Impl->isRunning = true;
	std::string message;
	message = "Graphical interface was initialized";
	this->LogMessage(message);

	if ( m_Impl->m_MainWindow )
	{
		m_Impl->m_MainWindow->OnGraphicalInterfaceInitialized( );
	}
}

void Core::Runtime::wxMitkGraphicalInterface::CreatePluginTab( 
	const std::string &caption )
{
	Core::Runtime::PERSPECTIVE_TYPE perspective;
	perspective = Core::Runtime::Kernel::GetApplicationSettings()->GetPerspective( );

	if ( perspective == PERSPECTIVE_PLUGIN && GetMainWindow( ) )
	{
		GetMainWindow( )->CreatePluginTab( caption );
	}
}

Core::BaseWindowFactories* 
Core::Runtime::wxMitkGraphicalInterface::GetBaseWindowFactory() const
{
	return m_Impl->m_BaseWindowFactory;
}

void Core::Runtime::wxMitkGraphicalInterface::RegisterFactory( 
	BaseWindowFactory* factory, WindowConfig config /*= WindowConfig( ) */ )
{
	if ( GetPluginProviderManager() )
	{
		std::string pluginName = GetPluginProviderManager()->GetCurrentPluginName();
		config.PluginName( pluginName );
	}

	GetBaseWindowFactory( )->RegisterFactory( factory, config );

	if ( GetMainWindow( ) )
	{
		GetMainWindow( )->RegisterWindow( factory->GetNameOfClass(), config );
	}
}

void Core::Runtime::wxMitkGraphicalInterface::UnRegisterFactory( const std::string &factoryName )
{
	GetBaseWindowFactory( )->UnRegisterFactory( factoryName );

	if ( GetMainWindow( ) )
	{
		GetMainWindow( )->UnRegisterWindow( factoryName );
	}
}

void Core::Runtime::wxMitkGraphicalInterface::RegisterFactory( 
	const std::string &name, BaseFactory* factory )
{
	if ( GetPluginProviderManager() )
	{
		std::string pluginName = GetPluginProviderManager()->GetCurrentPluginName();
		factory->GetProperties( )->AddTag( "PluginName", pluginName );
	}

	Core::FactoryManager::Register( name, factory );
}

void Core::Runtime::wxMitkGraphicalInterface::RegisterReader( 
	Core::IO::BaseDataEntityReader* reader )
{
	if ( GetPluginProviderManager() )
	{
		std::string pluginName = GetPluginProviderManager()->GetCurrentPluginName();
		reader->GetProperties( )->AddTag( "PluginName", pluginName );
	}

	Core::IO::DataEntityReader::RegisterFormatReader( reader );
}
	
void Core::Runtime::wxMitkGraphicalInterface::RegisterWriter( 
	Core::IO::BaseDataEntityWriter* writer )
{
	if ( GetPluginProviderManager() )
	{
		std::string pluginName = GetPluginProviderManager()->GetCurrentPluginName();
		writer->GetProperties( )->AddTag( "PluginName", pluginName );
	}

	Core::IO::DataEntityWriter::RegisterFormatWriter( writer );
}

void Core::Runtime::wxMitkGraphicalInterface::RegisterImpl( 
	const std::string &name, dynModuleExecutionImpl::Factory::Pointer factory )
{
	if ( GetPluginProviderManager() )
	{
		std::string pluginName = GetPluginProviderManager()->GetCurrentPluginName();
		factory->GetProperties( )->AddTag( "PluginName", pluginName );
	}
	
	dynModuleExecution::RegisterImpl( name, factory );
}
	

Core::BaseWorkingAreaStorage* Core::Runtime::wxMitkGraphicalInterface::GetWorkingAreaStorage() const
{
	return m_Impl->m_BaseWorkingAreaStorage;
}

void Core::Runtime::wxMitkGraphicalInterface::SetWorkingAreaStorage( BaseWorkingAreaStorage* val )
{
	m_Impl->m_BaseWorkingAreaStorage = val;
}

