/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreLoggerHelper.h"
#include "coreKernel.h"
#include "coreReportExceptionMacros.h"
#include "coreWxMitkGraphicalInterface.h"

Core::LoggerHelper::~LoggerHelper()
{

}

void Core::LoggerHelper::ShowMessage( 
						std::string strMessage,
						MessageType type,
						bool showAlsoDialog )
{
	try
	{
		Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalInterface;
		graphicalInterface = Core::Runtime::Kernel::GetGraphicalInterface();

		if ( graphicalInterface.IsNull() )
		{
			return;
		}

		switch ( type )
		{
		case MESSAGE_TYPE_INFO:
			graphicalInterface->ReportMessage( strMessage, showAlsoDialog );
			break;
		case MESSAGE_TYPE_WARNING:
			graphicalInterface->ReportWarning( strMessage, showAlsoDialog );
			break;
		case MESSAGE_TYPE_ERROR:
			graphicalInterface->ReportError( strMessage, showAlsoDialog );
			break;
		}

	}coreCatchExceptionsReportAndNoThrowMacro( 
		"Core::LoggerHelper::ShowMessage" );
}


