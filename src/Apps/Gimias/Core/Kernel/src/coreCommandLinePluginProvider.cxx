/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreCommandLinePluginProvider.h"
#include "coreKernel.h"
#include "coreLogger.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreSettings.h"
#include "coreException.h"
#include "coreReportExceptionMacros.h"
#include "coreDirectory.h"
#include "coreFile.h"
#include "coreAssert.h"
#include "coreBaseMainWindow.h"
#include "coreBaseWindowFactories.h"

using namespace Core::Runtime;

gmModuleFactory::gmModuleFactory( Core::BaseProcessor* processor /*= NULL*/ )
{
	m_Processor = processor;
}

ModuleDescriptionMap *gmModuleFactory::GetModuleDescriptionMap( )
{
	return this->InternalMap;
}

void gmModuleFactory::WarningMessage( const char *msg)
{
	if ( msg && std::string(msg).find( "in cache" ) == std::string::npos && m_Processor)
	{
		std::string message = std::string( msg ) + "\n";
		m_Processor->GetUpdateCallback( )->AddInformationMessage( message );
		m_Processor->GetUpdateCallback( )->Modified( );
	}
}

void gmModuleFactory::ErrorMessage( const char *msg )
{
	if ( msg && std::string(msg).find( "in cache" ) == std::string::npos && m_Processor)
	{
		std::string message = std::string( msg ) + "\n";
		m_Processor->GetUpdateCallback( )->AddInformationMessage( message );
		m_Processor->GetUpdateCallback( )->Modified( );
	}
}

void gmModuleFactory::InformationMessage( const char *msg)
{
	if ( msg && std::string(msg).find( "in cache" ) == std::string::npos && m_Processor)
	{
		//std::string message = std::string( msg ) + "\n";
		//m_Processor->GetUpdateCallback( )->AddInformationMessage( message );
		//m_Processor->GetUpdateCallback( )->Modified( );
	}
}

void gmModuleFactory::ModuleDiscoveryMessage( const char *msg)
{
	if ( msg && std::string(msg).find( "in cache" ) == std::string::npos && m_Processor)
	{
		std::string message = std::string( msg ) + "\n";
		m_Processor->GetUpdateCallback( )->AddInformationMessage( message );
		m_Processor->GetUpdateCallback( )->Modified( );
	}
}


CommandLinePluginProvider::CommandLinePluginProvider(void)
{
	m_ModuleFactory = new gmModuleFactory( this );

#ifdef _WIN32
		std::string delim(";");
#else
		std::string delim(":");
#endif

	blTagMap::Pointer configuration = blTagMap::New( );
	std::string defaultPaths;
	// Don't add delim at the end for backwards compatibility
	defaultPaths = std::string( "$(GimiasPath)/commandLinePlugins" );
	configuration->AddTag( "Scan folder", defaultPaths );
	// On Windows, we should use GimiasPath to use dependent DLLs
	configuration->AddTag( "Working directory", std::string( "$(GimiasPath)" ) );
	// On Windows 7, we should use APPDATA to write application data
	configuration->AddTag( "Temporary directory", std::string( "$(AppData)" ) );
	GetProperties()->AddTag( "Configuration", configuration );
	GetProperties()->AddTag( "Plugins", blTagMap::New( ) );
	GetProperties()->AddTag( "Priority", 5 );
}

/** Destructor for the class CommandLinePluginProvider. */
CommandLinePluginProvider::~CommandLinePluginProvider(void)
{
	if ( m_ModuleFactory )
	{
		delete m_ModuleFactory;
		m_ModuleFactory = NULL;
	}
}

void CommandLinePluginProvider::LoadPlugins( )
{
	try
	{
		Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
		graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
		if ( graphicalIface.IsNull( ) || graphicalIface->GetMainWindow() == NULL )
			return;

		blTagMap::Pointer plugins;
		plugins = GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );

		// For each XML of ModuleFactory -> Register a factory
		gmModuleFactory* factory = dynamic_cast<gmModuleFactory*> ( m_ModuleFactory );
		ModuleDescriptionMap* modules = factory->GetModuleDescriptionMap( );

		// Update number of outputs
		SetNumberOfOutputs( modules->size( ) );

		ModuleDescriptionMap::iterator it;
		int pluginCount = 0;
		for ( it = modules->begin() ; it != modules->end() ; it++ )
		{
			ModuleDescription& module = it->second;

			// Find module
			bool moduleIsLoaded = false;
			std::string factoryName = graphicalIface->GetMainWindow()->FindFactoryNameByModule( &module );
			if ( !factoryName.empty() )
			{
				Core::BaseWindowFactory::Pointer factory;
				factory = graphicalIface->GetBaseWindowFactory( )->FindFactory( factoryName );
				if ( factory->FindModule( GetName() ) )
				{
					moduleIsLoaded = true;
				}

			}

			// Load / Unload
			blTag::Pointer tag = plugins->FindTagByName( module.GetTitle( ) );
			// If category has the same name as a plugin -> search inside the category tag
			if ( tag.IsNotNull() && tag->GetTypeName( ) != blTag::GetTypeName( typeid( bool ) ) )
			{
				tag = tag->GetValueCasted<blTagMap::Pointer>( )->FindTagByName( module.GetTitle( ) );
			}

			if ( tag.IsNotNull() && tag->GetValueCasted<bool>() == true && !moduleIsLoaded )
			{

				std::string message = "Loading plugin " + module.GetTitle( ) + "\n";
				GetUpdateCallback( )->AddInformationMessage( message );
				GetUpdateCallback( )->Modified( );

				blTagMap::Pointer pluginInfo = blTagMap::New( );
				pluginInfo->AddTag( module.GetTitle( ), true );
				GetOutputPort( pluginCount )->SetWaitPortUpdate( false );
				UpdateOutput( pluginCount, pluginInfo, GetName( ) );
			}
			else if ( tag.IsNotNull() && tag->GetValueCasted<bool>() == false && moduleIsLoaded )
			{
				std::string message = "UnLoading plugin " + module.GetTitle( ) + "\n";
				GetUpdateCallback( )->AddInformationMessage( message );
				GetUpdateCallback( )->Modified( );

				blTagMap::Pointer pluginInfo = blTagMap::New( );
				pluginInfo->AddTag( module.GetTitle( ), false );
				UpdateOutput( pluginCount, pluginInfo, GetName( ) );

				m_LoadedPlugins.remove( module.GetTitle( ) );
			}
			
			pluginCount++;
		}
	}
	coreCatchExceptionsAddTraceAndThrowMacro(CommandLinePluginProvider::LoadPlugins);

	SetNumberOfOutputs( 0 );
}

void Core::Runtime::CommandLinePluginProvider::LoadPlugin( const std::string &name )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	// Find module description
	gmModuleFactory* factory = dynamic_cast<gmModuleFactory*> ( m_ModuleFactory );
	ModuleDescriptionMap* modules = factory->GetModuleDescriptionMap( );
	ModuleDescriptionMap::iterator itModule = modules->find( name );
	if ( itModule == modules->end( ) )
		return;

	// Register it
	graphicalIface->GetMainWindow()->RegisterModule( GetName(), &itModule->second );

	m_LoadedPlugins.push_back( name );
}

void Core::Runtime::CommandLinePluginProvider::UnLoadPlugin( const std::string &name )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	// Find module description
	gmModuleFactory* factory = dynamic_cast<gmModuleFactory*> ( m_ModuleFactory );
	ModuleDescriptionMap* modules = factory->GetModuleDescriptionMap( );
	ModuleDescriptionMap::iterator itModule = modules->find( name );
	if ( itModule == modules->end( ) )
		return;

	graphicalIface->GetMainWindow()->UnRegisterModule( GetName(), &itModule->second );

	m_LoadedPlugins.remove( name );
}

void Core::Runtime::CommandLinePluginProvider::ScanPlugins()
{

	try
	{
		// Clear the collected profile from the plugins and set it to the default
		blTagMap::Pointer plugins;
		plugins = GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );

		if ( m_ModuleFactory )
		{
			delete m_ModuleFactory;
			m_ModuleFactory = NULL;
		}
		m_ModuleFactory = new gmModuleFactory( this );

		Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
#ifdef _WIN32
		std::string delim(";");
#else
		std::string delim(":");
#endif

		blTagMap::Pointer configuration;
		configuration = GetProperties()->GetTagValue<blTagMap::Pointer>( "Configuration" );

		// Scan folders
		std::string folder = configuration->GetTag( "Scan folder" )->GetValueAsString();

		// Split folder list into components and create a new scan folders replacing some variables
		std::list<std::string> inputScanFolders;
		blTextUtils::ParseLine( folder, delim[ 0 ], inputScanFolders );
		std::list<std::string>::iterator it;
		std::string scanPaths;
		for ( it = inputScanFolders.begin( ) ; it != inputScanFolders.end( ) ; it++ )
		{
			std::string folder = *it;
			settings->ReplaceGimiasPath( folder );
			scanPaths = scanPaths + folder + delim;

			// If this folder contains a GimiasPath -> Add debug and release paths too
			if ( it->find( "$(GimiasPath)" ) != std::string::npos )
			{
				scanPaths = scanPaths + folder + "/debug" + delim;
				scanPaths = scanPaths + folder + "/release" + delim;
			}
		}
		m_ModuleFactory->SetSearchPaths( scanPaths  );

		// Working folder
		folder = configuration->GetTag( "Working directory" )->GetValueAsString();
		settings->ReplaceGimiasPath( folder );
		m_ModuleFactory->SetWorkingDirectory( folder );

		folder = configuration->GetTag( "Temporary directory" )->GetValueAsString();
		settings->ReplaceGimiasPath( folder );
		m_ModuleFactory->SetCachePath( folder + "/CLPCache" );

		Core::IO::Directory::Pointer cachePath = Core::IO::Directory::New( );
		cachePath->SetDirNameFullPath( m_ModuleFactory->GetCachePath() );
		cachePath->Create( );

		// Scan
		m_ModuleFactory->Scan();

		blTagMap::Pointer newPlugins = blTagMap::New( );
		gmModuleFactory* factory = dynamic_cast<gmModuleFactory*> ( m_ModuleFactory );
		ModuleDescriptionMap* modules = factory->GetModuleDescriptionMap( );
		ModuleDescriptionMap::iterator itModule;
		for ( itModule = modules->begin() ; itModule != modules->end() ; itModule++ )
		{
			ModuleDescription& module = itModule->second;

			bool selected = false;
			if ( plugins->FindTagByName( itModule->first ).IsNotNull() )
			{
				selected = plugins->FindTagByName( itModule->first )->GetValueCasted<bool>( );
			}
			
			// Overwrite type
			if ( !module.GetExecutableApp( ).empty( ) )
			{
				module.SetType( "ExternalApp" );
			}

			// Add tags for categories
			blTag::Pointer categoryTag;
			blTagMap::Pointer categoryTagMap;
			std::string category = module.GetCategory();
			std::list<std::string> words;
			blTextUtils::ParseLine( category, '.', words );
			std::list<std::string>::iterator it;
			categoryTagMap = newPlugins;
			for ( it = words.begin() ; it != words.end() ; it++ )
			{
				categoryTag = categoryTagMap->GetTag( *it );
				if ( categoryTag.IsNull() )
				{
					categoryTag = blTag::New( *it, blTagMap::New( ) );
				}
				categoryTagMap->AddTag( categoryTag );
				categoryTagMap = categoryTag->GetValueCasted<blTagMap::Pointer>( );
			}

			// Add tag for module
			categoryTagMap->AddTag( itModule->first, selected );
		}

		GetProperties()->AddTag( "Plugins", newPlugins );

	}
	coreCatchExceptionsAddTraceAndThrowMacro(CommandLinePluginProvider::ScanPlugins);
}

std::string Core::Runtime::CommandLinePluginProvider::GetName() const
{
	blTagMap::Pointer configuration;
	configuration = m_Properties->GetTagValue<blTagMap::Pointer>( "Configuration" );

	std::string folder = configuration->GetTag( "Scan folder" )->GetValueAsString();
	return "Command Line at " + folder;
}