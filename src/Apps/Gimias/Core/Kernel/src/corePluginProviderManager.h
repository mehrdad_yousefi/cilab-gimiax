/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#ifndef coreFrontEndPluginManager_H
#define coreFrontEndPluginManager_H

#include "gmKernelWin32Header.h"
#include "coreObject.h"
#include "corePluginProvider.h"
#include "corePluginProvidersUpdater.h"

namespace Core
{
namespace Runtime
{

/**
\brief Manager for all PluginProviders

Singleton class

Creates all instances of PluginProvider and executes them

\ingroup gmKernel
\author Xavi Planes
\date Feb 2011
*/
class GMKERNEL_EXPORT PluginProviderManager : public Core::SmartPointerObject
{
public:

	enum STATE {
		STATE_IDLE,
		STATE_LOADING_PLUGINS
	};

	typedef std::list<PluginProvider::Pointer> PluginProvidersType;

	coreDeclareSmartPointerClassMacro(
		Core::Runtime::PluginProviderManager, 
		Core::SmartPointerObject);

	//! If name is empty, scan for all providers
	void ScanPlugins( const std::string name = "", bool multithreading = false );

	/** Load plugins matching user profile or active workflow
	depending on the settings.
	If name is empty, scan for all providers.
	*/
	void LoadPlugins( const std::string name = "", bool multithreading = false );

	//!
	void UpdateProviders( const std::string name = "", bool multithreading = false );

	//! The current plugin name being created
	std::string GetCurrentPluginName() const;
	void SetCurrentPluginName(const std::string &name);

	//!
	PluginProvidersType GetPluginProviders() const;

	//!
	PluginProvider::Pointer GetPluginProvider( std::string name ) const;

	/** Add a provider 
	\note If provider is found, don't do anything 
	*/
	void Add( PluginProvider* provider );

	//!
	void Remove( PluginProvider* provider );

	//!
	void RemoveAll( );

	//! Register CommandLinePluginProvider and FrontEndPluginProvider
	void RegisterDefaultProviders( );

	//! Add FrontEndPluginProvider and CommandLinePluginProvider
	void AddDefaultProviders( );

	/** Read settings, register/unregister all providers
	\param [in] restoreConfiguration Should be set to true to restore plugin provider configuration from disk
	*/
	void LoadConfiguration( bool restoreConfiguration );

	/** Save properties of all plugin providers into application settings.
	This saves the configuration of selected plugins
	*/
	void SaveConfiguration( );

	//! Get state of this manager
	Core::DataHolder<STATE>::Pointer GetStateHolder( );

	/** Get list of plugins that will be unloaded, checking 
	configuration of each provider and currently loaded plugins
	*/
	std::list<std::string> GetPluginsToUnload( const std::string providerName = "" );

	/** Order providers using "PluginProviders" tag from settings and "Priority" tag
	\return a list of provider names
	*/
	std::list<std::string> OrderProvidersByPriorityTag( );

	//!
	PluginProvidersUpdater::Pointer GetProvidersUpdater( );

protected:
	//!
	PluginProviderManager(void);
	//!
	virtual ~PluginProviderManager(void);
	/**
	Unload plugin providers before unloading plugins.
	Some FonrtEndPlugins like SSHPlugin will register a new plugin proivder
	that needs to be removed before unloading the SSHPlugin
	*/
	void UnloadProviders( const std::string providerName = "" );

private:
	//!
	coreDeclareNoCopyConstructors(PluginProviderManager);

private:

	//!
	PluginProvidersType m_PluginProviders;

	//!
	Core::DataHolder<STATE>::Pointer m_StateHolder;

	//!
	PluginProvidersUpdater::Pointer m_PluginProvidersUpdater;
};

} // namespace Runtime
} // namespace Core

#endif

