/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#ifndef _coreBaseWindow_H
#define _coreBaseWindow_H

#include "gmKernelWin32Header.h"
#include "coreRenderingTree.h"
#include "coreBaseProcessor.h"
#include "coreProcessorOutputObserver.h"
#include "coreCommonDataTypes.h"
#include "coreRenderingTreeManager.h"
#include "coreBaseWindowFactory.h"
#include "blTagMap.h"
#include "coreWindowConfig.h"

class ModuleDescription;

namespace Core
{

	namespace Widgets
	{
		class UserHelper;
		class RenderWindowBase;
		class LandmarkSelectorWidget;
		class DataEntityListBrowser;
		class ProcessorInputWidget;
		class PluginTab;
	}

/**
\brief Base Interface for all Gimias Windows. 

It has some basic functions used by all the windows, to be able to 
reuse the code. The windows needs to be registered to Gimias, in order to get
benefit from this generic interface.

- Update the active RenderingTree
- Update the active RenderWindowBase
- Store a pointer to several plugin widgets

\ingroup gmKernel
\author Xavi Planes
\date 1 Mar 2010
*/
class GMKERNEL_EXPORT BaseWindow : public Core::Object
{
public:

	typedef Core::DataHolder<blTagMap::Pointer> MetadataHolderType;

	//!
	virtual Core::BaseProcessor::Pointer GetProcessor( );

	/** Set and Get properties of the window to serialize them
	or visualize in the GUI. Is responsibility of the subclass to 
	put the properties in m_MetadataHolder
	*/
	virtual void SetProperties( blTagMap::Pointer tagMap );
	virtual void GetProperties( blTagMap::Pointer tagMap );

	//! Metadata information of this window. 
	MetadataHolderType::Pointer GetMetadataHolder() const;
	blTagMap::Pointer GetMetadata() const;

	//! 
	virtual void SetRenderingTree( RenderingTree::Pointer tree );
	virtual RenderingTree::Pointer GetRenderingTree() const;

	//! 
	Core::Widgets::UserHelper * GetHelperWidget() const;
	virtual void SetHelperWidget(Core::Widgets::UserHelper * val);

	//!
	Core::Widgets::RenderWindowBase* GetMultiRenderWindow() const;
	virtual void SetMultiRenderWindow(Core::Widgets::RenderWindowBase* val);

	//!
	Core::Widgets::DataEntityListBrowser* GetListBrowser() const;
	virtual void SetListBrowser(Core::Widgets::DataEntityListBrowser* val);

	//!
	Core::Widgets::ProcessorInputWidget* GetInputWidget( int num );
	virtual void SetInputWidget( int num, Core::Widgets::ProcessorInputWidget* widget );
	int GetNumInputWidget();

	//!
	Core::ProcessorOutputObserver::Pointer GetProcessorOutputObserver( int num );

	//!
	Core::Widgets::PluginTab* GetPluginTab() const;
	virtual void SetPluginTab(Core::Widgets::PluginTab* val);

	/** Init observers to output processor data objects
	\param [in] enableDefaultObservers Enable ProcessorOutputObserver
	*/
	virtual void InitProcessorObservers( bool enableDefaultObservers );

	//!
	virtual int GetTimeStep() const;
	virtual void SetTimeStep( int time );
	virtual void SetTimeStepHolder(Core::IntHolderType::Pointer val);

	//!
	void SetBitmap(const char* const* data);
	const char* const* GetBitmap() const;

	//!
	std::string GetBitmapFilename() const;
	void SetBitmapFilename(std::string val);

	//!
	void Init( );

	//!
	std::string GetFactoryName() const;
	void SetFactoryName(std::string val);

	//!
	void SetModule( ModuleDescription* module );
	ModuleDescription* GetModule( );

	/**
	Connect outputs from processor of widget to current processor inputs.
	Iterate over all inputs and get the pointer to the output DataEntity.
	*/
	virtual void ConnectProcessor( Core::BaseWindow* baseWindow );

	//! Get child window using window id.
	virtual BaseWindow* GetChildWindow( int id );

	//! Get child window using name.
	virtual BaseWindow* GetChildWindow( const std::string &name );

	//! Get child window using factory name.
	virtual BaseWindow* GetChildWindowByFactory( const std::string &factoryname );

	//! Get all children windows
	virtual std::list<BaseWindow*> GetChildren( );

	//! Is this window is a container window?
	bool GetIsContainerWindow( );
	//! Add an observer to BaseWindowFactories
	void SetIsContainerWindow( bool val );

	//! Type of child windows BaseWindowFactories
	WIDGET_TYPE GetChildWindowType( );
	void SetChildWindowType( WIDGET_TYPE type );

	//!
	BaseWindow* GetParentWindow( );

	//! Show a specific child window
	virtual void ShowChild( BaseWindow* child, bool show = true );

protected:
	//!
	BaseWindow( );

	//!
	virtual ~BaseWindow( );

	//! Called when BaseWindow is initialized
	virtual void OnInit();

	//! Callback function called when input processor is new or modified
	virtual void OnModifiedInput( int num );

	//! Callback function called when output processor is new or modified
	virtual void OnModifiedOutput( int num );

	//! Callback function called when input processor is new
	virtual void OnNewInput( int num );

	//! Callback function called when output processor is new
	virtual void OnNewOutput( int num );

	//!
	virtual void OnModifiedMetadata( );

	/** Observer of BaseWindowFactories
	Search factories with the type m_ChildWindowsType and calls 
	CreateChild() and DestroyChild( )
	*/
	virtual void UpdateRegisteredWindows();

	//! Create a child window. To be redefined for container windows
	virtual BaseWindow* CreateChild( const std::string &factoryName );

	//! Destroy a child window. To be redefined for container windows
	virtual void DestroyChild( BaseWindow* win );

	/** Search registered windows that satisfy the criteria to be a child
	in BaseWindowFactories
	*/
	virtual std::list<std::string> SearchRegisteredChildWindows( );

	//! Add child to m_Children
	void AddChild( BaseWindow* baseWin );

	//! Remove child from m_Children
	void RemoveChild( BaseWindow* baseWin );

	//!
	void SetParentWindow( BaseWindow* baseWin );

private:
	//! Current rendering tree to render objects
	RenderingTree::Pointer m_RenderingTree;

	//! User helper
	Core::Widgets::UserHelper *m_HelperWidget;

	//!
	Core::Widgets::RenderWindowBase* m_MultiRenderWindow;

	//!
	Core::Widgets::DataEntityListBrowser* m_ListBrowser;

	//!
	Core::Widgets::PluginTab* m_PluginTab;

	//!
	std::vector<Core::ProcessorOutputObserver::Pointer> m_ProcessorOutputObserverVector;

	//!
	std::map<int, Core::Widgets::ProcessorInputWidget*> m_ProcessorInputWidgetMap;

	//!
	Core::IntHolderType::Pointer m_TimeStepHolder;

	//! Bitmap in xpm format for the toolbar
	boost::any m_Bitmap_xpm;

	//! Factory name used to create this widget
	std::string m_FactoryName;

	//! Filename of the bitmap used for the toolbar
	std::string m_BitmapFilename;

	//!
	ModuleDescription* m_Module;

	//! Set of generic properties
	MetadataHolderType::Pointer m_MetadataHolder;

	//! This window has child windows and has observer to BaseWindowFactories
	bool m_IsContainerWindow;

	//! Type of child windows
	WIDGET_TYPE m_ChildWindowType;

	//! Parent window
	BaseWindow* m_ParentWindow;

	//! Name of the child windows
	std::list<BaseWindow*> m_Children;
};

} // namespace Core

#endif // _coreBaseWindow_H

