/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreSessionReader_H
#define coreSessionReader_H


#include "gmKernelWin32Header.h"
#include "coreSession.h"
#include "coreObject.h"
#include "coreBaseDataEntityReader.h"


namespace Core 
{
namespace IO 
{

/** 
\brief The SessionReader will read all data into Core::DataList that was saved
in a previous session

\ingroup gmKernel
\author Jakub Lyko
\date 10 Dec 2009
*/
class GMKERNEL_EXPORT SessionReader : public BaseDataEntityReader
{
public:
	coreDeclareSmartPointerClassMacro(Core::IO::SessionReader, BaseDataEntityReader);	

	//!
	bool GetRemoveAllData() const;
	void SetRemoveAllData(bool val);

	//! Overwriten
	virtual void Update( );

	//! 
	void ReadData( );

protected:
	//! 
	SessionReader();

	//! 
	virtual ~SessionReader();

private:
	/**
	\brief The method first creates a DataEntity from gettingn necessary
	information from LightData, and then adds it to core DataList
	*/
	void LoadLightData( blTagMap::Pointer data, Core::DataEntity::Pointer father = NULL );

	//! Convert old session format to blTagMap
	blTagMap::Pointer ConvertSession( Core::LightData* lightData );

	//!
	void LoadViews( Core::DataEntity::Pointer sessionNode );

private:
	//! Remove all data before loading session
	bool m_RemoveAllData;
private:
	coreDeclareNoCopyConstructors(SessionReader);
	//!
	std::list<Core::DataEntity::Pointer> m_DataList;
};

} // namespace IO
} // namespace Core

#endif
