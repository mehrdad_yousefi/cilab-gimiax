/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreBaseWindowFactorySearch.h"
#include "coreBaseWindowFactories.h"
#include "coreKernel.h"
#include "coreWxMitkGraphicalInterface.h"

Core::BaseWindowFactorySearch::BaseWindowFactorySearch()
{
	m_Type = WIDGET_TYPE_MAX;
	m_Caption = "";
	m_Category = "";
	m_Id = -1;
	m_State = WIDGET_STATE_MAX;

}

void Core::BaseWindowFactorySearch::Update( )
{
	m_FactoriesNames.clear();

	BaseWindowFactories::FactoriesListType factoryList;
	factoryList = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( )->GetFactoryNames( );

	BaseWindowFactories::FactoriesListType::iterator it;
	for ( it = factoryList.begin( ) ; it != factoryList.end() ; it++ )
	{
		WindowConfig config;
		Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( )->GetWindowConfig( *it, config );
		bool hasFlag = config.HasFlag( m_State ) || m_State == WIDGET_STATE_MAX;
		bool hasType = config.HasType( m_Type ) || m_Type == WIDGET_TYPE_MAX;
		bool matchesCategory = m_Category == config.GetCategory() || m_Category.empty();
		bool matchesCaption = m_Caption == config.GetCaption() || m_Caption.empty();
		bool matchesID = m_Id == -1 || m_Id == config.GetId();

		bool matchesInstanceClassName = true;
		if ( !m_InstanceClassName.empty() )
		{
			BaseWindowFactory::Pointer factory;
			factory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( )->FindFactory( *it );
			if ( factory.IsNotNull() && factory->GetInstanceClassName() )
			{
				std::string classname = factory->GetInstanceClassName();
				matchesInstanceClassName = classname == m_InstanceClassName;
			}
		}

		if ( hasFlag && hasType && matchesCategory && matchesCaption && matchesID && matchesInstanceClassName )
		{
			m_FactoriesNames.push_back( *it );
		}
	}
}

std::list<std::string> Core::BaseWindowFactorySearch::GetFactoriesNames() const
{
	return m_FactoriesNames;
}

void Core::BaseWindowFactorySearch::SetType( WIDGET_TYPE val )
{
	m_Type = val;
}

void Core::BaseWindowFactorySearch::SetCaption( std::string val )
{
	m_Caption = val;
}

void Core::BaseWindowFactorySearch::SetCategory( std::string val )
{
	m_Category = val;
}

void Core::BaseWindowFactorySearch::SetState( WIDGET_STATE val )
{
	m_State = val;
}

void Core::BaseWindowFactorySearch::SetId( int val )
{
	m_Id = val;
}

void Core::BaseWindowFactorySearch::SetInstanceClassName( std::string val )
{
	m_InstanceClassName = val;
}


