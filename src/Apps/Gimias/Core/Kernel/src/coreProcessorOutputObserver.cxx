/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreProcessorOutputObserver.h"

#include "coreReportExceptionMacros.h"
#include "coreDataContainer.h"
#include "coreKernel.h"

Core::ProcessorOutputObserver::ProcessorOutputObserver()
{
	m_OutputNumber = 0;
	m_hideInput = true;
	m_HideInputNumber = 0;
	m_InitializeViews = true;
	m_SetSelected = true;
	m_AddToDataList = true;
	m_AddToRederingTree = true;
	m_opacityInput = false;
	m_opacityValue = 1;
	m_PropertyList = blTagMap::New();
	m_Enable = true;
}

Core::ProcessorOutputObserver::~ProcessorOutputObserver()
{
	RemoveObservers( );
}

void Core::ProcessorOutputObserver::SetProcessor( Core::BaseFilter::Pointer val )
{
	RemoveObservers( );
	
	m_Processor = val;

	AddObservers();
}

void Core::ProcessorOutputObserver::SetRenderingTree( Core::RenderingTree::Pointer val )
{
	m_RenderingTree = val;
}

void Core::ProcessorOutputObserver::AddObservers()
{
	m_Processor->GetOutputDataEntityHolder( m_OutputNumber )->AddObserver( 
		this, 
		&ProcessorOutputObserver::OnModifiedOutputDataEntity );
}

void Core::ProcessorOutputObserver::RemoveObservers()
{
	if ( m_Processor.IsNull() || 
		 m_OutputNumber >= int( m_Processor->GetNumberOfOutputs() ) )
	{
		return;
	}

	m_Processor->GetOutputDataEntityHolder( m_OutputNumber )->RemoveObserver( 
		this, 
		&ProcessorOutputObserver::OnModifiedOutputDataEntity );
}

void Core::ProcessorOutputObserver::OnModifiedOutputDataEntity()
{
	try{

		if ( GetOutputDataEntity( ).IsNull() || !m_Enable )
		{
			return;
		}

		AddToDataEntityList( );

		RenderDataEntity();

		// Hide input after we add the child, to avoid reinitialization of views
		HideInput( );

		SelectDataEntity();

		ApplyPropertyList( );
	}
	coreCatchExceptionsLogAndNoThrowMacro( 
		"CardiacInitializationPanelWidget::OnModifiedOutputDataEntity")

}

void Core::ProcessorOutputObserver::HideInput()
{
	if ( m_Processor->GetNumberOfInputs() == 0 )
	{
		return;
	}

	Core::DataEntity::Pointer inputDataEntity;
	inputDataEntity = m_Processor->GetInputDataEntity( m_HideInputNumber );

	// Hide input if is different from output and output is not empty
	Core::DataEntity::Pointer outputDataEntity = m_Processor->GetOutputDataEntity( m_OutputNumber );

	// Check if hide input tag is present
	if ( outputDataEntity.IsNotNull( ) )
	{
		blTagMap::Pointer renderingTags;
		renderingTags = outputDataEntity->GetMetadata( )->GetTagValue<blTagMap::Pointer>( "Rendering" );
		if ( renderingTags.IsNotNull() && 
			renderingTags->FindTagByName( "hideParent" ).IsNotNull( ) )
		{
			bool hideParent;
			hideParent = renderingTags->FindTagByName( "hideParent" )->GetValueCasted<bool>( );
			if ( hideParent )
			{
				m_RenderingTree->Show( inputDataEntity, false );
			}
			return;
		}
	}

	// Check the parameter
	if ( !m_hideInput )
	{
		if (m_opacityInput)
			OpacityInput();
		return;
	}

	// Hide input
	if ( outputDataEntity != inputDataEntity && m_RenderingTree.IsNotNull() )
	{
		m_RenderingTree->Show( inputDataEntity, false );
	}

}

void Core::ProcessorOutputObserver::OpacityInput()
{
	if ( m_Processor->GetNumberOfInputs() == 0 )
	{
		return;
	}

	Core::DataEntity::Pointer inputDataEntity;
	inputDataEntity = m_Processor->GetInputDataEntity( m_HideInputNumber );
	
	if ( m_RenderingTree.IsNull() )
		return;

	m_RenderingTree->SetProperty( inputDataEntity, blTag::New( "opacity", m_opacityValue ) );
}

void Core::ProcessorOutputObserver::SetOutputNumber( size_t outputNumber )
{
	m_OutputNumber = outputNumber;
}

void Core::ProcessorOutputObserver::SetHideInput( bool val, int inputNumber )
{
	m_hideInput = val;
	m_HideInputNumber = inputNumber;
}

void Core::ProcessorOutputObserver::SetOpacityInput( bool val, int inputNumber, double opacity )
{
	m_opacityInput = val;
	m_hideInput = !val;
	m_HideInputNumber = inputNumber;
	m_opacityValue = opacity;
}

void Core::ProcessorOutputObserver::SetInitializeViews( bool val )
{
	m_InitializeViews = val;
}

void Core::ProcessorOutputObserver::SelectDataEntity( bool val )
{
	m_SetSelected = val;
}

void Core::ProcessorOutputObserver::SelectDataEntity()
{
	if ( !m_SetSelected )
	{
		return;
	}

	Core::DataEntityHolder::Pointer holder;
	Core::DataContainer::Pointer dataContainer;
	dataContainer = Core::Runtime::Kernel::GetDataContainer();
	holder = dataContainer->GetDataEntityList()->GetSelectedDataEntityHolder();

	// In CardiacSegmentationPlugin we need to notify the 
	// QuantificationWidget in order to compute the volume
	// each time the output mesh is modified
	holder->SetSubject( GetOutputDataEntity(), true );
}

void Core::ProcessorOutputObserver::SetAddToDataList( bool val )
{
	m_AddToDataList = val;
}

void Core::ProcessorOutputObserver::SetAddToRederingTree( bool val )
{
	m_AddToRederingTree = val;
}

void Core::ProcessorOutputObserver::AddToDataEntityList()
{
	if ( !m_AddToDataList )
	{
		return;
	}

	// Add to list and connect holders
	Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
	Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();
	list->Add( GetOutputDataEntity( ) );

	list->ConnectOutputHolder( m_Processor->GetOutputDataEntityHolder( m_OutputNumber ) );
}

Core::DataEntity::Pointer Core::ProcessorOutputObserver::GetOutputDataEntity()
{
	Core::DataEntity::Pointer dataEntity;
	dataEntity = m_Processor->GetOutputDataEntity( m_OutputNumber );
	return dataEntity;
}

void Core::ProcessorOutputObserver::RenderDataEntity()
{
	if ( !m_AddToRederingTree || m_RenderingTree.IsNull() )
	{
		return;
	}

	bool visible = true;
	if ( m_PropertyList->GetTag( "visible" ).IsNotNull( ) )
	{
		visible = m_PropertyList->GetTagValue<bool>( "visible" );
	}

	// Render the generated mesh
	m_RenderingTree->Add( GetOutputDataEntity(), visible, m_InitializeViews );
}


void Core::ProcessorOutputObserver::AddProperty( blTag::Pointer property )
{
	if ( m_RenderingTree.IsNull() )
	{
		return;
	}

	Core::DataEntity::Pointer dataEntity;
	dataEntity = m_Processor->GetOutputDataEntity( m_OutputNumber );
	m_RenderingTree->SetProperty( dataEntity, property );
}

void Core::ProcessorOutputObserver::PushProperty( blTag::Pointer property )
{
	m_PropertyList->AddTag( property );
}

void Core::ProcessorOutputObserver::ApplyPropertyList()
{
	blTagMap::ListIterator it;
	for ( it = m_PropertyList->ListBegin() ; it != m_PropertyList->ListEnd() ; it++ )
	{
		AddProperty( m_PropertyList->GetTag( it ) );
	}
}

bool Core::ProcessorOutputObserver::GetEnable() const
{
	return m_Enable;
}

void Core::ProcessorOutputObserver::SetEnable( bool val )
{
	m_Enable = val;
}
