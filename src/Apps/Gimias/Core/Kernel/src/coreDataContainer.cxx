/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDataContainer.h"

using namespace Core;

//!
DataContainer::DataContainer(void)
{
	// Initialize the holder for the data entity list
	this->m_DataEntityList = DataEntityList::New();
}

//!
DataContainer::~DataContainer(void)
{
}

//!
DataEntityList::Pointer DataContainer::GetDataEntityList(void) const
{ 
	return static_cast<DataEntityList*>(this->m_DataEntityList.GetPointer());
}

