/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "corePluginProvidersUpdater.h"
#include "coreProcessorThread.h"
#include "coreProcessorManager.h"
#include "blTextUtils.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreKernel.h"
#include "coreLogger.h"

Core::PluginProvidersUpdater::PluginProvidersUpdater( )
{
	m_EnableScanPlugins  = true;
	m_EnableLoadPlugins = true;
	m_EnableMultithreading = true;
	m_SelectAllProviders = false;
	m_StatusHolder = Core::IntHolderType::New();
	m_StatusHolder->SetSubject( STATUS_IDLE );
}

//!
Core::PluginProvidersUpdater::~PluginProvidersUpdater(void)
{
}

void Core::PluginProvidersUpdater::SelectProvider( const std::string &providerName )
{
	boost::lock_guard<boost::mutex> lock_g(m_Mutex); 

	m_SelectAllProviders = false;
	m_SelectedProviders.clear( );
	m_SelectedProviders[ providerName ] = STATUS_IDLE;
}

void Core::PluginProvidersUpdater::SelectAllProviders( )
{
	boost::lock_guard<boost::mutex> lock_g(m_Mutex); 

	m_SelectAllProviders = true;
	m_SelectedProviders.clear( );
	std::map<std::string,Core::Runtime::PluginProvider::Pointer>::iterator itProvider;
	for ( itProvider = m_Providers.begin() ; itProvider != m_Providers.end() ; itProvider++ )
	{
		m_SelectedProviders[ itProvider->first ] = STATUS_IDLE;
	}	
}

void Core::PluginProvidersUpdater::EnableScanPlugins( bool enable )
{
	m_EnableScanPlugins = enable;
}

void Core::PluginProvidersUpdater::EnableLoadPlugins( bool enable )
{
	m_EnableLoadPlugins = enable;
}

void Core::PluginProvidersUpdater::EnableMultithreading( bool enable )
{
	m_EnableMultithreading = enable;
}

bool Core::PluginProvidersUpdater::GetEnableScanPlugins( )
{
	return m_EnableScanPlugins;
}

bool Core::PluginProvidersUpdater::GetEnableLoadPlugins( )
{
	return m_EnableLoadPlugins;
}

bool Core::PluginProvidersUpdater::GetEnableMultithreading( )
{
	return m_EnableMultithreading;
}

Core::IntHolderType::Pointer Core::PluginProvidersUpdater::GetStatusHolder( )
{
	return m_StatusHolder;
}

void Core::PluginProvidersUpdater::Update()
{
	boost::lock_guard<boost::mutex> lock_g(m_Mutex); 

	m_StatusHolder->SetSubject( STATUS_PROCESSING );

	// Get plugin names, because some plugins can add/remove more plugin providers
	std::map<std::string,int> pendingProviders = m_SelectedProviders;

	// Update each provider
	std::map<std::string,int>::iterator itProvider;
	for( itProvider = pendingProviders.begin() ; itProvider != pendingProviders.end() ; itProvider++ )
	{
		UpdatePluginProvider( itProvider->first );
	}

	if ( !m_EnableMultithreading )
	{
		m_StatusHolder->SetSubject( STATUS_FINISHED );
	}
}

void Core::PluginProvidersUpdater::UpdatePluginProvider( const std::string &name )
{
	Runtime::PluginProvider::Pointer provider;
	provider = m_Providers[ name ];
	
	// Configure
	provider->EnableScanPlugins( m_EnableScanPlugins );
	provider->EnableLoadPlugins( m_EnableLoadPlugins );
	provider->SetMultithreading( m_EnableMultithreading );
	
	// Remove previous observer (if any)
	provider->RemoveObserver1( this, &PluginProvidersUpdater::OnModifiedProvider );

	// Only enable observer if multithreading is enabled
	// otherwise, the provider will directly load the plugins
	provider->AddObserver1( this, &PluginProvidersUpdater::OnModifiedProvider );

	// Execute
	Core::ProcessorThread::Pointer thread;
	thread = Core::Runtime::Kernel::GetProcessorManager()->Execute( provider.GetPointer( ) );

	// Configure observer for processor thread status to clean observers
	if ( thread.IsNotNull( ) )
	{
		// Observer for information messages
		thread->GetUpdateCallback( )->AddObserver1( this, &PluginProvidersUpdater::OnProviderStatus );
	}
}

void Core::PluginProvidersUpdater::OnModifiedProvider( SmartPointerObject* object )
{
	Runtime::PluginProvider* provider = dynamic_cast<Runtime::PluginProvider*> ( object );
	if ( provider == NULL )
		return;

	// Add observer for each output
	for ( unsigned i = 0 ; i < provider->GetNumberOfOutputs() ; i++ )
	{
		provider->GetOutputDataEntityHolder( i )->AddObserver1( 
			this,
			&PluginProvidersUpdater::OnModifiedProviderOutput );
	}
}

void Core::PluginProvidersUpdater::OnProviderStatus( SmartPointerObject* object )
{
	Core::UpdateCallback::Pointer callback = dynamic_cast<Core::UpdateCallback*> ( object );
	if ( callback.IsNull( ) )
		return;

	Core::ProcessorThread::Pointer thread;
	thread = Runtime::Kernel::GetProcessorManager()->GetProcessorThread( callback->GetProcessorThreadID( ) );
	if ( thread.IsNull( ) )
	{
		return;
	}

	if ( thread->GetState( ) == ProcessorThread::STATE_FINISHED && m_EnableMultithreading )
	{
		Core::BaseProcessor::Pointer provider = thread->GetProcessor( );

		boost::lock_guard<boost::mutex> lock_g(m_Mutex); 

		// Mark as finished
		m_SelectedProviders[ provider->GetName( ) ] = STATUS_FINISHED;

		// Check if all processors have finished
		bool allFinished = true;
		std::map<std::string,int>::iterator itProvider;
		for( itProvider = m_SelectedProviders.begin() ; itProvider != m_SelectedProviders.end() ; itProvider++ )
		{
			allFinished &= (m_SelectedProviders[ itProvider->first ] == STATUS_FINISHED);
		}

		// Send finished all providers event
		if ( allFinished )
		{
			m_StatusHolder->SetSubject( STATUS_FINISHED );
		}
	}
}

void Core::PluginProvidersUpdater::OnModifiedProviderOutput( DataEntity::Pointer dataEntity )
{
	if ( dataEntity.IsNull( ) )
		return;

	// Get plugin info
	blTagMap::Pointer pluginInfo;
	dataEntity->GetProcessingData( pluginInfo );

	// Get information
	blTag::Pointer tag = pluginInfo->GetTag( pluginInfo->GetIteratorBegin( ) );

	// Load/Unload plugin using main gui thread
	Runtime::PluginProvider* provider;
	provider = m_Providers[ dataEntity->GetMetadata()->GetName() ];
	if ( provider == NULL || tag->GetValueCasted<bool>( ) )
	{
		// Initialize current plugin to keep track of registered window factories
		m_CurrentPluginName = tag->GetName();

		// Load plugin
		provider->LoadPlugin( tag->GetName() );

		// Restore active plugin
		m_CurrentPluginName = "";
	}
	else
	{
		provider->UnLoadPlugin( tag->GetName() );
	}
}


void Core::PluginProvidersUpdater::AddProvider( Core::Runtime::PluginProvider::Pointer provider )
{
	boost::lock_guard<boost::mutex> lock_g(m_Mutex); 
	
	m_Providers[ provider->GetName( ) ] = provider;
	if ( m_SelectAllProviders && GetStatusHolder( )->GetSubject( ) == STATUS_PROCESSING )
	{
		m_SelectedProviders[ provider->GetName( ) ] = STATUS_IDLE;
		UpdatePluginProvider( provider->GetName( ) );
	}
}

void Core::PluginProvidersUpdater::RemoveProvider( Core::Runtime::PluginProvider::Pointer provider )
{
	boost::lock_guard<boost::mutex> lock_g(m_Mutex); 

	m_Providers.erase( provider->GetName( ) );
}


std::string Core::PluginProvidersUpdater::GetCurrentPluginName() const
{
	return m_CurrentPluginName;
}

void Core::PluginProvidersUpdater::SetCurrentPluginName( const std::string &name ) 
{
	m_CurrentPluginName = name;
}
