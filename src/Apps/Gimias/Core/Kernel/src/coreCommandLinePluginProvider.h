/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#ifndef coreCommandLinePluginManager_H
#define coreCommandLinePluginManager_H

#include "gmKernelWin32Header.h"
#include "coreObject.h"
#include "corePluginProvider.h"

namespace Core
{
namespace Runtime
{

/**
\brief Loads Command line plugins 

\ingroup gmKernel
\author Xavi Planes
\date Feb 2011
*/
class GMKERNEL_EXPORT CommandLinePluginProvider : public PluginProvider
{
public:
	coreDeclareSmartPointerClassMacro( Core::Runtime::CommandLinePluginProvider, PluginProvider);
	coreDefineFactoryClass( CommandLinePluginProvider );
	coreDefineFactoryClassEnd( );

	//!
	std::string GetName( ) const;

	//! Create BaseFrontEndPlugin instances and attach them to GUI
	void LoadPlugins( );

	//!
	void ScanPlugins( );

	//!
	virtual void LoadPlugin( const std::string &name );

	//!
	virtual void UnLoadPlugin( const std::string &name );

protected:
	//!
	CommandLinePluginProvider(void);
	//!
	virtual ~CommandLinePluginProvider(void);

	//! Compute available profiles from loaded libraries
	void InferAvailableProfilesFromPlugins(void);

private:

	//!
	coreDeclareNoCopyConstructors(CommandLinePluginProvider);
};

} // namespace Runtime
} // namespace Core


class ModuleDescriptionMap : public std::map<std::string, ModuleDescription> {};

/**
\brief Access ModuleDescriptionMap and redirect messages to processor

\ingroup gmKernel
\author Xavi Planes
\date Jan 2013
*/
class gmModuleFactory : public ModuleFactory
{
public:
	gmModuleFactory( Core::BaseProcessor* processor = NULL );
	ModuleDescriptionMap *GetModuleDescriptionMap( );
	virtual void WarningMessage( const char *msg);
	virtual void ErrorMessage( const char *msg );
	virtual void InformationMessage( const char *msg);
	virtual void ModuleDiscoveryMessage( const char *msg);
private:
	Core::BaseProcessor* m_Processor;
};


#endif

