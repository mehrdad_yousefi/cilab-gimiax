/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#ifndef coreFrontEndPluginLoader_H
#define coreFrontEndPluginLoader_H

#include "gmKernelWin32Header.h"
#include "coreObject.h"
#include "coreFrontEndPlugin.h"
#include <itksys/DynamicLoader.hxx>
#include "coreBasePlugin.h"

// LoadLibrary is defined by Windows
#ifdef LoadLibrary
  #undef LoadLibrary
#endif // LoadLibrary
namespace Core
{
namespace Runtime
{

/**
\brief Loads a plugin dinamically.


\ingroup gmKernel
\author Xavi Planes
\date 10 April 2010
*/
class GMKERNEL_EXPORT FrontEndPluginLoader : public BasePlugin
{
public:
	typedef Core::FrontEndPlugin::FrontEndPlugin::Pointer 
		(*FrontEndPluginConstructorFunc)(void);

	coreDeclareSmartPointerClassMacro(
		Core::Runtime::FrontEndPluginLoader, 
		Core::SmartPointerObject);
	
	//!
	void SetPath( const std::string path );

	//! 
	void LoadXML( );

	//! Load plugin library into memory
	void LoadLibrary( );

	//!
	void CloseLibrary( );

	//! Resolve the procedure "newFrontEndPlugin"
	void CreateFrontEndPlugin( );

	//!
	Core::FrontEndPlugin::FrontEndPlugin::Pointer GetFrontEndPlugin();

	//!
	void ResetFrontEndPlugin();

	//!
	itksys::DynamicLoader::LibraryHandle GetLibraryHandle();

	//!
	std::string GetLibraryFilename() const;


protected:
	FrontEndPluginLoader(void);
	virtual ~FrontEndPluginLoader(void);

	coreDeclareNoCopyConstructors(FrontEndPluginLoader);

	//!
	void LoadLibraryHandle( const std::string &filePath );

	//!
	void FindLibraryPath( );
private:

	//!
	std::string m_Path;

	//!
	std::string m_LibraryFilename;

	//!
	itksys::DynamicLoader::LibraryHandle m_LibraryHandle;

	//!
	Core::FrontEndPlugin::FrontEndPlugin::Pointer m_FrontEndPlugin;
};

} // namespace Runtime
} // namespace Core

#endif

