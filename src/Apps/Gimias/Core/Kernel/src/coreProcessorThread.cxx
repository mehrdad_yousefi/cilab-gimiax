/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreProcessorThread.h"
#include "coreKernel.h"
#include "coreProcessorManager.h"
#include "coreFactoryManager.h"
#include "coreUpdateCallback.h"
#include "coreReportExceptionMacros.h"
#include "coreDataContainer.h"

#include <boost/thread/mutex.hpp>

#include "itksys/SystemTools.hxx"

using namespace Core;

long Core::ProcessorThread::m_IdCount = 0;

Core::ProcessorThread::ProcessorThread( 
	BaseProcessor::Pointer processor )
{
	m_Processor = processor;
	m_State = STATE_PENDING;
	m_ProcessorUID = m_Processor->GetUID();
	m_ProcessorName = m_Processor->GetName();
	m_UID = m_IdCount++;
	m_StartedTime = 0;
	m_FinishedTime = 0;
	m_ShowProcessingMessage = true;

	// Add update callback observer to notify GUI if needed and keep a 
	// reference to the Processor UpdateCallback
	AddUpdateCallbackObserver();
}

Core::ProcessorThread::~ProcessorThread()
{
}

void Core::ProcessorThread::WorkerFunc( )
{

	m_State = STATE_ACTIVE;
	m_StartedTime = itksys::SystemTools::GetTime();
	GetUpdateCallback( )->Modified();

	// Update processor
	UpdateProcessor( );

	// Notify observers that this processor has finished
	m_State = STATE_FINISHED;
	GetUpdateCallback( )->Modified();

	// If started time is no 0, this is an attached running process
	// and the time is set by other class
	if ( m_FinishedTime == 0.0 )
	{
		m_FinishedTime = itksys::SystemTools::GetTime();
	}

	// Remove update callback observer
	RemoveUpdateCallbackObserver();

	m_Processor = NULL;
}

BaseProcessor::Pointer Core::ProcessorThread::GetProcessor() const
{
	return m_Processor;
}

Core::ProcessorThread::STATE Core::ProcessorThread::GetState() const
{
	return m_State;
}

void Core::ProcessorThread::UpdateProcessor()
{
	std::string errorMessage;

	if ( GetUpdateCallback( )->GetAbortProcessing() )
	{
		return;
	}

	try
	{
		// Avoid to lock resources because it doesn't seem to be problems
		//LockResources( true );

		// Compute max number of time steps for all inputs
		size_t maxTimeSteps = m_Processor->ComputeNumberOfIterations( );

		// Call update for each time step
		for ( unsigned timeStep = 0 ; timeStep < maxTimeSteps ; timeStep++ )
		{
			m_Processor->UpdateSelectedPortsTimeStep( timeStep );

			// Call update
			m_Processor->Update();
		}
	}
	catch(Core::Exceptions::Exception& e) {
		errorMessage = e.what();
		Core::LogException(e);
	}
	catch(std::exception& stdEx)
	{
		Core::Exceptions::StdException e("ProcessorThread::WorkerFunc", stdEx);
		errorMessage = e.what();
		Core::LogException(e);
	}
	catch(...)
	{
		Core::Exceptions::UnhandledException e("ProcessorThread::WorkerFunc");
		errorMessage = e.what();
		Core::LogException(e);
	} 

	if ( !errorMessage.empty( ) && 
		!GetUpdateCallback( )->GetAbortProcessing() )
	{
		GetUpdateCallback( )->SetExceptionMessage( errorMessage );
		GetUpdateCallback( )->Modified();
	}

	// Avoid to lock resources because it doesn't seem to be problems
	// Unlock resources
	//LockResources( false );
}

long Core::ProcessorThread::GetProcessorUID() const
{
	return m_ProcessorUID;
}

std::string Core::ProcessorThread::GetProcessorName() const
{
	return m_ProcessorName;
}

long Core::ProcessorThread::GetUID() const
{
	return m_UID;
}

void Core::ProcessorThread::AddUpdateCallbackObserver()
{
	if ( m_Processor.IsNull() )
	{
		return;
	}

	// Find a CallbackObserver with GUI tag
	blTagMap::Pointer properties = blTagMap::New( );
	properties->AddTag( "GUI", true );
	SmartPointerObject::Pointer object;
	try{
		object = FactoryManager::CreateInstance( CallbackObserver::GetNameClass( ), properties );
		m_CallbackObserver = CallbackObserver::SafeDownCast( object );
	}catch(...)
	{
		m_CallbackObserver = Core::CallbackObserver::New( );
	}

	// Add observer on modified
	m_CallbackObserver->SetUpdateCallback( m_Processor->GetUpdateCallback() );
	m_Processor->GetUpdateCallback()->AddObserverOnModified( 
		m_CallbackObserver.GetPointer(), &CallbackObserver::OnModified );
	
	// Configure update callback
	m_Processor->GetUpdateCallback()->SetProcessorThreadID( m_UID );
}

void Core::ProcessorThread::RemoveUpdateCallbackObserver()
{
	try
	{
		// Remove UpdateCallback observer
		m_Processor->GetUpdateCallback( )->RemoveObserverOnModified( 
			m_CallbackObserver.GetPointer(), &CallbackObserver::OnModified );
	}
	catch ( Exceptions::FactoryNotFoundException &e )
	{
		if( e.GetCallingFunction() != "ProcessorThread::WorkerFunc" )
			e.AddTrace( "ProcessorThread::WorkerFunc" );
		GetUpdateCallback( )->SetExceptionMessage( e.what( ) );
		GetUpdateCallback( )->Modified();
		Core::LogException(e);
	}
}

UpdateCallback::Pointer Core::ProcessorThread::GetUpdateCallback()
{
	return m_CallbackObserver->GetUpdateCallback();
}

std::string Core::ProcessorThread::GetStatusMessage()
{
	std::string message;
	if ( GetUpdateCallback( )->GetAbortProcessing() )
	{
		message = "Cancelled";
	}
	else if ( !GetUpdateCallback( )->GetExceptionMessage( ).empty() )
	{
		message = "Finished with errors";
	}
	else
	{
		switch ( GetState( ) )
		{
		case STATE_PENDING:message = "Pending...";break;
		case STATE_ACTIVE:message = "Processing...";break;
		case STATE_FINISHED:message = "Completed successfully";break;
		}
	}

	return message;
}

void Core::ProcessorThread::LockResources( bool lock )
{
	Core::DataContainer::Pointer dataContainer;
	dataContainer = Core::Runtime::Kernel::GetDataContainer();

	if ( lock )
	{
		dataContainer->GetDataEntityList()->Lock( m_UID, m_Processor->GetDataEntitiesID() );
	}
	else
	{
		dataContainer->GetDataEntityList()->UnLock( m_UID );
	}
}

double Core::ProcessorThread::GetDuration()
{
	switch ( m_State )
	{
	case STATE_ACTIVE:
		return itksys::SystemTools::GetTime( ) - m_StartedTime;
		break;
	case STATE_FINISHED:
		return m_FinishedTime - m_StartedTime;
		break;
	case STATE_PENDING:
		return 0;
		break;
	}
	return 0;
}

std::string Core::ProcessorThread::GetDurationAsString()
{
	double duration = GetDuration();
	long hours = duration / 3600;
	duration = duration - hours * 3600;
	long minutes = duration / 60;
	duration = duration - minutes * 60;
	long seconds = duration;

	std::stringstream sstream;
	if ( hours > 0 )
	{
		sstream << hours << " h ";
	}
	if ( minutes > 0 )
	{
		sstream << minutes << " min ";
	}
	sstream << seconds << " sec";

	return sstream.str();
}

double Core::ProcessorThread::GetStartedTime() const
{
	return m_StartedTime;
}

void Core::ProcessorThread::SetStartedTime( double val )
{
	m_StartedTime = val;
}

double Core::ProcessorThread::GetFinishedTime() const
{
	return m_FinishedTime;
}

void Core::ProcessorThread::SetFinishedTime( double val )
{
	m_FinishedTime = val;
}

bool Core::ProcessorThread::GetShowProcessingMessage( ) const
{
	return m_ShowProcessingMessage;
}

void Core::ProcessorThread::SetShowProcessingMessage( bool val )
{
	m_ShowProcessingMessage = val;
}

