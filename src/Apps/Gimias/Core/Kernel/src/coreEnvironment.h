/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreEnvironment_H
#define coreEnvironment_H

#include "gmKernelWin32Header.h"
#include "coreObject.h"
#include "coreDataEntity.h"
#include "coreDataHolder.h"
#include <string>

namespace Core 
{
namespace Runtime 
{

//! Stores the mode in which the application is running.
typedef enum Mode { Console, Graphical } AppRunMode;

//! State of the application using main GUI thread
typedef enum{
	//! The application is processing an algorithm
	APP_STATE_PROCESSING,
	//! The application is idle state
	APP_STATE_IDLE
} APP_STATE;
typedef Core::DataHolder<APP_STATE> AppStateHolderType;

enum KERNEL_STATE
{
	//! Cannot read state from file
	KERNEL_STATE_UNKNOWN,
	//! Load config.xml
	KERNEL_STATE_IDLE,
	//! Load config.xml
	KERNEL_STATE_LOADING_CONFIGURATION,
	//! Register Kernel factories from gmIO, gmDataHandling
	KERNEL_STATE_INITIALIZING_KERNEL,
	//! Loading selected plugins
	KERNEL_STATE_LOADING_PLUGINS,
	//! Running main loop that handles the events
	KERNEL_STATE_RUNNING_MAIN_LOOP,
	//! Exiting application
	KERNEL_STATE_EXITING
};
typedef Core::DataHolder<KERNEL_STATE> KernelStateHolderType;

/** 
\brief The Environment class is the backbone of the application. It is 
responsible for managing the main application loop (the loop that
listens for events and calls the handler for these events). 

\todo This class has a protected section but no private section. The use 
of private sections should be preferred over 
the use of protected sections. When refactoring a class, changing 
protected sections maybe almost impossible (or at best:
dangerous) because it cannot be anticipated how child classes rely on 
these protected sections.

\note You should access and get the instance for this class through the 
runtime high-end object: Kernel

\sa Core::Runtime::Kernel
\ingroup gmKernel
\author Juan Antonio Moya
\date 31 May 2007
*/

class GMKERNEL_EXPORT Environment : public Core::SmartPointerObject
{
public:
	coreDeclareSmartPointerTypesMacro(Core::Runtime::Environment,Core::SmartPointerObject);
	coreClassNameMacro(Core::Runtime::Environment)
	coreTypeMacro(Core::Runtime::Environment,Core::SmartPointerObject)

	//!
	void InitApplication(
		int argc,
		char* argv[], 
		Core::Runtime::AppRunMode mode);

	//! Exits the application main loop and sets m_mustRestartOnExit to true.
	void RestartApplication(void);
	//! Returns true if the application is exiting.
	bool OnAppExit(void) const;

	//! Start the application main loop.
	void ExecuteMainLoop(void);
	//! Exits the application main loop (see header file).
	void Exit(void);
	//! Returns true if the application is running.
	virtual bool IsAppRunning(void) const = 0;
	/** Restart application if m_mustRestartOnExit is true.
	Executes the argv parameters.
	*/
	virtual void RestartIfNeeded( ) = 0;


	AppStateHolderType::Pointer GetAppStateHolder() const;

	/**
	\brief Change the state of the application. In the APP_STATE_PROCESSING
	a info window will appear with the message "Processing... Please wait".
	*/
	void SetAppState( APP_STATE val );

	//!
	static std::vector<std::string> GetArgv();

	//!
	static bool GetMustRestartOnExit();

	//!
	KernelStateHolderType::Pointer GetKernelStateHolder() const;
	void SetKernelState(KERNEL_STATE state );

	//! Read kernel state from file
	void ReadPreviousKernelState( );

	//!
	std::string GetKernelStateName( KERNEL_STATE state );

	//! Get previous kernel state
	Core::Runtime::KERNEL_STATE GetPreviousKernelState() const;

	//!
	static bool GetRestartWithAdministratorPrivileges();
	static void SetRestartWithAdministratorPrivileges(bool val);

protected:
	Environment(void);
	virtual ~Environment(void);

	//! Start the application main loop.
	virtual void Exec(void) = 0;

	//! Exits the application main loop (see header file).
	virtual void ExitApplication(void) = 0;

	//!
	void WriteKernelState( );

private:
	coreDeclareNoCopyConstructors(Environment);

protected:
	//! A boolean used for breaking the app loop on exit.
	bool exit;							
	//! data node count for assigning an unique identifier to each rendering node
	int dataNodeCount;
	//! Variable (global within the library scope) for holding the command-line arguments
	static std::vector<std::string> m_argv;
	/** Flag (global within the file scope) that states if the 
	 application must restart when RestartIfNeeded is called.
	 */
	static bool m_mustRestartOnExit;
	//! Current state of the app
	AppStateHolderType::Pointer m_AppStateHolder;
	//! Kernel state: 
	KernelStateHolderType::Pointer m_KernelStateHolder;
	//!
	KERNEL_STATE m_PreviousKernelState;
	//!
	static bool m_RestartWithAdministratorPrivileges;
};

} // namespace Runtime
} // namespace Core

#endif
