/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef corePluginMacros__H
#define corePluginMacros__H

/** Definitions for handling shared libraries in Windows */
#if (defined(_WIN32) || defined(WIN32)) && !defined(SWIG_WRAPPING)
#	undef PLUGIN_EXPORT
#	define PLUGIN_EXPORT __declspec(dllexport)
#else
/* unix needs nothing */
#	undef  PLUGIN_EXPORT
#	define PLUGIN_EXPORT
#endif

/** 
\brief Definition for exporting plugin objects out of a shared library. 
The PLUGIN_OBJECT macro makes the export definition crosscompiler, because 
extern "C" removes C++ name mangling. 
For all the macros for defining and exporting plugins, you should read the 
Core::FrontEndPlugin::FrontEndPlugin documentation.
\sa Core::FrontEndPlugin::FrontEndPlugin

\ingroup gmFrontEndPlugin
\author Juan Antonio Moya
\date 17 Nov 2007
*/
#ifdef PLUGIN_OBJECT
#undef PLUGIN_OBJECT
#endif
#define PLUGIN_OBJECT extern "C" PLUGIN_EXPORT

/**
\brief Macro for exporting a class named 'pluginClassName' and make it 
importable for the Core FrontEndPluginManager
\ingroup gmFrontEndPlugin

*/
// - 1 of 2 macros ---------------------
/**
\def coreBeginDefinePluginMacro starts a definition context that makes the 
class exportable plugin object
\ingroup gmFrontEndPlugin
\param classname name of the plugin class being defined, without namespace 
specifiers (forced to Core::FrontEndPlugin namespace)
\note Suppress warning C4190: 'newFrontEndPlugin' has C-linkage specified, 
 but returns UDT 'itk::SmartPointer<TObjectType>' which is incompatible with C
*/
#ifdef _MSC_VER
#define coreBeginDefinePluginMacro(classname)								\
	__pragma(warning(push))													\
	__pragma(warning(disable:4190)) 										\
	PLUGIN_OBJECT classname::Pointer newFrontEndPlugin(void)				\
	{																		\
		classname::Pointer plugin = classname::New();						\
		return plugin;														\
	};																		\
	__pragma(warning( pop ))
#else
#define coreBeginDefinePluginMacro(classname)								\
	PLUGIN_OBJECT classname::Pointer newFrontEndPlugin(void)				\
	{																		\
		classname::Pointer plugin = classname::New();						\
		return plugin;														\
	};
#endif

// - 4 of 4 macros ---------------------
/** \def coreEndDefinePluginMacro ends and closes a plugin definition 
context (opened by coreBeginDefinePluginMacro).
\ingroup gmFrontEndPlugin
\sa Core::Profile
*/
#define coreEndDefinePluginMacro() ;


#endif // corePluginMacros__H

