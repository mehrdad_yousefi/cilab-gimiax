/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreBaseWindowFactories_H
#define _coreBaseWindowFactories_H

#include "coreObject.h"
#include "coreBaseWindow.h"
#include "coreWindowConfig.h"
#include "coreBaseWindowFactory.h"

namespace Core
{

	class FactoryData{
	public:
		//!
		BaseWindowFactory::Pointer m_Factory;
		//!
		WindowConfig m_WindowConfig;
		//!
		bool operator==( const FactoryData f )
		{
			return this->m_Factory == f.m_Factory;
		}
	};


/**
\brief Repository of all BaseWindow registered to the framework

\ingroup gmKernel
\sa FrontEndPlugin
\author Xavi Planes
\date 10 April 2010
*/
class GMKERNEL_EXPORT BaseWindowFactories : public Core::SmartPointerObject
{
public:

	//! Insertion order
	typedef std::list<std::string> FactoriesListType;
	//! Map using factory name as key
	typedef std::map<std::string, FactoryData> FactoriesMapType;
	//!
	typedef Core::DataHolder<FactoriesMapType> FactoriesMapHolderType;

public:

	coreDeclareSmartPointerClassMacro(
		Core::BaseWindowFactories, 
		Core::SmartPointerObject);

	//!
	void CleanRegisteredWidgets(  );

	//! Create a registered widget
	Core::BaseWindow* CreateBaseWindow( 
		const std::string &factoryName, 
		wxWindow* parent );

	//! Create a registered widget
	BaseWindowFactory::Pointer FindFactory( 
		const std::string &factoryName );

	//!
	bool GetWindowConfig( 
		const std::string &factoryName,
		Core::WindowConfig &config );

	//!
	FactoriesMapHolderType::Pointer GetFactoriesHolder();

	//! Register a factory that will be used to create an instance
	void RegisterFactory( 
		BaseWindowFactory::Pointer factory,
		WindowConfig widgetConfig );

	//!
	void UnRegisterFactory( const std::string &factoryName );

	//!
	Core::BaseWindowFactories::FactoriesListType GetFactoryNames() const;

protected:
	//!
	BaseWindowFactories( );

	//!
	FactoriesMapType &GetFactoriesMap();

protected:

	//!
	FactoriesMapHolderType::Pointer m_CreatorsMapHolder;

	//!
	FactoriesListType m_CreatorsList;
};

} // namespace Core

namespace std
{
	// Needed to use the operator == for Core::DataHolder
	bool operator ==(const Core::FactoryData &f1,const Core::FactoryData &f2);
}


#endif // _coreBaseWindowFactories_H

