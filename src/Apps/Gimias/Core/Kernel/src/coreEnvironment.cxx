/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreEnvironment.h"

#include "coreException.h"
#include "coreAssert.h"
#include "coreLogger.h"
#include "coreKernel.h"
#include "coreSettings.h"

#include <fstream>

using namespace Core::Runtime;

std::vector<std::string> Environment::m_argv;
bool Environment::m_mustRestartOnExit = false;
bool Environment::m_RestartWithAdministratorPrivileges = false;

// ------------------------------------------------------------------------------------------------

//!
Environment::Environment()
{
	this->exit = false;
	m_AppStateHolder = AppStateHolderType::New( );
	m_AppStateHolder->SetName( "State Holder" );
	m_AppStateHolder->SetSubject( APP_STATE_IDLE );

	// Create a kernel state file to store the state in disk in case of crash at startup

	m_KernelStateHolder = KernelStateHolderType::New();
	m_KernelStateHolder->SetName( "Kernel state" );
	m_KernelStateHolder->SetSubject( KERNEL_STATE_IDLE );
}

//!
Environment::~Environment(void)
{
}

void Environment::InitApplication(int argc, char* argv[], Core::Runtime::AppRunMode mode)
{
	if ( argv != NULL )
	{
		m_argv.clear();
		for(int i(0); i <argc ; i++ )
		{
			m_argv.push_back( argv[ i ] );
		}
		
	}
}

bool Environment::OnAppExit(void) const
{
	return this->exit;
}

void Environment::RestartApplication(void)
{
	m_mustRestartOnExit = true;
	ExitApplication();
}

AppStateHolderType::Pointer Environment::GetAppStateHolder() const
{
	return m_AppStateHolder;
}

void Environment::SetAppState( 
	APP_STATE val )
{
	m_AppStateHolder->SetSubject( val );
}

std::vector<std::string> Environment::GetArgv() 
{ 
	return m_argv; 
}

bool Core::Runtime::Environment::GetMustRestartOnExit() 
{
	return m_mustRestartOnExit;
}

KernelStateHolderType::Pointer Core::Runtime::Environment::GetKernelStateHolder() const
{
	return m_KernelStateHolder;
}

void Core::Runtime::Environment::SetKernelState( KERNEL_STATE state )
{
	m_KernelStateHolder->SetSubject( state );

	WriteKernelState();
}

void Core::Runtime::Environment::ExecuteMainLoop( void )
{
	Core::Runtime::Kernel::GetApplicationRuntime()->SetKernelState(
		Core::Runtime::KERNEL_STATE_RUNNING_MAIN_LOOP );

	Exec( );
}

void Core::Runtime::Environment::Exit( void )
{
	Core::Runtime::Kernel::GetApplicationRuntime()->SetKernelState(
		Core::Runtime::KERNEL_STATE_EXITING );

	ExitApplication();
}

void Core::Runtime::Environment::WriteKernelState()
{
	Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
	if ( settings.IsNull() )
	{
		return;
	}

	// Write kernel state to file
	std::string kernelFileName = settings->GetProjectHomePath() + "/kernel.txt";
	std::ofstream kernelFile( kernelFileName.c_str() );
	kernelFile 
		<< GetKernelStateHolder()->GetSubject() 
		<< ": " << GetKernelStateName( GetKernelStateHolder()->GetSubject() );
}

void Core::Runtime::Environment::ReadPreviousKernelState()
{
	Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
	if ( settings.IsNull() )
	{
		m_PreviousKernelState = KERNEL_STATE_UNKNOWN;
	}

	// Write kernel state to file
	std::string kernelFileName = settings->GetProjectHomePath() + "/kernel.txt";
	std::ifstream kernelFile( kernelFileName.c_str() );

	if ( !kernelFile.is_open() )
	{
		m_PreviousKernelState = KERNEL_STATE_UNKNOWN;
	}
	else
	{
		int value;
		kernelFile >> value;
		m_PreviousKernelState = KERNEL_STATE( value );
	}
}

std::string Core::Runtime::Environment::GetKernelStateName( KERNEL_STATE state )
{
	std::string name;
	switch ( state )
	{
	case KERNEL_STATE_IDLE: name = "idle"; break;
	case KERNEL_STATE_LOADING_CONFIGURATION: name = "loading configuration";break;
	case KERNEL_STATE_INITIALIZING_KERNEL: name = "initializing kernel";break;
	case KERNEL_STATE_LOADING_PLUGINS: name = "loading plugins";break;
	case KERNEL_STATE_RUNNING_MAIN_LOOP: name = "running main loop";break;
	case KERNEL_STATE_EXITING: name = "exiting";break;
	}

	return name;
}

Core::Runtime::KERNEL_STATE Core::Runtime::Environment::GetPreviousKernelState() const
{
	return m_PreviousKernelState;
}

bool Core::Runtime::Environment::GetRestartWithAdministratorPrivileges()
{
	return m_RestartWithAdministratorPrivileges;
}

void Core::Runtime::Environment::SetRestartWithAdministratorPrivileges( bool val )
{
	m_RestartWithAdministratorPrivileges = val;
}
