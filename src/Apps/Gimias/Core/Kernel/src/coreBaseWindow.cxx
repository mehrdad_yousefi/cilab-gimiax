/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreBaseWindow.h"
#include "coreProcessorOutputsObserverBuilder.h"
#include "coreDataContainer.h"
#include "coreKernel.h"
#include "coreBaseWindowFactories.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreBaseWindowFactorySearch.h"

Core::BaseWindow::BaseWindow()
{
	m_HelperWidget = NULL;
	m_MultiRenderWindow = NULL;
	//m_LandmarkSelectorWidget = NULL;
	m_ListBrowser = NULL;
	m_PluginTab = NULL;
	m_Module = NULL;
	m_MetadataHolder = MetadataHolderType::New( );
	m_MetadataHolder->SetSubject( blTagMap::New( ) );
	m_MetadataHolder->AddObserver( this, &BaseWindow::OnModifiedMetadata );
	m_IsContainerWindow = false;
	m_ChildWindowType = WIDGET_TYPE_MAX;
	m_ParentWindow = NULL;
}

Core::BaseWindow::~BaseWindow()
{

}

void Core::BaseWindow::SetRenderingTree( RenderingTree::Pointer tree )
{
	m_RenderingTree = tree;
	for ( size_t i = 0 ; i < m_ProcessorOutputObserverVector.size() ; i++ )
	{
		m_ProcessorOutputObserverVector[ i ]->SetRenderingTree( tree );
	}
}

Core::RenderingTree::Pointer Core::BaseWindow::GetRenderingTree() const
{
	return m_RenderingTree;
}

Core::Widgets::UserHelper * Core::BaseWindow::GetHelperWidget() const
{
	return m_HelperWidget;
}

void Core::BaseWindow::SetHelperWidget( Core::Widgets::UserHelper * val )
{
	m_HelperWidget = val;
}

Core::Widgets::RenderWindowBase* Core::BaseWindow::GetMultiRenderWindow() const
{
	return m_MultiRenderWindow;
}

void Core::BaseWindow::SetMultiRenderWindow( Core::Widgets::RenderWindowBase* val )
{
	m_MultiRenderWindow = val;
}

Core::Widgets::DataEntityListBrowser* Core::BaseWindow::GetListBrowser() const
{
	return m_ListBrowser;
}

void Core::BaseWindow::SetListBrowser( Core::Widgets::DataEntityListBrowser* val )
{
	m_ListBrowser = val;
}

void Core::BaseWindow::InitProcessorObservers( bool enableDefaultObservers )
{
	if ( GetProcessor().IsNull() )
	{
		return;
	}

	if ( enableDefaultObservers )
	{
		Core::Widgets::ProcessorOutputsObserverBuilder::Pointer observerBuilder;
		observerBuilder = Core::Widgets::ProcessorOutputsObserverBuilder::New( );
		observerBuilder->Init( GetProcessor().GetPointer(), GetRenderingTree() );
		m_ProcessorOutputObserverVector = observerBuilder->GetList();
	}

	// Connect input observers
	for ( size_t i = 0 ; i < GetProcessor()->GetNumberOfInputs() ; i++ )
	{
		Core::DataEntityHolder::Pointer holder;
		holder = GetProcessor()->GetInputDataEntityHolder( i );
		Core::DataHolderBase::SignalType *signal;
		signal = holder->GetSignal( DH_SUBJECT_MODIFIED_OR_NEW_SUBJECT );
		signal->disconnect( boost::bind( &BaseWindow::OnModifiedInput, this, int( i ) ) );
		signal->connect( boost::bind( &BaseWindow::OnModifiedInput, this, int( i ) ) );
		signal = holder->GetSignal( DH_NEW_SUBJECT );
		signal->disconnect( boost::bind( &BaseWindow::OnNewInput, this, int( i ) ) );
		signal->connect( boost::bind( &BaseWindow::OnNewInput, this, int( i ) ) );
	}

	// Connect output observers
	for ( size_t i = 0 ; i < GetProcessor()->GetNumberOfOutputs() ; i++ )
	{
		Core::DataEntityHolder::Pointer holder;
		holder = GetProcessor()->GetOutputDataEntityHolder( i );
		Core::DataHolderBase::SignalType *signal;
		signal = holder->GetSignal( DH_SUBJECT_MODIFIED_OR_NEW_SUBJECT );
		signal->disconnect( boost::bind( &BaseWindow::OnModifiedOutput, this, int( i ) ) );
		signal->connect( boost::bind( &BaseWindow::OnModifiedOutput, this, int( i ) ) );
		signal = holder->GetSignal( DH_NEW_SUBJECT );
		signal->disconnect( boost::bind( &BaseWindow::OnNewOutput, this, int( i ) ) );
		signal->connect( boost::bind( &BaseWindow::OnNewOutput, this, int( i ) ) );
	}
}

int Core::BaseWindow::GetNumInputWidget( )
{
	return m_ProcessorInputWidgetMap.size();
}

Core::Widgets::ProcessorInputWidget* Core::BaseWindow::GetInputWidget( int num )
{
	if ( m_ProcessorInputWidgetMap.find( num ) == m_ProcessorInputWidgetMap.end( ) )
	{
		return NULL;
	}
	return m_ProcessorInputWidgetMap[ num ];
}

void Core::BaseWindow::SetInputWidget( int num, Core::Widgets::ProcessorInputWidget* widget )
{
	m_ProcessorInputWidgetMap[ num ] = widget;
}

Core::ProcessorOutputObserver::Pointer Core::BaseWindow::GetProcessorOutputObserver( int num )
{
	if ( size_t( num ) >= m_ProcessorOutputObserverVector.size( ) )
	{
		return NULL;
	}
	return m_ProcessorOutputObserverVector.at( num );
}

void Core::BaseWindow::OnModifiedInput( int num )
{

}

void Core::BaseWindow::OnModifiedOutput( int num )
{

}

int Core::BaseWindow::GetTimeStep() const
{
	if ( m_TimeStepHolder.IsNull() )
	{
		return 0;
	}
	return m_TimeStepHolder->GetSubject();
}

void Core::BaseWindow::SetTimeStepHolder( Core::IntHolderType::Pointer val )
{
	m_TimeStepHolder = val;
}

void Core::BaseWindow::SetTimeStep( int time )
{
	m_TimeStepHolder->SetSubject( time );
}

void Core::BaseWindow::OnNewInput( int num )
{

}

void Core::BaseWindow::OnNewOutput( int num )
{

}

Core::Widgets::PluginTab* Core::BaseWindow::GetPluginTab() const
{
	return m_PluginTab;
}

void Core::BaseWindow::SetPluginTab( Core::Widgets::PluginTab* val )
{
	m_PluginTab = val;
}

void Core::BaseWindow::SetBitmap( const char* const* data )
{
	m_Bitmap_xpm = data;
}

const char* const* Core::BaseWindow::GetBitmap() const { 

	if ( m_Bitmap_xpm.empty() )
	{
		return NULL;
	}

	try
	{
		return boost::any_cast< const char* const*>( m_Bitmap_xpm ); 
	}
	catch(boost::bad_any_cast&)
	{
	}
	return NULL;
}

void Core::BaseWindow::Init( )
{
	OnInit();
}

void Core::BaseWindow::OnInit()
{

}

std::string Core::BaseWindow::GetFactoryName() const
{
	return m_FactoryName;
}

void Core::BaseWindow::SetFactoryName( std::string val )
{
	m_FactoryName = val;
}

void Core::BaseWindow::SetProperties( blTagMap::Pointer tagMap )
{

}

void Core::BaseWindow::GetProperties( blTagMap::Pointer tagMap )
{

}

std::string Core::BaseWindow::GetBitmapFilename() const
{
	return m_BitmapFilename;
}

void Core::BaseWindow::SetBitmapFilename( std::string val )
{
	m_BitmapFilename = val;
}

void Core::BaseWindow::SetModule( ModuleDescription* module )
{
	m_Module = module;
}

ModuleDescription* Core::BaseWindow::GetModule()
{
	return m_Module;
}

Core::BaseProcessor::Pointer Core::BaseWindow::GetProcessor()
{
	return NULL;
}

Core::DataHolder<blTagMap::Pointer>::Pointer Core::BaseWindow::GetMetadataHolder() const
{
	return m_MetadataHolder;
}

blTagMap::Pointer Core::BaseWindow::GetMetadata() const
{
	return m_MetadataHolder->GetSubject();
}

void Core::BaseWindow::OnModifiedMetadata()
{
	
}

void Core::BaseWindow::ConnectProcessor( Core::BaseWindow* baseWindow )
{
	if ( baseWindow == NULL || baseWindow->GetProcessor().IsNull() || GetProcessor().IsNull() )
	{
		return;
	}

	Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
	Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();

	std::list<Core::DataEntity::Pointer> discardedList;
	for (size_t i = 0 ; i < GetProcessor()->GetNumberOfInputs() ; i++ )
	{
		for ( size_t j = 0 ; j < baseWindow->GetProcessor()->GetNumberOfOutputs() ; j++ )
		{
			Core::DataEntity::Pointer dataEntity;
			dataEntity = baseWindow->GetProcessor()->GetOutputDataEntity( j );

			std::list<Core::DataEntity::Pointer>::iterator it;
			it = std::find( discardedList.begin(), discardedList.end(), dataEntity );
			if ( it != discardedList.end() )
			{
				continue;
			}
			
			// If DataEntity is in the DataEntityList, use it
			if ( dataEntity.IsNotNull( ) && list->Find( dataEntity->GetId() ) == list->End( ) )
			{
				continue;
			}

			if ( GetProcessor()->GetInputPort( i )->SetDataEntity( dataEntity ) )
			{
				discardedList.push_back( dataEntity );
				break;
			}
		}
	}
	
}

Core::BaseWindow* Core::BaseWindow::GetChildWindow( int id )
{
	BaseWindowFactories::Pointer baseWindowFactories;
	baseWindowFactories = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	std::list<BaseWindow*>::iterator itChildren;
	for ( itChildren = m_Children.begin( ) ; itChildren != m_Children.end( ) ; itChildren++ )
	{
		Core::BaseWindowFactory::Pointer factory;
		factory = baseWindowFactories->FindFactory( (*itChildren)->GetFactoryName( ) );
		if ( factory.IsNotNull( ) && factory->GetWindowId( ) == id )
		{
			return *itChildren;
		}

		BaseWindow* child = (*itChildren)->GetChildWindow( id );
		if ( child )
		{
			return child;
		}
	}

	return NULL;
}

Core::BaseWindow* Core::BaseWindow::GetChildWindow( const std::string &name )
{
	BaseWindowFactories::Pointer baseWindowFactories;
	baseWindowFactories = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	std::list<BaseWindow*>::iterator itChildren;
	for ( itChildren = m_Children.begin( ) ; itChildren != m_Children.end( ) ; itChildren++ )
	{
		Core::BaseWindowFactory::Pointer factory;
		factory = baseWindowFactories->FindFactory( (*itChildren)->GetFactoryName( ) );
		if ( factory.IsNotNull( ) && factory->GetWindowName( ) == name )
		{
			return *itChildren;
		}

		BaseWindow* child = (*itChildren)->GetChildWindow( name );
		if ( child )
		{
			return child;
		}
	}

	return NULL;
}

Core::BaseWindow* Core::BaseWindow::GetChildWindowByFactory( const std::string &factoryname )
{
	std::list<BaseWindow*>::iterator itChildren;
	for ( itChildren = m_Children.begin( ) ; itChildren != m_Children.end( ) ; itChildren++ )
	{
		if ( (*itChildren)->GetFactoryName( ) == factoryname )
		{
			return *itChildren;
		}

		BaseWindow* child = (*itChildren)->GetChildWindowByFactory( factoryname );
		if ( child )
		{
			return child;
		}
	}

	return NULL;
}

bool Core::BaseWindow::GetIsContainerWindow( )
{
	return m_IsContainerWindow;
}

void Core::BaseWindow::SetIsContainerWindow( bool val )
{
	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	if ( m_IsContainerWindow )
	{
		baseWindowFactory->GetFactoriesHolder()->RemoveObserver( 
			this, &BaseWindow::UpdateRegisteredWindows );
	}

	m_IsContainerWindow = val;

	if ( m_IsContainerWindow )
	{
		baseWindowFactory->GetFactoriesHolder()->AddObserver(
			this,
			&BaseWindow::UpdateRegisteredWindows );
	}
}

WIDGET_TYPE Core::BaseWindow::GetChildWindowType( )
{
	return m_ChildWindowType;
}

void Core::BaseWindow::SetChildWindowType( WIDGET_TYPE type )
{
	m_ChildWindowType = type;
}

void Core::BaseWindow::UpdateRegisteredWindows( )
{
	std::list<std::string> windowsList = SearchRegisteredChildWindows( );
	std::list<BaseWindow*> windowsListToRemove = m_Children;

	// Add new registered widgets
	std::list<std::string>::iterator it;
	for ( it = windowsList.begin( ) ; it != windowsList.end() ; it++ )
	{
		BaseWindow* baseWin = GetChildWindowByFactory( *it );
		if ( baseWin == NULL )
		{
			try
			{
				// Add child window
				baseWin = CreateChild( *it );

				if ( baseWin != NULL )
				{
					// Register new factory
					AddChild( baseWin );
				}
			}
			coreCatchExceptionsReportAndNoThrowMacro( Core::BaseWindow::UpdateRegisteredWindows );
		}
		else
		{
			// Remove a window that is in the list
			windowsListToRemove.remove( baseWin );
		}
	}

	// Remove unrgistered widgets
	std::list<BaseWindow*>::iterator itWin;
	for ( itWin = windowsListToRemove.begin( ) ; itWin != windowsListToRemove.end() ; itWin++ )
	{
		try
		{
			RemoveChild( *itWin );

			DestroyChild( *itWin );
		}
		coreCatchExceptionsReportAndNoThrowMacro( Core::BaseWindow::UpdateRegisteredWindows );
	}
}

std::list<std::string> Core::BaseWindow::SearchRegisteredChildWindows( )
{
	// Search toolbox widgets
	Core::BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );
	Core::BaseWindowFactorySearch::Pointer search = BaseWindowFactorySearch::New( );
	search->SetType( m_ChildWindowType );
	search->Update( );
	std::list<std::string> windowsList = search->GetFactoriesNames();
	return windowsList;
}

Core::BaseWindow* Core::BaseWindow::CreateChild( const std::string &factoryName )
{
	return NULL;
}

void Core::BaseWindow::DestroyChild( Core::BaseWindow* win )
{
}

std::list<Core::BaseWindow*> Core::BaseWindow::GetChildren( )
{
	return m_Children;
}

void Core::BaseWindow::AddChild( BaseWindow* baseWin )
{
	baseWin->SetParentWindow( this );
	m_Children.push_back( baseWin );
}

void Core::BaseWindow::RemoveChild( BaseWindow* baseWin )
{
	baseWin->SetParentWindow( NULL );
	m_Children.remove( baseWin );
}

void Core::BaseWindow::SetParentWindow( BaseWindow* baseWin )
{
	m_ParentWindow = baseWin;
}

Core::BaseWindow* Core::BaseWindow::GetParentWindow( )
{
	return m_ParentWindow;
}

void Core::BaseWindow::ShowChild( BaseWindow* child, bool show /*= true*/ )
{
}
