/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreProcessorExecutionQueue.h"

#include <boost/threadpool.hpp>

using namespace Core;
using namespace boost::threadpool;

class ProcessorExecutionQueue::Impl
{
public:
	//!
	Impl( size_t initial_threads = 0 ) : m_pool( initial_threads ) 
	{

	}

	~Impl()
	{
	}

	//! shutdown policy should be set to immediately to kill all threads when app is closed
	boost::threadpool::thread_pool<task_func, fifo_scheduler, static_size, resize_controller, immediately> m_pool;
};


Core::ProcessorExecutionQueue::ProcessorExecutionQueue()
{
	m_Impl = new Impl( 2 );
}

Core::ProcessorExecutionQueue::~ProcessorExecutionQueue()
{
	delete m_Impl;
	m_Impl = NULL;
}

void Core::ProcessorExecutionQueue::Schedule( ProcessorThread::Pointer processorThread )
{

	// Add to the thread pool
	bool success;
	success = m_Impl->m_pool.schedule( 
		boost::bind( &ProcessorThread::WorkerFunc, processorThread.GetPointer( ) ) );
	if ( !success )
	{
		throw Exceptions::Exception( 
			"ProcessorExecutionQueue::ExecuteMultiThreading", 
			"Cannot schedule the task" );
	}

}

void Core::ProcessorExecutionQueue::SetNumberOfThreads( long num )
{
	m_NumOfThreads = num;
	m_Impl->m_pool.size_controller().resize( num );
}

long Core::ProcessorExecutionQueue::GetNumOfThreads() const
{
	return m_NumOfThreads;
}

void Core::ProcessorExecutionQueue::StopAllThreads( )
{
	size_t numThread = m_Impl->m_pool.size( );
	delete m_Impl;
	m_Impl = NULL;
	m_Impl = new Impl( numThread );
}
