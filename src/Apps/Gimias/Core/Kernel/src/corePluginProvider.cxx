/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "corePluginProvider.h"

using namespace Core::Runtime;


Core::Runtime::PluginProvider::PluginProvider( void )
{
	m_ModuleFactory = NULL;
	m_EnableScanPlugins = true;
	m_EnableLoadPlugins = true;
}

Core::Runtime::PluginProvider::~PluginProvider( void )
{

}

void Core::Runtime::PluginProvider::Update( )
{
	if ( m_EnableScanPlugins )
	{
		ScanPlugins( );
	}
	if ( m_EnableLoadPlugins )
	{
		LoadPlugins( );
		AttachRunningPlugins( );
	}
}

void Core::Runtime::PluginProvider::AttachRunningPlugins()
{

}

std::string Core::Runtime::PluginProvider::GetPluginName( const std::string &caption )
{
	return caption;
}

ModuleFactory* Core::Runtime::PluginProvider::GetModuleFactory()
{
	return m_ModuleFactory;
}

void Core::Runtime::PluginProvider::EnablePluginsByName( 
	const std::list<std::string> names, blTagMap::Pointer plugins )
{
	if ( GetProperties().IsNull() )
	{
		return;
	}

	if ( plugins.IsNull( ) )
	{
		plugins = GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );
		if ( plugins.IsNull( ) )
		{
			return;
		}
	}

	blTagMap::Iterator it;
	for ( it = plugins->GetIteratorBegin() ; it != plugins->GetIteratorEnd() ; it++ )
	{
		Workflow::PluginNamesListType::iterator itFound;
		if ( plugins->GetTag( it )->GetValue().type() == typeid( bool ) )
		{
			// Convert pugin caption to plugin name
			std::string name = GetPluginName( plugins->GetTag( it )->GetName() );
			
			// Find it
			std::list<std::string>::const_iterator itFound;
			itFound = std::find( names.begin(), names.end(), name );

			// Enable/Disable
			plugins->GetTag( it )->SetValue( itFound != names.end() );
		}
		else if ( plugins->GetTag( it )->GetValue().type() == typeid( blTagMap::Pointer ) )
		{
			EnablePluginsByName( names, plugins->GetTag( it )->GetValueCasted<blTagMap::Pointer>() );
		}
	}
}

std::list<std::string> Core::Runtime::PluginProvider::GetLoadedPlugins( )
{
	return m_LoadedPlugins;
}

std::list<std::string> Core::Runtime::PluginProvider::GetSelectedPlugins(
	blTagMap::Pointer plugins /*= NULL */ )
{
	std::list<std::string> pluginsList;
	if ( plugins.IsNull( ) )
	{
		return pluginsList;
	}

	blTagMap::Iterator it;
	for ( it = plugins->GetIteratorBegin() ; it != plugins->GetIteratorEnd() ; it++ )
	{
		if ( plugins->GetTag( it )->GetValue().type() == typeid( bool ) )
		{
			if ( plugins->GetTag( it )->GetValueCasted<bool>() )
			{
				// Convert pugin caption to plugin name
				std::string name = GetPluginName( plugins->GetTag( it )->GetName() );
				pluginsList.push_back( name );
			}
		}
		else if ( plugins->GetTag( it )->GetValue().type() == typeid( blTagMap::Pointer ) )
		{
			std::list<std::string> names;
			names = GetSelectedPlugins( plugins->GetTag( it )->GetValueCasted<blTagMap::Pointer>() );
			pluginsList.insert( pluginsList.end( ), names.begin( ), names.end( ) );
		}
	}

	return pluginsList;
}

Core::Runtime::BasePlugin::Pointer Core::Runtime::PluginProvider::GetPlugin( const std::string &name )
{
	return NULL;
}

void Core::Runtime::PluginProvider::LoadPlugin( const std::string &name )
{
}

void Core::Runtime::PluginProvider::UnLoadPlugin( const std::string &name )
{
}

void Core::Runtime::PluginProvider::EnableLoadPlugins( bool enableLoadPlugins )
{
	m_EnableLoadPlugins = enableLoadPlugins;
}

void Core::Runtime::PluginProvider::EnableScanPlugins( bool enable )
{
	m_EnableScanPlugins = enable;
}

blTagMap::Pointer Core::Runtime::PluginProvider::GetVirtualChildPlugins( const std::string &name )
{
	return blTagMap::New( );
}

void Core::Runtime::PluginProvider::UnselectAllPlugins( blTagMap::Pointer plugins )
{
	if ( plugins.IsNull( ) )
	{
		plugins = GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );
	}

	blTagMap::Iterator it;
	for ( it = plugins->GetIteratorBegin() ; it != plugins->GetIteratorEnd() ; it++ )
	{
		if ( plugins->GetTag( it )->GetValue().type() == typeid( bool ) )
		{
			plugins->GetTag( it )->SetValue( false );
		}
		else if ( plugins->GetTag( it )->GetValue().type() == typeid( blTagMap::Pointer ) )
		{
			UnselectAllPlugins( plugins->GetTag( it )->GetValueCasted<blTagMap::Pointer>() );
		}
	}
}

void Core::Runtime::PluginProvider::AddPendingPlugin( const std::string &name )
{
	boost::lock_guard<boost::mutex> lock_g(m_PendignPluginsMutex); 
	
	m_PendingPlugins.push_back( name );
}

void Core::Runtime::PluginProvider::RemovePendingPlugin( const std::string &name )
{
	boost::lock_guard<boost::mutex> lock_g(m_PendignPluginsMutex); 
	
	m_PendingPlugins.remove( name );

	// Notify that a plugin has been removed
	m_PendingPluginRemoved.notify_all( );
}

void Core::Runtime::PluginProvider::WaitPendingPlugins( )
{
	boost::lock_guard<boost::mutex> lock_g(m_PendignPluginsMutex); 

	while ( !m_PendingPlugins.empty( ) )
		m_PendingPluginRemoved.wait( m_PendignPluginsMutex );
}

