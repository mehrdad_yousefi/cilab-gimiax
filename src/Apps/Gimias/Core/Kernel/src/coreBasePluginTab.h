/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreBasePluginTab_H
#define coreBasePluginTab_H

#include "coreObject.h"
#include "coreBaseWindow.h"
#include "coreWindowConfig.h"

namespace Core
{

	/** 
	\brief Interface for plugin tab page

	\ingroup gmKernel
	\author Xavi Planes
	\date Sept 2011
	*/
	class GMKERNEL_EXPORT BasePluginTab : public Core::Object
	{
	public:

		//! Caption
		virtual std::string GetCaption() = 0;

		//! Add a window to the plugin tab page using the WindowConfig
		virtual void AddWindow( Core::BaseWindow* baseWindow, Core::WindowConfig config ) = 0;

		//! Remove a window from the plugin tab page
		virtual void RemoveWindow( const std::string &factoryName ) = 0;

		//!
		virtual void ShowWindow( const std::string &label, bool show = true ) = 0;

		//! Create a backup of the layout configuration
		virtual void BackupLayoutConfiguration( ) = 0;

		//! Call SetLayoutConfiguration but keep initial Show/hide state
		virtual void UpdateLayoutConfiguration( const std::string &strConfig ) = 0;

		//!
		virtual Core::RenderingTreeManager::Pointer GetRenderingTreeManager() const = 0;
	};


}

#endif
