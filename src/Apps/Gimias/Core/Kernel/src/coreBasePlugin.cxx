/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreBasePlugin.h"

#include "tinyxml.h"

using namespace Core::Runtime;
int BasePlugin::m_PluginNameIndex = 0;

BasePlugin::BasePlugin(void)
{
	char name[128];
	sprintf( name, "Plugin%d", m_PluginNameIndex++ );
	m_Name = name;
	m_Caption = m_Name;
}

BasePlugin::~BasePlugin(void)
{
}

void Core::Runtime::BasePlugin::ReadXML()
{
	TiXmlDocument doc( m_LibraryPath + "/plugin.xml" );
	if (!doc.LoadFile()) return;

	TiXmlHandle hDoc(&doc);
	TiXmlElement* pElem;
	TiXmlHandle hRoot(0);

	pElem=hDoc.FirstChildElement().Element();
	if (!pElem) return;
	m_Name = pElem->Attribute("name");
	if ( pElem->Attribute("caption") )
	{
		m_Caption = pElem->Attribute("caption");
	}
	else 
	{
		m_Caption = m_Name;
	}
	hRoot=TiXmlHandle(pElem);

	pElem=hRoot.FirstChild( "depends" ).FirstChild().Element();
	for( pElem; pElem; pElem=pElem->NextSiblingElement())
	{
		const char *pName=pElem->Attribute("name");
		m_Dependencies.push_back( pName );
	}

	pElem=hRoot.FirstChild( "license" ).Element( );
	if ( pElem )
	{
		if ( pElem->Attribute( "date" ) )
		{
			m_ExpireDate = pElem->Attribute( "date" );
		}
		if ( pElem->Attribute( "signature" ) )
		{
			m_Signature = pElem->Attribute( "signature" );
		}
	}
}

void Core::Runtime::BasePlugin::WriteXML( )
{
	TiXmlDocument doc;
	TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "", "" );
	doc.LinkEndChild( decl );

	TiXmlElement * plugin = new TiXmlElement( "plugin" );
	plugin->SetAttribute("name", m_Name);
	plugin->SetAttribute("caption", m_Caption);
	doc.LinkEndChild( plugin );

	TiXmlElement * pluginDependencies = new TiXmlElement( "depends" );
	plugin->LinkEndChild( pluginDependencies );

	std::list<std::string>::iterator it;
	for ( it = m_Dependencies.begin( ) ; it != m_Dependencies.end( ) ; it++ )
	{
		TiXmlElement * pluginName;
		pluginName = new TiXmlElement( "plugin" );
		pluginName->SetAttribute("name", *it);
		pluginDependencies->LinkEndChild( pluginName );
	}

	TiXmlElement * pluginTrial = new TiXmlElement( "license" );
	pluginTrial->SetAttribute( "date", m_ExpireDate );
	pluginTrial->SetAttribute( "signature", m_Signature );
	plugin->LinkEndChild( pluginTrial );

	doc.SaveFile( m_LibraryPath + "/plugin.xml" );
}

std::list<std::string> Core::Runtime::BasePlugin::GetDependencies() const
{
	return m_Dependencies;
}

std::string Core::Runtime::BasePlugin::GetName() const
{
	return m_Name;
}

void Core::Runtime::BasePlugin::SetName( const std::string &name )
{
	m_Name = name;
}

std::string Core::Runtime::BasePlugin::GetCaption() const
{
	return m_Caption;
}

void Core::Runtime::BasePlugin::SetCaption( std::string val )
{
	m_Caption = val;
}

std::string Core::Runtime::BasePlugin::GetExpireDate() const
{
	return m_ExpireDate;
}

void Core::Runtime::BasePlugin::SetExpireDate( const std::string &val )
{
	m_ExpireDate = val;
}

std::string Core::Runtime::BasePlugin::GetSignature() const
{
	return m_Signature;
}

void Core::Runtime::BasePlugin::SetSignature( const std::string &val )
{
	m_Signature = val;
}

std::string Core::Runtime::BasePlugin::GetLibraryPath() const
{
	return m_LibraryPath;
}
