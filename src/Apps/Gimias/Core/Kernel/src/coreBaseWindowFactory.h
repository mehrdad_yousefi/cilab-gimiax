/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreBaseWindowFactory_H
#define _coreBaseWindowFactory_H

#include "coreObject.h"
#include "coreBaseWindow.h"
#include "coreWindowConfig.h"
#include <typeinfo>
#include "ModuleDescription.h"

class wxWindow;

namespace Core
{

/**
\brief Base Factory for BaseWindow

\ingroup gmKernel
\author Xavi Planes
\date 10 May 2010
*/
class GMKERNEL_EXPORT BaseWindowFactory : public Core::SmartPointerObject
{
public:
	coreClassNameMacro(Core::BaseWindowFactory);
	coreDeclareSmartPointerTypesMacro(
		Core::BaseWindowFactory, 
		Core::SmartPointerObject);

	typedef std::map<std::string,ModuleDescription> ModuleMapType;

	//!
	void SetParent( wxWindow* parent );
	wxWindow* GetParent() const;

	//!
	void SetWindowId( int id );
	int GetWindowId( ) const;

	//!
	std::string GetWindowName() const;
	void SetWindowName(std::string val);

	//!
	std::string GetBitmapFilename() const;
	void SetBitmapFilename(std::string val);

	//! Get Unique Identifier
	virtual const char* GetInstanceClassName( ) const = 0;

	//!
	virtual Core::BaseWindow* CreateBaseWindow( ) = 0;

	/** Use a key to store multiple modules at different location
	If module is not found, return an empty one
	*/
	ModuleDescription &GetModule( const std::string &key );
	virtual void SetModule( const std::string &key, ModuleDescription val );

	//!
	bool FindModule( const std::string &key );

	//!
	void RemoveModule( const std::string &key );

	//!
	ModuleDescription &GetDefaultModule( );

	//!
	ModuleMapType &GetModules();

	//!
	std::string GetDefaultModuleKey() const;
	void SetDefaultModuleKey(std::string val);


protected:
	//!
	BaseWindowFactory( );

protected:
	//!
	wxWindow* m_Parent;
	//!
	int m_WindowId;
	//!
	std::string m_WindowName;
	//
	std::string m_BitmapFilename;
	//!
	ModuleMapType m_Modules;
	//! The first module that has been set
	std::string m_DefaultModuleKey;
};

/**
\brief Base Factory for BaseWindow

\ingroup gmKernel
\author Xavi Planes
\date 10 May 2010
*/
class GMKERNEL_EXPORT BaseWindowIOFactory : public BaseWindowFactory
{
public:
	coreClassNameMacro(Core::BaseWindowIOFactory);
	coreDeclareSmartPointerTypesMacro(Core::BaseWindowIOFactory, BaseWindowFactory);

	//! Check if type and ext matches this Factory and type is reading is true
	virtual bool CheckType( 
		Core::DataEntityType type, 
		const std::string &ext, 
		bool isReading,
		Core::DataEntity::Pointer dataEntity );

protected:
	//!
	BaseWindowIOFactory( );
protected:
	//!
	Core::DataEntityType m_DataEntitytype;
	//!
	std::string m_Extension;
};


} // namespace Core

/**
\brief NewBase() function
\ingroup gmKernel
\author Xavi Planes
\date 8 Oct 2010
*/
#define coreCommonFactoryFunctionsNewBase() \
	static BaseWindowFactory::Pointer NewBase(void) \
		{ \
		Pointer p(New()); \
		return p.GetPointer(); \
		}

/**
\brief CreateWindowCommon( ) function
\ingroup gmKernel
\author Xavi Planes
\date 8 Oct 2010
*/
#define coreCommonFactoryFunctionsCreateWindowCommon() \
		window->SetFactoryName( GetNameOfClass( ) );\
		window->SetBitmapFilename( GetBitmapFilename( ) ); \
		window->SetModule( &GetDefaultModule( ) ); \
		if ( !GetWindowName( ).empty( ) ){ \
			wxWindow* windowBase = window; \
			windowBase->SetName( GetWindowName( ) ); \
		} \
		return window; \
	}

/**
\brief Retrieve name of instance window
\ingroup gmKernel
\author Xavi Planes
\date Oct 2011
*/
#define coreCommonFactoryFunctionsClassNameMacro(classname) \
	virtual const char* GetInstanceClassName(void) const { return #classname; }


/**
\brief CreateBaseWindow( ) function
\ingroup gmKernel
\author Xavi Planes
\date 8 Oct 2010
*/
#define coreCommonFactoryFunctionsCreateWindow(className) \
	coreCommonFactoryFunctionsClassNameMacro( className ) \
	virtual Core::BaseWindow* CreateBaseWindow( ) \
	{ \
		className* window = new className( GetParent(), GetWindowId( ) );\
		coreCommonFactoryFunctionsCreateWindowCommon( )

/**
\brief CreateBaseWindow() function with 1 param
\ingroup gmKernel
\author Xavi Planes
\date 8 Oct 2010
*/
#define coreCommonFactoryFunctionsCreateWindow1param(className) \
	coreCommonFactoryFunctionsClassNameMacro( classname ) \
	virtual Core::BaseWindow* CreateBaseWindow( ) \
	{ \
		className* window = new className( m_p1, GetParent(), GetWindowId( ) );\
		coreCommonFactoryFunctionsCreateWindowCommon( )

/**
\brief CreateBaseWindow() function with 2 params
\ingroup gmKernel
\author Xavi Planes
\date 8 Oct 2010
*/
#define coreCommonFactoryFunctionsCreateWindow2param(className) \
	coreCommonFactoryFunctionsClassNameMacro( classname ) \
	virtual Core::BaseWindow* CreateBaseWindow( ) \
	{ \
		className* window = new className( m_p1, m_p2, GetParent(), GetWindowId( ) );\
		coreCommonFactoryFunctionsCreateWindowCommon( )


/**
\brief Common factory functions definitions
\note For template classes like SimpleProcessingWidget, we need to use
typeid, to be able to identify each class uniquelly
\ingroup gmKernel
\author Xavi Planes
\date 10 May 2010
*/
#define coreCommonFactoryFunctions(className) \
		coreCommonFactoryFunctionsNewBase( ) \
		coreCommonFactoryFunctionsCreateWindow( className )

/**
\brief Create a BaseWindowFactory derived class
\ingroup gmKernel
\author Xavi Planes
\date 10 May 2010
*/
#define coreDefineBaseWindowFactory(className) \
	class Factory : public Core::BaseWindowFactory \
	{ \
	public:  \
		coreDeclareSmartPointerTypesMacro(Factory,BaseWindowFactory) \
		coreFactorylessNewMacro(Factory) \
		coreClassNameMacro(className##Factory)\
		coreCommonFactoryFunctions( className )\
	};

/**
\brief Create a BaseWindowFactory derived class
\ingroup gmKernel
\author Xavi Planes
\date 10 May 2010
*/
#define coreDefineBaseWindowFactory1param(className,param1) \
	class Factory : public Core::BaseWindowFactory \
	{ \
	public:  \
		coreDeclareSmartPointerTypesMacro(Factory,BaseWindowFactory) \
		coreFactorylessNewMacro1Param( Factory, param1 ) \
		coreClassNameMacro(className##Factory)\
		coreCommonFactoryFunctionsCreateWindow1param( className ) \
    private: \
        param1 m_p1; \
        Factory(param1 p1) { \
            m_p1 = p1; \
        } \
	};

/**
\brief Create a BaseWindowFactory derived class
\ingroup gmKernel
\author Xavi Planes
\date 10 May 2010
*/
#define coreDefineBaseWindowFactory2param(className,param1,param2) \
	class Factory : public Core::BaseWindowFactory \
	{ \
	public:  \
		coreDeclareSmartPointerTypesMacro(Factory,BaseWindowFactory) \
		coreFactorylessNewMacro2Param( Factory, param1, param2 ) \
		coreClassNameMacro(className##Factory)\
		coreCommonFactoryFunctionsCreateWindow2param( className ) \
    private: \
        param1 m_p1; \
        param2 m_p2; \
        Factory(param1 p1, param2 p2) { \
            m_p1 = p1; \
            m_p2 = p2; \
        } \
	};


/**
\brief Create a BaseWindowFactory derived class for reader dialogs
- dataEntityType is the DataEntityType that this dialog can manage
- ext is the extension that this dialog can manage
\ingroup gmKernel
\author Xavi Planes
\date 21 June 2010
*/
#define coreDefineBaseWindowIOFactory(className, dataEntityType, ext) \
	class Factory : public Core::BaseWindowIOFactory \
	{ \
	public:  \
		coreDeclareSmartPointerTypesMacro(Factory,BaseWindowIOFactory) \
		coreFactorylessNewMacro(Factory) \
		coreClassNameMacro(className##Factory)\
		coreCommonFactoryFunctions( className )\
	protected:\
		Factory( ){\
			m_DataEntitytype = dataEntityType;\
			m_Extension = ext;\
		}\
	}; \


#endif // coreBaseWindowFactory_H
