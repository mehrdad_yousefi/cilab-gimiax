/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef corePluginProvidersUpdater_H
#define corePluginProvidersUpdater_H

#include "gmKernelWin32Header.h"
#include "coreObject.h"
#include "corePluginProvider.h"
#include <boost/thread/mutex.hpp>

namespace Core
{
/** 
\brief Update plugin providers using multi-threading or the current thread.

When all processors finish, sends an event.
Manages messages and outputs events

\ingroup gmWidgets
\author Xavi Planes
\date Jan 2013
*/
class GMKERNEL_EXPORT PluginProvidersUpdater : public Core::SmartPointerObject
{
public:
	enum STATUS_TYPE
	{
		STATUS_IDLE,
		STATUS_PROCESSING,
		STATUS_FINISHED
	};

	coreDeclareSmartPointerClassMacro(PluginProvidersUpdater, Core::SmartPointerObject);

	//!
	PluginProvidersUpdater();

	//!
	~PluginProvidersUpdater();

	//!
	void AddProvider( Core::Runtime::PluginProvider::Pointer provider );
	//!
	void RemoveProvider( Core::Runtime::PluginProvider::Pointer provider );
	//!
	void SelectAllProviders( );
	//!
	void SelectProvider( const std::string &name );

	//!
	void EnableScanPlugins( bool enable );
	//!
	void EnableLoadPlugins( bool enable );
	//!
	void EnableMultithreading( bool enable );
	//!
	bool GetEnableScanPlugins( );
	//!
	bool GetEnableLoadPlugins( );
	//!
	bool GetEnableMultithreading( );

	//! 
	void Update( );

	/** Status will change when Update() function is called and when
	all processors have finished processing
	*/
	Core::IntHolderType::Pointer GetStatusHolder( );

	//! The current plugin name being created
	std::string GetCurrentPluginName() const;
	void SetCurrentPluginName(const std::string &name);

protected:

	//!
	void UpdatePluginProvider( const std::string &name );

	/** Add observers to outputs 
	Called when SetNumberOfOutputs() is called in the provider, before loading plugins
	and after all plugins are loaded
	*/
	void OnModifiedProvider( SmartPointerObject* object );

	/** Check if provider execution has finished (only called in multithreading)
	*/
	void OnProviderStatus( SmartPointerObject* object );

	/** Load/Unload plugin (called when UpdateOutput() is called in the provider)
	\note This is the only handler executed by the main GUI thread
	*/
	void OnModifiedProviderOutput( DataEntity::Pointer dataEntity );

private:
	//!
	Core::IntHolderType::Pointer m_StatusHolder;
	//!
	bool m_EnableScanPlugins;
	//!
	bool m_EnableLoadPlugins;
	//!
	bool m_EnableMultithreading;
	//!
	bool m_SelectAllProviders;

	//! Store the selected providers and the status (pending,finished)
	std::map<std::string,int> m_SelectedProviders;
	//! List of all providers
	std::map<std::string,Core::Runtime::PluginProvider::Pointer> m_Providers;
	//! Protect access to m_SelectedProviders and m_Providers
	boost::mutex m_Mutex;
	//!
	std::string m_CurrentPluginName;
};
}

#endif // corePluginProvidersUpdater_H
