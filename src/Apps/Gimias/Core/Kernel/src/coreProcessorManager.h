/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreProcessorManager_H
#define _coreProcessorManager_H

// CoreLib
#include "gmKernelWin32Header.h"
#include "coreObject.h"
#include "coreProcessorThread.h"
#include "coreProcessorExecutionQueue.h"

namespace Core{

/**
\brief Executes Processors using thread pool pattern or current thread, 
depending on the configuration of the BaseProcessor

It has a complete list of all ProcessorThread and a set of execution queues
that can be configured independently.

To execute multiple instances of a processor, it needs to implement 
the copy function, otherwise, it can only be executed once. 

When a processor is added to the ProcessorManager using Execute() method, this is the sequence of operations:
- If multi threading is enabled:
 - Creates a copy of the processor
 - If copy fails, checks if the processor is already running. If it is, throw an exception
 - Creates a ProcessorThread and adds it to the list
 - Add the ProcessorThread to the ProcessorExecutionQueue 
- If multi threading is not enabled:
 - Executes the processor 

\ingroup gmKernel
\author Xavi Planes
\date 17 01 2011
*/
class GMKERNEL_EXPORT ProcessorManager : public Core::SmartPointerObject
{
public:
	//!
	coreDeclareSmartPointerClassMacro(Core::ProcessorManager, Core::SmartPointerObject);
	//!
	typedef std::list<ProcessorThread::Pointer> ProcessorThreadListType;
	typedef std::map<std::string,ProcessorExecutionQueue::Pointer> ProcessorExecutionQueueMapType;

	//! Initialize from settings
	void Initialize( );

	/** Execute this processor in background or current thread
	depending on the Multithreading attribute of the processor
	\param [in] queueName is only used for multithreading. If queueName 
	is not found, create a new one.
	*/
	ProcessorThread::Pointer Execute( 
		BaseProcessor::Pointer processor, const std::string &queueName = "Default" );

	/** Create processor thread and add it to the m_ProcessorList
	If ProcessorThread is already created, return the instance
	*/
	ProcessorThread::Pointer CreateProcessorThread( BaseProcessor::Pointer processor );

	/** If input parameter is -1, the parameter will not be used for search
	\param [in] uid UID of the processorThread
	\param [in] state state of the processorThread
	\param [in] processor processor pointer of the processorThread (only when is in execution)
	*/
	Core::ProcessorThread::Pointer GetProcessorThread( 
		long uid = -1, int state = -1, BaseProcessor::Pointer processor = NULL );

	//!
	ProcessorThreadListType GetProcessorThreadList() const;

	//!
	void EnableMultithreading( bool val );
	bool GetEnableMultiThreading() const;

	//! Return true if all processors are finished
	bool AbortAll( );

	//! Return true if all processors are detached
	bool DetachAll( );

	//!
	ProcessorExecutionQueue::Pointer GetExecutionQueue( const std::string &name = "Default" );

	//!
	void AddExecutionQueue( const std::string &name );

	//!
	void StopAllExecutionQueues( );

protected:
	//!
	ProcessorManager(void);

	//!
	virtual ~ProcessorManager(void);

	/** Execute this processor in background
	If queue with name queueName is not found, create a new one
	*/
	ProcessorThread::Pointer ExecuteMultiThreading( BaseProcessor::Pointer processor, const std::string &queueName );

	//! Execute this processor in background
	void ExecuteCurrentThread( BaseProcessor::Pointer processor );

private:
	//!
	ProcessorManager( const Self& );

	//!
	void operator=(const Self&);


protected:
	//!
	ProcessorExecutionQueueMapType m_ExecutionQueueMap;
	//!
	ProcessorThreadListType m_ProcessorList;
	//!
	bool m_EnableMultiThreading;
};

} // namespace Core{

#endif // _coreProcessorManager_H

