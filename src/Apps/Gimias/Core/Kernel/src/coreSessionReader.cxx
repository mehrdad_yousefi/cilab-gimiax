/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreSessionReader.h"
#include "coreDataEntityReader.h"
#include "coreDataEntityHelper.h"
#include "coreReportExceptionMacros.h"

#include "blTextUtils.h"
#include "blXMLTagMapReader.h"

#include "itksys/SystemTools.hxx"

#include <boost/archive/xml_iarchive.hpp>
#include <boost/filesystem/operations.hpp>

#include <fstream>

using namespace Core::IO;

SessionReader::SessionReader()
{
	m_RemoveAllData = false;
	// Old session file format (boost)
	m_ValidExtensionsList.push_back( ".xml" );
	// New session file format (blTagMap)
	m_ValidExtensionsList.push_back( ".gses" );
	m_ValidTypesList.push_back( SessionTypeId );
}

SessionReader::~SessionReader()
{
}

void SessionReader::Update()
{	
	ReadMetaData();

	ReadData( );
}

void SessionReader::ReadData()
{	
	if( GetFileName( ).empty() )
		return;

	Core::DataEntityList::Pointer dataList = Core::Runtime::Kernel::GetDataContainer()->GetDataEntityList();
	if(dataList.IsNull())
		return;

	if ( m_RemoveAllData )
	{
		dataList->RemoveAll();
	}

	blTagMap::Pointer session = blTagMap::New( );
	if ( GetExtension( ) == ".xml" )
	{
		//read session object
		Core::Session::Pointer oldSession = Core::Session::New( );
		std::ifstream readSession( GetFileName( ).c_str() );
		boost::archive::xml_iarchive xmlArchive(readSession);
		xmlArchive >> boost::serialization::make_nvp("session", *oldSession);

		// Convert from session object to blTagMap
		blTagMap::Pointer dataTag = blTagMap::New( );
		session->AddTag( "Data", dataTag );
		for(size_t i = 0; i < oldSession->GetDataList().size(); i++)
		{
			blTagMap::Pointer child = ConvertSession( oldSession->GetDataList().at(i) );
			dataTag->AddTag( child->FindTagByName( "Name" )->GetValueAsString( ), child );
		}
	}
	else if ( GetExtension( ) == ".gses" )
	{
		// Try to read session
		blXMLTagMapReader::Pointer reader = blXMLTagMapReader::New( );
		reader->SetFilename( GetFileName( ).c_str() );
		reader->Update( );
		session = reader->GetOutput( );
	}

	if ( session.IsNull( ) )
	{
		return;
	}

	// Create session node
	Core::DataEntity::Pointer sessionNode;
	if ( GetNumberOfOutputs( ) > 0 )
	{
		sessionNode = GetOutputDataEntity( 0 );
	}
	if ( sessionNode.IsNull( ) )
	{
		sessionNode = Core::DataEntity::New( Core::SessionTypeId );
	}
	std::string filename = itksys::SystemTools::GetFilenameWithoutLastExtension( GetFileName( ).c_str() );
	sessionNode->GetMetadata()->SetName( filename );

	// Pass parameters to session node
	if ( GetProperties( )->GetTag( "OpenSession" ).IsNotNull( ) )
	{
		sessionNode->GetMetadata()->AddTag( "OpenSession", true );
	}

	// Read views property
	sessionNode->AddTimeStep( session );
	m_DataList.push_back( sessionNode );

	// Load Views
	LoadViews( sessionNode );

	// Add data to the session node
	blTagMap::Pointer dataTag = session->GetTagValue<blTagMap::Pointer>( "Data" );
	if ( dataTag.IsNotNull( ) )
	{
		blTagMap::ListIterator itData;
		for(itData = dataTag->ListBegin() ; itData != dataTag->ListEnd() ; itData++)
		{
			try
			{
				blTag::Pointer tag = dataTag->GetTag( itData );
				blTagMap::Pointer tagMap = tag->GetValueCasted<blTagMap::Pointer>( );
				if ( tagMap.IsNotNull( ) )
				{
					LoadLightData( tagMap, sessionNode );
				}
			}
			coreCatchExceptionsLogAndNoThrowMacro( SessionReader::Update )
		}
	}


	// Add all data as output
	SetNumberOfOutputs( m_DataList.size( ) );
	std::list<Core::DataEntity::Pointer>::iterator it;
	int i = 0;
	for ( it = m_DataList.begin( ) ; it != m_DataList.end( ) ; it++ )
	{
		SetOutputDataEntity( i++, *it );
	}
}

void SessionReader::LoadLightData( blTagMap::Pointer data, Core::DataEntity::Pointer father )
{
	//create DataEntity
	std::string dirPath = itksys::SystemTools::GetFilenamePath( GetFileName( ) );
	std::vector< std::string > paths;
	
	// This is not a file entry
	if ( data->GetTag( "RelativePath" ).IsNull( ) )
	{
		return;
	}

	std::string relativePath = data->GetTag( "RelativePath" )->GetValueAsString( );
	int numberOfTimeSteps = 0;
	if ( data->GetTag( "NumberOfTimeSteps" ).IsNotNull( ) )
		numberOfTimeSteps = data->GetTag( "NumberOfTimeSteps" )->GetValueCasted<int>( );
	std::string filePath;
	filePath = dirPath +"/"+ relativePath;
	paths.push_back(filePath);

	// Read meta data if any
	Core::IO::DataEntityReader::Pointer reader;
	Core::DataEntity::Pointer currentDataEntity;
	bool readAllData = true;
	try
	{
		reader = Core::IO::DataEntityReader::New( );
		reader->SetFileNames( paths );
		reader->SetLightRead( true );
		reader->Update( );

		if ( reader->GetNumberOfOutputs() && reader->GetOutputDataEntity( 0 ).IsNotNull( ) )
		{
			// Read all data if this object is not a session
			currentDataEntity = reader->GetOutputDataEntity( 0 );
			readAllData = currentDataEntity->GetType( ) != Core::SessionTypeId && !m_LightRead;
		}
	}
	catch( ... ){}

	// Read all data
	if ( readAllData )
	{
		try
		{
			// Reset previous data
			reader->SetNumberOfOutputs( 0 );
			reader->SetFileNames( paths );
			reader->SetLightRead( false );
			reader->Update( );
		}catch(...)
		{
			// If DataEntity cannot be loaded, load children as child of father
			currentDataEntity = father;
		}
	}

	// Add outputs to the list
	if ( reader->GetNumberOfOutputs() && reader->GetOutputDataEntity( 0 ).IsNotNull( ) )
	{
		for ( size_t i = 0 ; i < reader->GetNumberOfOutputs() ; i++ )
		{
			// Set father
			Core::DataEntity::Pointer dataEntity = reader->GetOutputDataEntity( i );
			if ( dataEntity->GetFather( ).IsNull( ) )
			{
				dataEntity->SetFather(father);
			}
			if ( i == 0 )
			{
				currentDataEntity = dataEntity;
				// The other childern read the time steps in the session reader
				dataEntity->GetMetadata()->AddTag( "NumberOfTimeSteps", numberOfTimeSteps );
			}
			m_DataList.push_back( dataEntity );
		}
	}

	//load children
	blTagMap::Iterator itChildren;
	blTagMap::Pointer children = data->GetTagValue<blTagMap::Pointer>( "Children" );
	if ( children.IsNotNull( ) )
	{
		for(itChildren = children->GetIteratorBegin() ; itChildren != children->GetIteratorEnd() ; itChildren++)
		{
			blTagMap::Pointer tagMap = itChildren->second->GetValueCasted<blTagMap::Pointer>( );
			if ( tagMap.IsNotNull( ) )
			{
				LoadLightData(tagMap, currentDataEntity);
			}
		}
	}
}

bool Core::IO::SessionReader::GetRemoveAllData() const
{
	return m_RemoveAllData;
}

void Core::IO::SessionReader::SetRemoveAllData( bool val )
{
	m_RemoveAllData = val;
}

blTagMap::Pointer Core::IO::SessionReader::ConvertSession( 
	Core::LightData* lightData )
{
	blTagMap::Pointer data = blTagMap::New( );
	data->AddTag( "Name", lightData->GetName( ) );
	data->AddTag( "Filepath", lightData->GetFilepath( ) );
	data->AddTag( "RelativePath", lightData->GetRelativePath( ) );
	data->AddTag( "NumberOfTimeSteps", int( lightData->GetNumOfTimeSteps( ) ) );

	blTagMap::Pointer children = blTagMap::New();
	for(size_t i=0; i<lightData->GetChildren().size(); i++)
	{
		blTagMap::Pointer child = ConvertSession( lightData->GetChildren().at(i) );
		std::string name = child->FindTagByName( "Name", false )->GetValueAsString( );
		children->AddTag( name, child );
	}
	if ( lightData->GetChildren().size() )
	{
		data->AddTag( "Children", children );
	}

	return data;
}

void Core::IO::SessionReader::LoadViews( 
	Core::DataEntity::Pointer session )
{
	if ( session.IsNull( ) )
		return;

	// Get tagmap
	blTagMap::Pointer sessionTagMap;
	session->GetProcessingData( sessionTagMap );
	if ( sessionTagMap.IsNull( ) )
		return ;

	// Check views dir
	std::string dirPath = itksys::SystemTools::GetFilenamePath( GetFileName( ) );
	std::string viewsDir = dirPath + "/Views";
	Core::IO::Directory::Pointer directory = Core::IO::Directory::New( );
	directory->SetDirNameFullPath( viewsDir );
	Core::IO::FileNameList viewsDirs = directory->GetContents( );
	if ( viewsDirs.empty( ) )
		return;

	// Iterate over each view dir
	Core::IO::FileNameList::iterator itViewDir;
	blTagMap::Pointer views = blTagMap::New( );
	for ( itViewDir = viewsDirs.begin(); itViewDir != viewsDirs.end(); ++itViewDir)				
	{
		// Get last dir component to buid view filename
		std::vector<std::string> components;
		itksys::SystemTools::SplitPath( itViewDir->c_str( ), components );
		std::string viewName = components.at( components.size( ) - 1 );
		std::string viewFile = *itViewDir + "/" + viewName + ".gvw";

		// Read file
		blXMLTagMapReader::Pointer reader = blXMLTagMapReader::New( );
		reader->SetFilename( viewFile.c_str() );
		reader->Update( );
		blTagMap::Pointer view = reader->GetOutput( );
		if ( view.IsNull( ) )
			continue;

		// Create preview
		Core::DataEntityPreview::Pointer preview;
		BaseFactory::Pointer factory = FactoryManager::Find( DataEntityPreview::GetNameClass() );
		if ( factory.IsNotNull( ) )
			preview = dynamic_cast<DataEntityPreview*> ( factory->CreateInstance( ).GetPointer( ) );
		if ( preview.IsNull( ) )
			return;

		// Load preview image
		preview->SetName( viewName );
		preview->SetFolder( *itViewDir );
		preview->Load( );
		view->AddTag( "Image", preview->GetImage( ) );

		// Add view
		views->AddTag( viewName, view );
	}
	sessionTagMap->AddTag( "Views", views );
}

