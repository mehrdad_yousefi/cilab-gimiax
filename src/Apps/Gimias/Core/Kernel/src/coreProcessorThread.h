/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreProcessorThread_H
#define _coreProcessorThread_H

// CoreLib
#include "gmKernelWin32Header.h"
#include "coreObject.h"
#include "coreBaseProcessor.h"
#include "coreCallbackObserver.h"

namespace Core{

/**
\brief Execute a BaseProcessor using multi threading restrictions.
Each instance has a Unique identifier to allow identification of different
executions of the same processor.

A CallbackObserver instance will be created with the property "GUI" 
and will observe the BaseProcessor update callback.
This allows to send an event to the main GUI thread to refresh the status.

CallbackObserver has a reference to the UpdateCallback, so after detaching the
processor, the UpdateCallback can be accessed.

\ingroup gmKernel
\author Xavi Planes
\date 17 01 2011
*/
class GMKERNEL_EXPORT ProcessorThread : public Core::SmartPointerObject
{
public:
	enum STATE
	{
		STATE_PENDING = 1,
		STATE_ACTIVE = 2,
		STATE_FINISHED = 4
	};
	//!
	coreDeclareSmartPointerClassMacro1Param(
		Core::ProcessorThread, 
		Core::SmartPointerObject,
		BaseProcessor::Pointer )

	//!
	BaseProcessor::Pointer GetProcessor() const;

	//! Worker function that executes the processor
	void WorkerFunc( );

	//!
	STATE GetState() const;
	long GetUID() const;

	//!
	long GetProcessorUID() const;
	std::string GetProcessorName() const;

	//!
	UpdateCallback::Pointer GetUpdateCallback( );

	/**
	Depending on the state will return the appropriate message
	*/
	std::string GetStatusMessage( );

	//! Time elapsed since executed (in seconds)
	double GetDuration( );

	//! Time elapsed since executed (in h min sec)
	std::string GetDurationAsString( );

	//! Initialize started time for attached running processors
	double GetStartedTime() const;
	void SetStartedTime(double val);

	//!
	double GetFinishedTime() const;
	void SetFinishedTime(double val);

	//!
	bool GetShowProcessingMessage( ) const;
	void SetShowProcessingMessage( bool val );

protected:
	//! Processor to execute and a mutex to lock processor resources
	ProcessorThread(BaseProcessor::Pointer processor );

	//!
	virtual ~ProcessorThread(void);

	//!
	void LockResources( bool lock );

private:
	//!
	ProcessorThread( const Self& );

	//!
	void operator=(const Self&);

	//!
	void AddUpdateCallbackObserver( );

	//!
	void UpdateProcessor( );

	//!
	void RemoveUpdateCallbackObserver( );

protected:
	//!
	BaseProcessor::Pointer m_Processor;
	//!
	STATE m_State;
	//!
	long m_ProcessorUID;
	//!
	std::string m_ProcessorName;
	//!
	long m_UID;
	//! ID count of already created instances
	static long m_IdCount;
	//!
	CallbackObserver::Pointer m_CallbackObserver;
	//! started time
	double m_StartedTime;
	//! started time
	double m_FinishedTime;
	//! When executed in current thread, show processing message
	bool m_ShowProcessingMessage;
};

} // namespace Core{

#endif // _coreProcessorThread_H


