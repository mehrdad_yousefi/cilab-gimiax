/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#ifndef corePluginProvider_H
#define corePluginProvider_H

#include "gmKernelWin32Header.h"
#include "coreObject.h"
#include "ModuleFactory.h"
#include "coreBasePlugin.h"
#include "coreBaseProcessor.h"

#include "boost/thread/condition.hpp"
#include "boost/thread/mutex.hpp"

namespace Core
{
namespace Runtime
{

/**
\brief Interface class for all plugin providers. A plugin provider 
provides plugins and loads them.

The property "Plugins" in the tag map that you can get calling
GetProperties( ), contains the list of plugins that are provided by this provider.
Each tag has the caption of the plugin and a value true/false that
specifies if the plugin will be loaded.

\ingroup gmKernel
\author Xavi Planes
\date Feb 2011
*/
class GMKERNEL_EXPORT PluginProvider : public Core::BaseProcessor
{
public:
	coreDeclareSmartPointerTypesMacro(Core::Runtime::PluginProvider,SmartPointerObject)
	coreClassNameMacro(Core::Runtime::PluginProvider)
	coreTypeMacro(Core::Runtime::PluginProvider,SmartPointerObject)

	//!
	void Update( );

	//! Create BaseFrontEndPlugin instances and attach them to GUI
	virtual void LoadPlugins( ) = 0;

	//! For multithreading
	virtual void LoadPlugin( const std::string &name );

	//! For multithreading
	virtual void UnLoadPlugin( const std::string &name );

	/** Attach plugins that are running. For example a remote
	process using SSH */
	virtual void AttachRunningPlugins( );

	//!
	virtual void ScanPlugins( ) = 0;

	/** Convert plugin caption to plugin name
	The default implementation returns the caption
	*/
	virtual std::string GetPluginName( const std::string &caption );

	//! Contains all ModuleDescription instances scanned by this plugin provider
	ModuleFactory *GetModuleFactory();

	/** Enable ONLY the list of provided plugins using the name
	\param [in] names Name of the plugins to enable
	\param [in] if input is NULL, use internal "Plugins" property
	*/
	void EnablePluginsByName( const std::list<std::string> names, blTagMap::Pointer plugins );

	//! Get list of loaded plugins names
	std::list<std::string> GetLoadedPlugins( );

	//! Get list of selected plugins names
	std::list<std::string> GetSelectedPlugins( blTagMap::Pointer plugins = NULL );

	//!
	virtual BasePlugin::Pointer GetPlugin( const std::string &name );

	//!
	void EnableScanPlugins( bool enable );

	//!
	void EnableLoadPlugins( bool enableLoadPlugins );

	/** Get virtual child plugins for a GIMIAS Plugin. These virtual child plugins
	does not exist. Are just to get information about internal tools.
	*/
	virtual blTagMap::Pointer GetVirtualChildPlugins( const std::string &name );

	//!
	void UnselectAllPlugins( blTagMap::Pointer plugins = NULL );

protected:
	//!
	PluginProvider(void);
	//!
	virtual ~PluginProvider(void);

	//!
	void AddPendingPlugin( const std::string &name );

	//!
	void RemovePendingPlugin( const std::string &name );

	//!
	void WaitPendingPlugins( );

private:

	coreDeclareNoCopyConstructors(PluginProvider);

protected:
	//!
	ModuleFactory *m_ModuleFactory;
	//! List of loaded plugins. Needs to be filled by the provider
	std::list<std::string> m_LoadedPlugins;
	//!
	bool m_EnableLoadPlugins;
	//!
	bool m_EnableScanPlugins;
	//! Some providers need to wait until all plugins are processed
	std::list<std::string> m_PendingPlugins;
	//! Mutex to protect access to m_PendingPlugins
	boost::mutex m_PendignPluginsMutex;
	//! Signal when a pending plugin is removed
	boost::condition m_PendingPluginRemoved;
};

} // namespace Runtime
} // namespace Core

#endif

