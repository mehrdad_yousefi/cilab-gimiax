/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef GMKERNELPCH_H
#define GMKERNELPCH_H

#include "CILabBoostMacros.h"
#include "CILabExceptionMacros.h"
#include "CILabNamespaceMacros.h"

#include "ModuleDescription.h"
#include "ModuleFactory.h"

#include "blTagMap.h"
#include "blTextUtils.h"

#include "boost/bind.hpp"
#include "boost/filesystem.hpp"

#include "coreAssert.h"
#include "coreBaseFilter.h"
#include "coreCommonDataTypes.h"
#include "coreConfigVars.h"
#include "coreCreateExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreDataEntityBuildersRegistration.h"
#include "coreDataEntityHolder.h"
#include "coreDataEntityIORegistration.h"
#include "coreDataEntityInfoHelper.h"
#include "coreDataEntityReader.h"
#include "coreDataEntityWriter.h"
#include "coreDataHolder.h"
#include "coreDirectory.h"
#include "coreFile.h"
#include "coreObject.h"
#include "coreSession.h"
#include "coreSmartPointerMacros.h"
#include "coreWorkflow.h"
#include "coreWorkflowSerialize.h"
#include "coreException.h"
#include "coreSettingsIO.h"

#include "dynLib.h"
#include "tinyxml.h"

#include <boost/algorithm/string.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/signals.hpp>

#include <itksys/DynamicLoader.hxx>

#include <ctime>
#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <stdio.h>
#include <string>
#include <string.h>
#include <time.h>
#include <typeinfo>

#endif
