#ifndef testbedTestSettingsManager_h
#define testbedTestSettingsManager_h

#include <cxxtest/TestSuite.h>
#include <coreSettings.h>
#include <coreException.h>

#include <iostream>
#include <fstream>

class TestSettings : public CxxTest::TestSuite 
{
public:
	/* <Unit test> Checks that settings works with the filesystem and creates proper settings */
	void test1_CreatingAConfigFile( void )
	{
		Core::Runtime::Settings::Pointer settings;
		std::string appName = "TestSuite";
		std::string fileName;
		std::string inputStr;
		std::ifstream iFile;
		//
		// Step1. Create the object
		//
		try
		{
			settings = Core::Runtime::Settings::New(appName);
			TS_ASSERT(settings.IsNotNull());
		}
		catch(...)
		{
			TS_FAIL("An exception was caught on Step 1");
		}

		//
		// Step2: Check that the config file is created
		//
		try
		{
			// Check the file exists and contains data
			fileName = settings->GetConfigFileFullPath();
			iFile.open( fileName.c_str(), std::ios_base::in);
			TS_ASSERT( iFile.is_open( ) );
			iFile >> inputStr;
			TS_ASSERT( inputStr.size() > 0);
			iFile.close();
		}
		catch(...)
		{
			TS_FAIL("An exception was caught on Step 2");
		}
		
		//
		// CLEANUP
		//
		try
		{
		}
		catch(...)
		{
			TS_FAIL("An exception was caught on cleanup");
		}
		
	}
};

#endif // testbedTestSettingsManager_h
