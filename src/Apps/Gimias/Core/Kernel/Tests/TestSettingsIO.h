#ifndef testbedTestSettingsIO_h
#define testbedTestSettingsIO_h

#include <cxxtest/TestSuite.h>
#include <coreSettingsIO.h>
#include <coreException.h>

#include <iostream>
#include <fstream>

class TestSettingsIO : public CxxTest::TestSuite 
{
public:
	/* <Unit test> Checks that SettingsIO works with the filesystem and creates proper settings */
	void test1_CreatingAConfigFile( void )
	{
		Core::IO::SettingsIO::Pointer settingsIO;
		Core::IO::ConfigVars configVars;
		std::string configFileName = "testConfigFile.xml";
		std::string projectFolderName = "TestBed";
		std::string inputStr;

		std::ifstream iFile;
		//
		// Step1. Create the object and set a file
		//
		try
		{
			settingsIO = Core::IO::SettingsIO::New();
			TS_ASSERT(settingsIO.IsNotNull());
		}
		catch(...)
		{
			TS_FAIL("An exception was caught on Step 1");
		}

		//
		// Step2: Check that the config file is created
		//
		try
		{
			TS_ASSERT_THROWS_NOTHING(settingsIO->TestCreateConfigFile(projectFolderName, configFileName));

			// Check the file exists and contains data
			iFile.open(configFileName.c_str(), std::ios_base::in);
			TS_ASSERT( iFile.is_open( ) );
			iFile >> inputStr;
			TS_ASSERT( inputStr.size() > 0);
			iFile.close();
		}
		catch(...)
		{
			TS_FAIL("An exception was caught on Step 2");
		}

		//
		// Step3: Check that the contents can be read
		//
		try
		{
			TS_ASSERT_THROWS_NOTHING(settingsIO->ReadConfigFromFile(configFileName, configVars));
			TS_ASSERT(configVars.m_profile.IsNotNull());
			TS_ASSERT(!configVars.m_startedOnce);
		}
		catch(...)
		{
			TS_FAIL("An exception was caught on Step 3");
		}

		//
		// CLEANUP: Kill the config file and terminate xml subsystem
		//
		try
		{
			// Deinitialize the XML subsystem
			TS_ASSERT(remove(configFileName.c_str()) == 0);
		}
		catch(...)
		{
			TS_FAIL("An exception was caught on cleanup");
		}

	}




	/* <Unit test> Checks that SettingsIO writes the configuration properly */
	void test1_WritingToAConfigFile( void )
	{
		Core::IO::SettingsIO::Pointer settingsIO;
		Core::IO::ConfigVars configVars;
		Core::IO::ConfigVars readConfigVars;
		Core::Profile::Pointer profile;
		std::string configFileName = "testConfigFile.xml";
		std::string projectFolderName = "TestBed";
		std::string fullConfigFileName;

		//
		// Step1. Create the object and set a file
		//
		try
		{
			settingsIO = Core::IO::SettingsIO::New();
			TS_ASSERT(settingsIO.IsNotNull());
		}
		catch(...)
		{
			TS_FAIL("An exception was caught on Step 1");
		}

		//
		// Step2: Create a default config file
		//
		try
		{
			TS_ASSERT_THROWS_NOTHING(settingsIO->TestCreateConfigFile(projectFolderName, configFileName));
		}
		catch(...)
		{
			TS_FAIL("An exception was caught on Step 2");
		}

		//
		// Step3: Write some configuration
		//
		try
		{
			configVars.m_profile = Core::Profile::New();
			configVars.m_profile->AddToProfile("TestWorkingSet1");
			configVars.m_profile->AddToProfile("TestWorkingSet2");
			configVars.m_profile->AddToProfile("TestWorkingSet3");
			configVars.m_startedOnce = true;
			std::string dtdFileFullPath = "";
			TS_ASSERT_THROWS_NOTHING(settingsIO->WriteConfigToFile(configFileName, dtdFileFullPath, "MyProject",configVars));

			
		}
		catch(...)
		{
			TS_FAIL("An exception was caught on Step 2");
		}


		//
		// Step4: Check the written configuration is readable and correct
		//
		try
		{
			TS_ASSERT_THROWS_NOTHING(settingsIO->ReadConfigFromFile(configFileName, readConfigVars));
			TS_ASSERT_EQUALS(configVars.m_startedOnce, readConfigVars.m_startedOnce);
		}
		catch(...)
		{
			TS_FAIL("An exception was caught on Step 2");
		}
		
		//
		// CLEANUP: Kill the config file and terminate xml subsystem
		//
		try
		{
			// Deinitialize the XML subsystem
			TS_ASSERT(remove(configFileName.c_str()) == 0);
		}
		catch(...)
		{
			TS_FAIL("An exception was caught on cleanup");
		}
		
	}
};

#endif // testbedTestSettingsIO_h
