/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "coreBaseFactory.h"
#include "coreDataEntity.h"
#include "coreDataEntityHolder.h"
#include "coreDataHolder.h"
#include "coreException.h"
#include "coreFactoryManager.h"
#include "coreObject.h"

