/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreDirectPortInvocation_H
#define _coreDirectPortInvocation_H

#include "gmFilteringWin32Header.h"
#include "corePortInvocation.h"
#include "coreBaseFactory.h"

namespace Core
{

/** 
Execute a BaseFilterPort directly

\ingroup gmFiltering
\author Xavi Planes
\date Jan 2011
*/
class GMFILTERING_EXPORT DirectPortInvocation : public PortInvocation 
{
public:
	//!
	coreDeclareSmartPointerClassMacro( Core::DirectPortInvocation, PortInvocation );
	coreDefineFactory( Core::DirectPortInvocation );

	//!
	void Update( );

protected:
	//!
	DirectPortInvocation( );
	//!
	virtual ~DirectPortInvocation( );

protected:
};

} // namespace Core

#endif // _coreDirectPortInvocation_H
