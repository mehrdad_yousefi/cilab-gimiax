/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDirectPortInvocation.h"

using namespace Core;

Core::DirectPortInvocation::DirectPortInvocation()
{
}

Core::DirectPortInvocation::~DirectPortInvocation()
{

}

void Core::DirectPortInvocation::Update()
{
	m_PortUpdater->UpdatePort();
}
