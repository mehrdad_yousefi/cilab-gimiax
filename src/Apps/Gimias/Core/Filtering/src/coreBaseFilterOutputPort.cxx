/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreBaseFilterOutputPort.h"
#include "coreFactoryManager.h"

Core::BaseFilterOutputPort::BaseFilterOutputPort()
{
	m_ReuseOutput = true;
	m_DataEntityName = "";
	m_TotalTimeSteps = 1;
	m_Metadata = blTagMap::New();
	m_CopyMethod = OutputUpdater::COPY_DATA;
}

Core::BaseFilterOutputPort::~BaseFilterOutputPort()
{

}

bool Core::BaseFilterOutputPort::CheckDataEntityRestrictions( Core::DataEntity::Pointer val )
{
	return true;
}

bool Core::BaseFilterOutputPort::GetReuseOutput() const
{
	return m_ReuseOutput;
}

void Core::BaseFilterOutputPort::SetReuseOutput( bool val )
{
	m_ReuseOutput = val;
}

std::string Core::BaseFilterOutputPort::GetDataEntityName() const
{
	return m_DataEntityName;
}

void Core::BaseFilterOutputPort::SetDataEntityName( const std::string &val )
{
	m_DataEntityName = val;
}

int Core::BaseFilterOutputPort::GetTotalTimeSteps() const
{
	return m_TotalTimeSteps;
}

void Core::BaseFilterOutputPort::SetTotalTimeSteps( int val )
{
	m_TotalTimeSteps = val;
}


void Core::BaseFilterOutputPort::UpdateOutput( 
	boost::any data, 
	int timeStepNumber,
	Core::DataEntity::Pointer fatherDataEntity )
{
	OutputUpdater::Pointer updater = OutputUpdater::New();
	updater->SetHolder( GetDataEntityHolder() );
	updater->SetData( data );
	updater->SetReuseOutput( m_ReuseOutput );
	updater->SetDataEntityName( m_DataEntityName );
	updater->SetTotalTimeSteps( m_TotalTimeSteps );
	updater->SetTimeStep( timeStepNumber );
	updater->SetFatherDataEntity( fatherDataEntity );
	updater->SetDataEntityType( GetDataEntityType() );
	updater->SetMultithreading( GetMultithreading() );
	updater->SetMetadata( m_Metadata );
	updater->SetCopyMethod( m_CopyMethod );
	updater->SetWaitPortUpdate( GetWaitPortUpdate( ) );
	updater->Update( );
}

void Core::BaseFilterOutputPort::UpdateOutput( 
	std::vector<boost::any> anyDataVector, 
	Core::DataEntity::Pointer fatherDataEntity /*= NULL */ )
{
	OutputUpdater::Pointer updater = OutputUpdater::New();
	updater->SetHolder( GetDataEntityHolder() );
	updater->SetData( anyDataVector );
	updater->SetReuseOutput( m_ReuseOutput );
	updater->SetDataEntityName( m_DataEntityName );
	updater->SetTotalTimeSteps( m_TotalTimeSteps );
	updater->SetFatherDataEntity( fatherDataEntity );
	updater->SetDataEntityType( GetDataEntityType() );
	updater->SetMultithreading( GetMultithreading() );
	updater->SetMetadata( m_Metadata );
	updater->SetCopyMethod( m_CopyMethod );
	updater->SetWaitPortUpdate( GetWaitPortUpdate( ) );
	updater->Update( );
}

void Core::BaseFilterOutputPort::UpdateOutput( 
	Core::DataEntity::Pointer dataEntity, 
	Core::DataEntity::Pointer fatherDataEntity /*= NULL */ )
{
	OutputUpdater::Pointer updater = OutputUpdater::New();
	updater->SetHolder( GetDataEntityHolder() );
	updater->SetData( dataEntity );
	updater->SetFatherDataEntity( fatherDataEntity );
	updater->SetDataEntityType( GetDataEntityType() );
	updater->SetMultithreading( GetMultithreading() );
	updater->SetMetadata( m_Metadata );
	updater->SetReuseOutput( m_ReuseOutput );
	updater->SetCopyMethod( m_CopyMethod );
	updater->SetWaitPortUpdate( GetWaitPortUpdate( ) );
	updater->Update( );
}

void Core::BaseFilterOutputPort::InternalCopy( BaseFilterPort* port )
{
	BaseFilterOutputPort* outputPort = dynamic_cast<BaseFilterOutputPort*> ( port );
	m_ReuseOutput = outputPort->GetReuseOutput();
	m_DataEntityName = outputPort->GetDataEntityName();
	m_TotalTimeSteps = outputPort->GetTotalTimeSteps();
	m_CopyMethod = outputPort->GetCopyMethod( );
	m_Metadata = blTagMap::New();
	if ( outputPort->GetMetadata().IsNotNull() )
	{
		m_Metadata->AddTags( outputPort->GetMetadata() );
	}
}

blTagMap::Pointer Core::BaseFilterOutputPort::GetMetadata() const
{
	return m_Metadata;
}

void Core::BaseFilterOutputPort::SetMetadata( blTagMap::Pointer val )
{
	m_Metadata = val;
}

Core::OutputUpdater::COPY_METHOD Core::BaseFilterOutputPort::GetCopyMethod( )
{
	return m_CopyMethod;
}

void Core::BaseFilterOutputPort::SetCopyMethod( Core::OutputUpdater::COPY_METHOD method )
{
	m_CopyMethod = method;
}


