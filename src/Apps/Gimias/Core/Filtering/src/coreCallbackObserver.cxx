/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreCallbackObserver.h"

using namespace Core;

Core::CallbackObserver::CallbackObserver()
{
	
}

Core::CallbackObserver::~CallbackObserver()
{
	SetUpdateCallback( NULL );
}

void Core::CallbackObserver::OnModified()
{
}

UpdateCallback::Pointer Core::CallbackObserver::GetUpdateCallback() const
{
	return m_UpdateCallback;
}

void Core::CallbackObserver::SetUpdateCallback( UpdateCallback::Pointer val )
{
	if ( m_UpdateCallback.IsNotNull() )
	{
		m_UpdateCallback->RemoveObserverOnModified( this, &CallbackObserver::OnModified );
	}

	m_UpdateCallback = val;

	if ( m_UpdateCallback.IsNotNull() )
	{
		m_UpdateCallback->AddObserverOnModified( this, &CallbackObserver::OnModified );
	}
}

