/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _corePortInvocation_H
#define _corePortInvocation_H

#include "gmFilteringWin32Header.h"
#include "corePortUpdater.h"

namespace Core
{

/** 
Base interface to execute a BaseFilterPort 

\ingroup gmFiltering
\author Xavi Planes
\date Jan 2011
*/
class GMFILTERING_EXPORT PortInvocation : public SmartPointerObject 
{
public:
	//!
	coreDeclareSmartPointerTypesMacro(Core::PortInvocation,SmartPointerObject)
	coreClassNameMacro(Core::PortInvocation)
	coreTypeMacro(Core::PortInvocation,SmartPointerObject)

	//!
	virtual void Update( ) = 0;

	//!
	PortUpdater::Pointer GetPortUpdater() const;
	void SetPortUpdater(PortUpdater::Pointer val);

protected:
	//!
	PortInvocation( );
	//!
	virtual ~PortInvocation( );

protected:
	//!
	PortUpdater::Pointer m_PortUpdater;

};

} // namespace Core

#endif // _corePortInvocation_H
