/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreFilteringFactoriesRegistration_H
#define _coreFilteringFactoriesRegistration_H

#include "gmFilteringWin32Header.h"
#include "coreObject.h"

namespace Core{

/** 
Registers all Factories for this library

\ingroup gmFiltering
\author: Xavi Planes
\date: Jan 2011
*/
class GMFILTERING_EXPORT FilteringFactoriesRegistration : public Core::Object
{
public:
	//! Register all builders
	static void Register();

	//! Register all builders
	static void UnRegister();

protected:

};

} // namespace Core{

#endif // _coreFilteringFactoriesRegistration_H
