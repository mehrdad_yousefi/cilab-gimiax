/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreBaseProcessor.h"
#include "coreBaseExceptions.h"

Core::BaseProcessor::BaseProcessor()
{
}

Core::BaseProcessor::~BaseProcessor()
{
}


void Core::BaseProcessor::UpdateOutput( 
	int iIndex, 
	boost::any data,
	std::string strDataEntityName,
	bool bReuseOutput,
	int iTotalTimeSteps,
	Core::DataEntity::Pointer fatherDataEntity,
	blTagMap::Pointer metaData /*= NULL*/ )
{
	GetOutputPort( iIndex )->SetDataEntityName( strDataEntityName );
	GetOutputPort( iIndex )->SetReuseOutput( bReuseOutput );

	// iTotalTimeSteps should never be < GetTimeStepCount( )
	iTotalTimeSteps = int( std::max( size_t( iTotalTimeSteps ), ComputeNumberOfIterations() ) );
	GetOutputPort( iIndex )->SetTotalTimeSteps( iTotalTimeSteps );
	GetOutputPort( iIndex )->SetMetadata( metaData );
	GetOutputPort( iIndex )->UpdateOutput( data, GetOutputPort( iIndex )->GetSelectedTimeStep(), fatherDataEntity );
}

void Core::BaseProcessor::UpdateOutput( 
	int iIndex, 
	Core::DataEntity::Pointer dataEntity,
	Core::DataEntity::Pointer fatherDataEntity/* = NULL*/,
	blTagMap::Pointer metaData /*= NULL*/ )
{
	GetOutputPort( iIndex )->SetMetadata( metaData );
	GetOutputPort( iIndex )->UpdateOutput( dataEntity, fatherDataEntity );
}

void Core::BaseProcessor::SetParameters( blTagMap::Pointer tagMap )
{
	if ( tagMap.IsNull() )
	{
		return;
	}

	blTagMap::Iterator it;
	for ( it = tagMap->GetIteratorBegin() ; it != tagMap->GetIteratorEnd() ; it++ )
	{
		SetParameter( it->second );
	}
}

void Core::BaseProcessor::GetParameters( blTagMap::Pointer tagMap )
{
	if ( tagMap.IsNull() )
	{
		return;
	}

	blTagMap::Iterator it;
	for ( it = tagMap->GetIteratorBegin() ; it != tagMap->GetIteratorEnd() ; it++ )
	{
		GetParameter( it->second );
	}
}

void Core::BaseProcessor::SetParameter( blTag::Pointer tag )
{
	throw Core::Exceptions::NotImplementedException( "BaseProcessor::SetParameter" );
}

void Core::BaseProcessor::GetParameter( blTag::Pointer tag )
{
	throw Core::Exceptions::NotImplementedException( "BaseProcessor::GetParameter" );
}


/**
Compilation test
\ingroup gmFiltering
\author Xavi Planes
\date 09 11 09
*/
class BaseProcessorrTest : Core::BaseProcessor
{
	void Update( )
	{
		int i;
		GetProcessingData( 0, i );

		std::vector<int> v;
		GetProcessingData( 0, v );
	}
};
