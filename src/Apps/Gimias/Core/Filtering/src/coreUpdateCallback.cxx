/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreUpdateCallback.h"

Core::UpdateCallback::UpdateCallback()
{
	m_Progress = 0;
	m_AbortProcessing = false;
	m_ProcessorThreadID = -1;
	m_DetachProcessing = false;
	m_Modified = false;
}

Core::UpdateCallback::~UpdateCallback()
{
}

float Core::UpdateCallback::GetProgress() const
{
	return m_Progress;
}

void Core::UpdateCallback::SetProgress( float val )
{
	if ( m_Progress != val )
	{
		m_Progress = val;
		m_Modified = true;
	}
}

std::string Core::UpdateCallback::GetStatusMessage() const
{
	return m_StatusMessage;
}

void Core::UpdateCallback::SetStatusMessage( std::string val )
{
	if ( m_StatusMessage != val )
	{
		m_StatusMessage = val;
		m_Modified = true;
	}
}

bool Core::UpdateCallback::GetAbortProcessing() const
{
	return m_AbortProcessing;
}

void Core::UpdateCallback::SetAbortProcessing( bool val )
{
	if ( m_AbortProcessing != val )
	{
		m_AbortProcessing = val;
		m_Modified = true;
	}
}

std::string Core::UpdateCallback::GetExceptionMessage() const
{
	return m_ExceptionMessage;
}

void Core::UpdateCallback::SetExceptionMessage( std::string val )
{
	if ( m_ExceptionMessage != val )
	{
		m_ExceptionMessage = val;
		m_Modified = true;
	}
}

void Core::UpdateCallback::AddExceptionMessage( std::string val )
{
	m_ExceptionMessage = val;
	m_Modified = true;
}

std::string Core::UpdateCallback::GetInformationMessage() const
{
	return m_InformationMessage;
}

void Core::UpdateCallback::SetInformationMessage( std::string val )
{
	if ( m_InformationMessage != val )
	{
		m_InformationMessage = val;
		m_Modified = true;
	}
}

void Core::UpdateCallback::AddInformationMessage( std::string val )
{
	m_InformationMessage += val;
	m_Modified = true;
}

long Core::UpdateCallback::GetProcessorThreadID() const
{
	return m_ProcessorThreadID;
}

void Core::UpdateCallback::SetProcessorThreadID( long val )
{
	if ( m_ProcessorThreadID != val )
	{
		m_ProcessorThreadID = val;
		m_Modified = true;
	}
}

bool Core::UpdateCallback::GetDetachProcessing() const
{
	return m_DetachProcessing;
}

void Core::UpdateCallback::SetDetachProcessing( bool val )
{
	if ( m_DetachProcessing != val )
	{
		m_DetachProcessing = val;
		m_Modified = true;
	}
}

void Core::UpdateCallback::Modified() const
{
	if ( m_Modified )
	{
		SmartPointerObject::Modified();
	}
}
