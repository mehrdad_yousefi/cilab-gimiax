/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreInputUpdater_H
#define _coreInputUpdater_H

#include "gmFilteringWin32Header.h"
#include "coreDataEntityHolder.h"
#include "corePortUpdater.h"

namespace Core
{

/** 
Updates input data to match a processing type. You can set the boost:any 
pointer to the desired data type or use the type name (SetDataTypeName( ) )

\ingroup gmFiltering
\author Xavi Planes
\date Jan 2011
*/
class GMFILTERING_EXPORT InputUpdater : public PortUpdater 
{
public:
	//!
	coreDeclareSmartPointerClassMacro(Core::InputUpdater,PortUpdater)

	//! 
	void SetHolder(DataEntityHolder::Pointer val);
	DataEntityHolder::Pointer GetHolder() const;

	//!
	void SetTimeStep(Core::TimeStepIndex val);
	Core::TimeStepIndex GetTimeStep() const;

	//!
	boost::any GetData() const;
	void SetData(boost::any val);

	//!
	std::string GetDataTypeName() const;
	void SetDataTypeName(std::string val);

	//!
	bool GetGenerateNewInstance() const;
	void SetGenerateNewInstance(bool val);

	//!
	void UpdatePort( );


protected:
	//!
	InputUpdater( );
	//!
	virtual ~InputUpdater( );

protected:
	//!
	boost::any m_Data;
	//!
	DataEntityHolder::Pointer m_Holder;
	//!
	TimeStepIndex m_TimeStep;
	//!
	bool m_GenerateNewInstance;
	//!
	std::string m_DataTypeName;
};

} // namespace Core

#endif // _coreInputUpdater_H
