/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreFilteringFactoriesRegistration.h"
#include "coreFactoryManager.h"
#include "coreDirectPortInvocation.h"
#include "coreEmptyUpdateCallback.h"
#include "coreCallbackObserver.h"

void Core::FilteringFactoriesRegistration::Register()
{
	Core::FactoryManager::Register( PortInvocation::GetNameClass( ), DirectPortInvocation::Factory::New() );
	Core::FactoryManager::Register( UpdateCallback::GetNameClass( ), EmptyUpdateCallback::Factory::New() );
	Core::FactoryManager::Register( CallbackObserver::GetNameClass( ), CallbackObserver::Factory::New() );
}

void Core::FilteringFactoriesRegistration::UnRegister()
{
	Core::FactoryManager::UnRegister( FactoryManager::FindByInstanceClassName( DirectPortInvocation::GetNameClass( ) ) );
	Core::FactoryManager::UnRegister( FactoryManager::FindByInstanceClassName( EmptyUpdateCallback::GetNameClass( ) ) );
	Core::FactoryManager::UnRegister( FactoryManager::FindByInstanceClassName( CallbackObserver::GetNameClass( ) ) );
}
