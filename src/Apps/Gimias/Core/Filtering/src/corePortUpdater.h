/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _corePortUpdater_H
#define _corePortUpdater_H

#include "gmFilteringWin32Header.h"

namespace Core
{

/** 
Base interface for port updater 

\ingroup gmFiltering
\author Xavi Planes
\date Jan 2011
*/
class GMFILTERING_EXPORT PortUpdater : public SmartPointerObject 
{
public:
	//!
	coreDeclareSmartPointerTypesMacro(Core::PortUpdater,SmartPointerObject)
	coreClassNameMacro(Core::PortUpdater)
	coreTypeMacro(Core::PortUpdater,SmartPointerObject)

	//! Create a PortInvocation to update the port
	void Update( );

	//!
	bool GetMultithreading() const;
	void SetMultithreading(bool val);

	//!
	bool GetWaitPortUpdate( ) const;
	void SetWaitPortUpdate( bool val );

	//! Update port
	virtual void UpdatePort( ) = 0;

protected:
	//!
	PortUpdater( );
	//!
	virtual ~PortUpdater( );

protected:
	//!
	bool m_Multithreading;
	//! Wait until port has been updated before continuing (multithreading)
	bool m_WaitPortUpdate;
};

} // namespace Core

#endif // _corePortUpdater_H
