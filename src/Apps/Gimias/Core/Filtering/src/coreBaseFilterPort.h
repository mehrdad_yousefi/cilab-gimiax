/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreBaseFilterPort_H
#define _coreBaseFilterPort_H

// CoreLib
#include "gmFilteringWin32Header.h"
#include "coreObject.h"
#include "coreDataEntityHolder.h"

namespace Core{

/**
\brief Base port for a filter

\ingroup gmFiltering
\author Xavi Planes
\date 05 06 09
*/
class GMFILTERING_EXPORT BaseFilterPort : public Core::SmartPointerObject{

public:
	coreDeclareSmartPointerTypesMacro(Core::BaseFilterPort,Core::SmartPointerObject) \
	coreClassNameMacro(Core::BaseFilterPort)

	//!
	Core::DataEntityHolder::Pointer GetDataEntityHolder() const;

	/** If CheckDataEntityRestrictions( ) return true, set subject 
	of m_DataEntityHolder to val
	*/
	bool SetDataEntity(Core::DataEntity::Pointer val);

	//!
	Core::DataEntity::Pointer GetDataEntity( );

	//!
	Core::DataEntityType GetDataEntityType() const;
	virtual void SetDataEntityType(int val);

	//!
	std::string GetName() const;
	void SetName(std::string val);

	//!
	std::string GetProcessorName() const;
	void SetProcessorName(std::string val);

	//!
	bool GetOptional() const;
	void SetOptional(bool val);

	//!
	bool GetActive() const;
	void SetActive(bool val);

	//!
	void Copy( BaseFilterPort* port);

	//!
	bool GetMultithreading() const;
	void SetMultithreading(bool val);

	//!
	int GetSelectedTimeStep() const;
	void SetSelectedTimeStep(int val);

	//!
	bool GetWaitPortUpdate( ) const;
	void SetWaitPortUpdate( bool val );

protected:
	//!
	BaseFilterPort( );

	//!
	virtual ~BaseFilterPort( );

	//! Check data entity restrictions
	virtual bool CheckDataEntityRestrictions( Core::DataEntity::Pointer val ) = 0;

	//!
	virtual void InternalCopy( BaseFilterPort* port);

private:
	//!
	Core::DataEntityHolder::Pointer m_DataEntityHolder;
	//!
	std::string m_Name;
	//!
	std::string m_ProcessorName;
	//!
	Core::DataEntityType m_DataEntityType;
	//! This port is optional. The client can use it or not
	bool m_Optional;
	//! If it's optional, this flag tells if is active or not
	bool m_Active;
	//!
	bool m_Multithreading;
	//! Selected time step to get input data or set output data to DataEntity 
	int m_SelectedTimeStep;
	//! Wait until port has been updated before continuing (multithreading)
	bool m_WaitPortUpdate;
};

} // namespace Core{

#endif // _coreBaseFilterPort_H


