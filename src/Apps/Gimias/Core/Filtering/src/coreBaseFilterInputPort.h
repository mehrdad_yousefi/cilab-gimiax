/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreBaseFilterInputPort_H
#define _coreBaseFilterInputPort_H

// CoreLib
#include "coreObject.h"
#include "coreBaseFilterPort.h"

namespace Core{

/**
\brief Input port for a filter

Set restrictions for input data based on:
- Type
- Metadata tags

An input port holds a DataEntity that will be used as input for the BaseFilter.
When calling the BaseFilter::Update() function, it can access a single time step
or access multiple time steps. When accessing a single time step, is possible
to select a single one or select all time steps.

\section Examples

To allow all kind of images:
SetDataEntityType( Core::ImageTypeId )

To allow only MaskImages:
SetDataEntityType( Core::ROITypeId )

To allow all images except MaskImages:
SetDataEntityType( Core::ImageTypeId )
SetNotValidDataEntityType( Core::ROITypeId )

To allow only MRI images:
SetModality( Core::MRI )

To allow only images with Male sex:
AddValidDataEntityTag( blTag::New( "Patient sex", "Male" ) )

\ingroup gmFiltering
\author Xavi Planes
\date 06 11 09
*/
class GMFILTERING_EXPORT BaseFilterInputPort : public Core::BaseFilterPort{
public:
	coreDeclareSmartPointerClassMacro(Core::BaseFilterInputPort, Core::BaseFilterPort);

	//! Type of access when calling BaseFilter::Update()
	enum UPDATE_ACCESS_MODE{
		//! Access a single time step
		UPDATE_ACCESS_SINGLE_TIME_STEP,
		//! Access multiple time steps
		UPDATE_ACCESS_MULTIPLE_TIME_STEP
	};

	//! Type of selection when using UPDATE_SINGLE_TIME_STEP
	enum SELECT_TIME_STEP_TYPE{
		//! Select all time steps from input DataEntity
		SELECT_ALL_TIME_STEPS,
		//! Select single time step from input DataEntity
		SELECT_SINGLE_TIME_STEP
	};

	//!
	void SetDataEntityType(int val);

	//!
	Core::ModalityType GetModality() const;
	void SetModality(Core::ModalityType val);

	//! Redefined
	bool CheckDataEntityRestrictions( Core::DataEntity::Pointer val );

	//! Set and get not valid data entity types
	Core::DataEntityType GetNotValidDataEntityType() const;
	void SetNotValidDataEntityType(int val);

	//!
	void AddValidDataEntityTag( DataEntityTag::Pointer tag );

	//!
	Core::BaseFilterInputPort::SELECT_TIME_STEP_TYPE GetTimeStepSelection() const;
	void SetTimeStepSelection(Core::BaseFilterInputPort::SELECT_TIME_STEP_TYPE val);

	//!
	virtual void InternalCopy( BaseFilterPort* port);

	/** Update input to the desired data type and specific time step
	\param [in] iTimeStep If the value is -1, use internal member variable SelectedTimeStep
	*/
	void UpdateInput( boost::any &anyData, int iTimeStep, bool generateNewInstance = false );

	//! Switch implementation of input data using name of data type
	boost::any UpdateInput( const std::string &datatypename, int iTimeStep );

	//!
	Core::BaseFilterInputPort::UPDATE_ACCESS_MODE GetUpdateMode() const;
	void SetUpdateMode(Core::BaseFilterInputPort::UPDATE_ACCESS_MODE val);

private:
	//!
	BaseFilterInputPort( );

	//!
	virtual ~BaseFilterInputPort( );

	//!
	bool CheckAllowedInputDataTypes( Core::DataEntity::Pointer val );

private:
	//!
	Core::ModalityType m_Modality;
	//! Not valid data Entity types
	Core::DataEntityType m_NotValidDataEntityType;
	//!
	std::list<DataEntityTag::Pointer> m_ValidTagList;
	//!
	SELECT_TIME_STEP_TYPE m_TimeStepSelection;
	//!
	UPDATE_ACCESS_MODE m_UpdateAccessMode;
};

} // namespace Core{

#endif // _coreBaseFilterInputPort_H


