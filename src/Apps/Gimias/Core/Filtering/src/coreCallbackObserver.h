/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreCallbackObserver_H
#define _coreCallbackObserver_H

#include "gmFilteringWin32Header.h"
#include "coreBaseFactory.h"
#include "coreUpdateCallback.h"

namespace Core
{

/** 
Empty observer of UpdateCallback

\ingroup gmFiltering
\author Xavi Planes
\date Oct 2011
*/
class GMFILTERING_EXPORT CallbackObserver : public Core::SmartPointerObject 
{
public:
	//!
	coreDeclareSmartPointerClassMacro(Core::CallbackObserver,Core::SmartPointerObject)
	coreDefineFactory( Core::CallbackObserver );

	//!
	UpdateCallback::Pointer GetUpdateCallback() const;
	void SetUpdateCallback(UpdateCallback::Pointer val);

	//! Empty function
	virtual void OnModified( );

protected:
	//!
	CallbackObserver( );

	//!
	~CallbackObserver( );

protected:
	//!
	UpdateCallback::Pointer m_UpdateCallback;
};

} // namespace Core

#endif // _coreCallbackObserver_H
