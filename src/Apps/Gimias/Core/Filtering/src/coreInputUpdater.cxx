/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreInputUpdater.h"

using namespace Core;

Core::InputUpdater::InputUpdater()
{
	m_GenerateNewInstance = false;
	m_TimeStep = 0;
}

Core::InputUpdater::~InputUpdater()
{

}

void Core::InputUpdater::SetHolder( DataEntityHolder::Pointer val )
{
	m_Holder = val;
}

boost::any Core::InputUpdater::GetData() const
{
	return m_Data;
}

void Core::InputUpdater::SetData( boost::any val )
{
	m_Data = val;
}

void Core::InputUpdater::SetTimeStep( Core::TimeStepIndex val )
{
	m_TimeStep = val;
}

DataEntityHolder::Pointer Core::InputUpdater::GetHolder() const
{
	return m_Holder;
}

Core::TimeStepIndex Core::InputUpdater::GetTimeStep() const
{
	return m_TimeStep;
}

void Core::InputUpdater::UpdatePort()
{
	DataEntity::Pointer	dataEntity;

	dataEntity = m_Holder->GetSubject();
	if ( dataEntity.IsNotNull( ) && !m_Data.empty() )
	{
		dataEntity->GetProcessingData( m_Data, m_TimeStep, !m_GenerateNewInstance, m_GenerateNewInstance );
	}
	else if ( dataEntity.IsNotNull( ) && !m_DataTypeName.empty() )
	{
		// Try to switch implementation
		if ( !dataEntity->SwitchImplementation( m_DataTypeName ) )
		{
			return;
		}

		// Get factory
		BaseFactory::Pointer factory;
		factory = DataEntityImplFactoryHelper::FindFactoryBySimilarType( m_DataTypeName );
		if ( factory.IsNull() )
		{
			return;
		}

		// If multiple, ignore time step
		if ( factory->GetProperties()->FindTagByName( "multiple" ) )
		{
			m_Data = dataEntity->GetProcessingData( TimeStepIndex( -1 ) );
		}
		else if ( factory->GetProperties()->FindTagByName( "single" ) )
		{
			m_Data = dataEntity->GetProcessingData( TimeStepIndex( m_TimeStep ) );
		}
	}
}

bool Core::InputUpdater::GetGenerateNewInstance() const
{
	return m_GenerateNewInstance;
}

void Core::InputUpdater::SetGenerateNewInstance( bool val )
{
	m_GenerateNewInstance = val;
}

std::string Core::InputUpdater::GetDataTypeName() const
{
	return m_DataTypeName;
}

void Core::InputUpdater::SetDataTypeName( std::string val )
{
	m_DataTypeName = val;
}
