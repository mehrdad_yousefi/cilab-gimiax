/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "corePortUpdater.h"
#include "corePortInvocation.h"

using namespace Core;

Core::PortUpdater::PortUpdater()
{
	m_Multithreading = false;
	m_WaitPortUpdate = true;
}

Core::PortUpdater::~PortUpdater()
{

}

void Core::PortUpdater::Update()
{
	// Find the invocation
	blTagMap::Pointer properties = blTagMap::New( );
	if ( GetMultithreading() )
	{
		properties->AddTag( "multithreading", GetMultithreading() );
	}
	SmartPointerObject::Pointer object;
	object = Core::FactoryManager::CreateInstance( PortInvocation::GetNameClass( ), properties );
	PortInvocation::Pointer invocation = PortInvocation::SafeDownCast( object );

	// Execute it 
	invocation->SetPortUpdater( this );
	invocation->Update( );
}

bool Core::PortUpdater::GetMultithreading() const
{
	return m_Multithreading;
}

void Core::PortUpdater::SetMultithreading( bool val )
{
	m_Multithreading = val;
}

bool Core::PortUpdater::GetWaitPortUpdate( ) const
{
	return m_WaitPortUpdate;
}

void Core::PortUpdater::SetWaitPortUpdate( bool val )
{
	m_WaitPortUpdate = val;
}
