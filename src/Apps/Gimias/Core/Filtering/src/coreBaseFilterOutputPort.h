/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreBaseFilterOutputPort_H
#define _coreBaseFilterOutputPort_H

// CoreLib
#include "coreObject.h"
#include "coreBaseFilterPort.h"
#include "blTagMap.h"
#include "coreOutputUpdater.h"

namespace Core{

/**
\brief Output port for a filter

\ingroup gmFiltering
\author Xavi Planes
\date 06 11 09
*/
class GMFILTERING_EXPORT BaseFilterOutputPort : public Core::BaseFilterPort
{
public:
	coreDeclareSmartPointerClassMacro(Core::BaseFilterOutputPort, Core::BaseFilterPort);

	//!
	bool GetReuseOutput() const;
	void SetReuseOutput(bool val);

	//!
	std::string GetDataEntityName() const;
	void SetDataEntityName(const std::string &val);

	//!
	int GetTotalTimeSteps() const;
	void SetTotalTimeSteps(int val);

	//!
	void UpdateOutput( 
		boost::any any, 
		int timeStepNumber,
		Core::DataEntity::Pointer fatherDataEntity );

	//! Use a vector of data to update the output
	void UpdateOutput( 
		std::vector<boost::any> imageDataVector,
		Core::DataEntity::Pointer fatherDataEntity = NULL );

	//! Use a DataEntity
	void UpdateOutput( 
		Core::DataEntity::Pointer dataEntity,
		Core::DataEntity::Pointer fatherDataEntity = NULL );
	//!
	blTagMap::Pointer GetMetadata() const;
	void SetMetadata(blTagMap::Pointer val);

	//!
	OutputUpdater::COPY_METHOD GetCopyMethod( );
	void SetCopyMethod( OutputUpdater::COPY_METHOD method );

private:
	//!
	BaseFilterOutputPort( );

	//!
	~BaseFilterOutputPort( );

	//! Redefined
	bool CheckDataEntityRestrictions( Core::DataEntity::Pointer val );

	//!
	void InternalCopy( BaseFilterPort* port );

private:
	//! Use the same output data entity or build a new one
	bool m_ReuseOutput;
	//!
	std::string m_DataEntityName;
	//!
	int m_TotalTimeSteps;
	//!
	blTagMap::Pointer m_Metadata;
	//! How to copy output data
	OutputUpdater::COPY_METHOD m_CopyMethod;
};

} // namespace Core{

#endif // _coreBaseFilterOutputPort_H


