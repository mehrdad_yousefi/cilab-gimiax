/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreOutputUpdater.h"

using namespace Core;

Core::OutputUpdater::OutputUpdater()
{
	m_TimeStep = 0;
	m_ReuseOutput = true;
	m_TotalTimeSteps = 1;
	m_DataEntityType = UnknownTypeId;
	m_CopyMethod = OutputUpdater::COPY_DATA;
}

Core::OutputUpdater::~OutputUpdater()
{

}

void Core::OutputUpdater::SetHolder( DataEntityHolder::Pointer val )
{
	m_Holder = val;
}

void Core::OutputUpdater::UpdatePort()
{
	DataEntity::Pointer dataEntity;
	if ( m_Data.type() == typeid( Core::DataEntity::Pointer ) )
	{
		dataEntity = CreateDataEntityFromDataEntity( );
	}
	else if ( m_Data.type() == typeid( std::vector<boost::any> ) )
	{
		dataEntity = CreateDataEntityFromVectorData();
	}
	else
	{
		dataEntity = CreateDataEntityFromSimpleData();
	}
	
	NotifyObservers( dataEntity );
}

void Core::OutputUpdater::SetReuseOutput( bool val )
{
	m_ReuseOutput = val;
}

void Core::OutputUpdater::SetDataEntityName( std::string val )
{
	m_DataEntityName = val;
}

void Core::OutputUpdater::SetTotalTimeSteps( int val )
{
	m_TotalTimeSteps = val;
}

void Core::OutputUpdater::SetData( boost::any val )
{
	m_Data = val;
}

void Core::OutputUpdater::SetTimeStep( int val )
{
	m_TimeStep = val;
}

void Core::OutputUpdater::SetFatherDataEntity( Core::DataEntity::Pointer val )
{
	m_FatherDataEntity = val;
}

DataEntity::Pointer Core::OutputUpdater::CreateDataEntityFromSimpleData()
{
	// Get old data entity
	Core::DataEntity::Pointer dataEntityOutput;
	dataEntityOutput = m_Holder->GetSubject( );

	// Find a factory for any data type
	BaseFactory::Pointer factory;
	factory = DataEntityImplFactoryHelper::FindFactoryByType( m_Data.type().name() );
	if ( factory.IsNull() )
	{
		std::ostringstream strError;
		strError << "Cannot manage " << m_Data.type().name( ) << std::endl;
		throw Core::Exceptions::Exception( 
			"BaseFilterOutputPort::UpdateOutput",
			strError.str().c_str() );
	}

	// When the number of total time steps changes, only increase size
	// and reuse output
	if ( m_ReuseOutput &&
		 dataEntityOutput.IsNotNull() && 
		 dataEntityOutput->GetNumberOfTimeSteps( ) != m_TotalTimeSteps )
	{
		dataEntityOutput->Resize( m_TotalTimeSteps, m_Data.type() );
	}

	// Create a new data entity
	bool buildOutput = CheckBuildOutput( 
		m_ReuseOutput, m_FatherDataEntity, 
		dataEntityOutput, m_TotalTimeSteps );
	if ( buildOutput )
	{
		dataEntityOutput = DataEntity::New( );
		dataEntityOutput->GetMetadata()->SetName( m_DataEntityName );

		// Set specific metadata
		if ( m_Metadata.IsNotNull() )
		{
			dataEntityOutput->GetMetadata()->AddTags( m_Metadata );
		}

		// If the type is not configured, use the default of the Factory
		Core::DataEntityType type = m_DataEntityType;
		if ( type == UnknownTypeId )
		{
			blTag::Pointer tag;
			tag = factory->GetProperties()->FindTagByName( "DefaultDataEntityType" );
			if ( tag.IsNotNull() )
			{
				type = tag->GetValueCasted<Core::DataEntityType>();
			}
		}
		dataEntityOutput->SetType( type );
		dataEntityOutput->Resize( m_TotalTimeSteps, m_Data.type() );
	}

	// If single -> copy time step, else copy all time steps
	if ( factory->GetProperties()->FindTagByName( "single" ) )
	{
		// Copy the time step
		DataEntityImpl::Pointer impl = dataEntityOutput->GetTimeStep( m_TimeStep );
		switch ( m_CopyMethod )
		{
			case COPY_DATA: impl->DeepCopy( m_Data );break;
			case COPY_POINTER: impl->SetDataPtr( m_Data );break;
		}
	}
	else if ( factory->GetProperties()->FindTagByName( "multiple" ) )
	{
		// Copy all time steps
		switch ( m_CopyMethod )
		{
			case COPY_DATA: dataEntityOutput->GetTimeStep( -1 )->DeepCopy( m_Data );break;
			case COPY_POINTER: dataEntityOutput->GetTimeStep( -1 )->SetDataPtr( m_Data );break;
		}
	}

	// Set father
	if ( dataEntityOutput->GetFather( ).IsNull( ) && 
		m_FatherDataEntity.IsNotNull() &&
		dataEntityOutput->GetId() != m_FatherDataEntity->GetId() )
	{
		dataEntityOutput->SetFather( m_FatherDataEntity );
	}

	return dataEntityOutput;
}


bool Core::OutputUpdater::CheckBuildOutput( 
	bool bReuseOutput,
	Core::DataEntity::Pointer fatherDataEntity,
	Core::DataEntity::Pointer dataEntityOutput,
	unsigned totalTimeSteps )
{
	return ( !bReuseOutput || 
		dataEntityOutput.IsNull() || 
		dataEntityOutput->GetNumberOfTimeSteps( ) != totalTimeSteps ||
		( dataEntityOutput->GetFather( ).IsNotNull( ) && 
		fatherDataEntity.IsNotNull( ) &&
		fatherDataEntity->GetId( ) != dataEntityOutput->GetFather( )->GetId( ) && 
		fatherDataEntity->GetId( ) != dataEntityOutput->GetId( ) ) );

}

void Core::OutputUpdater::SetDataEntityType( Core::DataEntityType val )
{
	m_DataEntityType = val;
}

DataEntity::Pointer Core::OutputUpdater::CreateDataEntityFromVectorData()
{
	//!
	std::vector<boost::any> dataVector;
	Core::CastAnyProcessingData( m_Data, dataVector );

	if ( dataVector.size() == 0 )
	{
		return NULL;
	}

	// Get old data entity
	Core::DataEntity::Pointer dataEntityOutput;
	dataEntityOutput = m_Holder->GetSubject( );

	// Find a factory for any data type
	BaseFactory::Pointer factory;
	factory = DataEntityImplFactoryHelper::FindFactoryByType( dataVector[ 0 ].type().name( ) );
	if ( factory.IsNull() )
	{
		std::ostringstream strError;
		strError << "Cannot manage " << dataVector[ 0 ].type().name( ) << std::endl;
		throw Core::Exceptions::Exception( 
			"OutputUpdater::CreateDataEntityFromVectorData",
			strError.str().c_str() );
	}

	bool buildOutput = CheckBuildOutput( 
		m_ReuseOutput, m_FatherDataEntity, 
		dataEntityOutput, unsigned( dataVector.size() ) );
	if ( buildOutput )
	{
		// Reuse metadata. Needed for reader
		blTagMap::Pointer metadata;
		Core::DataEntityPreview::Pointer preview;
		if ( dataEntityOutput.IsNotNull() )
		{
			 metadata = dataEntityOutput->GetMetadata();
			 preview = dataEntityOutput->GetPreview( );
		}
		dataEntityOutput = DataEntity::New( );
		if ( metadata.IsNotNull() )
		{
			dataEntityOutput->GetMetadata()->AddTags( metadata );
		}
		dataEntityOutput->GetMetadata()->SetName( m_DataEntityName );
		dataEntityOutput->GetPreview( )->Copy( preview );

		// Set specific metadata
		if ( m_Metadata.IsNotNull() )
		{
			dataEntityOutput->GetMetadata()->AddTags( m_Metadata );
		}

		Core::DataEntityType type = m_DataEntityType;
		if ( type == UnknownTypeId )
		{
			blTag::Pointer tag;
			tag = factory->GetProperties()->FindTagByName( "DefaultDataEntityType" );
			if ( tag.IsNotNull() )
			{
				type = tag->GetValueCasted<Core::DataEntityType>();
			}
		}
		dataEntityOutput->SetType( type );
		dataEntityOutput->Resize( int( dataVector.size() ), dataVector[ 0 ].type() );
	}

	// Copy processing data
	for ( unsigned i = 0 ; i < dataVector.size() ; i++ )
	{
		DataEntityImpl::Pointer impl = dataEntityOutput->GetTimeStep( i );
		switch ( m_CopyMethod )
		{
			case COPY_DATA: impl->DeepCopy( dataVector[ i ] );break;
			case COPY_POINTER: impl->SetDataPtr( dataVector[ i ] );break;
		}
	}

	// Update rendering
	if ( dataEntityOutput->GetFather( ).IsNull( ) && 
		m_FatherDataEntity.IsNotNull() &&
		dataEntityOutput->GetId() != m_FatherDataEntity->GetId() )
	{
		dataEntityOutput->SetFather( m_FatherDataEntity );
	}

	return dataEntityOutput;
}

DataEntity::Pointer Core::OutputUpdater::CreateDataEntityFromDataEntity()
{
	// Get old data entity
	Core::DataEntity::Pointer dataEntityOutput;
	dataEntityOutput = m_Holder->GetSubject( );

	DataEntity::Pointer dataEntity;
	Core::CastAnyProcessingData( m_Data, dataEntity );

	if ( m_ReuseOutput && dataEntityOutput.IsNotNull( ) && dataEntity != dataEntityOutput )
	{
		switch ( m_CopyMethod )
		{
			case COPY_DATA: dataEntityOutput->Copy( dataEntity );break;
			case COPY_POINTER: dataEntityOutput->CopyDataPtr( dataEntity );break;
		}

		// Preview
		dataEntityOutput->GetPreview( )->Copy( dataEntity->GetPreview( ) );
	}
	else
	{
		dataEntityOutput = dataEntity;
	}

	// Set specific metadata
	if ( m_Metadata.IsNotNull() )
	{
		dataEntityOutput->GetMetadata()->AddTags( m_Metadata );
	}


	// Set type specified by user, like ROITypeId
	// However, the DataEntity as a generic type should match specific type
	// For example, if DataEntity type is SurfaceMeshTypeId, we cannot set it to ImageTypeId
	// But if DataEntity type is ImageTypeId, we can set it to ImageTypeId | ROITypeId
	if ( m_DataEntityType != UnknownTypeId && 
		 ( dataEntityOutput->GetType() & m_DataEntityType ) != 0 )
	{
		dataEntityOutput->SetType( m_DataEntityType );
	}

	if ( dataEntityOutput->GetFather( ).IsNull( ) && 
		m_FatherDataEntity.IsNotNull() &&
		dataEntityOutput->GetId() != m_FatherDataEntity->GetId() )
	{
		dataEntityOutput->SetFather( m_FatherDataEntity );
	}

	return dataEntityOutput;
}

void Core::OutputUpdater::NotifyObservers( DataEntity::Pointer dataEntity )
{
	// Rendering data should be updated
	// All holders that contain this subject will notify observers
	if ( dataEntity )
	{
		dataEntity->Modified();
	}

	// Set new subject. This has only effect when the subject is new
	m_Holder->SetSubject( dataEntity );
}

void Core::OutputUpdater::SetMetadata( blTagMap::Pointer val )
{
	m_Metadata = val;
}

void Core::OutputUpdater::SetCopyMethod( Core::OutputUpdater::COPY_METHOD method )
{
	m_CopyMethod = method;
}


