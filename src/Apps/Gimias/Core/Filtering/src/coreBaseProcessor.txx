/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _BaseProcessor_TXX
#define _BaseProcessor_TXX


template <class ProcessingDataType>
void Core::BaseProcessor::GetProcessingData( 
	size_t num,
	ProcessingDataType &processingData,
	int iTimeStep /*= 0*/,
	bool generateNewInstance /*= false */ )
{
	DataEntity::Pointer	dataEntity;

	dataEntity = this->GetInputDataEntity( num );
	if ( dataEntity.IsNull( ) )
	{
		throw Core::Exceptions::Exception("DataEntityHelper::GetProcessingData", 
			"You must select an input data from the Processing Browser and set it as input" 
			);
	}

	boost::any anyData = processingData;
	GetInputPort( num )->UpdateInput( anyData, iTimeStep, generateNewInstance );
	Core::CastAnyProcessingData( anyData, processingData );
	if( !processingData )
	{
		throw Core::Exceptions::Exception("DataEntityHelper::GetProcessingData", 
			"You must select an input data from the Processing Browser and set it as input" 
			);
	}
}




/**
*/
template <class ProcessingDataType>
void Core::BaseProcessor::GetProcessingData( 
	size_t num,
	std::vector< ProcessingDataType> &processingDataVector,
	bool generateNewInstance /*= false */ )
{
	Core::DataEntity::Pointer	dataEntity;

	dataEntity = this->GetInputDataEntity( num );
	if ( dataEntity.IsNull( ) )
	{
		throw Core::Exceptions::Exception("DataEntityHelper::GetProcessingData", 
			"You must select an input data from the Processing Browser and set it as input" 
			);
	}

	for ( size_t iTimeStep = 0 ; iTimeStep < dataEntity->GetNumberOfTimeSteps( ) ; iTimeStep++ )
	{
		ProcessingDataType processingData = NULL;
		GetProcessingData( num, processingData, int( iTimeStep ), generateNewInstance );
		processingDataVector.push_back( processingData );
	}
}

template <typename T>
void Core::BaseProcessor::UpdateOutput( 
	int iIndex, 
	std::vector<T> dataVector,
	std::string dataEntityName,
	bool bReuseOutput /*= false*/,
	Core::DataEntity::Pointer fatherDataEntity /*= NULL */,
	blTagMap::Pointer metaData /*= NULL*/ )
{
	GetOutputPort( iIndex )->SetDataEntityName( dataEntityName );
	GetOutputPort( iIndex )->SetReuseOutput( bReuseOutput );
	GetOutputPort( iIndex )->SetMetadata( metaData );

	std::vector<boost::any> anyDataVector;
	anyDataVector.resize( dataVector.size( ) );
	for ( size_t i = 0 ; i < dataVector.size( ) ; i++ )
	{
		anyDataVector[ i ] = dataVector[ i ];
	}

	GetOutputPort( iIndex )->UpdateOutput( anyDataVector, fatherDataEntity );
}


#endif // _BaseProcessor_TXX


