/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreBaseFilter.h"
#include "coreEmptyUpdateCallback.h"

long Core::BaseFilter::m_IdCount = 0;

Core::BaseFilter::BaseFilter(void)
{
	m_UID = ++m_IdCount;

	m_iTimeStep = 0;
	SetMultithreading( false );
	m_UpdateCallback = EmptyUpdateCallback::New();
}

Core::BaseFilter::~BaseFilter(void)
{
}


void Core::BaseFilter::SetNumberOfInputs( size_t iNum )
{
	m_InputPortVector.resize( iNum );
	for ( size_t i = 0 ; i < iNum ; i++ )
	{
		m_InputPortVector[ i ] = Core::BaseFilterInputPort::New();
		m_InputPortVector[ i ]->SetProcessorName( GetName() );
		m_InputPortVector[ i ]->SetMultithreading( m_Multithreading );
	}

	Modified( );
}

void Core::BaseFilter::SetNumberOfOutputs( size_t iNum )
{
	size_t prevSize = m_OutputPortVector.size( );
	m_OutputPortVector.resize( iNum );
	for ( size_t i = prevSize ; i < iNum ; i++ )
	{
		m_OutputPortVector[ i ] = Core::BaseFilterOutputPort::New();
		m_OutputPortVector[ i ]->SetProcessorName( GetName() );
		m_OutputPortVector[ i ]->SetMultithreading( m_Multithreading );
	}

	Modified( );
}


/**
 */
Core::DataEntityHolder::Pointer 
Core::BaseFilter::GetInputDataEntityHolder( size_t iIndex )
{
	return m_InputPortVector.at( iIndex )->GetDataEntityHolder();
}

/**
*/
Core::DataEntityHolder::Pointer 
Core::BaseFilter::GetOutputDataEntityHolder( size_t iIndex )
{
	return m_OutputPortVector.at( iIndex )->GetDataEntityHolder( );
}


std::string Core::BaseFilter::GetInputDataName( 
	size_t iIndex )
{
	return m_InputPortVector[ iIndex ]->GetName( );
}

void Core::BaseFilter::SetInputDataName( 
	size_t iIndex, 
	const std::string strName )
{
	m_InputPortVector[ iIndex ]->SetName( strName );
}

void Core::BaseFilter::SetOutputDataName( 
	size_t iIndex, 
	const std::string strName )
{
	m_OutputPortVector[ iIndex ]->SetName( strName );
}

int Core::BaseFilter::GetTimeStep() const
{
	return m_iTimeStep;
}

void Core::BaseFilter::SetTimeStep( int val )
{
	m_iTimeStep = val;
}

size_t Core::BaseFilter::GetInputDataNumberOfTimeSteps( size_t iIndex )
{
	Core::DataEntity::Pointer dataEntity;
	dataEntity = GetInputDataEntityHolder( iIndex )->GetSubject( );
	size_t iTimeSteps = 0;
	if ( dataEntity.IsNotNull() )
	{
		iTimeSteps = dataEntity->GetNumberOfTimeSteps( );
	}
	return iTimeSteps;
}

void Core::BaseFilter::Abort()
{
	if ( m_UpdateCallback.IsNotNull() )
	{
		m_UpdateCallback->SetAbortProcessing( true );
	}
}

Core::DataEntity::Pointer 
Core::BaseFilter::GetInputDataEntity( size_t iIndex )
{
	return GetInputDataEntityHolder( iIndex )->GetSubject( );
}

Core::DataEntity::Pointer 
Core::BaseFilter::GetOutputDataEntity( size_t iIndex )
{
	return GetOutputDataEntityHolder( iIndex )->GetSubject( );
}

void Core::BaseFilter::SetInputDataEntity( size_t iIndex, Core::DataEntity::Pointer dataEntity )
{
	GetInputDataEntityHolder( iIndex )->SetSubject( dataEntity );
}

void Core::BaseFilter::SetOutputDataEntity( size_t iIndex, Core::DataEntity::Pointer dataEntity )
{
	GetOutputDataEntityHolder( iIndex )->SetSubject( dataEntity );
}

size_t Core::BaseFilter::GetNumberOfInputs()
{
	return m_InputPortVector.size();
}

size_t Core::BaseFilter::GetNumberOfOutputs()
{
	return m_OutputPortVector.size();
}

std::string Core::BaseFilter::GetName() const
{
	return m_Name;
}

void Core::BaseFilter::SetName( std::string val )
{
	m_Name = val;

	for ( unsigned i = 0 ; i < GetNumberOfInputs() ; i++ )
	{
		m_InputPortVector[ i ]->SetProcessorName( GetName() );
	}

	for ( unsigned i = 0 ; i < GetNumberOfOutputs() ; i++ )
	{
		m_OutputPortVector[ i ]->SetProcessorName( GetName() );
	}
}

Core::BaseFilterInputPort::Pointer 
Core::BaseFilter::GetInputPort( size_t num )
{
	return m_InputPortVector.at( num );
}

Core::BaseFilterOutputPort::Pointer 
Core::BaseFilter::GetOutputPort( size_t num )
{
	return m_OutputPortVector.at( num );
}

void Core::BaseFilter::Update()
{
	
}

bool Core::BaseFilter::Copy( BaseFilter* filter )
{
	if ( !InternalCopy( filter ) )
	{
		return false;
	}

	SetNumberOfInputs( filter->GetNumberOfInputs() );
	for ( unsigned i = 0 ; i < GetNumberOfInputs() ; i++ )
	{
		GetInputPort( i )->Copy( filter->GetInputPort( i ) );
	}

	SetNumberOfOutputs( filter->GetNumberOfOutputs() );
	for ( unsigned i = 0 ; i < GetNumberOfOutputs() ; i++ )
	{
		GetOutputPort( i )->Copy( filter->GetOutputPort( i ) );
	}

	m_iTimeStep = filter->GetTimeStep();
	m_Name = filter->GetName();
	SetMultithreading( filter->GetMultithreading( ) );

	return true;
}

bool Core::BaseFilter::InternalCopy( BaseFilter* filter )
{
	return false;
}

bool Core::BaseFilter::GetMultithreading() const
{
	return m_Multithreading;
}

void Core::BaseFilter::SetMultithreading( bool val )
{
	m_Multithreading = val;
	for ( unsigned i = 0 ; i < GetNumberOfInputs() ; i++ )
	{
		GetInputPort( i )->SetMultithreading( m_Multithreading );
	}
	for ( unsigned i = 0 ; i < GetNumberOfOutputs() ; i++ )
	{
		GetOutputPort( i )->SetMultithreading( m_Multithreading );
	}
}

Core::UpdateCallback::Pointer Core::BaseFilter::GetUpdateCallback() const
{
	return m_UpdateCallback;
}

void Core::BaseFilter::SetUpdateCallback( UpdateCallback::Pointer val )
{
	m_UpdateCallback = val;
}

long Core::BaseFilter::GetUID() const
{
	return m_UID;
}

Core::BaseFilter::DataEntityGroupIDType Core::BaseFilter::GetDataEntitiesID()
{
	DataEntityGroupIDType group;

	for ( unsigned i = 0 ; i < GetNumberOfInputs() ; i++ )
	{
		if ( GetInputDataEntity( i ).IsNotNull() )
		{
			group.push_back( GetInputDataEntity( i )->GetId() );
		}
	}
	for ( unsigned i = 0 ; i < GetNumberOfOutputs() ; i++ )
	{
		if ( GetOutputDataEntity( i ).IsNotNull() )
		{
			group.push_back( GetOutputDataEntity( i )->GetId() );
		}
	}

	return group;
}

bool Core::BaseFilter::IsExecutedLocally( )
{
	return true;
}

size_t Core::BaseFilter::ComputeNumberOfIterations()
{
	size_t maxTimeSteps = -1;
	for ( unsigned i = 0 ; i < GetNumberOfInputs() ; i++ )
	{
		// If the input is optional and is not active -> return 0
		if ( GetInputPort( i )->GetOptional() == true && GetInputPort( i )->GetActive() == false )
		{
			continue;
		}

		// If the input has only one selected time step, ignore it
		if ( GetInputPort( i )->GetTimeStepSelection() == BaseFilterInputPort::SELECT_SINGLE_TIME_STEP )
		{
			continue;
		}

		// Input port has multiple time access 
		if ( GetInputPort( i )->GetUpdateMode() == BaseFilterInputPort::UPDATE_ACCESS_MULTIPLE_TIME_STEP )
		{
			continue;
		}

		// count time steps for all selected 
		if ( GetInputPort( i )->GetDataEntity( ).IsNotNull( ) )
		{
			if (maxTimeSteps == -1) 
			{
				maxTimeSteps = GetInputPort( i )->GetDataEntity( )->GetNumberOfTimeSteps();
			}
			else
			{
				maxTimeSteps = std::min( maxTimeSteps, GetInputPort( i )->GetDataEntity( )->GetNumberOfTimeSteps() );
			}
		}
	}
	
	if (maxTimeSteps == -1) 
	{
		maxTimeSteps = 1;
	}

	return maxTimeSteps;
}

void Core::BaseFilter::UpdateSelectedPortsTimeStep( int timeStep )
{
	// Select input time step
	for ( unsigned portID = 0 ; portID < GetNumberOfInputs() ; portID++ )
	{
		switch( GetInputPort( portID )->GetTimeStepSelection() )
		{
		case BaseFilterInputPort::SELECT_ALL_TIME_STEPS: 
			GetInputPort( portID )->SetSelectedTimeStep( timeStep );
			break;
		case BaseFilterInputPort::SELECT_SINGLE_TIME_STEP: 
			GetInputPort( portID )->SetSelectedTimeStep( GetTimeStep() );
			break;
		}

	}

	// Select output time step
	for ( unsigned portID = 0 ; portID < GetNumberOfOutputs() ; portID++ )
	{
		GetOutputPort( portID )->SetSelectedTimeStep( timeStep );
	}

}
