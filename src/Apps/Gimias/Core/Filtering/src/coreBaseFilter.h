/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef CoreBaseFilter_H
#define CoreBaseFilter_H

// CoreLib
#include "coreObject.h"
#include "coreDataHolder.h"
#include "coreDataEntity.h"
#include "coreBaseFilterInputPort.h"
#include "coreBaseFilterOutputPort.h"
#include "coreUpdateCallback.h"

namespace Core{


/**
\brief Generic Filter

It has a vector of DataEntityHolder for storing the selected processor input
of the data entity list and a vector of DataEntityHolder for output.

BaseFilter uses input and output ports to communicate with the rest of the 
framework. Each port has a holder that will send notifications each time 
the DataEntity is modified. The constructor of the concrete filter should 
initialize the number of input and output ports, and also the type of 
inputs is required.

You should not create a subclass of BaseFilter. Is better to use the 
subclass BaseProcessor.

A filter can access a DataEntity with multiple time steps or just process
a single time step. By default, a filter process a single time step but 
you can change this behavior calling BaseFilterInputPort::SetUpdateMode( ).

When configured as UPDATE_ACCESS_SINGLE_TIME_STEP, and calling Update(), 
the filter processes a single time step. To process multiple time steps, 
the framework needs to call Update( ) several times using an iteration loop 
and calling ComputeNumberOfIterations( ) to compute number of iteration of the loop.

\section Examples Examples

Initialize the filter at constructor:

\code
ExtractScalarProcessor::ExtractScalarProcessor( )
{
	SetName( "ExtractScalar" );

	SetNumberOfInputs( 2 );
	GetInputPort( 0 )->SetName( "Input mesh" );
	GetInputPort( 0 )->SetDataEntityType( Core::SurfaceMeshTypeId );
	GetInputPort( 1 )->SetName( "Input point" );
	GetInputPort( 1 )->SetDataEntityType( Core::PointSetTypeId );

	SetNumberOfOutputs( 1 );
	GetOutputPort( 0 )->SetDataEntityType( Core::SignalTypeId );
	GetOutputPort( 0 )->SetReuseOutput( true );
	GetOutputPort( 0 )->SetName( "Output signal" );
	GetOutputPort( 0 )->SetDataEntityName( "Scalar" );
}

\endcode

To retrieve the processing data:

\code
void ExtractScalarProcessor::Update()
{
	vtkPolyDataPtr surfaceMesh;
	GetProcessingData( 0, surfaceMesh );
}
\endcode

\ingroup gmFiltering
\author Xavi Planes
\date 05 06 09
*/
class GMFILTERING_EXPORT BaseFilter : public Core::SmartPointerObject
{
public:
	//!
	coreDeclareSmartPointerTypesMacro(Core::BaseFilter,Core::SmartPointerObject)
	coreClassNameMacro(Core::BaseFilter)
	coreTypeMacro(Core::BaseFilter,Core::SmartPointerObject)

	//! Group of DataEntity ID
	typedef std::list<long> DataEntityGroupIDType;

	//!
	virtual void Update( );

	//! Processor name
	virtual std::string GetName() const;

	//! Processor name
	void SetName(std::string val);

	//! Resize number of inputs with new DataEntityHolder and NULL Subject
	void SetNumberOfInputs( size_t iNum );

	//! Resize number of outputs with new DataEntityHolder and NULL Subject
	void SetNumberOfOutputs( size_t iNum );

	//! 
	size_t GetNumberOfInputs( );

	//! 
	size_t GetNumberOfOutputs( );

	//!
	Core::DataEntity::Pointer GetInputDataEntity( size_t iIndex );

	//!
	void SetInputDataEntity( size_t iIndex, Core::DataEntity::Pointer dataEntity );

	//!
	Core::DataEntity::Pointer GetOutputDataEntity( size_t iIndex = 0 );

	//!
	void SetOutputDataEntity( size_t iIndex, Core::DataEntity::Pointer dataEntity );

	//!
	Core::DataEntityHolder::Pointer GetInputDataEntityHolder( size_t iIndex );

	//!
	Core::DataEntityHolder::Pointer GetOutputDataEntityHolder( size_t iIndex );

	//! Return the name of this input as "Input image"
	std::string GetInputDataName( size_t iIndex );

	//! Set the name of this input as "Input image"
	void SetInputDataName( size_t iIndex, const std::string strName );

	//! Set the name of this output as "Output image"
	void SetOutputDataName( size_t iIndex, const std::string strName );

	//! Return GetNumberOfTimeSteps( ) of the input data
	size_t GetInputDataNumberOfTimeSteps( size_t iIndex );

	//!
	int GetTimeStep() const;

	//!
	void SetTimeStep(int val);

	//! Abort processing
	virtual void Abort( );

	//!
	Core::BaseFilterInputPort::Pointer GetInputPort( size_t num );

	//!
	Core::BaseFilterOutputPort::Pointer GetOutputPort( size_t num );

	/** Create a copy of this processor to execute it in background
	You should implement InternalCopy( ) to copy all parameters
	*/
	bool Copy( BaseFilter* filter );

	//! Enable Multi threading
	bool GetMultithreading() const;
	void SetMultithreading(bool val);

	//!
	UpdateCallback::Pointer GetUpdateCallback() const;
	void SetUpdateCallback(UpdateCallback::Pointer val);

	//!
	long GetUID() const;

	//! Get all DataEntities references by this filter
	DataEntityGroupIDType GetDataEntitiesID( );

	/** Check if this processor will be executed locally or remotely.
	This will be used to use the Default execution queue or the Remote
	execution queue.
	*/
	virtual bool IsExecutedLocally( );

	/** Compute number of iterations that this processor will be executed
	based on the number of selected input data time steps and m_TimeStepProcessing
	*/
	virtual size_t ComputeNumberOfIterations( );

	//! Update input / output ports with selected time step
	virtual void UpdateSelectedPortsTimeStep( int timeStep );

protected:
	//!
	BaseFilter(void);

	//!
	virtual ~BaseFilter(void);

private:
	//!
	BaseFilter( const Self& );

	//!
	void operator=(const Self&);

	//! 
	virtual bool InternalCopy( BaseFilter* filter );

protected:
	//! Unique identifier for this processor instance
	long m_UID;

	//! Input data entity for this processor
	std::vector<BaseFilterInputPort::Pointer> m_InputPortVector;

	//! Output of this processor
	std::vector<BaseFilterOutputPort::Pointer> m_OutputPortVector;

	//! Process only this time step
	int m_iTimeStep;

	//! Filter name
	std::string m_Name;

	//!
	bool m_Multithreading;

	//!
	UpdateCallback::Pointer m_UpdateCallback;

	// ID count of already created instances
	static long m_IdCount;
};

} // namespace Core{

#include "coreBaseFilter.txx"

#endif // ProcessorWorkingData_H


