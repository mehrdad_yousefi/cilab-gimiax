/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreBaseFilterPort.h"


Core::BaseFilterPort::BaseFilterPort()
{
	m_DataEntityHolder = Core::DataEntityHolder::New();
	m_DataEntityType = Core::UnknownTypeId;
	m_Optional = false;
	m_Active = false;
	m_Multithreading = false;
	m_SelectedTimeStep = 0;
	m_WaitPortUpdate = true;
}

Core::BaseFilterPort::~BaseFilterPort()
{

}

Core::DataEntityHolder::Pointer Core::BaseFilterPort::GetDataEntityHolder() const
{
	return m_DataEntityHolder;
}

std::string Core::BaseFilterPort::GetName() const
{
	return m_Name;
}

void Core::BaseFilterPort::SetName( std::string val )
{
	m_Name = val;
	m_DataEntityHolder->SetName( m_ProcessorName + ": " + m_Name );
}

Core::DataEntity::Pointer Core::BaseFilterPort::GetDataEntity()
{
	return m_DataEntityHolder->GetSubject();
}

bool Core::BaseFilterPort::SetDataEntity( Core::DataEntity::Pointer val )
{
	if ( !CheckDataEntityRestrictions( val ) )
	{
		return false;
	}

	// Always notify observers because when automatic is on, we
	// need to update the views sometimes
	// Modifictaion: In cardiac initialization, when the selection changes
	// and the input is the same, we need to update the observers (volume
	// computation), so we force the notification
	// Xavi: Changed with the new pipeline
	m_DataEntityHolder->SetSubject( val );

	return true;
}

std::string Core::BaseFilterPort::GetProcessorName() const
{
	return m_ProcessorName;
}

void Core::BaseFilterPort::SetProcessorName( std::string val )
{
	m_ProcessorName = val;
	m_DataEntityHolder->SetName( m_ProcessorName + ": " + m_Name );
}

Core::DataEntityType Core::BaseFilterPort::GetDataEntityType() const
{
	return m_DataEntityType;
}

void Core::BaseFilterPort::SetDataEntityType( int val )
{
	m_DataEntityType = Core::DataEntityType( val );
}

bool Core::BaseFilterPort::GetOptional() const
{
	return m_Optional;
}

void Core::BaseFilterPort::SetOptional( bool val )
{
	m_Optional = val;
}

bool Core::BaseFilterPort::GetActive() const
{
	return m_Active;
}

void Core::BaseFilterPort::SetActive( bool val )
{
	m_Active = val;
}

void Core::BaseFilterPort::Copy( BaseFilterPort* port )
{
	m_DataEntityHolder->SetSubject( port->m_DataEntityHolder->GetSubject() );
	m_Name = port->GetName();
	m_ProcessorName = port->GetProcessorName();
	m_DataEntityType = port->GetDataEntityType();
	m_Optional = port->GetOptional();
	m_Active = port->GetActive();
	m_SelectedTimeStep = port->GetSelectedTimeStep();
	m_WaitPortUpdate = port->GetWaitPortUpdate( );

	InternalCopy( port );
}

void Core::BaseFilterPort::InternalCopy( BaseFilterPort* port )
{
	
}

bool Core::BaseFilterPort::GetMultithreading() const
{
	return m_Multithreading;
}

void Core::BaseFilterPort::SetMultithreading( bool val )
{
	m_Multithreading = val;
}

int Core::BaseFilterPort::GetSelectedTimeStep() const
{
	return m_SelectedTimeStep;
}

void Core::BaseFilterPort::SetSelectedTimeStep( int val )
{
	m_SelectedTimeStep = val;
}

bool Core::BaseFilterPort::GetWaitPortUpdate( ) const
{
	return m_WaitPortUpdate;
}

void Core::BaseFilterPort::SetWaitPortUpdate( bool val )
{
	m_WaitPortUpdate = val;
}
