/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreOutputUpdater_H
#define _coreOutputUpdater_H

#include "gmFilteringWin32Header.h"
#include "coreDataEntityHolder.h"
#include "corePortUpdater.h"

namespace Core
{

/** 
Updates BaseProcessor outputs

\ingroup gmFiltering
\author Xavi Planes
\date Jan 2011
*/
class GMFILTERING_EXPORT OutputUpdater : public PortUpdater 
{
public:
	enum COPY_METHOD
	{
		/** Copy the input pointer to the destination pointer.
		The memory is allocated by the Processor
		*/
		COPY_POINTER,
		/** Copy the data from the input pointer to the destination.
		The memory is allocated by the destination DataEntityImpl class
		*/
		COPY_DATA
	};
	//!
	coreDeclareSmartPointerClassMacro(Core::OutputUpdater,PortUpdater)

	//! 
	void SetHolder(DataEntityHolder::Pointer val);
	void SetData(boost::any val);
	void SetReuseOutput(bool val);
	void SetDataEntityName(std::string val);
	void SetTotalTimeSteps(int val);
	void SetTimeStep(int val);
	void SetFatherDataEntity(Core::DataEntity::Pointer val);
	void SetDataEntityType(Core::DataEntityType val);
	void SetMetadata(blTagMap::Pointer val);
	void SetCopyMethod( OutputUpdater::COPY_METHOD method );

	//!
	void UpdatePort( );

protected:
	//!
	OutputUpdater( );
	//!
	virtual ~OutputUpdater( );
	//!
	DataEntity::Pointer CreateDataEntityFromSimpleData( );
	//!
	DataEntity::Pointer CreateDataEntityFromVectorData( );
	//!
	DataEntity::Pointer CreateDataEntityFromDataEntity( );

	//!
	void NotifyObservers( DataEntity::Pointer dataEntity );

	//!
	bool CheckBuildOutput( 
		bool bReuseOutput,
		Core::DataEntity::Pointer fatherDataEntity,
		Core::DataEntity::Pointer dataEntityOutput,
		unsigned totalTimeSteps );

protected:
	//!
	boost::any m_Data;
	//!
	int m_TimeStep;
	//!
	Core::DataEntity::Pointer m_FatherDataEntity;
	//!
	bool m_ReuseOutput;
	//!
	std::string m_DataEntityName;
	//!
	int m_TotalTimeSteps;
	//!
	Core::DataEntityType m_DataEntityType;
	//!
	DataEntityHolder::Pointer m_Holder;
	//!
	blTagMap::Pointer m_Metadata;
	//! How to copy output data
	COPY_METHOD m_CopyMethod;
};

} // namespace Core

#endif // _coreOutputUpdater_H
