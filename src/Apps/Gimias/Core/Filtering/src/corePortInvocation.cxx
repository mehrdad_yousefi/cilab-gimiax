/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "corePortInvocation.h"

using namespace Core;

Core::PortInvocation::PortInvocation()
{
}

Core::PortInvocation::~PortInvocation()
{

}


PortUpdater::Pointer Core::PortInvocation::GetPortUpdater() const
{
	return m_PortUpdater;
}

void Core::PortInvocation::SetPortUpdater( PortUpdater::Pointer val )
{
	m_PortUpdater = val;
}
