/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreBaseFilterInputPort.h"
#include "coreInputUpdater.h"
#include "coreFactoryManager.h"

Core::BaseFilterInputPort::BaseFilterInputPort()
{
	m_Modality = Core::UnknownModality;
	m_NotValidDataEntityType = Core::DataEntityType( 0 );
	m_TimeStepSelection = SELECT_SINGLE_TIME_STEP;

	// By default is using multiple time steps to mantains backwards compatibility
	m_UpdateAccessMode = UPDATE_ACCESS_MULTIPLE_TIME_STEP;
}

Core::BaseFilterInputPort::~BaseFilterInputPort()
{

}

void Core::BaseFilterInputPort::SetDataEntityType( int val )
{
	BaseFilterPort::SetDataEntityType( val );

	if ( !CheckDataEntityRestrictions( GetDataEntity() ) )
	{
		Superclass::SetDataEntity( NULL );
	}
}

Core::ModalityType Core::BaseFilterInputPort::GetModality() const
{
	return m_Modality;
}

void Core::BaseFilterInputPort::SetModality( Core::ModalityType val )
{
	m_Modality = val;

	if ( !CheckDataEntityRestrictions( GetDataEntity() ) )
	{
		Superclass::SetDataEntity( NULL );
	}
}

bool Core::BaseFilterInputPort::CheckDataEntityRestrictions( 
	Core::DataEntity::Pointer val )
{
	if ( val.IsNull() )
	{
		return true;
	}

	if ( !CheckAllowedInputDataTypes( val ) )
	{
		return false;
	}

	if ( GetDataEntityType() == UnknownTypeId )
	{
		return true;
	}

	if ( ( GetDataEntityType() & val->GetType() ) == 0 ||
		 ( m_NotValidDataEntityType == val->GetType() ) )
	{
		return false;
	}

 	return true;
}

bool Core::BaseFilterInputPort::CheckAllowedInputDataTypes( 
	Core::DataEntity::Pointer val )
{
	bool validModality = 
		m_Modality == Core::UnknownModality || 
		m_Modality == val->GetMetadata()->GetModality();

	bool validTagList = true;
	std::list<DataEntityTag::Pointer>::iterator it;
	for ( it = m_ValidTagList.begin() ; it != m_ValidTagList.end() ; it++)
	{
		DataEntityTag::Pointer validTag = *it;
		DataEntityTag::Pointer inputTag = val->GetMetadata()->FindTagByName( validTag->GetName() );
		if ( inputTag.IsNotNull() )
		{
			validTagList = validTagList && validTag->Compare( inputTag );
		}
	}

	return validModality && validTagList;
}

Core::DataEntityType Core::BaseFilterInputPort::GetNotValidDataEntityType() const
{
	return m_NotValidDataEntityType;
}

void Core::BaseFilterInputPort::SetNotValidDataEntityType( int val )
{
	m_NotValidDataEntityType = Core::DataEntityType( val );

	if ( !CheckDataEntityRestrictions( GetDataEntity() ) )
	{
		Superclass::SetDataEntity( NULL );
	}
	
}

void Core::BaseFilterInputPort::AddValidDataEntityTag( DataEntityTag::Pointer tag )
{
	m_ValidTagList.push_back( tag );

	if ( !CheckDataEntityRestrictions( GetDataEntity() ) )
	{
		Superclass::SetDataEntity( NULL );
	}
	
}

void Core::BaseFilterInputPort::InternalCopy( BaseFilterPort* port )
{
	BaseFilterInputPort* inputPort = dynamic_cast<BaseFilterInputPort*> ( port );
	m_Modality = inputPort->GetModality();
	m_NotValidDataEntityType = inputPort->GetNotValidDataEntityType();
	m_ValidTagList = inputPort->m_ValidTagList;
	m_TimeStepSelection = inputPort->m_TimeStepSelection;
	m_UpdateAccessMode = inputPort->m_UpdateAccessMode;
}

void Core::BaseFilterInputPort::UpdateInput( 
	boost::any &anyData, 
	int iTimeStep, 
	bool generateNewInstance /*= false */ )
{
	InputUpdater::Pointer updater = InputUpdater::New( );
	updater->SetHolder( GetDataEntityHolder() );
	updater->SetData( anyData );
	if ( iTimeStep == -1 )
	{
		updater->SetTimeStep( GetSelectedTimeStep() );
	}
	else
	{
		updater->SetTimeStep( iTimeStep );
	}
	updater->SetMultithreading( GetMultithreading() );
	updater->SetGenerateNewInstance( generateNewInstance );
	updater->Update( );

	// Retrieve output
	anyData = updater->GetData( );
}

boost::any Core::BaseFilterInputPort::UpdateInput( 
	const std::string &datatypename,
	int iTimeStep )
{
	InputUpdater::Pointer updater = InputUpdater::New( );
	updater->SetHolder( GetDataEntityHolder() );
	updater->SetDataTypeName( datatypename );
	if ( iTimeStep == -1 )
	{
		updater->SetTimeStep( GetSelectedTimeStep() );
	}
	else
	{
		updater->SetTimeStep( iTimeStep );
	}
	updater->SetMultithreading( GetMultithreading() );
	updater->Update( );

	return updater->GetData();
}

Core::BaseFilterInputPort::SELECT_TIME_STEP_TYPE 
Core::BaseFilterInputPort::GetTimeStepSelection() const
{
	return m_TimeStepSelection;
}

void Core::BaseFilterInputPort::SetTimeStepSelection( SELECT_TIME_STEP_TYPE val )
{
	m_TimeStepSelection = val;
}

Core::BaseFilterInputPort::UPDATE_ACCESS_MODE Core::BaseFilterInputPort::GetUpdateMode() const
{
	return m_UpdateAccessMode;
}

void Core::BaseFilterInputPort::SetUpdateMode( UPDATE_ACCESS_MODE val )
{
	m_UpdateAccessMode = val;
}
