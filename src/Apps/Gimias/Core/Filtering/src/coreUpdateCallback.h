/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreUpdateCallback_H
#define _coreUpdateCallback_H

// CoreLib
#include "gmFilteringWin32Header.h"
#include "coreObject.h"

namespace Core{

class BaseFilter;

/**
\brief Updates observers with status of BaseFilter execution

BaseFilter cannot be stored here because there will be a cyclic dependency

Each time you change a property, you need to call Modified to update obervers

\ingroup gmFiltering
\author Xavi Planes
\date Jan 2011
*/
class GMFILTERING_EXPORT UpdateCallback : public Core::SmartPointerObject
{

public:
	coreDeclareSmartPointerTypesMacro(Core::UpdateCallback,Core::SmartPointerObject)
	coreClassNameMacro(Core::UpdateCallback)
	coreTypeMacro(Core::UpdateCallback,SmartPointerObject)

	//!
	float GetProgress() const;
	void SetProgress(float val);
	std::string GetStatusMessage() const;
	void SetStatusMessage(std::string val);
	bool GetAbortProcessing() const;
	void SetAbortProcessing(bool val);
	std::string GetExceptionMessage() const;
	void SetExceptionMessage(std::string val);
	void AddExceptionMessage(std::string val);
	std::string GetInformationMessage() const;
	void SetInformationMessage(std::string val);
	void AddInformationMessage(std::string val);
	long GetProcessorThreadID() const;
	void SetProcessorThreadID(long val);
	bool GetDetachProcessing() const;
	void SetDetachProcessing(bool val);

	//! Only send modified event if m_Modified is true
	virtual void Modified() const;

protected:
	//!
	UpdateCallback( );

	//!
	virtual ~UpdateCallback( );

private:
	//! Progress of current processor
	float m_Progress;
	//! Status message
	std::string m_StatusMessage;
	//! The user canceled the process
	bool m_AbortProcessing;
	//! Exception thrown from processor
	std::string m_ExceptionMessage;
	//! Detailed information message after execution (or std::cout if CLP)
	std::string m_InformationMessage;
	//! ID of the processor thread
	long m_ProcessorThreadID;
	/** Detach process from GIMIAS, but keep process ID, so later we can 
	recover status */
	bool m_DetachProcessing;
	//! Will be set to true if any member has changed
	bool m_Modified;
};

} // namespace Core{

#endif // _coreUpdateCallback_H


