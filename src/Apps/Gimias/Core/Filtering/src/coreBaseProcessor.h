/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreBaseProcessor_H
#define _coreBaseProcessor_H

// CoreLib
#include "gmFilteringWin32Header.h"
#include "coreObject.h"
#include "coreDataHolder.h"
#include "coreDataEntity.h"
#include "coreBaseFilter.h"

#include "ModuleDescription.h"

namespace Core{

/**
\brief Generic BaseProcessor with UpdateOutput( ) functions for different types
of data

\ingroup gmFiltering
\author Xavi Planes
\date 10 09 09
*/
class GMFILTERING_EXPORT BaseProcessor : public Core::BaseFilter
{
public:
	//!
	coreDeclareSmartPointerTypesMacro(Core::BaseProcessor, Core::BaseFilter)
	coreClassNameMacro(Core::BaseProcessor)
	coreTypeMacro(Core::BaseProcessor,Core::BaseFilter)

	/** Get input processing data of a data entity and throw exception if error
	\param [in] generateNewInstance Generate a new instance of processing data
	\param [in] iTimeStep Select a time step to retrieve. If the value is -1, 
	use the selected time step from input port
	*/
	template <class ProcessingDataType>
	void GetProcessingData( 
		size_t num,
		ProcessingDataType &processingData,
		int iTimeStep = -1, 
		bool generateNewInstance = false );

	/** Get input processing data of a data entity and throw exception if error
	\param [in] generateNewInstance Generate a new instance of processing data
	*/
	template <class ProcessingDataType>
	void GetProcessingData( 
		size_t num,
		std::vector< ProcessingDataType> &processingDataVector,
		bool generateNewInstance = false );

	/**
	\brief Update the subject of m_OutputDataEntityHolderVector[ iIndex ]
	with polyData at time step GetTimeStep( ). 
	
	Perform a DeepCopy() of polyData

	\note Always notify observers

	\param [in] iIndex
	\param [in] data Data to set
	\param [in] strDataEntityName Name of the data entity
	\param [in] bReuseOutput If this is true, the DataEntity of 
		m_OutputDataEntityHolderVector[ iIndex ] will be reused else
		a new DataEntity will be created
	\param [in] iTotalTimeSteps Total time steps of the output data. If this value
	cannot be < ComputeNumberOfIterations( ).
	\param [in] blTagMap Metadata for output DataEntity
	*/
	void UpdateOutput( 
		int iIndex, 
		boost::any data,
		std::string strDataEntityName,
		bool bReuseOutput = false,
		int iTotalTimeSteps = 1,
		Core::DataEntity::Pointer fatherDataEntity = NULL,
		blTagMap::Pointer metaData = NULL );

	//! Use a vector of data to update the output
	template<class T>
	void UpdateOutput( 
		int iIndex, 
		std::vector<T> imageDataVector,
		std::string strDataEntityName,
		bool bReuseOutput = false,
		Core::DataEntity::Pointer fatherDataEntity = NULL,
		blTagMap::Pointer metaData = NULL );

	//! Use a Dataentity to update the output
	void UpdateOutput( 
		int iIndex, 
		Core::DataEntity::Pointer dataEntity,
		Core::DataEntity::Pointer fatherDataEntity = NULL,
		blTagMap::Pointer metaData = NULL );

	//! Set all parameter values
	void SetParameters( blTagMap::Pointer tagMap );

	//! Get all parameter values 
	void GetParameters( blTagMap::Pointer tagMap );

	/** Set parameter value.

	For example if the parameter name is "InvertSliceOrder", and
	the value is of type bool, to set the parameter value you need to call:

	if ( tag->GetName() == "InvertSliceOrder" )
	{
		tag->GetValue( m_InvertSliceOrder );
	}

	\note By default it is not implemented and throw an exception
	*/
	virtual void SetParameter( blTag::Pointer tag );

	/** Get parameter value.

	For example if the parameter name is "InvertSliceOrder", and
	the value is of type bool, to get the parameter value you need to call:

	if ( tag->GetName() == "InvertSliceOrder" )
	{
		tag->SetValue( m_InvertSliceOrder );
	}

	\note By default it is not implemented and throw an exception
	*/
	virtual void GetParameter( blTag::Pointer tag );

protected:
	//!
	BaseProcessor(void);

	//!
	virtual ~BaseProcessor(void);

private:
	//!
	BaseProcessor( const Self& );

	//!
	void operator=(const Self&);


protected:
};

} // namespace Core{

#include "coreBaseProcessor.txx"

/**
\brief Create standard smart pointer and processor factory
\ingroup gmKernel
\author Xavi Planes
\date 10 May 2010
*/
#define coreProcessor(className,SuperClassName) \
	coreDeclareSmartPointerClassMacro(className,SuperClassName)\
	coreDefineFactoryClass( className )\
	coreDefineFactoryClassEnd( )

#endif // _coreBaseProcessor_H


