/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreEmptyUpdateCallback_H
#define _coreEmptyUpdateCallback_H

#include "gmFilteringWin32Header.h"
#include "coreBaseFactory.h"
#include "coreUpdateCallback.h"

namespace Core
{
	class BaseFilter;

/** 
Empty update callback to simplify code

Stores a raw pointer to the BaseFilter

\ingroup gmFiltering
\author Xavi Planes
\date Jan 2011
*/
class GMFILTERING_EXPORT EmptyUpdateCallback : public UpdateCallback 
{
public:
	//!
	coreDeclareSmartPointerClassMacro( Core::EmptyUpdateCallback, UpdateCallback );
	coreDefineFactory( Core::EmptyUpdateCallback );

protected:
	//!
	EmptyUpdateCallback( );
	//!
	virtual ~EmptyUpdateCallback( );

protected:
};

} // namespace Core

#endif // _coreEmptyUpdateCallback_H
