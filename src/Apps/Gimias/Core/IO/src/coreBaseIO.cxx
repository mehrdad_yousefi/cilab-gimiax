/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreBaseIO.h"
#include "coreFile.h"
#include <cctype>


long Core::IO::BaseIO::GetLastDigits( const std::string &filename )
{
	std::string name = Core::IO::File::GetFilenameName( filename );

	// Remove extension
	name = itksys::SystemTools::GetFilenameWithoutExtension( name );

	// Remove last digits
	long pos = long( name.size() ) - 1;
	while ( pos > 0 && name.size() && std::isdigit( name[ pos ] ) )
	{
		pos--;
	}
	pos++;

	if ( pos < 0 || size_t( pos ) >= name.size() )
	{
		return -1;
	}

	std::string digit = name.substr( pos, name.size() - 1 );
	return atoi( digit.c_str() );
}

bool Core::IO::BaseIO::CompareStringNumbers( const std::string &str1,const std::string &str2 )
{
	return GetLastDigits( str1 ) < GetLastDigits( str2 ); 
}

std::string Core::IO::BaseIO::RemoveExtensionAndLastDigits( 
	const std::string &filename,
	const std::string &ext /*= ""*/ )
{
	std::string name = Core::IO::File::GetFilenameName( filename );

	// Remove extension
	size_t pos = Core::StringHelper::ToLowerCase( name ).rfind( ext );
	if ( ext.empty() || pos == std::string::npos )
	{
		name = itksys::SystemTools::GetFilenameWithoutExtension( name );
	}
	else
	{
		name = name.substr( 0, pos );
	}

	// Remove last digits
	while ( name.size() && std::isdigit( name[ name.size() - 1 ] ) )
	{
		name = name.substr( 0, name.size() - 1 );
	}

	// Remove '_' at the end
	if ( name.size() && name[ name.size() - 1 ] == '_' )
	{
		name = name.substr( 0, name.size() - 1 );
	}
	
	return name;
}

Core::IO::BaseIO::BaseIO( void )
{
	m_TagMap = blTagMap::New( );
	m_UseMultipleTimeSteps = true;
	m_TransferType = LOCAL_TRANSFER_TYPE;
}

Core::IO::BaseIO::~BaseIO( void )
{

}

void Core::IO::BaseIO::SetFileNames( 
	const std::vector< std::string > &filenames )
{
	m_Filenames = filenames;
}

std::vector< std::string > Core::IO::BaseIO::GetFileNames( )
{
	return m_Filenames;
}

void Core::IO::BaseIO::SetFileName( const std::string &filename )
{
	m_Filenames.clear( );
	m_Filenames.push_back( filename );
}

std::string Core::IO::BaseIO::GetFileName( )
{
	if ( m_Filenames.empty( ) )
	{
		return "";
	}

	return m_Filenames[ 0 ];
}

bool Core::IO::BaseIO::Check( Core::DataEntityType type )
{
	ValidTypesListType types = GetValidDataEntityTypes( );
	ValidTypesListType::iterator it;
	for ( it = types.begin() ; it != types.end() ; it++ )
	{
		if ( (*it) & type )
		{
			return true;
		}
	}

	return false;
}

bool Core::IO::BaseIO::Check( const std::string &filename )
{
	m_Extension = "";
	ValidExtensionsListType extensions = GetValidExtensions( );
	ValidExtensionsListType::iterator it;
	for ( it = extensions.begin() ; it != extensions.end() ; it++ )
	{
		// Find most right position of the extension
		size_t pos = filename.rfind( (*it) );
		std::string filenameName = itksys::SystemTools::GetFilenameName( filename );
		if ( !it->empty() && pos != std::string::npos && pos + it->size() == filename.size() )
		{
			m_Extension = itksys::SystemTools::LowerCase( (*it) );
			return true;
		}
		// files with no extension
		else if ( it->empty() && filenameName.find(".") == std::string::npos )
		{
			m_Extension = itksys::SystemTools::LowerCase( (*it) );
			return true;
		}
		else if ( (*it) == ".*" )
		{
			std::string ext = itksys::SystemTools::GetFilenameExtension(filename);
			m_Extension = itksys::SystemTools::LowerCase(ext);
			return true;
		}
	}

	return false;
}

Core::IO::BaseIO::ValidExtensionsListType 
Core::IO::BaseIO::GetValidExtensions()
{
	return m_ValidExtensionsList;
}

Core::IO::BaseIO::ValidTypesListType 
Core::IO::BaseIO::GetValidDataEntityTypes()
{
	return m_ValidTypesList;
}

blTagMap::Pointer Core::IO::BaseIO::GetTagMap() const
{
	return m_TagMap;
}

void Core::IO::BaseIO::SetTagMap( blTagMap::Pointer val )
{
	m_TagMap = val;
}

bool Core::IO::BaseIO::GetUseMultipleTimeSteps() const
{
	return m_UseMultipleTimeSteps;
}

void Core::IO::BaseIO::SetUseMultipleTimeSteps( bool val )
{
	m_UseMultipleTimeSteps = val;
}

std::string Core::IO::BaseIO::GetExtension() const
{
	return m_Extension;
}

void Core::IO::BaseIO::SetExtension( const std::string &ext )
{
	m_Extension = ext;
}

Core::IO::BaseIO::TRANSFER_TYPE Core::IO::BaseIO::GetTransferType( )
{
	return m_TransferType;
}