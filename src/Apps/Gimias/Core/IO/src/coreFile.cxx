/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreFile.h"
#include "coreException.h"
#include "coreDirectory.h"
#include <itksys/SystemTools.hxx>

#include <stdio.h>
#include <string>
#include <iostream>
#include <iomanip>

using namespace Core::IO;

//!
File::File(void)
{
}

//!
File::~File(void)
{
}

//!
void File::SetFileNameFullPath(const std::string& fullPath)
{
	this->m_FileNameFullPath = fullPath;
	DirectoryHelper::ConvertToOutputPath(this->m_FileNameFullPath);
	size_t pos = this->m_FileNameFullPath.find_last_of(SlashChar);
	if((pos > 0) && (pos < this->m_FileNameFullPath.length()))
	{
		this->m_PathToFile = this->m_FileNameFullPath.substr(0, pos);
		this->m_FileName = this->m_FileNameFullPath.substr(pos+1, this->m_FileNameFullPath.length());
		DirectoryHelper::RemoveQuotesFromPath(this->m_PathToFile);
		DirectoryHelper::RemoveQuotesFromPath(this->m_FileNameFullPath);
	}
	else
	{
		// throw exception
	}
}


//!
void File::SetFileName(const std::string& path, const std::string& fileName)
{
	this->m_FileName = fileName;
	this->m_PathToFile = path;
	this->m_FileNameFullPath = path + SlashChar + fileName;
	DirectoryHelper::RemoveQuotesFromPath(this->m_PathToFile);
	DirectoryHelper::RemoveQuotesFromPath(this->m_FileNameFullPath);
}

//!		
bool File::Exists(void)
{
	FILE* fp;
	bool exists = false;
	if(this->m_FileNameFullPath.length() > 0)
	{
		fp = fopen(this->m_FileNameFullPath.c_str(), "r");
		exists = (fp != NULL);
		if(exists) fclose(fp);
	}
	return exists;
}

//!
void File::Create(void)
{
	FILE* fp;
	if(this->m_FileNameFullPath.length() > 0)
	{
		fp = fopen(this->m_FileNameFullPath.c_str(), "a");
		if(fp != NULL) fclose(fp);
	}
}

/**
* Return file name of a full filename (i.e. file name without path)
*/
std::string File::GetFilenameName(const std::string& fileName)
{
	return itksys::SystemTools::GetFilenameName(fileName);
}


/**
* Return file name without extension of a full filename
*/
std::string File::GetFilenameWithoutExtension(const std::string& fileName)
{
	return itksys::SystemTools::GetFilenameWithoutExtension(fileName);
}

//!
void File::Append(const std::string& input)
{
	FILE* fp;
	size_t nread = 0;
	if(this->m_FileNameFullPath.length() > 0)
	{
		try
		{
			fp = fopen(this->m_FileNameFullPath.c_str(), "a");
			nread = fwrite(input.c_str(), sizeof(char), input.length(), fp);
			if(fp != NULL) fclose(fp);
		}
		coreCatchExceptionsAddTraceAndThrowMacro(File::Append)
	}

	if ( nread != input.length() ) 
	{
		Core::Exceptions::CannotWriteFileException e("File::Append");
		e.Append("\nFile was: ");
		e.Append(this->m_FileNameFullPath.c_str());
		throw e;
	}
}

//!
void File::Write(const std::string& input)
{
	FILE* fp;
	size_t nread = 0;
	if(this->m_FileNameFullPath.length() > 0)
	{
		try
		{
			fp = fopen(this->m_FileNameFullPath.c_str(), "w");
			nread = fwrite(input.c_str(), sizeof(char), input.length(), fp);
			if(fp != NULL) fclose(fp);
		}
		coreCatchExceptionsAddTraceAndThrowMacro(File::Write)
	}
	if ( nread != input.length() ) 
	{
		Core::Exceptions::CannotWriteFileException e("File::Write");
		e.Append("\nFile was: ");
		e.Append(this->m_FileNameFullPath.c_str());
		throw e;
	}
}

//!
void File::Delete(void)
{
	try
	{
		remove(this->m_FileNameFullPath.c_str());
	}
	coreCatchExceptionsAddTraceAndThrowMacro(File::Delete);
}

/**
 */
std::string Core::IO::File::GetFileName() const
{
	return m_FileName;
}

/**
 */
std::string Core::IO::File::GetPathToFile() const
{
	return m_PathToFile;
}

/**
 */
std::string Core::IO::File::GetFileNameFullPath() const
{
	return m_FileNameFullPath;
}


void Core::IO::File::CheckDataFilenames( 
	const std::string &filename, std::vector<std::string> &inputFileNames )
{
	bool fileExists = true;
	int count = 0;
	
	fileExists = itksys::SystemTools::FileExists( filename.c_str() );
	if ( fileExists )
	{
		inputFileNames.push_back( filename );
	}
	else
	{

		// Try to read file using filename00.vtk, filename01.vtk, ...
		fileExists = true;
		while ( fileExists )
		{

			std::string path = itksys::SystemTools::GetFilenamePath( filename.c_str( ) );;
			std::string fileName = itksys::SystemTools::GetFilenameWithoutExtension( filename.c_str( ) );
			std::string ext = itksys::SystemTools::GetFilenameExtension( filename.c_str( ) );
			std::stringstream sstream;
			sstream << path << "/" << fileName << std::setfill('0') << std::setw(2) << count << ext;
			fileExists = itksys::SystemTools::FileExists( sstream.str().c_str() );
			if( fileExists )
			{
				inputFileNames.push_back(sstream.str().c_str());
			}
			count++;
		}
	}
	
	// For remote filenames
	if ( inputFileNames.empty( ) )
	{
		inputFileNames.push_back( filename );
	}

}
