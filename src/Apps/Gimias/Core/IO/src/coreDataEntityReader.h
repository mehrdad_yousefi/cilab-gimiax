/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreDataEntityReader_H
#define coreDataEntityReader_H

#include "coreObject.h"
#include "coreDataEntity.h"
#include "coreBaseDataEntityReader.h"
#include "coreBaseProcessor.h"

#include <string>
#include <map>
#include <list>

namespace Core 
{
namespace IO 
{

/** 
\brief Reader of data entities of any type.

The DataEntityReader will register, at runtime, any new format 
provided by a plugin developer.
This high level manager will be singleton, referenced from the Kernel instance.
Registering a format will require storing a FormatDescriptor object into 
the DataEntity.

When the user selects a DataEntity and selects File->Load, the Core will 
search over a list of registered readers, to find a suitable one that 
can read the requested file. The first reader that is able to do this is 
called to read the data and output a DataEntity.

Each plug-in is able to register readers and writers with the Core. 
When using a specialized reader, register it to the DataEntityReader
class as the Reader available for that format, so the application and other 
plugins can notice and benefit of this format reader.

DataEntityReader provides methods to check that the tuple 
<format type, data type> is uniquely identified in the list of registered 
formats.

\ingroup gmIO
\sa Core::Runtime::Kernel
\author Juan Antonio Moya
\date 11 Feb 2007
*/

class GMIO_EXPORT DataEntityReader : public Core::BaseProcessor
{
public:
	typedef std::map<std::string,BaseDataEntityReader::Pointer> RegisteredReadersType;

public:

	coreDeclareSmartPointerClassMacro(Core::IO::DataEntityReader, Core::BaseProcessor);	

	//!
	void SetFileName( const std::string &filename );

	//!
	void SetFileNames( const std::vector< std::string > &filenames );

	//! Get transfer type of passed filenames
	Core::IO::BaseIO::TRANSFER_TYPE GetTransferType( );

	//!
	bool ReadHeader( );

	//! 
	void Update();

	//! Register a reader
	static void RegisterFormatReader(BaseDataEntityReader::Pointer);

	//! UnRegister a reader
	static void UnRegisterFormatReader(BaseDataEntityReader::Pointer);

	//! Get a registered reader. Use the name of the class. For example "DICOMFileReader"
	static BaseDataEntityReader::Pointer GetRegisteredReader( const std::string &name);

	//!
	static void UnRegisterAllFormats( );

	//! Unregister all readers of a plugin
	static void UnRegisterPluginReaders( const std::string &pluginName );

	/**
	\brief Returns a filter wild card for the open file dialog, that 
	filters all the available reader data formats registered.
	*/
	static std::string GetFileFilterTypesForRead(void);

	//!
	blTagMap::Pointer GetTagMap() const;
	void SetTagMap(blTagMap::Pointer val);

	//! Read/Write multiple files in a single DataEntity
	bool GetUseMultipleTimeSteps() const;
	void SetUseMultipleTimeSteps(bool val);

	//!
	void SetFather( Core::DataEntity::Pointer father );

	//!
	void SetLightRead( bool val );

protected:
	//! 
	DataEntityReader(void);

	//! 
	virtual ~DataEntityReader(void);

	//! 
	static BaseIO::ValidExtensionsListType 
		GetRegisteredReaderFormats( Core::DataEntityType type);

	//!
	void ReadGroup( 
		const std::string name,
		const std::vector<std::string> &filenames );

	//!
	void OnUpdateCallback( SmartPointerObject* object );

private:
	coreDeclareNoCopyConstructors(DataEntityReader);

private:

	//!
	static RegisteredReadersType m_RegisteredReaders;

	//!
	std::vector< std::string > m_Filenames;

	//! Header information
	blTagMap::Pointer m_TagMap;

	//! Read/Write multiple files in a single DataEntity
	bool m_UseMultipleTimeSteps;

	//!
	Core::DataEntity::Pointer m_Father;

	//! Only read metadata and preview
	bool m_LightRead;
};

} // namespace IO
} // namespace Core

#endif
