/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreBaseIO_H
#define _coreBaseIO_H

#include "gmIOWin32Header.h"
#include "coreObject.h"
#include "coreDataEntity.h"
#include "coreBaseProcessor.h"

namespace Core
{
namespace IO
{


/** 
\brief The BaseIO class is the base class for BaseDataEntityReader and 
BaseDataEntityWriter

\ingroup gmIO
\author Xavi Planes
\date 13 May 2010
*/
class GMIO_EXPORT BaseIO : public Core::BaseProcessor
{
public:
	typedef std::list<std::string> ValidExtensionsListType;
	typedef std::list<DataEntityType> ValidTypesListType;

	enum TRANSFER_TYPE {
		LOCAL_TRANSFER_TYPE,
		REMOTE_TRANSFER_TYPE
	};

public:
	coreDeclareSmartPointerTypesMacro(Core::IO::BaseIO, Core::BaseProcessor);
	coreClassNameMacro(Core::IO::BaseIO);

	//! Get last digits before extension
	static long GetLastDigits( const std::string &filename );

	//! Compare two file paths using last digits
	static bool CompareStringNumbers( const std::string &str1,const std::string &str2 );

	/** Remove last digits and extension from file name
	\param [in] ext If ext is empty, call GetFilenameWithoutExtension else
	remove extension ext from file name
	*/
	static std::string RemoveExtensionAndLastDigits( 
		const std::string &filename, 
		const std::string &ext = "" );

	//!
	virtual void SetFileNames( const std::vector< std::string > &filenames );
	std::vector< std::string > GetFileNames( );

	//! Just for a single file
	virtual void SetFileName( const std::string &filename );
	std::string GetFileName( );

	//! Return the extensions that the reader can read, like ".vtk"
	virtual ValidExtensionsListType GetValidExtensions( );

	//! Return DataEntity types that the reader can read
	virtual ValidTypesListType GetValidDataEntityTypes( );

	/** Check if this IO can manage the DataEntityType.
	Uses operator & to compare
	*/
	virtual bool Check( Core::DataEntityType type );

	/** Use filename to match the last part with a valid extension
	If a valid extension is empty, check that filename doesn't have a '.'.
	Store the valid extension in the member variable m_Extension
	*/
	virtual bool Check( const std::string &filename );

	//!
	blTagMap::Pointer GetTagMap() const;
	void SetTagMap(blTagMap::Pointer val);

	//! Read/Write multiple files in a single DataEntity
	bool GetUseMultipleTimeSteps() const;
	void SetUseMultipleTimeSteps(bool val);

	//! Get m_Extension
	std::string GetExtension() const;
	void SetExtension( const std::string &ext);

	//!
	TRANSFER_TYPE GetTransferType( );

protected:
	BaseIO(void);;
	virtual ~BaseIO(void);

	coreDeclareNoCopyConstructors(BaseIO);

protected:
	//!
	std::vector< std::string > m_Filenames;
	//! Extension validated using Check()
	std::string m_Extension;
	//!
	ValidExtensionsListType m_ValidExtensionsList;
	//!
	ValidTypesListType m_ValidTypesList;
	//!
	blTagMap::Pointer m_TagMap;
	//! Read/Write multiple files in a single DataEntity
	bool m_UseMultipleTimeSteps;
	//! Local or remote transfer
	TRANSFER_TYPE m_TransferType;
};


}
}

#endif
