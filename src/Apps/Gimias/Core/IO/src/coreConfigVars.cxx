/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreConfigVars.h"

using namespace Core::IO;

ConfigVars::ConfigVars( )
{
	m_startedOnce = false;
	m_ImportConfiguration = false;
	m_profile = Core::Profile::New();
	m_profile->ResetProfileToDefault();
	m_pacsCalledAE= "SERVERAE:176.34.102.200:4006";
	m_pacsCallingAE= "GIMIAS:127.0.0.1:12350";
	m_Perspective = "Plugin";
	m_ActiveWorkflow = "";
	m_ShowRegistrationForm = true;
	m_UserRegistered = false;
	// Global configuration parameters for GIMIAS
	m_MapPluginConfiguration[ "GIMIAS" ] = ConfigVarsPlugin( );
}


Core::IO::ConfigVarsPlugin::ConfigVarsPlugin()
{
	m_Properties = blTagMap::New( );	
}

