/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDataEntityIORegistration.h"

#include "coreDataEntityReader.h"
#include "coreDataEntityWriter.h"

#include "coreNumericDataReader.h"
#include "coreNumericDataWriter.h"

Core::IO::DataEntityIORegistration::DataEntityIORegistration()
{
}

Core::IO::DataEntityIORegistration::~DataEntityIORegistration()
{
}

void Core::IO::DataEntityIORegistration::RegisterDefaultFormats( const std::string &resourcePath )
{
	DataEntityReader::RegisterFormatReader( BaseDataEntityReader::Pointer(NumericDataReader::New()) );
	DataEntityWriter::RegisterFormatWriter( BaseDataEntityWriter::Pointer(NumericDataWriter::New()) );
}
