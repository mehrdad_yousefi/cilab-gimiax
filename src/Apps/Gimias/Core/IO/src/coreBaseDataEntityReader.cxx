/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreBaseDataEntityReader.h"
#include "coreFile.h"

#include "blXMLTagMapReader.h"

Core::IO::BaseDataEntityReader::BaseDataEntityReader( void )
{
	m_LightRead = false;
	SetNumberOfOutputs( 1 );
}

Core::IO::BaseDataEntityReader::~BaseDataEntityReader( void )
{

}

bool Core::IO::BaseDataEntityReader::ReadHeader( )
{
	return false;
}

bool Core::IO::BaseDataEntityReader::CanRead( Core::DataEntityType type )
{
	return Check( type );
}

bool Core::IO::BaseDataEntityReader::CanRead( const std::string &filename )
{
	return Check( filename );
}

void Core::IO::BaseDataEntityReader::ReadAllTimeSteps( Core::DataEntityType type )
{
	if ( m_Filenames.size() == 0 )
	{
		return ;
	}

	// Read type from metadata and overwrite input parameter
	if ( GetNumberOfOutputs() && GetOutputDataEntity( 0 ).IsNotNull() )
	{
		int temp;
		blTag::Pointer tag;
		tag = GetOutputDataEntity( 0 )->GetMetadata()->FindTagByName( "Type" );
		if ( tag.IsNotNull() && tag->GetValue( temp ) && temp != UnknownTypeId )
		{
			type = Core::DataEntityType( temp );
		}
	}


	Core::DataEntity::Pointer dataEntity = NULL;

	// Sort 00, 01, 02, ...
	std::vector<std::string> fileNamesSorted;
	for ( unsigned i = 0 ; i < m_Filenames.size( ) ; i++ )
	{
		fileNamesSorted.push_back( m_Filenames[ i ].c_str( ) );
	}
	std::sort( fileNamesSorted.begin( ), fileNamesSorted.end( ), CompareStringNumbers );

	// Load a vector of vtkImageData
	std::vector< boost::any > processingDataVector;
	for ( unsigned iTimeStep = 0 ; iTimeStep < fileNamesSorted.size() ; iTimeStep++ )
	{
		boost::any processingData = 
			ReadSingleTimeStep( iTimeStep, fileNamesSorted[ iTimeStep ].c_str() );

		if ( !processingData.empty() )
		{
			processingDataVector.push_back( processingData );
		}
	}

	// Create the DataEntity
	if( processingDataVector.size() > 0 )
	{
		if ( m_UseMultipleTimeSteps )
		{
			// Create a single DataEntity
			GetOutputPort( 0 )->SetDataEntityType( type );
			UpdateOutput(0, processingDataVector, RemoveExtensionAndLastDigits(fileNamesSorted[ 0 ], GetExtension()), true );
		}
		else
		{
			// Create one DataEntity for each file
			SetNumberOfOutputs( int( processingDataVector.size() ) );
			for ( unsigned i = 0 ; i < GetNumberOfOutputs() ; i++ )
			{
				GetOutputPort( i )->SetDataEntityType( type );
				std::string name = Core::IO::File::GetFilenameName( fileNamesSorted[ i ] );
				name = itksys::SystemTools::GetFilenameWithoutExtension( name );
				UpdateOutput( i, processingDataVector[ i ], name, true );
				if ( GetOutputDataEntity( i ).IsNotNull() )
				{
					GetOutputDataEntity( i )->GetMetadata( )->AddTag( "FilePath", fileNamesSorted[ i ] );
					GetOutputDataEntity( i )->GetMetadata( )->AddTag( "Extension", GetExtension( ) );
				}
			}
		}
	}

	// Update time from metadata
	if ( GetOutputDataEntity( 0 ).IsNotNull( ) )
	{
		Core::DataEntityMetadata::Pointer metadata = GetOutputDataEntity( 0 )->GetMetadata( );
		blTag::Pointer timeTag = metadata->GetTag( "Time" );
		if ( timeTag.IsNotNull( ) )
		{
			// Separate items by ;
			std::string timeStr = timeTag->GetValueAsString( );
			std::list<std::string> timeList;
			blTextUtils::ParseLine( timeStr, ';', timeList );

			// Pass data to DataEntity
			std::list<std::string>::iterator itTime;
			int count = 0;
			for ( itTime = timeList.begin( ) ; itTime != timeList.end( ) ; itTime++ )
			{
				double time = atof( itTime->c_str( ) );
				if ( count < GetOutputDataEntity( 0 )->GetNumberOfTimeSteps( ) )
					GetOutputDataEntity( 0 )->SetTimeAtTimeStep( count, time );
				count++;
			}
		}
	}
}

boost::any Core::IO::BaseDataEntityReader::ReadSingleTimeStep( 
	int iTimeStep, const std::string &filename )
{
	throw Core::Exceptions::Exception( 
		"BaseDataEntityReader::ReadSingleTimeStep()", 
		"Function not implemented" );

	return NULL;
}

bool Core::IO::BaseDataEntityReader::ReadMetaData()
{
	if ( m_Filenames.empty() || GetNumberOfOutputs() == 0 )
	{
		return false;
	}

	// Add "gmi" extension GiMias Information
	std::string filename = BaseIO::RemoveExtensionAndLastDigits( m_Filenames[ 0 ], m_Extension );
	filename = itksys::SystemTools::GetFilenamePath( m_Filenames[ 0 ] ) + "/" + filename + ".gmi";
	// If new filename doesn't exist, use the old behavior that uses
	// first time step filename
	if ( !itksys::SystemTools::FileExists( filename.c_str() ) )
	{
		filename = itksys::SystemTools::GetFilenameWithoutExtension( m_Filenames[ 0 ] );
		filename = itksys::SystemTools::GetFilenamePath( m_Filenames[ 0 ] ) + "/" + filename + ".gmi";
	}

	try
	{
		blXMLTagMapReader::Pointer xmlTagMapReader = blXMLTagMapReader::New( );
		xmlTagMapReader->SetFilename( filename.c_str() );
		xmlTagMapReader->Update();
		blTagMap::Pointer tagMap = xmlTagMapReader->GetOutput();
		if ( tagMap.IsNull() )
		{
			return false;
		}

		Core::DataEntity::Pointer dataEntity = Core::DataEntity::New();
		Core::DataEntityMetadata::Pointer metadata = dataEntity->GetMetadata( );
		metadata->AddTags( tagMap );

		// Check "Type" tag
		blTag::Pointer tagType = tagMap->GetTag( "Type" );
		if ( tagType.IsNotNull( ) )
		{
			Core::DataEntityType type = Core::DataEntityType( tagType->GetValueCasted<int>( ) );
			dataEntity->SetType( type );
		}

		// Add FilePath before loading preview. Remove last digits
		std::string filename;
		// If data is session or is a single file, read file as it is
		if ( Check( Core::SessionTypeId ) || m_Filenames.size( ) == 1 )
		{
			filename = m_Filenames[ 0 ];
		}
		else
		{
			filename = RemoveExtensionAndLastDigits(m_Filenames[ 0 ], GetExtension());
			filename = itksys::SystemTools::GetFilenamePath( m_Filenames[ 0 ] ) + "/" + filename + GetExtension();
		}
		metadata->AddTag( "FilePath", filename );

		// Load preview
		dataEntity->GetPreview( )->Load( );

		SetOutputDataEntity( 0, dataEntity );
	}
	catch(...)
	{
		return false;
	}

	return true;
}

void Core::IO::BaseDataEntityReader::Update()
{
	ReadMetaData();

	if ( !m_LightRead )
	{
		ReadData( );
	}
}

void Core::IO::BaseDataEntityReader::ReadData()
{

}

void Core::IO::BaseDataEntityReader::SetLightRead( bool val )
{
	m_LightRead = val;
}

