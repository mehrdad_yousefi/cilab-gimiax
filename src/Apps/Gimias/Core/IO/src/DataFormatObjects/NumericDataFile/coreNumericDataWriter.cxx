// Copyright 2008 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "coreNumericDataWriter.h"
#include "coreDataEntity.h"
#include "coreException.h"

#include "blNumericDataWriter.h"
#include "blXMLTagMapWriter.h"

using namespace Core::IO;

//!
NumericDataWriter::NumericDataWriter(void) 
{
	m_ValidExtensionsList.push_back( ".csv" );
	m_ValidExtensionsList.push_back( ".xml" );
	m_ValidTypesList.push_back( NumericDataTypeId );
}

//!
NumericDataWriter::~NumericDataWriter(void)
{
}

//!
void NumericDataWriter::WriteData( )
{
	WriteAllTimeSteps( );
}

void Core::IO::NumericDataWriter::WriteSingleTimeStep( 
	const std::string& fileName, 
	Core::DataEntity::Pointer dataEntity, 
	int iTimeStep )
{
	blTagMap::Pointer tagMap = NULL;
	bool worked = dataEntity->GetProcessingData( tagMap, iTimeStep );
	if( !worked || tagMap == NULL )
	{
		throw Core::Exceptions::Exception(
			"NumericDataWriterWriter::WriteSingleTimeStep",
			"Input data is not of the correct type" );
	}

	if ( fileName.find( ".csv" ) != std::string::npos )
	{
		blNumericDataWriter::Pointer writer = blNumericDataWriter::New( );
		writer->SetFilename( fileName.c_str() );
		writer->SetInput( tagMap );
		writer->Update();
	}
	else if ( fileName.find( ".xml" ) != std::string::npos )
	{
		blXMLTagMapWriter::Pointer writer = blXMLTagMapWriter::New( );
		writer->SetFilename( fileName.c_str() );
		writer->SetInput( tagMap );
		writer->Update();
	}
}


