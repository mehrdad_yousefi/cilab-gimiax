/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreBaseDataEntityWriter.h"
#include "blTextUtils.h"
#include "coreException.h"

#include "blXMLTagMapWriter.h"

Core::IO::BaseDataEntityWriter::BaseDataEntityWriter( void ) 
{
	SetNumberOfInputs( 1 );

	// Write all time steps
	GetInputPort( 0 )->SetUpdateMode( BaseFilterInputPort::UPDATE_ACCESS_MULTIPLE_TIME_STEP );
}

Core::IO::BaseDataEntityWriter::~BaseDataEntityWriter( void )
{

}

void Core::IO::BaseDataEntityWriter::WriteAllTimeSteps( )
{
	if( GetInputDataEntity( 0 ).IsNull() )
	{
		throw Core::Exceptions::Exception( 
			"BaseDataEntityWriter::WriteSingleTimeStep()", 
			"Input data entity is NULL" );
	}

	//write the image
	switch ( GetInputPort( 0 )->GetUpdateMode() )
	{
	case BaseFilterInputPort::UPDATE_ACCESS_SINGLE_TIME_STEP:
		{
			int timeStep = GetInputPort( 0 )->GetSelectedTimeStep();
			std::string strFileName = GetTimeStepFilename( timeStep );
			m_WrittenFilenames.push_back( strFileName );
			WriteSingleTimeStep( strFileName, GetInputDataEntity( 0 ), timeStep );
		}
	break;
	case BaseFilterInputPort::UPDATE_ACCESS_MULTIPLE_TIME_STEP:
		{
			for ( size_t iTimeStep = 0 ; iTimeStep < GetInputDataNumberOfTimeSteps( 0 ) ; iTimeStep++ )
			{
				std::string strFileName = GetTimeStepFilename( int( iTimeStep ) );
				m_WrittenFilenames.push_back( strFileName );
				WriteSingleTimeStep( strFileName, GetInputDataEntity( 0 ), int( iTimeStep ) );
			}
		}
	break;
	}

}

void Core::IO::BaseDataEntityWriter::WriteSingleTimeStep( 
	const std::string& fileName, 
	Core::DataEntity::Pointer dataEntity,
	int iTimeStep )
{

	throw Core::Exceptions::Exception(
		"Core::IO::BaseDataEntityWriter::WriteSingleTimeStep",
		"Function not implemented");
}

bool Core::IO::BaseDataEntityWriter::CanWrite( Core::DataEntityType type )
{
	return Check( type );
}

bool Core::IO::BaseDataEntityWriter::CanWrite( std::string filename )
{
	return Check( filename );
}

bool Core::IO::BaseDataEntityWriter::WriteMetaData()
{
	if ( m_Filenames.empty() || GetNumberOfInputs() == 0 || GetInputDataEntity( 0 ).IsNull() )
	{
		return false;
	}

	// Add "gmi" extension GiMias Information
	std::string strFileName = GetTimeStepFilename( 0 );
	std::string filePath = itksys::SystemTools::GetFilenamePath( m_Filenames[ 0 ] );
	std::string filename = BaseIO::RemoveExtensionAndLastDigits( strFileName, m_Extension );
	filename = filePath + "/" + filename + ".gmi";

	// Add "FilePath"
	GetInputDataEntity( 0 )->GetMetadata( )->AddTag( "FilePath", strFileName );
	GetInputDataEntity( 0 )->GetMetadata( )->AddTag( "Extension", GetExtension( ) );

	Core::DataEntityMetadata::Pointer metadata = GetInputDataEntity( 0 )->GetMetadata( );

	// Time
	if ( GetInputDataEntity( 0 )->GetNumberOfTimeSteps( ) )
	{
		// Create string separated by ;
		std::stringstream stream;
		for ( int i = 0 ; i < GetInputDataEntity( 0 )->GetNumberOfTimeSteps( ) ; i++ )
		{
			stream << GetInputDataEntity( 0 )->GetTimeAtTimeStep( i );
			if ( i != GetInputDataEntity( 0 )->GetNumberOfTimeSteps( ) - 1 )
				stream << ";";
		}
		metadata->AddTag( "Time", stream.str( ) );
	}

	try
	{
		blXMLTagMapWriter::Pointer writer = blXMLTagMapWriter::New();
		writer->SetFilename( filename.c_str() );
		writer->SetInput( metadata.GetPointer( ) );
		writer->Update();
	}
	catch(...)
	{
		return false;
	}

	// Save preview using FilePath
	GetInputDataEntity( 0 )->GetPreview( )->Save( );

	return true;
}

void Core::IO::BaseDataEntityWriter::WriteData()
{

}

void Core::IO::BaseDataEntityWriter::Update()
{
	std::string path = itksys::SystemTools::GetFilenamePath( m_Filenames[ 0 ].c_str( ) );
	itksys::SystemTools::MakeDirectory( path.c_str( ) );
	WriteData( );
	WriteMetaData( );
}

std::string Core::IO::BaseDataEntityWriter::GetTimeStepFilename( int iTimeStep )
{
	std::string strFileName;
	// If there's more than one image -> Create new name for each one
	if ( GetInputDataEntity( 0 )->GetNumberOfTimeSteps( ) > 1 )
	{
		if ( m_Filenames.size() == 1 )
		{
			strFileName = AddFileNumber( m_Filenames[ 0 ], iTimeStep );
		}
		else
		{
			strFileName = m_Filenames.at( iTimeStep );
		}
	}
	else
	{
		strFileName = m_Filenames[ 0 ];
	}

	return strFileName;
}

std::vector< std::string > Core::IO::BaseDataEntityWriter::GetWrittenFilenames() const
{
	return m_WrittenFilenames;
}
