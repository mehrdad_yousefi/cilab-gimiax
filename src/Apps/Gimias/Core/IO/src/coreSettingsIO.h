/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreSettingsIO_H
#define coreSettingsIO_H

#include "coreObject.h"
#include "coreConfigVars.h"
#include <string>


namespace Core
{
// forward declarations
namespace Runtime { class Settings; }

namespace IO
{

/** 
\brief Store and retrieve configuration data in cross platform way. 
It provides XML access to the settings file of the application.

\note Note that its methods are only accessible from the Settings manager. 
You shall not use directly this class.

\ingroup gmIO
\sa Core::Runtime::Settings
\author Juan Antonio Moya
\date 18 Apr 2007
*/
class GMIO_EXPORT SettingsIO : public Core::SmartPointerObject
{
public:
	coreDeclareSmartPointerClassMacro( Core::IO::SettingsIO, Core::SmartPointerObject);

	/**
	Check if the config file already exists, if don't, just create an 
	empty file for that user
	*/
	void TestCreateConfigFile(
		const std::string& projectHomePath, 
		const std::string& configFileFullPath);

	//! Read config file
	void ReadConfigFromFile(
		const std::string& configFileFullPath, 
		ConfigVars& configVars);

	//! Write config file
	void WriteConfigToFile(
		const std::string& configFileFullPath, 
		const std::string& dtdFileFullPath, 
		const std::string& projectName, 
		ConfigVars& configVars);

private:
	SettingsIO( );

	~SettingsIO( );

	coreDeclareNoCopyConstructors( SettingsIO );


	//! Return "true" or "false"
	std::string Bool2String( bool bValue );

	//! "true" or "false" to bool
	bool String2Bool( std::string bValue );


private:

};

} // namespace Core
} // namespace IO


#endif
