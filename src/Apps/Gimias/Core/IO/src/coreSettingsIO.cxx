/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreAssert.h"
#include "coreProfile.h"
#include "coreException.h"
#include "coreSettingsIO.h"
#include "coreDirectory.h"
#include "coreFile.h"
#include <stdio.h>
#include "tinyxml.h"
#include "boost/spirit/home/support/utf8.hpp"
#include "blXMLTagMapWriter.h"
#include "blXMLTagMapReader.h"

#include <iostream>
#include <fstream>

#include "utf8.h"

using namespace Core::IO;

const char* TAG_STARTED_ONCE = "startedOnce";
const char* TAG_PROFILE = "profile";
const char* TAG_PROFILE_ITEM = "profileItem";
const char* TAG_LAST_OPENED_PATH = "lastOpenedPath";
const char* TAG_LAST_SAVE_PATH = "lastSavePath";
const char* TAG_LAST_OPENED_DICOMDIR_PATH = "lastOpenedDicomDirPath";
const char* TAG_LAST_OPENED_DICOMFILE_PATH = "lastOpenedDicomFilePath";
const char* TAG_MRU_LIST = "MRUList";
const char* TAG_MRU_LIST_ITEM = "MRUListItem";
const char* TAG_PLUGIN_CONFIG = "PluginConfiguration";
const char* TAG_PLUGIN = "Plugin";
const char* TAG_PLUGIN_NAME = "PluginName";
const char* TAG_PLUGIN_LAYOUT = "PluginLayout";
const char* TAG_PACS_CALLEDAE = "Pacs Called AE";
const char* TAG_PACS_CALLINGAE = "Pacs Calling AE";
const char* TAG_PERSPECTIVE = "Perspective";
const char* TAG_ACTIVE_WORKFLOW = "Active Workflow";
const char* TAG_STARTED_IMPORT_CONFIGURATION = "importConfigurationWizard";
const char* TAG_PLUGINS_SCAN_FOLDERS_ITEM = "ScanFolder";
const char* TAG_SHOW_REGISTRATION_FORM = "ShowRegistrationForm";
const char* TAG_USER_REGISTERED = "UserRegistered";

const char* TAG_TRUE = "true";
const char* TAG_FALSE = "false";
const char* TAG_NAME = "name";
const char* TAG_VARIABLE = "variable";


void ReadStringVector( 
	TiXmlElement* pElem,
	std::vector<std::string> &strVector )
{
	if ( !pElem )
	{
		return;
	}

	strVector.clear();
	pElem = pElem->FirstChildElement( );
	for( pElem; pElem; pElem=pElem->NextSiblingElement())
	{
		if ( pElem->Type( ) == TiXmlNode::TINYXML_ELEMENT && 
			 pElem->GetText() )
		{
			strVector.push_back( pElem->GetText() );
		}
	}
}

void WriteStringVector( 
	TiXmlElement* pElem,
	const char* tagName,
	const char* subtagName,
	const std::vector<std::string> &strVector )
{
	TiXmlElement *mainElem = new TiXmlElement( tagName );
	pElem->LinkEndChild( mainElem );

	for( size_t i = 0 ; i < strVector.size() ; i++ )
	{
		TiXmlElement *subtagElem = new TiXmlElement( subtagName );
		subtagElem->LinkEndChild( new TiXmlText( strVector[ i ] ) );
		mainElem->LinkEndChild( subtagElem );
	}
}


/** 
*/
void SettingsIO::TestCreateConfigFile(const std::string& projectHomePath, const std::string& configFileFullPath)
{
	Directory::Pointer homeDir;
	File::Pointer file;
	std::string defaultProfile;
	std::string defaultInput;

	// Test if exists folder + create it if don't
	homeDir = Directory::New();
	homeDir->SetDirNameFullPath(projectHomePath);
	homeDir->Create();

	// Test if config file exists + create if don't
	file = File::New();
	file->SetFileNameFullPath(configFileFullPath);
	if(!file->Exists())
	{
		// Set the default contents for the config file
		defaultInput = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		defaultInput += "<!DOCTYPE GIMIAS_Configuration PUBLIC \"GIMIAS_Config_file\" \"config.dtd\">\n";
		defaultInput += "<GIMIAS_Configuration>\n";
		defaultInput += "  <variable name=\"startedOnce\">false</variable>\n";
		defaultInput += "  <profile>\n";
		defaultInput += "    <profileItem>" + defaultProfile + "</profileItem>\n";
		defaultInput += "  </profile>\n";
		defaultInput += "</GIMIAS_Configuration>";
		file->Create();
		file->Append(defaultInput);
	}

}

//!
void SettingsIO::ReadConfigFromFile(
	const std::string& configFileFullPath, ConfigVars& configVars)
{

	// Convert UTF-16 to UTF-8
	try
	{
		std::ifstream ifs( configFileFullPath.c_str( ), std::ifstream::binary );
		if (!ifs)
		{
			return ;
		}

		std::stringstream ss;
		ss << ifs.rdbuf() << '\0';
		if ( ss.str().find( "encoding=\"UTF-8\"" ) == std::string::npos )
		{
			std::wstring u16string((wchar_t *)ss.str().c_str());

			if ( utf8::is_valid( u16string.begin(), u16string.end() ) )
			{
				std::string u8string;
				utf8::utf16to8(u16string.begin(), u16string.end(), back_inserter(u8string));

				blTextUtils::StrSub( u8string, "encoding=\"UTF-16\"", "encoding=\"UTF-8\"" );

				std::ofstream ofs( configFileFullPath.c_str(), std::ofstream::trunc );
				ofs << u8string;
			}
		}

	}
	catch (...)
	{
		// If it's not a UTF16 file -> Is a ASCII file
	}


	// Read XML
	TiXmlDocument doc( configFileFullPath );
	if (!doc.LoadFile()) 
	{
		throw Core::Exceptions::CannotRetrieveSettingsException( "ReadConfigFromFile" );
	}

	TiXmlHandle hDoc(&doc);
	TiXmlHandle hRoot(hDoc.FirstChildElement().Element());

	std::string version = "1.0";
	if ( doc.FirstChild() && doc.FirstChild()->ToDeclaration( ) )
	{
		version = doc.FirstChild()->ToDeclaration( )->Version();
	}

	TiXmlElement* pElem = hRoot.FirstChildElement( ).Element();
	for( pElem; pElem; pElem=pElem->NextSiblingElement())
	{
		if ( pElem->Type( ) == TiXmlNode::TINYXML_ELEMENT && 
			strcmp( pElem->Value( ), TAG_VARIABLE ) == 0 &&
			pElem->GetText() )
		{
			std::string name = pElem->Attribute( "name" );
			if ( name == TAG_STARTED_ONCE )
			{
				configVars.m_startedOnce = String2Bool( pElem->GetText() );
			}
			else if ( name == TAG_STARTED_IMPORT_CONFIGURATION )
			{
				configVars.m_ImportConfiguration = String2Bool( pElem->GetText() );
			}
			else if ( name == TAG_LAST_OPENED_PATH )
			{
				configVars.m_lastOpenedPath = pElem->GetText();
			}
			else if ( name == TAG_LAST_OPENED_DICOMFILE_PATH )
			{
				configVars.m_lastOpenedDicomFilePath = pElem->GetText();
			}
			else if ( name == TAG_LAST_SAVE_PATH )
			{
				configVars.m_lastSavePath = pElem->GetText();
			}
			else if ( name == TAG_PACS_CALLEDAE )
			{
				configVars.m_pacsCalledAE = pElem->GetText();
			}
			else if ( name == TAG_PACS_CALLINGAE )
			{
				configVars.m_pacsCallingAE = pElem->GetText();
			}
			else if ( name == TAG_PERSPECTIVE )
			{
				configVars.m_Perspective = pElem->GetText();
			}
			else if ( name == TAG_ACTIVE_WORKFLOW )
			{
				configVars.m_ActiveWorkflow = pElem->GetText();
			}
			else if ( name == TAG_SHOW_REGISTRATION_FORM )
			{
				configVars.m_ShowRegistrationForm = String2Bool( pElem->GetText() );
			}
			else if ( name == TAG_USER_REGISTERED )
			{
				configVars.m_UserRegistered = String2Bool( pElem->GetText() );
			}
		}
		else if ( pElem->Type( ) == TiXmlNode::TINYXML_ELEMENT && 
			strcmp( pElem->Value( ), TAG_PROFILE ) == 0 )
		{
			// Clear the profile
			if(configVars.m_profile.IsNull())
			{
				configVars.m_profile = Core::Profile::New();
			}
			configVars.m_profile->ClearProfile();
			std::vector<std::string> strVector;
			ReadStringVector( pElem, strVector );
			for ( unsigned i = 0 ; i < strVector.size() ; i++ )
			{
				configVars.m_profile->AddToProfile( strVector[ i ] );
			}
		}
		else if ( pElem->Type( ) == TiXmlNode::TINYXML_ELEMENT && 
			strcmp( pElem->Value( ), TAG_MRU_LIST ) == 0 )
		{
			ReadStringVector( pElem, configVars.m_MRUList );
		}
		else if ( pElem->Type( ) == TiXmlNode::TINYXML_ELEMENT && 
			strcmp( pElem->Value( ), TAG_PLUGIN_CONFIG ) == 0 )
		{
			TiXmlElement* pPluginElem = pElem->FirstChildElement( );
			for( pPluginElem; pPluginElem; pPluginElem=pPluginElem->NextSiblingElement())
			{
				ConfigVarsPlugin pluginConfig;
				std::string pluginName;

				if ( version == "1.0" )
				{
					pluginName = pPluginElem->FirstChildElement( TAG_PLUGIN_NAME )->GetText( );
					pluginConfig.m_Properties->AddTag( "name", pluginName );

					// Properties
					TiXmlElement* pluginChildElem = pPluginElem->FirstChildElement();
					for( pluginChildElem; pluginChildElem; pluginChildElem=pluginChildElem->NextSiblingElement())
					{
						if ( pluginChildElem->Type( ) == TiXmlNode::TINYXML_ELEMENT && 
							strcmp( pluginChildElem->Value( ), TAG_PLUGIN_LAYOUT ) == 0 &&
							pluginChildElem->GetText() )
						{
							std::string tagValue = pluginChildElem->GetText();
							pluginConfig.m_Properties->AddTag( "layout", tagValue );
						}
						else if ( pluginChildElem->Type( ) == TiXmlNode::TINYXML_ELEMENT && 
							strcmp( pluginChildElem->Value( ), TAG_VARIABLE ) == 0 &&
							pluginChildElem->GetText() )
						{
							std::string tagName = pluginChildElem->Attribute( "name" );
							std::string tagValue = pluginChildElem->GetText();
							pluginConfig.m_Properties->AddTag( tagName, tagValue );
						}

					}
				}
				else if ( version == "2.0" )
				{
					try
					{
						blXMLTagMapReader::LoadData( pPluginElem, pluginConfig.m_Properties, "Properties" );
					}
					catch(...)
					{
					}

					if ( pluginConfig.m_Properties->GetTag( "name" ).IsNotNull() )
					{
						pluginName = pluginConfig.m_Properties->GetTag( "name" )->GetValueAsString();
					}
				}

				// Add/Overwrite the config
				configVars.m_MapPluginConfiguration[pluginName] = pluginConfig;
			}
		}
	}
}

void SettingsIO::WriteConfigToFile(
						const std::string& configFileFullPath, 
						const std::string& dtdFileFullPath, 
						const std::string& projectName, 
						ConfigVars& configVars)
{

	TiXmlDocument doc;
	TiXmlDeclaration * decl = new TiXmlDeclaration( "2.0", "UTF-8", "yes" );
	doc.LinkEndChild( decl );

	std::string qualifiedName = projectName + "_Configuration";
	TiXmlElement *rootElem = new TiXmlElement( qualifiedName );
	doc.LinkEndChild( rootElem );

	TiXmlElement *variableElem;
	variableElem = new TiXmlElement( TAG_VARIABLE );
	variableElem->LinkEndChild( new TiXmlText( Bool2String( configVars.m_startedOnce ) ) );
	variableElem->SetAttribute( "name", TAG_STARTED_ONCE );
	rootElem->LinkEndChild( variableElem );

	variableElem = new TiXmlElement( TAG_VARIABLE );
	variableElem->LinkEndChild( new TiXmlText( Bool2String( configVars.m_ImportConfiguration ) ) );
	variableElem->SetAttribute( "name", TAG_STARTED_IMPORT_CONFIGURATION );
	rootElem->LinkEndChild( variableElem );

	variableElem = new TiXmlElement( TAG_VARIABLE );
	variableElem->LinkEndChild( new TiXmlText( configVars.m_lastOpenedPath ) );
	variableElem->SetAttribute( "name", TAG_LAST_OPENED_PATH );
	rootElem->LinkEndChild( variableElem );

	variableElem = new TiXmlElement( TAG_VARIABLE );
	variableElem->LinkEndChild( new TiXmlText( configVars.m_lastOpenedDicomDirPath ) );
	variableElem->SetAttribute( "name", TAG_LAST_OPENED_DICOMDIR_PATH );
	rootElem->LinkEndChild( variableElem );

	variableElem = new TiXmlElement( TAG_VARIABLE );
	variableElem->LinkEndChild( new TiXmlText( configVars.m_lastOpenedDicomFilePath ) );
	variableElem->SetAttribute( "name", TAG_LAST_OPENED_DICOMFILE_PATH );
	rootElem->LinkEndChild( variableElem );

	variableElem = new TiXmlElement( TAG_VARIABLE );
	variableElem->LinkEndChild( new TiXmlText( configVars.m_lastSavePath ) );
	variableElem->SetAttribute( "name", TAG_LAST_SAVE_PATH );
	rootElem->LinkEndChild( variableElem );

	variableElem = new TiXmlElement( TAG_VARIABLE );
	variableElem->LinkEndChild( new TiXmlText( configVars.m_pacsCalledAE ) );
	variableElem->SetAttribute( "name", TAG_PACS_CALLEDAE );
	rootElem->LinkEndChild( variableElem );

	variableElem = new TiXmlElement( TAG_VARIABLE );
	variableElem->LinkEndChild( new TiXmlText( configVars.m_pacsCallingAE ) );
	variableElem->SetAttribute( "name", TAG_PACS_CALLINGAE );
	rootElem->LinkEndChild( variableElem );

	variableElem = new TiXmlElement( TAG_VARIABLE );
	variableElem->LinkEndChild( new TiXmlText( configVars.m_Perspective ) );
	variableElem->SetAttribute( "name", TAG_PERSPECTIVE );
	rootElem->LinkEndChild( variableElem );

	variableElem = new TiXmlElement( TAG_VARIABLE );
	variableElem->LinkEndChild( new TiXmlText( configVars.m_ActiveWorkflow ) );
	variableElem->SetAttribute( "name", TAG_ACTIVE_WORKFLOW );
	rootElem->LinkEndChild( variableElem );

	variableElem = new TiXmlElement( TAG_VARIABLE );
	variableElem->LinkEndChild( new TiXmlText( Bool2String( configVars.m_ShowRegistrationForm ) ) );
	variableElem->SetAttribute( "name", TAG_SHOW_REGISTRATION_FORM );
	rootElem->LinkEndChild( variableElem );

	variableElem = new TiXmlElement( TAG_VARIABLE );
	variableElem->LinkEndChild( new TiXmlText( Bool2String( configVars.m_UserRegistered ) ) );
	variableElem->SetAttribute( "name", TAG_USER_REGISTERED );
	rootElem->LinkEndChild( variableElem );

	WriteStringVector( rootElem, TAG_MRU_LIST, TAG_MRU_LIST_ITEM, configVars.m_MRUList );

	variableElem = new TiXmlElement( TAG_PLUGIN_CONFIG );
	rootElem->LinkEndChild( variableElem );

	// Write plugin configuration
	Core::IO::ConfigVars::PluginMapType::iterator it;
	it = configVars.m_MapPluginConfiguration.begin();
	while ( it != configVars.m_MapPluginConfiguration.end() )
	{
		// Element of the plugin
		TiXmlElement *pluginElem = new TiXmlElement( TAG_PLUGIN );
		variableElem->LinkEndChild( pluginElem );

		blXMLTagMapWriter::SaveData( pluginElem, it->second.m_Properties, "Properties" );

		it++;
	}


	doc.SaveFile( configFileFullPath );
}


Core::IO::SettingsIO::SettingsIO()
{
}

Core::IO::SettingsIO::~SettingsIO()
{
}


std::string Core::IO::SettingsIO::Bool2String( bool bValue )
{
	
	std::string strValue;
	if ( bValue )
	{
		strValue = TAG_TRUE;
	}
	else
	{
		strValue = TAG_FALSE;
	}
	
	return strValue;
}

bool Core::IO::SettingsIO::String2Bool( std::string bValue )
{
	if ( bValue.compare( TAG_TRUE ) == 0 )
	{
		return true;
	}
	else if ( bValue.compare( TAG_FALSE ) == 0 )
	{
		return false;
	}

	return false;
}
