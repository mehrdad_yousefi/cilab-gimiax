/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreFile_H
#define coreFile_H

#include "gmIOWin32Header.h"
#include "coreObject.h"
#include <string>

namespace Core
{
namespace IO
{
/**
\brief The File class hides the complexity of working with files, using a 
platform independent approach.

\sa Core::IO::Directory
\ingroup gmIO
\author Juan Antonio Moya
\date 21 Feb 2007
*/
class GMIO_EXPORT File : public Core::SmartPointerObject
{
public:
	coreDeclareSmartPointerClassMacro(Core::IO::File, Core::SmartPointerObject)
			
	void SetFileName(const std::string& path, const std::string& fileName);
	void SetFileNameFullPath(const std::string& fullPath);
	
	bool Exists(void);
	void Create(void);
	void Append(const std::string& input);
	void Write(const std::string& input);
	void Delete(void);

	std::string GetFileName() const;
	std::string GetPathToFile() const;
	std::string GetFileNameFullPath() const;

	static std::string GetFilenameWithoutExtension(const std::string& fileName);
	static std::string GetFilenameName(const std::string& fileName);


	/**
	\brief If input filename exists in disk return the input filename, otherwise check the 
	if 3D+T version exists:
	image.vtk -> image00.vtk, image01.vtk, ...

	\param [in] filename Input filename
	\param [out] fileNames Generated 3D+T filenames
	*/
	static void CheckDataFilenames( 
		const std::string &filename, std::vector<std::string> &inputFileNames );


protected:
	File(void);
	~File(void);

private:
	std::string m_FileName;
	std::string m_PathToFile;
	std::string m_FileNameFullPath;
};
}
}

#endif

