/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreDataEntityIORegistration_H
#define coreDataEntityIORegistration_H

#include "gmIOWin32Header.h"
#include "coreObject.h"

namespace Core{

namespace IO{

/** 
DataEntityIORegistration registers available Readers and writers


\ingroup gmIO
\author Xavi Planes
\date 08 Jun 2009
*/
class GMIO_EXPORT DataEntityIORegistration : public Core::Object
{
public:
	//!
	static void RegisterDefaultFormats( const std::string &resourcePath );

protected:
	//! 
	DataEntityIORegistration();
	//! 
	~DataEntityIORegistration();

};

} // namespace IO{
} // namespace Core{

#endif
