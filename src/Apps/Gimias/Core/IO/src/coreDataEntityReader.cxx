/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDataEntityReader.h"
#include "coreDataEntityInfoHelper.h"
#include "coreScanFolder.h"
#include "coreFile.h"
#include "coreStringHelper.h"

#include <itksys/SystemTools.hxx>

Core::IO::DataEntityReader::RegisteredReadersType Core::IO::DataEntityReader::m_RegisteredReaders;

//!
Core::IO::DataEntityReader::DataEntityReader()
{
	SetNumberOfOutputs( 0 );
	SetName( "Reader" );
	m_TagMap = blTagMap::New( );
	m_UseMultipleTimeSteps = true;
	m_LightRead = false;
}

//!
Core::IO::DataEntityReader::~DataEntityReader(void)
{
}

//!
Core::IO::BaseIO::ValidExtensionsListType
Core::IO::DataEntityReader::GetRegisteredReaderFormats(Core::DataEntityType type)
{
	BaseIO::ValidExtensionsListType formatList;
	RegisteredReadersType::iterator it;
	for(it = m_RegisteredReaders.begin(); it != m_RegisteredReaders.end(); ++it)
	{
		if ( it->second->CanRead( type ) )
		{
			BaseIO::ValidExtensionsListType extensions;
			extensions = it->second->GetValidExtensions();
			formatList.insert(formatList.end(), extensions.begin(), extensions.end());
		}
	}

	return formatList;
}

void Core::IO::DataEntityReader::SetFileName( const std::string &filename )
{
	// On windows, network paths gives an error when calling CollapseFullPath( )
	// Avoid calling collapse full path for remote files
	std::string fullPathFilename;
	if ( filename.find( "\\\\" ) == std::string::npos &&
		 filename.find( "://" ) == std::string::npos )
	{
		fullPathFilename = itksys::SystemTools::CollapseFullPath( filename.c_str() );
	}
	else
	{
		fullPathFilename = filename;
	}
	m_Filenames.clear();
	m_Filenames.push_back( fullPathFilename );
}

void Core::IO::DataEntityReader::SetFileNames( const std::vector< std::string > &filenames )
{
	m_Filenames.clear();
	for ( size_t i = 0 ; i < filenames.size() ; i++ )
	{
		std::string fullPathFilename;
		// On windows, network paths gives an error when calling CollapseFullPath( )
		// Avoid calling collapse full path for remote files
		// Example: //storage2.cistib.upf.edu/data/file.vtk
		if ( filenames[ i ].find( "\\\\" ) == std::string::npos &&
			 filenames[ i ].find( "://" ) == std::string::npos &&
			 !( filenames[ i ].find( "//" ) == 0 ) )
		{
			fullPathFilename = itksys::SystemTools::CollapseFullPath( filenames[ i ].c_str() );
		}
		else
		{
			fullPathFilename = filenames[ i ];
		}
		m_Filenames.push_back( fullPathFilename );
	}
}

bool  Core::IO::DataEntityReader::ReadHeader()
{
	if ( m_Filenames.empty() )
	{
		return false;
	}

	blTagMap::Pointer tagMap;
	bool correctlyRead = false;
	std::string fileName0 = m_Filenames[ 0 ];


	// For each reader, check if it can read
	RegisteredReadersType::iterator it = m_RegisteredReaders.begin();
	while( !correctlyRead  && it != m_RegisteredReaders.end() )
	{
		if ( it->second->CanRead( fileName0 ) )
		{
			// Found a reader, instantiate it and try read header
			BaseDataEntityReader::Pointer reader;
			reader = static_cast<Core::IO::BaseDataEntityReader*>(it->second->CreateAnother().GetPointer());

			reader->SetFileNames( m_Filenames );
			if ( reader->ReadHeader( ) )
			{
				m_TagMap = reader->GetTagMap();
				// Infer the extension from the filename
				m_TagMap->AddTag( "Extension", it->second->GetExtension() );
				correctlyRead = true;
			}
		}
		++it;
	}
		
	return correctlyRead;
}

void Core::IO::DataEntityReader::Update()
{
	Core::StringHelper::setLocale();

	// Try to read the complete folder using DICOM reader
	if ( !m_Filenames.empty() && 
		 itksys::SystemTools::FileIsDirectory( m_Filenames[ 0 ].c_str() ) )
	{
		ReadGroup( "", m_Filenames );

		// If read is ok, return
		if ( GetNumberOfOutputs() > 0 )
		{
			return;
		}
	}

	// Check if any file is 3D+T, and add the corresponding 3D+T version
	// This is needed for compatibility with CLP when reading 3D+T
	std::vector< std::string > processedFilenames;
	std::vector< std::string >::iterator itNames;
	for ( itNames = m_Filenames.begin( ) ; itNames != m_Filenames.end( ) ; itNames++ )
	{
		Core::IO::File::CheckDataFilenames( *itNames, processedFilenames );
	}
	m_Filenames = processedFilenames;

	// Else try to read the grouped filenames
	ScanFolder::Pointer scanFolder = ScanFolder::New( );
	scanFolder->SetFileNames( m_Filenames );
	scanFolder->Update();

	ScanFolder::GroupMapType groups = scanFolder->GetGroups();
	ScanFolder::GroupMapType::iterator itGroup;
	for ( itGroup  = groups.begin() ; itGroup != groups.end() ; itGroup++ )
	{
		ReadGroup( itGroup->first, itGroup->second.GetFilenames( ) );
	}

	// Throw exception if 0 outputs
	if ( GetNumberOfOutputs() == 0 && m_Filenames.size() )
	{
		Core::Exceptions::CannotOpenFileException e("DataEntityReader::Update");
		e.Append("File was: ");
		e.Append(m_Filenames[0].c_str());
		throw e;
	}

	// Throw exception output is NULL
	for ( size_t i = 0 ; i < GetNumberOfOutputs() ; i++ )
	{
		if ( GetOutputDataEntity( int( i ) ).IsNull() )
		{
			Core::Exceptions::CannotOpenFileException e("DataEntityReader::Update");
			e.Append("File was: ");
			e.Append(m_Filenames[0].c_str());
			throw e;
		}
	}

	Core::StringHelper::restoreLocale();
}


std::string Core::IO::DataEntityReader::GetFileFilterTypesForRead(void) 
{
	Core::IO::DataEntityReader::Pointer dataEntityReader;
	dataEntityReader = Core::IO::DataEntityReader::New( );
	std::ostringstream filter;
	filter << "All files(*.*)|*.*";
	bool addedToFilter = false;

	// For each entity type
	Core::DataEntityInfoHelper::EntityList entityList = Core::DataEntityInfoHelper::GetAvailableEntityTypes();
	Core::DataEntityInfoHelper::EntityList::iterator itType;
	for( itType = entityList.begin();itType != entityList.end();++itType)
	{
		BaseIO::ValidExtensionsListType formatList;
		BaseIO::ValidExtensionsListType::iterator formatListIt;

		formatList = dataEntityReader->GetRegisteredReaderFormats( (*itType) );
		formatListIt = formatList.begin();
		// Append the Entity type string
		addedToFilter = formatListIt != formatList.end();
		if( addedToFilter )
		{
			filter << "|" << Core::DataEntityInfoHelper::GetEntityTypeAsString((*itType)) << "(";
		}

		// Append the types to the string
		while(formatListIt != formatList.end())
		{
			filter << "*" << (*formatListIt);
			++formatListIt;
			if(formatListIt != formatList.end())
				filter << ", ";
		}
		if(addedToFilter)
			filter << ")|";

		// Append now the wildcards
		for(formatListIt = formatList.begin(); formatListIt != formatList.end() && addedToFilter; ++formatListIt)
		{
			filter << "*" << (*formatListIt);
			if(formatListIt != formatList.end())
				filter << ";";
		}
	}

	// return the cumulated filter
	return filter.str();
}

void Core::IO::DataEntityReader::UnRegisterAllFormats()
{
	m_RegisteredReaders.clear();
}


void Core::IO::DataEntityReader::RegisterFormatReader(
	BaseDataEntityReader::Pointer reader)
{
	m_RegisteredReaders[ reader->GetNameOfClass() ] = reader;
}

void Core::IO::DataEntityReader::UnRegisterFormatReader(
	BaseDataEntityReader::Pointer reader)
{
	RegisteredReadersType::iterator it = m_RegisteredReaders.begin();
	bool found = false;
	while ( it != m_RegisteredReaders.end() && !found )
	{
		if ( it->second.GetPointer() == reader.GetPointer() )
		{
			found = true;
		}
		else
		{
			++it;
		}
	}

	if (found) 
	{
		m_RegisteredReaders.erase( it );
	}
}

blTagMap::Pointer Core::IO::DataEntityReader::GetTagMap() const
{
	return m_TagMap;
}

void Core::IO::DataEntityReader::SetTagMap( blTagMap::Pointer val )
{
	m_TagMap = val;
}

void Core::IO::DataEntityReader::ReadGroup( 
	const std::string name,
	const std::vector<std::string> &filenames )
{
	// Check input filenames is not empty
	if ( filenames.size() == 0 )
	{
		return;
	}

	// For each reader, check if it can read
	std::string fileName0 = filenames[ 0 ];
	bool correctlyRead = false;
	RegisteredReadersType::iterator it;
	for( it = m_RegisteredReaders.begin() ; 
		 it != m_RegisteredReaders.end() && !correctlyRead;
		 it++)
	{
		// Check if reader can read this file
		if ( !it->second->CanRead( fileName0 ) )
		{
			continue;
		}

		// Found a reader, instantiate it and try read
		// add observer for update callback
		BaseDataEntityReader::Pointer reader;
		reader = static_cast<Core::IO::BaseDataEntityReader*>(it->second->CreateAnother().GetPointer());
		reader->SetExtension( it->second->GetExtension( ) );
		reader->SetFileNames( filenames );
		reader->SetTagMap( m_TagMap );
		reader->SetUseMultipleTimeSteps( m_UseMultipleTimeSteps );
		reader->SetLightRead( m_LightRead );
		// Pass parameters to specific reader
		reader->GetProperties( )->AddTags( GetProperties( ) );
		reader->GetUpdateCallback()->AddObserver1( 
			this, &DataEntityReader::OnUpdateCallback );

		// Try to read
		try
		{
			reader->Update( );
		}
		catch(...){}

		// Check at least one reader output
		if ( reader->GetNumberOfOutputs() == 0 ||
			 reader->GetOutputDataEntity(0).IsNull() )
		{
			continue;
		}

		// Check output has processing data
		Core::DataEntity::Pointer dataEntity = reader->GetOutputDataEntity(0);
		if ( dataEntity->GetNumberOfTimeSteps() == 0 && !m_LightRead )
		{
			continue;
		}

		// Check valid type when reading metadata. 
		// XML extension can be read by two readers and session has different update function
		if ( !reader->CanRead( dataEntity->GetType( ) ) )
		{
			continue;
		}

		// Process output data
		correctlyRead = true;
		size_t previousNumOutputs = GetNumberOfOutputs( );
		SetNumberOfOutputs( previousNumOutputs + reader->GetNumberOfOutputs() );
		for ( size_t i = 0 ; i < reader->GetNumberOfOutputs() ; i++ )
		{
			Core::DataEntity::Pointer dataEntity = reader->GetOutputDataEntity( int( i ) );

			// If dataEntity has no name
			if ( dataEntity->GetMetadata( )->GetName( ).empty( ) ||
					dataEntity->GetMetadata( )->GetName( ) == "Unnamed" )
			{
				std::string dataEntityName;
				if ( filenames.size() > 1 )
				{
					dataEntityName = BaseIO::RemoveExtensionAndLastDigits( name );
				}
				else
				{
					dataEntityName = BaseIO::RemoveExtensionAndLastDigits( fileName0 );
				}
						
				dataEntity->GetMetadata( )->SetName( dataEntityName );
			}

			// Add "FilePath"
			if ( dataEntity->GetMetadata( )->GetTag( "FilePath" ).IsNull() )
				dataEntity->GetMetadata( )->AddTag( "FilePath", fileName0 );

			// Add extension
			if ( dataEntity->GetMetadata( )->GetTag( "Extension" ).IsNull() )
				dataEntity->GetMetadata( )->AddTag( "Extension", reader->GetExtension( ) );

			if ( dataEntity->GetFather( ).IsNull( ) )
			{
				dataEntity->SetFather( m_Father );
			}

			UpdateOutput( int( previousNumOutputs + i ), dataEntity );
			correctlyRead = true;
		}

	}
}

Core::IO::BaseDataEntityReader::Pointer 
Core::IO::DataEntityReader::GetRegisteredReader( const std::string &name )
{
	RegisteredReadersType::iterator it = m_RegisteredReaders.find( name );
	if ( it == m_RegisteredReaders.end() )
	{
		return NULL;
	}	

	return it->second;
}

void Core::IO::DataEntityReader::OnUpdateCallback( SmartPointerObject* object )
{
	UpdateCallback* updateCallback = static_cast<UpdateCallback*> ( object );

	GetUpdateCallback()->AddExceptionMessage( updateCallback->GetExceptionMessage() );
	GetUpdateCallback()->SetProgress( updateCallback->GetProgress() );
	GetUpdateCallback()->SetStatusMessage( updateCallback->GetStatusMessage() );
	GetUpdateCallback()->AddInformationMessage( updateCallback->GetInformationMessage() );
	GetUpdateCallback()->Modified( );

	updateCallback->SetAbortProcessing( GetUpdateCallback()->GetAbortProcessing() );
}

bool Core::IO::DataEntityReader::GetUseMultipleTimeSteps() const
{
	return m_UseMultipleTimeSteps;
}

void Core::IO::DataEntityReader::SetUseMultipleTimeSteps( bool val )
{
	m_UseMultipleTimeSteps = val;
}

Core::IO::BaseIO::TRANSFER_TYPE Core::IO::DataEntityReader::GetTransferType( )
{
	Core::IO::BaseIO::TRANSFER_TYPE transferType;
	transferType = Core::IO::BaseIO::LOCAL_TRANSFER_TYPE;
	if ( m_Filenames.empty() )
	{
		return transferType;
	}

	std::string fileName0 = Core::StringHelper::ToLowerCase( m_Filenames[ 0 ] );

	// For each reader, check if a remote transfer can read it
	RegisteredReadersType::iterator it = m_RegisteredReaders.begin();
	while( it != m_RegisteredReaders.end() )
	{
		if ( it->second->CanRead( fileName0 ) && 
			 it->second->GetTransferType( ) == Core::IO::BaseIO::REMOTE_TRANSFER_TYPE )
		{
			transferType = it->second->GetTransferType( );
		}

		it++;
	}


	return transferType;
}


void Core::IO::DataEntityReader::SetFather( Core::DataEntity::Pointer father )
{
	m_Father = father;
}

void Core::IO::DataEntityReader::SetLightRead( bool val )
{
	m_LightRead = val;
}

void Core::IO::DataEntityReader::UnRegisterPluginReaders( const std::string &pluginName )
{
	// Get readers matching plugin name
	RegisteredReadersType::iterator itRegReader;
	std::list<BaseDataEntityReader::Pointer> readersToUnregister;
	for ( itRegReader = m_RegisteredReaders.begin( ) ; itRegReader != m_RegisteredReaders.end( ) ; itRegReader++ )
	{
		blTag::Pointer pluginNameTag = itRegReader->second->GetProperties( )->GetTag( "PluginName" );
		if ( pluginNameTag.IsNull( ) )
			continue;

		if ( pluginNameTag->GetValueAsString( ) == pluginName )
		{
			readersToUnregister.push_back( itRegReader->second );
		}
	}

	// Unregister them
	std::list<BaseDataEntityReader::Pointer>::iterator itReader;
	for ( itReader = readersToUnregister.begin( ) ; itReader != readersToUnregister.end( ) ; itReader++ )
	{
		UnRegisterFormatReader( *itReader );
	}
}
