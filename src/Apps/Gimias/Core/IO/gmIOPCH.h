/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "assert.h"
#include "blTextUtils.h"
#include "coreAssert.h"
#include "coreBaseFilter.h"
#include "coreDataEntity.h"
#include "coreDataEntityInfoHelper.h"
#include "coreException.h"
#include "coreObject.h"
#include "coreStringHelper.h"
#include <itkLightObject.h>
#include <itkSmartPointer.h>
#include <itksys/Glob.hxx>
#include <itksys/SystemTools.hxx>
#include <list>
#include <map>
#include <set>
#include <stdio.h>
#include <string>
#include <string.h>
#include <vector>
