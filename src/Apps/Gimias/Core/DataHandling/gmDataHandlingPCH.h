/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "CILabAssertMacros.h"
#include "CILabExceptionMacros.h"
#include "CILabNamespaceMacros.h"
#include "coreAssert.h"
#include "coreCreateExceptionMacros.h"
#include "coreDataHolder.h"
#include "coreException.h"
#include "coreObject.h"
#include <boost/any.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/signals.hpp>
#include <list>
#include <map>
#include <sstream>
#include <string>
#include <typeinfo>
#include <vector>
