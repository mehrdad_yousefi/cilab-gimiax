/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreDataEntityImplFactory_H
#define _coreDataEntityImplFactory_H

#include "gmDataHandlingWin32Header.h"
#include "coreObject.h"
#include <string>
#include <typeinfo>
#include <vector>
#include <boost/any.hpp>
#include "blTagMap.h"
#include "coreDataEntityImpl.h"
#include "coreDataEntityTypes.h"
#include "coreBaseFactory.h"
#include "coreFactoryManager.h"

namespace Core{

/**
Helper class to facilitate finding DataEntityImpl factories

\author Xavi Planes
\date 07 sept 2010
\ingroup gmDataHandling
*/
class GMDATAHANDLING_EXPORT DataEntityImplFactoryHelper : public Core::Object
{
public:
	coreClassNameMacro(Core::DataEntityImplFactoryHelper)

	//! Find a factory that matches exact data type
	static BaseFactory::Pointer FindFactoryByType( const std::string &datatypename );

	//! Find a factory using similar type name like "vtkPolyData"
	static BaseFactory::Pointer FindFactoryBySimilarType( const std::string &name );

};

} // end namespace Core

#define coreDefineDataFactoryAddTypeInfo( type1 ) \
		if ( m_Properties->FindTagByName( "DataType" ).IsNull( ) ) \
		{ \
			m_Properties->AddTag( "DataType", blTagMap::New( ) ); \
		} \
		GetPropertyValue<blTagMap::Pointer>( "DataType" )->AddTag( typeid( type1 ).name( ), true );  \

#define coreDefineSingleDataFactory( DataEntityImplType, type1, dataEntityType ) \
	coreDefineFactoryClass( DataEntityImplType ) \
	coreDefineFactoryTagsBegin() \
	coreDefineDataFactoryAddTypeInfo( type1 ) \
	coreDefineFactoryAddTag( "single", true ) \
	coreDefineFactoryAddTag( "DefaultDataEntityType", dataEntityType ) \
	coreDefineFactoryTagsEnd( ) \
	coreDefineFactoryClassEnd( )

#define coreDefineMultipleDataFactory( DataEntityImplType, type1, dataEntityType ) \
	coreDefineFactoryClass( DataEntityImplType ) \
	coreDefineFactoryTagsBegin() \
	coreDefineDataFactoryAddTypeInfo( type1 ) \
	coreDefineFactoryAddTag( "multiple", true ) \
	coreDefineFactoryAddTag( "DefaultDataEntityType", dataEntityType ) \
	coreDefineFactoryTagsEnd( ) \
	coreDefineFactoryClassEnd( )

#define coreDefineSingleDataFactory2Types( DataEntityImplType, type1, type2, dataEntityType ) \
	coreDefineFactoryClass( DataEntityImplType ) \
	coreDefineFactoryTagsBegin() \
	coreDefineDataFactoryAddTypeInfo( type1 ) \
	coreDefineDataFactoryAddTypeInfo( type2 ) \
	coreDefineFactoryAddTag( "single", true ) \
	coreDefineFactoryAddTag( "DefaultDataEntityType", dataEntityType ) \
	coreDefineFactoryTagsEnd( ) \
	coreDefineFactoryClassEnd( )

#define coreDefineMultipleDataFactory2Types( DataEntityImplType, type1, type2, dataEntityType ) \
	coreDefineFactoryClass( DataEntityImplType ) \
	coreDefineFactoryTagsBegin() \
	coreDefineDataFactoryAddTypeInfo( type1 ) \
	coreDefineDataFactoryAddTypeInfo( type2 ) \
	coreDefineFactoryAddTag( "multiple", true ) \
	coreDefineFactoryAddTag( "DefaultDataEntityType", dataEntityType ) \
	coreDefineFactoryTagsEnd() \
	coreDefineFactoryClassEnd( )

#endif // _coreDataEntityImplFactory_H

