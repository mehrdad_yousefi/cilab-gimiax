/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreMultipleDataEntityImpl.h"
#include "coreDataEntityImplFactory.h"

using namespace Core;

/**
*/
Core::MultipleDataEntityImpl::MultipleDataEntityImpl( ) 
{
}

Core::MultipleDataEntityImpl::~MultipleDataEntityImpl()
{

}

boost::any Core::MultipleDataEntityImpl::GetDataPtr() const
{
	return m_Data;
}

void Core::MultipleDataEntityImpl::SetData( 
	blTagMap::Pointer tagMap, ImportMemoryManagementType mem/* = gmCopyMemory*/ )
{
	blTag::Pointer tagDataEntitySetImplSet = SafeFindTag( tagMap, "DataEntityImplSet" );
	DataEntityImplSetType setOfDataEntitySetImpl;
	setOfDataEntitySetImpl = tagDataEntitySetImplSet->GetValueCasted<DataEntityImplSetType>();

	ResetData( );

	DataEntityImplSetType::iterator it;
	for ( it = setOfDataEntitySetImpl.begin() ; it != setOfDataEntitySetImpl.end() ; it++)
	{
		// Create a new DataEntityImpl
		BaseFactory::Pointer factory;

		// If no factory defined, use the one of the input data
		if ( m_Factory.IsNull() )
		{
			factory = DataEntityImplFactoryHelper::FindFactoryByType( (*it)->GetDataPtr().type().name() );
		}
		else
		{
			factory = m_Factory;
		}
		DataEntityImpl::Pointer dataEntityImpl;
		dataEntityImpl = DataEntityImpl::SafeDownCast( factory->CreateInstance() );
		// Copy data
		dataEntityImpl->DeepCopy( *it, mem );

		// Add
		SetAt( GetSize(), dataEntityImpl, mem );
	}
}

void Core::MultipleDataEntityImpl::GetData( blTagMap::Pointer tagMap )
{
	tagMap->AddTag( "DataEntityImplSet", m_Data );
}

void Core::MultipleDataEntityImpl::ResetData()
{
	m_Data.clear();
}

void Core::MultipleDataEntityImpl::SwitchImplementation( const std::string &datatypename )
{
	m_Factory = DataEntityImplFactoryHelper::FindFactoryBySimilarType( datatypename );
	DataEntityImplSetType::iterator it;
	for ( it = m_Data.begin() ; it != m_Data.end() ; it++)
	{
		if ( !(*it)->IsValidType( datatypename ) )
		{
			DataEntityImpl::Pointer newData;
			newData = DataEntityImpl::SafeDownCast( m_Factory->CreateInstance() );

			if ( (*it).IsNotNull() )
			{
				newData->DeepCopy( (*it) );
			}

			(*it) = newData;
		}
	}
}

size_t Core::MultipleDataEntityImpl::GetSize() const
{
	return m_Data.size();
}

DataEntityImpl::Pointer Core::MultipleDataEntityImpl::GetAt( size_t pos )
{
	return m_Data.at( pos );
}

void Core::MultipleDataEntityImpl::SetAt( 
	size_t pos, 
	DataEntityImpl::Pointer data,
	ImportMemoryManagementType mem /*= gmCopyMemory*/ )
{
	if ( m_Data.size() )
	{
		const std::type_info &type1 = typeid( *data.GetPointer() );
		const std::type_info &type2 = typeid( *m_Data[ 0 ].GetPointer() );
		if ( type1 != type2 )
		{
			throw Exceptions::DataTypeNotValidException( 
				"MultipleDataEntityImpl::SetAt" );
		}
	}

	if ( pos >= m_Data.size( ) )
	{
		m_Data.resize( pos + 1 );
	}
	m_Data.at( pos ) = data;
}

void Core::MultipleDataEntityImpl::SetAnyData( boost::any val )
{
	m_Data = boost::any_cast<DataEntityImplSetType> ( val );
}

std::string Core::MultipleDataEntityImpl::GetMemoryOwner( )
{
	if ( m_Data.empty( ) )
	{
		return "Unknown";
	}

	return (*m_Data.begin( ))->GetMemoryOwner( );
}
