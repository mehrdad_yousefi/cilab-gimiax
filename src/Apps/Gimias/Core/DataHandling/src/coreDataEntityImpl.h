/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreDataEntityImpl_H
#define _coreDataEntityImpl_H

#include "gmDataHandlingWin32Header.h"
#include "coreObject.h"
#include <string>
#include <vector>
#include <boost/any.hpp>
#include "blTagMap.h"
#include "coreException.h"
#include "coreBaseExceptions.h"


namespace Core{

//! Type used for real time measurements, expressed in seconds.
typedef double RealTime;

enum ImportMemoryManagementType 
{ 
	//! Copy the source memory to destination memory (like image buffer)
	gmCopyMemory, 
	//! Reuse the source data buffer and free it when necessary
	gmManageMemory, 
	//! Reuse the source data buffer and never free it
	gmReferenceMemory
};

/**
Stores the processing data at a certain time step. 
It is mutable (so that one client can change the Processing 
Data instance of another client).

To copy data from one instance to another, it uses the functions SetData
 and GetData. A blTagMap is used to transfer the data, using a predefined
 set of tags. For example, for a surface mesh, the tags are:
- "Points": std::vector<Point3D>*
- "SurfaceElements": std::vector<SurfaceElement3D>*

\author Xavi Planes
\date 07 sept 2010
\ingroup gmDataHandling
*/
class GMDATAHANDLING_EXPORT DataEntityImpl : public Core::SmartPointerObject
{
public:
	coreDeclareSmartPointerTypesMacro(Core::DataEntityImpl,Core::SmartPointerObject) 
	coreClassNameMacro(Core::DataEntityImpl)
	coreTypeMacro( Core::DataEntityImpl,Core::SmartPointerObject) 

	enum TimeTypes { CONSTANT_IN_TIME = -1 };

public:
	typedef std::vector<double> Point3D;
	typedef std::vector<int> SurfaceElement3D;

	typedef std::map<std::string, DataEntityImpl::Pointer > DataEntityImplMapType;

	typedef std::vector<DataEntityImpl::Pointer> DataEntityImplSetType;

public:
	//@{ 
	/// \name Interface for Multiple data

	//!
	virtual DataEntityImpl::Pointer GetAt( size_t num );

	/** Set a data pointer at position pos (for multiple data)
	\throw DataTypeNotValidException if data type is not valid
	*/
	virtual void SetAt( 
		size_t pos, 
		DataEntityImpl::Pointer data, 
		ImportMemoryManagementType mem = gmCopyMemory );

	//!
	virtual size_t GetSize() const;

	//!
	virtual void SetSize(size_t size, DataEntityImpl::Pointer data = NULL );

	//@}

	/** Set internal pointer
	\throw DataTypeNotValidException if data type is not valid
	*/
	virtual void SetDataPtr( boost::any val );

	//! Get internal pointer
	virtual boost::any GetDataPtr() const = 0;

	//!
	virtual void* GetVoidPtr( ) const;

	//!
	virtual void SetVoidPtr( void* );

	//! Remove all internal data
	virtual void ResetData( ) = 0;

	//! Return true if val is valid type to call SetDataPtr( )
	virtual bool IsValidType( const std::string &datatypename );

	/** Create a DataEntityImpl and call DeepCopy( )
	\throw FactoryNotFoundException
	*/
	void DeepCopy( boost::any val, ImportMemoryManagementType mem = gmCopyMemory );

	/** Copy all the data. First, will try to copy it calling GetData( ) and
	SetData( ). If this fails, it will try with m_GenerateTemporalData set to true.
	If mem is gmReferenceMemory, but this option is not available, it will 
	use gmCopyMemory.
	*/
	void DeepCopy( DataEntityImpl::Pointer input, ImportMemoryManagementType mem = gmCopyMemory );

	//! Returns true if the processing data has no real time point, but is constant in time.
	bool IsConstantInTime() const;

	//!
	Core::RealTime GetRealTime() const;
	void SetRealTime(Core::RealTime val);

	//!
	bool GetGenerateTemporalData() const;
	void SetGenerateTemporalData(bool val);

	//!
	virtual void CleanTemporalData( );

	/** Fill internal data pointer with data. This function should not
	create a new data pointer because some other observer can depend on it.

	- If gmCopyMemory is used, it will copy all contents, like for example 
	an image buffer
	- If gmReferenceMemory is used, it will try to set a reference to the data,
	like an image buffer pointer

	\throw NotImplementedException
	*/
	virtual void SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem = gmCopyMemory );

	/** Get all the data using pointers and filling blTagMap. 

	If m_GenerateTemporalData is true, it can create temporal data that 
	will be freed calling CleanTemporalData( ). For example, for VtkPolyDataImpl.
	This temporal data is needed when the internal data representation
	is very different from the interface tags.

	\throw NotImplementedException
	*/
	virtual void GetData( blTagMap::Pointer tagMap );

	/**Find a tag 
	\throw TagNotFoundException
	*/
	static blTag::Pointer SafeFindTag( blTagMap::Pointer tagMap, const std::string &name );

	/** Get the name of the plugin that owns the memory allocated when calling SetData( ).
	
	This function will be used when unloading plugins. The memory allocated
	by a plugin needs to be dealocated before unloading it.

	- When SetData() copies all the memory, the owner is the plugin that 
	registered this class. For example Core::VtkUnstructuredGridImpl::SetData()
	- When SetData() gets the pointer to an instance, the owner is unknown.
	For example Core::ITKTransformImpl::SetData()
	*/
	std::string GetMemoryOwner( );

	//!
	void SetMemoryOwner( std::string val );

private:

	//! Cast any data and replace current internal data pointer
	virtual void SetAnyData( boost::any val ) = 0;

protected:
	//!
	DataEntityImpl( );

protected:
	//! The time associated with this data
	RealTime m_RealTime;

	//! Map of factories
	static DataEntityImplMapType m_DataEntityImplMap;

	//! Generate temporal data when calling GetData( )
	bool m_GenerateTemporalData;

	//!
	std::string m_MemoryOwner;
};

} // end namespace Core

#endif // _coreDataEntityImpl_H

