/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDataEntityImplFactory.h"
#include "coreFactoryManager.h"

using namespace Core;


BaseFactory::Pointer 
Core::DataEntityImplFactoryHelper::FindFactoryByType( const std::string &datatypename )
{
	blTagMap::Pointer tagMap = blTagMap::New( );
	blTagMap::Pointer tagMapTypes = blTagMap::New( );
	tagMapTypes->AddTag( datatypename, true );
	tagMap->AddTag( "DataType", tagMapTypes );
	BaseFactory::Pointer factory = FactoryManager::Find( DataEntityImpl::GetNameClass(), tagMap );
	return factory;
}

BaseFactory::Pointer 
Core::DataEntityImplFactoryHelper::FindFactoryBySimilarType( const std::string &name )
{
	BaseFactory::Pointer factory;
	FactoryManager::FactoryListType factories;
	factories = FactoryManager::FindAll( DataEntityImpl::GetNameClass() );
	FactoryManager::FactoryListType::iterator it = factories.begin( );
	while( it != factories.end() && factory.IsNull() )
	{
		blTagMap::Pointer validTypes = (*it)->GetPropertyValue<blTagMap::Pointer>( "DataType" );
		blTagMap::Iterator itType = validTypes->GetIteratorBegin( );
		while ( itType != validTypes->GetIteratorEnd() && factory.IsNull() )
		{
			std::string datatypename = itType->second->GetName( );
			if ( datatypename.find( name ) != std::string::npos )
			{
				factory = (*it);
			}
			itType++;
		}
		it++;
	}

	return factory;
}
