/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef COREDATAENTITYHOLDER_H
#define COREDATAENTITYHOLDER_H

#include "gmDataHandlingWin32Header.h"
#include "coreDataEntity.h"
#include "coreDataHolder.h"

namespace Core{

class DataEntityHolderConnection;

class GMDATAHANDLING_EXPORT DataEntityHolder : public DataHolder<DataEntity::Pointer>
{
public:
	//!
	coreDeclareSmartPointerClassMacro(Core::DataEntityHolder, DataHolder<DataEntity::Pointer>);

	//!
	void AddConnection( DataEntityHolderConnection* connection );

	//!
	void RemoveConnection( DataEntityHolderConnection* connection );

	//!
	DataEntityHolderConnection* FindConnection( DataEntityHolder* holder );

	//!
	void DeleteAllConnections( );

	//!
	virtual void SetSubject( const DataEntity::Pointer& data, bool bForceNotification = false );
	
private:
	//!
	DataEntityHolder( );

	virtual ~DataEntityHolder( );

	//!
	virtual bool doSetSubject( const DataEntity::Pointer& data, bool bForceNotification = false );
private:
	//!
	std::list<DataEntityHolderConnection*> m_connectionList;

	//!
	bool m_NotifyingObservers;
};

//! Creates a new data holder in \a dataHolder and sets its subject to p
void GMDATAHANDLING_EXPORT Initialize( DataEntityHolder::Pointer& dataHolder, DataEntity::Pointer p );

} // Core

#endif //COREDATAENTITYHOLDER_H
