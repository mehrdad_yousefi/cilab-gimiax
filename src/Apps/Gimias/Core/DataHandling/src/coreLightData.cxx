/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreLightData.h"

using namespace Core;

LightData::LightData()
{
}

LightData::~LightData()
{
	for(size_t i=0; i<m_childrenList.size(); i++)
	{
		if(m_childrenList.at(i) != NULL)
			delete m_childrenList.at(i);
	}
}

void LightData::AddChild(LightData* child)
{
	m_childrenList.push_back(child);	
}
