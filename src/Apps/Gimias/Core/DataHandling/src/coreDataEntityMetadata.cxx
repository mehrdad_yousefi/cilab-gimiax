/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDataEntityMetadata.h"
#include <cstdio>

Core::DataEntityMetadata::DataEntityMetadata( unsigned int dataEntityId )
{
	m_DataEntityId = dataEntityId;

	AddTag( DataEntityTag::New( "Modality", int( Core::UnknownModality ) ) );
	AddTag( DataEntityTag::New( "Name", std::string("") ) );

	blTagMap::Pointer patientData = blTagMap::New( );
	patientData->AddTag( DataEntityTag::New( "Patient name", std::string("") ) );
	patientData->AddTag( DataEntityTag::New( "Patient sex", std::string("") ) );
	patientData->AddTag( DataEntityTag::New( "Patient birth date", std::string("") ) );
	AddTag( "Patient", patientData );

	AddTag( DataEntityTag::New( "ED flag", std::string("") ) );
	AddTag( DataEntityTag::New( "ES flag", std::string("") ) );
}

Core::DataEntityMetadata::~DataEntityMetadata(void)
{
}

void Core::DataEntityMetadata::SetName( const std::string& name )
{
	AddTag( DataEntityTag::New( "Name", name ) );
}

std::string Core::DataEntityMetadata::GetName()
{
	std::string name;
	DataEntityTag::Pointer tag = GetTag( "Name" );
	if ( tag.IsNotNull( ) )
	{
		tag->GetValue<std::string>(name);
	}
	return name;
}

void Core::DataEntityMetadata::SetModality( ModalityType modality )
{
	AddTag( DataEntityTag::New( "Modality", int( modality ) ) );
}

Core::ModalityType Core::DataEntityMetadata::GetModality()
{
	Core::ModalityType modality = UnknownModality;
	DataEntityTag::Pointer tag = GetTag( "Modality" );
	if ( tag.IsNotNull( ) )
	{
		int temp;
		tag->GetValue<int>(temp);
		modality = Core::ModalityType( temp );
	}
	return modality;
}

unsigned int Core::DataEntityMetadata::GetDataEntityId() const
{
	return m_DataEntityId;
}

std::string Core::DataEntityMetadata::GetEDTimeStep() 
{
	std::string  ts;
	DataEntityTag::Pointer tag = GetTag( "ED flag" );
	if ( tag.IsNotNull( ) )
	{
		tag->GetValue<std::string>(ts);
	}
	return ts;
}


void Core::DataEntityMetadata::SetEDTimeStep(size_t timeStep) 
{
	char edTimeStep[10] = "";
	if ( timeStep != -1 )
	{
		sprintf (edTimeStep, "%i", timeStep);
	}
	AddTag( DataEntityTag::New( "ED flag", std::string( edTimeStep ) ) );
}


std::string Core::DataEntityMetadata::GetESTimeStep() 
{
	std::string ts;
	DataEntityTag::Pointer tag = GetTag( "ES flag" );
	if ( tag.IsNotNull( ) )
	{
		tag->GetValue<std::string>(ts);
	}
	return ts;
}


void Core::DataEntityMetadata::SetESTimeStep(size_t timeStep) 
{

	char esTimeStep[10] = "";
	if ( timeStep != -1 )
	{
		sprintf (esTimeStep, "%i", timeStep);
	}
	AddTag( DataEntityTag::New( "ES flag", std::string( esTimeStep ) ) );
}

