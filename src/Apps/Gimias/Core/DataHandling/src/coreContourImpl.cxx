/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreContourImpl.h"

Core::ContourImpl::ContourImpl( )
{
	ResetData();
}

Core::ContourImpl::~ContourImpl()
{

}

boost::any Core::ContourImpl::GetDataPtr() const
{
	return m_Data;
}

void Core::ContourImpl::ResetData()
{
	m_Data.clear();
}

void Core::ContourImpl::SetAnyData( boost::any val )
{
	m_Data = boost::any_cast<DataType> ( val );
}

std::string Core::ContourImpl::GetMemoryOwner( )
{
	return "Unknown";
}

