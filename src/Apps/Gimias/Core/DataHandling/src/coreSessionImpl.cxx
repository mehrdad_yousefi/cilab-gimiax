// Copyright 2007 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "coreSessionImpl.h"



Core::SessionImpl::SessionImpl( )
{
	ResetData( );
}

Core::SessionImpl::~SessionImpl()
{

}

void Core::SessionImpl::SetAnyData( boost::any val )
{
	m_Data = boost::any_cast<DataType> ( val );
}

boost::any Core::SessionImpl::GetDataPtr() const
{
	return m_Data;
}

void Core::SessionImpl::ResetData()
{
	m_Data = blTagMap::New( );
}

void Core::SessionImpl::SetData( 
	blTagMap::Pointer tagMap, ImportMemoryManagementType mem/* = gmCopyMemory*/ )
{
	bool success = false;
	blTag::Pointer tagDataPtr = tagMap->FindTagByName( "DataPtr" );
	if ( tagDataPtr.IsNotNull() )
	{
		DataType srcData;
		if ( tagDataPtr->GetValue<DataType>( srcData ) )
		{
			m_Data->RemoveAll( );
			m_Data->AddTags( srcData );
			success = true;
		}
	}

	// Throw exception
	if ( !success )
	{
		DataEntityImpl::SetData( tagMap, mem );
	}
}

void Core::SessionImpl::GetData( blTagMap::Pointer tagMap )
{
	tagMap->AddTag( "DataPtr", m_Data ); 
}

std::string Core::SessionImpl::GetMemoryOwner( )
{
	return "Unknown";
}

