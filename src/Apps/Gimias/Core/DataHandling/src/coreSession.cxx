/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreSession.h"

using namespace Core;

Session::Session()
{
}

Session::~Session()
{
	for(size_t i=0; i<m_dataList.size(); i++)
	{
		if(m_dataList.at(i) != NULL)
			delete m_dataList.at(i);
	}
}

void Session::AddData(LightData* data)
{
	m_dataList.push_back(data);
}
