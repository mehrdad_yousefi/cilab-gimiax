/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreDataEntityInfoHelper_H
#define coreDataEntityInfoHelper_H

#include "gmDataHandlingWin32Header.h"
#include "coreObject.h"
#include "coreDataEntity.h"
#include <list>

namespace Core
{
/**
\brief Identifier string for each DataEntityType
*/
class DataType
{
public:
	//!
	DataType( 
		const std::string &type = "", 
		const std::string &subType = "", 
		const std::string &description = "",
		Core::DataEntityType dataEntityType = Core::UnknownTypeId )
	{
		m_Type = type;
		m_SubType = subType;
		m_Description = description;
		m_DataEntityType = dataEntityType;
		m_InputDataEntityType = dataEntityType;
	}
	//!
	DataType( 
		const std::string &type, 
		const std::string &subType, 
		const std::string &description,
		Core::DataEntityType dataEntityType,
		Core::DataEntityType inputDataEntityType)
	{
		m_Type = type;
		m_SubType = subType;
		m_Description = description;
		m_DataEntityType = dataEntityType;
		m_InputDataEntityType = inputDataEntityType;
	}
	//!
	DataType( const DataType &dataType )
	{
		m_Type = dataType.m_Type;
		m_SubType = dataType.m_SubType;
		m_Description = dataType.m_Description;
		m_DataEntityType = dataType.m_DataEntityType;
		m_InputDataEntityType = dataType.m_InputDataEntityType;
	}

	std::string m_Type;
	std::string m_SubType;
	std::string m_Description;
	// Data entity type when creating a DataEntity (e.g. IMAGE|ROI)
	Core::DataEntityType m_DataEntityType;
	// Data entity type when selecting an input DataEntity (e.g. ROI)
	Core::DataEntityType m_InputDataEntityType;
};

/** 
\brief DataEntityHelper provides helper functions for working with 
DataEntities and to retrieve info from them:
 - Modality
 - Type

\ingroup gmDataHandling
\author Juan A. Moya
\date 26 Mar 2008
*/
class GMDATAHANDLING_EXPORT DataEntityInfoHelper : public Core::Object
{
public:
	typedef Core::DataEntityType DataEntityType;
	typedef std::list<DataEntityType> EntityList;
	typedef Core::ModalityType ModalityType;
	typedef std::list<ModalityType> ModalityList;
	typedef std::list<Core::DataType> DataTypeListType;

public:
	//! Define virtual destructor to avoid warning
	virtual ~DataEntityInfoHelper( );

	//! Returns the current type of entity in its std:string representation 
	static std::string GetEntityTypeAsString(
		const Core::DataEntityType &type);
	static std::string GetModalityTypeAsString(
		const DataEntityInfoHelper::ModalityType &modality);
	static EntityList GetAvailableEntityTypes(void);
	static ModalityList GetAvailableModalityTypes(void);

	//! Returns a std:string with a short description of what the DataEntity is holding 
	static std::string GetShortDescription(
		Core::DataEntity::Pointer dataEntity);
	static ModalityType GetModalityType( const std::string &strModality );
	static Core::DataEntityType GetDataEntityType( 
		const std::string &strDataEntityType );

	/** Get start time in seconds using the tag "StartTime"
	The tag is a std::string with the format HH:MM:SS
	*/
	static int GetStartTime( Core::DataEntity::Pointer dataEntity );

	/** Get type as an identifier string. For example ROITypeId -> "image", "label"
	\return the first occurrence
	**/
	static DataType IdToDataType( const Core::DataEntityType &type );

	/** Get identifier string type as DataEntityType. For example "image","label" -> ROITypeId
	\return the first occurrence
	*/
	static Core::DataEntityType DataTypeToId( const DataType &dataType );

	/** Get identifier string type as DataEntityType for input controls. For example "image","label" -> ROITypeId
	\return the first occurrence
	*/
	static Core::DataEntityType DataTypeToInputId( const DataType &dataType );

private:
	//!
	static void BuildMaps( );

private:
	//! names for each data entity type
	static DataTypeListType m_ListDataType;

	//! names for each modality type
	static std::map<Core::ModalityType,std::string> m_MapModalityType;
};

}

#endif
