/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDataEntityImpl.h"
#include "coreDataEntityImplFactory.h"
#include "coreException.h"

using namespace Core;

DataEntityImpl::DataEntityImplMapType DataEntityImpl::m_DataEntityImplMap;

/**
*/
Core::DataEntityImpl::DataEntityImpl( ) 
{
	m_RealTime = CONSTANT_IN_TIME;
	m_GenerateTemporalData = true;
	m_MemoryOwner = "Unknown";
}

bool Core::DataEntityImpl::IsConstantInTime() const
{
	return m_RealTime == CONSTANT_IN_TIME;
}


Core::RealTime Core::DataEntityImpl::GetRealTime() const
{
	return m_RealTime;
}

void Core::DataEntityImpl::SetRealTime( Core::RealTime val )
{
	m_RealTime = val;
}

void Core::DataEntityImpl::DeepCopy( DataEntityImpl::Pointer input, ImportMemoryManagementType mem /*= gmCopyMemory*/ )
{
	blTagMap::Pointer data = blTagMap::New();

	// In some implementations, SetData( ) is not implemented
	// and we should be able to call DeepCopy( ) with an empty
	// input DataEntity
	if ( input->GetSize() == 0 )
	{
		return;
	}

	SetRealTime( input->GetRealTime() );

	try
	{
		// Try without generating temporal data
		input->SetGenerateTemporalData( false );
		input->GetData( data );
		SetData( data, mem );
	}
	catch(...)
	{
		try
		{
			// Try using temporal data
			input->SetGenerateTemporalData( true );
			input->GetData( data );
			SetData( data, mem );
			input->CleanTemporalData( );
		}
		catch(...)
		{
			input->CleanTemporalData( );
			throw;
		}
	}
}

void Core::DataEntityImpl::DeepCopy( boost::any val, ImportMemoryManagementType mem /*= gmCopyMemory*/ )
{
	// Create a DataEntityImpl for this type
	BaseFactory::Pointer factory;
	factory = DataEntityImplFactoryHelper::FindFactoryByType( val.type().name() );
	if ( factory.IsNull() )
	{
		throw Core::Exceptions::FactoryNotFoundException("DataEntityImpl::DeepCopy");
	}

	DataEntityImpl::Pointer temporalDataEntity;
	temporalDataEntity = DataEntityImpl::SafeDownCast( factory->CreateInstance() );
	temporalDataEntity->SetDataPtr( val );
	DeepCopy( temporalDataEntity, mem );
}

DataEntityImpl::Pointer Core::DataEntityImpl::GetAt( size_t num )
{
	return this;
}

void Core::DataEntityImpl::SetAt( 
	size_t pos, 
	DataEntityImpl::Pointer data,
	ImportMemoryManagementType mem /*= gmCopyMemory*/ )
{
	throw Exceptions::NotImplementedException( "DataEntityImpl::SetAt" );
}

size_t Core::DataEntityImpl::GetSize() const
{
	return 1;
}

void* Core::DataEntityImpl::GetVoidPtr() const
{
	return 0;
}

void Core::DataEntityImpl::SetVoidPtr( void* )
{

}

void Core::DataEntityImpl::SetSize( size_t size, DataEntityImpl::Pointer data )
{
}

void Core::DataEntityImpl::SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem )
{
	throw Exceptions::NotImplementedException( "DataEntityImpl::SetData" );
}

void Core::DataEntityImpl::GetData( blTagMap::Pointer tagMap )
{
	throw Exceptions::NotImplementedException( "DataEntityImpl::GetData" );
}

bool Core::DataEntityImpl::IsValidType( const std::string &datatypename )
{
	return ( datatypename == GetDataPtr().type().name() );
}

void Core::DataEntityImpl::SetDataPtr( boost::any val )
{
	if ( val.type() != GetDataPtr().type() )
	{
		throw Exceptions::DataTypeNotValidException( "DataEntityImpl::SetDataPtr" );
	}

	SetAnyData( val );
}

bool Core::DataEntityImpl::GetGenerateTemporalData() const
{
	return m_GenerateTemporalData;
}

void Core::DataEntityImpl::SetGenerateTemporalData( bool val )
{
	m_GenerateTemporalData = val;
}

void Core::DataEntityImpl::CleanTemporalData()
{
	
}

blTag::Pointer Core::DataEntityImpl::SafeFindTag( 
	blTagMap::Pointer tagMap, const std::string &name )
{
	blTag::Pointer tag = tagMap->FindTagByName( name );
	if ( tag.IsNull() )
	{
		throw Exceptions::TagNotFoundException( "DataEntityImpl::SafeFindTag", name.c_str() );
	}
	return tag;
}

std::string Core::DataEntityImpl::GetMemoryOwner( )
{
	return m_MemoryOwner;
}

void Core::DataEntityImpl::SetMemoryOwner( std::string val )
{
	m_MemoryOwner = val;
}
