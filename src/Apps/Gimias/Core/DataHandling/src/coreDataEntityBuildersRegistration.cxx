/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDataEntityBuildersRegistration.h"

#include "coreDataEntityImplFactory.h"
#include "coreFactoryManager.h"

#include "coreMultipleDataEntityImpl.h"
#include "coreContourImpl.h"
#include "coreNumericImpl.h"
#include "coreSessionImpl.h"

using namespace Core;

DataEntityBuildersRegistration::DataEntityBuildersRegistration()
{
}

DataEntityBuildersRegistration::~DataEntityBuildersRegistration()
{
}


void DataEntityBuildersRegistration::RegisterDataEntityBuilders()
{
	FactoryManager::Register( DataEntityImpl::GetNameClass(), MultipleDataEntityImpl::Factory::New() );
	FactoryManager::Register( DataEntityImpl::GetNameClass(), ContourImpl::Factory::New() );
	FactoryManager::Register( DataEntityImpl::GetNameClass(), NumericImpl::Factory::New() );
	FactoryManager::Register( DataEntityImpl::GetNameClass(), SessionImpl::Factory::New() );
}

void Core::DataEntityBuildersRegistration::UnRegisterDataEntityBuilders()
{
	FactoryManager::FactoryListType factories;
	factories = FactoryManager::FindAll( DataEntityImpl::GetNameClass() );

	FactoryManager::FactoryListType::iterator it;
	for( it = factories.begin( ) ; it != factories.end() ; it++ )
	{
		FactoryManager::UnRegister( *it );
	}

}
