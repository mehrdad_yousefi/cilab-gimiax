/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreMultipleDataEntityImpl_H
#define _coreMultipleDataEntityImpl_H

#include "gmDataHandlingWin32Header.h"
#include "coreDataEntityImplFactory.h"

namespace Core{

/**
Set of DataEntityImpl
\author Xavi Planes
\date 06 Sept 2010
\ingroup gmDataHandling
*/
class GMDATAHANDLING_EXPORT MultipleDataEntityImpl : public DataEntityImpl
{
public:
	coreDeclareSmartPointerClassMacro( Core::MultipleDataEntityImpl, DataEntityImpl )

	coreDefineMultipleDataFactory( MultipleDataEntityImpl, DataEntityImplSetType, UnknownTypeId )

public:

	//@{ 
	/// \name Interface
	virtual size_t GetSize() const;
	virtual DataEntityImpl::Pointer GetAt( size_t num );
	virtual void SetAt( 
		size_t pos, 
		DataEntityImpl::Pointer data,
		ImportMemoryManagementType mem = gmCopyMemory );
	virtual void SwitchImplementation( const std::string &datatypename );

	virtual boost::any GetDataPtr() const;
	virtual void ResetData( );
	std::string GetMemoryOwner( );

private:
	virtual void SetAnyData( boost::any val );
	virtual void SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem = gmCopyMemory );
	virtual void GetData( blTagMap::Pointer tagMap );
	//@}


protected:
	//!
	MultipleDataEntityImpl( );

	//!
	virtual ~MultipleDataEntityImpl( );

protected:

	//! Set of MultipleDataEntityImpl
	DataEntityImplSetType m_Data;
	//! Factory for each time step
	BaseFactory::Pointer m_Factory;
};

} // end namespace Core

#endif // _coreMultipleDataEntityImpl_H

