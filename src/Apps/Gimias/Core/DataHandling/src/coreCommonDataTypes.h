/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef CORECOMMONDATATYPES_H
#define CORECOMMONDATATYPES_H

#include "coreDataHolder.h"

namespace Core
{
	typedef Core::DataHolder<bool> BoolHolderType;

	// TYPES
	typedef Core::DataHolder<int> IntHolderType;

	// TYPES
	typedef Core::DataHolder<float> FloatHolderType;

};


#endif //CORECOMMONDATATYPES_H
