/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreDataEntity_H
#define coreDataEntity_H

#include "gmDataHandlingWin32Header.h"
#include "coreObject.h"
#include "coreDataEntityMetadata.h"
#include "coreDataEntityImpl.h"
#include "coreDataEntityImplFactory.h"
#include "coreDataEntityTypes.h"
#include "coreDataEntityPreview.h"
#include <string>
#include <vector>
#include <typeinfo>
#include <boost/signals.hpp>
#include <boost/any.hpp>

namespace Core{

class RenDataBuilder;

typedef boost::any AnyProcessingData;

/**
This helper function tries to cast \a data to T, and store the result 
in \a result.
Returns true iff the cast could be made.

\ingroup gmDataHandling
\author Maarten Nieber
\date 15 sep 2008
*/

template< class T >
bool CastAnyProcessingData(const AnyProcessingData& data, T& result);

typedef size_t TimeStepIndex;

/** 
\brief A DataEntity is a class that represents "data" that can be processed 
by GIMIAS, such as an image, mesh, simulation result, etc.

DataEntity is the basic unit for data processing, mapping and interaction 
for the Core. Every image, mesh, etc going in and out of the Core should 
be cast to a DataEntity

Every DataEntity has a header with meta-info required to understand what 
it holds: Core::DataEntityMetadata. It has also a set of processing data 
objects, that holds the data in its original representation: Core::DataEntityImpl.

The DataEntity also holds a rendering representation (DataEntity::Pointer) so as to 
allow displaying and rendering the original data. The rendering data object 
holds a rendering representation of the original 
data object, called also processing data (the type used when processing 
it with algorithms). Therefore, whenever the original data is modified, 
a Builder must be called to convert that data into its rendering 
representation. Rendering Data Builders are responsible for converting 
original data into its rendering representation. 

DataEntity contains a set of TimeSteps. Each time step stores the data in 
a concrete time instant. The DataEntityImpl class follows the Bridge Pattern: 
The intent of Bridge pattern is to decouple an abstraction from its 
implementation so that the two can vary independently. This allows to 
switch the current implementation of a data object on the fly, depending 
on the used filter. Imagine that the filter1 uses vtkPolyData surface 
meshes and the filter2 uses Netgen surface meshes. When the filter1 is 
called, the data implementation will switch to vtkPolydata automatically. 
When the filter2 is called, the data implementation will be switched 
to Netgen automatically.

Each new data type should extend this hierarchy. A plugin, for example, 
can extend the possible surface mesh implementations supported by the 
framework. Imagine you have a new data type called NetgenMesh. To add 
this implementation to the framework, you need to create a subclass of 
SurfaceTimeStepImpl and register this new class into the framework. Each 
time a processor needs a NetgenMesh, the current surface mesh 
implementation will be automatically converted.


\author Juan A. Moya
\date 26 Mar 2008
\ingroup gmDataHandling
*/
class GMDATAHANDLING_EXPORT DataEntity : public Core::SmartPointerObject
{
public:

	typedef boost::signal1<void, Core::DataEntity*> DataEntityDestroyedSignal;

	coreDeclareSmartPointerTypesMacro(Core::DataEntity, Core::SmartPointerObject)
	coreClassNameMacro(Core::DataEntity)
	coreFactorylessNewMacro(Core::DataEntity)
	coreFactorylessNewMacro1Param(Core::DataEntity, DataEntityType)
	coreFactorylessNewMacro2Param(Core::DataEntity, DataEntityType, ModalityType)

	//! Typedefef for keeping references to all the children of the data Entity
	typedef std::vector<Core::DataEntity*> ChildrenListType;

	//! Map a string into a DataEntityImpl
	typedef std::map<std::string, DataEntity::Pointer> RenderingDataMapType;

	int GetId() const;
	DataEntityType GetType() const;
	void SetType(DataEntityType type);
	//! Returns data entity cast to ImageDataEntity (if the modality is image) or NULL otherwise.
	bool IsImage() const;
	//! Convenience function.
	bool IsSurfaceMesh() const;
	//! Convenience function.
	bool IsVolumeMesh() const;
	//! Convenience Function
	bool IsSkeletonMesh() const;
	//! Convenience Function
	bool IsROI( ) const;
	//! Convenience Function
	bool IsMeasurement( ) const;
	//! Convenience Function
	bool IsPointSet( ) const;
	//! Convenience Function
	bool IsSignal() const;
	//! Convenience Function
	bool IsContour( ) const;
	//! Convenience Function
	bool IsNumericData() const;
	//! Convenience Function
	bool IsVectorField() const;
	//! Convenience Function
	bool IsTransform() const;
	//! Convenience Function
	bool IsCuboid() const;
	//! Convenience Function
	bool IsTensor() const;


	//! Get a rendering data that matches the type, like mitk::BaseData::Pointer
	DataEntity::Pointer GetRenderingData( const std::string& name );
	
	/** Set rendering data for a concrete rendering view
	If the data already exists, replace the pointer
	*/
	void SetRenderingData( const std::string& name, DataEntity::Pointer dataEntity );
	
	//! \note Return empty pointer if timeStep > GetTimeDimension( )
	boost::any GetProcessingData(TimeStepIndex timeStep = 0);

	/** Templated GET method. 
	\param [in] switchImplementation If type of T is not the type of the 
	internal pointer, it will try to change to another implementation 
	\param [in] generateNewInstance Generate a new instance of processing data
	*/
	template <typename T> bool GetProcessingData(
		T& processingDataByRef, 
		TimeStepIndex timeStep = 0,
		bool switchImplementation = false,
		bool generateNewInstance = false );

	/** GET method. 
	\param [in] switchImplementation If type of T is not the type of the 
	internal pointer, it will try to change to another implementation 
	\param [in] generateNewInstance Generate a new instance of processing data
	\note Throws an exception if data cannot be retrieved
	*/
	void GetProcessingData(
		boost::any &processingDataType, 
		TimeStepIndex timeStep,
		bool switchImplementation = false,
		bool generateNewInstance = false );

	/** Add a new time step
	Create a new DataEntityImpl that can store processingData. Tries to add
	the new implementation in the m_DataEntityImpl. If it fails and the factory
	is multiple it will replace the m_DataEntityImpl	
	*/
	bool AddTimeStep(
		boost::any processingData, 
		RealTime realTime = DataEntityImpl::CONSTANT_IN_TIME);

	/** Get implementation at a specific time step
	\param [in] timeStepIndex If it's -1, return the internal variable m_DataEntityImpl,
	otherwise, return the implementation at specific time step 
	*/
	Core::DataEntityImpl::Pointer GetTimeStep( int timeStepIndex = -1 );

	//!
	template <typename T> 
	bool AddTimeSteps( std::vector<T> &processingData);

	//!
	size_t GetNumberOfTimeSteps() const;

	//! Reset time steps to 0
	void ResetTimeSteps( );

	//! Resize the implementation to handle this type
	bool Resize( int size, const std::type_info &type );

	//!
	double GetTimeAtTimeStep(TimeStepIndex timeStep) const;

	//! set real time for every timestep
	void SetTimeAtTimeStep (TimeStepIndex timeStep, double timeVal);

	//! Find closest timestep to a real time 
	TimeStepIndex FindTimeStep( Core::RealTime time );

	//! set real time for every timestep
	void SetTimeForAllTimeSteps (std::vector <float>& timesteps);

	/** Replace timeStep with this new data and call UpdateRenderingRepresentation( )
	If the size if m_TimeSteps is <= timeStep -> throw an exception
	*/
	void SetTimeStep(
						boost::any processingDataObj, 
						TimeStepIndex timeStep = 0,
						RealTime realTime = DataEntityImpl::CONSTANT_IN_TIME );

	/** 
	Because a DataEntity is referenced by a mitk::GenericProperty, 
	it has to implement the following members:
	- an operator<< so that the properties value can be put into a std::stringstream
    - an operator== so that two properties can be checked for equality
	
	Read the documentation of Core::DataEntityProperty, you can find 
	its definition at coreMitkProperties.h
	*/
	virtual std::string operator*(void);

	/** Templated subject-observer methods (using boost signals). 
	\note Remember to include coreDataEntity.txx
	*/
	template <class T> boost::signals::connection AddObserverOnDestroyDataEntity(
		T* observer, 
		void (T::*slotFunction)(Core::DataEntity*));
	
	//! Set The father of the Data Enity
	/*! A children data entity is a Data Entity that is associated to 
	another Data Entity, e.g. an ECG associated	to a image
	 \param father -[in] the Father Data Entity
	*/
	bool SetFather(DataEntity* father);

	//! Returns the father. If it's doesn't have a father, returns null
	Pointer GetFather();

	/** Returns the children list. 
	If the data entity doesn't have any child, the list is empty
	*/
	ChildrenListType GetChildrenList();

	//!
	Core::DataEntityMetadata::Pointer GetMetadata() const;

	//!
	Core::DataEntityPreview::Pointer GetPreview( ) const;

	//!
	void SetPreview( Core::DataEntityPreview::Pointer preview );

	//! Change to a implementation that can handle this type
	bool SwitchImplementation( const std::string &datatypename );

	//! Change to a implementation that can handle this type
	bool SwitchImplementation( const std::type_info &type );

	//! If data cannot be copied, it will throw an exception
	void Copy( const DataEntity::Pointer& de, ImportMemoryManagementType mem = gmCopyMemory );

	//! Copy data pointers
	void CopyDataPtr( const DataEntity::Pointer& de );

	/** Move m_DataEntityImpl from source to dest
	This function is usefull when the impl cannot be copied, like CMGUI region
	*/
	void MoveDataImpl( const DataEntity::Pointer de );

	//! Get name of the plugin that owns the memory allocated
	std::string GetMemoryOwner( );

	//! Unload internal memory
	void Clean( );

protected:
	DataEntity(DataEntityType type = UnknownTypeId, ModalityType modality = UnknownModality);
	virtual ~DataEntity();

private:
	coreDeclareNoCopyConstructors(DataEntity);

	//! Add a data entity to the list of children
	void RegisterChild(Core::DataEntity* child);

	//! Removes the child that has the coreDataEntityId from the list. 
	/*! Returns true if success,false otherwise
	\param coreDataEntityId -[in] The Id of the child dataEntity to be removed
	*/
	bool RemoveChildFromList(int coreDataEntityId);

private:
	virtual void NotifyObserversOfDestruction(void) const;

	//! This a Core TAG that cannot be modified and will be created by Core
	DataEntityType m_Type;
	
	//! Id: This is a Core TAG that cannot be modified and will be created by Core
	unsigned int m_Id;

	//!
	DataEntityImpl::Pointer m_DataEntityImpl;

	/** 
	Different implementations of rendering data
	*/
	RenderingDataMapType m_RenderingDataList;

	// SignalTypeId handling
	DataEntityDestroyedSignal m_OnDestroyedSignal;

	// ID count of already created DataEntities
	static unsigned int dataEntityIdCount;

	//! A list containing all the children of a data entity
	ChildrenListType m_ChildrenList;

	//! The Father of the Data Entity. Each data Entity can have only one father
	DataEntity* m_Father;

	//!
	Core::DataEntityMetadata::Pointer m_Metadata;

	//!
	Core::DataEntityPreview::Pointer m_Preview;
};

} // end namespace Core

#include "coreDataEntity.txx"
#include "coreDataEntityHolder.h"

#endif

