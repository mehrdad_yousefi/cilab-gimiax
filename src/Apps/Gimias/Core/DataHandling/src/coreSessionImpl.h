/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreSessionImpl_H
#define _coreSessionImpl_H

#include "gmDataHandlingWin32Header.h"
#include "coreDataEntityImplFactory.h"
#include "blTagMap.h"


namespace Core{

/**

Data entity for storing a session

\author Xavi Planes
\date Oct 2012
\ingroup gmDataHandling
*/
class GMDATAHANDLING_EXPORT SessionImpl : public DataEntityImpl
{
public:
	typedef blTagMap::Pointer DataType;

public:
	coreDeclareSmartPointerClassMacro( Core::SessionImpl, DataEntityImpl );

	coreDefineSingleDataFactory( SessionImpl, DataType, SessionTypeId )


	//@{ 
	/// \name Interface
public:
	boost::any GetDataPtr() const;
	std::string GetMemoryOwner( );

private:
	virtual void SetData( blTagMap::Pointer tagMap, ImportMemoryManagementType mem = gmCopyMemory );
	virtual void GetData( blTagMap::Pointer tagMap );
	void SetAnyData( boost::any val );
	virtual void ResetData( );
	//@}

protected:
	//!
	SessionImpl( );

	//!
	virtual ~SessionImpl();

	//! Not implemented
	SessionImpl(const Self&);

	//! Not implemented
   	void operator=(const Self&);

private:

	//!
	DataType m_Data;
};


}

#endif //_coreSessionImpl_H
