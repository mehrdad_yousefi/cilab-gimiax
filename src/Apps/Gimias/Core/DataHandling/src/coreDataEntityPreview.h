/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreDataEntityPreview_H
#define coreDataEntityPreview_H

#include "gmDataHandlingWin32Header.h"
#include "coreObject.h"

namespace Core{

class DataEntity;

/** 
\brief Base class for preview image of a DataEntity

\author Xavi Planes
\date Nov 2012
\ingroup gmDataHandling
*/
class GMDATAHANDLING_EXPORT DataEntityPreview : public Core::SmartPointerObject
{
public:
	coreDeclareSmartPointerClassMacro(Core::DataEntityPreview,Core::SmartPointerObject)

public:

	//! Load preview image using FileName
	virtual void Load( );

	//! Save preview image to FileName path
	virtual void Save( );

	//! Save preview in a Temp file
	virtual void SaveTempFile( );

	//! Get preview image
	virtual boost::any GetImage( );

	//! Set preview image
	virtual void SetImage( boost::any val );

	//! Retrun true if preview image is ok
	virtual bool IsOk( );

	//!
	virtual void Copy( DataEntityPreview::Pointer val );

	//! Set related DataEntity
	void SetDataEntity( Core::DataEntity* dataEntity );

	//!
	void SetTempFolder( const std::string &val );

	//! Get filename using "FilePath" attribute of Metadata
	std::string GetFileName( );

	//!
	std::string GetTmpFileName( );

	//!
	void SetName( const std::string &name );

	//!
	void SetFolder( const std::string &folder );
protected:
	//!
	DataEntityPreview( );

	//!
	virtual ~DataEntityPreview();

	//! Remove Temp file (called at destructor)
	void RemoveTempFile( );

	//! Generate Temp file
	void GenerateTempFile( );

protected:
	//!
	Core::DataEntity* m_DataEntity;
	//!
	std::string m_TempFolder;
	//!
	std::string m_TempFileName;
	//! Configure name externally
	std::string m_Name;
	//!
	std::string m_Folder;
};

} // end namespace Core


#endif // corePreview_H

