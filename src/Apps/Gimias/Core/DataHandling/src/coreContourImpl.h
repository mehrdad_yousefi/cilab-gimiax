/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreContourImpl_H
#define _coreContourImpl_H

#include "gmDataHandlingWin32Header.h"
#include "coreDataEntityImplFactory.h"

namespace Core{

/**

Data entity for storing contours

\author Xavi Planes
\date 24 April 2009
\ingroup gmDataHandling
*/

class GMDATAHANDLING_EXPORT ContourImpl : public DataEntityImpl
{
public:
	typedef std::vector<float> DataType;

public:
	coreDeclareSmartPointerClassMacro( Core::ContourImpl, DataEntityImpl );

	coreDefineSingleDataFactory( ContourImpl, DataType, ContourTypeId )

	//@{ 
	/// \name Interface
public:
	boost::any GetDataPtr() const;
	std::string GetMemoryOwner( );

private:
	virtual void SetAnyData( boost::any val );
	virtual void ResetData( );
	//@}


protected:
	//!
	ContourImpl( );

	//!
	virtual ~ContourImpl();

	//! Not implemented
	ContourImpl(const Self&);

	//! Not implemented
   	void operator=(const Self&);

private:
	//!
	DataType m_Data;

};


}

#endif //_coreContourImpl_H
