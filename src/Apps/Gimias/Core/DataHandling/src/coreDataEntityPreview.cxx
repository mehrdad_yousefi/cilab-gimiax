/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDataEntityPreview.h"
#include "coreDataEntity.h"
#include "itksys/SystemTools.hxx"

Core::DataEntityPreview::DataEntityPreview( )
{
	m_DataEntity = NULL;
}

Core::DataEntityPreview::~DataEntityPreview(void)
{
	RemoveTempFile( );
}

void Core::DataEntityPreview::RemoveTempFile( )
{
	if ( !m_TempFileName.empty( ) &&
		itksys::SystemTools::FileExists( m_TempFileName.c_str( ) ) )
	{
		itksys::SystemTools::RemoveFile( m_TempFileName.c_str( ) );
	}
}

void Core::DataEntityPreview::SetTempFolder( const std::string &val )
{
	RemoveTempFile( );
	m_TempFolder = val;
	GenerateTempFile( );
	// Create directory
	itksys::SystemTools::MakeDirectory( m_TempFolder.c_str( ) );
}

void Core::DataEntityPreview::GenerateTempFile( )
{
	if ( m_TempFolder.empty( ) )
	{
		throw Core::Exceptions::Exception( "GenerateTempFile", "Temp folder is not configured" );
	}

	if ( !m_Name.empty( ) )
	{
		std::stringstream stream;
		stream << m_TempFolder << "/" << "Preview" << m_Name << ".png";
		m_TempFileName = stream.str( );
	}
	else if ( m_DataEntity )
	{
		std::stringstream stream;
		stream << m_TempFolder << "/" << "Preview" << m_DataEntity->GetId( ) << ".png";
		m_TempFileName = stream.str( );
	}
}

void Core::DataEntityPreview::SetDataEntity( Core::DataEntity* dataEntity )
{
	m_DataEntity = dataEntity;
}

std::string Core::DataEntityPreview::GetFileName( )
{
	std::string previewFileName;
	if ( !m_Name.empty( ) )
	{
		previewFileName =  m_Folder + "/prev" + m_Name + ".png";
	}
	else if ( m_DataEntity )
	{
		blTag::Pointer tag = m_DataEntity->GetMetadata( )->GetTag( "FilePath" );
		if ( tag.IsNull( ) )
		{
			return "";
		}

		std::string path = itksys::SystemTools::GetFilenamePath( tag->GetValueAsString( ) );
		tag = m_DataEntity->GetMetadata( )->GetTag( "Name" );
		previewFileName =  path + "/prev" + tag->GetValueAsString( ) + ".png";
	}

	return previewFileName;
}

void Core::DataEntityPreview::SetName( const std::string &name )
{
	m_Name = name;
}

void Core::DataEntityPreview::SetFolder( const std::string &folder )
{
	m_Folder = folder;
}

std::string Core::DataEntityPreview::GetTmpFileName( )
{
	return m_TempFileName;
}

void Core::DataEntityPreview::Load( )
{
}

void Core::DataEntityPreview::Save( )
{
}

void Core::DataEntityPreview::SaveTempFile( )
{
}

boost::any Core::DataEntityPreview::GetImage( )
{
	return NULL;
}

void Core::DataEntityPreview::SetImage( boost::any val )
{
}

bool Core::DataEntityPreview::IsOk( )
{
	return false;
}

void Core::DataEntityPreview::Copy( DataEntityPreview::Pointer val )
{
}

