/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDataEntityInfoHelper.h"
#include "coreAssert.h"

#include <sstream>
#include <stdio.h>

using namespace Core;

Core::DataEntityInfoHelper::DataTypeListType Core::DataEntityInfoHelper::m_ListDataType;
std::map<Core::ModalityType,std::string> Core::DataEntityInfoHelper::m_MapModalityType;

Core::DataEntityInfoHelper::~DataEntityInfoHelper()
{

}

std::string DataEntityInfoHelper::GetEntityTypeAsString(
	const Core::DataEntityType &type)
{
	BuildMaps( );

	DataType dataType = IdToDataType( type );
	return dataType.m_Description;
}

std::string DataEntityInfoHelper::GetModalityTypeAsString(
	const DataEntityInfoHelper::ModalityType &modality)
{
	BuildMaps( );
	std::map<Core::ModalityType,std::string>::iterator it;
	it = m_MapModalityType.find( modality );
	if ( it != m_MapModalityType.end() )
	{
		return it->second;
	}
	return "";
}

DataEntityInfoHelper::ModalityType DataEntityInfoHelper::GetModalityType( 
	const std::string &strModality )
{
	DataEntityInfoHelper::ModalityType modality = Core::UnknownModality;

	BuildMaps( );

	static std::map<Core::ModalityType,std::string>::iterator it;
	it = m_MapModalityType.begin();
	while ( it != m_MapModalityType.end() )
	{
		if ( strModality == it->second )
		{
			modality = it->first;
		}
		it++;
	}

	return modality;
}

Core::DataEntityType 
Core::DataEntityInfoHelper::GetDataEntityType( 
	const std::string &strDataEntityType )
{
	Core::DataEntityType dataEntityType = Core::UnknownTypeId;

	BuildMaps( );

	DataTypeListType::iterator it = m_ListDataType.begin();
	while ( it != m_ListDataType.end() )
	{
		std::string subName = strDataEntityType.substr(0,it->m_Description.size());
		if ( subName == it->m_Description )
		{
			dataEntityType = it->m_DataEntityType;
		}
		it++;
	}

	return dataEntityType;
}

DataEntityInfoHelper::ModalityList DataEntityInfoHelper::GetAvailableModalityTypes(void)
{
	ModalityList list;

	BuildMaps( );

	static std::map<Core::ModalityType,std::string>::iterator it;
	it = m_MapModalityType.begin();
	while ( it != m_MapModalityType.end() )
	{
		list.push_back( it->first );
		it++;
	}

	return list;
}

DataEntityInfoHelper::EntityList DataEntityInfoHelper::GetAvailableEntityTypes(void)
{
	EntityList list;

	BuildMaps( );

	DataTypeListType::iterator it = m_ListDataType.begin();
	while ( it != m_ListDataType.end() )
	{
		if ( std::find( list.begin( ), list.end( ), it->m_DataEntityType ) == list.end( ) )
		{
			list.push_back( it->m_DataEntityType );
		}
		it++;
	}

	return list;
}

std::string DataEntityInfoHelper::GetShortDescription(Core::DataEntity::Pointer dataEntity)
{
	std::ostringstream oss;
	oss << dataEntity->GetId() << "#" 
		<< dataEntity->GetMetadata( )->GetName() << " (" 
		<< GetEntityTypeAsString(dataEntity->GetType()) << ", " 
		<< GetModalityTypeAsString(dataEntity->GetMetadata( )->GetModality()) << ")";
	return oss.str();
}

void Core::DataEntityInfoHelper::BuildMaps()
{
	if ( m_MapModalityType.size() == 0 )
	{
		m_MapModalityType[ Core::US ] = "US";
		m_MapModalityType[ Core::MRI ] = "MR";
		m_MapModalityType[ Core::CT ] = "CT";
		m_MapModalityType[ Core::NM ] = "NM";
		m_MapModalityType[ Core::XA ] = "XA";
		m_MapModalityType[ Core::THREE_D_RA ] = "3DRA";
		m_MapModalityType[ Core::RTDOSE ] = "RTDOSE";
		m_MapModalityType[ Core::RTPLAN ] = "RTPLAN";
		m_MapModalityType[ Core::RTSTRUCT ] = "RTSTRUCT";
		m_MapModalityType[ Core::PT ] = "PT";
		m_MapModalityType[ Core::UnknownModality ] = "-";
	}

	if ( m_ListDataType.empty( ) )
	{
		m_ListDataType.push_back( DataType( "image", "", "Volume image", ImageTypeId ) );
		m_ListDataType.push_back( DataType( "image", "scalar", "Volume image", ImageTypeId ) );
		m_ListDataType.push_back( DataType( "image", "any", "Volume image", ImageTypeId ) );
		m_ListDataType.push_back( DataType( "image", "diffusion-weighted", "Volume image", ImageTypeId ) );
		m_ListDataType.push_back( DataType( "geometry", "", "Surface mesh", SurfaceMeshTypeId ) );
		m_ListDataType.push_back( DataType( "geometry", "scalar", "Surface mesh", SurfaceMeshTypeId ) );
		m_ListDataType.push_back( DataType( "geometry", "fiberbundle", "Surface mesh", SurfaceMeshTypeId ) );
		m_ListDataType.push_back( DataType( "geometry", "model", "Surface mesh", SurfaceMeshTypeId ) );
		m_ListDataType.push_back( DataType( "volmesh", "", "Volume mesh", VolumeMeshTypeId ) );
		m_ListDataType.push_back( DataType( "volmesh", "scalar", "Volume mesh", VolumeMeshTypeId ) );
		m_ListDataType.push_back( DataType( "signal", "", "Signal", SignalTypeId ) );
		m_ListDataType.push_back( DataType( "signal", "scalar", "Signal", SignalTypeId ) );
		m_ListDataType.push_back( DataType( "image", "vector", "Vector Field", DataEntityType( ImageTypeId | VectorFieldTypeId ), VectorFieldTypeId ) );
		m_ListDataType.push_back( DataType( "transform", "", "Transform file", TransformId ) );
		m_ListDataType.push_back( DataType( "transform", "unknown", "Transform file", TransformId ) );
		m_ListDataType.push_back( DataType( "transform", "linear", "Transform file", TransformId ) );
		m_ListDataType.push_back( DataType( "transform", "nonlinear", "Transform file", TransformId ) );
		m_ListDataType.push_back( DataType( "transform", "bspline", "Transform file", TransformId ) );
		m_ListDataType.push_back( DataType( "geometry", "skeleton", "Skeleton", DataEntityType( SurfaceMeshTypeId | SkeletonTypeId ), SkeletonTypeId ) );
		m_ListDataType.push_back( DataType( "pointset", "", "Point set", PointSetTypeId ) );
		m_ListDataType.push_back( DataType( "pointset", "scalar", "Point set", PointSetTypeId ) );
		// Not in XML
		m_ListDataType.push_back( DataType( "cuboid", "", "Cuboid", CuboidTypeId ) );
		// Not in XML
		m_ListDataType.push_back( DataType( "contour", "", "Contour", ContourTypeId ) );
		// Not in XML
		m_ListDataType.push_back( DataType( "pointset", "measurement", "Measurement", DataEntityType( PointSetTypeId | MeasurementTypeId ), MeasurementTypeId ) );
		m_ListDataType.push_back( DataType( "image", "label", "ROI", DataEntityType( ImageTypeId | ROITypeId ), ROITypeId ) );
		// Not in XML
		m_ListDataType.push_back( DataType( "sliceimage", "", "Slice image", SliceImageTypeId ) );
		// Not in XML
		m_ListDataType.push_back( DataType( "numericdata", "", "Numeric Data", NumericDataTypeId ) );
		m_ListDataType.push_back( DataType( "image", "tensor", "Tensor", TensorTypeId ) );
		m_ListDataType.push_back( DataType( "-", "-", "-", UnknownTypeId ) );
		// Not in XML
		m_ListDataType.push_back( DataType( "session", "", "Session", SessionTypeId ) );
	}
}

int Core::DataEntityInfoHelper::GetStartTime( Core::DataEntity::Pointer dataEntity )
{
	if ( dataEntity.IsNull() )
	{
		return 0;
	}

	blTag::Pointer startTimeTag = dataEntity->GetMetadata()->GetTag( "StartTime" );
	if ( startTimeTag.IsNull() )
	{
		return 0;
	}

	std::string startTimeStr = startTimeTag->GetValueAsString();
	int seconds = 0;
	int minutes = 0;
	int hours = 0;
	sscanf(startTimeStr.c_str(), "%d:%d:%d",&hours, &minutes, &seconds);
	int timeSec = seconds + minutes * 60 + hours * 3600;

	return timeSec;
}


Core::DataType Core::DataEntityInfoHelper::IdToDataType( const Core::DataEntityType &type )
{
	BuildMaps( );
	
	DataTypeListType::iterator it = m_ListDataType.begin();
	while ( it != m_ListDataType.end() )
	{
		if ( it->m_DataEntityType == type )
		{
			return *it;
		}
		it++;
	}

	return DataType( "" );
}

Core::DataEntityType Core::DataEntityInfoHelper::DataTypeToId( const DataType &dataType )
{
	Core::DataEntityType dataEntityType = Core::UnknownTypeId;

	BuildMaps( );

	DataTypeListType::iterator it = m_ListDataType.begin();
	while ( it != m_ListDataType.end() )
	{
		if ( dataType.m_Type == it->m_Type && dataType.m_SubType == it->m_SubType )
		{
			dataEntityType = it->m_DataEntityType;
		}
		it++;
	}

	return dataEntityType;
}


Core::DataEntityType Core::DataEntityInfoHelper::DataTypeToInputId( const DataType &dataType )
{
	Core::DataEntityType dataEntityType = Core::UnknownTypeId;

	BuildMaps( );

	DataTypeListType::iterator it = m_ListDataType.begin();
	while ( it != m_ListDataType.end() )
	{
		if ( dataType.m_Type == it->m_Type && dataType.m_SubType == it->m_SubType )
		{
			dataEntityType = it->m_InputDataEntityType;
		}
		it++;
	}

	return dataEntityType;
}


