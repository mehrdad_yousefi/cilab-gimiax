/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifdef WIN32 
#pragma warning(push)
#pragma warning(disable: 4675)
#pragma warning(disable: 4251)
#pragma warning(disable: 4275)
#endif

#include "coreDataEntity.h"
#include "coreException.h"
#include "coreAssert.h"
#include "coreMultipleDataEntityImpl.h"

using namespace Core;

// ---------------------------------------------------------------------------
unsigned int DataEntity::dataEntityIdCount = 0;

const bool ENABLE_DATAENTITY_DEBUG = false;

// ---------------------------------------------------------------------------


/** Initializer for the class DataEntity 
*/
DataEntity::DataEntity(
	DataEntityType type /*= UnknownTypeId*/, 
	ModalityType modality /*= UnknownModality*/)
{
	m_Id = ++DataEntity::dataEntityIdCount;
	m_Metadata = DataEntityMetadata::New( m_Id );
	m_Metadata->SetName( "Unnamed" );
	m_Metadata->SetModality( modality );
	SetType( type );
	m_Father = NULL;
	m_ChildrenList.clear();
	m_DataEntityImpl = MultipleDataEntityImpl::New();

	// Create an instance of preview
	BaseFactory::Pointer factory = FactoryManager::Find( DataEntityPreview::GetNameClass() );
	if ( factory.IsNotNull( ) )
	{
		m_Preview = dynamic_cast<DataEntityPreview*> ( factory->CreateInstance( ).GetPointer( ) );
	}
	else
	{
		m_Preview = DataEntityPreview::New( );
	}
	m_Preview->SetDataEntity( this );
}

/** Destructor for the class DataEntity */
DataEntity::~DataEntity(void)
{
	NotifyObserversOfDestruction();

	if ( ENABLE_DATAENTITY_DEBUG )
	{
		std::cout << "Destroying: " << GetId( );
	}

	//If has a father, remove the reference. If there are children, delete them
	if (GetFather().IsNotNull())
	{
		GetFather()->RemoveChildFromList(GetId());
	}
	if ( ENABLE_DATAENTITY_DEBUG )
	{
		std::cout << std::endl;
	}

	// For each children, remove the father
	// All the elements of the vector are dropped: their destructors are called,
	// and then they are removed from the vector container
	while (m_ChildrenList.size())
	{
		m_ChildrenList.at( 0 )->SetFather( NULL );
	}
    
	// first destroy the rendering data because it can reuse the processing 
	// data buffer
	m_RenderingDataList.clear();

	// Destroy preview and temporary file
	m_Preview = NULL;

	// Processing data
	ResetTimeSteps(); 
}

/** Notifies observers OnDestroy signal, being cast when this dataentity is forced to unregister and destroy */
void DataEntity::NotifyObserversOfDestruction(void) const
{
	// Calling the function call operator may invoke undefined behavior if no slots are connected
	// to the signal, depending on the combiner used, so check it was connected to a slot beforehand
	if(!m_OnDestroyedSignal.empty())
		m_OnDestroyedSignal(const_cast<Core::DataEntity*>(this));
}

bool DataEntity::IsImage() const
{
	return ( GetType() & ImageTypeId ) != 0;
}

bool Core::DataEntity::IsSurfaceMesh() const
{
	return ( GetType() & SurfaceMeshTypeId ) != 0;
}

bool Core::DataEntity::IsVolumeMesh() const
{
	return ( GetType() & VolumeMeshTypeId ) != 0;
}

bool Core::DataEntity::IsSkeletonMesh() const
{
	return ( GetType() & SkeletonTypeId ) != 0;
}

bool Core::DataEntity::IsROI() const
{
	return ( GetType() & ROITypeId ) != 0;
}

bool Core::DataEntity::IsMeasurement() const
{
	return ( GetType() & MeasurementTypeId ) != 0;
}

bool Core::DataEntity::IsPointSet() const
{
	return ( GetType() & PointSetTypeId ) != 0;
}

bool Core::DataEntity::IsSignal() const
{
	return ( GetType() & SignalTypeId ) != 0;
}

bool Core::DataEntity::IsContour() const
{
	return ( GetType() & ContourTypeId ) != 0;
}

bool Core::DataEntity::IsNumericData() const
{
	return ( GetType() & NumericDataTypeId ) != 0;
}

bool Core::DataEntity::IsVectorField() const
{
	return ( GetType() & VectorFieldTypeId ) != 0;
}

bool Core::DataEntity::IsTransform() const
{
	return ( GetType() & TransformId ) != 0;
}

bool Core::DataEntity::IsCuboid() const
{
	return ( GetType() & CuboidTypeId ) != 0;
}

bool Core::DataEntity::IsTensor() const
{
	return ( GetType() & TensorTypeId ) != 0;
}

/**
*/
void DataEntity::SetTimeForAllTimeSteps (std::vector <float>& timesteps)
{
	for (unsigned i=0; i<m_DataEntityImpl->GetSize(); i++)
	{
		SetTimeAtTimeStep(i, timesteps.at(i));
	}
}

/**
*/
int Core::DataEntity::GetId() const
{
	return m_Id;
}

/**
*/
Core::DataEntityType Core::DataEntity::GetType() const
{
	return m_Type;
}

/**
*/
void Core::DataEntity::SetType(Core::DataEntityType type)
{
	m_Type = type;
	GetMetadata()->AddTag( "Type", int( type ) );
}

/**
*/
DataEntity::Pointer Core::DataEntity::GetRenderingData( 
	const std::string& name )
{
	RenderingDataMapType::iterator it = m_RenderingDataList.find( name );
	if ( it != m_RenderingDataList.end() )
	{
		return it->second;
	}

	return NULL;
}

/**
*/
void Core::DataEntity::SetRenderingData( 
	const std::string& name, DataEntity::Pointer dataEntity )
{
	// Create a new one or replace old one
	m_RenderingDataList[ name ] = dataEntity;
}

/**
*/
size_t Core::DataEntity::GetNumberOfTimeSteps() const
{
	return m_DataEntityImpl->GetSize();
}

/**
*/
double Core::DataEntity::GetTimeAtTimeStep( TimeStepIndex timeStep ) const
{
	return m_DataEntityImpl->GetAt( timeStep )->GetRealTime();
}

/**
*/
void Core::DataEntity::SetTimeAtTimeStep( TimeStepIndex timeStep, double timeVal )
{
	m_DataEntityImpl->GetAt(timeStep)->SetRealTime(timeVal);
}

/**
*/
void Core::DataEntity::RegisterChild(Core::DataEntity* child) 
{
	if ( ENABLE_DATAENTITY_DEBUG )
	{
		std::cout 
			<< "DataEntity: " << GetId()
			<< "RegisterChild: " << child->GetId()
			<< std::endl;
	}

	// Find if it is yet inside the list
	ChildrenListType::iterator it;
	it = std::find( m_ChildrenList.begin(), m_ChildrenList.end(), child );
	if ( it != m_ChildrenList.end() )
	{
		throw Core::Exceptions::Exception( 
			"RegisterChild", 
			"Children is already in the list" );
	}

	// Check that the child is not it self
	if ( child->GetId() == this->GetId() )
	{
		throw Core::Exceptions::Exception( 
			"RegisterChild", 
			"A child cannot be itself" );
	}

	// Add to the list
	m_ChildrenList.push_back( child );

	Modified( );
}

/**
*/
bool Core::DataEntity::SetFather(Core::DataEntity* father)
{
	// Remove the children from the previous father
	if (m_Father != NULL )
	{
		m_Father->RemoveChildFromList(this->GetId());
	}
	
	m_Father = father;

	// Register the children
	if ( m_Father != NULL )
	{
		m_Father->RegisterChild(this);
	}

	return true;
}

/**
*/
Core::DataEntity::Pointer Core::DataEntity::GetFather()
{
	return m_Father;
}

/**
*/
bool Core::DataEntity::RemoveChildFromList( int coreDataEntityId )
{
	ChildrenListType::iterator it = m_ChildrenList.begin();
	
	if ( ENABLE_DATAENTITY_DEBUG )
	{
		std::cout << " RemoveChildFromList."
			<< " ID: " << GetId( )
			<< " Size: " << m_ChildrenList.size();
	}

	while (it != m_ChildrenList.end())
	{
		if ((*it)->GetId() == coreDataEntityId )
		{
			m_ChildrenList.erase(it);
			if ( ENABLE_DATAENTITY_DEBUG )
			{
				std::cout << " Removed " << coreDataEntityId;
				std::cout << " Size: " << m_ChildrenList.size();
			}
			return true;
		}
		it++;
	}
	return false;
}

/**
*/
Core::DataEntity::ChildrenListType Core::DataEntity::GetChildrenList()
{
	return m_ChildrenList;
}

Core::DataEntityMetadata::Pointer Core::DataEntity::GetMetadata() const
{
	return m_Metadata;
}

Core::DataEntityPreview::Pointer Core::DataEntity::GetPreview() const
{
	return m_Preview;
}

void Core::DataEntity::SetPreview( Core::DataEntityPreview::Pointer preview )
{
	m_Preview = preview;
}

std::string Core::DataEntity::operator*( void )
{
	return GetMetadata()->GetName();
}

bool DataEntity::AddTimeStep(boost::any processingDataObj, RealTime realTime)
{
	BaseFactory::Pointer factory;
	factory = DataEntityImplFactoryHelper::FindFactoryByType( processingDataObj.type().name() );
	if ( factory.IsNull() )
	{
		return false;
	}

	// Add tag "Name" that is used by CMGUI impl
	blTag::Pointer nameTag = GetMetadata()->FindTagByName( "Name" );
	if ( nameTag.IsNotNull() )
	{
		factory->GetProperties()->AddTag( nameTag->GetName(), nameTag->GetValue() );
	}

	// Create new DataEntityImpl
	DataEntityImpl::Pointer newData = DataEntityImpl::SafeDownCast( factory->CreateInstance() );
	newData->SetDataPtr( processingDataObj );
	newData->SetRealTime( realTime );

	// Try to add a single time step
	bool success = false;
	if ( factory->GetProperties()->FindTagByName( "single" ) )
	{
		// If it fails-> throw an exception
		try
		{
			m_DataEntityImpl->SetAt( m_DataEntityImpl->GetSize(), newData );
			success = true;
		}
		catch (...)
		{
		}
	}

	// Failed because it's multiple or the type is not correct
	if ( !success )
	{
		// Add a multiple data type only if it's empty
		if ( factory->GetProperties()->FindTagByName( "multiple" ) && 
			GetNumberOfTimeSteps() == 0 )
		{
			m_DataEntityImpl = newData;
			success = true;
		}
	}

	if ( !success )
	{
		return false;
	}

	// Data entity modified to update rendering representation
	Modified();

	return true;
}

/**
*/
void DataEntity::SetTimeStep(
							 boost::any processingDataObj, 
							 TimeStepIndex timeStep,
							 RealTime realTime )
{
	BaseFactory::Pointer factory;
	factory = DataEntityImplFactoryHelper::FindFactoryByType( processingDataObj.type().name() );
	if ( factory.IsNull() )
	{
		return;
	}

	// Add tag "Name" that is used by CMGUI impl
	blTag::Pointer nameTag = GetMetadata()->FindTagByName( "Name" );
	if ( nameTag.IsNotNull() )
	{
		factory->GetProperties()->AddTag( nameTag->GetName(), nameTag->GetValue() );
	}

	DataEntityImpl::Pointer data = DataEntityImpl::SafeDownCast( factory->CreateInstance() );
	data->SetDataPtr( processingDataObj );
	data->SetRealTime( realTime );
	m_DataEntityImpl->SetAt( timeStep, data );

	Modified();
}

bool Core::DataEntity::SwitchImplementation( const std::string &datatypename )
{
	if ( m_DataEntityImpl->IsValidType( datatypename ) )
	{
		return true;
	}

	BaseFactory::Pointer factory;
	factory = DataEntityImplFactoryHelper::FindFactoryBySimilarType( datatypename );
	if ( factory.IsNull() )
	{
		return false;
	}

	// Add tag "Name" that is used by CMGUI impl
	blTag::Pointer nameTag = GetMetadata()->FindTagByName( "Name" );
	if ( nameTag.IsNotNull() )
	{
		factory->GetProperties()->AddTag( nameTag->GetName(), nameTag->GetValue() );
	}

	// Create new DataEntityImpl
	if ( factory->GetProperties()->FindTagByName( "multiple" ) )
	{
		DataEntityImpl::Pointer newData = DataEntityImpl::SafeDownCast( factory->CreateInstance() );
		newData->DeepCopy( m_DataEntityImpl );
		m_DataEntityImpl = newData;
		m_RenderingDataList.clear();
		Modified();
		return true;
	}
	else if ( factory->GetProperties()->FindTagByName( "single" ) )
	{
		// Type matches for the time steps ?
		if ( m_DataEntityImpl->GetSize() )
		{
			// Sometimes the switch is using "vtkPolyData" and not the complete type name
			std::string name = m_DataEntityImpl->GetAt( 0 )->GetDataPtr().type().name();
			if ( name.find( datatypename ) != std::string::npos )
			{
				return true;
			}
		}

		// Switch to multiple
		MultipleDataEntityImpl::Pointer multiple;
		multiple = dynamic_cast<MultipleDataEntityImpl*> ( m_DataEntityImpl.GetPointer() );
		if ( multiple.IsNull() )
		{
			multiple = MultipleDataEntityImpl::New();
			multiple->SwitchImplementation( datatypename );
			multiple->DeepCopy( m_DataEntityImpl );
		}
		else
		{
			multiple->SwitchImplementation( datatypename );
		}
		m_DataEntityImpl = multiple;
		m_RenderingDataList.clear();
		Modified();
		return true;

	}

	return false;
}

void Core::DataEntity::GetProcessingData( 
	boost::any &anyProcessingData, 
	TimeStepIndex timeStep, 
	bool switchImplementation /*= false */,
	bool generateNewInstance /*= false */ )
{
	// Find factory
	BaseFactory::Pointer factory;
	factory = DataEntityImplFactoryHelper::FindFactoryByType( anyProcessingData.type().name() );
	if ( factory.IsNull() )
	{
		throw Core::Exceptions::Exception( 
			"GetProcessingData", "Factory not found" );
	}

	bool validTimeStepIndex = timeStep < this->m_DataEntityImpl->GetSize() && timeStep >= 0;
	if ( factory->GetProperties()->FindTagByName( "single" ) && !validTimeStepIndex )
	{
		throw Core::Exceptions::Exception( 
			"GetProcessingData", "Not valid time step" );
	}

	// Switch implementation
	if ( switchImplementation )
	{
		SwitchImplementation( anyProcessingData.type() );
	}

	// Get the pointer to the data
	if ( !generateNewInstance )
	{
		if ( factory->GetProperties()->FindTagByName( "single" ) )
		{
			anyProcessingData = m_DataEntityImpl->GetAt( timeStep )->GetDataPtr( );
		}
		else if ( factory->GetProperties()->FindTagByName( "multiple" ) )
		{
			anyProcessingData = m_DataEntityImpl->GetDataPtr( );
		}
	} 
	else
	{
		DataEntityImpl::Pointer newData = DataEntityImpl::SafeDownCast( factory->CreateInstance() );

		// Create a copy
		if ( factory->GetProperties()->FindTagByName( "single" ) )
		{
			newData->DeepCopy( GetTimeStep( int( timeStep ) ) );
			anyProcessingData = newData->GetDataPtr();
		}
		else if ( factory->GetProperties()->FindTagByName( "multiple" ) )
		{
			newData->DeepCopy( m_DataEntityImpl );
			anyProcessingData = newData->GetDataPtr();
		}
	}

}

bool Core::DataEntity::SwitchImplementation( const std::type_info &type )
{
	return SwitchImplementation( type.name() );
}

/**
*/
boost::any DataEntity::GetProcessingData(TimeStepIndex timeStep)
{
	DataEntityImpl::Pointer data;
	if ( timeStep == -1 )
	{
		data = m_DataEntityImpl;
	}
	else
	{
		data = m_DataEntityImpl->GetAt( timeStep );
	}

	if ( data.IsNull() )
	{
		throw Core::Exceptions::Exception( 
			"GetProcessingData", "Time step is NULL" );
	}

	return data->GetDataPtr();
}

Core::DataEntityImpl::Pointer Core::DataEntity::GetTimeStep(
	int timeStepIndex /*= -1*/ )
{
    // If the processing data is empty, return an empty MultipleDataEntityImpl
	if ( timeStepIndex == -1 || size_t( timeStepIndex ) >= m_DataEntityImpl->GetSize() )
	{
		return m_DataEntityImpl;
	}
	return m_DataEntityImpl->GetAt( timeStepIndex );
}


Core::TimeStepIndex Core::DataEntity::FindTimeStep( Core::RealTime time )
{
	Core::TimeStepIndex timeStep = 0;
	bool found = false;
	while ( !found && timeStep < GetNumberOfTimeSteps() )
	{
		if ( time < GetTimeAtTimeStep( timeStep ) )
		{
			found = true;
		}
		else
		{
			timeStep++;
		}
	}

	return timeStep;
}

bool Core::DataEntity::Resize( int size, const std::type_info &type )
{
	BaseFactory::Pointer factory;
	factory = DataEntityImplFactoryHelper::FindFactoryByType( type.name() );
	if ( factory.IsNull() )
	{
		return false;
	}

	// Add tag "Name" that is used by CMGUI impl
	blTag::Pointer nameTag = GetMetadata()->FindTagByName( "Name" );
	if ( nameTag.IsNotNull() )
	{
		factory->GetProperties()->AddTag( nameTag->GetName(), nameTag->GetValue() );
	}

	if ( factory->GetProperties()->FindTagByName( "single" ) )
	{
		MultipleDataEntityImpl::Pointer multiple = MultipleDataEntityImpl::New();
		for ( size_t i = 0 ; i < size_t( size ) ; i++ )
		{
			// Create a new implementation
			DataEntityImpl::Pointer impl = DataEntityImpl::SafeDownCast( factory->CreateInstance() );

			// If types are the same -> reuse previous data
			if ( m_DataEntityImpl.IsNotNull() && 
				 i < m_DataEntityImpl->GetSize() &&
				 m_DataEntityImpl->GetAt( i )->GetDataPtr().type() == impl->GetDataPtr().type() )
			{
				impl = m_DataEntityImpl->GetAt( i );
			}

			multiple->SetAt( i, impl );
		}
		m_DataEntityImpl = multiple;
	}
	else if ( factory->GetProperties()->FindTagByName( "multiple" ) )
	{
		DataEntityImpl::Pointer multiple;
		multiple = DataEntityImpl::SafeDownCast( factory->CreateInstance() );
		multiple->SetSize( size );
		m_DataEntityImpl = multiple;
	}

	return true;
}

/**
*/
void DataEntity::ResetTimeSteps()
{
	m_DataEntityImpl->ResetData();
}

void DataEntity::Copy( 
	const DataEntity::Pointer& de, ImportMemoryManagementType mem /*= gmCopyMemory*/ )
{

	m_DataEntityImpl->DeepCopy( de->m_DataEntityImpl, mem );

	// Data entity modified to update rendering representation
	Modified();
}

void DataEntity::CopyDataPtr( const DataEntity::Pointer& de )
{
	// If the DataEntity is the same type -> Copy multiple pointer
	if ( GetTimeStep( -1 )->GetDataPtr().type() == 
		 de->GetTimeStep( -1 )->GetDataPtr().type() )
	{
		boost::any anyData = de->GetTimeStep( -1 )->GetDataPtr();
		GetTimeStep( -1 )->SetDataPtr( anyData );
	}
	else
	{
		m_DataEntityImpl->SetSize( de->GetNumberOfTimeSteps( ) );

		// Copy multiple time steps
		for ( TimeStepIndex iTimeStep = 0 ; iTimeStep < de->GetNumberOfTimeSteps() ; iTimeStep++ )
		{
			boost::any anyData = de->GetProcessingData( iTimeStep );
			SetTimeStep( anyData, iTimeStep );
		}
	}

	Modified( );
}

void DataEntity::MoveDataImpl( const DataEntity::Pointer de )
{
	m_DataEntityImpl = de->m_DataEntityImpl;
	de->m_DataEntityImpl = MultipleDataEntityImpl::New( );

	Modified( );
}

std::string DataEntity::GetMemoryOwner( )
{
	return m_DataEntityImpl->GetMemoryOwner( );
}

void DataEntity::Clean( )
{
	m_DataEntityImpl->ResetData( );
	m_RenderingDataList.clear();
	Modified();
}

#ifdef WIN32 
#pragma warning(pop)
#endif

/**
*/
namespace TestCompilation
{
	void Test()
	{
		double processingData;
		Core::DataEntity::Pointer p;
		p->GetProcessingData(processingData, 0);
	}
}

