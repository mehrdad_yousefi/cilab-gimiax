/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "gmCommonObjectsWin32Header.h"
#include "assert.h"
#include "boost/bind.hpp"
#include "boost/signals.hpp"
#include <cstring>
#include <exception>
#include <itkLightObject.h>
#include <itkSmartPointer.h>
#include <list>
#include <set>
#include <string>
#include <string.h>
#include <typeinfo>
#include <vector>

