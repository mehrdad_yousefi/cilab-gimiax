/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDataHolderBase.h"
#include "coreAssert.h"

Core::DataHolderBase::DataHolderBase(void)
{
	m_EnableNotification = true;
}

Core::DataHolderBase::~DataHolderBase()
{
	if ( m_EnableNotification )
	{
		this->m_DataHolderDestructSignal();
	}
}

void Core::DataHolderBase::NotifyObservers() const
{
	if ( m_EnableNotification )
	{
		this->Modified();
		this->m_DataModifiedSignalOrNewSubject( );
		this->m_DataModifiedSignal();
	}
}

Core::DataHolderBase::SignalType* 
Core::DataHolderBase::GetSignal( int _eventType )
{
	SignalType* signal;
	switch( _eventType )
	{
	case DH_NEW_SUBJECT: signal = &m_NewDataSignal; break;
	case DH_SUBJECT_MODIFIED:signal = &m_DataModifiedSignal;break;
	case DH_SUBJECT_MODIFIED_OR_NEW_SUBJECT: signal = &m_DataModifiedSignalOrNewSubject;break;
	case DH_DESTRUCTED: signal = &m_DataHolderDestructSignal; break;
	}

	return signal;
}

std::string Core::DataHolderBase::GetName() const
{
	return m_Name;
}

void Core::DataHolderBase::SetName( std::string val )
{
	m_Name = val;
}

bool Core::DataHolderBase::GetEnableNotification() const
{
	return m_EnableNotification;
}

void Core::DataHolderBase::SetEnableNotification( bool val )
{
	m_EnableNotification = val;
}
