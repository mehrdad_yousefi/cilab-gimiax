/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


template<>
void DataHolder<Core::SmartPointerObject::Pointer>::SetSubject( const Core::SmartPointerObject::Pointer& data, bool bForceNotification )
{
	if ( m_Data.IsNotNull( ) )
	{
		m_Data->RemoveObserverOnModified< DataHolder<Core::SmartPointerObject::Pointer> >( 
			this,
			&DataHolder<Core::SmartPointerObject::Pointer>::OnModifiedSubject );
	}

	doSetSubject( data, bForceNotification );

	if ( data.IsNotNull( ) )
	{
		data->AddObserverOnModified< DataHolder<Core::SmartPointerObject::Pointer> >( 
			this,
			&DataHolder<Core::SmartPointerObject::Pointer>::OnModifiedSubject );
	}
}


