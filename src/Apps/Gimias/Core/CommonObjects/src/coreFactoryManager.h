/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreFactoryManager_H
#define _coreFactoryManager_H

#include "gmCommonObjectsWin32Header.h"
#include "coreBaseFactory.h"
#include "coreException.h"
#include "coreCreateExceptionMacros.h"
#include "boost/thread/mutex.hpp"

#if BOOST_VERSION>105200
#include <boost/thread/lock_guard.hpp>
#endif

namespace Core
{

/**
\brief Manages all instances of BaseFactory

Is possible to register several factories using the same name. For example:

\code
Core::FactoryManager::Register( OutputUpdater::GetNameClass( ), DefaultOutputUpdater::Factory::New() );
Core::FactoryManager::Register( OutputUpdater::GetNameClass( ), WxOutputUpdater::Factory::New() );
\endcode

Each Factory has a unique identifier that can be used to unregister it:
Core::FactoryManager::UnRegister(
	Core::FactoryManager::FindByInstanceClassName( SSHPluginProvider::GetNameClass() ) );

\ingroup gmCommonObjects
\author Xavi Planes
\date Jan 2011
*/
class GMCOMMONOBJECTS_EXPORT FactoryManager : public Object
{
public:
	coreClassNameMacro(FactoryManager)


	//!
	typedef std::multimap<std::string, BaseFactory::Pointer> FactoryMapType;

	//!
	typedef std::list<BaseFactory::Pointer> FactoryListType;

	/** Register a factory using a name.
	*/
	static void Register( const std::string &name, BaseFactory* factory );

	/** Register a factory that has the members GetNameOfClass( ) and
	CreateAnother( )
	*/
	static void UnRegister( BaseFactory* factory );

	//! Helper function
	static void UnRegisterByInstanceClassName( const std::string &classname );

	/**
	Generic factory function that creates an object matching a set of properties
	*/
	static SmartPointerObject::Pointer CreateInstance(
		const std::string &name,
		blTagMap::Pointer properties = NULL )
			throw (Exceptions::FactoryNotFoundException);

	//!
	static FactoryMapType GetFactories();

	/** Find a registered factory with "name" and other properties
	Properties are compared using blTagMap::Compare() member function
	*/
	static BaseFactory::Pointer Find(
		const std::string &name, blTagMap::Pointer properties = NULL );

	//! Find a factory using Instance class name
	static BaseFactory::Pointer FindByInstanceClassName( const std::string &classname );

	/**
	Find all factories that matches a concrete name
	*/
	static FactoryListType FindAll(
		const std::string &name, blTagMap::Pointer properties = NULL );

private:
	//!
	static FactoryMapType m_Factories;

	//! Mutex to protect access to m_Factories
	static boost::mutex m_FactoriesMutex;
};

}

#endif // _coreFactoryManager_H
