/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreFactoryManager.h"
#include <strstream>
#include "coreException.h"

Core::FactoryManager::FactoryMapType Core::FactoryManager::m_Factories;
boost::mutex Core::FactoryManager::m_FactoriesMutex; 

Core::SmartPointerObject::Pointer 
Core::FactoryManager::CreateInstance( 
	const std::string &name, blTagMap::Pointer properties )
	throw (Core::Exceptions::FactoryNotFoundException)
{
	BaseFactory::Pointer factory = Find( name, properties );
	if ( factory.IsNull() )
	{
		throw Exceptions::FactoryNotFoundException( "FactoryManager::CreateInstance" );
	}

	return factory->CreateInstance();
}

void Core::FactoryManager::Register( const std::string &name, BaseFactory* factory )
{
	if ( Find( name, factory->GetProperties() ).IsNull( ) )
	{
		boost::lock_guard<boost::mutex> lock_g(m_FactoriesMutex); 
		m_Factories.insert( FactoryMapType::value_type( name, factory ) );
	}
}

Core::BaseFactory::Pointer 
Core::FactoryManager::Find( const std::string &name, blTagMap::Pointer properties )
{
	boost::lock_guard<boost::mutex> lock_g(m_FactoriesMutex); 

	// If properties is NULL, create an empty properties
	if ( properties.IsNull( ) )
	{
		properties = blTagMap::New( );
	}
	
	// Search
	BaseFactory::Pointer factory;
	std::pair<FactoryMapType::iterator,FactoryMapType::iterator> range;
	range = m_Factories.equal_range( name );
	FactoryMapType::iterator it = range.first;
	while ( factory.IsNull() && it != range.second )
	{
		// If the factory properties is empty, it will return true when
		// comparing with an empty properties
		// If the factory properties is not empty, will return false
		if ( properties->Compare( it->second->GetProperties() ) )
		{
			factory = it->second;
		}
		it++;
	}

	return factory;
}

Core::FactoryManager::FactoryListType Core::FactoryManager::FindAll( 
	const std::string &name, blTagMap::Pointer properties /*= NULL*/ )
{
	boost::lock_guard<boost::mutex> lock_g(m_FactoriesMutex); 

	FactoryListType list;

	// If properties is NULL, create an empty properties
	if ( properties.IsNull( ) )
	{
		properties = blTagMap::New( );
	}

	// Search
	std::pair<FactoryMapType::iterator,FactoryMapType::iterator> range;
	if ( name.empty( ) )
	{
		range.first = m_Factories.begin( );
		range.second = m_Factories.end( );
	}
	else
	{
		range = m_Factories.equal_range( name );
	}
	FactoryMapType::iterator it;
	for ( it = range.first ; it != range.second ; it++ )
	{
		// If the factory properties is empty, it will return true when
		// comparing with an empty properties
		// If the factory properties is not empty, will return false
		if ( properties->Compare( it->second->GetProperties() ) )
		{
			list.push_back( it->second );
		}
	}

	return list;
}

Core::BaseFactory::Pointer Core::FactoryManager::FindByInstanceClassName( 
	const std::string &classname )
{
	boost::lock_guard<boost::mutex> lock_g(m_FactoriesMutex); 

	BaseFactory::Pointer factory;
	FactoryMapType::iterator it = m_Factories.begin( );
	while ( factory.IsNull() && it != m_Factories.end() )
	{
		if ( it->second->GetInstanceClassName() == classname )
		{
			factory = it->second;
		}
		it++;
	}

	return factory;
}

Core::FactoryManager::FactoryMapType Core::FactoryManager::GetFactories()
{
	return m_Factories;
}

void Core::FactoryManager::UnRegister( BaseFactory* factory )
{
	boost::lock_guard<boost::mutex> lock_g(m_FactoriesMutex); 

	FactoryMapType::iterator it;
	for ( it = m_Factories.begin() ; it != m_Factories.end() ; it++ )
	{
		if ( it->second == factory )
		{
			break;
		}
	}

	if ( it != m_Factories.end() )
	{
		m_Factories.erase( it );
	}
}

void Core::FactoryManager::UnRegisterByInstanceClassName( const std::string &classname )
{
	BaseFactory::Pointer baseFactory;
	baseFactory = FindByInstanceClassName( classname );
	UnRegister( baseFactory );
}


