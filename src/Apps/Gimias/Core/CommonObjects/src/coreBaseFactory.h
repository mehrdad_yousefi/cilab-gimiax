/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreBaseFactory_H
#define _coreBaseFactory_H

#include "gmCommonObjectsWin32Header.h"
#include "coreCommonMacros.h"
#include "coreSmartPointerMacros.h"
#include "coreObject.h"

namespace Core
{

/** 
\brief Generic interface for all factories. 

It has an abstract function CreateInstance( ) that the sublcass needs to
implement.

It uses m_Properties to set the properties of this factory related to factories
that implement the same interface.

\ingroup gmCommonObjects
\author Xavi Planes
\date Jan 2011
*/
class GMCOMMONOBJECTS_EXPORT BaseFactory : public SmartPointerObject
{
public:
	coreDeclareSmartPointerTypesMacro(Core::BaseFactory, SmartPointerObject)
	coreClassNameMacro(Core::BaseFactory)

	//!
	BaseFactory( );

	//! 
	virtual ~BaseFactory( );

	//! Create a new instance
	virtual SmartPointerObject::Pointer CreateInstance( ) = 0;

	//! Get Unique Identifier
	virtual std::string GetInstanceClassName( ) = 0;

	//! Add tag "InstanceClassName" with the value GetInstanceClassName( )
	virtual void AddTags( );

private:
};

}


#define coreDefineFactoryClass( className ) \
	class Factory : public Core::BaseFactory \
	{ \
	public: \
		coreDeclareSmartPointerClassMacro( Factory, Core::BaseFactory ); \
		virtual SmartPointerObject::Pointer CreateInstance( ) \
		{ \
			SmartPointerObject::Pointer object = className::New().GetPointer(); \
			object->GetProperties( )->AddTag( "NameOfClass", std::string( object->GetNameOfClass() ) ); \
			return object; \
		} \
		virtual std::string GetInstanceClassName( ) \
		{ \
			return className::GetNameClass( ); \
		} \
	private: \
		Factory( ) \
		{ \
			AddTags( ); \
		} \

#define coreDefineFactoryTagsBegin( ) \
		void AddTags( ) \
		{ \
			BaseFactory::AddTags( ); 

#define coreDefineFactoryAddTag( name, value ) \
			m_Properties->AddTag( name, value );

#define coreDefineFactoryTagsEnd( ) \
		} \

#define coreDefineFactoryClassEnd( ) \
	};

#define coreDefineFactoryClass1param( className, param1 ) \
	class Factory : public Core::BaseFactory \
	{ \
	public: \
		coreDeclareSmartPointerClassMacro1Param( Factory, Core::BaseFactory, param1 ); \
		virtual SmartPointerObject::Pointer CreateInstance( ) \
		{ \
			SmartPointerObject::Pointer object = className::New( m_p1 ).GetPointer(); \
			object->GetProperties( )->AddTag( "NameOfClass", std::string( object->GetNameOfClass() ) ); \
			return object; \
		} \
		virtual std::string GetInstanceClassName( ) \
		{ \
			return className::GetNameClass( ); \
		} \
	protected: \
		param1 m_p1; \
	private: \
		Factory( param1 p1 ) \
		{ \
			m_p1 = p1; \
			AddTags( ); \
		} \

/** 
\def coreDefineFactory
\ingroup gmCommonObjects
Define a Factory to create object of type className and add a property "instance"
to the m_Properties member variable
*/
#define coreDefineFactory(className) \
	coreDefineFactoryClass(className) \
	coreDefineFactoryTagsBegin() \
	coreDefineFactoryTagsEnd() \
	coreDefineFactoryClassEnd()

/** 
\def coreDefineFactory
\ingroup gmCommonObjects
Define a Factory to create object of type className and add a property "instance"
to the m_Properties member variable
*/
#define coreDefineFactory1param(className,param1) \
	coreDefineFactoryClass1param(className,param1) \
	coreDefineFactoryTagsBegin() \
	coreDefineFactoryTagsEnd() \
	coreDefineFactoryClassEnd()

#endif // _coreBaseFactory_H
