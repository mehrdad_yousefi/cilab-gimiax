/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef CORECOMMONMACROS_H
#define CORECOMMONMACROS_H


#ifdef coreClassNameMacro
#undef coreClassNameMacro
#endif

//! Preserved for legacy reasons.
#define coreClassNameMacro(classname) \
	virtual const char* GetNameOfClass(void) const { return #classname; } \
	static const char* GetNameClass(void) { return #classname; }

#endif //CORECOMMONMACROS_H
