/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreObject.h"
#include "coreException.h"
#include "coreBaseExceptions.h"

using namespace Core;

Core::Object::~Object()
{

}

blTagMap::Pointer Core::Object::GetProperties()
{
	return m_Properties;
}

Core::Object::Object()
{
	m_Properties = blTagMap::New( );
}

blTag::Pointer Core::Object::FindProperty( const std::string &name )
{
	blTag::Pointer tag = m_Properties->FindTagByName( name );
	if ( tag.IsNull() )
	{
		throw Exceptions::TagNotFoundException( "Object::SafeFindProperty", name.c_str() );
	}
	return tag;
}

Core::SmartPointerObject::SmartPointerObject( void ) : itk::LightObject()
{

}

Core::SmartPointerObject::~SmartPointerObject( void )
{

}

/**
* Return the modification for this object.
*/
unsigned long Core::SmartPointerObject::GetMTime() const
{
	return m_MTime.GetMTime();
}


/**
* Make sure this object's modified time is greater than all others.
*/
void
Core::SmartPointerObject::Modified() const
{
	m_MTime.Modified();
	m_OnModified( );
	m_DataModifiedSignal1( (SmartPointerObject*) this );
}
