/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "coreBoostWorkflowReader.h"
#include "coreWorkflowSerialize.h"
#include "blTextUtils.h"

#include <fstream>
#include <boost/archive/xml_iarchive.hpp>


Core::BoostWorkflowReader::BoostWorkflowReader() 
{
}

Core::BoostWorkflowReader::~BoostWorkflowReader()
{
}

void Core::BoostWorkflowReader::InternalUpdate()
{
	std::string ext = blTextUtils::GetFilenameLastExtension( m_Filename );
	if ( ext != ".xml" )
	{
		return;
	}

	std::ifstream file( m_Filename.c_str() );
	if ( !file.is_open() )
	{
		return;
	}

	try
	{
		m_Data = Core::Workflow::New( "" );
		boost::archive::xml_iarchive xmlArchive(file);
		using boost::serialization::make_nvp;
		xmlArchive >> BOOST_SERIALIZATION_NVP( m_Data );
		file.close();
	}
	catch( ... )
	{
		m_Data = NULL;
	}
}
