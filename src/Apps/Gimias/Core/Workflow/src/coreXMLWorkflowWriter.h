/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef XMLWorkflowWriter_H
#define XMLWorkflowWriter_H

#include "coreBaseWorkflowIO.h"

class TiXmlNode;

namespace Core{

/**
XML Writer for Workflow

Please see XMLWorkflowReader for the format of the XML file.

\author Xavi Planes
\date Nov 2010
\ingroup gmFrontEndPlugin
*/
class XMLWorkflowWriter : public BaseWorkflowIO 
{
public:
	coreDeclareSmartPointerClassMacro( Core::XMLWorkflowWriter, BaseWorkflowIO );
	
public:

protected:
	XMLWorkflowWriter( );

	~XMLWorkflowWriter( );

	//!
	void InternalUpdate( );

	//!
	void SaveData( TiXmlNode* node, Workflow::Pointer workflow );
private:

};

} // Core

#endif //blSignalWriter_H
