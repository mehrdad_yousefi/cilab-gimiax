/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "coreWorkflowReader.h"
#include "coreXMLWorkflowReader.h"
#include "coreBoostWorkflowReader.h"

Core::WorkflowReader::WorkflowReader() 
{
}

Core::WorkflowReader::~WorkflowReader()
{
}

void Core::WorkflowReader::InternalUpdate()
{
	m_Data = NULL;

	XMLWorkflowReader::Pointer reader = XMLWorkflowReader::New( );
	reader->SetFilename( m_Filename.c_str() );
	reader->Update();
	m_Data = reader->GetOutput();

	if ( m_Data.IsNull() )
	{
		BoostWorkflowReader::Pointer boostReader = BoostWorkflowReader::New( );
		boostReader->SetFilename( m_Filename.c_str() );
		boostReader->Update();
		m_Data = boostReader->GetOutput();
	}
}
