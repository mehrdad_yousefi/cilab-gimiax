/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _WorkflowReader_H
#define _WorkflowReader_H

#include "coreBaseWorkflowIO.h"

namespace Core{

/**
Reader for a Workflow

\author Xavi Planes
\date Nov 2010
\ingroup gmFrontEndPlugin
*/
class GMWORKFLOW_EXPORT WorkflowReader : public BaseWorkflowIO 
{
public:
	coreDeclareSmartPointerClassMacro( Core::WorkflowReader, BaseWorkflowIO );
	
protected:
	WorkflowReader( );

	~WorkflowReader( );

	//!
	virtual void InternalUpdate( );

protected:
};

} // Core

#endif //_WorkflowReader_H
