/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _WorkflowWriter_H
#define _WorkflowWriter_H

#include "coreBaseWorkflowIO.h"

namespace Core{

/**
Writer for Workflow

\author Xavi Planes
\date Nov 2010
\ingroup gmFrontEndPlugin
*/
class GMWORKFLOW_EXPORT WorkflowWriter : public BaseWorkflowIO 
{
public:
	coreDeclareSmartPointerClassMacro( Core::WorkflowWriter, BaseWorkflowIO );


protected:
	WorkflowWriter( );

	~WorkflowWriter( );

	//!
	virtual void InternalUpdate( );

protected:
};

} // Core

#endif //_WorkflowWriter_H
