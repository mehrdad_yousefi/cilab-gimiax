/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreWorkflowSubStep.h"

Core::WorkflowSubStep::WorkflowSubStep( const char* name )
{
	SetName( name );
}

std::string Core::WorkflowSubStep::GetName() const
{
	return m_Name;
}

void Core::WorkflowSubStep::SetName( std::string val )
{
	m_Name = val;
	AddAlternative( val );
}

Core::WorkflowSubStep::AlternativesListType& Core::WorkflowSubStep::GetAlternatives()
{
	return m_Alternatives;
}

void Core::WorkflowSubStep::AddAlternative( std::string name )
{
	AlternativesListType::iterator it;
	it = std::find( m_Alternatives.begin(), m_Alternatives.end(), name );
	if ( it != m_Alternatives.end() )
	{
		return;
	}

	m_Alternatives.push_back( name );
}
