/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "coreBaseWorkflowIO.h"


Core::BaseWorkflowIO::BaseWorkflowIO() 
{
	m_Version = 0;
}

Core::BaseWorkflowIO::~BaseWorkflowIO()
{
}

void Core::BaseWorkflowIO::SetFilename( const char* filename )
{
	m_Filename = filename;
}

void Core::BaseWorkflowIO::SetInput( Workflow::Pointer workflow )
{
	m_Data = workflow;
}

void Core::BaseWorkflowIO::Update()
{
	InternalUpdate();
}

Core::Workflow::Pointer Core::BaseWorkflowIO::GetOutput()
{
	return m_Data;
}

