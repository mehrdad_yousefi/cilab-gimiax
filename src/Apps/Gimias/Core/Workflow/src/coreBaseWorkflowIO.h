/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _BaseWorkflowIO_H
#define _BaseWorkflowIO_H

#include "gmWorkflowWin32Header.h"
#include "coreObject.h"
#include "coreWorkflow.h"

namespace Core
{

/**
Base reader/writer for all Workflow reader/writers. 

\author Xavi Planes
\date Nov 2010
\ingroup gmFrontEndPlugin
*/
class GMWORKFLOW_EXPORT BaseWorkflowIO : public SmartPointerObject
{
public:
	coreDeclareSmartPointerTypesMacro(Core::BaseWorkflowIO,SmartPointerObject)
	coreClassNameMacro(Core::BaseWorkflowIO)
	
public:

	//! Set the complete filename 
	void SetFilename( const char* );

	//!
	void SetInput( Workflow::Pointer workflow );

	//! execute
	virtual void Update( );

	//! Returns the output of the reader
	Workflow::Pointer GetOutput( );


protected:
	BaseWorkflowIO( );

	~BaseWorkflowIO( );

	//!
	virtual void InternalUpdate( ) = 0;

protected:

	//!
	Workflow::Pointer m_Data;

	//!
	std::string m_Filename;

	//!
	unsigned int m_Version;
};

} // Core

#endif //_BaseWorkflowIO_H
