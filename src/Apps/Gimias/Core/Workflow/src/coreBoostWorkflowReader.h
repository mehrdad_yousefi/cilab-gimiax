/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _BoostWorkflowReader_H
#define _BoostWorkflowReader_H

#include "coreBaseWorkflowIO.h"

namespace Core{

/**
XML Boost reader using serialize. This reader is deprecated because
the format of the XML file is difficult to read for humans.
Please use XMLWorkflowReader.

\code
<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<!DOCTYPE boost_serialization>
<boost_serialization signature="serialization::archive" version="5">
<workflow class_id="0" tracking_level="0" version="2">
	<Name>AngioImages</Name>
	<Version>0.0.0</Version>
	<StepVectorSize>3</StepVectorSize>
	<WorkflowStep class_id="1" tracking_level="0" version="1">
		<Name>Visualize &amp; Crop</Name>
		<WindowList class_id="2" tracking_level="0" version="0">
...
\endcode

\author Xavi Planes
\date Nov 2010
\ingroup gmFrontEndPlugin
*/
class BoostWorkflowReader : public BaseWorkflowIO 
{
public:
	coreDeclareSmartPointerClassMacro( Core::BoostWorkflowReader, BaseWorkflowIO );

protected:
	BoostWorkflowReader( );

	~BoostWorkflowReader( );

	//!
	void InternalUpdate();

private:

};

} // Core

#endif //_BoostWorkflowReader_H
