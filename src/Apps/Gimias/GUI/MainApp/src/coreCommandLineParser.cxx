/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include "wx/wxprec.h"

#ifndef WX_PRECOMP
       #include "wx/wx.h"
#endif

#include "coreCommandLineParser.h"
#include "guicon.h"
#include "coreDataEntityReader.h"
#include "coreBaseDataEntityReader.h"
#include "coreBaseIO.h"
#include "coreConfig.h" // for define DISABLE_FILE_LOADING

using namespace Core;


CommandLineParser::CommandLineParser( )
{
#ifndef DISABLE_FILE_LOADING
	AddOption("", CmdLineOption::FILENAME, "Load filename");
	AddOption("", CmdLineOption::SESSION, "Load session");
#endif
	AddSwitch("", CmdLineOption::COMPILER_VERSION, "Compiler version");
	AddSwitch("", CmdLineOption::ARCHITECTURE_VERSION, "Architecture version");
	AddSwitch("", CmdLineOption::CONSOLE_WIN, "Show console window");
	AddSwitch("", CmdLineOption::GM_VERSION, "Show version");
	AddOption("", CmdLineOption::LOAD_CONFIG, "Load GIMIAS configuration file and overwrite the loaded options");
	AddOption("", CmdLineOption::DOWNLOAD, "Download files from Remote server");
	AddOption("", CmdLineOption::UPLOAD, "Upload files to Remote server");
	AddSwitch("", CmdLineOption::STOP, "Stop Gimias after loading all plugins and executing command line arguments");

	m_argc = 0;
	m_argv = NULL;
}

bool CommandLineParser::PraseInitialArguments( )
{
	bool success = true;

	try
	{
		wxString value;
		if ( Found(CmdLineOption::COMPILER_VERSION) )
		{
			#ifdef _MSC_VER
				#if (_MSC_VER >= 1600) && (_MSC_VER < 1700)
					std::cout << "VS2010" << std::endl;
				#elif (_MSC_VER >= 1500) && (_MSC_VER < 1600)
					std::cout << "VS2008" << std::endl;
				#elif (_MSC_VER >= 1400) && (_MSC_VER < 1500)
					std::cout << "VS2005" << std::endl;
				#elif (_MSC_VER >= 1300) && (_MSC_VER < 1400)
					std::cout << "VS2003" << std::endl;
				#endif
			#else //_MSC_VER
				std::cout << "Unknown" << std::endl;
			#endif //_MSC_VER

			success = false;
		}
		if ( Found(CmdLineOption::ARCHITECTURE_VERSION) )
		{
			#ifdef _MSC_VER
				#ifdef _WIN64
					std::cout << "64 bits" << std::endl;
				#else // _WIN64
					std::cout << "32 bits" << std::endl;
				#endif // _WIN64
			#else // _MSC_VER
				std::cout << "Unknown" << std::endl;
			#endif // _MSC_VER

			success = false;
		}
		if ( Found( CmdLineOption::CONSOLE_WIN ) )
		{
			// Nothing
		}
		if ( Found( CmdLineOption::GM_VERSION ) )
		{
			Core::Runtime::Settings::Pointer settings = Core::Runtime::Settings::New( m_argv[ 0 ] );

			// Use the console of the parent of the current process.
			RedirectIOToConsole( true );

			std::cout << settings->GetKernelVersion() << std::endl;

			return false;
		}
		if ( Found( CmdLineOption::LOAD_CONFIG ) )
		{
			// Nothing to do
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro( PraseInitialArguments );

	return success;
}

bool CommandLineParser::ParseFinalArgments( )
{
	try
	{
		if ( Parse(false /* don't show usage */) != 0 )
		{
			return true;
		}

		wxString value;
#ifndef DISABLE_FILE_LOADING
		if ( Found(CmdLineOption::FILENAME, &value) )
		{
			Core::DataEntity::Pointer dataEntity;
			dataEntity = Core::DataEntityHelper::LoadDataEntity( value.ToStdString() );

			Core::DataEntityHolder::Pointer holder = Core::DataEntityHolder::New( );
			holder->SetSubject( dataEntity );
			RenderingTree::Pointer renderingTree;

			Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
			graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
			Core::BasePluginTab* tab;
			tab = graphicalIface->GetMainWindow()->GetCurrentPluginTab();
			if ( tab )
			{
				Core::DataTreeHelper::PublishOutput( 
					holder, 
					tab->GetRenderingTreeManager()->GetActiveTree() );
			}
		}
		
		if ( Found(CmdLineOption::SESSION, &value) )
		{
			Core::IO::SessionReader::Pointer sessionReader = Core::IO::SessionReader::New();
			sessionReader->SetFileName(value.ToStdString());
			sessionReader->Update();
		}
#endif
		
		if ( Found(CmdLineOption::DOWNLOAD, &value) )
		{
			Core::IO::DataEntityReader::Pointer downloadReader = Core::IO::DataEntityReader::New();
			downloadReader->SetFileName(value.ToStdString());
			downloadReader->Update();
		}
		
		if ( Found(CmdLineOption::UPLOAD, &value) )
		{
			Core::IO::DataEntityReader::Pointer uploadReader = Core::IO::DataEntityReader::New();
			uploadReader->SetFileName(value.ToStdString());
			uploadReader->Update();
		}

	}
	catch(Core::Exceptions::Exception& e) 
	{
		Found( CmdLineOption::STOP )? Core::LogException(e) : Core::LogExceptionWithLoggerAndGraphicalInterface(e);
	}
	catch(std::exception& stdEx)
	{
		Core::Exceptions::StdException e("CommandLineParser::ParseFinalArgments", stdEx);
		Found( CmdLineOption::STOP )? Core::LogException(e) : Core::LogExceptionWithLoggerAndGraphicalInterface(e);
	}
	catch(...)
	{
		Core::Exceptions::UnhandledException e("CommandLineParser::ParseFinalArgments");
		Found( CmdLineOption::STOP )? Core::LogException(e) : Core::LogExceptionWithLoggerAndGraphicalInterface(e);
	} 

	return true;
}

void CommandLineParser::SetCmdLine(int argc, char** argv)
{
	m_argc = argc;
	m_argv = argv;
	wxCmdLineParser::SetCmdLine(argc, argv);
}
