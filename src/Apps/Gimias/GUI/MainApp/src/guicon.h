/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef __GUICON_H__
#define __GUICON_H__

/**
Redirect Windows IO to a console window

\param [in] attach If true, attach to the parent console else create a new one

\ingroup gmMainApp
\author Xavi Planes
\date Jul 2011
*/
void RedirectIOToConsole( bool attach );

#endif

/* End of File */
