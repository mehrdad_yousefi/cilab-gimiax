/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include "wx/wxprec.h"

#ifndef WX_PRECOMP
       #include "wx/wx.h"
#endif

#include "coreKernel.h"
#include "coreMainApp.h"
#include "coreLogger.h"
#include "coreException.h"
#include "coreSettings.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreDataEntityBuildersRegistration.h"
#include "coreWxMitkCoreMainWindow.h"
#include "coreReportExceptionMacros.h"
#include "coreDataEntityHelper.h"
#include "coreDataTreeHelper.h"
#include "corePluginTab.h"
#include "coreSessionReader.h"
#include "guicon.h"

#include <wx/cmdline.h>
#include <iostream>
#include <stdio.h>
#include <string>
#ifdef _MSC_VER
	#include <new.h>

#endif //not MSVC

#include "wxMitkAbortEventFilter.h"

#include "wx/log.h"

#ifdef __WXMAC__
#include <Carbon/Carbon.h>
extern "C" { void CPSEnableForegroundOperation(ProcessSerialNumber* psn); }
#endif

using namespace Core;

IMPLEMENT_APP(MainApp)


/**
The startup initialization of the Kernel and other main objects
*/

//! this specify the exception handler of the memory allocation failure when _set_new_mode(1) (only on MSVC)
//seems that the function cannot be included in a namespace.. I don't know why!
int CISTIB_memory_failure(size_t size)
{
	throw std::bad_alloc( );
	return 0;
}

bool MainApp::OnInit(void)
{
	m_Clock.Start( );

	/* this hack enables to have a GUI on Mac OSX even if the
	 * program was called from the command line (and isn't a bundle) */
#ifdef __WXMAC__
	ProcessSerialNumber psn;
	GetCurrentProcess( &psn );
	CPSEnableForegroundOperation( &psn );
	SetFrontProcess( &psn );
#endif

	Core::Runtime::Logger::Pointer logger;

	try
	{
		//old_mode = _set_new_mode(1);
		//old_handler = _set_new_handler(CISTIB_memory_failure );
		//I don't use these returns, since I am not planning to restore the original values...
#ifdef _MSC_VER
			_set_new_mode(1);
			_set_new_handler(CISTIB_memory_failure );
#endif //not MSVC

		// Parse simple options like architecture version
#if wxUSE_CMDLINE_PARSER
		m_CommandLineParser.SetCmdLine(argc, argv);

		bool cont;
		switch ( m_CommandLineParser.Parse(false /* don't show usage */) )
		{
			case -1:
				cont = OnCmdLineHelp(m_CommandLineParser);
				break;

			case 0:
				cont = m_CommandLineParser.PraseInitialArguments( );
				break;

			default:
				cont = OnCmdLineError(m_CommandLineParser);
				break;
		}

		if ( !cont )
			return false;
#endif // wxUSE_CMDLINE_PARSER

		// Always show console window in debug mode
#if defined(_DEBUG)
		RedirectIOToConsole( false );
#else
		// show console if specified by command line option
		if ( m_CommandLineParser.Found( CmdLineOption::CONSOLE_WIN ) )
		{
			RedirectIOToConsole( false );
		}
#endif

		// Create main window
		wxWindow *mainWindow;
		mainWindow = new Core::Widgets::wxMitkCoreMainWindow(NULL, wxID_ANY, "");
		this->SetTopWindow( mainWindow );

		//
		m_Runtime = Core::Runtime::WxEnvironment::New();
		m_Runtime->InitApplication( argc, argv, Core::Runtime::Graphical );

		// Initialize Kernel
		Core::Widgets::BaseMainWindow* baseMainWindow;
		baseMainWindow = dynamic_cast<Core::Widgets::BaseMainWindow*> ( mainWindow );
		Core::Runtime::Kernel::Initialize( m_Runtime.GetPointer());

		wxString value;
		if ( m_CommandLineParser.Found( CmdLineOption::LOAD_CONFIG, &value ) )
		{
			Core::Runtime::Kernel::GetApplicationSettings()->LoadSettings( value.c_str() );
		}

		if ( m_CommandLineParser.Found( CmdLineOption::STOP ) )
		{
			m_Runtime->Exit( );
		}

		// Init graphical interface
		Core::Runtime::Kernel::InitializeGraphicalInterface( baseMainWindow, Core::Runtime::Graphical );

		// Show message
		std::cout << Core::Runtime::Kernel::GetApplicationSettings()->GetApplicationTitleAndVersion() <<
			" is starting..." << std::endl;

		// Parse final arguments after everything is initialized
#if wxUSE_CMDLINE_PARSER
		cont = m_CommandLineParser.ParseFinalArgments( );
#endif // wxUSE_CMDLINE_PARSER
	}
	catch(Core::Exceptions::Exception& e)
	{
		logger = Core::Runtime::Kernel::GetLogManager();
		std::cerr << e.what() << std::endl;
		std::cerr << "---  Stack Trace --- \n" << e.GetStackTrace() << std::endl;
		if(logger.IsNotNull())
			logger->LogException(e);
		std::cerr << "Press a key to exit..." << std::endl;
		getchar();
		return false;
	}
	catch(...)
	{
		// Catch any other unhandled exception to gain in stability
		std::cerr << "Unhandled Exception caught" << std::endl;
		std::cerr << "The application exited because an unexpected error while initializing. Contact the support" << std::endl;
		std::cerr << " team in order to solve this problem." << std::endl << std::endl;
		std::cerr << "Press a key to exit..." << std::endl;
		getchar();
		return false;
	}

	return true;
}


//! this is executed upon startup, like 'main()' in non-wxWidgets programs, and holds the application main loop
int MainApp::OnRun(void)
{
	// Don't execute main loop
	if ( m_Runtime->OnAppExit( ) )
	{
		return 0;
	}

	int errorCode = 0;
	Core::Runtime::Logger::Pointer logger;
	std::string message;

	std::cout << "Startup time:" << m_Clock.Finish( ) << " sec" << std::endl;

	try
	{
		logger = Core::Runtime::Kernel::GetLogManager();
		m_Runtime->ExecuteMainLoop();
	}
	catch(Core::Exceptions::Exception& e)
	{
		// Report an error code 1 (exit by unhandled exception caught on runtime)
		errorCode = 1;
		std::cerr << e.what() << std::endl;
		std::cerr << "---  Stack Trace --- \n" << e.GetStackTrace() << std::endl;
		if(logger.IsNotNull())
			logger->LogException(e);
		std::cerr << "Press a key to exit..." << std::endl;
		getchar();
	}
	catch(std::exception& e)
	{
		// Catch any other unhandled std::exception to gain in stability
		// Report an error code 1 (exit by unhandled exception caught on runtime)
		errorCode = 1;
		std::cerr << "Std Unhandled Exception caught: " << std::endl;
		std::cerr << e.what() << std::endl << std::endl;
		std::cerr << "The application exited because an unexpected error while in runtime. Contact the support" << std::endl;
		std::cerr << " team in order to solve this problem." << std::endl << std::endl;
		std::cerr << "Press a key to exit..." << std::endl;
		getchar();
	}
	catch(...)
	{
		// Catch any other unhandled exception to gain in stability
		// Report an error code 2 (exit by unknown unhandled exception caught on runtime)
		errorCode = 2;
		std::cerr << "Unhandled Exception caught" << std::endl;
		std::cerr << "The application exited because an unexpected error while in runtime. Contact the support" << std::endl;
		std::cerr << " team in order to solve this problem." << std::endl << std::endl;
		std::cerr << "Press a key to exit..." << std::endl;
		getchar();
	}

	return errorCode;
}

/**
On exit application is called when closing the main frame of the application. Here all objects must be
unloaded to prevent memmory leaks.
*/
int MainApp::OnExit(void)
{
	int errorCode = 0;
	try
	{
		// Close main window to kill all windows before destroying the Kernel
		if ( GetTopWindow( ) )
		{
			GetTopWindow( )->Destroy();

			// Process idle events and destroy the pending objects
			wxTheApp->ProcessIdle();
		}

		// Cleanup allocation of all the members of static objects
		Core::Runtime::Kernel::Terminate();

		// Remove DataStorage
		wxApp::OnExit( );

		// Restart if needed. This function cannot be called with
		// atexit() because ShellExecution must be called when running
		// WebUpdate and if it's called in the function configured atexit()
		// it crashes.
		Core::Runtime::Kernel::GetApplicationRuntime()->RestartIfNeeded( );
	}
	catch(std::exception& e)
	{
		// Catch any other unhandled std::exception to gain in stability
		// Report an error code 1 (exit by unhandled exception caught on runtime)
		errorCode = 1;
		std::cerr << "Std Unhandled Exception caught: " << std::endl;
		std::cerr << e.what() << std::endl << std::endl;
		std::cerr << "The application exited because an unexpected error while in runtime. Contact the support" << std::endl;
		std::cerr << " team in order to solve this problem." << std::endl << std::endl;
		std::cerr << "Press a key to exit..." << std::endl;
		getchar();
	}
	catch(...)
	{
		// Catch any other unhandled exception to gain in stability
		// Report an error code 2 (exit by unknown unhandled exception caught on runtime)
		errorCode = 2;
		std::cerr << "Unhandled Exception caught" << std::endl;
		std::cerr << "The application exited because an unexpected error while in runtime. Contact the support" << std::endl;
		std::cerr << " team in order to solve this problem." << std::endl << std::endl;
		std::cerr << "Press a key to exit..." << std::endl;
		getchar();
	}

	return errorCode == 0;
}



void MainApp::HandleEvent(
	wxEvtHandler *handler, wxEventFunction func, wxEvent& event ) const
{
	try
	{
		wxApp::HandleEvent( handler, func, event );
	}
	coreCatchExceptionsReportAndNoThrowMacro( MainApp::HandleEvent );
}

int MainApp::FilterEvent(wxEvent& event)
{
	if ( m_Runtime.IsNull() )
	{
		return -1;
	}

	return m_Runtime->FilterEvent( event );
}
