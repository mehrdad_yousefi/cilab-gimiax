/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreMainApp_H
#define coreMainApp_H

#include <wx/app.h>
#include "coreWxEnvironment.h"
#include "coreCommandLineParser.h"
#include "blClock.h"

namespace Core
{
/**
A version of wxApp is provided for wxMitk. When you use Lambda Core, 
you must create an instance of MainApp.

The reason behind this relies in specific routines and event filtering 
needed for the correct behavior of the mitk rendering engine. Read more 
about in wxMitkApp class.
\sa wxMitkApp

\ingroup gmMainApp
\author Juan Antonio Moya
\date 02 Jan 2008
*/
class MainApp : public wxApp
{
public:
	virtual bool OnInit(void);
	virtual int OnRun(void);
	virtual int OnExit(void);

	//! Filters all events captured by the application. This is an important routine.
	virtual int FilterEvent(wxEvent& event);

	//!
	void HandleEvent(wxEvtHandler *handler,
		wxEventFunction func,
		wxEvent& event) const;

private:
	//!
	Core::Runtime::WxEnvironment::Pointer m_Runtime;
	//!
	Core::CommandLineParser m_CommandLineParser;
	//!
	blClock m_Clock;
};

DECLARE_APP(MainApp)

}

#endif // coreMainApp_H
