/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreCommandLineParser_H
#define coreCommandLineParser_H


// constants for the command line options names
namespace CmdLineOption
{

	const char * const FILENAME = "filename";
	const char * const SESSION = "session";
	const char * const COMPILER_VERSION = "compiler_version";
	const char * const ARCHITECTURE_VERSION = "architecture_version";
	const char * const CONSOLE_WIN = "consolewin";
	const char * const GM_VERSION = "version";
	const char * const LOAD_CONFIG = "load_config";
	const char * const DOWNLOAD = "download";
	const char * const UPLOAD = "upload";
	const char * const STOP = "stop";

} // namespace CmdLineOption

namespace Core
{
/**
Parses command line arguments

\ingroup gmMainApp
\author Xavi Planes
\date Nov 2011
*/
class CommandLineParser : public wxCmdLineParser
{
public:
	//!
	CommandLineParser( );

	//!
	void SetCmdLine(int argc, char** argv);

	//! Parse arguments before GIMIAS starts
	bool PraseInitialArguments( );

	/** Parse arguments after GIMIAS starts
	\param [out] startGimias set it to true if GIMIAS should continue
	\return true if actions are performed correctly
	*/
	bool ParseFinalArgments( );

	//!
	bool GetShowConsoleWindow( );

private:
	//!
	int m_argc;
	//!
	char** m_argv;
};

}

#endif // coreCommandLineParser_H
