/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#ifdef WX_PRECOMP
  #include <wx/wxprec.h>
#endif
#include <wx/wx.h>
#include <wx/cmdline.h>

#include "coreDataEntityBuildersRegistration.h"
#include "coreDataEntityHelper.h"
#include "coreDataTreeHelper.h"
#include "coreException.h"
#include "coreKernel.h"
#include "coreLogger.h"
#include "corePluginTab.h"
#include "coreReportExceptionMacros.h"
#include "coreSessionReader.h"
#include "coreSettings.h"
#include "coreWxMitkGraphicalInterface.h"
#include <iostream>
#include <stdio.h>
#include <string>

