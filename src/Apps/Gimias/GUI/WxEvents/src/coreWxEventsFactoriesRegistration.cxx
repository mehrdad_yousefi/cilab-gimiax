/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreWxEventsFactoriesRegistration.h"
#include "coreFactoryManager.h"
#include "coreWxPortInvocation.h"
#include "coreWxCallbackObserver.h"

void Core::WxEventsFactoriesRegistration::Register()
{
	Core::FactoryManager::Register( PortInvocation::GetNameClass( ), WxPortInvocation::Factory::New( ) );
	Core::FactoryManager::Register( CallbackObserver::GetNameClass( ), WxCallbackObserver::Factory::New( ) );
}

void Core::WxEventsFactoriesRegistration::UnRegister()
{
	Core::FactoryManager::UnRegister( Core::FactoryManager::FindByInstanceClassName( WxPortInvocation::GetNameClass( ) ) );
	Core::FactoryManager::UnRegister( Core::FactoryManager::FindByInstanceClassName( WxCallbackObserver::GetNameClass( ) ) );
}
