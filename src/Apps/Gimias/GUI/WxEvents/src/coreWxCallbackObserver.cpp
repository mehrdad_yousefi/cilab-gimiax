/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include "wx/wxprec.h"

#ifndef WX_PRECOMP
	#include "wx/wx.h"
#endif

#include "coreWxCallbackObserver.h"
#include "coreWxUpdateCallbackEvent.h"

using namespace Core;

Core::WxCallbackObserver::WxCallbackObserver()
{
	
}

void Core::WxCallbackObserver::OnModified()
{
	if ( wxTheApp->GetTopWindow( ) == NULL )
		return;

  WxUpdateCallbackEvent event;
  event.SetUpdateCallback( m_UpdateCallback );
  event.SetEventObject( wxTheApp->GetTopWindow( ) );

	if ( !wxIsMainThread() )
	{
    wxQueueEvent(wxTheApp->GetTopWindow()->GetEventHandler(), event.Clone());
	}
	else if ( wxIsMainThread() )
	{
    wxPostEvent(wxTheApp->GetTopWindow()->GetEventHandler(), event);
	}
}

