/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreWxGenericEvtHandler_H
#define _coreWxGenericEvtHandler_H

#include "gmWxEventsWin32Header.h"
#include "coreWxGenericEvent.h"

namespace Core
{

/**
Handle WxGenericEvent and call the configured function of the event
when the event is received.

\ingroup gmWxEvents
\author Xavi Planes
\date Oct 2011
*/
class GMWXEVENTS_EXPORT WxGenericEvtHandler : public wxEvtHandler
{
public:
	//!
	WxGenericEvtHandler( );
	//!
	~WxGenericEvtHandler( );

private:
	//! Exception is catch and reported to log
	void OnGenericEvent(WxGenericEvent& event);

    wxDECLARE_EVENT_TABLE();
};


} // namespace Core

#endif // _coreWxGenericEvtHandler_H
