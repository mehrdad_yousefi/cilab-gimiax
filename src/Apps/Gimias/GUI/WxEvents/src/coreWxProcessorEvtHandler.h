/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreWxProcessorEvtHandler_H
#define _coreWxProcessorEvtHandler_H

#include "gmWxEventsWin32Header.h"
#include <wx/event.h>
#include "coreWxUpdatePortEvent.h"

namespace Core
{

/**
Singleton class that will handle the events in the main GUI thread
\ingroup gmWxEvents
\author Xavi Planes
\date Jan 2011
*/
class GMWXEVENTS_EXPORT WxProcessorEvtHandler : public wxEvtHandler
{
public:
	//!
	WxProcessorEvtHandler( );
	//!
	~WxProcessorEvtHandler( );
private:
	//!
	void OnUpdatePortEvent(WxUpdatePortEvent& event);

    wxDECLARE_EVENT_TABLE();
};


} // namespace Core

#endif // _coreWxProcessorEvtHandler_H
