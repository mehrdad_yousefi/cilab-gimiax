/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreWxGenericEvtHandler.h"
#include "coreReportExceptionMacros.h"

using namespace Core;

BEGIN_EVENT_TABLE(WxGenericEvtHandler, wxEvtHandler)
  EVT_GENERIC_EVENT(WxGenericEvtHandler::OnGenericEvent)
END_EVENT_TABLE();


void Core::WxGenericEvtHandler::OnGenericEvent( WxGenericEvent& event )
{
	try
	{
		event.GetFunction()( );
	}
	coreCatchExceptionsLogAndNoThrowMacro( OnGenericEvent )

	event.Finished();
}

Core::WxGenericEvtHandler::WxGenericEvtHandler()
{

}

Core::WxGenericEvtHandler::~WxGenericEvtHandler()
{

}
