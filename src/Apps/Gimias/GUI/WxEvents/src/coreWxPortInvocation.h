/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreWxPortInvocation_H
#define _coreWxPortInvocation_H

#include "gmWxEventsWin32Header.h"
#include "corePortInvocation.h"

namespace Core
{

/** 
Sends a wxEvent to itself and notifies observers of the holder

This allows to send the notification to the main GUI thread and use
multi threading

When a filter is executed using multi threading, this is the sequence of 
performed steps:
- Update input data: BaseProcessor::GetProcessingData( ) is called. 
This function calls BaseFilterInputPort::UpdateInput( ), that creates a 
InputUpdater, initializes it, calls Update( ) and finally gets the data 
using GetData( ) method. The InputUpdater will create a new PortInvocation 
with multithreading capabilty or not, depending if multi threading has been enabled.
- Update processor: Processor is executed and generates new output data from input data
- Update output data: Output data is set as output of the processor and 
observers are notified. The function BaseProcessor::UpdateOutput( ) 
calls BaseFilterOutputPort::UpdateOutput( ), that creates a new OutputUpdater, 
initializes it and calls Update( ). OutputUpdater will create a new 
PortIncovation with multithreading capabilty or not, depending if 
multi threading has been enabled. 

\ingroup gmWxEvents
\author Xavi Planes
\date Jan 2011
*/
class GMWXEVENTS_EXPORT WxPortInvocation : public PortInvocation 
{
public:
	//!
	coreDeclareSmartPointerClassMacro( Core::WxPortInvocation, PortInvocation );
	coreDefineFactoryClass( WxPortInvocation );
	coreDefineFactoryTagsBegin( );
	coreDefineFactoryAddTag( "multithreading", true );
	coreDefineFactoryTagsEnd( );
	coreDefineFactoryClassEnd( )

	//!
	void Update( );

private:
	//!
	WxPortInvocation( );

protected:
};

} // namespace Core

#endif // _coreWxPortInvocation_H
