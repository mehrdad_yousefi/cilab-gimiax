/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreWxUpdatePortEvent_H
#define _coreWxUpdatePortEvent_H

#include "gmWxEventsWin32Header.h"
#include <wx/event.h>
#include "corePortUpdater.h"
#include "boost/shared_ptr.hpp"

namespace Core
{

/**
Updates the inputs before processing and the ouputs after processing. This 
code should be executed by main GUI thread because the observers of rendering 
data can update the render windows. When the input is updated, it will 
switch to the needed type for the processor. 

\ingroup gmWxEvents
\author Xavi Planes
\date Jan 2011
*/
class GMWXEVENTS_EXPORT WxUpdatePortEvent : public wxEvent
{
public:
	WxUpdatePortEvent( wxWindow* win = (wxWindow*) NULL );
	~WxUpdatePortEvent( );
	wxEvent* Clone() const;

	PortUpdater::Pointer GetPortUpdater() const;
	void SetPortUpdater(PortUpdater::Pointer val);

	//!
	void Finished( );

	//!
	void Wait( );
  wxSemaError WaitFor(unsigned long timeout_millis = 100);

	DECLARE_DYNAMIC_CLASS( WxUpdatePortEvent )
private:
	//!
	PortUpdater::Pointer m_PortUpdater;
	//!
	boost::shared_ptr<wxSemaphore> m_SemaphoreFinished;
};


} // namespace Core

typedef void (wxEvtHandler::*gmUpdatePortEventFunction)(Core::WxUpdatePortEvent&);

BEGIN_DECLARE_EVENT_TYPES()
  DECLARE_EXPORTED_EVENT_TYPE( GMWXEVENTS_EXPORT, gmEVT_UPDATE_PORT, 1 )
END_DECLARE_EVENT_TYPES()


#define EVT_UPDATE_PORT_EVENT(func)     \
	DECLARE_EVENT_TABLE_ENTRY( gmEVT_UPDATE_PORT,      \
	-1,                                    \
	-1,                                    \
	(wxObjectEventFunction) (gmUpdatePortEventFunction) & func, \
	(wxObject *) NULL ),

#endif // _coreWxUpdatePortEvent_H
