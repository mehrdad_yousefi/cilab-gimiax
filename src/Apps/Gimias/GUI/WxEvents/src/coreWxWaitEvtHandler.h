/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreWxWaitEvtHandler_H
#define _coreWxWaitEvtHandler_H

#include "gmWxEventsWin32Header.h"
#include "coreWxGenericEvent.h"

namespace Core
{

/**
Wait for an event to be received and signals a semaphore.

\ingroup gmWxEvents
\author Xavi Planes
\date Oct 2011
*/
class GMWXEVENTS_EXPORT WxWaitEvtHandler : public wxEvtHandler
{
public:
	//!
	WxWaitEvtHandler( );
	//!
	~WxWaitEvtHandler( );

	//!
	void SetEventType(wxEventType val);

	//!
	void SetId(int val);

	//!
	void Connect( );

	//!
	void Wait( );

	//!
	wxSemaError WaitTimeout(unsigned longtimeout_millis);

private:
	//! Activate the semaphore
	void OnEvent(wxCommandEvent& event);

private:
	//!
	boost::shared_ptr<wxSemaphore> m_SemaphoreEvent;

	//!
	wxEventType m_EventType;

	//!
	int m_Id;
};


} // namespace Core

#endif // _coreWxWaitEvtHandler_H
