/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreWxProcessorEvtHandler.h"
#include "coreReportExceptionMacros.h"

using namespace Core;

BEGIN_EVENT_TABLE(WxProcessorEvtHandler, wxEvtHandler)
  EVT_UPDATE_PORT_EVENT(WxProcessorEvtHandler::OnUpdatePortEvent)
END_EVENT_TABLE();


void Core::WxProcessorEvtHandler::OnUpdatePortEvent( WxUpdatePortEvent& event )
{
	// Exception can be thrown when input data type is not matching the
	// requested data type
	try
	{
		event.GetPortUpdater( )->UpdatePort( );
	}
	coreCatchExceptionsLogAndNoThrowMacro( OnUpdatePortEvent )

	event.Finished();
}

Core::WxProcessorEvtHandler::WxProcessorEvtHandler()
{

}

Core::WxProcessorEvtHandler::~WxProcessorEvtHandler()
{

}

