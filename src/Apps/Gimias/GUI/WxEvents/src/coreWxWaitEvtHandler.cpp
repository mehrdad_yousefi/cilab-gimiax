/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreWxWaitEvtHandler.h"
#include "coreReportExceptionMacros.h"

using namespace Core;

Core::WxWaitEvtHandler::WxWaitEvtHandler()
{
	m_SemaphoreEvent = boost::shared_ptr<wxSemaphore>( new wxSemaphore( ) );
	m_Id = wxID_ANY;
	m_EventType = wxID_ANY;
}

Core::WxWaitEvtHandler::~WxWaitEvtHandler()
{

}

void Core::WxWaitEvtHandler::OnEvent( wxCommandEvent& event )
{
	m_SemaphoreEvent->Post();
}

void Core::WxWaitEvtHandler::Wait()
{
	m_SemaphoreEvent->Wait();
}

wxSemaError Core::WxWaitEvtHandler::WaitTimeout(unsigned longtimeout_millis)
{
	return m_SemaphoreEvent->WaitTimeout(longtimeout_millis);
}

void Core::WxWaitEvtHandler::SetEventType( wxEventType val )
{
	m_EventType = val;
}

void Core::WxWaitEvtHandler::Connect()
{
	wxEvtHandler::Connect( m_Id, m_EventType, wxCommandEventHandler(WxWaitEvtHandler::OnEvent) );
  //Bind(m_EventType, &WxWaitEvtHandler::OnEvent, this, m_Id);
}

void Core::WxWaitEvtHandler::SetId( int val )
{
	m_Id = val;
}
