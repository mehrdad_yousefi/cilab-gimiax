/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreWxUpdateCallbackEvent_H
#define _coreWxUpdateCallbackEvent_H

#include "gmWxEventsWin32Header.h"
#include <wx/event.h>
#include "coreUpdateCallback.h"

namespace Core
{

/**
Send progress, status, exception messages to GUI thread and abort 
processing if user click cancel button 

\ingroup gmWxEvents
\author Xavi Planes
\date Jan 2011
*/
class GMWXEVENTS_EXPORT WxUpdateCallbackEvent : public wxEvent
{
public:
	WxUpdateCallbackEvent( wxWindow* win = (wxWindow*) NULL );
	~WxUpdateCallbackEvent( );
	wxEvent* Clone() const;

	//!
	UpdateCallback::Pointer GetUpdateCallback() const;
	void SetUpdateCallback(UpdateCallback::Pointer val);

	DECLARE_DYNAMIC_CLASS( WxUpdateCallbackEvent )
private:
	//!
	UpdateCallback::Pointer m_UpdateCallback;
};


} // namespace Core

typedef void (wxEvtHandler::*gmUpdateCallbackEventFunction)(Core::WxUpdateCallbackEvent&);

BEGIN_DECLARE_EVENT_TYPES()
  DECLARE_EXPORTED_EVENT_TYPE( GMWXEVENTS_EXPORT, gmEVT_UPDATE_CALLBACK, 1 )
END_DECLARE_EVENT_TYPES()


#define EVT_UPDATE_CALLBACK_EVENT(func)     \
	DECLARE_EVENT_TABLE_ENTRY( gmEVT_UPDATE_CALLBACK,      \
	-1,                                    \
	-1,                                    \
	(wxObjectEventFunction) (gmUpdateCallbackEventFunction) & func, \
	(wxObject *) NULL ),

#endif // _coreWxUpdateCallbackEvent_H
