/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreWxGenericEvent_H
#define _coreWxGenericEvent_H

#include "gmWxEventsWin32Header.h"
#include <wx/event.h>
#include "boost/shared_ptr.hpp"
#include "boost/function.hpp"


namespace Core
{

/**
Generic event that executes a function provided by the user.

Optionally the user can wait until it finishes

\ingroup gmWxEvents
\author Xavi Planes
\date Oct 2011
*/
class GMWXEVENTS_EXPORT WxGenericEvent : public wxEvent
{
public:
	WxGenericEvent( wxWindow* win = (wxWindow*) NULL );
	~WxGenericEvent( );
	wxEvent* Clone() const;

	//!
	void SetFunction(boost::function0<void> val);

	//!
	template <class T>
	void SetFunction( 
		T* _recipient, 
		void (T::*_recipientMemberFunction)( void ) )
	{
		SetFunction( boost::bind( _recipientMemberFunction, _recipient ) );
	}

	boost::function0<void> GetFunction() const;

	//!
	void Finished( );

	//!
	void Wait( );

	//!
	wxSemaError WaitTimeout(unsigned longtimeout_millis);

	DECLARE_DYNAMIC_CLASS( WxGenericEvent )

private:
	//!
	boost::function0<void> m_Function;
	//!
	boost::shared_ptr<wxSemaphore> m_SemaphoreFinished;
};


} // namespace Core

typedef void (wxEvtHandler::*gmGenericEventFunction)(Core::WxGenericEvent&);

BEGIN_DECLARE_EVENT_TYPES()
  DECLARE_EXPORTED_EVENT_TYPE( GMWXEVENTS_EXPORT, gmEVT_GENERIC_EVENT, 1 )
END_DECLARE_EVENT_TYPES()


#define EVT_GENERIC_EVENT(func)     \
	DECLARE_EVENT_TABLE_ENTRY( gmEVT_GENERIC_EVENT,      \
	-1,                                    \
	-1,                                    \
	(wxObjectEventFunction) (gmGenericEventFunction) & func, \
	(wxObject *) NULL ),

#endif // _coreWxGenericEvent_H
