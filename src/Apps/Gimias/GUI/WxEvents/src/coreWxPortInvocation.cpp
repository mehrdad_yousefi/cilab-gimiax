/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include "wx/wxprec.h"

#ifndef WX_PRECOMP
	#include "wx/wx.h"
#endif

#include "coreWxPortInvocation.h"
#include "coreWxUpdatePortEvent.h"

using namespace Core;

Core::WxPortInvocation::WxPortInvocation()
{
}

void Core::WxPortInvocation::Update()
{
	// Assure that the calling thread is not the main thread
	if ( !wxIsMainThread() )
	{
    // XXX the proper thing to do is using smart and weak pointer
    // but wxwidget is not using it for queueevent
    // or some type or callback
		WxUpdatePortEvent event;
		event.SetPortUpdater( GetPortUpdater() );
		event.SetEventObject( wxTheApp->GetTopWindow( ) );
    wxQueueEvent(wxTheApp->GetTopWindow()->GetEventHandler(), event.Clone());

		// Wait until input data has been updated to continue processing
		if ( GetPortUpdater( )->GetWaitPortUpdate( ) )
		{
      // we suppose event will be delete and assigned null again :(
			//while (event.WaitFor(200) != wxSEMA_NO_ERROR) { /* wait for completiion */}
			event.WaitFor(1000);
		}
	}
	else
	{
		m_PortUpdater->UpdatePort();
	}

}

