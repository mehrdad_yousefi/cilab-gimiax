/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreWxUpdatePortEvent.h"

using namespace Core;

DEFINE_EVENT_TYPE(gmEVT_UPDATE_PORT)
IMPLEMENT_DYNAMIC_CLASS(WxUpdatePortEvent, wxEvent)

WxUpdatePortEvent::WxUpdatePortEvent( wxWindow* win /*= (wxWindow*) NULL */ )
{
	SetEventType( gmEVT_UPDATE_PORT );
	SetEventObject( win );
	m_SemaphoreFinished = boost::shared_ptr<wxSemaphore>( new wxSemaphore( ) );
}

Core::WxUpdatePortEvent::~WxUpdatePortEvent()
{
}

wxEvent* WxUpdatePortEvent::Clone() const
{
	return new WxUpdatePortEvent(*this);
}

PortUpdater::Pointer Core::WxUpdatePortEvent::GetPortUpdater() const
{
	return m_PortUpdater;
}

void Core::WxUpdatePortEvent::SetPortUpdater( PortUpdater::Pointer val )
{
	m_PortUpdater = val;
}

void Core::WxUpdatePortEvent::Finished()
{
	m_SemaphoreFinished->Post();
}

void Core::WxUpdatePortEvent::Wait()
{
	m_SemaphoreFinished->Wait();
}

wxSemaError Core::WxUpdatePortEvent::WaitFor(unsigned long timeout_millis /* = 100 */)
{
	return m_SemaphoreFinished->WaitTimeout(timeout_millis);
}

