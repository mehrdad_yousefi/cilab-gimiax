/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreWxEventsFactoriesRegistration_H
#define _coreWxEventsFactoriesRegistration_H

#include "gmWxEventsWin32Header.h"
#include "coreObject.h"

namespace Core{

/** 
Registers all Factories for this library

\ingroup gmWxEvents
\author: Xavi Planes
\date: Jan 2011
*/
class GMWXEVENTS_EXPORT WxEventsFactoriesRegistration : public Core::Object
{
public:
	//! Register all builders
	static void Register();

	//! Register all builders
	static void UnRegister();

protected:

};

} // namespace Core{

#endif // _coreWxEventsFactoriesRegistration_H
