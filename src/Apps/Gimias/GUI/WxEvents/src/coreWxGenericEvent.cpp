/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreWxGenericEvent.h"

using namespace Core;

DEFINE_EVENT_TYPE(gmEVT_GENERIC_EVENT)
IMPLEMENT_DYNAMIC_CLASS(WxGenericEvent, wxEvent)

WxGenericEvent::WxGenericEvent( wxWindow* win /*= (wxWindow*) NULL */ )
{
	SetEventType( gmEVT_GENERIC_EVENT );
	SetEventObject( win );
	m_SemaphoreFinished = boost::shared_ptr<wxSemaphore>( new wxSemaphore( ) );
}

Core::WxGenericEvent::~WxGenericEvent()
{
}

wxEvent* WxGenericEvent::Clone() const
{
	return new WxGenericEvent(*this);
}

void Core::WxGenericEvent::Finished()
{
	m_SemaphoreFinished->Post();
}

void Core::WxGenericEvent::Wait()
{
	m_SemaphoreFinished->Wait();
}

wxSemaError Core::WxGenericEvent::WaitTimeout(unsigned longtimeout_millis)
{
	return m_SemaphoreFinished->WaitTimeout(longtimeout_millis);
}

void Core::WxGenericEvent::SetFunction( boost::function0<void> val )
{
	m_Function = val;
}

boost::function0<void> Core::WxGenericEvent::GetFunction() const
{
	return m_Function;
}

