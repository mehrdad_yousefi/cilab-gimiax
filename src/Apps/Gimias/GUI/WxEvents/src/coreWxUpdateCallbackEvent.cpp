/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreWxUpdateCallbackEvent.h"

using namespace Core;

DEFINE_EVENT_TYPE(gmEVT_UPDATE_CALLBACK)
IMPLEMENT_DYNAMIC_CLASS(WxUpdateCallbackEvent, wxEvent)

WxUpdateCallbackEvent::WxUpdateCallbackEvent( wxWindow* win /*= (wxWindow*) NULL */ )
{
	SetEventType( gmEVT_UPDATE_CALLBACK );
	SetEventObject( win );
}

Core::WxUpdateCallbackEvent::~WxUpdateCallbackEvent()
{
}

UpdateCallback::Pointer Core::WxUpdateCallbackEvent::GetUpdateCallback() const
{
	return m_UpdateCallback;
}

void Core::WxUpdateCallbackEvent::SetUpdateCallback( UpdateCallback::Pointer val )
{
	m_UpdateCallback = val;
}

wxEvent* WxUpdateCallbackEvent::Clone() const
{
	return new WxUpdateCallbackEvent(*this);
}

