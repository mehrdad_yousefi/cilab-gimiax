/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreWxCallbackObserver_H
#define _coreWxCallbackObserver_H

#include "gmWxEventsWin32Header.h"
#include "coreBaseFactory.h"
#include "coreCallbackObserver.h"

namespace Core
{

/** 
Sends a wxEvent to top window

\ingroup gmWxEvents
\author Xavi Planes
\date Jan 2011
*/
class GMWXEVENTS_EXPORT WxCallbackObserver : public Core::CallbackObserver 
{
public:
	//!
	coreDeclareSmartPointerClassMacro( Core::WxCallbackObserver, Core::CallbackObserver );
	coreDefineFactoryClass( WxCallbackObserver );
	coreDefineFactoryTagsBegin( );
	coreDefineFactoryAddTag( "GUI", true );
	coreDefineFactoryTagsEnd( );
	coreDefineFactoryClassEnd( )

	//!
	void OnModified( );

private:
	//!
	WxCallbackObserver( );

protected:
};

} // namespace Core

#endif // _coreWxCallbackObserver_H
