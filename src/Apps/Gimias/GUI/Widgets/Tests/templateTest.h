/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
#ifndef TEMPLATETEST_H
#define TEMPLATETEST_H

#include "cxxtest/TestSuite.h"

/**
*\brief Tests for MyLib
*\ingroup MyLibTests
*/
class templateTest : public CxxTest::TestSuite 
{
public:

void TestOne()
{
   TSM_ASSERT( "TestOne run ok.", true );
}

}; // class templateTest

#endif TEMPLATETEST_H
