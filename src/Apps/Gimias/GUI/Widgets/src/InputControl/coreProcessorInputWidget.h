/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreProcessorInputWidget_H
#define _coreProcessorInputWidget_H

#include "gmWidgetsWin32Header.h"
#include "coreBaseInputControl.h"
#include "coreDataEntity.h"
#include "wxCheckableControl.h"

namespace Core{ namespace Widgets{ class DataEntityListBrowser; } }

namespace Core
{
namespace Widgets 
{ 

#define wxID_SelectProcessing	wxID( "wxID_SelectProcessing" )

/** 
Widget to show and set the input of a processor.

If you set bAutomaticSelection to true, the selected data entity form the
DataList will be selected automatically, else, a button will appear
to allow the user to set it as selected.

On the right side there's a button that allows to select the time 
steps from the selected DataEntity.

\sa AcquireInputControl, DataEntity
\ingroup gmWidgets
\author Xavi Planes
\date 06 Nov 2009
*/
class GMWIDGETS_EXPORT ProcessorInputWidget 
	: public BaseInputControl
{
public:
	coreClassNameMacro(Core::Widgets::ProcessorInputWidget);

	ProcessorInputWidget(
		wxWindow* parent, 
		wxWindowID id, 
		bool bAutomaticSelection = false,
		bool bSetDefaultDataEntity = true,
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = wxBORDER_NONE, 
		const wxString& name = "ProcessorInputWidget");
	virtual ~ProcessorInputWidget(void);

	//!
	void SetInputPort( Core::BaseFilterInputPort::Pointer inputPort );

	//!
	wxCheckableControl* GetCheckableControl() const;

protected:
	//! 
	bool SetAcquiredInputData( Core::DataEntity::Pointer &subject );

	//! Check if subject is ok to be selected or not
	bool CheckSubjectIsOk( DataEntity::Pointer &subject );

	//!
	void UpdateOptional( );

	//!
	void OnCheckBox(wxCommandEvent& event);

	/** User pressed Select processing button
	Show a context menu that allows user to select 
	*/
	void OnSelectProcessingBtn(wxCommandEvent &event);

	//! A Select all menu item has been clicked
	void OnSelectAllTimeSteps( wxCommandEvent& event );

	//! A Select current menu item has been clicked
	void OnSelectCurrentTimeStep( wxCommandEvent& event );

    wxDECLARE_EVENT_TABLE();

protected:

	//!
	Core::BaseFilterInputPort::Pointer m_InputPort;

	//!
	wxCheckableControl* m_CheckableControl;

	//!
	wxBitmapButton* m_btnSelectProcessing;

	//!
	wxMenu *m_SelectionMenu;
};

}
}


#endif // _coreProcessorInputWidget_H
