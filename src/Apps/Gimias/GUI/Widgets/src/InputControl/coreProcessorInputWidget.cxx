/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreProcessorInputWidget.h"
#include "coreDataEntityInfoHelper.h"
#include "coreDataEntityListBrowser.h"
#include "coreDataEntityHelper.h"
#include "coreSelectionComboBox.h"
#include "SelectProcessing.xpm"

using namespace Core::Widgets;

#define wxID_SELECT_ALL wxID( "wxID_SELECT_ALL" )
#define wxID_SELECT_CURRENT wxID( "wxID_SELECT_CURRENT" )

BEGIN_EVENT_TABLE(ProcessorInputWidget, BaseInputControl)
  EVT_CHECKBOX			(wxID_ANY, ProcessorInputWidget::OnCheckBox)
  EVT_BUTTON(wxID_SelectProcessing, ProcessorInputWidget::OnSelectProcessingBtn)
  EVT_MENU( wxID_SELECT_ALL, ProcessorInputWidget::OnSelectAllTimeSteps )
  EVT_MENU( wxID_SELECT_CURRENT, ProcessorInputWidget::OnSelectCurrentTimeStep )
END_EVENT_TABLE()

//!
ProcessorInputWidget::ProcessorInputWidget(
	wxWindow* parent, wxWindowID id, bool bAutomaticSelection, bool bSetDefaultDataEntity,
	const wxPoint& pos, const wxSize& size, 
	long style, const wxString& name)
: BaseInputControl(parent, id, pos, size, style, name, bAutomaticSelection, bSetDefaultDataEntity)
{
	m_InputPort = Core::BaseFilterInputPort::New();

	// Reset the acquired input holder
	SetAcquiredInputDataHolder( m_InputPort->GetDataEntityHolder().GetPointer() );

	m_CheckableControl = NULL;

	m_btnSelectProcessing = NULL;
	m_SelectionMenu = NULL;
}

//!
ProcessorInputWidget::~ProcessorInputWidget(void)
{
}


bool Core::Widgets::ProcessorInputWidget::SetAcquiredInputData( 
	Core::DataEntity::Pointer &subject )
{
	// We are going to change the acquired input
	if ( !CheckSubjectIsOk( subject ) )
	{
		return false;
	}

	bool success = m_InputPort->SetDataEntity( subject );

	return success;
}

void Core::Widgets::ProcessorInputWidget::SetInputPort( 
	Core::BaseFilterInputPort::Pointer inputPort )
{
	m_InputPort = inputPort;
	SetAcquiredInputDataHolder( m_InputPort->GetDataEntityHolder().GetPointer() );
	SetHeaderText( m_InputPort->GetName() );
	FillListBox( );
	UpdateOptional( );
	SetName( m_InputPort->GetName() );

	if ( m_InputPort->GetUpdateMode() == BaseFilterInputPort::UPDATE_ACCESS_SINGLE_TIME_STEP &&
		m_btnSelectProcessing == NULL )
	{
		m_btnSelectProcessing = new wxBitmapButton(
			this, wxID_SelectProcessing, 
			wxBitmap( SelectProcessing_xpm ) );
		m_btnSelectProcessing->SetSize( 21, 21 );
		m_btnSelectProcessing->SetMinSize( wxSize( 21, 21 ) );
		m_btnSelectProcessing->SetMaxSize( wxSize( 21, 21 ) );
		GetSizer()->Add(m_btnSelectProcessing, 0, wxLEFT, 5 );

		m_SelectionMenu = new wxMenu( );
		m_SelectionMenu->Append( wxID_SELECT_CURRENT, wxT( "Select current time step" ), wxT( "Select current time step" ), wxITEM_CHECK );
		m_SelectionMenu->Append( wxID_SELECT_ALL, wxT( "Select all time steps" ), wxT( "Select all time steps" ), wxITEM_CHECK );
		m_SelectionMenu->Check( wxID_SELECT_CURRENT, true );
	}
}


bool Core::Widgets::ProcessorInputWidget::CheckSubjectIsOk(
	Core::DataEntity::Pointer &subject )
{

	// Check if is a children of the father
	bool bTypeIsOk = BaseInputControl::CheckSubjectIsOk( subject );
	if ( !bTypeIsOk )
	{
		return false;
	}

	bTypeIsOk = m_InputPort->CheckDataEntityRestrictions( subject );

	return bTypeIsOk;
}

void Core::Widgets::ProcessorInputWidget::UpdateOptional()
{
	if ( m_InputPort.IsNull() )
	{
		return;
	}

	if ( ( m_InputPort->GetOptional() && m_CheckableControl != NULL )
		|| ( !m_InputPort->GetOptional() && m_CheckableControl == NULL ) )
	{
		return;
	}

	if ( m_InputPort->GetOptional() )
	{
		m_CheckableControl = new wxCheckableControl(this, wxID_ANY);
		GetSizer( )->Detach( m_wxListBox );
		// Set proportion to 0
		m_CheckableControl->SetCheckBox( 
			m_CheckableControl->GetCheckBox( ), 
			wxSizerFlags( ).Align( wxALIGN_CENTER_VERTICAL ) );
		m_CheckableControl->AddWindow( m_wxListBox, wxSizerFlags().Expand().Proportion(1) );
		GetSizer( )->Add( m_CheckableControl, wxSizerFlags( ).Expand().Proportion(1) );
		m_InputPort->SetActive( m_CheckableControl->GetValue() );
	}
}

void Core::Widgets::ProcessorInputWidget::OnCheckBox( wxCommandEvent& event )
{
	m_InputPort->SetActive( m_CheckableControl->GetValue() );
}

wxCheckableControl* Core::Widgets::ProcessorInputWidget::GetCheckableControl() const
{
	return m_CheckableControl;
}

void Core::Widgets::ProcessorInputWidget::OnSelectProcessingBtn( wxCommandEvent &event )
{
	// Update selected mode from input port
	switch( m_InputPort->GetTimeStepSelection() )
	{
	case BaseFilterInputPort::SELECT_SINGLE_TIME_STEP:
		m_SelectionMenu->Check( wxID_SELECT_CURRENT, true );
		m_SelectionMenu->Check( wxID_SELECT_ALL, false );
		break;
	case BaseFilterInputPort::SELECT_ALL_TIME_STEPS:
		m_SelectionMenu->Check( wxID_SELECT_CURRENT, false );
		m_SelectionMenu->Check( wxID_SELECT_ALL, true );
		break;
	}

	// Show menu
	wxPoint position = m_btnSelectProcessing->GetPosition();
	position.y += m_btnSelectProcessing->GetSize().GetHeight();
	PopupMenu( m_SelectionMenu, position );
}

void Core::Widgets::ProcessorInputWidget::OnSelectAllTimeSteps( wxCommandEvent& event )
{
	m_InputPort->SetTimeStepSelection( BaseFilterInputPort::SELECT_ALL_TIME_STEPS );
}

void Core::Widgets::ProcessorInputWidget::OnSelectCurrentTimeStep( wxCommandEvent& event )
{
	m_InputPort->SetTimeStepSelection( BaseFilterInputPort::SELECT_SINGLE_TIME_STEP );
}
