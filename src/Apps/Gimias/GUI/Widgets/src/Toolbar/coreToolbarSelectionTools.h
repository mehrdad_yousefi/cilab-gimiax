/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreToolbarSelectionTools_H
#define _coreToolbarSelectionTools_H

#include "gmWidgetsWin32Header.h"

#include "coreToolbarRegisteredWindows.h"

namespace Core
{
namespace Widgets
{

/**
\brief Automatically create tools from  registered windows on BaseWindowFactory
\ingroup gmWidgets
\author Xavi Planes
\date 1 April 2010
*/
class GMWIDGETS_EXPORT ToolbarSelectionTools: public ToolbarRegisteredWindows {
public:
	//!
	coreDefineBaseWindowFactory( Core::Widgets::ToolbarSelectionTools );

	//!
	ToolbarSelectionTools(wxWindow* parent, wxWindowID id = wxID_ANY,
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = wxAUI_TB_DEFAULT_STYLE, 
		const wxString& name = wxPanelNameStr )
		:ToolbarRegisteredWindows( parent, id, pos, size, style, name )
	{
		SetToolsState( WIDGET_TYPE_SELECTION_TOOL );
	};

protected:

private:
};

} // namespace Widget
} // namespace Core

#endif // _coreToolbarSelectionTools_H
