/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreToolbarIO_H
#define _coreToolbarIO_H

#include "gmWidgetsWin32Header.h"

#include "coreToolbarBase.h"

namespace Core
{
namespace Widgets
{

/**
\brief Toolbar for IO
\ingroup gmWidgets
\author Xavi Planes
\date 10 June 2010
*/
class GMWIDGETS_EXPORT ToolbarIO: public Core::Widgets::ToolbarBase {
public:
	//!
	coreDefineBaseWindowFactory( Core::Widgets::ToolbarIO );

	//!
	ToolbarIO(wxWindow* parent, wxWindowID id = wxID_ANY, 
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = wxAUI_TB_DEFAULT_STYLE, 
		const wxString& name = wxPanelNameStr);

	//!
	Core::BaseProcessor::Pointer GetProcessor( );



protected:
	//!
	void UpdateState();
	

    wxDECLARE_EVENT_TABLE();
private:

};

} // namespace Widget
} // namespace Core

#endif // _coreToolbarIO_H
