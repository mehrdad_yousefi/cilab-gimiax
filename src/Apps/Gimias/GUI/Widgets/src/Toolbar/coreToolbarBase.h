/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved.
* 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved.
* 2009-2012
* See license.txt file for details.
*/

#ifndef _coreToolbarBase_H
#define _coreToolbarBase_H

#include <wx/wx.h>
#include <wx/image.h>
#include <wx/aui/auibar.h>

#include "gmWidgetsWin32Header.h"

#include "coreBaseWindow.h"
#include "coreWindowConfig.h"

namespace Core {
namespace Widgets {

/**
\brief Base toolbar derived from wxAuiToolBar
\ingroup gmWidgets
\author Xavi Planes
\date 1 April 2010
*/
class GMWIDGETS_EXPORT ToolbarBase : public wxAuiToolBar, public BaseWindow {
public:
  ToolbarBase(wxWindow *parent, wxWindowID id,
              const wxPoint &pos = wxDefaultPosition,
              const wxSize &size = wxDefaultSize,
              long style = wxAUI_TB_DEFAULT_STYLE | wxAUI_TB_OVERFLOW |
                           wxAUI_TB_VERTICAL,
              const wxString &name = wxPanelNameStr);

protected:
    wxDECLARE_EVENT_TABLE();

private:
};

} // namespace Widget
} // namespace Core

#endif // _coreToolbarBase_H
