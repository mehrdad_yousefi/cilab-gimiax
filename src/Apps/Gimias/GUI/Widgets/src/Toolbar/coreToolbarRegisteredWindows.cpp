/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreToolbarRegisteredWindows.h"
#include "coreBaseWindowFactories.h"
#include "coreWxMitkGraphicalInterface.h"
#include "corePluginTab.h"
#include "coreSelectionToolboxWidget.h"
#include "corePluginTabFactory.h"
#include "coreBaseWindowFactorySearch.h"

#include <wx/wupdlock.h>

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(Core::Widgets::ToolbarRegisteredWindows, ToolbarBase)
    EVT_TOOL (wxID_ANY, ToolbarRegisteredWindows::OnSelectedTool)
END_EVENT_TABLE()


Core::Widgets::ToolbarRegisteredWindows::ToolbarRegisteredWindows(
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
    ToolbarBase(parent, id, pos, size, style, name)
{
	SetName( "Toolbar Window Factory" );

	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );
	baseWindowFactory->GetFactoriesHolder()->AddObserver(
		this,
		&ToolbarRegisteredWindows::UpdateTools );

}

void Core::Widgets::ToolbarRegisteredWindows::OnInit( )
{
	GetPluginTab()->GetWindowsMapHolder()->AddObserver( 
		this,
		&ToolbarRegisteredWindows::UpdateTools );
}

void Core::Widgets::ToolbarRegisteredWindows::UpdateTools( )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	if ( graphicalIface.IsNull( ) )
	{
		return;
	}

	BaseWindowFactories::Pointer baseWindowFactory = graphicalIface->GetBaseWindowFactory( );
	if ( baseWindowFactory.IsNull( ) )
	{
		return;
	}

	if ( GetPluginTab() == NULL )
	{
		return;
	}

	size_t toolCount = GetToolCount();

	BaseWindowFactorySearch::Pointer search = BaseWindowFactorySearch::New( );
	search->SetType( m_ToolsType );
	search->Update( );
	std::list<std::string> windowsList = search->GetFactoriesNames();

	SelectionToolboxWidget* selectionToolboxWidget;
	GetPluginTab()->GetWidget( wxID_SelectionToolboxWidget, selectionToolboxWidget );
	if ( selectionToolboxWidget == NULL )
	{
		return;
	}

	std::list<std::string>::iterator it;
	for ( it = windowsList.begin( ) ; it != windowsList.end() ; it++ )
	{
		ToolsMapType::iterator itTool;
		itTool = m_ToolWidgetMap.find( *it );

		// Add icon to toolbar
		if ( itTool == m_ToolWidgetMap.end() )
		{
			AddWidgetTool( *it );
		}

		// Change status
		itTool = m_ToolWidgetMap.find( *it );
		if ( GetPluginTab() && itTool != m_ToolWidgetMap.end() )
		{
			SelectionToolWidget* widget;
			widget = selectionToolboxWidget->GetSelectionToolWidget( );
			bool enabled = false;
			if ( widget )
			{
				enabled = widget->GetFactoryName() == itTool->first;
			}
			ToggleTool( itTool->second, enabled );
		}
	}

	// Remove unregistered tools
	ToolsMapType::iterator itTool;
	for ( itTool = m_ToolWidgetMap.begin( ) ; itTool != m_ToolWidgetMap.end() ; itTool++ )
	{
		if ( baseWindowFactory->FindFactory( itTool->first ).IsNull() )
		{
			DeleteTool( itTool->second );
			m_ToolWidgetMap.erase( itTool );
			break;
		}
	}

	// Update toolbar
	if ( toolCount != GetToolCount( ) )
	{
		Realize( );

		// Update pane info
		wxAuiPaneInfo& paneInfo = GetPluginTab()->GetAuiPaneInfo( this );
		if ( paneInfo.IsOk( ) )
		{
			paneInfo.best_size = GetClientSize();
			GetPluginTab()->UpdateAuiManager();
		}
	}

}

void Core::Widgets::ToolbarRegisteredWindows::OnSelectedTool( wxCommandEvent& event )
{
	try
	{
		wxAuiToolBarItem* tool = FindTool( event.GetId() );

		// On Windows 7, the message WM_SETREDRAW creates a flickering
		// wxWindowUpdateLocker noUpdates( GetPluginTab() );

		SelectionToolboxWidget* selectionToolboxWidget;
		GetPluginTab()->GetWidget( wxID_SelectionToolboxWidget, selectionToolboxWidget );

		if ( GetToolToggled( event.GetId() ) )
		{
			selectionToolboxWidget->SetToolByName( tool->GetLabel().ToStdString() );
			selectionToolboxWidget->Start();
		}
		else
		{
			selectionToolboxWidget->Stop();
			selectionToolboxWidget->SetToolByName( "None" );
		}

		GetPluginTab()->ShowWindow( wxID_SelectionToolboxWidget, GetToolToggled( event.GetId() ) );
	}
	coreCatchExceptionsReportAndNoThrowMacro( ToolbarRegisteredWindows::OnSelectedTool )

	UpdateTools( );
}


void Core::Widgets::ToolbarRegisteredWindows::AddWidgetTool( 
	const std::string &factoryName )
{
	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );
	Core::WindowConfig config;
	baseWindowFactory->GetWindowConfig( factoryName, config );

	const char* const* xpmBitmap = config.GetBitmap();
	if ( xpmBitmap == NULL )
	{
		return;
	}

	wxBitmap bitmap;
	bitmap = wxBitmap( xpmBitmap );
	if ( !bitmap.IsOk() )
	{
		return;
	}

	wxWindowID id = wxNewId();
	Core::Widgets::ToolbarBase::AddTool(
		id, 
		config.GetCaption(),
		bitmap, 
		config.GetCaption(), 
		wxITEM_CHECK );

	m_ToolWidgetMap[ factoryName ] = id;

	Realize( );
}

void Core::Widgets::ToolbarRegisteredWindows::SetPluginTab( Core::Widgets::PluginTab* val )
{
	ToolbarBase::SetPluginTab( val );

	UpdateTools();
}