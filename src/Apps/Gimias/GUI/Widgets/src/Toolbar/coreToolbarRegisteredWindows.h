/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreToolbarRegisteredWindows_H
#define _coreToolbarRegisteredWindows_H

#include "gmWidgetsWin32Header.h"

#include "coreToolbarBase.h"

namespace Core
{
namespace Widgets
{

/**
\brief Automatically create tools from  registered windows on BaseWindowFactory
\ingroup gmWidgets
\author Xavi Planes
\date 1 April 2010
*/
class GMWIDGETS_EXPORT ToolbarRegisteredWindows: public ToolbarBase {
public:
	typedef std::map<std::string,wxWindowID> ToolsMapType;
public:

	ToolbarRegisteredWindows(wxWindow* parent, wxWindowID id, 
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = wxAUI_TB_DEFAULT_STYLE, 
		const wxString& name = wxPanelNameStr);

	//!
	void SetToolsState(WIDGET_TYPE val) { m_ToolsType = val; }

	//!
	void SetPluginTab(Core::Widgets::PluginTab* val);

	//!
	void OnInit( );

protected:
	//! Update tools when BaseWindowFactory changes
	virtual void UpdateTools();

	//! Show selected tool
	void OnSelectedTool( wxCommandEvent& event );

	//! Add a new widget tool using factory name of Registered Window
	void AddWidgetTool( const std::string &factoryName );

    wxDECLARE_EVENT_TABLE();
private:

	//!
	WIDGET_TYPE m_ToolsType;

	//! From registered window factory name to tool ID
	ToolsMapType m_ToolWidgetMap;
};

} // namespace Widget
} // namespace Core

#endif // _coreToolbarRegisteredWindows_H
