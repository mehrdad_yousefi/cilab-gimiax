/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreToolbarWindows_H
#define _coreToolbarWindows_H

#include "gmWidgetsWin32Header.h"

#include "coreWindowConfig.h"
#include "coreToolbarPluginTab.h"

namespace Core
{
namespace Widgets
{

/**
\brief Open different widgets
\ingroup gmWidgets
\author Xavi Planes
\date 1 April 2010
*/
class GMWIDGETS_EXPORT ToolbarWindows: public ToolbarPluginTab {
public:
	//!
	coreDefineBaseWindowFactory( Core::Widgets::ToolbarWindows );

	//!
	ToolbarWindows(wxWindow* parent, wxWindowID id = wxID_ANY, 
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = wxAUI_TB_DEFAULT_STYLE, 
		const wxString& name = wxPanelNameStr);

protected:
	//!
	void AddWidgetTool( wxWindow* window );

    wxDECLARE_EVENT_TABLE();
private:
};

} // namespace Widget
} // namespace Core

#endif // _coreToolbarWindows_H
