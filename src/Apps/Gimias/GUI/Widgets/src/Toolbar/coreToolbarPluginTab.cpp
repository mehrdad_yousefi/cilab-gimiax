/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreToolbarPluginTab.h"
#include "corePluginTab.h"
#include "corePluginTabFactory.h"

#include <wx/wupdlock.h>

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(Core::Widgets::ToolbarPluginTab, Core::Widgets::ToolbarBase)
	EVT_TOOL (wxID_ANY, ToolbarPluginTab::OnSelectedTool)
END_EVENT_TABLE()


Core::Widgets::ToolbarPluginTab::ToolbarPluginTab(
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
    Core::Widgets::ToolbarBase(parent, id, pos, size, style, name)
{
}

Core::BaseProcessor::Pointer Core::Widgets::ToolbarPluginTab::GetProcessor()
{
	return NULL;
}

void Core::Widgets::ToolbarPluginTab::SetPluginTab( Core::Widgets::PluginTab* val )
{
	if ( GetPluginTab() )
	{
		GetPluginTab()->GetWindowsMapHolder()->RemoveObserver( 
			this,
			&ToolbarPluginTab::UpdateState );
	}

	BaseWindow::SetPluginTab( val );

	if ( GetPluginTab() )
	{
		GetPluginTab()->GetWindowsMapHolder()->AddObserver( 
			this,
			&ToolbarPluginTab::UpdateState );

		UpdateState( );
	}

}

void Core::Widgets::ToolbarPluginTab::UpdateState()
{
	if ( !GetPluginTab() )
	{
		return;
	}

	size_t toolCount = GetToolCount();

	// Add new tools/update help info/change status
	PluginTab::WindowMapType windows = GetPluginTab()->GetAllWindows();
	PluginTab::WindowMapType::iterator it;
	for ( it = windows.begin() ; it != windows.end() ; it++ )
	{
		wxWindowID windowID = (*it)->GetId( );
		std::map<wxWindowID,wxWindowID>::iterator itTool;
		itTool = m_ToolWidgetMap.find( windowID );

		// Add icon to toolbar
		if ( itTool == m_ToolWidgetMap.end() )
		{
			size_t n = GetToolCount();
			AddWidgetTool( GetPluginTab()->GetWindow( windowID ) );
		}
		else
		{
			wxAuiToolBarItem* tool = FindTool( itTool->second );
			if ( tool && tool->GetShortHelp() != (*it)->GetName() )
			{
				tool->SetShortHelp( (*it)->GetName() );
			}
		}

		// Change status
		itTool = m_ToolWidgetMap.find( (*it)->GetId( ) );
		if ( itTool != m_ToolWidgetMap.end() )
		{
			bool shown = GetPluginTab()->IsWindowShown( windowID );
			ToggleTool( itTool->second, shown );
		}
	}

	// Remove tools of removed windows
	std::map<wxWindowID,wxWindowID>::iterator itTool;
	std::list<wxWindowID> removedTools;
	for ( itTool = m_ToolWidgetMap.begin() ; itTool != m_ToolWidgetMap.end() ; itTool++ )
	{
		if ( !GetPluginTab()->GetWindow( itTool->first ) )
		{
			size_t n = GetToolCount();
			DeleteTool( itTool->second );
			removedTools.push_back( itTool->first );
		}
	}

	std::list<wxWindowID>::iterator itRemoved;
	for ( itRemoved = removedTools.begin() ; itRemoved != removedTools.end() ; itRemoved++ )
	{
		m_ToolWidgetMap.erase( *itRemoved );
	}

	// Update toolbar
	if ( toolCount != GetToolCount( ) )
	{
		Realize( );

		// Update pane info
		wxAuiPaneInfo& paneInfo = GetPluginTab()->GetAuiPaneInfo( this );
		if ( paneInfo.IsOk( ) )
		{
			paneInfo.best_size = GetClientSize();
			GetPluginTab()->UpdateAuiManager();
		}
	}

	Refresh( );
}

void Core::Widgets::ToolbarPluginTab::AddWidgetTool( wxWindow* window )
{
	Core::BaseWindow* baseWindow = dynamic_cast<Core::BaseWindow*> ( window );
	if ( baseWindow == NULL )
	{
		return;
	}

	wxBitmap bitmap;
	if ( baseWindow->GetBitmap() )
	{
		bitmap = wxBitmap( baseWindow->GetBitmap() );
	}
	else if ( !baseWindow->GetBitmapFilename().empty() )
	{
		Core::IO::File::Pointer file = Core::IO::File::New();
		file->SetFileNameFullPath( baseWindow->GetBitmapFilename() );
		if ( file->Exists() )
		{
			wxImage img;
			img.LoadFile( file->GetFileNameFullPath() );
			if ( img.IsOk() )
			{
				bitmap = wxBitmap( img );
			}
		}
	}
	else
	{
		BaseWindowFactories::Pointer baseWindowFactory;
		baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

		WindowConfig config;
		bool success = baseWindowFactory->GetWindowConfig( baseWindow->GetFactoryName(), config );
		if ( success && config.GetBitmap() )
		{
			bitmap = wxBitmap( config.GetBitmap() );
		}
	}

	if ( !bitmap.IsOk() )
	{
		return;
	}

	wxWindowID id = wxNewId();
	AddTool(id, window->GetName(), bitmap );

	m_ToolWidgetMap[ window->GetId( ) ] = id;
}

void Core::Widgets::ToolbarPluginTab::OnSelectedTool( wxCommandEvent& event )
{
	wxAuiToolBarItem* tool = FindTool( event.GetId() );
	if ( tool == NULL )
	{
		event.Skip();
		return;
	}

	// If window is not present in plugin tab, skip this event to be processed
	// by other handler
	if ( !GetPluginTab()->GetWindow( tool->GetLabel() ) )
	{
		event.Skip();
		return;
	}

	GetPluginTab()->ShowWindow( tool->GetLabel().ToStdString(), GetToolToggled( event.GetId() ) );

	UpdateState( );
}

void Core::Widgets::ToolbarPluginTab::AddTool( 
	wxWindowID id, 
	const wxString &label, 
	const wxBitmap &bitmap )
{
	wxAuiToolBar::AddTool(
		id, 
		label,
		bitmap, 
		label, 
		wxITEM_CHECK );

}

wxWindowID Core::Widgets::ToolbarPluginTab::GetWindowID( wxWindowID toolID )
{
	std::map<wxWindowID,wxWindowID>::iterator itTool;
	for (itTool = m_ToolWidgetMap.begin( ); itTool != m_ToolWidgetMap.end( ); itTool++ )
	{
		if ( itTool->second == toolID )
		{
			return itTool->first;
		}
	}

	return wxID_ANY;
}


