/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreToolbarPluginTab_H
#define _coreToolbarPluginTab_H

#include "gmWidgetsWin32Header.h"

#include "coreToolbarBase.h"
#include "coreWindowConfig.h"

namespace Core
{
namespace Widgets
{

/**
\brief Automatically create tools from BaseWindow bitmaps and update
shown/hidden state
\ingroup gmWidgets
\author Xavi Planes
\date 1 April 2010
*/
class GMWIDGETS_EXPORT ToolbarPluginTab: public Core::Widgets::ToolbarBase {
public:

	ToolbarPluginTab(wxWindow* parent, wxWindowID id, 
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = wxAUI_TB_DEFAULT_STYLE, 
		const wxString& name = wxPanelNameStr);


	//!
	Core::BaseProcessor::Pointer GetProcessor( );

protected:
	//!
	virtual void UpdateState();

	//!
	virtual void AddWidgetTool( wxWindow* window );

	//!
	void SetPluginTab(Core::Widgets::PluginTab* val);

	//!
	virtual void OnSelectedTool( wxCommandEvent& event );

	//!
	virtual void AddTool( 
		wxWindowID id, const wxString &label, const wxBitmap &bitmap );

	//! Return Window ID from tool ID (using m_ToolWidgetMap)
	wxWindowID GetWindowID( wxWindowID toolID );

    wxDECLARE_EVENT_TABLE();
protected:

	//! From window id to tool ID
	std::map<wxWindowID,wxWindowID> m_ToolWidgetMap;
};

} // namespace Widget
} // namespace Core

#endif // _coreToolbarPluginTab_H
