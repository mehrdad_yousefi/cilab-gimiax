/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreToolbarWorkingArea_H
#define _coreToolbarWorkingArea_H

#include "gmWidgetsWin32Header.h"

#include "coreWorkingAreaFactory.h"
#include "coreToolbarPluginTab.h"

#define wxID_WORKING_AREA_TOOL			wxID( "wxID_WORKING_AREA_TOOL" )

namespace Core
{
namespace Widgets
{

/**
\brief Toolbar for selecting WorkingArea
\ingroup gmWidgets
\author Xavi Planes
\date 5 Mar 2010
*/
class GMWIDGETS_EXPORT ToolbarWorkingArea: public ToolbarPluginTab {
public:
	//!
	coreDefineBaseWindowFactory( Core::Widgets::ToolbarWorkingArea );

	//!
	ToolbarWorkingArea(wxWindow* parent, wxWindowID id = wxID_ANY, 
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = wxAUI_TB_DEFAULT_STYLE, 
		const wxString& name = wxPanelNameStr);

	//!
	~ToolbarWorkingArea( );

	//! Add observer of active working area state
	void Init( );

	//!
	void OnLinkWorkingAreas(wxCommandEvent& event);
	
	//!
	void SetPluginTab(Core::Widgets::PluginTab* val);

protected:
	//!
	void UpdateState();

	//!
	void SelectWorkingArea( wxWindowID type );

	//!
	void AddWidgetTool( wxWindow* window );

	//!
	virtual void AddTool( 
		wxWindowID id, const wxString &label, const wxBitmap &bitmap );

	//!
	void OnSelectedTool( wxCommandEvent& event );

    wxDECLARE_EVENT_TABLE();
private:
	//!
	wxAuiToolBarItemArray m_append_items;

};

} // namespace Widget
} // namespace Core

#endif // _coreToolbarWorkingArea_H
