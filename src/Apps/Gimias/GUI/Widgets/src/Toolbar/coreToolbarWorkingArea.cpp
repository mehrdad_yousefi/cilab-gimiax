/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreToolbarWorkingArea.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreKernel.h"
#include "coreMultiRenderWindow.h"
#include "corePluginTab.h"
#include "coreDataEntityListBrowser.h"
#include "coreRenderWindowContainer.h"

//#include ".xpm"
#include "Link.xpm"
#include "UnLink.xpm"

#include <wx/artprov.h>

#define wxID_LINK_WORKING_AREAS wxID("wxID_LINK_WORKING_AREAS")

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(Core::Widgets::ToolbarWorkingArea, Core::Widgets::ToolbarPluginTab)
  EVT_TOOL(wxID_LINK_WORKING_AREAS, Core::Widgets::ToolbarWorkingArea::OnLinkWorkingAreas)
  EVT_MENU(	wxID_ANY, ToolbarWorkingArea::OnSelectedTool)
END_EVENT_TABLE()


Core::Widgets::ToolbarWorkingArea::ToolbarWorkingArea(
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
		ToolbarPluginTab(parent, id, pos, size, style, name)
{
	wxBitmap bitmap;
	bitmap = wxBitmap( link_xpm );
	Core::Widgets::ToolbarBase::AddTool(
		wxID_LINK_WORKING_AREAS, 
		_T("Link/Unlink render windows"),
		bitmap, 
		_T("Link/Unlink render windows"),
		wxITEM_CHECK );

	SetOverflowVisible( true );

	Realize();

}

Core::Widgets::ToolbarWorkingArea::~ToolbarWorkingArea( )
{
	// Unbind events, if main window is still available
	Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
	gIface = Core::Runtime::Kernel::GetGraphicalInterface();
	wxFrame* mainWindow = dynamic_cast<wxFrame*> ( gIface->GetMainWindow( ) );
	if (!mainWindow)
	{
		return;
	}

	wxEvtHandler* mainWindowEvtHandler = mainWindow->GetMenuBar()->GetEventHandler();
	const int append_items_size = m_append_items.GetCount();
	for(int i=0; i < append_items_size; i++)
	{
		wxAuiToolBarItem item = m_append_items.Item(i);
		mainWindow->GetMenuBar()->GetEventHandler()->Unbind( wxEVT_COMMAND_TOOL_CLICKED, &ToolbarWorkingArea::OnSelectedTool, this, item.GetId());
	}

}

void Core::Widgets::ToolbarWorkingArea::OnLinkWorkingAreas( wxCommandEvent& event )
{
	RenderWindowContainer* window;
	window = dynamic_cast<RenderWindowContainer*> ( GetPluginTab()->GetCurrentWorkingArea() );
	if ( window )
	{
		window->LinkWindows( GetToolToggled( wxID_LINK_WORKING_AREAS ) );
	}

	UpdateState( );
}

void Core::Widgets::ToolbarWorkingArea::Init()
{
	if ( !GetPluginTab() || !GetPluginTab()->GetWorkingAreaManager() )
	{
		return;
	}

	IntHolderType::Pointer workingAreaHolder;
	workingAreaHolder = GetPluginTab()->GetWorkingAreaManager()->GetActiveWorkingAreaHolder();
	workingAreaHolder->AddObserver( 
		this,
		&ToolbarWorkingArea::UpdateState );
}

void Core::Widgets::ToolbarWorkingArea::UpdateState()
{
	ToolbarPluginTab::UpdateState();

	if ( !GetPluginTab() )
	{
		return;
	}

	// Update active working area
	wxWindow *workingArea = GetPluginTab( )->GetWorkingAreaManager( )->GetActiveWorkingArea( );
	if ( workingArea && FindTool( wxID_WORKING_AREA_TOOL ) != NULL )
	{
		std::map<wxWindowID,wxWindowID>::iterator itTool;
		itTool = m_ToolWidgetMap.find( workingArea->GetId( ) );
		if ( itTool != m_ToolWidgetMap.end() )
		{
			bool shown = GetPluginTab()->IsWindowShown( workingArea->GetId( ) );
			size_t count = m_append_items.GetCount();
			for (int i = 0; i < count; ++i)
			{
				if ( m_customOverflowAppend[i].GetId( ) == itTool->second )
				{
					SetToolBitmap( wxID_WORKING_AREA_TOOL, m_customOverflowAppend[i].GetBitmap( ) );
					Refresh( );
				}
			}
		}
	}

	// Update link tool state
	RenderWindowContainer* window;
	window = dynamic_cast<RenderWindowContainer*> ( GetPluginTab()->GetCurrentWorkingArea() );
	if ( window )
	{
		EnableTool( wxID_LINK_WORKING_AREAS, true );

		if ( window->GetStateHolder()->GetSubject().m_LinkedWindows )
		{
			ToggleTool( wxID_LINK_WORKING_AREAS, true );
		}
		else
		{
			ToggleTool( wxID_LINK_WORKING_AREAS, false );
		}

	}
	else
	{
		EnableTool( wxID_LINK_WORKING_AREAS, false );
	}
}


void Core::Widgets::ToolbarWorkingArea::AddWidgetTool( wxWindow* window )
{
	BaseWindow* baseWindow = GetPluginTab()->GetWorkingArea( window->GetId() );
	if ( !baseWindow )
	{
		return;
	}

	RenderWindowContainer* container;
	container = dynamic_cast<RenderWindowContainer*> ( baseWindow );
	if ( container && !container->CheckValidProperties( ) )
	{
		return;
	}

	ToolbarPluginTab::AddWidgetTool( window );
}


void Core::Widgets::ToolbarWorkingArea::AddTool( 
	wxWindowID id, 
	const wxString &label, 
	const wxBitmap &bitmap )
{

	// Add working area tool
	if ( FindTool( wxID_WORKING_AREA_TOOL ) == NULL )
	{
		Core::Widgets::ToolbarBase::AddTool(
			wxID_WORKING_AREA_TOOL, 
			_T("Working area"),
			bitmap, 
			_T("Working area"), 
			wxITEM_NORMAL);
	}

	// Layout
	wxAuiToolBarItemArray prepend_items;
	wxAuiToolBarItem item;

	item.SetKind(wxITEM_NORMAL);
    item.SetId(id);
    item.SetLabel( label );
	item.SetBitmap( bitmap );

	// Bind to main window event handler
	Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
	gIface = Core::Runtime::Kernel::GetGraphicalInterface();
	wxFrame* mainWindow = dynamic_cast<wxFrame*> ( gIface->GetMainWindow( ) );
	mainWindow->GetMenuBar()->GetEventHandler()->Bind( wxEVT_COMMAND_TOOL_CLICKED, &ToolbarWorkingArea::OnSelectedTool, this, id);

    m_append_items.Add(item);

	SetCustomOverflowItems(prepend_items, m_append_items);
}

void Core::Widgets::ToolbarWorkingArea::SetPluginTab( Core::Widgets::PluginTab* val )
{
	ToolbarPluginTab::SetPluginTab( val );
	Init( );
	UpdateState();
}

void Core::Widgets::ToolbarWorkingArea::OnSelectedTool( wxCommandEvent& event )
{
	wxWindowID windowID = GetWindowID( event.GetId( ) );
	if ( windowID == wxID_ANY )
	{
		event.Skip();
		return;
	}

	// If window is not present in plugin tab, skip this event to be processed
	// by other handler
	if ( !GetPluginTab()->GetWindow( windowID ) )
	{
		event.Skip();
		return;
	}

	GetPluginTab()->ShowWindow( windowID, true );
}
