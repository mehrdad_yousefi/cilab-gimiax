/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreToolbarIO.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreKernel.h"
#include "coreMainMenu.h"
#include "coreConfig.h" // for define DISABLE_FILE_LOADING

//#include ".xpm"
#include "OpenFile.xpm"
#include "SaveFile.xpm"
#include "OpenFolder.xpm"
#include "import.xpm"
#include "export.xpm"

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(Core::Widgets::ToolbarIO, Core::Widgets::ToolbarBase)
END_EVENT_TABLE()


Core::Widgets::ToolbarIO::ToolbarIO(
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
    Core::Widgets::ToolbarBase(parent, id, pos, size, style, name)
{
	wxBitmap bitmap;

#ifndef DISABLE_FILE_LOADING
	bitmap = wxBitmap( openfile_xpm );
	AddTool(wxID_OpenDataMenuItem, _T("Open File"),
		bitmap, _T("Open File"), wxITEM_NORMAL);
#endif

	bitmap = wxBitmap( savefile_xpm );
	AddTool(wxID_SaveDataMenuItem, _T("Save File"),
		bitmap, _T("Save File"),wxITEM_NORMAL );

#ifndef DISABLE_FILE_LOADING
	bitmap = wxBitmap( openfolder_xpm );
	AddTool(wxID_OpenDirectoryMenuItem, _T("Open directory"),
		bitmap, _T("Open directory"), wxITEM_NORMAL);

	bitmap = wxBitmap( import_xpm );
	AddTool(wxID_OpenSessionMenuItem, _T("Open Session"),
		bitmap, _T("Open Session"), wxITEM_NORMAL);

	bitmap = wxBitmap( export_xpm );
	AddTool(wxID_SaveSessionMenuItem, _T("Save Session"),
		bitmap, _T("Save Session"), wxITEM_NORMAL);
#endif

	Realize();
}

void Core::Widgets::ToolbarIO::UpdateState()
{
}

Core::BaseProcessor::Pointer Core::Widgets::ToolbarIO::GetProcessor()
{
	return NULL;
}

