/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreToolbarBase.h"

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(Core::Widgets::ToolbarBase, wxAuiToolBar)
END_EVENT_TABLE()


Core::Widgets::ToolbarBase::ToolbarBase(
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
    wxAuiToolBar(parent, id, pos, size, style)
{
	SetName( name );
}
