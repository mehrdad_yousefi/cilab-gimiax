/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreToolbarWindows.h"
#include "corePluginTab.h"
#include "corePluginTabFactory.h"
#include "coreMainMenu.h"

#include "download.xpm"

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(Core::Widgets::ToolbarWindows, Core::Widgets::ToolbarPluginTab)
END_EVENT_TABLE()


Core::Widgets::ToolbarWindows::ToolbarWindows(
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
    ToolbarPluginTab(parent, id, pos, size, style, name)
{
	Core::Widgets::ToolbarBase::AddTool(
		wxID_CheckForUpdates, "Check for Updates", 
		wxBitmap( download_xpm ), "Check for Updates", wxITEM_NORMAL ); // wxITEM_CHECK ); --> ??
}

void Core::Widgets::ToolbarWindows::AddWidgetTool( wxWindow* window )
{
	if ( GetPluginTab()->GetWorkingArea( window->GetId() ) )
	{
		return;
	}
	
	Core::BaseWindow* baseWindow = dynamic_cast<Core::BaseWindow*> ( window );
	if ( baseWindow == NULL )
	{
		return;
	}

	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	WindowConfig config;
	bool success = baseWindowFactory->GetWindowConfig( baseWindow->GetFactoryName(), config );
	if ( !success || !( config.GetCategory() == "Windows" || config.GetCategory() == "Visualization" ) )
	{
		return;
	}

	ToolbarPluginTab::AddWidgetTool( window );
}
