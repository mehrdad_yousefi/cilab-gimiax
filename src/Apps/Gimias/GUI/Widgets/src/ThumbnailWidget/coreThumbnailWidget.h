/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreThumbnailWidget_H
#define _coreThumbnailWidget_H

#include "gmWidgetsWin32Header.h"
#include "coreThumbnailWidgetUI.h"
#include "coreBaseWindow.h"

#include "thumbnailctrl.h"

namespace Core
{
namespace Widgets
{

/** 
\brief Widget for Showing thumbnails

\ingroup gmWidgets
\author Xavi Planes
\date 06 August 2009
*/
class GMWIDGETS_EXPORT ThumbnailWidget : 
	public coreThumbnailWidgetUI,
	public Core::BaseWindow
{
public:

	coreDefineBaseWindowFactory( Core::Widgets::ThumbnailWidget )

	//!
	ThumbnailWidget(
		wxWindow* parent, 
		int id, 
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=wxDEFAULT_DIALOG_STYLE);

	//!
	~ThumbnailWidget(void);

	//!
	void ShowFolder(const wxString& path);

	//!
	void Append( wxString file, wxString title = wxEmptyString );

	//!
	wxThumbnailCtrl* GetThumbnailCtrl() const;

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

private:

    wxDECLARE_EVENT_TABLE();

private:

	//! Thumbnail control
	wxThumbnailCtrl* m_ThumbnailCtrl;
};

} // namespace Widgets
} // namespace Core

#endif // _coreThumbnailWidget_H
