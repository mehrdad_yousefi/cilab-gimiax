/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreToolboxWidget_H
#define _coreToolboxWidget_H

#include "coreBaseWindow.h"
#include <wx/aui/auibook.h>

#define wxID_ToolboxWidget wxID("wxID_ToolboxWidget")
#define wxID_ToolboxWidget_Notebook wxID("wxID_ToolboxWidget_Notebook")


namespace Core{
namespace Widgets{


/** 
\brief Generic toolbox widget where to put all tools

It's a Notebook with icons. The default one contains 4 pages:
- Processing
- Selection
- Visual Properties
- Information

\ingroup gmWidgets
\author Xavi Planes
\date April 2012
*/
class GMWIDGETS_EXPORT ToolboxWidget 
	: public wxScrolledWindow, public BaseWindow
{
public:
	coreDefineBaseWindowFactory(Core::Widgets::ToolboxWidget);
	
	ToolboxWidget(wxWindow* parent, 
						wxWindowID id = wxID_ANY, 
						const wxPoint& pos = wxDefaultPosition, 
						const wxSize& size = wxDefaultSize, 
						long style = wxBORDER_NONE | wxFULL_REPAINT_ON_RESIZE, 
						const wxString& name = wxPanelNameStr);

	//!
	void OnInit();

	//!
	size_t GetToolCount( );

	//!
	int GetSelection( );

	//!
	Core::BaseWindow* GetTool( int index );

	//! Get first tool with a processor
	virtual Core::BaseProcessor::Pointer GetProcessor( );

	//! Redefined
	void ShowChild( BaseWindow* child, bool show = true );

protected:
	//!
	virtual ~ToolboxWidget(void);	

	//! Overwride
	Core::BaseWindow* CreateChild(const std::string &factoryName );

	//! Overwride
	void DestroyChild( BaseWindow* win );

	//!
	void set_properties();
	void do_layout();

    wxDECLARE_EVENT_TABLE();

protected:

private:
	//!
	wxAuiNotebook* m_Notebook;
};

} // Widgets
} // Core

#endif // coreToolboxWidget_H
