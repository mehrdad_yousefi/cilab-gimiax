/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include "wx/wxprec.h"

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include <wx/wupdlock.h>

// Core
#include "coreToolboxWidget.h"
#include "coreBaseWindowFactories.h"
#include "coreWxMitkGraphicalInterface.h"
#include "corePluginTab.h"

#include "toolbox.xpm"

BEGIN_EVENT_TABLE( Core::Widgets::ToolboxWidget, wxScrolledWindow )
END_EVENT_TABLE( )

//!
Core::Widgets::ToolboxWidget::ToolboxWidget(wxWindow* parent, 
	wxWindowID id, 
	const wxPoint& pos, 
	const wxSize& size, 
	long style, 
	const wxString& name)
: wxScrolledWindow(parent, id, pos, size, style, name)
{
	SetBitmap( toolbox_xpm );

	m_Notebook = new wxAuiNotebook(
		this, wxID_ToolboxWidget_Notebook, wxDefaultPosition, wxDefaultSize, 
		wxAUI_NB_TOP | wxAUI_NB_TAB_SPLIT | wxAUI_NB_TAB_MOVE | wxAUI_NB_SCROLL_BUTTONS );

	set_properties( );
	do_layout( );

	SetIsContainerWindow( true );
	SetChildWindowType( WIDGET_TYPE_TOOLBOX );
}

void Core::Widgets::ToolboxWidget::set_properties()
{
	SetScrollRate(10, 10);
}

void Core::Widgets::ToolboxWidget::do_layout()
{
	// layout them
	wxBoxSizer* vlayout = new wxBoxSizer(wxVERTICAL);
	vlayout->Add(m_Notebook, 1, wxEXPAND | wxALL, 4);
	SetSizer(vlayout);
	vlayout->Fit(this);
}

Core::Widgets::ToolboxWidget::~ToolboxWidget(void)
{
}

void Core::Widgets::ToolboxWidget::OnInit( )
{
	UpdateRegisteredWindows( );
}

Core::BaseWindow* Core::Widgets::ToolboxWidget::CreateChild(const std::string &factoryName )
{
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();
	if ( settings->GetPerspective() == Core::Runtime::PERSPECTIVE_PLUGIN &&
		factoryName == "Core::Widgets::WorkflowNavigationWidgetFactory")
	{
		return NULL;
	}

	// Get factory
	Core::BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	// Create window
	BaseWindow *baseWin = baseWindowFactory->CreateBaseWindow( factoryName, m_Notebook );
	
	// Add it to the notebook
	wxWindow* win = dynamic_cast<wxWindow*> ( baseWin );
	m_Notebook->AddPage( win, "", false, 
		baseWin->GetBitmap( ) ? baseWin->GetBitmap( ) : wxNullBitmap );

	// Init processor observers
	baseWin->InitProcessorObservers( true );

	// OnInit can change the processor observers
	GetPluginTab( )->InitBaseWindow( baseWin );

	return baseWin;
}

void Core::Widgets::ToolboxWidget::DestroyChild( Core::BaseWindow* win )
{
	wxWindow* page = dynamic_cast<wxWindow*> ( win );
	int index = m_Notebook->GetPageIndex( page );

	if ( index != wxNOT_FOUND )
	{
		m_Notebook->RemovePage(index );
		page->Destroy( );
	}
}

Core::BaseProcessor::Pointer Core::Widgets::ToolboxWidget::GetProcessor( )
{
	for ( int i = 0 ; i < GetToolCount( ) ; i++ )
	{
		if ( GetTool( i )->GetProcessor( ).IsNotNull( ) )
		{
			return GetTool( i )->GetProcessor( );
		}
	}

	return NULL;
}

int Core::Widgets::ToolboxWidget::GetSelection( )
{
	return m_Notebook->GetSelection();
}

Core::BaseWindow* Core::Widgets::ToolboxWidget::GetTool( int index )
{
	return dynamic_cast<Core::BaseWindow*> ( m_Notebook->GetPage( index ) );
}

size_t Core::Widgets::ToolboxWidget::GetToolCount( )
{
	return m_Notebook->GetPageCount();
}

void Core::Widgets::ToolboxWidget::ShowChild( BaseWindow* child, bool show /*= true*/ )
{
	for ( int i = 0 ; i < GetToolCount( ) ; i++ )
	{
		if ( GetTool( i ) == child )
		{
			m_Notebook->SetSelection( i );
		}
	}
	
}

