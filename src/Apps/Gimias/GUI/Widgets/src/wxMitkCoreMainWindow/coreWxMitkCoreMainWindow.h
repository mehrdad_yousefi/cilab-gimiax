/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreWxMitkCoreMainWindow_H
#define coreWxMitkCoreMainWindow_H

#include "gmWidgetsWin32Header.h"
#include "coreObject.h"
#include "coreBaseMainWindow.h"
#include "coreWindowConfig.h"
#include "coreWorkingAreaStorage.h"
#include "coreWxUpdateCallbackEvent.h"
#include "coreStyleManager.h"

#include <wx/frame.h>
#include <wx/docview.h>
#include <wx/aui/auibook.h>
#include "wxID.h"

#include <string>

#define wxID_TabContainer wxID( "wxID_TabContainer" )

class wxNotebook;
class wxProgressDialog;

namespace Core
{
namespace Runtime { class wxMitkGraphicalInterface; }

class WxProcessorEvtHandler;

namespace Widgets
{

class MacroPannelWidget;
class LogFileViewer;
class SplashScreen;
class PluginTab;
class MainMenu;
class WorkflowEditorWidget;
class WorkflowManagerWidget;
class PreferencesDialog;
class WebUpdateDialog;
class CustomApplicationManagerWidget;

/** 
\brief Main window for the Core application. It is instantiated when the 
graphical interface is created.
It is implemented using wxWidgets and Mitk.

The main window contains a macro panel. This panel displays messages and 
warnings, and is intended to be used for typing script commands. Also, 
it displays the script-equivalent of operations performed in the GUI.

\ingroup gmWidgets
\author Juan Antonio Moya
\date 03 Jan 2008
*/
class GMWIDGETS_EXPORT wxMitkCoreMainWindow : 
	public wxFrame,
	public Core::Widgets::BaseMainWindow
{

public:
	coreClassNameMacro(Core::Widgets::wxMitkCoreMainWindow)
	typedef wxMitkCoreMainWindow Self; 
	typedef boost::signal0<void> SignalType;
	typedef SignalType::slot_function_type SlotType;
	typedef std::pair<int, SlotType> IdCallbackPair;
	typedef std:: map <int, SlotType> MenuEventCallbackMap;

	enum DialogResult { 
		DialogResult_YES, 
		DialogResult_NO, 
		DialogResult_OK, 
		DialogResult_CANCEL, 
		DialogResult_ERROR };

	wxMitkCoreMainWindow(
		wxWindow* parent, 
		wxWindowID id, 
		const wxString& title, 
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize,
		long style = wxDEFAULT_FRAME_STYLE, 
		const wxString& name = wxT("wxMitkCoreMainWindow"));
	~wxMitkCoreMainWindow(void);
	
	//!
	void Initialize( );

	//!
	wxMitkCoreMainWindow::DialogResult AskQuestionToUserYesOrNo(
		const std::string& message, 
		const std::string& caption = "Question");
	//!
	wxMitkCoreMainWindow::DialogResult AskQuestionToUserOkOrCancel(
		const std::string& message, 
		const std::string& caption = "Question");
	//!
	void ReportWarning(const std::string& message, bool showAlsoDialog);
	//!
	void ReportError(const std::string& message, bool showAlsoDialog);
	//!
	void ReportMessage(const std::string& message, bool showAlsoDialog);
	//!
	void ReportSplashMessage(const std::string& message);
	//!
	void WriteVerbatimToMacroPanel(const std::string& message);

	/**
	\brief Returns the PluginTab currently selected and displayed in the 
	main window. So it is actually the GUI of the plugin that the user 
	is currently working with.
	*/
	Core::BasePluginTab* GetCurrentPluginTab(void);

	//!
	Core::BasePluginTab* GetLastPluginTab(void);

	//!
	virtual std::list<Core::BasePluginTab*> GetAllPluginTabs( );

	//!
	void CreatePluginTab( const std::string &caption );

	//!
	bool AttachPluginGUI(Core::BasePluginTab* page);
	//!
	void DetachPluginGUI(Core::BasePluginTab* page, bool destroy = false);
	//!
	void ShowLogFileBrowser(void);
	//!
	void ShowProfileManager(void);
	//!
	void ShowImportConfigurationWizard(void);
	//!
	void ShowAboutScreen(void);
	//!
	void ShowRegistrationForm(void);
	//!
	void ShowWebUpdater( bool show = true );
	//!
	void CheckForUpdates( );
	//!
	MainMenu* GetMainMenu( );

	//! Redefined
	void RegisterWindow( 
		Core::BaseWindow *baseWindow,
		Core::WindowConfig config = Core::WindowConfig( ) );

	//! Redefined
	void RegisterWindow( 
		const std::string &factoryName,
		Core::WindowConfig config = Core::WindowConfig( ) );
	
	//! Redefined
	void UnRegisterWindow( const std::string &factoryName );

	//! Redefined
	void AttachWorkflow(Workflow::Pointer workflow);

	//! Redefined
	void ShowWorkflowEditor( bool show = true );

	//! Redefined
	void ShowWorkflowManager( bool show = true );

	//! Redefined
	void UpdateActiveWorkflowLayout( );

	//! Redefined
	void RestartApplication( );

	//! Redefined
	void UpdateLayout( );

	//! Redefined
	void InitializePerspective( );

	//! Redefined
	void OnGraphicalInterfaceInitialized( );

	//! Redefined
	void RegisterModule( const std::string &pluginProvider, ModuleDescription *module );

	//! Redefined
	void UnRegisterModule( const std::string &pluginProvider, ModuleDescription *module );

	//! Redefined
	std::string FindFactoryNameByModule( ModuleDescription *module );

	//! Redefined
	bool ShowMissingDataEntityFieldsWizard(
		Core::DataEntity::Pointer dataEntity);

	//! Redefined
	void ShowPreferencesdialog(void);

	//! Redefined
	void ShowCustomApplicationManager(void);

	//! 
	blTagMap::Pointer GetProperties( );

	//!
	void SetProperties( blTagMap::Pointer properties );

	//!
	PreferencesDialog* GetPreferencesDialog() const;

	//!
	void ShowSplashScreen( bool show );

	//!
	void SetTitle( const std::string &title );

	//!
	bool Show( bool show );

	//! Redefined
	virtual void Lock( bool lock = true );
	
	//! Redefined
	void UpdatePluginConfiguration( );

	//! Clean Temporary Data generated by plugins
	void CleanPluginTemporaryData( );

protected:
	//!
	void CreateWidgets(void);
	/**
	\brief Creates the menu for the graphical interface and attaches the 
	event handlers to the menu items
	*/
	void CreateMainMenu(void);

	//! Creates the context menu
	void CreateContextMenu(void);

	//!
	void CloseWindow(void);

	//!
	void CreateSplashScreen();

	//! Create custom status bar
	wxStatusBar* OnCreateStatusBar(
		int number, 
		long style, 
		wxWindowID id, 
		const wxString& name);

	//! Create a caption depending on ModuleDescription type: ITK, VTK, 3D Slicer, ...
	std::string CreateModuleDescriptionCaption( ModuleDescription *module );

private:

	//!
	void OnEventToRedirectToMenuBar(wxCommandEvent& event);

	//!
	void OnWxMouseEvent(wxMouseEvent& event);

	//!
	void OnClose(wxCloseEvent& event);

	//! m_TabContainer page has changed
	void OnPageChanged( wxAuiNotebookEvent& event );

	//! Create processing dialog
	void CreateProcessingDialog( );

	//! Create the Style Manager
	void CreateStyleManager( );

	//! Add observer to state holder
	void AddStateHolderObserver( );

	//! Show/Hide processing dialog
	void ShowProcessingDialog( bool show );

	/**
	\brief Called when Core::Runtime::Environment::GetAppStateHolder() changes
	and show a progress dialog or hide it
	*/
	void OnAppStateChanged( );

	//! Check if a file exist
	bool FileExists(std::string strFilename);

	//!
	void OnUpdateCallback( WxUpdateCallbackEvent& event );

	//!
	void OnMove(wxMoveEvent& event);

	//!
	void OnMaximize( wxMaximizeEvent& event );

private:
	//!
	wxAuiNotebook* m_TabContainer;
	//!
	Core::Widgets::MacroPannelWidget* m_MacroPannel;
	//!
	wxStaticText* m_EmptyLabel;

	/**
	\brief If this is true, at the end of release right button, context 
	menu will be shown
	*/
	bool m_bShowContextMenu;

	//! Context menu
	wxMenu* m_ContextMenu;

	//! Status bar
	wxStatusBar* m_StatusBar;

	//! Processing dialog: "Processing... Please wait"
	wxFrame* m_processingDialog;

	//! Progress dialog
	wxProgressDialog *m_ProgressDialog;

	//!
	WorkflowEditorWidget* m_WorkflowEditorWidget;

	//!
	WorkflowManagerWidget* m_WorkflowManagerWidget;

	//!
	PreferencesDialog* m_PreferencesDialog;

	CustomApplicationManagerWidget* m_CustomApplicationManager;
	
	//!
	blTagMap::Pointer m_Properties;

	//! The StyleManager for the look&feel of the application
	Core::Runtime::StyleManager::Pointer m_StyleManager;	

	//! The startup SplashScreen
	Core::Widgets::SplashScreen* m_SplashScreen;		

	//!
	Core::Widgets::WebUpdateDialog* m_WebUpdateDlg;

	//!
	Core::WxProcessorEvtHandler* m_WxProcessorEvtHandler;
    wxDECLARE_EVENT_TABLE();
};

} // namespace Core
} // namespace Widgets


#endif
