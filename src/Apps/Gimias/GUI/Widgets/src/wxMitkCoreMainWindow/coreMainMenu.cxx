/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include "wx/wxprec.h"

#ifndef WX_PRECOMP
       #include "wx/wx.h"
#endif

#include <wx/wupdlock.h>
#include "wx/busyinfo.h"

#include "coreMainMenu.h"
#include "coreKernel.h"
#include "coreWxMitkGraphicalInterface.h"
#include "corePluginTab.h"
#include "corePluginTabFactory.h"
#include "coreLogger.h"
#include "coreAssert.h"
#include "coreReportExceptionMacros.h"
#include "coreBaseExceptions.h"
#include "coreSplashScreen.h"
#include "coreStyleManager.h"
#include "coreLogFileViewer.h"
#include "coreMacroPannelWidget.h"
#include "coreDirectory.h"
#include "coreDataEntityHelper.h"
#include "coreLoggerHelper.h"
#include "coreWxDataObjectDataEntity.h"
#include "coreMultiRenderWindow.h"
#include "coreStatusBar.h"
#include "coreDataEntityReader.h"
#include "coreDataEntityWriter.h"
#include "coreWorkflowManager.h"
#include "coreWxMitkCoreMainWindow.h"
#include "coreDataTreeHelper.h"
#include "coreUserRegistrationDialog.h"
#include "coreProcessorManager.h"
#include "coreCompatibility.h"
#include "coreBaseWindowFactorySearch.h"

#include "coreSessionWriter.h"
#include "coreSessionReader.h"

#include "coreMovieToolbar.h"
#include "coreProcessingToolboxWidget.h"
#include "coreSelectionToolboxWidget.h"

#include "coreDirectory.h"

#include "coreConfig.h" // for define DISABLE_FILE_LOADING

#include "wxID.h"

#include <wx/notebook.h>
#include <wx/stattext.h>
#include <wx/menu.h>
#include <wx/filedlg.h>
#include <wx/dnd.h>
#include <wx/utils.h>
#include <wx/clipbrd.h>
#include <wx/progdlg.h>

#include "wxMemoryUsageIndicator.h"
#include "wxUnicode.h"
#include "wxCaptureWindow.h"

#include <stdio.h>

#include "itksys/SystemTools.hxx"

#include "blTextUtils.h"

/** Definitions for handling shared libraries in Windows */
#if (defined(_WIN32) || defined(WIN32)) && !defined(SWIG_WRAPPING)
#	undef MAIN_WINDOW_EXPORT
#	define MAIN_WINDOW_EXPORT __declspec(dllexport)
#else
/* unix needs nothing */
#	undef  MAIN_WINDOW_EXPORT
#	define MAIN_WINDOW_EXPORT
#endif

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(MainMenu, wxMenuBar)
	EVT_MENU	(wxID_OpenDataMenuItem, MainMenu::OnMenuOpenFile)	
	EVT_MENU	(wxID_OpenDirectoryMenuItem, MainMenu::OnMenuOpenDirectory)	
	EVT_MENU	(wxID_SaveDataMenuItem, MainMenu::OnMenuSaveFile)
	EVT_MENU	(wxID_SaveImageMenuItem, MainMenu::OnMenuSaveImage)
	EVT_MENU	(wxID_SaveImageWorkingAreaMenuItem, MainMenu::OnMenuSaveWorkingAreaImage)
	EVT_MENU	(wxID_OpenSessionMenuItem, MainMenu::OnMenuOpenSession)
	EVT_MENU	(wxID_AddSessionMenuItem, MainMenu::OnMenuOpenSession)
	EVT_MENU	(wxID_SaveSessionMenuItem, MainMenu::OnMenuSaveSession)
	EVT_MENU	(wxID_ExitMenuItem, MainMenu::OnMenuExit)

	EVT_MENU_RANGE(wxID_FILE1, wxID_FILE9, MainMenu::OnMRUFile)

	EVT_MENU	(wxID_ImportConfigurationMenuItem, MainMenu::OnMenuImportConfiguration)
	EVT_MENU	(wxID_ShowLogMenuItem, MainMenu::OnMenuShowLog)
	EVT_MENU	(wxID_ShowLogFolder, MainMenu::OnMenuShowLogFolder)
	EVT_MENU	(wxID_Preferences, MainMenu::OnMenuPreferences)
	EVT_MENU	(wxID_CustomApplicationManagerMenuItem, MainMenu::OnMenuCustomApplicationManager)
	
	EVT_MENU	(wxID_ReportBug, MainMenu::OnMenuReportBug)
	EVT_MENU	(wxID_UserRegistration, MainMenu::OnMenuUserRegistration)
	EVT_MENU	(wxID_CheckForUpdates, MainMenu::OnMenuCheckForUpdates)
	EVT_MENU	(wxID_DevelopersSite, MainMenu::OnMenuDevelopersSite)
	EVT_MENU	(wxID_FrameworkVideos, MainMenu::OnMenuFrameworkVideos)
	
	EVT_MENU	(wxID_ShowAboutMenuItem, MainMenu::OnMenuShowAbout)
	
	EVT_MENU	(wxID_CopyMenuItem, MainMenu::OnMenuCopy)	
	EVT_MENU	(wxID_PasteMenuItem, MainMenu::OnMenuPaste)	
	
	EVT_MENU	(wxID_WindowResetLayoutMenuItem, MainMenu::OnMenuWindowResetLayout)	
	EVT_MENU	(wxID_WindowMaximizeWorkingAreaMenuItem, MainMenu::OnMenuWindowMaximizeWorkingAreaMenuItem)	
	EVT_MENU	(wxID_WindowShowFullScreen, MainMenu::OnMenuShowFullScreen)	

	EVT_MENU	(wxID_WorkflowManagerMenuItem,MainMenu::OnMenuItemWorkflowManager)
	EVT_MENU	(wxID_PerspectivePluginMenuItem,MainMenu::OnMenuItemPerspectivePlugin)
	EVT_MENU	(wxID_PerspectiveWorkflowMenuItem,MainMenu::OnMenuItemPerspectiveWorkflow)

	EVT_MENU	(wxID_ShowAllToolbarsMenuItem, MainMenu::OnMenuShowAllToolbars)	
	EVT_MENU	(wxID_HideAllToolbarsMenuItem, MainMenu::OnMenuHideAllToolbars)	

END_EVENT_TABLE();

// non recursive search
wxMenuItem *FindChildItem( wxMenu* menu, const wxString& text)
{
	wxString label = wxMenuItem::GetLabelFromText(text);

	wxMenuItemList items = menu->GetMenuItems( );
	wxMenuItem *item = (wxMenuItem *)NULL;
	for ( wxMenuItemList::compatibility_iterator node = items.GetFirst();
		node;
		node = node->GetNext() )
	{
		wxMenuItem *item = node->GetData();
		if ( !item->IsSeparator() )
		{
			if ( item->GetLabel() == label )
				return item;
		}
	}

	return item;
}



MainMenu::MainMenu() : wxMenuBar()
{
	CreateMainMenu( );

	Initialize();
}


MainMenu::~MainMenu()
{
	// Save history file list
	std::vector<std::string> strList;
	for ( unsigned i = 0 ; i < m_FileHistory->GetCount() ; i++ )
	{
		strList.push_back( _U(m_FileHistory->GetHistoryFile( i )) );
	}
	Core::Runtime::Kernel::GetApplicationSettings()->SetMRUList( strList );

	if ( m_FileHistory )
		delete m_FileHistory;
}

/** 
*/
void MainMenu::CreateMainMenu(void)
{
	try
	{
		wxMenu* menu;

		// The file menu
		menu = new wxMenu();
		this->Append(menu, wxT("&File"));
#ifndef DISABLE_FILE_LOADING
		menu->Append(wxID_OpenDataMenuItem, wxT("&Open data\tCtrl+O"), 
			wxT("Open a data file, and load it as a new data entity to work with"), wxITEM_NORMAL);
		menu->Append(wxID_OpenDirectoryMenuItem, wxT("Open &directory\tCtrl+D"), 
			wxT("Open a directory (DICOM)"), wxITEM_NORMAL);
#endif

		menu->Append(wxID_SaveDataMenuItem, wxT("&Save selected data\tCtrl+S"), 
			wxT("Save to a file the data entity that is currently being selected at the Processing Browser"), wxITEM_NORMAL);
		menu->Append(wxID_SaveImageMenuItem, wxT("&Save image of current 3D view"), 
			wxT("Save an image of the data entity that is currently rendered in the 3D view"), wxITEM_NORMAL);
		menu->Append(wxID_SaveImageWorkingAreaMenuItem, wxT("&Save working area as image"), 
			wxT("Save an image of the Working Area"), wxITEM_NORMAL);
		
		menu->AppendSeparator();

#ifndef DISABLE_FILE_LOADING
		menu->Append(wxID_OpenSessionMenuItem, wxT("&Open session"), 
			wxT("Read all data saved in a session and clear current data"), wxITEM_NORMAL);
		menu->Append(wxID_AddSessionMenuItem, wxT("&Add session"), 
			wxT("Read all data saved in a session and add it to the Data tree"), wxITEM_NORMAL);
#endif
		menu->Append(wxID_SaveSessionMenuItem, wxT("&Save session"), 
			wxT("Save all data to session"), wxITEM_NORMAL);

		menu->AppendSeparator();
		menu->Append(wxID_WorkflowManagerMenuItem, wxT("Clinical Workflow Manager"), 
			wxT("Clinical Workflow Manager"));
		wxMenu *subMenu = new wxMenu();
		subMenu->Append(wxID_PerspectivePluginMenuItem, wxT("Plugin"), wxT("Plugin"), wxITEM_RADIO);
		subMenu->Append(wxID_PerspectiveWorkflowMenuItem, wxT("Workflow"), wxT("Workflow"), wxITEM_RADIO);
		menu->Append(wxID_PerspectiveMenuItem, wxT("&Perspective"), subMenu );


		menu->AppendSeparator();
		menu->Append(wxID_ExitMenuItem, wxT("E&xit"), wxT("Exits the application"), wxITEM_NORMAL);

		// Create file history
		m_FileHistory = new wxFileHistory;

#ifndef DISABLE_FILE_LOADING
		m_FileHistory->UseMenu( menu );

		// Fill history file list
		std::vector<std::string> strList;
		strList = Core::Runtime::Kernel::GetApplicationSettings()->GetMRUList( );
		// Start with the last
		for ( int i = int( strList.size() ) - 1 ; i >= 0  ; i-- )
		{
			if ( FileExists(strList[i]) )
				m_FileHistory->AddFileToHistory( _U(strList[ i ]) );
		}
#endif

		// Edit menu
		menu = new wxMenu();
		this->Append(menu, wxT("&Edit"));
		menu->Append(wxID_CopyMenuItem, wxT("&Copy timepoint\tCtrl+C"), 
			wxT("Copy the current timepoint of the selected data"), wxITEM_NORMAL);
		menu->Append(wxID_PasteMenuItem, wxT("&Paste timepoint\tCtrl+V"), 
			wxT("Paste to the selected data timepoint"), wxITEM_NORMAL);
		menu->Append(wxID_Preferences, wxT("Preferences"), 
			wxT("Preferences"), wxITEM_NORMAL);

		//View menu
		menu = new wxMenu();
		this->Append(menu, wxT("&View"));
		menu->Append(wxID_WindowResetLayoutMenuItem, wxT("Reset layout"), 
			wxT("Reset layout"));
		menu->Append(wxID_WindowMaximizeWorkingAreaMenuItem, wxT("Maximize working area"), 
			wxT("Maximize working area"));
		menu->Append(wxID_WindowShowFullScreen, wxT("Show full screen\tF11"), 
			wxT("Show full screen"), wxITEM_CHECK);

		subMenu = new wxMenu();
		menu->AppendSeparator();
		menu->Append(wxID_ANY, wxT("Toolbars"), subMenu);
		subMenu->Append(wxID_ShowAllToolbarsMenuItem, wxT("Show all"), 
			wxT("Show all toolbars"));
		subMenu->Append(wxID_HideAllToolbarsMenuItem, wxT("Hide all"), 
			wxT("Hide all toolbars"));

		// Tools menu
		menu = new wxMenu();
		this->Append(menu, wxT("&Tools"));

		menu = new wxMenu();
		this->Append(menu, wxT("&Selection"));

		// Advanced
		menu = new wxMenu();
		this->Append(menu, wxT("&Advanced"));
		menu->Append(wxID_ImportConfigurationMenuItem, wxT("Import Configuration"), 
			wxT("Import configuration from previous versions"), wxITEM_NORMAL);
		menu->Append(wxID_ShowLogMenuItem, wxT("View &Log file"), 
			wxT("Shows the log file of the application"), wxITEM_NORMAL);
#ifdef _WIN32
		menu->Append(wxID_ShowLogFolder, wxT("Show &Log folder"), 
			wxT("Shows the log folder"), wxITEM_NORMAL);
#endif
		menu->Append(wxID_CustomApplicationManagerMenuItem, wxT("Custom App Manager"), 
			wxT("Custom Application Manager"), wxITEM_NORMAL);


		// Help menu
		menu = new wxMenu();
		this->Append(menu, wxT("&Help"));
		menu->Append(wxID_UserRegistration, wxT("&User registration"), wxT("User registration"), wxITEM_NORMAL);
		menu->Append(wxID_ReportBug, wxT("&Report Bug or Enhancement"), wxT("Report Bug or Enhancement"), wxITEM_NORMAL);
		menu->Append(wxID_CheckForUpdates, wxT("&Check for Updates"), wxT("Check for Updates"), wxITEM_NORMAL);
		menu->AppendSeparator();
		menu->Append(wxID_DevelopersSite, wxT("&Developer resources"), wxT("Developer resources"), wxITEM_NORMAL);
		menu->Append(wxID_FrameworkVideos, wxT("&Watch Videos"), wxT("Watch Videos"), wxITEM_NORMAL);
		menu->AppendSeparator();
		menu->Append(wxID_ShowAboutMenuItem, wxT("&About"), wxT("About this program"), wxITEM_NORMAL);
		

	}
	coreCatchExceptionsAddTraceAndThrowMacro(MainMenu::CreateMainMenu)
}

void MainMenu::Initialize()
{
	//// Main initialization ////
	try
	{

		BaseWindowFactories::Pointer baseWindowFactory;
		baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );
		baseWindowFactory->GetFactoriesHolder()->AddObserver(
			this,
			&MainMenu::UpdateRegisteredWindowsMenuItems );

		UpdatePerspectiveSelection();

        SetExtraStyle(GetExtraStyle() | wxWS_EX_BLOCK_EVENTS);

	}
	coreCatchExceptionsReportAndNoThrowMacro(MainMenu::MainMenu);
}

//!
void MainMenu::OnMenuOpenFile(wxCommandEvent& event)
{
	try
	{	
		std::string fileName(event.GetString().mb_str());

		std::vector<std::string> completePath;
		Core::DataEntity::Pointer father;
		if ( event.GetInt( ) > 0 )
		{
			// Get father DataEntity
			Core::DataContainer::Pointer container = Core::Runtime::Kernel::GetDataContainer();
			if ( container->GetDataEntityList( )->IsInTheList( event.GetInt( ) ) )
			{
				father = container->GetDataEntityList( )->GetDataEntity( event.GetInt( ) );
			}
		}
		
		if ( fileName.empty() )
		{
			// Open file dialog
			std::string filetypes = Core::IO::DataEntityReader::GetFileFilterTypesForRead();
			Core::Runtime::Settings::Pointer settings;
			settings = Core::Runtime::Kernel::GetApplicationSettings();
			std::string dataPath;
			wxFileDialog openFileDialog(this, wxT("Open a data file"), wxT(""), wxT(""), wxT(""), 
				wxFD_OPEN | wxFD_FILE_MUST_EXIST | wxFD_MULTIPLE );
			openFileDialog.SetWildcard(_U(filetypes));

			// Default value
			std::string lastPath = settings->GetCurrentDataPath();
			openFileDialog.SetDirectory(_U( lastPath.c_str() ));

			if(openFileDialog.ShowModal() == wxID_OK)
			{
				wxArrayString wxFilenames;
				openFileDialog.GetFilenames( wxFilenames );
				dataPath = _U(openFileDialog.GetDirectory());

				for ( unsigned i = 0 ; i < wxFilenames.size() ; i++ )
				{
					completePath.push_back( dataPath + Core::IO::SlashChar + 
						_U(wxFilenames[ i ]) );
				}
			}
		}
		else
		{
			completePath.push_back( fileName );
		}

		LoadDataEntity( completePath, father );

	}
	coreCatchExceptionsReportAndNoThrowMacro(MainMenu::OnMenuOpenFile)
}

void Core::Widgets::MainMenu::OnMenuOpenDirectory( wxCommandEvent& event )
{
	std::string dirPath(event.GetString().mb_str());
	
	wxDirDialog* openDirectoryDialog = new wxDirDialog( 
		this, wxT("Open directory"), wxT(""));

	try
	{	
		if(dirPath.compare("")==0)
		{

			Core::Runtime::Settings::Pointer settings;
			settings = Core::Runtime::Kernel::GetApplicationSettings();
			std::string dataPath = settings->GetCurrentDataPath();

			openDirectoryDialog->SetPath(_U(dataPath));
			if(openDirectoryDialog->ShowModal() == wxID_OK)
			{
				dirPath = openDirectoryDialog->GetPath().mb_str(wxConvUTF8);
				if(dirPath.compare("") == 0)
				{
					throw Core::Exceptions::Exception( 
						"OnMenuOpenDirectory",
						"Provide a valid directory path" );
				}
			}
		}
		else //directory specified by the caller..thus we have to check that it exists!
		{
			//check if dirpath exists
			Core::IO::Directory::Pointer dirToOpen = Core::IO::Directory::New();
			dirToOpen->SetDirNameFullPath(dirPath);
			if(!dirToOpen->Exists())
			{
				throw Core::Exceptions::Exception( 
					"OnMenuOpenDirectory",
					"Provide a valid directory path" );
			}
		}

		if ( !dirPath.empty() )
		{
			std::vector<std::string> completePath;
			completePath.push_back( dirPath );
			LoadDataEntity( completePath );
		}

	}
	coreCatchExceptionsReportAndNoThrowMacro(MainMenu::OnMenuOpenDirectory)

	openDirectoryDialog->Destroy();
}

//!
void MainMenu::OnMenuSaveFile(wxCommandEvent& event)
{
	try
	{
		Core::Runtime::Settings::Pointer settings;
		settings = Core::Runtime::Kernel::GetApplicationSettings();
		Core::DataContainer::Pointer container;
		container = Core::Runtime::Kernel::GetDataContainer();
		coreAssertMacro(container.IsNotNull());
		Core::DataEntity::Pointer dataEntity = 
			container->GetDataEntityList( )->GetSelectedDataEntityHolder()->GetSubject();
		
		if(dataEntity.IsNull())
		{
			throw Exceptions::Exception("OnMenuSaveFile",
				"First you have to select a data entity from the Processing browser");
		}

		std::string fileName(event.GetString().mb_str());
		std::string dataPath;

		if ( fileName.empty() )
		{

			std::string filetypes = Core::IO::DataEntityWriter::GetFileFilterTypesForWrite(dataEntity->GetType());
			wxFileDialog saveFileDialog(this, wxT("Save data to file"), 
									wxT(""), wxT(""), wxT(""), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
			saveFileDialog.SetDirectory(_U(settings->GetLastSavePath()));
			saveFileDialog.SetWildcard(_U(filetypes));
			saveFileDialog.SetFilename( wxString::FromAscii(dataEntity->GetMetadata( )->GetName().c_str()) );
			if(saveFileDialog.ShowModal() != wxID_OK)
			{
				return;
			}

			fileName = _U(saveFileDialog.GetFilename());
			dataPath = _U(saveFileDialog.GetDirectory());
			if ( fileName == "" )
			{
				throw Exceptions::Exception("OnMenuSaveFile", 
					"Provide a valid file name for saving a data file");
				return;
			}
		}
		else
		{
			// Avoid using itksys::SystemTools::GetFilenamePath because converts http:// to http:/
			std::string name = itksys::SystemTools::GetFilenameName( fileName );
			size_t pos = fileName.rfind( name );
			if ( pos != std::string::npos )
			{
				dataPath = fileName.substr( 0, pos );
				fileName = name;
			}
		}

		std::string fileNameWithoutExtension;
		fileNameWithoutExtension.append( fileName,0, fileName.find_last_of("."));
		std::string ext = itksys::SystemTools::GetFilenameLastExtension( fileName );
		std::string extension = itksys::SystemTools::LowerCase(ext);
		dataEntity->GetMetadata()->AddTag( "Extension", extension );

		Core::DataEntity::Pointer outDataEntity;
		outDataEntity = ShowIOHeaderDialog( dataEntity->GetMetadata().GetPointer( ), dataEntity, false );
		if ( outDataEntity.IsNotNull() )
		{
			dataEntity = outDataEntity;
		}


		if ( !dataPath.empty() )
		{
			SaveDataEntity( dataEntity, dataPath, fileName );
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro(MainMenu::OnMenuSaveFile)

}


void Core::Widgets::MainMenu::SaveDataEntity( 
	Core::DataEntity::Pointer dataEntity,
	const std::string &dataPath,
	const std::string &fileName )
{
	//clean the screen before saving
	wxTheApp->Yield();

	// Pass rendering data to MetaData before saving the XML
	Core::RenderingTreeManager::Pointer treeManager;
	treeManager = GetCurrentPluginTab()->GetRenderingTreeManager();
	if ( treeManager.IsNotNull() )
	{
		std::list<RenderingTree::Pointer> treeList = treeManager->GetTreeList( );
		std::list<RenderingTree::Pointer>::iterator it;
		for ( it = treeList.begin() ; it != treeList.end() ; it++ )
		{
			(*it)->UpdateMetadata( dataEntity );
		}
	}

	Core::IO::DataEntityWriter::Pointer dataEntityWriter;
	dataEntityWriter = Core::IO::DataEntityWriter::New( );

	// When using ftp://... /name.vtk, the path already contains the last '/' at the end
	// and we doesn't want to duplicate it
	std::string finalFileName = dataPath;
	if ( dataPath.size( ) && ( dataPath[ dataPath.size( ) - 1 ] != '\\' && dataPath[ dataPath.size( ) - 1 ] != '/' ) )
	{
		finalFileName += Core::IO::SlashChar;
	}
	finalFileName += fileName;
	dataEntityWriter->SetFileName( finalFileName );
	dataEntityWriter->SetInputDataEntity( 0, dataEntity );


	dataEntityWriter->SetMultithreading( true );
	Runtime::Kernel::GetProcessorManager()->Execute( dataEntityWriter.GetPointer() );

	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();
	settings->SetLastSavePath( dataPath );
}

//!
void MainMenu::OnMenuSaveImage(wxCommandEvent& event)
{
	try
	{
		Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
		std::string filetypes = "*.png";
		wxFileDialog saveFileDialog(this, wxT("Save Image of 3D view"), 
				wxT(""), wxT(""), wxT(""), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);

		std::string dataPath = settings->GetCurrentDataPath();
		settings->GetPluginProperty( "GIMIAS", "LastSaveImagePath", dataPath );
		saveFileDialog.SetDirectory(_U(dataPath));
		saveFileDialog.SetWildcard(_U(filetypes));
		if(saveFileDialog.ShowModal() != wxID_OK)
		{
			return;
		}

		std::string fileName = _U(saveFileDialog.GetFilename());
		dataPath = _U(saveFileDialog.GetDirectory());
		if(fileName.compare("") == 0)
		{
			throw Exceptions::Exception("OnMenuSaveImage",
				"Provide a valid file name for opening a data file");
		}

		bool saved = false;

		try
		{	
			const std::string& completeFileName = dataPath + Core::IO::SlashChar + fileName;
			GetCurrentMultiRenderWindow()->SaveImage3DWindow( completeFileName );
			saved = true;
		}
		coreCatchExceptionsReportAndNoThrowMacro(
			MainMenu::OnMenuSaveImage);

		if(!saved)
			throw Exceptions::Exception(
			"OnMenuSaveImage", 
			"The Image could not be saved or either the application \n" \
			"was not able to find a suitable writer for that format.");


		settings->SetPluginProperty( "GIMIAS", "LastSaveImagePath", dataPath );

	}
	coreCatchExceptionsReportAndNoThrowMacro(MainMenu::OnMenuSaveImage)
}

void Core::Widgets::MainMenu::OnMenuSaveWorkingAreaImage( wxCommandEvent& event )
{

	try
	{
		if ( !GetCurrentPluginTab() )
		{
			return;
		}

		Core::BaseWindow* baseWindow = GetCurrentPluginTab( )->GetCurrentWorkingArea();
		if ( !baseWindow )
		{
			return;
		}

		wxWindow* win = dynamic_cast<wxWindow*> ( baseWindow );
		if ( !win )
		{
			return;
		}

		Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
		std::string filetypes = "*.png";
		wxFileDialog saveFileDialog(this, wxT("Save Image of Working Area"), 
				wxT(""), wxT(""), wxT(""), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);

		std::string dataPath = settings->GetCurrentDataPath();
		settings->GetPluginProperty( "GIMIAS", "LastSaveWorkingAreaImagePath", dataPath );
		saveFileDialog.SetDirectory(_U(dataPath));
		saveFileDialog.SetWildcard(_U(filetypes));
		if(saveFileDialog.ShowModal() != wxID_OK)
		{
			return;
		}
		saveFileDialog.Close();

		std::string fileName = _U(saveFileDialog.GetFilename());
		dataPath = _U(saveFileDialog.GetDirectory());
		if(fileName.compare("") == 0)
		{
			throw Exceptions::Exception("OnMenuSaveWorkingAreaImage",
				"Provide a valid file name for saving a data file");
		}

		wxSafeYield();

		// Send an event to be able to refresh the view
		bool saved = false;
		try
		{	
			const std::string& completeFileName = dataPath + Core::IO::SlashChar + fileName;
			wxCaptureWindow( win, completeFileName );
			saved = true;
		}
		coreCatchExceptionsReportAndNoThrowMacro(
			MainMenu::OnMenuSaveImage);

		if(!saved)
		{
			throw Exceptions::Exception( 
				"OnMenuSaveWorkingAreaImage", 
				"The Image could not be saved" );
		}

		settings->SetPluginProperty( "GIMIAS", "LastSaveWorkingAreaImagePath", dataPath );

	}
	coreCatchExceptionsReportAndNoThrowMacro(MainMenu::OnMenuSaveImage)
}

void MainMenu::OnMenuExit(wxCommandEvent& event)
{
	try
	{
		Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
		graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
		graphicalIface->GetMainWindow()->CloseWindow();
	}
	coreCatchExceptionsReportAndNoThrowMacro(MainMenu::OnMenuExit)
}

//!
void MainMenu::OnMenuShowLog(wxCommandEvent& event)
{
	try
	{
		Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
		graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
		graphicalIface->GetMainWindow()->ShowLogFileBrowser();
	}
	coreCatchExceptionsReportAndNoThrowMacro(MainMenu::OnMenuShowLog)
}

//!
void MainMenu::OnMenuShowLogFolder(wxCommandEvent& event)
{
	try
	{
		Core::Runtime::Settings::Pointer settings;
		settings = Core::Runtime::Kernel::GetApplicationSettings();
		std::string call = "start explorer \"" + 
			itksys::SystemTools::ConvertToWindowsOutputPath( settings->GetProjectHomePath( ).c_str( ) ) + "\"";
		system( call.c_str( ) );
	}
	coreCatchExceptionsReportAndNoThrowMacro(MainMenu::OnMenuShowLog)
}

void Core::Widgets::MainMenu::OnMenuImportConfiguration( wxCommandEvent& event )
{
	try
	{
		Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
		graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
		graphicalIface->GetMainWindow()->ShowImportConfigurationWizard();
	}
	coreCatchExceptionsReportAndNoThrowMacro(MainMenu::OnMenuImportConfiguration)
}

//!
void MainMenu::OnMenuShowAbout(wxCommandEvent& event)
{
	try
	{
		Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
		graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
		graphicalIface->GetMainWindow()->ShowAboutScreen();
	}
	coreCatchExceptionsReportAndNoThrowMacro(MainMenu::OnMenuShowAbout)
}


//!
void MainMenu::OnMRUFile(wxCommandEvent& event)
{
	try
	{	
		wxString f(m_FileHistory->GetHistoryFile(event.GetId() - wxID_FILE1));

		std::vector<std::string> pathFilenames;
		pathFilenames.push_back( _U(f) );

		LoadDataEntity( pathFilenames );
	}
	coreCatchExceptionsReportAndNoThrowMacro(MainMenu::OnMRUFile)
}

void MainMenu::LoadDataEntity( 
	std::vector<std::string> pathFilenames,
	Core::DataEntity::Pointer father /*= NULL*/,
	bool openSession /*= false*/ )
{
	try
	{	

		// PreLoad data entity
		blTagMap::Pointer tagMap;
		tagMap = Core::DataEntityHelper::PreLoadDataEntityVector( pathFilenames );

		ShowIOHeaderDialog( tagMap );

		// Read settings
		Core::Runtime::Settings::Pointer settings;
		settings = Core::Runtime::Kernel::GetApplicationSettings();
		std::string readMultipeTimeStepsStr;
		settings->GetPluginProperty( "GIMIAS", "ReadMultipeTimeSteps", readMultipeTimeStepsStr );
		bool readMultipeTimeSteps = readMultipeTimeStepsStr == "false" ? false : true;

		// Load the file
		Core::IO::DataEntityReader::Pointer dataEntityReader;
		dataEntityReader = Core::IO::DataEntityReader::New( );

		// Pass parameter to reader (for Session reader)
		dataEntityReader->GetProperties( )->AddTag( "OpenSession", openSession );

		// Add observer on changed number of outputs
		dataEntityReader->AddObserver1( this, &MainMenu::OnModifiedReader );
		dataEntityReader->SetFileNames( pathFilenames );
		dataEntityReader->SetTagMap( tagMap );
		dataEntityReader->SetFather( father );
		dataEntityReader->SetUseMultipleTimeSteps( readMultipeTimeSteps );

		dataEntityReader->SetMultithreading( true );
		Runtime::Kernel::GetProcessorManager( )->Execute( dataEntityReader.GetPointer( ) );
	}
	coreCatchExceptionsReportAndNoThrowMacro(MainMenu::LoadDataEntity);
}

void Core::Widgets::MainMenu::OnModifiedReader( SmartPointerObject* object )
{
	IO::DataEntityReader* reader = dynamic_cast<IO::DataEntityReader*> ( object );

	// Add observer for each output
	for ( unsigned i = 0 ; i < reader->GetNumberOfOutputs() ; i++ )
	{
		reader->GetOutputDataEntityHolder( i )->AddObserver1( 
			this,
			&MainMenu::OnModifiedReaderOutput );
	}

}

void Core::Widgets::MainMenu::OnModifiedReaderOutput( DataEntity::Pointer dataEntity )
{
	if ( dataEntity.IsNull() )
	{
		return;
	}

	try
	{

		// When loading a folder with multiple files, this function will be called
		// for the already loaded DataEntity instances. We need to check if the DataEntity
		// has been already added to DataEntity list
		Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
		Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();

		// If father is not in the list -> return
		if ( list->IsInTheList( dataEntity->GetId() ) )
		{
			return;
		}

		OnLoadSingleDataEntity( dataEntity );

		// Show message
		Core::LoggerHelper::ShowMessage(
			"File imported successfully", 
			Core::LoggerHelper::MESSAGE_TYPE_INFO, false);

		blTag::Pointer tagFilePath = dataEntity->GetMetadata( )->FindTagByName( "FilePath" );
		if ( tagFilePath.IsNotNull() )
		{
			// Check the file path is a parent session node
			// and it has not been loaded yet
			if ( !CheckBelongsToParentSession( dataEntity ) )
			{
				std::string filePath = tagFilePath->GetValueAsString();

				// Add files to history 
				m_FileHistory->AddFileToHistory( _U( filePath ) );

				// Set last opened path
				Core::Runtime::Kernel::GetApplicationSettings()->SetLastOpenedPath(
					itksys::SystemTools::GetFilenamePath( filePath ) );
			}
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro(MainMenu::OnModifiedReaderOutput);

}

bool Core::Widgets::MainMenu::CheckBelongsToParentSession( DataEntity::Pointer dataEntity )
{
	// Get parent session node
	Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
	Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();
	Core::DataEntityList::iterator itList;

	bool belongsToParentSession = false;
	for ( itList = list->Begin( ) ; !belongsToParentSession && itList != list->End( ) ; itList++ )
	{
		Core::DataEntity::Pointer session = list->Get( itList );
		if ( session->GetType() != Core::SessionTypeId )
			continue;

		// Get processing data
		blTagMap::Pointer sessionData;
		if ( !session->GetProcessingData( sessionData ) )
			continue;

		// Get tagmap for this DataEntity
		std::string name = dataEntity->GetMetadata( )->GetName( );
		blTag::Pointer dataTag = sessionData->FindTagByName( name );
		if ( dataTag.IsNull( ) )
			continue;

		// Get FilePath from session
		blTagMap::Pointer dataMap = dataTag->GetValueCasted<blTagMap::Pointer>( );
		if ( dataMap.IsNull( ) || dataMap->GetTag( "RelativePath" ).IsNull( ) )
			continue;
		std::string relativePathSession = dataMap->GetTag( "RelativePath" )->GetValueAsString( );
	
		// Get FilePath from DataEntity
		blTag::Pointer filePathTag = dataEntity->GetMetadata( )->GetTag( "FilePath" );
		if ( filePathTag.IsNull( ) )
			continue;
		std::string filePathDataEntity = filePathTag->GetValueAsString( );

		// Compare FilePaths
		itksys::SystemTools::ConvertToUnixSlashes( filePathDataEntity );
		itksys::SystemTools::ConvertToUnixSlashes( relativePathSession );
		belongsToParentSession = filePathDataEntity.find( relativePathSession ) != std::string::npos;
	}

	return belongsToParentSession;
}

void Core::Widgets::MainMenu::OnMenuCut( wxCommandEvent& event )
{

}

void Core::Widgets::MainMenu::OnMenuCopy( wxCommandEvent& event )
{
	try
	{

		if (wxTheClipboard->Open())
		{

			Core::DataEntity::Pointer selectedDataEntity;
			Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
			selectedDataEntity = dataContainer->GetDataEntityList( )->GetSelectedDataEntity( );
			
			Core::DataEntity::Pointer newDataEntity;

			if ( selectedDataEntity.IsNotNull( ) )
			{
				// Build a new data entity using the current time step
				boost::any	processingData;
				processingData = selectedDataEntity->GetProcessingData( GetCurrentPluginTimeStep( ) );

				newDataEntity = Core::DataEntity::New( selectedDataEntity->GetType() );
				newDataEntity->GetMetadata()->SetName( "Copy" );
				newDataEntity->AddTimeStep( processingData );

				// This data objects are held by the clipboard, 
				// so do not delete them in the app.
				wxDataObjectDataEntity* data = new wxDataObjectDataEntity( );
				data->SetDataEntity( newDataEntity );
				wxTheClipboard->SetData( data );
			}

			wxTheClipboard->Close();
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro( OnMenuCopy );

}

void Core::Widgets::MainMenu::OnMenuPaste( wxCommandEvent& event )
{

	try
	{

		if (wxTheClipboard->Open())
		{
			if ( wxTheClipboard->IsSupported( wxDataFormat( Core::wxDataObjectDataEntity::GetFormatId( ) ) ) )
			{
				wxDataObjectDataEntity data;
				bool bRes = wxTheClipboard->GetData( data );
				if ( bRes )
				{
					// Get selected data entity
					Core::DataEntity::Pointer selectedDataEntity;
					Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
					selectedDataEntity = dataContainer->GetDataEntityList( )->GetSelectedDataEntity( );

					// Set the time step to the selected data entity
					if ( selectedDataEntity.IsNotNull( ) && 
						selectedDataEntity->GetType( ) == data.GetDataEntity( )->GetType( ) )
					{
						Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
						gIface = Core::Runtime::Kernel::GetGraphicalInterface();

						Core::DataEntityImpl::Pointer impl;
						impl = selectedDataEntity->GetTimeStep( GetCurrentPluginTimeStep( ) );
						impl->DeepCopy( data.GetDataEntity( )->GetProcessingData( 0 ) );

						// Update the rendering tree with the new data
						selectedDataEntity->Modified();

						GetCurrentMultiRenderWindow()->RequestUpdateAll();
					}
				}
			}  
			wxTheClipboard->Close();
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro( OnMenuPaste );

}


void Core::Widgets::MainMenu::OnMenuWindowResetLayout( wxCommandEvent& event )
{
	try
	{
		if ( GetCurrentPluginTab() )
		{
			GetCurrentPluginTab()->ResetLayout();
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro( "OnMenuWindowResetLayout" )
}

void Core::Widgets::MainMenu::UpdateMenus()
{
	UpdateViewMenuItems();
	UpdateRegisteredWindowsMenuItems();

}

void Core::Widgets::MainMenu::OnMenuWindowMaximizeWorkingAreaMenuItem( wxCommandEvent& event )
{
	// If the focus is inside a text control -> Don't do anything
	wxWindow *window = wxWindow::FindFocus();
	wxTextCtrl *textControl = dynamic_cast<wxTextCtrl *> ( window );
	if ( textControl != NULL )
	{
		return;
	}

	GetCurrentPluginTab()->MaximizeWindow( wxID_WorkingArea );
}

Core::Widgets::RenderWindowBase* Core::Widgets::MainMenu::GetCurrentMultiRenderWindow()
{
	if ( GetCurrentPluginTab() == NULL )
	{
		return NULL;
	}
	return GetCurrentPluginTab()->GetWorkingAreaManager( )->GetActiveMultiRenderWindow( );
}

blTagMap::Pointer Core::Widgets::MainMenu::GetCurrentMultiRenderWindowState()
{
	if ( GetCurrentPluginTab() == NULL )
	{
		return NULL;
	}
	return GetCurrentPluginTab()->GetWorkingAreaManager( )->GetActiveWindowMetadata( );
}

void Core::Widgets::MainMenu::OnLoadSingleDataEntity(
	Core::DataEntity::Pointer dataEntity )
{
	Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
	Core::DataEntityHolder::Pointer selectedDataEntity;
	selectedDataEntity = dataContainer->GetDataEntityList( )->GetSelectedDataEntityHolder();

	if ( dataEntity->IsROI()  && dataEntity->GetFather().IsNull() )
	{
		if ( selectedDataEntity.IsNotNull( ) && 
			selectedDataEntity->GetSubject().IsNotNull( ) && 
			selectedDataEntity->GetSubject()->GetType( ) == ImageTypeId )
		{
			dataEntity->SetFather( selectedDataEntity->GetSubject() );
		}
	}

	// Ask missing data entity fields 
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();
	std::string askForModalityStr;
	settings->GetPluginProperty( "GIMIAS", "AskForModality", askForModalityStr );
	bool askForModality = askForModalityStr == "true" ? true : false;
	if( askForModality &&
		dataEntity->IsImage() && 
		dataEntity->GetMetadata( )->GetModality() == Core::UnknownModality &&
		dataEntity->GetType() != Core::ROITypeId )
	{
		Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
		gIface = Core::Runtime::Kernel::GetGraphicalInterface();
		if( !gIface->GetMainWindow()->ShowMissingDataEntityFieldsWizard(dataEntity) )
		{
			throw Core::Exceptions::Exception( 
				"MainMenu::LoadDataEntity", 
				"File import canceled" );
		}
	}

	// Update metadata
	Core::Compatibility::UpdateDataEntityMetadata( dataEntity );

	// If it's a session and OpenSession is true -> Overwrite root node
	Core::DataEntity::Pointer root = dataContainer->GetDataEntityList( )->Get( dataContainer->GetDataEntityList( )->Begin( ) );
	if ( dataEntity->GetType( ) == Core::SessionTypeId && 
		 dataEntity->GetMetadata( )->GetTagValue<bool>( "OpenSession" ) )
	{
		blTagMap::Pointer sessionTagMap;
		dataEntity->GetProcessingData( sessionTagMap );
		root->SetTimeStep( sessionTagMap );
		return;
	}

	// Check if there's a session node as father
	Core::DataEntity::Pointer fatherEntity = dataEntity;
	while( fatherEntity->GetType( ) != Core::SessionTypeId && fatherEntity->GetFather( ) )
	{
		fatherEntity = fatherEntity->GetFather( );
	}
	bool setSelected = fatherEntity->GetType( ) != Core::SessionTypeId;

	// remove a parent node that is not in the list
	if ( dataEntity->GetFather( ).IsNotNull ( ) &&
		!dataContainer->GetDataEntityList( )->IsInTheList( dataEntity->GetFather( )->GetId( ) ) )
	{
		dataEntity->SetFather( NULL );
	}

	// Get active tree
	RenderingTreeManager::Pointer renderingTreeManager = GetCurrentPluginTab( )->GetRenderingTreeManager();
	RenderingTree::Pointer renderingTree = renderingTreeManager->GetActiveTree( );

	// Publish output
	Core::DataEntityHolder::Pointer holder = Core::DataEntityHolder::New( );
	holder->SetSubject( dataEntity );
	Core::DataTreeHelper::PublishOutput( 
		holder, 
		renderingTree,
		true,
		setSelected );

}

/**
*/
bool MainMenu::FileExists( std::string strFilename )
{
	if ( strFilename.empty( ) )
	{
		return false;
	}

	if (FILE * file = fopen(strFilename.c_str(), "r"))
	{
		fclose(file);
		return true;
	}
	return false;
}


int Core::Widgets::MainMenu::GetCurrentPluginTimeStep()
{
	if ( GetCurrentPluginTab( ) == NULL )
	{
		return 0;
	}

	MovieToolbar* movieToolbar;
	GetCurrentPluginTab( )->GetWidget( wxID_MovieToolbar, movieToolbar );

	Core::IntHolderType::Pointer timeStep;
	timeStep = movieToolbar->GetCurrentTimeStep();
	return timeStep->GetSubject( );
}

Core::Widgets::PluginTab* Core::Widgets::MainMenu::GetCurrentPluginTab( void )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	BasePluginTab* basePluginTab = graphicalIface->GetMainWindow()->GetCurrentPluginTab();
	Core::Widgets::PluginTab* tabPage = dynamic_cast<Core::Widgets::PluginTab*> (basePluginTab);
	return tabPage;
}

void Core::Widgets::MainMenu::SetPluginTab( Core::Widgets::PluginTab* val )
{

	// Add observer to current state of plugin tab
	if ( GetCurrentPluginTab( ) )
	{
		m_PluginTabConnection.disconnect( );

		// Update the menus, when the registered windows change
		m_PluginTabConnection = GetCurrentPluginTab( )->GetWindowsMapHolder()->AddObserver( 
			this,
			&Core::Widgets::MainMenu::UpdateMenus );

	}


	ResetViewMenuItems();

	UpdateViewMenuItems( );
	UpdateRegisteredWindowsMenuItems( );
}

Core::BaseProcessor::Pointer Core::Widgets::MainMenu::GetProcessor()
{
	return NULL;
}

void Core::Widgets::MainMenu::UpdateRegisteredWindowsMenuItems( )
{
	if ( !/*IsEnabled()*/IsThisEnabled() )
	{
		return;
	}

	UpdateRegisteredWindowsMenu( 
		"Tools",
		WIDGET_TYPE( WIDGET_TYPE_PROCESSING_TOOL |WIDGET_TYPE_COMMAND_PANEL), 
		wxCommandEventHandler(MainMenu::OnProcessingToolsMenu) );

	UpdateRegisteredWindowsMenu( 
		"Selection",
		WIDGET_TYPE_SELECTION_TOOL, 
		wxCommandEventHandler(MainMenu::OnSelectionToolsMenu ) );
}

void Core::Widgets::MainMenu::UpdateRegisteredWindowsMenu(
	wxString menuName,
	WIDGET_TYPE state,
	wxObjectEventFunction func )
{
	if ( !/*IsEnabled()*/IsThisEnabled() )
	{
		return;
	}

	int menuId = FindMenu( menuName );
	if ( menuId == wxNOT_FOUND )
	{
		return;
	}
	wxMenu* toolsMenu = GetMenu( menuId );

	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	BaseWindowFactorySearch::Pointer search = BaseWindowFactorySearch::New( );
	search->SetType( state );
	search->Update( );
	std::list<std::string> windowsList = search->GetFactoriesNames();

	std::list<std::string>::iterator it;
	for ( it = windowsList.begin( ) ; it != windowsList.end() ; it++ )
	{
		Core::WindowConfig config;
		if ( ! baseWindowFactory->GetWindowConfig( *it, config ) )
		{
			continue;
		}

		// Get category menu or the main menu
		wxMenu* categoryMenu;

		std::string category = config.GetCategory();
		if ( category.empty( ) )
		{
			category = config.GetTabPage( );
		}

		if ( !category.empty() )
		{
			// Separate category by '.'
			std::list<std::string> words;
			blTextUtils::ParseLine( category, '.', words );

			// Find/Create menu sub items
			wxMenu *parentMenuItem = toolsMenu;
			std::list<std::string>::iterator it;
			for ( it = words.begin() ; it != words.end() ; it++ )
			{
				// Find category, or create it
				wxMenuItem *menuItem = FindChildItem( parentMenuItem, *it );
				if ( !menuItem )
				{
					menuItem = parentMenuItem->Append(
						wxNewId(), ( *it ), 
						new wxMenu( ), ( *it ) );
				}
				categoryMenu = menuItem->GetSubMenu();
				parentMenuItem = categoryMenu;
			}
		}
		else
		{
			categoryMenu = toolsMenu;
		}

		// Find item, or create it
		int windowMenuId = categoryMenu->FindItem( config.GetCaption() );
		if ( windowMenuId == wxNOT_FOUND )
		{
			wxWindowID id = wxNewId();
			categoryMenu->Append(
				id, 
				config.GetCaption(), 
				config.GetCaption(), 
				wxITEM_NORMAL);

			// Connect handler
			Connect(
				id,
				wxEVT_COMMAND_MENU_SELECTED,
				func
				);
		}
	}

	CleanOldItems( toolsMenu );
}

void Core::Widgets::MainMenu::UpdateViewMenuItems()
{
	if ( !/*IsEnabled()*/IsThisEnabled() )
	{
		return;
	}

	// Get view menu
	int menuId = FindMenu( "View" );
	if ( menuId == wxNOT_FOUND || GetCurrentPluginTab( ) == NULL )
	{
		return;
	}
	wxMenu* viewMenu = GetMenu( menuId );

	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	PluginTab::WindowMapType windowsMap;
	windowsMap = GetCurrentPluginTab( )->GetAllWindows();

	// Add toolbar menu
	PluginTab::WindowMapType::iterator it;
	for ( it = windowsMap.begin() ; it != windowsMap.end() ; it++ )
	{
		Core::BaseWindow* baseWindow = dynamic_cast<Core::BaseWindow*> ( *it );
		std::string factoryName = baseWindow->GetFactoryName();
		Core::WindowConfig config;
		if ( !baseWindowFactory->GetWindowConfig( factoryName, config ) )
		{
			continue;
		}

		// Don't add command panel windows
		if ( config.GetCommandPanel( ) )
		{
			continue;
		}

		// Add category menu
		wxMenu* categoryMenu;

		std::string category = config.GetCategory();
		if ( !category.empty() )
		{
			// Separate category by '.'
			std::list<std::string> words;
			blTextUtils::ParseLine( category, '.', words );

			// Find/Create menu sub items
			wxMenu *parentMenuItem = viewMenu;
			std::list<std::string>::iterator it;
			for ( it = words.begin() ; it != words.end() ; it++ )
			{
				// Find category, or create it
				wxMenuItem *menuItem = FindChildItem( parentMenuItem, *it );
				if ( !menuItem )
				{
					menuItem = parentMenuItem->Append(
						wxNewId(), ( *it ), 
						new wxMenu( ), ( *it ) );
				}
				categoryMenu = menuItem->GetSubMenu();
				parentMenuItem = categoryMenu;
			}
		}
		else
		{
			categoryMenu = viewMenu;
		}

		// Find item, or create it
		wxWindowID windowMenuId = categoryMenu->FindItem( (*it)->GetName() );
		if ( windowMenuId == wxNOT_FOUND )
		{
			windowMenuId = wxNewId();
			categoryMenu->Append(
				windowMenuId, 
				(*it)->GetName(), 
				(*it)->GetName(), 
				wxITEM_CHECK);

			// Connect handler
			Connect(
				windowMenuId,
				wxEVT_COMMAND_MENU_SELECTED,
				wxCommandEventHandler(MainMenu::OnAutomatedMenu)
				);
		}

		this->Check( windowMenuId, GetCurrentPluginTab( )->IsWindowShown( (*it)->GetId() ) );
	}

	CleanOldItems( viewMenu );
}

void Core::Widgets::MainMenu::OnAutomatedMenu( wxCommandEvent& event )
{
	if ( !GetCurrentPluginTab() )
	{
		return;
	}

	wxMenuItem* item = FindItem( event.GetId() );
	GetCurrentPluginTab()->ShowWindow( item->GetItemLabel().ToStdString(), event.IsChecked() );
	UpdateMenus(); // refresh menu, to update checked menu items
}

void Core::Widgets::MainMenu::OnProcessingToolsMenu( wxCommandEvent& event )
{
	if ( !GetCurrentPluginTab() )
	{
		return;
	}

	wxMenuItem* item = FindItem( event.GetId() );
	try
	{
		// open the tools panel widget 
		ProcessingToolboxWidget* processingToolboxWidget;
		GetCurrentPluginTab()->GetWidget( wxID_ProcessingToolboxWidget, processingToolboxWidget );
		GetCurrentPluginTab()->ShowWindow( wxID_ProcessingToolboxWidget );
		processingToolboxWidget->SetToolByName( item->GetItemLabel().ToStdString() );
	}
	coreCatchExceptionsReportAndNoThrowMacro( "OnProcessingToolsMenu" )

}

void Core::Widgets::MainMenu::OnSelectionToolsMenu( wxCommandEvent& event )
{
	if ( GetCurrentPluginTab() == NULL )
	{
		return;
	}

	wxMenuItem* item = FindItem( event.GetId() );
	try
	{
		SelectionToolboxWidget* selectionToolboxWidget;
		GetCurrentPluginTab()->GetWidget( wxID_SelectionToolboxWidget, selectionToolboxWidget );
		if ( selectionToolboxWidget )
		{
			selectionToolboxWidget->SetToolByName( item->GetItemLabel().ToStdString() );
			GetCurrentPluginTab()->ShowWindow( wxID_SelectionToolboxWidget );
			selectionToolboxWidget->Start();
			GetCurrentPluginTab()->Refresh(); // refresh to update toolbars
		}

	}
	coreCatchExceptionsReportAndNoThrowMacro( "OnProcessingToolsMenu" )
}


void Core::Widgets::MainMenu::SetActiveMultiRenderWindow( 
	Core::Widgets::RenderWindowBase* multiRenderWindow )
{
	// Nothing to do.
}

void Core::Widgets::MainMenu::OnMenuItemWorkflowManager( wxCommandEvent& event )
{
	Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow( )->ShowWorkflowManager( true );
}

void Core::Widgets::MainMenu::OnMenuItemPerspectivePlugin( wxCommandEvent& event )
{
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();
	settings->SetPerspective( Core::Runtime::PERSPECTIVE_PLUGIN );

	// Restore selected plugins from configuration
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	graphicalIface->GetPluginProviderManager()->LoadConfiguration( true );

	// Update configuration
	Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow( )->UpdatePluginConfiguration( );
}

void Core::Widgets::MainMenu::OnMenuItemPerspectiveWorkflow( wxCommandEvent& event )
{
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();
	settings->SetPerspective( Core::Runtime::PERSPECTIVE_WORKFLOW );

	Core::Runtime::Kernel::GetWorkflowManager()->SetActiveWorkflow( settings->GetActiveWorkflow() );

	Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow( )->UpdatePluginConfiguration( );
}

wxFileHistory * Core::Widgets::MainMenu::GetFileHistory() const
{
	return m_FileHistory;
}

Core::DataEntity::Pointer Core::Widgets::MainMenu::ShowIOHeaderDialog( 
	blTagMap::Pointer tagMap,
	Core::DataEntity::Pointer dataEntity,
	bool isReader )
{
	// Get registered windows list for reader headers
	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	BaseWindowFactorySearch::Pointer search = BaseWindowFactorySearch::New( );
	search->SetType( WIDGET_TYPE_IO_HEADER_WINDOW );
	search->Update( );
	std::list<std::string> windowsList = search->GetFactoriesNames();

	// Get DataEntity and Extension tags
	Core::DataEntityType type = UnknownTypeId;
	blTag::Pointer tag = tagMap->FindTagByName( "DataEntityType" );
	if ( tag.IsNotNull() )
	{
		tag->GetValue( type );
	}

	std::string ext;
	tag = tagMap->FindTagByName( "Extension" );
	if ( tag.IsNotNull() )
	{
		tag->GetValue( ext );
	}

	// Add the ID of the DataEntity
	if ( dataEntity.IsNotNull() )
	{
		tagMap->AddTag( "DataEntityID", dataEntity->GetId() );
	}

	// Create list of windows
	wxWizard* wizard = NULL;
	wxWizardPageSimple* firstWizardPage = NULL;
	std::list<BaseWindow*> baseWindowList;

	// Create a dialog for this file and show to the user
	std::list<std::string>::iterator it;
	for ( it = windowsList.begin() ; it != windowsList.end() ; it++ )
	{
		// Get reader factory
		BaseWindowFactory::Pointer factory = baseWindowFactory->FindFactory( *it );
		Core::BaseWindowIOFactory* ioFactory;
		ioFactory = dynamic_cast<Core::BaseWindowIOFactory*> ( factory.GetPointer() );
		if ( ioFactory == NULL )
		{
			throw Core::Exceptions::Exception( 
				"MainMenu::ShowIOHeaderDialog", 
				"BaseWindowIOFactory is NULL" );
		}

		// Check if the data entity type is valid for this window
		// and create the wizard pages
		if ( ioFactory->CheckType( type, ext, isReader, dataEntity ) )
		{
			if ( wizard == NULL )
			{
				wxWindow* window;
				window = dynamic_cast<wxWindow*> ( Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow( ) );
				wxString title = "File Header Options";
				wizard = new wxWizard( window, wxID_ANY, title );
			}

			wxWizardPageSimple* wizardPage = new wxWizardPageSimple( wizard );
			wizard->GetPageAreaSizer()->Add( wizardPage );
			if ( firstWizardPage == NULL )
			{
				firstWizardPage = wizardPage;
			}

			factory->SetParent( wizardPage );
			Core::BaseWindow* baseWindow = factory->CreateBaseWindow();

			wxWindow* window = dynamic_cast<wxWindow*> ( baseWindow );
			wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
			sizer->Add( window, 1, wxEXPAND | wxALL, 5 );
			wizardPage->SetSizer( sizer );

			baseWindowList.push_back( baseWindow );
		}
	}

	// Add windows to wizard
	if ( wizard )
	{
		// Add pages and set properties
		std::list<BaseWindow*>::iterator it;
		for ( it = baseWindowList.begin() ; it != baseWindowList.end() ; it++ )
		{
			(*it)->SetProperties( tagMap );
		}

		// Run wizard
		bool result = wizard->RunWizard( firstWizardPage );

		// Get properties
		if ( result )
		{
			for ( it = baseWindowList.begin() ; it != baseWindowList.end() ; it++ )
			{
				(*it)->GetProperties( tagMap );

				// Process data if processor available
				if ( (*it)->GetProcessor( ) )
				{
					(*it)->GetProcessor( )->SetInputDataEntity( 0, dataEntity );
					(*it)->GetProcessor( )->Update( );
					dataEntity = (*it)->GetProcessor( )->GetOutputDataEntity( 0 );
				}
			}
		}

		wizard->Destroy();

		if ( !result )
		{
			throw Core::Exceptions::Exception( 
				"MainMenu::ShowIOHeaderDialog", 
				"File import canceled" );
		}
	}

	return dataEntity;
}

void Core::Widgets::MainMenu::OnMenuShowAllToolbars( wxCommandEvent& event )
{
	ShowAllWindows( "Toolbars", true );
}

void Core::Widgets::MainMenu::OnMenuHideAllToolbars( wxCommandEvent& event )
{
	ShowAllWindows( "Toolbars", false );
}

void Core::Widgets::MainMenu::ShowAllWindows( const std::string &category, bool show )
{
	if ( !GetCurrentPluginTab( ) )
	{
		return;
	}

	wxWindowUpdateLocker lock( GetCurrentPluginTab( ) );

	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	PluginTab::WindowMapType windowsMap;
	windowsMap = GetCurrentPluginTab( )->GetAllWindows();

	// Add toolbar menu
	PluginTab::WindowMapType::iterator it;
	for ( it = windowsMap.begin() ; it != windowsMap.end() ; it++ )
	{
		Core::BaseWindow* baseWindow = dynamic_cast<Core::BaseWindow*> ( *it );
		std::string factoryName = baseWindow->GetFactoryName();
		Core::WindowConfig config;
		if ( !baseWindowFactory->GetWindowConfig( factoryName, config ) )
		{
			continue;
		}

		if ( config.GetCategory() == category )
		{
			GetCurrentPluginTab()->ShowWindow( config.GetCaption().c_str( ), show );
		}
	}
}

void Core::Widgets::MainMenu::OnMenuUserRegistration( wxCommandEvent& event )
{
	UserRegistrationDialog dialog( this, wxID_ANY, "User Registration" );
	dialog.Center();
	dialog.ShowModal();
}

void Core::Widgets::MainMenu::OnMenuReportBug( wxCommandEvent& event )
{
	wxLaunchDefaultBrowser( "http://sourceforge.net/apps/mantisbt/gimias/bug_report_page.php" );
}

void Core::Widgets::MainMenu::OnMenuDevelopersSite( wxCommandEvent& event )
{
	wxLaunchDefaultBrowser( "http://www.gimias.org/index.php?option=com_content&view=article&id=11&Itemid=20" );
}

void Core::Widgets::MainMenu::OnMenuFrameworkVideos( wxCommandEvent& event )
{
	wxLaunchDefaultBrowser( "http://www.gimias.org/index.php?option=com_content&view=article&id=105&Itemid=61" );
}

void Core::Widgets::MainMenu::OnMenuPreferences( wxCommandEvent& event )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	graphicalIface->GetMainWindow()->ShowPreferencesdialog();
}

void Core::Widgets::MainMenu::OnMenuCustomApplicationManager( wxCommandEvent& event )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	graphicalIface->GetMainWindow()->ShowCustomApplicationManager();
}

void Core::Widgets::MainMenu::OnMenuShowFullScreen( wxCommandEvent& event )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	wxFrame* frame = dynamic_cast<wxFrame*> ( graphicalIface->GetMainWindow() );
	frame->ShowFullScreen( event.GetInt( ), 
		wxFULLSCREEN_NOBORDER | 
		wxFULLSCREEN_NOCAPTION | 
		wxFULLSCREEN_NOMENUBAR | 
		wxFULLSCREEN_NOSTATUSBAR | 
		wxFULLSCREEN_NOTOOLBAR );
}

void Core::Widgets::MainMenu::ResetViewMenuItems()
{
	// Clear view menu items
	int menuId = FindMenu( "View" );
	if ( menuId == wxNOT_FOUND || GetCurrentPluginTab( ) == NULL )
	{
		return;
	}

	wxMenu* viewMenu = GetMenu( menuId );

	// Get separator position
	wxMenuItemList list = viewMenu->GetMenuItems();
	size_t pos = 0;
	while ( pos < list.size( ) && !list[pos]->IsSeparator( ) )
	{
		pos++;
	}
	// Don't remove separator
	pos++;

	// Get list of IDs to remove
	std::list<int> itemIdList;
	while ( pos < list.size( ) )
	{
		itemIdList.push_back( list[pos]->GetId( ) );
		pos++;
	}

	// Remove
	std::list<int>::iterator it;
	for ( it = itemIdList.begin() ; it != itemIdList.end() ; it++ )
	{
		viewMenu->Remove( *it );
	}

	// Append two default menu items for "Toolbars"
	wxMenu* subMenu = new wxMenu();
	viewMenu->Append(wxID_ANY, wxT("Toolbars"), subMenu);
	subMenu->Append(wxID_ShowAllToolbarsMenuItem, wxT("Show all"), 
		wxT("Show all toolbars"));
	subMenu->Append(wxID_HideAllToolbarsMenuItem, wxT("Hide all"), 
		wxT("Hide all toolbars"));

}

void Core::Widgets::MainMenu::OnMenuCheckForUpdates( wxCommandEvent& event )
{
	try
	{

		Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
		graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
		graphicalIface->GetMainWindow()->ShowWebUpdater();


	}
	coreCatchExceptionsReportAndNoThrowMacro( MainMenu::OnMenuCheckForUpdates )

}

void Core::Widgets::MainMenu::CleanOldItems( wxMenu* menu, std::list<std::string> captionList )
{
	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	// Get all caption names
	if ( captionList.empty( ) )
	{
		BaseWindowFactories::FactoriesListType factoryList = baseWindowFactory->GetFactoryNames( );
		BaseWindowFactories::FactoriesListType::iterator it;
		for ( it = factoryList.begin( ) ; it != factoryList.end() ; it++ )
		{
			WindowConfig config;
			Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( )->GetWindowConfig( *it, config );
			captionList.push_back( config.GetCaption( ) );
		}
	}

	// Clean removed factories
	wxMenuItemList items = menu->GetMenuItems( );
	wxMenuItem *item = (wxMenuItem *)NULL;
	for ( wxMenuItemList::compatibility_iterator node = items.GetFirst();
		node;
		node = node->GetNext() )
	{
		wxMenuItem *item = node->GetData();
		if ( item->IsSeparator() )
		{
			continue;
		}

		if ( item->GetId() == wxID_WindowResetLayoutMenuItem ||
			 item->GetId() == wxID_WindowMaximizeWorkingAreaMenuItem ||
			 item->GetId() == wxID_WindowShowFullScreen ||
			 item->GetId() == wxID_ShowAllToolbarsMenuItem ||
			 item->GetId() == wxID_HideAllToolbarsMenuItem ||
			 item->GetText( ) == "&MultiRenderWindow" ||
			 item->GetText( ) == "&Layout" )
		{
			continue;
		}

		if ( item->GetSubMenu() )
		{
			CleanOldItems( item->GetSubMenu(), captionList );
			if ( item->GetSubMenu()->GetMenuItemCount() == 0 )
			{
				menu->Destroy( item );
			}
		}
		else
		{
			std::list<std::string>::iterator itFound;
			itFound = std::find( captionList.begin(), captionList.end( ), item->GetLabel().c_str() );
			if ( itFound == captionList.end( ) )
			{
				menu->Destroy( item );
			}
		}
	}
}

void Core::Widgets::MainMenu::UpdatePerspectiveSelection()
{
	// Update menu items from settings
	Core::Runtime::PERSPECTIVE_TYPE type;
	type = Core::Runtime::Kernel::GetApplicationSettings()->GetPerspective( );
	switch( type )
	{
	case Core::Runtime::PERSPECTIVE_PLUGIN:this->Check( wxID_PerspectivePluginMenuItem, true );break;
	case Core::Runtime::PERSPECTIVE_WORKFLOW:this->Check( wxID_PerspectiveWorkflowMenuItem, true );break;
	}
}

void MainMenu::OnMenuSaveSession(wxCommandEvent& event)
{
	try
	{
		Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
		gIface = Core::Runtime::Kernel::GetGraphicalInterface();
		coreAssertMacro(gIface.IsNotNull());

		wxFileDialog saveFileDialog(this, wxT("Save data to file"), 
			wxT(""), wxT(""), wxT(""), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);

		// get file path for session
		Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();

		// Retrieve last path
		std::string lastPath;
		settings->GetPluginProperty( "GIMIAS", "SessionPath", lastPath );
		if ( lastPath.empty( ) )
		{
			lastPath = settings->GetCurrentDataPath();
		}

		Core::DataEntityList::Pointer dataList = Core::Runtime::Kernel::GetDataContainer()->GetDataEntityList();
		Core::DataEntity::Pointer sessionDataEntity;
		if ( dataList->GetSelectedDataEntity( ).IsNotNull() &&
			 dataList->GetSelectedDataEntity( )->GetType() == Core::SessionTypeId )
		{
			sessionDataEntity = dataList->GetSelectedDataEntity( );
		}

		saveFileDialog.SetDirectory(_U(lastPath));
		if ( sessionDataEntity.IsNotNull( ) )
		{
			saveFileDialog.SetFilename(_U( sessionDataEntity->GetMetadata()->GetName().c_str() ));
		}
		else
		{
			saveFileDialog.SetFilename(_U("Session"));
		}

		std::string filetypes = "Session files(*.gses)|*.gses|All files(*.*)|*.*";
		saveFileDialog.SetWildcard(_U(filetypes));

		if(saveFileDialog.ShowModal() == wxID_OK)
		{
			// Save last path
			lastPath = _U( saveFileDialog.GetDirectory() );
			settings->SetPluginProperty( "GIMIAS", "SessionPath", lastPath );

			std::string sessionFilepath = _U(saveFileDialog.GetPath());

			//write session
			wxBusyInfo info(wxT("Saving session, please wait..."), this);
			Core::IO::SessionWriter::Pointer sessionWriter = Core::IO::SessionWriter::New();
			sessionWriter->SetFileName(sessionFilepath);
			sessionWriter->SetTreeManager( GetCurrentPluginTab()->GetRenderingTreeManager() );

			// If DataEntity is a session node, save all children
			// otherwise, save all data
			sessionWriter->SetInputDataEntity( 0, sessionDataEntity );

			sessionWriter->Update();
		}

	}
	coreCatchExceptionsReportAndNoThrowMacro(
		MainMenu::OnMenuSaveSession)
}

void MainMenu::OnMenuOpenSession(wxCommandEvent& event)
{
	try
	{
		Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
		gIface = Core::Runtime::Kernel::GetGraphicalInterface();
		coreAssertMacro(gIface.IsNotNull());

		// get file path for session
		std::string filetypes = "Session files(*.gses,*.xml)|*.gses;*.xml|All files(*.*)|*.*";
		wxFileDialog readFileDialog(this, wxT("Open file"), 
			wxT(""), wxT(""), wxT(""),
			wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	
		// Retrieve last path
		Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
		std::string lastPath;
		settings->GetPluginProperty( "GIMIAS", "SessionPath", lastPath );
		if ( lastPath.empty( ) )
		{
			lastPath = settings->GetCurrentDataPath();
		}

		readFileDialog.SetDirectory(_U( lastPath ));
		readFileDialog.SetWildcard(_U(filetypes));

		if(readFileDialog.ShowModal() == wxID_OK)
		{
			// Save last path
			lastPath = _U( readFileDialog.GetDirectory() );
			settings->SetPluginProperty( "GIMIAS", "SessionPath", lastPath );

			std::string sessionFilepath = _U(readFileDialog.GetPath());

			if ( event.GetId( ) == wxID_OpenSessionMenuItem )
			{
				Core::DataEntityList::Pointer dataList = Core::Runtime::Kernel::GetDataContainer()->GetDataEntityList();
				dataList->RemoveAll( );
			}

			std::vector<std::string> pathFilenames;
			pathFilenames.push_back( sessionFilepath );
			LoadDataEntity( pathFilenames );
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro(
		MainMenu::OnMenuReadSession)
}

