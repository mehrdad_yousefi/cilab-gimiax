/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _WorkingAreaFactory_H
#define _WorkingAreaFactory_H

#include "coreRenderWindowContainer.h"

namespace Core{
namespace Widgets{

/**
\brief Dynamic processing widget Factory 
Specific factory that uses internal name instead of class name
to identify it
\ingroup gmWidgets
\author Xavi Planes
\date 15 July 2010
*/
class WorkingAreaFactory : public Core::BaseWindowFactory
{
public:
	coreDeclareSmartPointerTypesMacro(Core::Widgets::WorkingAreaFactory,BaseWindowFactory)
	coreFactorylessNewMacro(Core::Widgets::WorkingAreaFactory)
	coreCommonFactoryFunctionsClassNameMacro( Core::Widgets::WorkingAreaFactory )

	static BaseWindowFactory::Pointer NewBase( const std::string &name, blTagMap::Pointer tagMap ) 
	{ 
		WorkingAreaFactory::Pointer factory = New( );
		factory->SetWindowClassname( name );
		factory->SetProperties( tagMap );
		return factory.GetPointer( );
	} 
	virtual Core::BaseWindow* CreateBaseWindow( ) 
	{ 
		RenderWindowContainer* window = new RenderWindowContainer( GetParent(), GetWindowId( ) );
		window->SetFactoryName( GetNameOfClass( ) );
		window->SetBitmapFilename( GetBitmapFilename( ) );
		if ( !GetWindowName( ).empty( ) ){ 
			wxWindowBase* windowBase = window; 
			windowBase->SetName( GetWindowName( ) ); 
		} 
		window->SetProperties( m_Properties );
		return window; 
	} 
	const char* GetNameOfClass() const 
	{ 
		return m_Name.c_str(); 
	} 
	void SetWindowClassname( const std::string &name) 
	{ 
		m_Name = name;
	} 
	blTagMap::Pointer GetProperties() const { return m_Properties; }
	void SetProperties(blTagMap::Pointer val) { m_Properties = val; }

	std::string m_Name;
	blTagMap::Pointer m_Properties;
};

}
}

#endif // WorkingAreaFactory_H
