/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include "wx/wxprec.h"

#ifndef WX_PRECOMP
       #include "wx/wx.h"
#endif

#include "coreWxMitkCoreMainWindow.h"
#include "coreMainMenu.h"
#include "coreMainContextMenu.h"
#include "coreKernel.h"
#include "coreWxMitkGraphicalInterface.h"
#include "corePluginTab.h"
#include "corePluginTabFactory.h"
#include "coreLogger.h"
#include "coreAssert.h"
#include "coreReportExceptionMacros.h"
#include "coreBaseExceptions.h"
#include "coreSplashScreen.h"
#include "coreStyleManager.h"
#include "coreProfileWizard.h"
#include "coreLogFileViewer.h"
#include "coreMissingDataEntityFieldsWizard.h"
#include "coreStatusBar.h"
#include "coreBaseWindowFactories.h"
#include "coreWorkflowGUIBuilder.h"
#include "coreDataEntityInformation.h"
#include "coreWorkflowManager.h"
#include "coreWorkflowEditorWidget.h"
#include "coreWorkflowManagerWidget.h"
#include "coreDynProcessingWidgetFactory.h"
#include "coreImportConfigurationWizard.h"
#include "coreUserRegistrationDialog.h"
#include "corePreferencesDialog.h"
#include "coreProcessorManager.h"
#include "coreWxProcessorEvtHandler.h"
#include "coreFactoryManager.h"
#include "coreWxEventsFactoriesRegistration.h"
#include "coreWebUpdateDialog.h"
#include "coreBaseWindowFactorySearch.h"
#include "coreWxGenericEvent.h"
#include "coreWxGenericEvtHandler.h"
#include "coreCustomApplicationManagerWidget.h"
#include "coreDynDataTransferBase.h"
#include "coreConfig.h" // for define DISABLE_FILE_LOADING

#include "wxID.h"

#include <wx/notebook.h>
#include <wx/stattext.h>
#include <wx/menu.h>
#include <wx/dnd.h>
#include <wx/utils.h>
#include <wx/clipbrd.h>
#include <wx/progdlg.h>
#include <wx/wupdlock.h>

#include "wxUnicode.h"
#include "wxEventHandlerHelper.h"

#include <stdio.h>
#include <time.h>

#include "blClock.h"

/** Definitions for handling shared libraries in Windows */
#if (defined(_WIN32) || defined(WIN32)) && !defined(SWIG_WRAPPING)
#	undef MAIN_WINDOW_EXPORT
#	define MAIN_WINDOW_EXPORT __declspec(dllexport)
#else
/* unix needs nothing */
#	undef  MAIN_WINDOW_EXPORT
#	define MAIN_WINDOW_EXPORT
#endif

using namespace Core::Widgets;

#define wxID_WorkflowEditor wxID( "wxID_WorkflowEditor" )
#define wxID_WorkflowManager wxID( "wxID_WorkflowManager" )

Core::WxGenericEvtHandler* wxGenericEvtHandler;

BEGIN_EVENT_TABLE(wxMitkCoreMainWindow, wxFrame)
	EVT_CLOSE	(wxMitkCoreMainWindow::OnClose)
	EVT_MOVE(wxMitkCoreMainWindow::OnMove)
	EVT_AUINOTEBOOK_PAGE_CHANGED(wxID_TabContainer, wxMitkCoreMainWindow::OnPageChanged)
	EVT_MOUSE_EVENTS		(wxMitkCoreMainWindow::OnWxMouseEvent)
	EVT_UPDATE_CALLBACK_EVENT( wxMitkCoreMainWindow::OnUpdateCallback )
	EVT_MAXIMIZE(wxMitkCoreMainWindow::OnMaximize)
	// redirect to the main menu anything that is not handled above, to compensate for the lack of the
	// previously existing link, from the event handler of this class with the event handler of the main menu
	EVT_TOOL (wxID_ANY, wxMitkCoreMainWindow::OnEventToRedirectToMenuBar)
END_EVENT_TABLE();


#define wxID_ProgressDialog (1 + wxID_HIGHEST)

/**
\brief Info frame defined inside wxWIDGETS but is not public

\author Xavi Planes
\date 05 Dec 2008
\ingroup gmWidgets
*/
class WXDLLEXPORT wxInfoFrame : public wxFrame
{
public:
	wxInfoFrame(wxWindow *parent, const wxString& message);

private:
	DECLARE_NO_COPY_CLASS(wxInfoFrame)
};


#if wxUSE_DRAG_AND_DROP
/**
\brief Drop target for Data entity list (i.e. user drags a file from 
explorer unto window and adds the file to entity list)

\author Xavi Planes
\date 05 Dec 2008
\ingroup gmWidgets
*/
class wxDataEntityListDropTarget : public wxFileDropTarget
{
public:
	wxDataEntityListDropTarget(wxMitkCoreMainWindow& mainWindow) : m_mainWindow(mainWindow) {}
	~wxDataEntityListDropTarget(){}
	virtual bool OnDropFiles(wxCoord WXUNUSED(x), wxCoord WXUNUSED(y),
                         const wxArrayString& files)
    {

		std::vector<std::string> pathFilenames;
		for ( unsigned i = 0 ; i < files.size() ; i++ )
		{
			pathFilenames.push_back( _U(files[ i ]) );
		}

		m_mainWindow.GetMainMenu( )->LoadDataEntity( pathFilenames );

		return true;
    }
    wxMitkCoreMainWindow& m_mainWindow;
};
#endif


/** 
Constructor for the class wxMitkCoreMainWindow. It implements a Main Form for the application,
and defines all menus, toolbars, color schemas and font schemas, to provide the application a
friendly-user interface.

Also it will react and dispatch the signals to the other widgets it will contain.
\param parent Parent widget that contains the main window. If there is no parent, call it with NULL
\param name Name for the main window widget (for internal purposes)
\param f Some flags for style spec
*/
wxMitkCoreMainWindow::wxMitkCoreMainWindow(
	wxWindow* parent, 
	wxWindowID id, 
	const wxString& title, 
	const wxPoint& pos, 
	const wxSize& size, 
	long style, 
	const wxString& name) : wxFrame(parent, id,title, pos, size, style, name)
{
	this->SetAutoLayout(true);

	m_Properties = blTagMap::New( );

	m_SplashScreen = NULL;
}

void wxMitkCoreMainWindow::Initialize()
{
	//// Main initialization ////
	try
	{
		CreateStyleManager();

		// Build and attach the menu bar
		CreateMainMenu();

		// Create context menu
		CreateContextMenu( );

		// Populate the containers and widgets by current profile
		CreateWidgets();
		
		// Status bar
		CreateStatusBar( );

		// Create processing dialog
		CreateProcessingDialog();

		// Configure state holder
		AddStateHolderObserver( );


		// Set a caption and icon to the title bar
		Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
		coreAssertMacro(settings.IsNotNull() && "The Settings manager has to be initialized");
		Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
		coreAssertMacro(graphicalIface.IsNotNull() && "The Graphical interface has to be initialized");

		std::string filename = settings->GetCoreResourceForFile("Icon32.png");
		wxIcon pix(_U(filename), wxBITMAP_TYPE_PNG);
		this->SetIcon(pix);

#if wxUSE_DRAG_AND_DROP && !defined(DISABLE_FILE_LOADING)
		// Drag & Drop
		SetDropTarget( new wxDataEntityListDropTarget( *this ) );
#endif

		m_ProgressDialog = NULL;

		WorkingAreaStorage::Pointer workingAreaStorage = WorkingAreaStorage::New();
		workingAreaStorage->ScanDirectory( settings->GetProjectHomePath() );
		graphicalIface->SetWorkingAreaStorage( workingAreaStorage );

		m_WxProcessorEvtHandler = new WxProcessorEvtHandler( );
		PushEventHandler( m_WxProcessorEvtHandler );

		wxGenericEvtHandler = new Core::WxGenericEvtHandler( );
		PushEventHandler( wxGenericEvtHandler );

		WxEventsFactoriesRegistration::Register();

		// Restore main window properties
		blTagMap::Pointer properties;
		properties = settings->GetPluginProperties( "GIMIAS" );
		SetProperties( properties->GetTagValue<blTagMap::Pointer>( "MainWindow" ) );

	}
	coreCatchExceptionsReportAndNoThrowMacro(wxMitkCoreMainWindow::wxMitkCoreMainWindow);
}


/** Destructor for the class wxMitkCoreMainWindow */
wxMitkCoreMainWindow::~wxMitkCoreMainWindow()
{
	if ( Core::Runtime::Kernel::GetGraphicalInterface().IsNull() )
	{
		return;
	}

	// Process pending events before destroying the handler
	wxTheApp->ProcessPendingEvents();

	// Clean memory allocated by plugins
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	if ( graphicalIface.IsNotNull( ) )
	{
		BaseWindowFactories::Pointer baseWindowFactory;
		baseWindowFactory = graphicalIface->GetBaseWindowFactory( );
		if ( baseWindowFactory )
		{
			baseWindowFactory->CleanRegisteredWidgets();
		}
	}

	// Save plugins tab pages layout
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();
	Core::Widgets::PluginTab* page;
	for ( size_t i = 0 ; i < m_TabContainer->GetPageCount( ) ; i++ )
	{
		page = dynamic_cast<Core::Widgets::PluginTab*> ( m_TabContainer->GetPage( i ) );
		switch ( settings->GetPerspective() )
		{
		case Core::Runtime::PERSPECTIVE_PLUGIN:
			{
				settings->SetPluginProperty(
					page->GetCaption( ).c_str(),
					"layout",
					page->GetLayoutConfiguration() );
				// Save working area configuration
				Core::BaseWindow *workingArea = page->GetCurrentWorkingArea( );
				if ( workingArea ) 
				{
					blTagMap::Pointer properties = blTagMap::New( );
					blTagMap::Pointer waProps = blTagMap::New( );

					// true state is for just saving state of planes, layout, ... and not all configuration
					waProps->AddTag( "State", true );

					wxWindow* workingAreaWindow;
					workingAreaWindow = dynamic_cast<wxWindow*> (workingArea);
                    std::string workingAreaName = std::string( workingAreaWindow->GetName( ).c_str( ) );
					properties->AddTag( "WorkingAreaName", workingAreaName );
					
					workingArea->GetProperties( waProps );
					properties->AddTag( "workingArea", waProps );
					settings->AddPluginProperties(
						page->GetCaption( ).c_str(),
						properties );
				}
			}
			break;
		case Core::Runtime::PERSPECTIVE_WORKFLOW:
			// Nothing
			break;
		}
	}

	// Backup main window properties
	blTagMap::Pointer properties;
	properties = settings->GetPluginProperties( "GIMIAS" );
	properties->AddTag( "MainWindow", GetProperties() );
	settings->SetPluginProperties( "GIMIAS", properties );

	// Removes all pushed event handlers (plugins should remove the handlers before!!!)
	wxPopEventHandler( this, m_WxProcessorEvtHandler );
	delete m_WxProcessorEvtHandler;
	m_WxProcessorEvtHandler = NULL;

	//wxPopEventHandler( this, GetMenuBar()->GetEventHandler() ); // Cannot be done after upgrade to WxWidgets 3.0.2

	wxPopEventHandler( this, wxGenericEvtHandler );
	delete wxGenericEvtHandler;
	wxGenericEvtHandler = NULL;

	if ( m_ContextMenu )
	{
		delete m_ContextMenu;
	}

#if wxUSE_DRAG_AND_DROP
	SetDropTarget( NULL );
#endif

	WxEventsFactoriesRegistration::UnRegister();

	m_StyleManager = NULL;

	m_TabContainer->Destroy();

	if ( graphicalIface.IsNotNull( ) )
	{
		graphicalIface->InitializeMainWindow( NULL );
	}

	Core::Runtime::AppStateHolderType::Pointer appStateHolder;
	appStateHolder = Core::Runtime::Kernel::GetApplicationRuntime( )->GetAppStateHolder( );
	appStateHolder->RemoveObserver( 
		this, 
		&wxMitkCoreMainWindow::OnAppStateChanged, 
		Core::DH_SUBJECT_MODIFIED_OR_NEW_SUBJECT);

}


/** 
Creates the widgets for the graphical interface, attaches the connection events
and configures the interface elements in order to hide that widgets out of the
current profile schema.
*/
void wxMitkCoreMainWindow::CreateWidgets(void)
{
	try
	{
		Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
		graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
		coreAssertMacro(graphicalIface.IsNotNull());

		// Create a label to show when the tab page container is empty.
		this->m_EmptyLabel = new wxStaticText(this, wxID_ANY, wxT("\n\n    <No plugin has been loaded into the application. Please choose Edit Profile in the Edit menu.>"));

		// Creates the main tab page container.
		this->m_TabContainer = new wxAuiNotebook(
			this, wxID_TabContainer, wxDefaultPosition, wxDefaultSize, 
			wxAUI_NB_TOP | wxAUI_NB_TAB_SPLIT | wxAUI_NB_TAB_MOVE | wxAUI_NB_SCROLL_BUTTONS );

		wxFont font = *wxNORMAL_FONT;
		font.SetPointSize( 16 );
		m_TabContainer->SetFont( font );

		this->m_TabContainer->Hide();

		wxBoxSizer* vlayout = new wxBoxSizer(wxVERTICAL);
		vlayout->Add(this->m_TabContainer, 1, wxEXPAND);

		//vlayout->Add(hlayout, 0, wxEXPAND);
		this->SetSizer(vlayout);

		// Force relayout the window
		wxSizeEvent event(this->GetSize(), this->GetId());
		event.SetEventObject(this);
		this->GetEventHandler()->ProcessEvent(event);

		PluginTabFactory::RegisterDefaultFactories();

		m_WorkflowEditorWidget = NULL;

		m_WorkflowManagerWidget = NULL;

		// Create preferences dialog
		m_PreferencesDialog = new PreferencesDialog( this, wxID_ANY, "Preferences" );
		m_PreferencesDialog->Center();

		// Is not possible to initialize the window here because
		// plugin manager has not been created yet
		m_CustomApplicationManager = NULL;
	}
	coreCatchExceptionsAddTraceAndThrowMacro(wxMitkCoreMainWindow::CreateWidgets)
}

/** 
*/
void wxMitkCoreMainWindow::CreateMainMenu(void)
{
	try
	{
		// Create the menu bar and attach it
		this->SetMenuBar( new MainMenu( ) );
	
		//PushEventHandler( GetMenuBar()->GetEventHandler() );	// Cannot be done after upgrade to WxWidgets 3.0.2 !.
																// Redirect events to the MenuBar using OnEventToRedirectToMenuBar 
																// and the static table of events

	}
	coreCatchExceptionsAddTraceAndThrowMacro(wxMitkCoreMainWindow::CreateMainMenu)
}
	

/**
 * Redirect events to the Menu bar. 
 */
void wxMitkCoreMainWindow::OnEventToRedirectToMenuBar(wxCommandEvent& event)
{
	if (GetMenuBar()->GetEventHandler()->ProcessEvent(event))
	{
		// if the event is handled
		GetMenuBar()->UpdateMenus(); // refresh menu, to update checked menu items
	}
}

/** 
Add the tab page widget given by a PluginTab. 
This method is called by the GraphicalInterface when FrontEndPluginManager 
tells it to load a plugin object. 
It retrieves the GUI part of the plugin, held by a PluginTab, and inserts 
it as a new Tab page in the
tab page control of the main window
\sa Core::FrontEndPlugin::FrontEndPlugin, Core::Widgets::PluginTab
*/
bool wxMitkCoreMainWindow::AttachPluginGUI(Core::BasePluginTab* basePluginTab)
{
	bool attached = false;

	PluginTab* page = dynamic_cast<PluginTab*> ( basePluginTab );

	// Adds the Tab
	coreAssertMacro(page != NULL && "The tab page holding the GUI of the plugin cannot be NULL");
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	coreAssertMacro(graphicalIface.IsNotNull() && "The Graphical interface has to be initialized");
	try
	{
		page->Reparent(this->m_TabContainer);
		attached = this->m_TabContainer->AddPage(page, _U(page->GetCaption().c_str()), false);
		if(!attached)
		{
			// If the plugin could not be loaded, display error
			Core::Exceptions::OnAttachPluginToGUIException eNoAtt("wxMitkCoreMainWindow::AttachPluginGUI");
			throw eNoAtt;
		}

		this->m_EmptyLabel->Hide();
		this->m_TabContainer->Show();
		
		// Disable the tab if it's not the active one
		wxWindow* currentPage;
		currentPage = m_TabContainer->GetPage( m_TabContainer->GetSelection( ) );
		if ( currentPage != page )
		{
			page->SetActiveTab( false );
		}

		//// Force relayout the window. We cannot do this because there's a bug that
		// if we do this, at the constructor some toolbars, the size in X get the size of the plugin tab page
		//wxSizeEvent event(this->GetSize(), this->GetId());
		//event.SetEventObject(this);
		//this->GetEventHandler()->ProcessEvent(event);
	}
	coreCatchExceptionsAddTraceAndThrowCriticalExceptionMacro(wxMitkCoreMainWindow::AttachPluginGUI);
	return attached;
}


/** 
ARemoves the tab page widget given by a PluginTab. 
This method is called by the GraphicalInterface when FrontEndPluginManager tells it to unload a plugin object. 
It retrieves the GUI part of the plugin, held by a PluginTab, and removes it from the tab page control of 
the main window
\sa Core::FrontEndPlugin::FrontEndPlugin, Core::Widgets::PluginTab
*/
void wxMitkCoreMainWindow::DetachPluginGUI(Core::BasePluginTab* basePluginTab, bool destroy)
{
	PluginTab* page = dynamic_cast<PluginTab*> ( basePluginTab );

	// Adds the Tab
	coreAssertMacro(page != NULL && "The tab page holding the GUI of the plugin cannot be NULL");
	try
	{
		unsigned int i = 0;
		bool found = false;
		while(i < this->m_TabContainer->GetPageCount() && !found)
		{
			found = this->m_TabContainer->GetPage(i) == page;

			if(found)
			{
				this->m_TabContainer->RemovePage(i);

				if ( destroy )
				{
					page->Hide( );
					page->Enable( false );
					page->Destroy();
				}

				break;
			}
			else
			{
				i++;
			}

		}
	}
	coreCatchExceptionsCastToNewTypeAndThrowMacro(Core::Exceptions::OnDetachPluginFromGUIException, 
		wxMitkCoreMainWindow::DetachPluginGUI);
}


Core::BasePluginTab* wxMitkCoreMainWindow::GetCurrentPluginTab(void)
{
	if ( m_TabContainer->GetPageCount() == 0 )
	{
		return NULL;
	}

	wxWindow* currentPage;
	currentPage = m_TabContainer->GetPage( m_TabContainer->GetSelection( ) );
	return dynamic_cast<Core::Widgets::PluginTab*> (currentPage);
}

Core::BasePluginTab* Core::Widgets::wxMitkCoreMainWindow::GetLastPluginTab( void )
{
	wxWindow* page;
	size_t count = m_TabContainer->GetPageCount();
	if ( count == 0 )
	{
		return NULL;
	}
	page = m_TabContainer->GetPage( count - 1 );
	Core::BasePluginTab* pluginTab = dynamic_cast<Core::BasePluginTab*> (page);
	return pluginTab;
}

/** 
Implementation of Close window handler to kill all instances of the application
when the close is issued to the main window. It also prompts for a confirmation.
*/
void wxMitkCoreMainWindow::CloseWindow(void)
{

	// Check if there are pending processors executing
	Core::ProcessorThread::Pointer processorThread;
	int state = ProcessorThread::STATE_PENDING | ProcessorThread::STATE_ACTIVE;
	processorThread = Runtime::Kernel::GetProcessorManager( )->GetProcessorThread( -1, state );
	if ( processorThread.IsNotNull() )
	{
		std::string message ="The application will exit now and\n"
			"there are pending tasks executing.\n"
			"Cancel all tasks?";
		DialogResult question = this->AskQuestionToUserYesOrNo( message, "Exit application?" );
		if ( question == DialogResult_NO)
		{
			return;
		}

		// Detach remote processes
		Runtime::Kernel::GetProcessorManager( )->DetachAll( );

		// Cancel all processes and wait 5 sec.
		blClock clock;
		clock.Start( );
		while( !Runtime::Kernel::GetProcessorManager( )->AbortAll( ) && clock.Finish( ) < 5 )
		{
			::wxSafeYield( );
			wxSleep( 1 );
		}

	}
	else
	{

		DialogResult question = this->AskQuestionToUserYesOrNo(
			"The application will exit now and\n"
			"all unsaved work will be lost.\n"
			"Exit now?",
			"Exit application?"
			);
		if ( question == DialogResult_NO)
		{
			return;
		}
	}
	
	std::string message;
	message = "Closing Application now...";
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	graphicalIface->LogMessage(message);

    Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
    coreAssertMacro(settings.IsNotNull());
    settings->SetFirstTimeStart(false);
	Core::Runtime::Environment::Pointer runtime = Core::Runtime::Kernel::GetApplicationRuntime();
	runtime->Exit();
}

/** 
Displays the log file browser file
*/
void wxMitkCoreMainWindow::ShowLogFileBrowser(void)
{
	Core::Widgets::LogFileViewer* logView = new Core::Widgets::LogFileViewer(this);

	Core::Runtime::Logger::Pointer logger =  Core::Runtime::Kernel::GetLogManager();
	coreAssertMacro(logger.IsNotNull());
	logView->SetFileName( logger->GetFileName() );

	logView->SetSize(600, 600);
	logView->ShowModal();
	logView->Destroy();
}

/** 
Displays the Profile Manager file
*/
void wxMitkCoreMainWindow::ShowProfileManager(void)
{
	try
	{
		Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
		coreAssertMacro(settings.IsNotNull());

		Core::Widgets::ProfileWizard* wizard = new Core::Widgets::ProfileWizard(this);
		bool success;
		if(settings->IsFirstTimeStart())
		{
			success = wizard->RunWizard(wizard->GetStartPage());
		}
		else
		{
			success = wizard->RunWizard(wizard->GetSecondPage());
		}

		if( success )
		{
			wizard->UpdateData( );
			wizard->Destroy();
		}
		else
		{
			Show( true );
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro(wxMitkCoreMainWindow::ShowProfileManager);
}

void Core::Widgets::wxMitkCoreMainWindow::ShowImportConfigurationWizard( void )
{
	try
	{
		Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();

		Core::Widgets::ImportConfigurationWizard* wizard;
		wizard = new Core::Widgets::ImportConfigurationWizard(this);
		if ( !wizard->ScanPreviousConfigurations( ) )
		{
			wizard->Destroy();
			return;
		}

		wizard->RunWizard( wizard->GetStartPage() );

		wizard->Destroy();
	}
	coreCatchExceptionsReportAndNoThrowMacro(wxMitkCoreMainWindow::ShowImportConfigurationWizard);
}

/** 
Displays the About screen.
*/
void wxMitkCoreMainWindow::ShowAboutScreen(void)
{
	try
	{
		Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
		coreAssertMacro(settings.IsNotNull() && "The Settings manager must be running");

		// Create the Splash screen
		std::string filename(settings->GetCoreResourceForFile("Splash.jpg"));
		wxBitmap image( wxString::FromUTF8( filename.c_str() ), wxBITMAP_TYPE_JPEG);

		Core::Widgets::SplashScreen* splashScreen = new Core::Widgets::SplashScreen(image, Core::Widgets::SplashScreen::Behaviour_ABOUT);
		splashScreen->Show();

	}
	coreCatchExceptionsReportAndNoThrowMacro(wxMitkCoreMainWindow::showLogViewer);
}


//!
void wxMitkCoreMainWindow::OnClose(wxCloseEvent& event)
{
	try
	{
		this->CloseWindow();
	}
	coreCatchExceptionsReportAndNoThrowMacro(wxMitkCoreMainWindow::OnClose)
}

/**
Prompts a modal dialog to the user, displaying a question message and 
asking confirmation for YES or NO.
*/
wxMitkCoreMainWindow::DialogResult 
wxMitkCoreMainWindow::AskQuestionToUserYesOrNo(
	const std::string& message, const std::string& caption)
{
	try
	{
		wxMessageDialog question(this, _U(message.c_str()),_U(caption.c_str()),
			wxYES_NO | wxICON_QUESTION | wxSTAY_ON_TOP);
		switch(question.ShowModal())
		{
		case wxID_YES: { return DialogResult_YES; }
			case wxID_NO: { return DialogResult_NO; }
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro(wxMitkCoreMainWindow::AskQuestionToUserYesOrNo);
	return DialogResult_ERROR;

}

/**
Prompts a modal dialog to the user, displaying a question message and 
asking for OK or CANCEL.
*/
wxMitkCoreMainWindow::DialogResult 
wxMitkCoreMainWindow::AskQuestionToUserOkOrCancel(
	const std::string& message, const std::string& caption)
{
	try
	{
		wxMessageDialog* question = new wxMessageDialog(this, _U(message.c_str()),
								_U(caption.c_str()),
			wxOK | wxCANCEL | wxICON_QUESTION | wxSTAY_ON_TOP);
		switch(question->ShowModal())
		{
			case wxID_OK: { return DialogResult_OK; }
			case wxID_CANCEL: { return DialogResult_CANCEL; }
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro(
		wxMitkCoreMainWindow::AskQuestionToUserYesOrNo);

	return DialogResult_ERROR;

}


/** Reports a warning message to the Information pannel and displays a dialog*/
void wxMitkCoreMainWindow::ReportWarning(
	const std::string& message, 
	bool showAlsoDialog)
{
	try
	{
		//if(this->m_MacroPannel != NULL) 
		//	this->m_MacroPannel->ReportWarning(message);
		
		if(showAlsoDialog)
		{
			wxMessageDialog* warning = new wxMessageDialog(this, _U(message.c_str()),
								       wxT("Warning"),
				wxOK | wxICON_WARNING | wxSTAY_ON_TOP);

			warning->ShowModal();
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro(wxMitkCoreMainWindow::ReportWarning);
}


/** Reports an error message to the Information pannel and displays a dialog */
void wxMitkCoreMainWindow::ReportError(const std::string& message, bool showAlsoDialog)
{
	try
	{
		if(showAlsoDialog)
		{
			wxMessageDialog* error = new wxMessageDialog(this, _U(message.c_str()),
								     wxT("Error"),
				wxOK | wxICON_ERROR | wxSTAY_ON_TOP);

			error->ShowModal();
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro(wxMitkCoreMainWindow::ReportError);
}


/** Reports an information message to the Information pannel and displays a dialog */
void wxMitkCoreMainWindow::ReportMessage(const std::string& message, bool showAlsoDialog)
{
	try
	{
		if(showAlsoDialog)
		{
			wxMessageDialog* dialog = new wxMessageDialog(this, _U(message.c_str()),
								      wxT("Info"),
				wxOK | wxICON_INFORMATION | wxSTAY_ON_TOP);

			dialog->ShowModal();
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro(wxMitkCoreMainWindow::ReportMessage);
}

/** Writes a raw text message to the Information pannel. This call is 
used for writing script commands (macros) 
to the macro pannel, displayed verbatim */
void wxMitkCoreMainWindow::WriteVerbatimToMacroPanel(const std::string& message)
{
	try
	{
		//if(this->m_MacroPannel != NULL) 
		//	this->m_MacroPannel->WriteVerbatim(message);
	}
	coreCatchExceptionsReportAndNoThrowMacro(wxMitkCoreMainWindow::ReportMessage);
}

void Core::Widgets::wxMitkCoreMainWindow::OnPageChanged( wxAuiNotebookEvent& event )
{
//	wxWindowUpdateLocker lock( this );

	// First disable all Plugins tab to avoid having to plugins tab active 
	// at the same moment
	Core::Widgets::PluginTab* page;
	for ( size_t i = 0 ; i < m_TabContainer->GetPageCount( ) ; i++ )
	{
		page = dynamic_cast<Core::Widgets::PluginTab*> ( m_TabContainer->GetPage( i ) );
		page->SetActiveTab( false );
	}

	// Second, enable the active one
	for ( size_t i = 0 ; i < m_TabContainer->GetPageCount( ) ; i++ )
	{
		page = dynamic_cast<Core::Widgets::PluginTab*> ( m_TabContainer->GetPage( i ) );
		if ( m_TabContainer->GetSelection( ) == i )
		{
			page->SetActiveTab( true );
			if ( GetMainMenu() )
			{
				GetMainMenu()->SetPluginTab( page );
			}
		}
	}

}

void Core::Widgets::wxMitkCoreMainWindow::CreateContextMenu( )
{
	MainContextMenu* contextMenu = new MainContextMenu( );
	m_ContextMenu = contextMenu;
}

void wxMitkCoreMainWindow::OnWxMouseEvent(wxMouseEvent& event)
{
	try
	{
		//if( event.GetButton() == wxMOUSE_BTN_RIGHT )
		//{
		//	if( event.RightDown() && !event.ShiftDown() )
		//	{
		//		m_bShowContextMenu = true;
		//	}
		//	else
		//	{
		//		if ( m_bShowContextMenu &&
		//			 m_TabContainer->GetRect().Contains( event.GetPosition() ) )
		//		{
		//			PopupMenu( m_ContextMenu );
		//		}
		//	}

		//}

		//if ( event.Dragging( ) || event.Moving( ) )
		//{
		//	m_bShowContextMenu = false;
		//}

		event.Skip( );
	}
	coreCatchExceptionsReportAndNoThrowMacro(wxMitkCoreMainWindow::OnWxMouseEvent)
}

void wxMitkCoreMainWindow::CreateProcessingDialog()
{
	m_processingDialog = new wxInfoFrame( this, wxT("Processing... Please wait") );
	m_processingDialog->Show( false );
}

void wxMitkCoreMainWindow::AddStateHolderObserver()
{
	Core::Runtime::AppStateHolderType::Pointer appStateHolder;
	appStateHolder = Core::Runtime::Kernel::GetApplicationRuntime( )->GetAppStateHolder( );
	appStateHolder->AddObserver( 
		this, 
		&wxMitkCoreMainWindow::OnAppStateChanged, 
		Core::DH_SUBJECT_MODIFIED_OR_NEW_SUBJECT);
}


void wxMitkCoreMainWindow::ShowProcessingDialog( bool show )
{
	if ( !IsVisible() )
	{
		return;
	}

	if ( show )
	{
		// Show hour glass icon
		SetCursor( wxCursor( wxCURSOR_WAIT ) );

		m_processingDialog->CenterOnParent();
		m_processingDialog->Show( true );
		// We need to refresh the dialog
		m_processingDialog->Update( );
	}
	else
	{
		// Show hour glass icon
		SetCursor( wxNullCursor );

		m_processingDialog->Show( false );
	}
}

/**
*/
void wxMitkCoreMainWindow::OnAppStateChanged( )
{
	if ( !wxIsMainThread()  )
	{
		Core::WxGenericEvent event;
		event.SetFunction( this, &wxMitkCoreMainWindow::OnAppStateChanged );
    wxPostEvent(this->GetEventHandler(), event);
		return;
	}
	
	try
	{
		Core::Runtime::AppStateHolderType::Pointer appStateHolder;
		appStateHolder = Core::Runtime::Kernel::GetApplicationRuntime( )->GetAppStateHolder( );

		switch( appStateHolder->GetSubject( ) )
		{
		case Core::Runtime::APP_STATE_PROCESSING:
			ShowProcessingDialog( true );
			break;
		case Core::Runtime::APP_STATE_IDLE:
			// hide the processing dialog
			ShowProcessingDialog( false );
			break;
		default:
			break;
		}
	}
	coreCatchOnlyRuntimeExceptionsReportAndNoThrowMacro(
		wxMitkCoreMainWindow::OnAppStateChanged)
}

wxStatusBar* Core::Widgets::wxMitkCoreMainWindow::OnCreateStatusBar( 
	int number, long style, wxWindowID id, const wxString& name )
{
	return new Core::Widgets::StatusBar( this );
}

MainMenu* Core::Widgets::wxMitkCoreMainWindow::GetMainMenu()
{
	return dynamic_cast<MainMenu*> ( GetMenuBar() );
}

void Core::Widgets::wxMitkCoreMainWindow::CreatePluginTab( 
	const std::string &caption )
{
	Core::Widgets::PluginTab* pluginTab;
	pluginTab = Core::Widgets::PluginTabFactory::Build( caption );

	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface; 
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	if ( graphicalIface->GetPluginProviderManager() )
	{
		std::string pluginName = graphicalIface->GetPluginProviderManager()->GetCurrentPluginName();
		pluginTab->GetProperties( )->AddTag( "PluginName", pluginName );
	}

	AttachPluginGUI( pluginTab );
}


void Core::Widgets::wxMitkCoreMainWindow::RegisterWindow( 
	Core::BaseWindow *baseWindow,
	Core::WindowConfig config /*= Core::WindowConfig( )*/ )
{
	if ( !config.GetProcessingTool() )
	{
		GetLastPluginTab()->AddWindow( baseWindow, config );
	}
}

void Core::Widgets::wxMitkCoreMainWindow::RegisterWindow( 
	const std::string &factoryName,
	Core::WindowConfig config /*= Core::WindowConfig( )*/ )
{
	std::list<Core::BasePluginTab*> list;
	list = GetAllPluginTabs( );
	std::list<Core::BasePluginTab*>::iterator it;
	for ( it = list.begin() ; it != list.end( ) ; it++ )
	{
		try
		{
			PluginTab* page = dynamic_cast<PluginTab*> (*it);
			PluginTabFactory::AddWindow( factoryName, page );
		}
		coreCatchExceptionsReportAndNoThrowMacro(RegisterWindow)

	}
}

void Core::Widgets::wxMitkCoreMainWindow::UnRegisterWindow( const std::string &factoryName )
{
	std::list<Core::BasePluginTab*> list = GetAllPluginTabs( );
	std::list<Core::BasePluginTab*>::iterator it;
	for ( it = list.begin() ; it != list.end( ) ; it++ )
	{
		try
		{
			(*it)->RemoveWindow( factoryName );
		}
		coreCatchExceptionsReportAndNoThrowMacro(RegisterWindow)

	}
}

std::list<Core::BasePluginTab*> Core::Widgets::wxMitkCoreMainWindow::GetAllPluginTabs()
{
	std::list<Core::BasePluginTab*> list;

	Core::BasePluginTab* page;
	for ( size_t i = 0 ; i < m_TabContainer->GetPageCount( ) ; i++ )
	{
		page = dynamic_cast<Core::BasePluginTab*> ( m_TabContainer->GetPage( i ) );
		list.push_back( page );
	}	

	return list;
}

void Core::Widgets::wxMitkCoreMainWindow::AttachWorkflow( Workflow::Pointer workflow )
{
	if ( workflow.IsNull() )
	{
		return;
	}

	wxAuiNotebook* notebook;

	try
	{
		// First we should destroy the old tab container to avoid error
		// of too many windows created in Windows
		m_TabContainer->Destroy( );
		Update( );

		std::string message = "Building workflow GUI...";
		Core::Runtime::Kernel::GetGraphicalInterface()->LogMessage(message);
		Core::Runtime::Kernel::GetLogManager()->LogMessage(message);
		std::cout << message << std::endl;

		notebook = Core::Widgets::WorkflowGUIBuilder::Build( workflow );
		notebook->SetId( wxID_TabContainer );
		m_EmptyLabel->Hide();

		m_TabContainer = notebook;

		wxBoxSizer* vlayout = new wxBoxSizer(wxVERTICAL);
		vlayout->Add(m_TabContainer, 1, wxEXPAND);

		this->SetSizer(vlayout);

		GetSizer( )->Layout();

		// Update menus
		wxAuiNotebookEvent event = wxAuiNotebookEvent();
		OnPageChanged( event );

		// Cast a resize event
		wxSizeEvent resEvent(GetBestSize(), GetId());
		resEvent.SetEventObject( this );
		GetEventHandler()->ProcessEvent(resEvent);

	}
	coreCatchExceptionsReportAndNoThrowMacro( wxMitkCoreMainWindow::AttachWorkflow )
}

void Core::Widgets::wxMitkCoreMainWindow::ShowWorkflowEditor( bool show /*= true */ )
{
	if ( m_WorkflowEditorWidget == NULL )
	{
		m_WorkflowEditorWidget = new WorkflowEditorWidget( this , wxID_WorkflowEditor );
	}

	Core::Workflow::Pointer workflow;
	workflow = Core::Runtime::Kernel::GetWorkflowManager()->GetActiveWorkflow( );
	m_WorkflowEditorWidget->SetWorkflow( workflow );

	m_WorkflowEditorWidget->Show( show );
}

void Core::Widgets::wxMitkCoreMainWindow::ShowWorkflowManager( bool show /*= true */ )
{
	if ( m_WorkflowManagerWidget == NULL )
	{
		m_WorkflowManagerWidget = new WorkflowManagerWidget( this , wxID_WorkflowManager );
	}

	m_WorkflowManagerWidget->Show( show );
}

void Core::Widgets::wxMitkCoreMainWindow::RestartApplication()
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();

	// Show a confirmation for restart
	std::string message;
	if(settings->IsFirstTimeStart())
	{
		message = \
			"The application will restart in order.\n" \
			"to apply the selected configuration.\n" \
			"Remember that you can run again this wizard\n" \
			"at any time from the application menu.\n"
			"Exit now?";
	}
	else
	{
		message = \
			"The application needs to restart in order.\n" \
			"to apply the new configuration.\n" \
			"All unsaved data will be lost if you proceed.\n" \
			"Make sure to save all your data before\n" \
			"continuing.\n" \
			"Exit now?";
	}

	wxMessageDialog question(
		this, _U(message.c_str()), _U( "Exit application?" ),
		wxYES_NO | wxICON_QUESTION | wxSTAY_ON_TOP);
	if ( question.ShowModal() == wxID_YES)
	{
		if(graphicalIface.IsNotNull())
			graphicalIface->LogMessage("Restarting application now...");
		settings->SetFirstTimeStart(false);
		Core::Runtime::Environment::Pointer runtime;
		runtime = Core::Runtime::Kernel::GetApplicationRuntime();
		coreAssertMacro(runtime.IsNotNull());
		runtime->RestartApplication();
	}

}


void Core::Widgets::wxMitkCoreMainWindow::UpdateLayout()
{

	wxWindowUpdateLocker lock( this );

	std::list<Core::BasePluginTab*> list = GetAllPluginTabs( );
	std::list<Core::BasePluginTab*>::iterator it;
	for ( it = list.begin() ; it != list.end( ) ; it++ )
	{
		// Backup default layout
		(*it)->BackupLayoutConfiguration();

		// Configure plugin tab layout
		Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
		std::string strConfig;
		settings->GetPluginProperty( (*it)->GetCaption( ).c_str(), "layout", strConfig );
		(*it)->UpdateLayoutConfiguration( strConfig );

		// Configure working area
		std::string workingAreaName;
		settings->GetPluginProperty( (*it)->GetCaption( ).c_str(), "WorkingAreaName", workingAreaName );
		Core::Widgets::PluginTab* page;
		page = dynamic_cast<Core::Widgets::PluginTab*> (*it);
		if ( page == NULL || page->GetWorkingAreaManager( ) == NULL )
			continue;
		
		// If working area does not exist, continue
		wxWindow *win = page->GetWorkingAreaManager( )->GetWorkingArea( workingAreaName );
		if ( win == NULL )
			continue;

		// Set active
		page->GetWorkingAreaManager( )->SetActiveWorkingArea( win->GetId( ) );
		Core::BaseWindow *workingArea = page->GetCurrentWorkingArea( );

		if ( !workingArea ) 
			continue;

		// Update properties
		blTagMap::Pointer properties;
		properties = settings->GetPluginProperties( page->GetCaption( ).c_str() );
		blTagMap::Pointer waProps = properties->GetTagValue<blTagMap::Pointer>( "workingArea" );
		if ( waProps.IsNotNull( ) )
		{
			workingArea->SetProperties( waProps );
		}
	}

	// Refresh active tab after creating all tab pages because when creating a tab page, 
	// the interactors are enabled, disabled and the menus are changed
	if ( m_TabContainer->GetSelection( ) != -1 )
	{
		wxWindow* currentPage;
		currentPage = m_TabContainer->GetPage( m_TabContainer->GetSelection( ) );
		PluginTab* page = dynamic_cast<PluginTab*> ( currentPage );
		if ( page )
		{
			page->SetActiveTab( true );
		}
	}

	// Force relayout the window. 
	wxSizeEvent event(this->GetSize(), this->GetId());
	event.SetEventObject(this);
	this->GetEventHandler()->ProcessEvent(event);
}

void Core::Widgets::wxMitkCoreMainWindow::InitializePerspective( )
{
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();

	if ( settings->GetPerspective() == Core::Runtime::PERSPECTIVE_PLUGIN )
	{
		std::list<Core::BasePluginTab*> allPluginsTabs = GetAllPluginTabs();
		if ( !allPluginsTabs.empty( ) )
		{
			std::cout << "Detaching old GUI" << "..." << std::endl;
		}

		std::list<Core::BasePluginTab*>::iterator it;
		for ( it = allPluginsTabs.begin() ; it != allPluginsTabs.end( ) ; it++ )
		{
			blTag::Pointer tag = (*it)->GetProperties( )->GetTag( "PluginName" );
			if ( tag.IsNull( ) )
			{
				DetachPluginGUI( *it, true );
			}
		}
	}
	else if ( settings->GetPerspective() == Core::Runtime::PERSPECTIVE_WORKFLOW )
	{
		// Remove all plugin tab pages
		std::list<Core::BasePluginTab*> allPluginsTabs = GetAllPluginTabs();
		if ( !allPluginsTabs.empty( ) )
		{
			std::cout << "Detaching old GUI" << "..." << std::endl;
		}

		std::list<Core::BasePluginTab*>::iterator it;
		for ( it = allPluginsTabs.begin() ; it != allPluginsTabs.end( ) ; it++ )
		{
			DetachPluginGUI( *it, true );
		}
	}
}

void Core::Widgets::wxMitkCoreMainWindow::OnGraphicalInterfaceInitialized()
{
	std::string message;
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();

	// Create preferences window and attach kernel instances
	// This widget will manage the callback messages from the plugin providers
	m_PreferencesDialog->CreatePreferencesWindow( );
	for ( int i = 0 ; i < m_PreferencesDialog->GetPreferencesWindow( )->GetPageCount( ) ; i++ )
	{
		Core::Widgets::PluginSelectorWidget* page;
		page = dynamic_cast<Core::Widgets::PluginSelectorWidget*> 
			( m_PreferencesDialog->GetPreferencesWindow( )->GetPage( i ) );
		if ( page )
		{
			page->EnableCallbackMessages( true );
		}
	}
	
	// Load configuration from settings
	Core::Runtime::Kernel::GetGraphicalInterface( )->GetPluginProviderManager( )->LoadConfiguration( true );

	// Scan plugin providers before anything and show results in splash screen
	if( settings->IsFirstTimeStart())
	{
		Core::Runtime::Kernel::GetGraphicalInterface( )->GetPluginProviderManager( )->ScanPlugins( "", false );
	}

	// Registration form
	if ( settings->GetShowRegistrationForm() )
	{
		ShowSplashScreen( false );
		ShowRegistrationForm( );
	}


	// If is the first time start, do not build widgets and run the profile wizard instead
	if( settings->IsFirstTimeStart())
	{
		message = "Starting for first time...\nRunning the Import configuration";
		Core::Runtime::Kernel::GetGraphicalInterface( )->LogMessage(message);
		
		// Kill the splash screen
		ShowSplashScreen( false );

		// Display the Profile Wizard for importing previous configurations
		//ShowImportConfigurationWizard();
	}

	// Check previous kernel state
	Core::Runtime::KERNEL_STATE prevState;
	prevState = Core::Runtime::Kernel::GetApplicationRuntime()->GetPreviousKernelState( );
#if defined(NDEBUG)
	if ( !settings->IsFirstTimeStart() && 
		 ( prevState > Core::Runtime::KERNEL_STATE_UNKNOWN && prevState < Core::Runtime::KERNEL_STATE_RUNNING_MAIN_LOOP ) )
	{
		std::stringstream stream;
		stream << "There was an error when starting GIMIAS last time" << std::endl;
		stream << "In the step: " << Core::Runtime::Kernel::GetApplicationRuntime()->GetKernelStateName( prevState ) << std::endl;
		switch( prevState )
		{
		case Core::Runtime::KERNEL_STATE_LOADING_CONFIGURATION:
			stream << "Configuration has not been loaded this time" << std::endl;
			stream << "Please configure GIMIAS again" << std::endl;
			break;
		case Core::Runtime::KERNEL_STATE_INITIALIZING_KERNEL:
			stream << "Please contact GIMIAS team at cistib.developers@gmail.com" << std::endl;
			break;
		case Core::Runtime::KERNEL_STATE_LOADING_PLUGINS:
			stream << "Please select another plugin configuration" << std::endl;
			break;
		}

		ReportError( stream.str(), true );

		// Restart application after selecting the plugins
		settings->SetFirstTimeStart( true );

		// Select the plugins
		ShowProfileManager();

	}
	else 
#endif
	if ( settings->IsFirstTimeStart() )
	{
		message = "Running the Profile Manager";
		Core::Runtime::Kernel::GetGraphicalInterface( )->LogMessage(message);

		ShowProfileManager();
	}

	// Clean plugin temporary data from previous executions
	CleanPluginTemporaryData( );

	// Update plugin providers
	if ( !settings->IsFirstTimeStart() )
	{
		Core::Runtime::Kernel::GetGraphicalInterface( )->GetPluginProviderManager( )->UpdateProviders( "", true );
	}

	// Check for udpates
	m_WebUpdateDlg = new Core::Widgets::WebUpdateDialog( this, wxID_ANY, "GIMIAS update" );
	
	// Rename Downloaded GIMIAS updater
#ifdef WIN32
	wxString downloadedApp( settings->GetApplicationPath() + "/_tpExtLibWebUpdateAppApplications_webapp.exe" );
	wxString newDownloadedApp( settings->GetApplicationPath() + "/tpExtLibWebUpdateAppApplications_webapp.exe" );
#else
	wxString downloadedApp( settings->GetApplicationPath() + "/_tpExtLibWebUpdateAppApplications_webapp" );
	wxString newDownloadedApp( settings->GetApplicationPath() + "/tpExtLibWebUpdateAppApplications_webapp" );
#endif
	if ( wxFileName( downloadedApp ).FileExists() )
	{
		wxRenameFile( downloadedApp, newDownloadedApp, true );
	}
	
	// Check date for updates
	std::string enable;
	settings->GetPluginProperty( "GIMIAS", "Enable Automatic Updates", enable );
	if ( enable != "false" )
	{
		std::string minDays = "7";
		settings->GetPluginProperty( "GIMIAS", "Min days for Automatic Updates", minDays );

		// Local time
		wxDateTime localTime( *wxDateTime::GetTmNow() );

		bool checkForUpdates = true;

		// Last update time
		std::string lastDay;
		if ( settings->GetPluginProperty( "GIMIAS", "Last day checked for updates", lastDay ) )
		{
			wxDateTime lastTime;
			lastTime.ParseFormat( lastDay.c_str( ), "%x" );

			wxTimeSpan diff = localTime - lastTime;
			checkForUpdates = diff.GetDays( ) >= atoi( minDays.c_str( ) );
		}

		if ( checkForUpdates )
		{
			CheckForUpdates();
		}
	}
}
	
std::string Core::Widgets::wxMitkCoreMainWindow::CreateModuleDescriptionCaption( 
	ModuleDescription *module )
{
	std::string caption = module->GetTitle();
	if ( module->GetAcknowledgements().find( "Insight Software Consortium" )  != std::string::npos )
	{
		caption = module->GetTitle() + " (ITK)";
	}
	else if ( module->GetAcknowledgements().find( "NAMIC" ) != std::string::npos ||
		 module->GetDocumentationURL().find( "slicer" ) != std::string::npos )
	{
		caption = module->GetTitle() + " (3D Slicer)";
	}
	else if ( module->GetDynamicLibrary().find( "vtk" ) == 0 )
	{
		caption = module->GetTitle() + " (VTK)";
	}
	else if ( module->GetDynamicLibrary().find( "mitk" ) == 0 )
	{
		caption = module->GetTitle() + " (MITK)";
	}

	return caption;
}

void Core::Widgets::wxMitkCoreMainWindow::RegisterModule( 
	const std::string &pluginProvider, ModuleDescription *module )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	// Special patch to add "Slicer" to slicer command line plugins
	std::string caption = CreateModuleDescriptionCaption( module );

	// Find factory
	std::string factoryName = FindFactoryNameByModule( module );

		
	// ModuleDescription will be used to add help information in 
	// PluginTab::InitBaseWindow( )
	// Or as a new module
	// There could be multiple modules for a single DynProcessingWidgetFactory widget
	// for example SSH plugin provider and CommandLine Plugin Provider
	if ( !factoryName.empty() )
	{
		Core::BaseWindowFactory::Pointer factory;
		factory = graphicalIface->GetBaseWindowFactory( )->FindFactory( factoryName );
		factory->SetModule( pluginProvider, *module );
	}
	// Create a new DynProcessingWidget
	else if ( factoryName.empty() )
	{
		DynProcessingWidgetFactory::Pointer factory = DynProcessingWidgetFactory::New( );
		factory->SetModule( pluginProvider, *module );
		graphicalIface->RegisterFactory( 
			factory.GetPointer( ),
			WindowConfig( ).Category( module->GetCategory() )
			.ProcessingTool().ProcessorObservers().Caption( caption ) );
	}
	else
	{
		Core::Runtime::Kernel::GetGraphicalInterface()->LogMessage("Module " + module->GetTitle() + " not loaded");
	}
}

void Core::Widgets::wxMitkCoreMainWindow::UnRegisterModule( 
	const std::string &pluginProvider, ModuleDescription *module )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	// Special patch to add "Slicer" to slicer command line plugins
	std::string caption = CreateModuleDescriptionCaption( module );

	// Find factory
	std::string factoryName = FindFactoryNameByModule( module );
		
	if ( !factoryName.empty() )
	{
		Core::BaseWindowFactory::Pointer factory;
		factory = graphicalIface->GetBaseWindowFactory( )->FindFactory( factoryName );
		factory->RemoveModule( pluginProvider );
		
		// If there are no more modules, remove factory
		if ( factory->GetModules().empty( ) && 
			 ( dynamic_cast<DynProcessingWidgetFactory*>( factory.GetPointer( ) ) ) != 0 )
		{
			graphicalIface->GetBaseWindowFactory( )->UnRegisterFactory( factoryName );
		}
	}
}

std::string Core::Widgets::wxMitkCoreMainWindow::FindFactoryNameByModule( 
	ModuleDescription *module )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	// Special patch to add "Slicer" to slicer command line plugins
	std::string caption = CreateModuleDescriptionCaption( module );

	std::list<std::string> windows;
	BaseWindowFactorySearch::Pointer search = BaseWindowFactorySearch::New( );
	
	// Search the factory using the caption or hierarchy
	if ( !module->GetHierarchy( ).empty() )
	{
		// Search the factory using the Hierarchy. This is used when module is 
		// of type RawDynLibModule (single XML file)
		// The ModuleDescription can be used for DLL execution or single Widget interface
		search->SetInstanceClassName( module->GetHierarchy() );
		search->Update( );
		windows = search->GetFactoriesNames();
	}

	// If no window found -> use the caption
	if ( windows.empty() )
	{
		search->SetCaption( caption );
		search->SetInstanceClassName( "" );
		search->Update( );
		windows = search->GetFactoriesNames();
	}
		
	if ( windows.empty() )
	{
		return "";
	}

	return *windows.begin( );
}


bool Core::Widgets::wxMitkCoreMainWindow::ShowMissingDataEntityFieldsWizard( 
	Core::DataEntity::Pointer dataEntity )
{
	bool result = false;
	try
	{
		MissingDataEntityFieldsWizard* wizard;
		wizard = new MissingDataEntityFieldsWizard(this, dataEntity);
		wxWizardPage* startPage = wizard->GetStartPage();
		if(startPage != NULL)
		{
			result = wizard->RunWizard(startPage);
		}

		if(result)
		{
			// If it was not canceled, apply the user choices
			dataEntity->GetMetadata( )->SetModality(wizard->GetSelectedModality());
		}
		wizard->Destroy();
	}
	coreCatchExceptionsReportAndNoThrowMacro(
		MainMenu::ShowMissingDataEntityFieldsWizard);
	return result;
}

void Core::Widgets::wxMitkCoreMainWindow::UpdateActiveWorkflowLayout()
{
	Core::Widgets::PluginTab* page;
	for ( size_t i = 0 ; i < m_TabContainer->GetPageCount( ) ; i++ )
	{
		page = dynamic_cast<Core::Widgets::PluginTab*> ( m_TabContainer->GetPage( i ) );
		Core::Workflow::Pointer workflow;
		workflow = Core::Runtime::Kernel::GetWorkflowManager()->GetActiveWorkflow( );
		if ( workflow.IsNotNull() )
		{
			Core::WorkflowStep::Pointer step;
			step = workflow->GetStep( page->GetCaption() );
			if ( step.IsNotNull() )
			{
				step->GetProperties()->AddTags( page->GetProperties( ) );
			}
		}
	}
}

void Core::Widgets::wxMitkCoreMainWindow::ShowRegistrationForm( void )
{
	UserRegistrationDialog dialog( this, wxID_ANY, "User Registration" );
	dialog.Center();
	dialog.ShowModal();
}

void Core::Widgets::wxMitkCoreMainWindow::ShowPreferencesdialog( void )
{
	if ( !m_PreferencesDialog->IsShown() )
	{
		m_PreferencesDialog->Show( );
		if ( m_Properties->FindTagByName( "Preferences" ).IsNull() )
		{
			m_PreferencesDialog->Fit();
		}
		
	}
}

void Core::Widgets::wxMitkCoreMainWindow::ShowCustomApplicationManager( void )
{
	if ( m_CustomApplicationManager == NULL )
	{
		m_CustomApplicationManager = new CustomApplicationManagerWidget( this, wxID_ANY );
	}

	if ( !m_CustomApplicationManager->IsShown() )
	{
		m_CustomApplicationManager->Center();
		m_CustomApplicationManager->Show( );
	}
}

void Core::Widgets::wxMitkCoreMainWindow::OnUpdateCallback( 
	WxUpdateCallbackEvent& event )
{
	GetStatusBar()->ProcessWindowEvent( event );

	event.Skip();
}

blTagMap::Pointer Core::Widgets::wxMitkCoreMainWindow::GetProperties()
{
	m_Properties->AddTag( "Maximized", IsMaximized( ) );
	m_Properties->AddTag( "XPos", GetPosition().x );
	m_Properties->AddTag( "YPos", GetPosition().y );
	m_Properties->AddTag( "Width", GetSize().GetWidth() );
	m_Properties->AddTag( "Height", GetSize().GetHeight() );

	blTagMap::Pointer preferences = blTagMap::New( );
	preferences->AddTag( "XPos_Pref", m_PreferencesDialog->GetPosition().x );
	preferences->AddTag( "YPos_Pref", m_PreferencesDialog->GetPosition().y );
	preferences->AddTag( "Width_Pref", m_PreferencesDialog->GetSize().GetWidth() );
	preferences->AddTag( "Height_Pref", m_PreferencesDialog->GetSize().GetHeight() );
	m_Properties->AddTag( "Preferences", preferences );
	return m_Properties;
}

void Core::Widgets::wxMitkCoreMainWindow::SetProperties( blTagMap::Pointer properties )
{
	if ( properties.IsNull() )
	{
		// Status bar position is not updated
		//Maximize( true );
		m_Properties->AddTag( "Maximized", true );
		return;
	}

	m_Properties = properties;
	if ( m_Properties->FindTagByName( "Maximized" ).IsNotNull() )
	{
		Maximize( m_Properties->GetTagValue<bool>( "Maximized" ) );
	}

	if ( m_Properties->FindTagByName( "XPos" ).IsNotNull() && 
		 m_Properties->FindTagByName( "YPos" ).IsNotNull() )
	{
		wxPoint pos( m_Properties->GetTagValue<int>( "XPos" ), m_Properties->GetTagValue<int>( "YPos" ) );
		if ( pos.x >= 0 && pos.y >= 0 )
		{
			SetPosition( pos );
		}
	}

	if ( m_Properties->FindTagByName( "Width" ).IsNotNull() && 
		 m_Properties->FindTagByName( "Height" ).IsNotNull() )
	{
		wxSize size( m_Properties->GetTagValue<int>( "Width" ), m_Properties->GetTagValue<int>( "Height" ) );
		if ( size.GetHeight( ) > 0 && size.GetWidth( ) > 0 )
		{
			SetSize( size );
		}
	}

	if ( m_Properties->FindTagByName( "Preferences" ).IsNotNull() )
	{
		blTagMap::Pointer preferences;
		preferences = m_Properties->GetTagValue<blTagMap::Pointer>( "Preferences" );

		wxPoint pos( preferences->GetTagValue<int>( "XPos_Pref" ), preferences->GetTagValue<int>( "YPos_Pref" ) );
		if ( pos.x >= 0 && pos.y >= 0 )
		{
			m_PreferencesDialog->SetPosition( pos );
		}
		wxSize size( preferences->GetTagValue<int>( "Width_Pref" ), preferences->GetTagValue<int>( "Height_Pref" ) );
		if ( size.GetHeight( ) > 0 && size.GetWidth( ) > 0 )
		{
			m_PreferencesDialog->SetSize( size );
		}
		
	}

}

void Core::Widgets::wxMitkCoreMainWindow::OnMove(wxMoveEvent& event)
{
	if ( GetStatusBar() )
	{
		GetStatusBar()->ProcessWindowEvent( event );
	}
}

PreferencesDialog* Core::Widgets::wxMitkCoreMainWindow::GetPreferencesDialog() const
{
	return m_PreferencesDialog;
}

void Core::Widgets::wxMitkCoreMainWindow::ShowSplashScreen( bool show )
{
	if ( show )
	{
		CreateSplashScreen( );
	}
	else
	{
		// Unload the splash screen
		if(m_SplashScreen != NULL)
		{
			m_SplashScreen->Close(true);
			m_SplashScreen = NULL;
		}
	}
}

void Core::Widgets::wxMitkCoreMainWindow::ReportSplashMessage( const std::string& message )
{
	if(m_SplashScreen != NULL 
		&& m_SplashScreen->GetBehaviour() == Core::Widgets::SplashScreen::Behaviour_SPLASH)
	{
		m_SplashScreen->AppendToStartupMessages(message);
	}
}


void Core::Widgets::wxMitkCoreMainWindow::CreateSplashScreen()
{
	Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
	#if !defined(coreNoLoadSplashScreen) && defined(NDEBUG)
		try
		{
			// Create the Splash screen
			std::string filename(settings->GetCoreResourceForFile("Splash.jpg"));
			wxBitmap image( wxString::FromUTF8( filename.c_str() ), wxBITMAP_TYPE_JPEG);

			m_SplashScreen = new Core::Widgets::SplashScreen(
				image, 
				Core::Widgets::SplashScreen::Behaviour_SPLASH,
				this );
			m_SplashScreen->Show();

		}
		coreCatchExceptionsCastToNewTypeAndNoThrowMacro(Core::Exceptions::SplashWindowBitmapIsNotSetException, wxMitkGraphicalInterface::wxMitkGraphicalInterface);
	#endif
}

void Core::Widgets::wxMitkCoreMainWindow::SetTitle( const std::string &title )
{
	wxFrame::SetTitle(  wxString::FromUTF8( title.c_str() ) );
}

bool Core::Widgets::wxMitkCoreMainWindow::Show( bool show )
{
	bool retVal = wxFrame::Show( show );

	// Status bar will only be updated if main window is shown
	if ( show && m_Properties->FindTagByName( "Maximized" ).IsNotNull() )
	{
		Maximize( m_Properties->GetTagValue<bool>( "Maximized" ) );
	}

	return retVal;
}

void Core::Widgets::wxMitkCoreMainWindow::CreateStyleManager()
{
	m_StyleManager = Core::Runtime::StyleManager::New();
}

void Core::Widgets::wxMitkCoreMainWindow::ShowWebUpdater( bool show )
{
	m_WebUpdateDlg->ShowModal( );
}

void Core::Widgets::wxMitkCoreMainWindow::CheckForUpdates()
{
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();

	// Check for updates
	m_WebUpdateDlg->GetUpdateList();

	// Save current date
	wxDateTime localTime( *wxDateTime::GetTmNow() );
	settings->SetPluginProperty( "GIMIAS", "Last day checked for updates", localTime.FormatDate().ToStdString() );

}

void Core::Widgets::wxMitkCoreMainWindow::Lock( bool lock /*= true */ )
{
	Enable( !lock );

	if ( lock )
	{
		Freeze( );
		GetMainMenu()->Enable( !lock );
	}
	else
	{
		GetMainMenu()->Enable( !lock );
		GetMainMenu()->UpdateViewMenuItems();
		GetMainMenu()->UpdateRegisteredWindowsMenuItems( );
		Thaw( );
	}
}

void Core::Widgets::wxMitkCoreMainWindow::OnMaximize( wxMaximizeEvent& event )
{
	if ( GetStatusBar() )
	{
		wxSizeEvent resEvent(GetStatusBar()->GetBestSize(), GetStatusBar()->GetId( ));
		resEvent.SetEventObject( GetStatusBar() );
		GetStatusBar()->GetEventHandler( )->ProcessEvent( resEvent );
	}
}

void Core::Widgets::wxMitkCoreMainWindow::UpdatePluginConfiguration()
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();

	std::string restartGIMIASStr;
	settings->GetPluginProperty( "GIMIAS", "RestartGIMIASForPluginConfiguration", restartGIMIASStr );
	bool restartGIMIAS = restartGIMIASStr == "true" ? true : false;
	if ( restartGIMIAS )
	{
		RestartApplication( );
	}
	else
	{

		// Show a confirmation for restart
		bool loadPlugins = true;
		std::string message;
		if ( !settings->IsFirstTimeStart() )
		{
			Core::DataEntityList::Pointer dataEntityList;
			dataEntityList = Core::Runtime::Kernel::GetDataContainer()->GetDataEntityList( );
			
			if ( dataEntityList->GetCount( ) > 1 )
			{
				// Get memory owners
				std::list<std::string> memoryOwners;
				Core::DataEntityList::iterator it;
				for( it = dataEntityList->Begin(); it != dataEntityList->End(); dataEntityList->Next(it))
				{
					// Avoid root data element
					if ( it == dataEntityList->Begin() )
						continue;

					memoryOwners.push_back( dataEntityList->Get( it )->GetMemoryOwner( ) );
				}

				// Get plugins to unload
				std::list<std::string> pluginsUnload;
				pluginsUnload = graphicalIface->GetPluginProviderManager()->GetPluginsToUnload();

				// Check the memory owners with the plugins to unload
				bool unloadData = false;
				std::list<std::string>::iterator itOwner;
				for ( itOwner = memoryOwners.begin( ); itOwner != memoryOwners.end( ) ; itOwner++ )
				{
					std::list<std::string>::iterator itFound;
					itFound = std::find( pluginsUnload.begin( ), pluginsUnload.end( ), *itOwner );
					if ( itFound != pluginsUnload.end( ) || 
						 ( *itOwner == "Unknown" && pluginsUnload.size( ) ) )
					{
						unloadData = true;
					}
				}

				if ( unloadData )
				{
					message = \
						"The application needs to unload all data in order.\n" \
						"to apply the new configuration.\n" \
						"All unsaved data will be lost if you proceed.\n" \
						"Make sure to save all your data before\n" \
						"continuing.\n" \
						"Unload all data?";

					wxMessageDialog question(
						this, _U(message.c_str()), _U( "Exit application?" ),
						wxYES_NO | wxICON_QUESTION | wxSTAY_ON_TOP);
					if ( question.ShowModal() == wxID_NO)
					{
						loadPlugins = false;
					}
					else
					{
						dataEntityList->RemoveAll();
					}
				}
			}

			// Clean root metadata
			if ( dataEntityList->GetCount( ) > 0 )
			{
				Core::DataEntity::Pointer root = dataEntityList->Get( dataEntityList->Begin( ) );
				root->GetMetadata( )->RemoveAll( );
				Core::DataEntityMetadata::Pointer metadata = Core::DataEntityMetadata::New( root->GetId( ) );
				root->GetMetadata( )->AddTags( metadata.GetPointer( ) );
				root->GetMetadata( )->SetName( "Root" );
			}
		}

		if ( loadPlugins )
		{
			Core::Runtime::Kernel::GetApplicationRuntime( )->SetAppState( 
				Core::Runtime::APP_STATE_PROCESSING );

			// Load plugins
			graphicalIface->GetPluginProviderManager( )->LoadPlugins( "", false );

			Core::Runtime::Kernel::GetApplicationRuntime( )->SetAppState( 
				Core::Runtime::APP_STATE_IDLE );
		}
	}

	wxTheApp->ProcessPendingEvents( );

	GetMainMenu()->UpdatePerspectiveSelection( );
}

void Core::Widgets::wxMitkCoreMainWindow::CleanPluginTemporaryData( )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	Core::Runtime::PluginProviderManager::Pointer pluginProviderManager;
	pluginProviderManager = graphicalIface->GetPluginProviderManager();

	// Iterate over all providers
	std::list<std::string> paths;
	Core::Runtime::PluginProviderManager::PluginProvidersType providers;
	providers = pluginProviderManager->GetPluginProviders( );
	Core::Runtime::PluginProviderManager::PluginProvidersType::iterator it;
	for ( it = providers.begin() ; it != providers.end() ; it++ )
	{
		// For each provider
		Core::Runtime::PluginProvider::Pointer provider = *it;

		// Get temporary subdirectory
		Core::DynProcessor::Pointer processor = Core::DynProcessor::New();
		processor->UpdateProcessorDirectories( provider->GetName( ) );
		std::string tempDir = processor->GetTemporaryDirectory( );
		tempDir += Core::DynDataTransferBase::GetTemporarySubDir( );

		// Check if directory is empty
		Core::IO::Directory::Pointer dir = Core::IO::Directory::New( );
		dir->SetDirNameFullPath( tempDir );
		Core::IO::FileNameList contents = dir->GetContents( );
		if ( contents.empty( ) )
		{
			continue;
		}

		if ( std::find(paths.begin( ), paths.end( ), tempDir ) == paths.end( ) )
		{
			paths.push_back( tempDir );
		}
	}

	// Ask user
	if ( !paths.empty() )
	{
		std::string message;
		message = "There's temporary data from previous executions in the folder: " + *paths.begin( ) + "\nDo you want to delete it?";
		DialogResult question = this->AskQuestionToUserYesOrNo( message, "Temporary data" );
		if ( question == DialogResult_YES)
		{
			// Clean directories
			std::list<std::string>::iterator itPath;
			for ( itPath = paths.begin( ) ; itPath != paths.end( ) ; itPath++ )
			{
				Core::IO::Directory::Pointer dir = Core::IO::Directory::New( );
				dir->SetDirNameFullPath( *itPath );
				dir->Remove( );
			}
		}
	}

	// Clean preview data
	Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
	std::string tempDir = settings->GetProjectHomePath( ) + "/Temp";
	Core::IO::Directory::Pointer dir = Core::IO::Directory::New( );
	dir->SetDirNameFullPath( tempDir );
	Core::IO::FileNameList contents = dir->GetContents( );
	if ( !contents.empty( ) )
	{
		// Ask user
		std::string message;
		message = "There's temporary data from previous executions in the folder: " + tempDir + "\nDo you want to delete it?";
		DialogResult question = this->AskQuestionToUserYesOrNo( message, "Temporary data" );
		if ( question == DialogResult_YES)
		{
			// Clean directory
			dir->Remove( );
		}
	}

}

