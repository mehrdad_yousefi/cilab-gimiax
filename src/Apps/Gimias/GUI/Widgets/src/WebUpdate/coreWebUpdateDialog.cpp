/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreWebUpdateDialog.h"
#include "coreDirectory.h"

// Web update
#include "wx/filename.h"
#include "wx/installer.h"
#include "wx/webupdatedlg.h"
#include <wx/apptrait.h>


//! The local XML script which is loaded by default if no --xml option is specified.
#define wxWU_LOCAL_XMLSCRIPT            wxT("local.xml")

//! The name of our default log file.
#define wxWU_LOGFILENAME                wxT("webupdatelog.txt")


BEGIN_EVENT_TABLE(Core::Widgets::WebUpdateDialog, wxWebUpdateDlg)
END_EVENT_TABLE();

Core::Widgets::WebUpdateDialog::WebUpdateDialog(wxWindow* parent, int id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) 
	: wxWebUpdateDlg( parent, id, title, pos, size, style )
{
	wxString xml = wxWU_LOCAL_XMLSCRIPT;

	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();

	// start this app's logger
	wxLog::SetActiveTarget(wxTheApp->GetTraits()->CreateLogTarget());
	m_log = new wxWebUpdateLog();

	// create the default wx logger
	std::string logFileName;
	logFileName = settings->GetProjectHomePath() + "/" + wxWU_LOGFILENAME;
	wxLogDebug(wxT("CreateFileLogger - creating the logfile [") +
		wxString(logFileName) + wxT("]"));
	m_log->WriteAllMsgAlsoToFile(logFileName);
	wxLogNewSection(wxT(" LOG OF WEBUPDATER ") +
		wxWebUpdateInstaller::Get()->GetVersion() +
		wxT(" SESSION BEGAN AT ") +
		wxDateTime::Now().Format(wxT("%x %X")));        // it automatically installs itself as the new logger

	// show our platform in log files
	wxLogAdvMsg(wxT("WebUpdaterApp::OnPreInit - current platform is [")
		+ wxWebUpdatePlatform::GetThisPlatform().GetAsString() + wxT("]"));

	// this is for using wxDownloadThread. Socket must be initialized from main thread
	wxLogAdvMsg(wxT("WebUpdaterApp::OnInit - initializing sockets & handlers"));
	wxSocketBase::Initialize();

	// load the local XML webupdate script
	wxLogUsrMsg(wxT("WebUpdaterApp::OnInit - loading the local XML webupdate script ") + xml);
	wxFileName fn(xml);
	fn.MakeAbsolute( settings->GetApplicationPath() );
	if (!m_script.Load(fn.GetFullPath())) {
		wxWebUpdateInstaller::Get()->ShowErrorMsg(
			wxT("The installation of the WebUpdater component of this application\n")
			wxT("is corrupted; the file:\n\n\t") +
			fn.GetFullPath() +
			wxT("\n\nis missing (or invalid); please reinstall the program."));
		return ;
	}

	// do not proceed if in this stage we are still missing some required info
	if (!m_script.IsComplete()) {
		wxWebUpdateInstaller::Get()->ShowErrorMsg(
			wxT("The WebUpdater configuration file is corrupted; the local XML script ")
			wxT("is missing some required info. Please correct the local XML script or ")
			wxT("give these info to WebUpdater through the command line options."));
		return ;
	}

	wxASSERT(m_script.IsOk());

	Initialize( m_script );
}


Core::Widgets::WebUpdateDialog::~WebUpdateDialog()
{
	DeleteThreads();

	wxUninitializeWebUpdate();
}

void Core::Widgets::WebUpdateDialog::GetUpdateList()
{
	wxCommandEvent event;
	event.SetEventType( wxEVT_COMMAND_BUTTON_CLICKED );
	event.SetEventObject( this );
	event.SetId( wxID_GET_UPDATE_LIST );
  wxPostEvent(this, event);
}

bool Core::Widgets::WebUpdateDialog::CheckForAllUpdated( 
	wxWebUpdatePackageArray &arr, bool forcedefaultmsg /*= FALSE*/, bool showMessages /*= true*/ )
{
	bool allupdated = TRUE;
	for (int j=0; j < (int)arr.GetCount(); j++) {
		if (m_pUpdatesList->IsPackageUp2date(arr[j]) != wxWUCF_UPDATED) {
			allupdated = FALSE;
			break;      // not all packages are up to date
		}
	}

	if ( showMessages )
	{
		if (!allupdated) 
		{
			wxString usermsg = m_xmlRemote.GetUpdateAvailableMsg();
			if (!usermsg.IsEmpty())
				wxWebUpdateInstaller::Get()->ShowNotificationMsg(usermsg);
		}
	}

	// FALSE = do not exit the dialog
	return false;
}

void Core::Widgets::WebUpdateDialog::OnDownload(wxCommandEvent &)
{
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();

#ifdef WIN32
	std::string updateApp = "tpExtLibWebUpdateAppApplications_webapp.exe";
#else
	std::string updateApp = "tpExtLibWebUpdateAppApplications_webapp";
#endif

	static std::string command;
	command = settings->GetApplicationPath() + "/" + updateApp;
	std::cout << command;
	if ( !wxFileExists( command ) )
	{
		std::stringstream stream;
		stream << "Cannot find the update application " << updateApp;
		throw Core::Exceptions::Exception( "MainMenu::OnMenuCheckForUpdates", stream.str( ).c_str( ) );
	}

	// Set downloaded scripts
	std::string optScriptlist = "--scripts=";
	std::list<wxString>::iterator it;
	for ( it = m_TempScriptList.begin() ; it != m_TempScriptList.end() ; it++ )
	{
		wxFileName fn( *it );
		optScriptlist += fn.GetFullName( ) + ";";
	}

	// Set selected packages
	std::string optPackageslist = "--install=";
	wxWebUpdatePackageArray remotePackages = m_pUpdatesList->GetRemotePackages( );
	for (int i=0; i<m_pUpdatesList->GetItemCount(); i++) 
	{
		if ( m_pUpdatesList->IsChecked( i ) )
		{
			wxString name = remotePackages[ m_pUpdatesList->GetPackageIndexForItem( i ) ].GetName();
			optPackageslist += name + ";";
		}
	}

	// Set log file
	std::string logFolder = "--log-folder=";
	logFolder += settings->GetProjectHomePath( );

	// Configure execution at exit
	char* argv[6] = { (char*) command.c_str(), "--restart", (char*) logFolder.c_str(), (char*) optScriptlist.c_str(), (char*) optPackageslist.c_str(), 0 };
	Core::Runtime::Kernel::GetApplicationRuntime( )->InitApplication( 6 , argv, Core::Runtime::Graphical );
	Core::Runtime::Kernel::GetApplicationRuntime( )->RestartApplication();
	Core::Runtime::Kernel::GetApplicationRuntime( )->SetRestartWithAdministratorPrivileges( true );

	AbortDialog();
}
