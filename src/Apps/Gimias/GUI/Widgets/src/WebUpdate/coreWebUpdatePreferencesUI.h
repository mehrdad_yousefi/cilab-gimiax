// -*- C++ -*- generated by wxGlade 0.6.3 on Fri Jul 01 15:53:47 2011

#include <wx/wx.h>
#include <wx/image.h>

#ifndef COREWEBUPDATEPREFERENCESUI_H
#define COREWEBUPDATEPREFERENCESUI_H

// begin wxGlade: ::dependencies
// end wxGlade

// begin wxGlade: ::extracode
#include "wxID.h"

#define wxID_BTN_CHECK_FOR_UPDATES wxID("wxID_BTN_CHECK_FOR_UPDATES")

// end wxGlade


class coreWebUpdatePreferencesUI: public wxPanel {
public:
    // begin wxGlade: coreWebUpdatePreferencesUI::ids
    // end wxGlade

    coreWebUpdatePreferencesUI(wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

private:
    // begin wxGlade: coreWebUpdatePreferencesUI::methods
    void set_properties();
    void do_layout();
    // end wxGlade

protected:
    // begin wxGlade: coreWebUpdatePreferencesUI::attributes
    wxCheckBox* m_chkEnable;
    wxStaticText* label_1;
    wxTextCtrl* m_txtMinDays;
    wxButton* m_btnCheckForUpdates;
    // end wxGlade

    wxDECLARE_EVENT_TABLE();

public:
    virtual void OnCheckForUpdates(wxCommandEvent &event); // wxGlade: <event_handler>
}; // wxGlade: end class


#endif // COREWEBUPDATEPREFERENCESUI_H
