/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreWebUpdateDialog_H
#define _coreWebUpdateDialog_H

#include "wx/webupdatedlg.h"
#include "wx/webupdate.h"

namespace Core
{

namespace Widgets
{

/** 
User registration form
\ingroup gmWidgets
\author Xavi Planes
\date Nov 2010
*/
class WebUpdateDialog: public wxWebUpdateDlg {
public:
    // begin wxGlade: coreWebUpdateDialog::ids
    // end wxGlade

    WebUpdateDialog(wxWindow* parent, int id, const wxString& title, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=wxDEFAULT_DIALOG_STYLE);

	virtual ~WebUpdateDialog( );

	//!
	void GetUpdateList( );

private:
    wxDECLARE_EVENT_TABLE();

	//! Show message only if there are pending updates
	virtual bool CheckForAllUpdated(wxWebUpdatePackageArray &arr, bool forcedefaultmsg = FALSE, bool showMessages = true);

	/** Ask user to close GIMIAS and execute external web update with the 
	packages to install
	*/
	void OnDownload(wxCommandEvent &);

protected:
	// our local script
	wxWebUpdateLocalXMLScript m_script;

	// our log window
	wxWebUpdateLog *m_log;
};

} // namespace Core
} // namespace Widgets

#endif // COREWebUpdateDialog_H
