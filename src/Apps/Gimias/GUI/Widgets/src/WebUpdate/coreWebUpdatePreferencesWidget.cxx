/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "wx/wupdlock.h"

#include <blMitkUnicode.h>

#include "coreWebUpdatePreferencesWidget.h"
#include "coreAssert.h"
#include "coreKernel.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreReportExceptionMacros.h"
#include "coreProcessorManager.h"

using namespace Core::Widgets;

Core::Widgets::WebUpdatePreferencesWidget::WebUpdatePreferencesWidget( 
	wxWindow* parent, 
	int id /*= wxID_ANY*/, 
	const wxPoint& pos/*=wxDefaultPosition*/, 
	const wxSize& size/*=wxDefaultSize*/, 
	long style/*=0*/ ) : coreWebUpdatePreferencesUI( parent, id, pos, size, style )
{
	UpdateWidget();
}

//!
WebUpdatePreferencesWidget::~WebUpdatePreferencesWidget(void)
{

}

void Core::Widgets::WebUpdatePreferencesWidget::UpdateWidget()
{
	try
	{
		Core::Runtime::Settings::Pointer settings;
		settings = Core::Runtime::Kernel::GetApplicationSettings();

		std::string enable;
		settings->GetPluginProperty( "GIMIAS", "Enable Automatic Updates", enable );
		m_chkEnable->SetValue( enable == "false" ? false : true );

		std::string minDays = "7";
		settings->GetPluginProperty( "GIMIAS", "Min days for Automatic Updates", minDays );
		m_txtMinDays->SetValue( minDays );
		
	}
	coreCatchExceptionsReportAndNoThrowMacro(WebUpdatePreferencesWidget::UpdateWidget);
}

void Core::Widgets::WebUpdatePreferencesWidget::UpdateData()
{
	try
	{
		Core::Runtime::Settings::Pointer settings;
		settings = Core::Runtime::Kernel::GetApplicationSettings();

		std::string enable = m_chkEnable->GetValue()? "true" : "false";
		settings->SetPluginProperty( "GIMIAS", "Enable Automatic Updates", enable );

		std::string minDays = std::string(m_txtMinDays->GetValue().mb_str());
		settings->GetPluginProperty( "GIMIAS", "Min days for Automatic Updates", minDays );
	}
	coreCatchExceptionsReportAndNoThrowMacro(WebUpdatePreferencesWidget::UpdateData);
}

void Core::Widgets::WebUpdatePreferencesWidget::OnCheckForUpdates( wxCommandEvent &event )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer gIface;
	gIface = Core::Runtime::Kernel::GetGraphicalInterface();
	gIface->GetMainWindow()->CheckForUpdates();
}
