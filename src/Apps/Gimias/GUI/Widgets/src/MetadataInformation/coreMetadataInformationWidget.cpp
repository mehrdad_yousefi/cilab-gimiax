/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreMetadataInformationWidget.h"

#include "metadata24.xpm"

#include "TagItemTreeData.h"
#include "EditTagDialog.h"

using namespace Core::Widgets;

MetadataInformationWidget::MetadataInformationWidget( wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style)
: MetadataInformationWidgetUI(parent, id, pos, size, style)
{
	SetBitmap( metadata24_xpm );

	SetInputHolder( Core::DataEntityHolder::New() );
}


MetadataInformationWidget::~MetadataInformationWidget()
{
	if ( m_InputHolder.IsNotNull() )
	{
		m_InputHolder->RemoveObserver(
			this, 
			&MetadataInformationWidget::OnInputHolderChanged, 
			Core::DH_SUBJECT_MODIFIED_OR_NEW_SUBJECT);
	}
}

void MetadataInformationWidget::SetInputHolder( Core::DataEntityHolder::Pointer inputHolder )
{
	if ( m_InputHolder.IsNotNull() )
	{
		m_InputHolder->RemoveObserver(
			this, 
			&MetadataInformationWidget::OnInputHolderChanged, 
			Core::DH_SUBJECT_MODIFIED_OR_NEW_SUBJECT);
	}

	m_InputHolder = inputHolder;

	if ( m_InputHolder.IsNotNull() )
	{
		m_InputHolder->AddObserver(
			this, 
			&MetadataInformationWidget::OnInputHolderChanged, 
			Core::DH_SUBJECT_MODIFIED_OR_NEW_SUBJECT);
	}
}

void MetadataInformationWidget::UpdateTree(const wxTreeItemId& fatherItem, blTagMap::Pointer tagMapInstance)
{
	blTagMap::ListIterator it;

	for ( it = tagMapInstance->ListBegin() ; it != tagMapInstance->ListEnd() ; it++ )
	{
		blTag::Pointer tag = tagMapInstance->GetTag( it );

		std::string valueString = "";

		if ( tag->GetValue().type() != typeid( blTagMap::Pointer ) )
		{
			valueString = tag->GetValueAsString();
		}

		wxTreeItemId tagItem = m_MetadataTree->AppendItem
			(
			fatherItem,
			_U( tag->GetName() + ": " +  valueString),
			1,
			1,
			new TagItemTreeData(tag)
			);

		if ( tag->GetValue().type() == typeid( blTagMap::Pointer ) )
		{
			blTagMap::Pointer tagMapProperty;
			tag->GetValue<blTagMap::Pointer>( tagMapProperty );

			UpdateTree(tagItem, tagMapProperty);
		}
	}
}


void MetadataInformationWidget::OnInputHolderChanged( )
{
	UpdateWidget( );
}


void MetadataInformationWidget::OnTreeItemActivated(wxTreeEvent &event)
{
	wxTreeItemId treeItem = event.GetItem();
	EditTag( treeItem );
}

void MetadataInformationWidget::EditTag( wxTreeItemId treeItem )
{
	blTag::Pointer tag = GetTag( treeItem );
	if ( tag.IsNull( ) )
	{
		return;
	}

	EditTagDialog editTagDialog(this,wxID_ANY,"");
	editTagDialog.SetTag( tag );

	if( editTagDialog.ShowModal() == wxID_OK )
	{
		std::string value = editTagDialog.GetValueText();
		std::string typeName = editTagDialog.GetType();
		tag->SetValueAsString(typeName,value);
		tag->SetName( editTagDialog.GetName( ) );
		m_InputHolder->NotifyObservers();
	}

}

void MetadataInformationWidget::UpdateWidget()
{
	Core::DataEntity::Pointer dataEntity = m_InputHolder->GetSubject();

	wxWindowUpdateLocker lock( this );

	blTag::Pointer tag = GetTag( m_MetadataTree->GetSelection( ) );
	m_MetadataTree->DeleteAllItems(); 
	wxTreeItemId root = m_MetadataTree->AddRoot(wxT("Metadata"), 0, 0);

	if ( dataEntity.IsNotNull() )
	{
		blTagMap::Pointer metadata = static_cast< blSmartPointer<blTagMap> >( dataEntity->GetMetadata() );
		UpdateTree( root, metadata );

		m_MetadataTree->Expand( m_MetadataTree->GetRootItem( ) ); 
	}

	m_MetadataTree->SetScrollPos(wxVERTICAL,0); 
	if ( tag.IsNotNull( ) )
	{
		wxTreeItemId item = GetItem( tag, m_MetadataTree->GetRootItem( ) );
		if ( item.IsOk( ) )
		{
			m_MetadataTree->SelectItem( item );
			m_MetadataTree->EnsureVisible( item );
		}
	}
}


void MetadataInformationWidget::OnInit()
{
	UpdateWidget();
}

void MetadataInformationWidget::OnBtnAdd(wxCommandEvent &event)
{
	blTag::Pointer tag = blTag::New( "NewTag", std::string( "" ) );

	EditTagDialog editTagDialog(this,wxID_ANY,"");
	editTagDialog.SetTag( tag );

	if( editTagDialog.ShowModal() == wxID_OK )
	{
		// Update tag values
		tag->SetName( editTagDialog.GetName( ) );
		tag->SetValueAsString( editTagDialog.GetType( ), editTagDialog.GetValueText() );
		
		// Add tag to metadata
		Core::DataEntity::Pointer dataEntity = m_InputHolder->GetSubject();
		blTagMap::Pointer metadata = static_cast< blSmartPointer<blTagMap> >( dataEntity->GetMetadata() );

		// Check selected item
		blTag::Pointer parentTag = GetTag( m_MetadataTree->GetSelection( ) );
		if ( parentTag.IsNotNull( ) && parentTag->GetValue().type() == typeid( blTagMap::Pointer ) )
		{
			parentTag->GetValueCasted<blTagMap::Pointer>( )->AddTag( tag );
		}
		else
		{
			metadata->AddTag( tag );
		}

		UpdateWidget( );
		m_InputHolder->NotifyObservers();
	}
}

void MetadataInformationWidget::OnBtnEdit(wxCommandEvent &event)
{
	EditTag( m_MetadataTree->GetSelection( ) );
}

void MetadataInformationWidget::OnBtnRemove(wxCommandEvent &event)
{
	wxTreeItemId item = m_MetadataTree->GetSelection( );
	blTag::Pointer tag = GetTag( item );
	if ( tag.IsNull( ) )
	{
		return;
	}

	Core::DataEntity::Pointer dataEntity = m_InputHolder->GetSubject();
	blTagMap::Pointer metadata = static_cast< blSmartPointer<blTagMap> >( dataEntity->GetMetadata() );
	metadata->RemoveTag( tag );

	UpdateWidget( );
	m_InputHolder->NotifyObservers();
}

blTag::Pointer MetadataInformationWidget::GetTag( wxTreeItemId treeItem )
{
	if ( !treeItem.IsOk( ) )
	{
		return NULL;
	}

	TagItemTreeData* tagItemTreeData;
	tagItemTreeData = dynamic_cast<TagItemTreeData*>( m_MetadataTree->GetItemData(treeItem) );
	if (!tagItemTreeData)
	{
		return NULL;
	}

	blTag::Pointer tag = tagItemTreeData->GetTag();
	return tag;
}


wxTreeItemId MetadataInformationWidget::GetItem( blTag::Pointer tag, wxTreeItemId parentItem )
{
	wxTreeItemId item;
	if ( GetTag( parentItem ) == tag )
	{
		return parentItem;
	}

	wxTreeItemIdValue cookie;
	item = m_MetadataTree->GetFirstChild( parentItem, cookie );
	while ( item.IsOk( ) )
	{
		wxTreeItemId currentItem = GetItem( tag, item );
		if ( currentItem.IsOk( ) )
		{
			return currentItem;
		}
		item = m_MetadataTree->GetNextChild( parentItem, cookie );
	}

	return wxTreeItemId( );
}

