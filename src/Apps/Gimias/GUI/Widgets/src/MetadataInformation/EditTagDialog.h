/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef EditTagDialog_H
#define EditTagDialog_H

// Core
#include "gmWidgetsWin32Header.h"
#include "blTag.h"
#include "EditTagDialogUI.h"

namespace Core{
namespace Widgets{

#define wxID_EditTagDialog wxID("wxID_EditTagDialog")


/** 
A class that displays the metadata information of a data entity

\ingroup gmWidgets
\author Albert Sanchez
\date 14 Oct 2011
*/
class GMWIDGETS_EXPORT EditTagDialog : public EditTagDialogUI
{
public:
	
	//! 
	EditTagDialog(wxWindow* parent, int id = wxID_EditTagDialog, const wxString& title = "", const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

	//!
	~EditTagDialog();

	//!
	void SetTag(blTag::Pointer tag);

	//!
	std::string GetValueText();

	//!
	std::string GetType();

	//!
	std::string GetName();

protected:
	//!
    void OnChangedType(wxCommandEvent &event);
	
private:

	blTag::Pointer m_Tag;
};

} // Widgets
} // Core

#endif //EditTagDialog_H
