/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef MetadataInformationWidget_H
#define MetadataInformationWidget_H

// Core
#include "coreDataEntity.h"
#include "MetadataInformationWidgetUI.h"

namespace Core{
namespace Widgets{

#define wxID_MetadataInformationWidget wxID("wxID_MetadataInformationWidget")

class InputControl;

/** 
A class that displays the metadata information of a data entity

\ingroup gmWidgets
\author Albert Sanchez
\date 14 Oct 2011
*/
class MetadataInformationWidget : 
	public MetadataInformationWidgetUI,
	public Core::BaseWindow
{
public:
	coreDefineBaseWindowFactory( MetadataInformationWidget )

	coreClassNameMacro(Core::Widgets::DataEntityInformation);

	//! 
	MetadataInformationWidget(wxWindow* parent, int id = wxID_MetadataInformationWidget, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

	//!
	~MetadataInformationWidget();

public:
	//! 
	void OnInit( );

	//!
	void SetInputHolder( Core::DataEntityHolder::Pointer inputHolder );

	//!
	void OnInputHolderChanged( );

private:

	//!
	void OnTreeItemActivated(wxTreeEvent &event);

	//!
	void UpdateTree(const wxTreeItemId& fatherItem, blTagMap::Pointer tagMapInstance);

	//! Updates GUI
	void UpdateWidget();

	//! 
	void OnBtnAdd(wxCommandEvent &event);

	//! 
	void OnBtnEdit(wxCommandEvent &event);

	//! 
	void OnBtnRemove(wxCommandEvent &event);

	//!
	void EditTag( wxTreeItemId treeItem );

	//!
	blTag::Pointer GetTag( wxTreeItemId treeItem );

	//!
	wxTreeItemId GetItem( blTag::Pointer tag, wxTreeItemId parentItem );

private:

	//!
	Core::DataEntityHolder::Pointer m_InputHolder;
};

} // Widgets
} // Core

#endif //MetadataInformationWidget_H
