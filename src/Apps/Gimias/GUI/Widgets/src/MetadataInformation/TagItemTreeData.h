/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef TagItemItemTreeData_H
#define TagItemItemTreeData_H

#include <wx/treectrl.h>


/** 
A class that holds some information about experiment in the tree item

\ingroup gmWidgets
\author Albert Sanchez
\date 14 Oct 2011
*/

class TagItemTreeData : public wxTreeItemData
{
public:
	//!
	TagItemTreeData
	(blTag::Pointer tag) 
	: m_Tag(tag)
	{ }

	//!
	~TagItemTreeData() {}

	blTag::Pointer GetTag() { return m_Tag; }
	void SetTag(blTag::Pointer tag) { m_Tag = tag; }
	

private:
	blTag::Pointer m_Tag;
};


#endif //TagItemItemTreeData_H
