/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "EditTagDialog.h"


using namespace Core::Widgets;

EditTagDialog::EditTagDialog(wxWindow* parent, int id, const wxString& title, const wxPoint& pos, const wxSize& size, long style)
: EditTagDialogUI(parent, id,"Edit Tag")
{
	m_cmbType->Append( "bool" );
	m_cmbType->Append( "std::string" );
	m_cmbType->Append( "float" );
	m_cmbType->Append( "double" );
	m_cmbType->Append( "char" );
	m_cmbType->Append( "unsigned char" );
	m_cmbType->Append( "short" );
	m_cmbType->Append( "unsigned short" );
	m_cmbType->Append( "int" );
	m_cmbType->Append( "unsigned int" );
	m_cmbType->Append( "long" );
	m_cmbType->Append( "unsigned long" );
	m_cmbType->Append( "class blSmartPointer<class blTagMap>" );
}


EditTagDialog::~EditTagDialog()
{

}


void EditTagDialog::SetTag(blTag::Pointer tag)
{
	m_Tag = tag;
	m_cmbType->SetValue( tag->GetTypeName() );
	m_TxtName->SetValue( tag->GetName() );
	m_TextValue->SetValue( tag->GetValueAsString() );
	
    wxCommandEvent _event;
	OnChangedType( _event );
}


std::string EditTagDialog::GetValueText()
{
	return m_TextValue->GetValue().ToStdString();
}

std::string EditTagDialog::GetType()
{
	return m_cmbType->GetValue().ToStdString();
}

std::string EditTagDialog::GetName()
{
	return m_TxtName->GetValue().ToStdString();
}

void EditTagDialog::OnChangedType(wxCommandEvent &event)
{
	wxTextCtrl* text;
	if ( m_cmbType->GetValue() == "std::string" )
	{
		text = new wxTextCtrl( this, wxID_ANY, m_Tag->GetValueAsString(), wxDefaultPosition, wxSize(130,100), wxTE_MULTILINE);
	}
	else
	{
		text = new wxTextCtrl( this, wxID_ANY, m_Tag->GetValueAsString() );
	}

	GetSizer( )->Replace( m_TextValue, text, true );
	m_TextValue->Destroy( );
	m_TextValue = text;
	Layout( );
	Fit( );	
	
}


