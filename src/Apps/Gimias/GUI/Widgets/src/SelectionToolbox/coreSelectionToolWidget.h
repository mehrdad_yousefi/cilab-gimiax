/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreSelectionToolWidget_H
#define _coreSelectionToolWidget_H

#include "gmWidgetsWin32Header.h"
#include "coreBaseWindow.h"

namespace Core{
namespace Widgets{

/**
\brief Selection tool base widget
\ingroup gmWidgets
\author Xavi Planes
\date 25 April 2010
*/

class GMWIDGETS_EXPORT SelectionToolWidget: public BaseWindow {
public:
    SelectionToolWidget( );
	 
	virtual ~SelectionToolWidget( );

	//!
	void Init( );

	//! Generic function to retrieve the name of the data that will be created
	std::string GetDataName() const;
	
    /** Generic function to set the name of the data that will be created
	(landmarks, roi, ...)
	*/
	void SetDataName(const std::string& name);

	//!
	virtual void StartInteractor( ) = 0;

	//!
	virtual void StopInteraction( ) = 0;

	//!
	virtual bool IsSelectionEnabled( ) = 0;

	//! Used by DynProcessingWidget
	virtual boost::any GetData( );

	//! Get DataEntity used to put the new selected DataEntity
	virtual Core::DataEntity::Pointer GetDataEntity( );

protected:
	//!
	virtual void OnInit();

protected:

	//!
	std::string m_DataName;
};

}
}

#endif // _coreSelectionToolWidget_H
