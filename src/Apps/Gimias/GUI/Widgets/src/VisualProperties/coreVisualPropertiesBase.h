/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreVisualPropertiesBase_H
#define coreVisualPropertiesBase_H

#include "coreDataEntity.h"
#include "coreBaseWindow.h"

namespace Core{
namespace Widgets{


/** 
Base class for all visual properties widgets

\ingroup gmWidgets
\author Xavi Planes
\date Nov 2010
*/
class VisualPropertiesBase : public BaseWindow
{
	public:
	//! Check if this widget can work with this DataEntity
	virtual bool IsValidData( Core::DataEntity::Pointer dataEntity ) = 0;
	//! Set input holder used to manage the DataEntity
	virtual void SetInputHolder( Core::DataEntityHolder::Pointer inputHolder ) = 0;
};

} // Widgets
} // Core

#endif // coreVisualPropertiesBase_H
