/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreVisualProperties_H
#define coreVisualProperties_H

#include "gmWidgetsWin32Header.h"
#include "coreObject.h"
#include "coreDataHolder.h"
#include "coreRenderingTree.h"
#include <wx/panel.h>
#include "wx/scrolwin.h"
#include "coreBaseWindow.h"
#include "wxID.h"
#include "coreVisualPropertiesBase.h"

// Forward declarations
class wxButton;
class wxStaticText;
namespace mitk 
{ 
	class wxMitkRenderWindow;
}

namespace Core{
namespace Widgets{

// Forward declarations
class SurfaceMeshMaterialPropertiesWidget;
class VolumeMeshMaterialPropertiesWidget;
class ITKTransformVisualPropertiesWidget;
class VolumeImagePropertiesWidget;
class InputControl;
class DataEntityListBrowser;

#define wxID_VisualProperties wxID("wxID_VisualProperties")

/** 
A widget for changing the appearance of a data entity (a mesh or an image).

\ingroup gmWidgets
\author Juan Antonio Moya
\date 06 Feb 2008
*/
class GMWIDGETS_EXPORT VisualProperties : public wxScrolledWindow, public BaseWindow
{
public:
	//!
	coreDefineBaseWindowFactory( Core::Widgets::VisualProperties );

	coreClassNameMacro(Core::Widgets::VisualProperties);
	//!
	VisualProperties(
		wxWindow* parent, 
		wxWindowID id = wxID_VisualProperties, 
		const wxPoint& pos = wxDefaultPosition, 
		const wxSize& size = wxDefaultSize, 
		long style = wxBORDER_NONE | wxFULL_REPAINT_ON_RESIZE, 
		const wxString& name = wxT("VisualProperties"));
	//!
	virtual ~VisualProperties(void);
	
	//!
	void SetListBrowser( Core::Widgets::DataEntityListBrowser* val );

	//! Set the rendering window
	void SetMultiRenderWindow( Core::Widgets::RenderWindowBase *multiRenderWindow );

	//! Show or hide the Appearance suitecase widget
	bool Show( bool show /* = true */ );

	//!
	wxWidgetStackControl* GetStackControl() const;

	//! Overwride
	void OnInit();

private:

	//!
	void OnInputRenderingHolderChanged(void);
	//!
	void OnInputDataEntityHolderChanged(void);
	//!
	void OnUnloadDataEntity(Core::DataEntity* dataEntity);
	//! Update the rendering node of the input data if selected
	void SetRenderingTree( RenderingTree::Pointer tree );
	//!
	void SetProperties();
	
	//!
	void DoLayout();

	//!
	void OnModifiedRenderingTree( );

	//! Overwride
	Core::BaseWindow* CreateChild(const std::string &factoryName );

	//! Overwride
	void DestroyChild( BaseWindow* win );

    wxDECLARE_EVENT_TABLE();

private:
	//!
	wxStaticText* m_EmptyLabel;

	//! Widget for altering the visualization properties
	wxWidgetStackControl* m_StackControl; 
	//!
	Core::Widgets::InputControl* m_AcquireInput;
	//! Input rendering holder
	Core::DataEntityHolder::Pointer m_InputRenderingHolder;
	//! Input data entity holder
	Core::DataEntityHolder::Pointer m_InputDataEntityHolder;
	//! 
	boost::signals::connection m_RenderingTreeObserver;

	static bool resetTargetAfterResize;

	//! true if a renderWindow as been associated to the widget
	bool m_hasRenderWindowAttached;
};

} // Widgets
} // Core

#endif // coreVisualProperties_H
