/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "corePropertiesConfigurationDialog.h"

using namespace Core::Widgets;

PropertiesConfigurationDialog::PropertiesConfigurationDialog(
    wxWindow* parent, int id, const wxString& title, const wxPoint& pos, const wxSize& size, long style):
    wxDialog(parent, id, title, pos, size, wxDEFAULT_DIALOG_STYLE)
{
    m_btnOK = new wxButton(this, wxID_OK, wxT("OK"));
    m_btnCancel = new wxButton(this, wxID_CANCEL, wxT("Cancel"));

    set_properties();
    do_layout();
}


BEGIN_EVENT_TABLE(PropertiesConfigurationDialog, wxDialog)
    EVT_BUTTON(wxID_OK, PropertiesConfigurationDialog::OnOK)
    EVT_BUTTON(wxID_CANCEL, PropertiesConfigurationDialog::OnCancel)
END_EVENT_TABLE();


void PropertiesConfigurationDialog::OnOK(wxCommandEvent &event)
{
    UpdateData();
    EndModal( wxID_OK );
}


void PropertiesConfigurationDialog::OnCancel(wxCommandEvent &event)
{
    EndModal( wxID_CANCEL );
}

void PropertiesConfigurationDialog::set_properties()
{
    m_btnOK->SetDefault();
}


void PropertiesConfigurationDialog::do_layout()
{
    wxBoxSizer* GlobalSizer = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    sizer->Add(m_btnOK, 0, wxALL, 5);
    sizer->Add(m_btnCancel, 0, wxALL, 5);
    GlobalSizer->Add(sizer, 0, wxALIGN_RIGHT, 0);
    SetSizer(GlobalSizer);
    GlobalSizer->Fit(this);
    Layout();
}

blTagMap::Pointer PropertiesConfigurationDialog::GetProperties() const
{
    return m_Properties;
}

void PropertiesConfigurationDialog::SetProperties( blTagMap::Pointer val )
{
    m_Properties = val;
    UpdateWidget( );
}

void PropertiesConfigurationDialog::UpdateData()
{
    if ( m_Properties.IsNull() )
    {
        return;
    }

    wxSizerItemList children = GetSizer()->GetChildren();
    wxSizerItemList::iterator it;
    for ( it = children.begin( ) ; it != children.end( ) ; it++ )
    {
        wxSizerItem* item = *it;
        if ( item->IsSizer( ) )
        {
            wxWindow *win;
            win = item->GetSizer()->GetItem( size_t( 0 ) )->GetWindow();
            if ( win->GetId() == wxID_OK )
            {
                continue;
            }
            std::string label = std::string(win->GetLabel().mb_str());

            win = item->GetSizer()->GetItem( size_t( 1 ) )->GetWindow();
            wxTextCtrl* txtCtrl = wxDynamicCast( win, wxTextCtrl );
            if ( txtCtrl )
            {
                std::string value = std::string( txtCtrl->GetValue().c_str() );
                blTag::Pointer tag = m_Properties->GetTag( label );
                tag->SetValueAsString( tag->GetTypeName( ), value );
            }
        }
    }
}

void PropertiesConfigurationDialog::UpdateWidget()
{
    if ( m_Properties.IsNull() )
    {
        return;
    }

    GetSizer( )->Clear( true );

    m_btnOK = new wxButton(this, wxID_OK, wxT("OK"));
    m_btnCancel = new wxButton(this, wxID_CANCEL, wxT("Cancel"));

    set_properties();
    do_layout();

    blTagMap::Iterator it;
    for ( it = m_Properties->GetIteratorBegin() ; 
          it != m_Properties->GetIteratorEnd() ; it++ )
    {

        wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
        wxStaticText* label = new wxStaticText(this, wxID_ANY, it->first);
        sizer->Add(label, 1, wxALIGN_CENTER_VERTICAL, 0);

        wxTextCtrl* txtCtrl;
        txtCtrl = new wxTextCtrl(this, wxID_ANY, it->second->GetValueAsString() );
        txtCtrl->SetName( it->first );
        sizer->Add(txtCtrl, 1, 0, 0);

        GetSizer( )->Insert(0, sizer, 0, wxALL|wxEXPAND, 5);
    }

    GetSizer( )->Fit(this);
    Layout();
}
