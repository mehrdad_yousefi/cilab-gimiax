/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include <wx/wx.h>

#ifndef PropertiesConfigurationDialog_H
#define PropertiesConfigurationDialog_H

#include "gmWidgetsWin32Header.h"
#include "wxID.h"

namespace Core{
namespace Widgets{


/** 
\brief A automated GUI dialog that take a blTagMap as input.

Creates wxTextCtrl for each blTag.

To transfer data between the controls and the blTag, uses the functions:
blTag::GetValueAsString( ) and blTag::SetValueAsString( )

\ingroup gmWidgets
\author Xavi Planes
\date Mar 2011
*/
class GMWIDGETS_EXPORT PropertiesConfigurationDialog : 
    public wxDialog,
    public Core::BaseWindow {
public:

    PropertiesConfigurationDialog(wxWindow* parent, int id, const wxString& title, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=wxDEFAULT_DIALOG_STYLE);


    blTagMap::Pointer GetProperties() const;
    void SetProperties(blTagMap::Pointer val);

protected:
    void set_properties();
    void do_layout();

    virtual void UpdateData( );

    virtual void UpdateWidget( );

    virtual void OnOK(wxCommandEvent &event);
    virtual void OnCancel(wxCommandEvent &event);

protected:
    wxButton* m_btnOK;
    wxButton* m_btnCancel;

    blTagMap::Pointer m_Properties;
    wxDECLARE_EVENT_TABLE();
};

} //namespace Widgets{
} //namespace Core{


#endif // PropertiesConfigurationDialog_H
