/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreGlobalPreferencesWidget_H
#define coreGlobalPreferencesWidget_H

#include "gmWidgetsWin32Header.h"
#include "coreObject.h"
#include <wx/wizard.h>
#include "coreGlobalPreferencesUI.h"
#include "corePreferencesPage.h"

class wxWizardEvent;

namespace Core
{
namespace Widgets
{
/** 
\brief Global preferences like multi threading options

\ingroup gmWidgets
\author Xavi Planes
\date Jan 2011
*/
class GMWIDGETS_EXPORT GlobalPreferencesWidget 
	: public coreGlobalPreferencesUI
	,public Core::Widgets::PreferencesPage
{
public:
	coreDefineBaseWindowFactory( Core::Widgets::GlobalPreferencesWidget )

	//!
	GlobalPreferencesWidget(
		wxWindow* parent, 
		int id  = wxID_ANY, 
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=0);

	//!
	~GlobalPreferencesWidget(void);

protected:
	void UpdateWidget( );
	void UpdateData( );

private:
};
}
}

#endif // coreGlobalPreferencesWidget_H
