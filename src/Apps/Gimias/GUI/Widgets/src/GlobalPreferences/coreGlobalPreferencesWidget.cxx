/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "wx/wupdlock.h"

#include <blMitkUnicode.h>

#include "coreGlobalPreferencesWidget.h"
#include "coreAssert.h"
#include "coreKernel.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreReportExceptionMacros.h"
#include "coreProcessorManager.h"

using namespace Core::Widgets;

Core::Widgets::GlobalPreferencesWidget::GlobalPreferencesWidget(
	wxWindow* parent,
	int id /*= wxID_ANY*/,
	const wxPoint& pos/*=wxDefaultPosition*/,
	const wxSize& size/*=wxDefaultSize*/,
	long style/*=0*/ ) : coreGlobalPreferencesUI( parent, id, pos, size, style )
{
	UpdateWidget();
}

//!
GlobalPreferencesWidget::~GlobalPreferencesWidget(void)
{

}

void Core::Widgets::GlobalPreferencesWidget::UpdateWidget()
{
	try
	{
		bool enabled;
		enabled = Core::Runtime::Kernel::GetProcessorManager()->GetEnableMultiThreading( );
		m_chkMultiThreading->SetValue( enabled == true );

		unsigned int numThreads;
		numThreads = static_cast<unsigned int>(Core::Runtime::Kernel::GetProcessorManager()->GetExecutionQueue( )->GetNumOfThreads( ));
		m_txtMultipleThreads->SetValue( wxString::Format("%d", numThreads ) );

		Core::Runtime::Settings::Pointer settings;
		settings = Core::Runtime::Kernel::GetApplicationSettings();

		std::string orient;
		settings->GetPluginProperty( "GIMIAS", "OrientImagesWhenReading", orient );
		bool orientImages = orient == "false" ? false : true;
		m_chkOrientImages->SetValue( orientImages );

		std::string readMultipeTimeStepsStr;
		settings->GetPluginProperty( "GIMIAS", "ReadMultipeTimeSteps", readMultipeTimeStepsStr );
		bool readMultipeTimeSteps = readMultipeTimeStepsStr == "false" ? false : true;
		m_chkReadMultipeTimeSteps->SetValue( readMultipeTimeSteps );

		std::string askForModalityStr;
		settings->GetPluginProperty( "GIMIAS", "AskForModality", askForModalityStr );
		bool askForModality = askForModalityStr == "true" ? true : false;
		m_chkAskForModality->SetValue( askForModality );

		std::string scaleSmallInputDataStr;
		settings->GetPluginProperty( "GIMIAS", "ScaleSmallInputData", scaleSmallInputDataStr );
		bool scaleSmallInputData = scaleSmallInputDataStr == "true" ? true : false;
		m_chkScaleSmallInputData->SetValue( scaleSmallInputData );

		std::string loadMITKPluginStr;
		settings->GetPluginProperty( "GIMIAS", "LoadMITKPlugin", loadMITKPluginStr );
		bool loadMITKPlugin = loadMITKPluginStr == "false" ? false : true;
		m_chkLoadMITKPlugin->SetValue( loadMITKPlugin );

		std::string restartGIMIASStr;
		settings->GetPluginProperty( "GIMIAS", "RestartGIMIASForPluginConfiguration", restartGIMIASStr );
		bool restartGIMIAS = restartGIMIASStr == "true" ? true : false;
		m_chkRestartForPluginConfiguration->SetValue( restartGIMIAS );

		std::string shareLevelWindowStr;
		settings->GetPluginProperty( "GIMIAS", "ShareLevelWindow", shareLevelWindowStr );
		bool shareLevelWindow = shareLevelWindowStr == "true" ? true : false;
		m_chkShareLevelWindowRange->SetValue( shareLevelWindow );

		std::string generateNewOutputCLPDataStr;
		settings->GetPluginProperty( "GIMIAS", "GenerateNewOutputCLPData", generateNewOutputCLPDataStr );
		bool generateNewOutputCLPData = generateNewOutputCLPDataStr == "true" ? true : false;
		m_chkGenerateNewOutputCLPData->SetValue( generateNewOutputCLPData );

	}
	coreCatchExceptionsReportAndNoThrowMacro(GlobalPreferencesWidget::UpdateWidget);
}

void Core::Widgets::GlobalPreferencesWidget::UpdateData()
{
	try
	{
		long numThreads;
		m_txtMultipleThreads->GetValue().ToLong( &numThreads );
		Core::Runtime::Kernel::GetProcessorManager()->GetExecutionQueue( )->SetNumberOfThreads( numThreads );

		bool multithreading = m_chkMultiThreading->GetValue() ? true : false;
		Core::Runtime::Kernel::GetProcessorManager()->EnableMultithreading( multithreading );

		Core::Runtime::Settings::Pointer settings;
		settings = Core::Runtime::Kernel::GetApplicationSettings();

		std::string orient = m_chkOrientImages->GetValue( )? "true" : "false";
		settings->SetPluginProperty( "GIMIAS", "OrientImagesWhenReading", orient );

		std::string readMultipeTimeStepsStr = m_chkReadMultipeTimeSteps->GetValue( )? "true" : "false";
		settings->SetPluginProperty( "GIMIAS", "ReadMultipeTimeSteps", readMultipeTimeStepsStr );

		std::string askForModalityStr = m_chkAskForModality->GetValue( )? "true" : "false";
		settings->SetPluginProperty( "GIMIAS", "AskForModality", askForModalityStr );

		std::string scaleSmallInputDataStr = m_chkScaleSmallInputData->GetValue( )? "true" : "false";
		settings->SetPluginProperty( "GIMIAS", "ScaleSmallInputData", scaleSmallInputDataStr );

		std::string loadMITKPluginStr = m_chkLoadMITKPlugin->GetValue( )? "true" : "false";
		settings->SetPluginProperty( "GIMIAS", "LoadMITKPlugin", loadMITKPluginStr );

		std::string restartGIMIASStr = m_chkRestartForPluginConfiguration->GetValue( )? "true" : "false";
		settings->SetPluginProperty( "GIMIAS", "RestartGIMIASForPluginConfiguration", restartGIMIASStr );

		std::string shareLevelWindowStr = m_chkShareLevelWindowRange->GetValue( )? "true" : "false";
		settings->SetPluginProperty( "GIMIAS", "ShareLevelWindow", shareLevelWindowStr );

		std::string generateNewOutputCLPDataStr = m_chkGenerateNewOutputCLPData->GetValue( )? "true" : "false";
		settings->SetPluginProperty( "GIMIAS", "GenerateNewOutputCLPData", generateNewOutputCLPDataStr );
	}
	coreCatchExceptionsReportAndNoThrowMacro(GlobalPreferencesWidget::UpdateData);
}
