// Copyright 2009 Pompeu Fabra University (Computational Imaging Laboratory), Barcelona, Spain. Web: www.cilab.upf.edu.
// This software is distributed WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#include "coreProcessingWidget.h"
#include "coreSelectionToolboxWidget.h"
#include "corePluginTab.h"
#include "coreProcessorManager.h"

Core::Widgets::ProcessingWidget::ProcessingWidget()
{
}

void Core::Widgets::ProcessingWidget::UpdateData()
{
}

void Core::Widgets::ProcessingWidget::UpdateWidget()
{
}

Core::Widgets::ProcessingWidget::~ProcessingWidget()
{

}

void Core::Widgets::ProcessingWidget::UpdateProcessor( bool multithreading )
{
	try
	{
		GetProcessor()->SetMultithreading( multithreading );
		GetProcessor()->SetTimeStep( GetTimeStep() );
		if ( GetProcessor()->IsExecutedLocally() )
		{
			Core::Runtime::Kernel::GetProcessorManager()->Execute( GetProcessor(), "Default" );
		}
		else
		{
			Core::Runtime::Kernel::GetProcessorManager()->Execute( GetProcessor(), "Remote" );
		}
	}
	coreCatchExceptionsReportAndNoThrowMacro( "ProcessingWidget::UpdateProcessor" );
}

void Core::Widgets::ProcessingWidget::SetInfoUserHelperWidget( std::string text )
{
	if ( GetHelperWidget() == NULL )
	{
		return;
	}

	GetHelperWidget()->SetInfo( 
		Core::Widgets::HELPER_INFO_ONLY_TEXT, text );
}

void Core::Widgets::ProcessingWidget::SetSelectionTool(
	wxWindowID id )
{
	if ( GetPluginTab() == NULL )
	{
		return;
	}

	SelectionToolboxWidget* selectionToolboxWidget;
	GetPluginTab()->GetWidget( wxID_SelectionToolboxWidget, selectionToolboxWidget );
	selectionToolboxWidget->SetToolByID( id );
	//GetPluginTab()->ShowWindow( wxID_SelectionToolboxWidget, true );
}

void Core::Widgets::ProcessingWidget::SetSelectionTool( const std::string &caption )
{
	if ( GetPluginTab() == NULL )
	{
		return;
	}

	SelectionToolboxWidget* selectionToolboxWidget;
	GetPluginTab()->GetWidget( wxID_SelectionToolboxWidget, selectionToolboxWidget );
	if ( selectionToolboxWidget )
	{
		selectionToolboxWidget->SetToolByName( caption );
	}
}

