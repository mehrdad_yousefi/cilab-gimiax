/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include "wx/wxprec.h"

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include <wx/wupdlock.h>

// Core
#include "coreBaseToolboxWidget.h"
#include "coreReportExceptionMacros.h"
#include "coreBaseWindowFactories.h"
#include "coreWxMitkGraphicalInterface.h"
#include "corePluginTab.h"
#include "coreBaseWindowFactorySearch.h"
#include "coreProcessorInputWidget.h"

#include "SearchIcon.xpm"
#include "History.xpm"
#include "red-cross.xpm"

#include "wxAutoCompletionTextCtrl.h"

#include "blTextUtils.h"

#define wxID_SEARCH_BTN wxID( "wxID_SEARCH_BTN" )
#define wxID_RED_CROSS_BTN wxID( "wxID_RED_CROSS_BTN" )
#define wxID_HISTORY_BTN wxID( "wxID_HISTORY_BTN" )
#define wxID_CMB_SEARCH wxID( "wxID_CMB_SEARCH" )


using namespace Core::Widgets;

BEGIN_EVENT_TABLE( BaseToolboxWidget, wxScrolledWindow )
  EVT_BUTTON( wxID_SEARCH_BTN, BaseToolboxWidget::OnSearch )
  EVT_BUTTON( wxID_RED_CROSS_BTN, BaseToolboxWidget::OnRedCross )
  EVT_BUTTON( wxID_HISTORY_BTN, BaseToolboxWidget::OnHistory )
  EVT_TEXT( wxID_CMB_SEARCH, BaseToolboxWidget::OnCmbSearchText )
END_EVENT_TABLE( )

//!
BaseToolboxWidget::BaseToolboxWidget(wxWindow* parent,
	wxWindowID id,
	const wxPoint& pos,
	const wxSize& size,
	long style,
	const wxString& name)
: wxScrolledWindow(parent, id, pos, size, style, name)
{
	//SetBitmap( processingtools_xpm );

	m_EmptyLabel = new wxStaticText(
		this,
		wxID_ANY,
		wxT("\n\nThere isn't any selected tool"),
		wxDefaultPosition,
		wxDefaultSize );

	m_bmpButtonSearch = new wxBitmapButton( this, wxID_SEARCH_BTN, wxBitmap( SearchIcon_xpm ) );
	m_bmpButtonRedCross = new wxBitmapButton( this, wxID_RED_CROSS_BTN, wxBitmap( red_cross_xpm ) );
	m_bmpButtonRedCross->Hide( );
	m_bmpButtonHistory = new wxBitmapButton( this, wxID_HISTORY_BTN, wxBitmap( History_xpm ) );
	m_txtSearch = new wxAutoCompletionTextCtrl( this, wxID_CMB_SEARCH );
	m_HistoryMenu = new wxMenu( );
	m_popupMenu =  new wxMenu( );
	set_properties( );
	do_layout( );

	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );
	baseWindowFactory->GetFactoriesHolder()->AddObserver(
		this,
		&BaseToolboxWidget::UpdateRegisteredWindows );

	m_SaveHistory = false;
}

void BaseToolboxWidget::set_properties()
{
	SetScrollRate(10, 10);

	m_txtSearch->SetPopupMaxHeight( 100 );
	m_txtSearch->SetMinSize(wxSize(-1, 20));
	m_txtSearch->SetFont(wxFont(9, wxDEFAULT, wxNORMAL, wxBOLD, 0, wxT("")));
	m_txtSearch->SetWindowStyle(wxBORDER_SUNKEN);
}

void BaseToolboxWidget::do_layout()
{
	// layout them
	wxBoxSizer* vlayout = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* horizSizer = new wxBoxSizer( wxHORIZONTAL );
	horizSizer->Add( m_txtSearch, 1, wxEXPAND );
	horizSizer->Add( m_bmpButtonSearch, 0 );
	horizSizer->Add( m_bmpButtonRedCross, 0 );
	horizSizer->Add( m_bmpButtonHistory, 0 );
	vlayout->Insert(0,horizSizer, 0, wxALL|wxEXPAND, 5);

	vlayout->Add(m_EmptyLabel, 0, wxEXPAND | wxALL, 4);
	SetSizer(vlayout);
	vlayout->Fit(this);

}

//!
BaseToolboxWidget::~BaseToolboxWidget(void)
{
}

void Core::Widgets::BaseToolboxWidget::SetToolByName( const std::string &caption )
{
	if ( GetToolWindow( ) && GetToolWindow( )->GetName( ) == caption )
	{
		return;
	}

	wxWindowUpdateLocker noUpdates( this );

	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	Clean( );

	// Create widget
	std::list<std::string> windowsList;
	BaseWindowFactorySearch::Pointer search = BaseWindowFactorySearch::New( );
	search->SetType( GetWidgetState() );
	search->SetCaption( caption );
	search->Update( );
	windowsList = search->GetFactoriesNames();
	if ( windowsList.empty() )
	{
		return;
	}

	SetToolByFactoryName( windowsList.front() );

	InitTool( );
}

void Core::Widgets::BaseToolboxWidget::SetToolByID( wxWindowID id )
{
	if ( GetToolWindow( ) && GetToolWindow( )->GetId( ) == id )
	{
		return;
	}

	wxWindowUpdateLocker noUpdates( this );

	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	Clean( );

	// Create widget
	BaseWindowFactorySearch::Pointer search = BaseWindowFactorySearch::New( );
	search->SetType( GetWidgetState() );
	search->SetId( id );
	search->Update( );
	std::list<std::string> windowsList = search->GetFactoriesNames();

	if ( windowsList.empty() )
	{
		return;
	}

	SetToolByFactoryName( windowsList.front() );

	InitTool( );
}

Core::BaseProcessor::Pointer Core::Widgets::BaseToolboxWidget::GetProcessor()
{
	return NULL;
}

void Core::Widgets::BaseToolboxWidget::Clean()
{
	if ( GetToolWindow( ) == NULL )
	{
		return;
	}

	wxWindowUpdateLocker noUpdates(this);

	// Destroy old window
	GetSizer()->Detach( GetToolWindow( ) );
	GetToolWindow( )->Destroy();
	SetToolWindow( NULL );

	m_EmptyLabel->Show();
	// Avoid sending the event
	m_txtSearch->HidePopup();
	m_txtSearch->ChangeValue( "" );
}

bool Core::Widgets::BaseToolboxWidget::Enable( bool enable /*= true */ )
{
	if ( enable == false )
	{
		Clean( );
	}
	else
	{
		UpdateRegisteredWindows( );
	}

	return wxScrolledWindow::Enable( enable );
}

void Core::Widgets::BaseToolboxWidget::OnInit()
{
	ReadSettings( );
}

void Core::Widgets::BaseToolboxWidget::InitTool()
{
	if ( GetToolWindow( ) == NULL )
	{
		return;
	}

	// Enable the window when everything is initialized
	// Is not possible to call Enable because the widget is not initialized
	GetToolWindow( )->Enable( false );

	BaseWindow* baseWindow = dynamic_cast<BaseWindow*> ( GetToolWindow( ) );
	if ( baseWindow == NULL )
	{
		throw Core::Exceptions::Exception(
			"BaseToolboxWidget::InitTool",
			"input widget is not a BaseWindow" );
	}

	// Init processor observers
	baseWindow->InitProcessorObservers( true );

	// OnInit can change the processor observers
	GetPluginTab( )->InitBaseWindow( baseWindow );

	// Enable
	GetToolWindow( )->Enable();
	for (int i = 0; i < baseWindow->GetNumInputWidget() ; i++)
	{
		// the previous call to Enable() does not enable the widgets automatically,
		// so do it separately here
		baseWindow->GetInputWidget(i)->Enable();
	}


	m_EmptyLabel->Hide();
	// Don't send the event
	m_txtSearch->ChangeValue( GetToolWindow()->GetName() );

	// layout them
	GetSizer()->Add( GetToolWindow( ), 1, wxEXPAND | wxALL, 4);
	GetSizer()->Show( GetToolWindow( ), true, true );

	// Remove intem from history
	wxWindowID oldId = m_HistoryMenu->FindItem( GetToolWindow()->GetName() );
	if ( oldId != wxNOT_FOUND )
	{
		 m_HistoryMenu->Remove( oldId );
	}

	// Add item to history
	wxWindowID id = wxNewId();
	m_HistoryMenu->Insert(0, id, GetToolWindow()->GetName() );
	Connect(
		id,
		wxEVT_COMMAND_MENU_SELECTED,
		wxCommandEventHandler(BaseToolboxWidget::OnHistoryMenu )
		);
	// Remove last item
	size_t count = m_HistoryMenu->GetMenuItemCount( );
	if ( count > 5 )
	{
		wxMenuItem* item = m_HistoryMenu->FindItemByPosition( int( count -1 ) );
		if ( item )
		{
			m_HistoryMenu->Destroy( item );
		}
	}

	WriteSettings( );

	m_bmpButtonRedCross->Hide( );
	m_bmpButtonSearch->Show( );
	m_txtSearch->HidePopup();

	// Send a resize event to update the size of m_EmbededWindow
	wxSizeEvent resEvent( GetBestSize(), GetId() );
	resEvent.SetEventObject( this );
	this->GetEventHandler()->ProcessEvent(resEvent);
}


void Core::Widgets::BaseToolboxWidget::SetToolByFactoryName( const std::string &factoryName )
{
	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	Core::BaseWindow* baseWindow;
	baseWindow = baseWindowFactory->CreateBaseWindow( factoryName, this );

	wxWindow* window = dynamic_cast<wxWindow*> (baseWindow);
	if ( !window )
	{
		throw Core::Exceptions::Exception(
			"BaseToolboxWidget::SetToolByFactoryName",
			"input widget is not a wxWindow" );
	}

	SetToolWindow( window );
}

void Core::Widgets::BaseToolboxWidget::RecursiveMenuCleanUp(wxMenu* menu)
{
	if (menu==NULL)
		return;

	wxMenuItemList items = menu->GetMenuItems();
	for (wxMenuItemList::Node *node = items.GetFirst(); node; node = node->GetNext())
	{
		wxMenuItem *mi = (wxMenuItem*)node->GetData();
		RecursiveMenuCleanUp(mi->GetSubMenu());
		menu->Remove(mi->GetId());
		delete mi; //menu item is removed from menu, so we have to delete it ourselves
	}
}

void Core::Widgets::BaseToolboxWidget::RecursiveMenuCopy(wxMenu* source, wxMenu* destination)
{
	wxMenuItemList items = source->GetMenuItems();
	for (wxMenuItemList::Node *node = items.GetFirst(); node; node = node->GetNext())
	{
		wxMenuItem *mi = (wxMenuItem*)node->GetData();
		if (!mi->GetSubMenu())
		{
			wxMenuItem *miCopy = new wxMenuItem(destination, mi->GetId(), mi->GetLabel());
			destination->Append(miCopy);
		}
		else
		{
			wxMenu* m_SubMenu = new wxMenu();
			RecursiveMenuCopy( mi->GetSubMenu(), m_SubMenu);
			destination->AppendSubMenu(m_SubMenu,  mi->GetLabel());
		}


	}
}

void Core::Widgets::BaseToolboxWidget::OnSearch( wxCommandEvent &event )
{
	wxFrame* mainFrame;
	mainFrame = dynamic_cast<wxFrame*> ( Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow( ) );
	wxMenuBar* menuBar;
	menuBar = mainFrame->GetMenuBar();

	std::string itemName = GetNameMenuItem();
	int menuId = menuBar->FindMenu( itemName );
	if ( menuId == wxNOT_FOUND )
	{
		return;
	}

	wxPoint position = m_bmpButtonSearch->GetPosition();
	position.y += m_bmpButtonSearch->GetSize().GetHeight();
	wxMenu* targetMenu = menuBar->GetMenu( menuId );

	// remove current popup menu items
	RecursiveMenuCleanUp(m_popupMenu);

	// add brand new ones from the tools/selection menu
	RecursiveMenuCopy(targetMenu, m_popupMenu);

	PopupMenu( m_popupMenu, position );
}

void Core::Widgets::BaseToolboxWidget::OnHistory( wxCommandEvent &event )
{
	if ( m_HistoryMenu->GetMenuItemCount() == 0 )
	{
		return;
	}

	wxPoint position = m_bmpButtonHistory->GetPosition();
	position.y += m_bmpButtonHistory->GetSize().GetHeight();
	PopupMenu( m_HistoryMenu, position );
}

void Core::Widgets::BaseToolboxWidget::OnHistoryMenu( wxCommandEvent& event )
{
	wxMenuItem* item = m_HistoryMenu->FindItem( event.GetId() );
	SetToolByName( item->GetItemLabel().ToStdString() );
}

void Core::Widgets::BaseToolboxWidget::OnRedCross( wxCommandEvent &event )
{
	StopSearch( );

	if ( GetToolWindow( ) )
	{
		m_txtSearch->HidePopup();
		m_txtSearch->ChangeValue( GetToolWindow( )->GetName() );
	}
}

void Core::Widgets::BaseToolboxWidget::OnCmbSearchText( wxCommandEvent &event )
{
	if ( !m_txtSearch->IsPopupShown( ) && !m_bmpButtonSearch->IsShown() )
	{
		StopSearch();
	}
	else if ( m_txtSearch->IsPopupShown( ) && m_bmpButtonSearch->IsShown() )
	{
		StartSearch();
	}

	// The user selected an entry that is not empty
	if ( !m_txtSearch->IsPopupShown( ) &&
		 !m_txtSearch->GetValue().IsEmpty() )
	{
		SetToolByName( m_txtSearch->GetValue().ToStdString() );
	}

}

void Core::Widgets::BaseToolboxWidget::StopSearch()
{
	wxWindowUpdateLocker lock( this );

	if ( m_txtSearch->IsPopupShown() )
	{
		m_txtSearch->HidePopup();
	}
	m_bmpButtonRedCross->Hide( );
	m_bmpButtonSearch->Show( );
	GetSizer()->Layout();
}

void Core::Widgets::BaseToolboxWidget::StartSearch()
{
	wxWindowUpdateLocker lock( this );

	m_bmpButtonSearch->Hide( );
	m_bmpButtonRedCross->Show( );
	GetSizer()->Layout();
}

void Core::Widgets::BaseToolboxWidget::UpdateRegisteredWindows()
{
	// Update list of similar captions
	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	BaseWindowFactorySearch::Pointer search = BaseWindowFactorySearch::New( );
	search->SetType( GetWidgetState() );
	search->Update( );
	std::list<std::string> windowsList = search->GetFactoriesNames();

	// Set all names of windows that can be selected
	m_txtSearch->ClearStrings( );
	std::list<std::string>::iterator it;
	for ( it = windowsList.begin( ) ; it != windowsList.end() ; it++ )
	{
		Core::WindowConfig config;
		if ( ! baseWindowFactory->GetWindowConfig( *it, config ) )
		{
			continue;
		}

		m_txtSearch->AppendString( config.GetCaption() );
	}

	// Check if selected item is not registered
	if ( GetToolWindow( ) )
	{
		BaseWindowFactorySearch::Pointer search = BaseWindowFactorySearch::New( );
		search->SetCaption( GetToolWindow( )->GetName( ).ToStdString() );
		search->Update( );
		windowsList = search->GetFactoriesNames();

		if ( windowsList.empty() )
		{
			Clean( );
		}
		else
		{
			// Refresh it
			std::string name = std::string( GetToolWindow( )->GetName( ).c_str() );
			Clean( );
			SetToolByName( name );
		}
	}

}

void Core::Widgets::BaseToolboxWidget::ReadSettings( )
{
	// Retrieve history to settings
	if ( m_SaveHistory )
	{
		Core::Runtime::Settings::Pointer settings;
		settings = Core::Runtime::Kernel::GetApplicationSettings();
		blTagMap::Pointer properties = settings->GetPluginProperties( "GIMIAS" );
		blTag::Pointer tagHistory = properties->FindTagByName( "toolbox history" );
		if ( tagHistory.IsNotNull( ) )
		{
			std::string historyString = tagHistory->GetValueCasted<std::string>();
			std::list<std::string> words;
			blTextUtils::ParseLine( historyString, ';', words );

			std::list<std::string>::iterator it;
			for ( it = words.begin() ; it != words.end( ) ; it++ )
			{
				wxWindowID id = wxNewId();
				m_HistoryMenu->Append(id, *it );
				Connect(
					id,
					wxEVT_COMMAND_MENU_SELECTED,
					wxCommandEventHandler(BaseToolboxWidget::OnHistoryMenu )
					);
			}
		}
	}
}

void Core::Widgets::BaseToolboxWidget::WriteSettings( )
{
		// Add history to settings
	if ( m_SaveHistory )
	{
		Core::Runtime::Settings::Pointer settings;
		settings = Core::Runtime::Kernel::GetApplicationSettings();
		blTagMap::Pointer properties = settings->GetPluginProperties( "GIMIAS" );
		std::stringstream sstream;
		for ( int i = 0 ; i < m_HistoryMenu->GetMenuItemCount( ) ; i++ )
		{
			wxMenuItem* item = m_HistoryMenu->FindItemByPosition( i );
			sstream << item->GetLabel(  ).c_str( ) << ";";
		}
		properties->AddTag( "toolbox history", sstream.str( ) );
		settings->SetPluginProperties( "GIMIAS", properties );
	}
}
