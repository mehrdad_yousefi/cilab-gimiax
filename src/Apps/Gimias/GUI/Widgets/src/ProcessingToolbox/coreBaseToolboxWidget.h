/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreBaseToolboxWidget_H
#define _coreBaseToolboxWidget_H

#include "coreBaseWindow.h"

class wxAutoCompletionTextCtrl;

namespace Core{
namespace Widgets{


/** 
\brief class that display the parameters for the processors of the tools menu
\ingroup gmWidgets
\author Chiara Riccobene
\date 05 Nov 2009
*/
class GMWIDGETS_EXPORT BaseToolboxWidget 
	: public wxScrolledWindow, public BaseWindow
{
public:
	coreClassNameMacro(Core::Widgets::BaseToolboxWidget);
	
	BaseToolboxWidget(wxWindow* parent, 
						wxWindowID id = wxID_ANY, 
						const wxPoint& pos = wxDefaultPosition, 
						const wxSize& size = wxDefaultSize, 
						long style = wxBORDER_NONE | wxFULL_REPAINT_ON_RESIZE, 
						const wxString& name = wxPanelNameStr);

	//!
	void SetToolByName( const std::string &caption );

	//!
	void OnInit();

	//!
	void SetToolByID( wxWindowID id );

	//!
	virtual wxWindow* GetToolWindow() = 0;

	//!
	virtual void SetToolWindow( wxWindow* win ) = 0;

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

	//!
	bool Enable(bool enable = true );

private:

	//!
	void RecursiveMenuCleanUp(wxMenu* menu);
	//!
	void RecursiveMenuCopy(wxMenu* source, wxMenu* destination);

protected:

	virtual ~BaseToolboxWidget(void);	

	//!
	void set_properties();
	void do_layout();

	//!
	void SetToolByFactoryName( const std::string &factoryName );

	//!
	void Clean( );

	//!
	void OnSearch( wxCommandEvent &event );

	//!
	void OnRedCross( wxCommandEvent &event );

	//!
	void OnHistory( wxCommandEvent &event );

	//!
	void OnCmbSearchText( wxCommandEvent &event );

	//! A history menu item has been clicked
	void OnHistoryMenu( wxCommandEvent& event );

	//!
	virtual std::string GetNameMenuItem( ) = 0;

	//!
	virtual WIDGET_TYPE GetWidgetState( ) = 0;

	//!
	void InitTool( );

	//!
	void StartSearch( );

	//!
	void StopSearch( );

	//!
	void UpdateRegisteredWindows( );

	//! Read settings, like history
	void ReadSettings( );

	//! Write settings, like history
	void WriteSettings( );

    wxDECLARE_EVENT_TABLE();

protected:
	//! Save history to GIMIAS settings
	bool m_SaveHistory;

private:
	//!
	wxStaticText* m_EmptyLabel;
	//!
	wxAutoCompletionTextCtrl* m_txtSearch;
	//!
	wxBitmapButton* m_bmpButtonSearch;
	//!
	wxBitmapButton* m_bmpButtonRedCross;
	//!
	wxBitmapButton* m_bmpButtonHistory;
	//!
	wxMenu* m_HistoryMenu;
	//!
	wxMenu* m_popupMenu;
};

} // Widgets
} // Core

#endif // coreBaseToolboxWidget_H
