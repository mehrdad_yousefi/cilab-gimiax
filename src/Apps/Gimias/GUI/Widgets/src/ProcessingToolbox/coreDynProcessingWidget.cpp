/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDynProcessingWidget.h"
#include "coreProcessorInputWidget.h"
#include "coreSelectionToolWidget.h"

#include "dynWxAGUIBuilder.h"
#include "dynModuleHelper.h"

#include <wx/tglbtn.h>
#include "itkImageRegion.h"
#include "corePluginProviderManager.h"

#include "blTextUtils.h"

#include "coreProcessorWidgetsBuilder.h"

#define wxID_BTN_REGION wxID("wxID_BTN_REGION")
#define wxID_BTN_POINT wxID("wxID_BTN_POINT")
#define wxID_BTN_LOCATION wxID("wxID_BTN_LOCATION")

BEGIN_EVENT_TABLE(Core::Widgets::DynProcessingWidget, wxPanel)
    EVT_BUTTON(wxID_APPLY, Core::Widgets::DynProcessingWidget::OnApply)
	EVT_BUTTON(wxID_SAVE_AS_SCRIPT, Core::Widgets::DynProcessingWidget::OnSaveScript)
	EVT_TOGGLEBUTTON(wxID_BTN_REGION, Core::Widgets::DynProcessingWidget::OnBtnRegion)
	EVT_TOGGLEBUTTON(wxID_BTN_POINT, Core::Widgets::DynProcessingWidget::OnBtnPoint)
	EVT_BUTTON(wxID_BTN_LOCATION, Core::Widgets::DynProcessingWidget::OnBtnLocation)
	EVT_MENU	(wxID_ANY, Core::Widgets::DynProcessingWidget::OnMenu)	
END_EVENT_TABLE();

Core::Widgets::DynProcessingWidget::DynProcessingWidget( 
	wxWindow* parent, int id, const wxPoint& pos/*=wxDefaultPosition*/, 
	const wxSize& size/*=wxDefaultSize*/, long style/*=0*/ )
	: wxPanel(parent, id, pos, size, style)
{
	m_Processor = DynProcessor::New();

	wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
	SetSizer(sizer);
	GetSizer( )->Fit(this);

	m_btnLocation = new wxButton( this, wxID_BTN_LOCATION, "Location" );
	wxBoxSizer* sizer_9 = new wxBoxSizer(wxHORIZONTAL);
	sizer_9->Add (m_btnLocation, 1, wxEXPAND|wxALL, 5);
	GetSizer()->Add (sizer_9, 0, wxEXPAND|wxALL, 5);

	m_DynPanel = NULL;
}

void Core::Widgets::DynProcessingWidget::UpdateData()
{
	m_DynPanel->GetUpdater()->UpdateData();
}

void Core::Widgets::DynProcessingWidget::UpdateWidget()
{
	m_DynPanel->GetUpdater()->UpdateWidget();
}

Core::Widgets::DynProcessingWidget::~DynProcessingWidget()
{

}

Core::BaseProcessor::Pointer Core::Widgets::DynProcessingWidget::GetProcessor()
{
	return m_Processor.GetPointer();
}

void Core::Widgets::DynProcessingWidget::SetModule( ModuleDescription* module )
{
	// If module is already set, add a new Execution data
	if ( GetModule() == NULL )
	{
		BaseWindow::SetModule( module );

		m_Processor = DynProcessor::New();

		UpdateDefaultPluginProvider();
	}

	m_Processor->SetModule( module );
}

void Core::Widgets::DynProcessingWidget::OnApply( wxCommandEvent &event )
{
	UpdateProcessor( );
}

void Core::Widgets::DynProcessingWidget::UpdateProcessor( bool multithreading /*= true*/ )
{
	try
	{
		m_Processor->UpdateProcessorDirectories( m_ActivePluginProvider );

		wxWindow* win = FindWindowById( wxID_RUN_AS_COMMAND_LINE, this );
		wxCheckBox* checkBox = wxDynamicCast( win, wxCheckBox );
		if ( checkBox && checkBox->GetValue() )
		{
			m_Processor->GetModuleExecution( )->SetForceExecutionMode( "CommandLineModule" );
		}
		else if ( m_Processor->GetModuleExecution( )->GetSaveScript( ) )
		{
			m_Processor->GetModuleExecution( )->SetForceExecutionMode( "CommandLineModule" );
		}
		else
		{
			m_Processor->GetModuleExecution( )->SetForceExecutionMode( "UnknownModule" );
		}


		// Clean input values used in previous execution because 
		// new values need to be created
		for ( int i = 0 ; i < m_Processor->GetNumberOfInputs() ; i++ )
		{
			ModuleParameter* param;
			param = dynModuleHelper::GetInput( m_Processor->GetModule(), i );
			param->SetDefault( "" );
		}


		// Clean output values to set them as new output filenames
		for ( int i = 0 ; i < m_Processor->GetNumberOfOutputs() ; i++ )
		{
			ModuleParameter* param;
			param = dynModuleHelper::GetOutput( m_Processor->GetModule(), i );

			// Outputs have the check box set to true or false indicating if are active or not
			// If they are set to true -> the value will be replaced by the output file name
			// When execution finishes
			bool isOptional = m_Processor->GetOutputPort( i )->GetOptional();
			if ( isOptional && param->GetDefault() == "false" )
			{
				// Do nothing
			}
			else
			{
				param->SetDefault( "" );
			}
		}

		// Update processor
		ProcessingWidget::UpdateProcessor( multithreading );
	}
	coreCatchExceptionsReportAndNoThrowMacro( "DynProcessingWidget::UpdateProcessor" );
}

void Core::Widgets::DynProcessingWidget::OnInit()
{
	CreateWidget();

	UpdateWidget( );

	// Don't select ouptut by default
	for ( int i = 0 ; i < m_Processor->GetNumberOfOutputs() ; i++ )
	{
		GetProcessorOutputObserver( i )->SelectDataEntity( false );
		GetProcessorOutputObserver( i )->SetHideInput( false );
	}
}

void Core::Widgets::DynProcessingWidget::OnSaveScript( wxCommandEvent &event )
{
	if ( m_Processor->GetModuleExecution( ).IsNull() )
	{
		return;
	}

	try
	{
		m_DynPanel->GetUpdater( )->UpdateData( );
		m_Processor->GetModuleExecution( )->SetSaveScript( true );
		UpdateProcessor( false );

#ifdef _WIN32
		std::string message = "Script has been saved in the folder: " + 
			m_Processor->GetModuleExecution( )->GetUseCaseDirectory( ) + 
			"\n Do you want to open the folder?" ;
		wxMessageDialog question(this, _U(message.c_str()),_U("Script saved"),
			wxYES_NO | wxICON_QUESTION | wxSTAY_ON_TOP);
		if (question.ShowModal() == wxID_YES )
		{
			std::string call = "start explorer \"" + 
				itksys::SystemTools::ConvertToWindowsOutputPath( 
				m_Processor->GetModuleExecution( )->GetUseCaseDirectory( ).c_str( ) ) + "\"";
			system( call.c_str( ) );
		}

#endif //_WIN32
	}
	coreCatchExceptionsReportAndNoThrowMacro( "OnSaveScript::UpdateProcessor" );

	m_Processor->GetModuleExecution( )->SetForceExecutionMode( "UnknownModule" );
	m_Processor->GetModuleExecution( )->SetSaveScript( false );
}

void Core::Widgets::DynProcessingWidget::AddInteractionButtons()
{
	dynWxGUIUpdater::ControlUpdaterMapType updaters;
	updaters = m_DynPanel->GetUpdater()->GetControlUpdaters( );

	dynWxGUIUpdater::ControlUpdaterMapType::iterator it;
	for ( it = updaters.begin() ; it != updaters.end() ; it++ )
	{
		ModuleParameter* param;
		param = dynModuleHelper::FindParameter( m_Processor->GetModule( ), "", 0, it->first );
		if ( !param )
		{
			continue;
		}
		
		if ( param->GetTag() == "region" || param->GetTag() == "point" )
		{
			wxWindow* win = wxWindow::FindWindowByName( it->first, m_DynPanel );
			if ( !win )
			{
				return;
			}

			wxWindowID id;
			if ( param->GetTag() == "region" )
			{
				id = wxID_BTN_REGION;
			}
			else if ( param->GetTag() == "point" )
			{
				id = wxID_BTN_POINT;
			}

			wxSizer* sizer = win->GetContainingSizer( );
			wxToggleButton* button = new wxToggleButton( win->GetParent( ), id, "S", wxDefaultPosition, wxSize( 21, 21 ) );
			sizer->Add( button, wxSizerFlags( ).Proportion( 0 ) );
		}
	}
}

void Core::Widgets::DynProcessingWidget::OnBtnRegion( wxCommandEvent &event )
{
	if ( GetPluginTab() == NULL )
	{
		return;
	}

	if ( event.GetInt( ) )
	{
		StartInteraction( event.GetEventObject(), "Bounding Box" );
	}
	else
	{
		wxWindow* applyWin = m_DynPanel->FindWindowById( wxID_APPLY );
		applyWin->Enable( true );

		SelectionToolWidget* bboxWidget = GetSelectionToolWidget<SelectionToolWidget>( "Bounding Box" );
		itk::ImageRegion<3> region;
		if ( Core::CastAnyProcessingData( bboxWidget->GetData( ), region ) )
		{
			std::ostringstream strRegion;
			strRegion << region.GetIndex( 0 ) << ",";
			strRegion << region.GetIndex( 1 ) << ",";
			strRegion << region.GetIndex( 2 ) << ",";
			strRegion << region.GetSize( 0 ) << ",";
			strRegion << region.GetSize( 1 ) << ",";
			strRegion << region.GetSize( 2 ) << std::ends;
			m_CurrentInteractionControl->SetValue( strRegion.str() );
		}

		bboxWidget->StopInteraction();
	}

}

void Core::Widgets::DynProcessingWidget::OnBtnPoint( wxCommandEvent &event )
{
	if ( GetPluginTab() == NULL )
	{
		return;
	}

	if ( event.GetInt( ) )
	{
		StartInteraction( event.GetEventObject(), "Landmark selector" );
	}
	else
	{
		wxWindow* applyWin = m_DynPanel->FindWindowById( wxID_APPLY );
		applyWin->Enable( true );

		SelectionToolWidget* landmarkWidget = GetSelectionToolWidget<SelectionToolWidget>( "Landmark selector" );
		if ( !landmarkWidget->IsSelectionEnabled() )
		{
			return;
		}

		Core::DataEntity::Pointer landmarks = landmarkWidget->GetDataEntity( );
		if ( landmarks.IsNotNull() )
		{
			Core::DataEntityImpl::Pointer impl = landmarks->GetTimeStep( GetTimeStep( ) );
			blTagMap::Pointer data = blTagMap::New( );
			impl->GetData( data );

			blTag::Pointer tagPoints = DataEntityImpl::SafeFindTag( data, "Points" );
			std::vector<DataEntityImpl::Point3D>* points;
			points = tagPoints->GetValueCasted< std::vector<DataEntityImpl::Point3D>* >();

			std::ostringstream strPoint;
			if ( points && !points->empty( ) )
			{

				ModuleParameter* param;
				param = dynModuleHelper::FindParameter( 
					m_Processor->GetModule( ), "", 0, m_CurrentInteractionControl->GetName().ToStdString() );

				int xSign = 1, ySign = 1, zSign = 1;
				// Convert from coordinateSystem
				if ( param )
				{
					if ( param->GetCoordinateSystem() == "lps" )
					{
						// nothing
					}else if ( param->GetCoordinateSystem() == "ras" )
					{
						xSign = -1;
						ySign = -1;
						zSign = 1;
					}
				}

				for ( int i = 0 ; i < points->size() ; i++ )
				{
					strPoint << xSign*points->at( i )[ 0 ] << ",";
					strPoint << ySign*points->at( i )[ 1 ] << ",";
					strPoint << zSign*points->at( i )[ 2 ] << ";";
				}
				m_CurrentInteractionControl->SetValue( strPoint.str() );
			}

		}

		Core::DataEntity::Pointer ldDataEntity = landmarkWidget->GetDataEntity();
		landmarkWidget->StopInteraction();
	}

}

void Core::Widgets::DynProcessingWidget::StartInteraction( wxObject* object, const std::string &name )
{
	// Find the text control
	wxWindow* win = wxDynamicCast( object, wxWindow);
	wxSizer* sizer = win->GetContainingSizer( );
	wxTextCtrl* textCtrl = NULL;
	wxSizerItemList list =sizer->GetChildren();
	wxSizerItemList::iterator it = list.begin( );
	while ( it != list.end( ) && textCtrl == NULL )
	{
		textCtrl = wxDynamicCast( (*it)->GetWindow( ), wxTextCtrl);
		it++;
	}

	wxWindow* applyWin = m_DynPanel->FindWindowById( wxID_APPLY );
	applyWin->Enable( false );

	// Start interaction
	if ( textCtrl )
	{
		m_CurrentInteractionControl = textCtrl;

        ModuleParameter* param;
		param = dynModuleHelper::FindParameter( 
			m_Processor->GetModule( ), "", 0, m_CurrentInteractionControl->GetName().ToStdString() );
        GetSelectionToolWidget<SelectionToolWidget>( name )->SetDataName( "Landmark "+param->GetLabel() );

		GetSelectionToolWidget<SelectionToolWidget>( name )->StartInteractor();
	}
}

void Core::Widgets::DynProcessingWidget::ShowLocationsMenu()
{
	m_LocationMenu = new wxMenu();

	// Add execution data for the active provider
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	Runtime::PluginProviderManager::Pointer pluginProviderManager;
	pluginProviderManager = graphicalIface->GetPluginProviderManager();

	Core::Runtime::PluginProviderManager::PluginProvidersType pluginProviers;
	pluginProviers = pluginProviderManager->GetPluginProviders( );

	Core::Runtime::PluginProviderManager::PluginProvidersType::iterator it;
	for ( it = pluginProviers.begin( ) ; it != pluginProviers.end( ) ; it++ )
	{
		blTagMap::Pointer plugins;
		plugins = (*it)->GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );

		blTag::Pointer tag = plugins->FindTagByName( GetModule()->GetTitle() );
		if ( tag.IsNotNull() && tag->GetValueCasted<bool>() == true )
		{
			wxWindowID id = wxNewId();
			m_LocationMenu->Append( id, (*it)->GetName(), (*it)->GetName(), wxITEM_RADIO );
			if ( m_ActivePluginProvider == (*it)->GetName() )
			{
				m_LocationMenu->Check( id, true );
			}
		}
	}

	PopupMenu( m_LocationMenu );

	delete m_LocationMenu;
}

void Core::Widgets::DynProcessingWidget::OnBtnLocation( wxCommandEvent &event )
{
	ShowLocationsMenu( );
}

void Core::Widgets::DynProcessingWidget::UpdateActiveModule()
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	// Get Base factory
	Core::BaseWindowFactory::Pointer factory;
	factory = graphicalIface->GetBaseWindowFactory( )->FindFactory( GetFactoryName() );

	// Get module using current active plugin provider
	ModuleDescription &module = factory->GetModule( m_ActivePluginProvider );

	m_Processor->SetActivePluginProviderName( m_ActivePluginProvider );

	// Set active module
	SetModule( &module );
}

void Core::Widgets::DynProcessingWidget::CreateWidget()
{
	dynWxAGUIBuilder builder;
	builder.SetModule( m_Processor->GetModule() );
	builder.SetParentWindow( this );
	builder.Update();
	dynBasePanel* newDynPanel = builder.GetPanel();

	if ( m_DynPanel != NULL )
	{
		GetSizer()->Replace( m_DynPanel, newDynPanel );
		m_DynPanel->Destroy();
	}
	else
	{
		GetSizer( )->Add( newDynPanel, 0, wxEXPAND );
	}

	m_DynPanel = newDynPanel;

	AddInteractionButtons( );

	GetSizer()->Detach( m_btnLocation->GetContainingSizer() );
	GetSizer()->Add ( m_btnLocation->GetContainingSizer(), 0, wxEXPAND, 5);

	// Resize parent
	// Cast a resize event
	wxSizeEvent resEvent( GetBestSize(), GetId() );
	resEvent.SetEventObject( this );
	GetEventHandler()->ProcessEvent(resEvent);
}

void Core::Widgets::DynProcessingWidget::OnMenu( wxCommandEvent& event )
{
	wxMenuItem* item = m_LocationMenu->FindItem( event.GetId() );
	m_ActivePluginProvider = item->GetLabel().c_str();

	wxWindowUpdateLocker lock( this );

	UpdateActiveModule( );

	Enable( false );

	// Init processor observers
	InitProcessorObservers( true );

	// Init Input widgets. Disable automatic selection
	// Don't select input by default. It's incompatible with single time step processing
	Core::ProcessorWidgetsBuilder::Init( 
		GetProcessor().GetPointer(), 
		this, 
		GetListBrowser(), 
		false );

	Enable();

	CreateWidget( );

	UpdateWidget( );
}

void Core::Widgets::DynProcessingWidget::UpdateDefaultPluginProvider()
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	// Get Base factory
	Core::BaseWindowFactory::Pointer factory;
	factory = graphicalIface->GetBaseWindowFactory( )->FindFactory( GetFactoryName() );

	m_ActivePluginProvider = factory->GetDefaultModuleKey();
}

