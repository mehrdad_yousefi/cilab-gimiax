/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreSimpleProcessingWidget_H
#define _coreSimpleProcessingWidget_H

#include "coreSimpleProcessingWidgetUI.h"
#include "coreProcessingWidget.h"


namespace Core{
namespace Widgets{

/**
\brief Processing widget with apply button that calls UpdateProcessor( )
\ingroup gmWidgets
\author Xavi Planes
\date 27 April 2010
*/
template <class T>
class SimpleProcessingWidget: 
	public coreSimpleProcessingWidgetUI,
	public ProcessingWidget
{
public:
	class Factory : public Core::BaseWindowFactory
	{
	public:
		coreDeclareSmartPointerTypesMacro(Factory,BaseWindowFactory)
		coreFactorylessNewMacro(Factory)
		coreCommonFactoryFunctions( Core::Widgets::SimpleProcessingWidget<T> )
		virtual const char* GetNameOfClass(void) const 
		{
			return GetNameClass( );
		}
		static const char* GetNameClass(void)
		{
			m_Name = "Core::Widgets::SimpleProcessingWidget<";
			m_Name += T::GetNameClass( );
			m_Name += ">Factory";
			return m_Name.c_str();
		}
		Factory( )
		{
		}
		static std::string m_Name;
	};

	//!
    SimpleProcessingWidget(wxWindow* parent, int id = wxID_ANY, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

	//!
	virtual Core::BaseProcessor::Pointer GetProcessor( );

private:

	//!
	void OnApply(wxCommandEvent &event);

	//!
	typename T::Pointer m_Processor;
};

}
}

#include "coreSimpleProcessingWidget.txx"

#endif // _coreSimpleProcessingWidget_H
