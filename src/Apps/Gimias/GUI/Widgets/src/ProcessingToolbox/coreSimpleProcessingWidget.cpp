/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreSimpleProcessingWidget.h"
#include "coreDynProcessor.h"

void TestSimpleProcessingWidget( )
{
	typedef Core::Widgets::SimpleProcessingWidget<Core::DynProcessor> 
		SampleWidget;
	SampleWidget* widget;
	Core::BaseWindowFactory::Pointer factory = SampleWidget::Factory::NewBase();
	factory->GetNameClass( );
	widget = NULL;
	//widget = new Core::Widgets::SimpleProcessingWidget<Core::BaseProcessor>( NULL, wxID_ANY );
}
