/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _DynProcessingWidgetFactory_H
#define _DynProcessingWidgetFactory_H

#include "coreDynProcessingWidget.h"

namespace Core{
namespace Widgets{

/**
\brief Dynamic processing widget Factory 
Specific factory that uses internal name instead of class name
to identify it
\ingroup gmWidgets
\author Xavi Planes
\date 15 July 2010
*/
class DynProcessingWidgetFactory : public Core::BaseWindowFactory
{
public:
	coreDeclareSmartPointerTypesMacro(Core::Widgets::DynProcessingWidgetFactory,BaseWindowFactory)
	coreFactorylessNewMacro(Core::Widgets::DynProcessingWidgetFactory)
	coreCommonFactoryFunctionsNewBase( )
	coreCommonFactoryFunctionsCreateWindow( Core::Widgets::DynProcessingWidget )

	void SetModule( const std::string &key, ModuleDescription val)
	{
		BaseWindowFactory::SetModule( key, val );
		m_Name = GetModule( key ).GetTitle( );
	}

	const char* GetNameOfClass() const 
	{ 
		return m_Name.c_str(); 
	} 

	void SetName(std::string val) 
	{ 
		m_Name = val + "Factory"; 
	}

	std::string m_Name;
};

}
}

#endif // DynProcessingWidgetFactory_H
