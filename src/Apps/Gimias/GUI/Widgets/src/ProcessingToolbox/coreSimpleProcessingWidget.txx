/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreSimpleProcessingWidget.h"

template <class T>
std::string Core::Widgets::SimpleProcessingWidget<T>::Factory::m_Name;

template <class T>
Core::Widgets::SimpleProcessingWidget<T>::SimpleProcessingWidget(
	wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
		coreSimpleProcessingWidgetUI(parent, id, pos, size, wxTAB_TRAVERSAL)
{
	SetId( wxNewId( ) );
	m_Processor = T::New( );
}

template <class T>
Core::BaseProcessor::Pointer Core::Widgets::SimpleProcessingWidget<T>::GetProcessor( )
{
	return m_Processor.GetPointer( );
}

template <class T>
void Core::Widgets::SimpleProcessingWidget<T>::OnApply(wxCommandEvent &event)
{
	UpdateProcessor( );
}

