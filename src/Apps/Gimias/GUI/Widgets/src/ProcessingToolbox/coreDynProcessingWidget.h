/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _DynProcessingWidget_H
#define _DynProcessingWidget_H

#include "coreProcessingWidget.h"
#include "coreDynProcessor.h"

class dynBasePanel;

namespace Core{
namespace Widgets{

/**
\brief Dynamic processing widget using Automated GUI

It has a button "Location" that allows to select 
the location that will be used for executing it. 

\ingroup gmWidgets
\author Xavi Planes
\date 15 July 2010
*/
class GMWIDGETS_EXPORT DynProcessingWidget: 
	public wxPanel, 
	public ProcessingWidget {
public:

	//!
	DynProcessingWidget(wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);
	 
	virtual ~DynProcessingWidget( );

	//!	
	virtual void UpdateData();

	//!
	virtual void UpdateWidget();

	//!
	void CreateWidget( );

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

	//!
	void SetModule( ModuleDescription* module );

	//! Build automated GUI
	void OnInit();

	//!
	virtual void OnApply(wxCommandEvent &event);

protected:
    wxDECLARE_EVENT_TABLE();

	//!
	//virtual void OnApply(wxCommandEvent &event);

	//!
	virtual void OnSaveScript(wxCommandEvent &event);

	//!
	virtual void OnBtnRegion(wxCommandEvent &event);

	//!
	virtual void OnBtnPoint(wxCommandEvent &event);

	//!
	void OnBtnLocation(wxCommandEvent &event);

	//!
	void OnMenu( wxCommandEvent& event );

	//!
	void AddInteractionButtons( );

	//!
	void StartInteraction( wxObject* win, const std::string &name );

	//!
	void ShowLocationsMenu( );

	//!
	void UpdateActiveModule( );

	//!
	void UpdateDefaultPluginProvider( );

	//!
	void UpdateProcessor( bool multithreading = true );
private:

	//!
	DynProcessor::Pointer m_Processor;

	//!
	dynBasePanel* m_DynPanel;

	//!
	wxTextCtrl* m_CurrentInteractionControl;

	//!
	wxButton* m_btnLocation;

	//!
	std::string m_ActivePluginProvider;

	//!
	wxMenu* m_LocationMenu;
};

}
}

#endif // DynProcessingWidget_H
