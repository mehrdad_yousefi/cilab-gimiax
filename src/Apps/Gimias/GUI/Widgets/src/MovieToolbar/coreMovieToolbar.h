/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef COREMOVIETOOLBAR_H
#define COREMOVIETOOLBAR_H

#include "coreMovieToolbarUI.h"

#include "gmWidgetsWin32Header.h"
#include "coreCommonDataTypes.h"
#include <wx/spinctrl.h>
#include "coreBaseWindow.h"

namespace Core
{
namespace Widgets
{

#define wxID_MovieToolbar wxID("wxID_MovieToolbar")

// begin wxGlade: ::extracode
// end wxGlade
/**
\brief A widget for managing 3D+t data. 
\ingroup gmWidgets
\author Martin Bianculli
\details This widget allows to play, pause and stop a 3D+t data. It doesn't allow to create a video.
*/
class GMWIDGETS_EXPORT  MovieToolbar: 
	public coreMovieToolbarUI, 
	public Core::BaseWindow{

public:
	//!
	coreDefineBaseWindowFactory( Core::Widgets::MovieToolbar );

	typedef MovieToolbar Self; 
	enum MovieMode {STOPPED = 0, PLAYING = 1, PAUSE = 2};

	enum TimeUnitsType
	{
		TIME_IN_FRAMES,
		TIME_IN_SECS,
		TIME_IN_HOURS,
		TIME_IN_MAX
	};

    // begin wxGlade: MovieToolbarUI::ids
    // end wxGlade
    MovieToolbar(
		wxWindow* parent, 
		int id = wxID_MovieToolbar, 
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=0);

	//! Destructor
	virtual ~MovieToolbar();

	//! Returns the movie slider
	wxSlider* GetSlider();

	//! This function returns the current time stem (the one shown)
	Core::IntHolderType::Pointer GetCurrentTimeStep();

	//! Redefined
	void SetRenderingTree( RenderingTree::Pointer tree );

	//!
	bool Enable(bool enable);

	//! Prints the fame number on the TextCtrl Edit Box
	void UpdateWidget();

	//!
	int GetTimeUnits() const;
	void SetTimeUnits(int val);

	//! Get time range [min, max)
	virtual void GetTimeRange( int &min, int &max );

	//! Get current time
	virtual int GetTime( );

	//!
	void SetFrameRate( unsigned int frameRate );

	//! Play
	void Play();

	//! Stops the video
	void Stop();

protected:

	//! The callback function which observes the mitk::Stepper
	void OnRenderingTreeChanged();

	//! The callback function which observes the m_CurrentTimeStep
	virtual void OnModifiedTimeStepHolder( );

	//! Displays next phase of the 3D+t data
	void OnTimer(wxTimerEvent& event);

	//! Set the 3D+t data to the slider position
	virtual void OnMovieSlider(wxScrollEvent &event);

	//! Start playing 3D+t data
	virtual void OnPlayButton(wxCommandEvent &event);

	//! Stop playing 3D+t data
	virtual void OnStopButton(wxCommandEvent &event);

	//!
    virtual void OnPreviousButton(wxCommandEvent &event);

	//!
    virtual void OnNextButton(wxCommandEvent &event);

	//! Process the spin box commands
	void OnFrameRateSlider(wxScrollEvent &event);

	//! Change current frame number / time in sec.
	void OnFrameNumber(wxCommandEvent &event);

	//! Change numbers to frames or seconds
	void OnBtnFrameTime(wxCommandEvent &event);

	//!
	void UpdateBitmapPlayIcon( );

protected:
	//! milliseconds
	unsigned int m_FrameRate; 
	//! Stores the movie current state
	MovieMode m_MovieMode;
	//! For displaying 3D+t data
	wxTimer* m_Timer;
	//!
	Core::IntHolderType::Pointer m_CurrentTimeStep;

	//!
	int m_TimeUnits;

	//!
	int m_bitmapPlayIcon;

    wxDECLARE_EVENT_TABLE();


}; // wxGlade: end class


} // namespace Widget
} // namespace Core

#endif // COREMOVIETOOLBAR_H
