/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreSlicePlane_H
#define coreSlicePlane_H

#include "gmWidgetsWin32Header.h"
#include "coreObject.h"

namespace Core
{

/** 
Slice Plane information to pass to other windows
\ingroup gmWidgets
\author Xavi Planes
\date Nov 2010
*/
class SlicePlane : public Core::SmartPointerObject
{
public:
	coreDeclareSmartPointerClassMacro(Core::SlicePlane, Core::SmartPointerObject);

	enum PlaneOrientation { Transversal, Sagittal, Frontal };

protected:
	SlicePlane( ) 
	{
		m_Orientation = Transversal;
		m_Id = 0;
		m_Pos = -1;
	}
	~SlicePlane( ) {}

public:
	//!
	PlaneOrientation m_Orientation;
	//!
	int m_Id;
	//!
	int m_Pos;
};

typedef Core::DataHolder<SlicePlane::Pointer> SlicePlaneHolderType;

}

#endif // coreSlicePlane_H
