/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreMultiRenderWindow.h"
#include "coreDataEntity.h"
#include "coreAssert.h"
#include "coreKernel.h"
#include "coreSettings.h"
#include "coreException.h"
#include "coreReportExceptionMacros.h"

using namespace Core::Widgets;

//!
RenderWindowBase::RenderWindowBase()
{
	// Setup the logo
	Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
	coreAssertMacro(settings.IsNotNull() && "The Settings Manager must have been initialized");

	m_EnableTimeUpdates = false;
	m_Lock3DCamera = false;
	m_LockSlicePlanes = false;

	Core::DataEntityHolder::Pointer holder;
	Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
	holder = dataContainer->GetDataEntityList()->GetSelectedDataEntityHolder();
	holder->AddObserver1( this, &RenderWindowBase::OnDataSelected );
}

//!
RenderWindowBase::~RenderWindowBase(void)
{
	if ( m_RenderingTreeConnection.connected() )
	{
		m_RenderingTreeConnection.disconnect();
	}

	Core::DataEntityHolder::Pointer holder;
	Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
	holder = dataContainer->GetDataEntityList()->GetSelectedDataEntityHolder();
	holder->RemoveObserver1( this, &RenderWindowBase::OnDataSelected );
}

void Core::Widgets::RenderWindowBase::SetPrivateRenderingTree( 
	Core::RenderingTree::Pointer val )
{
	m_RenderingTreeConnection.disconnect( );

	SetRenderingTreeInstance( val );

	if ( GetPrivateRenderingTree( ).IsNotNull() )
	{
		m_RenderingTreeConnection = GetPrivateRenderingTree( )->AddObserverOnModified<RenderWindowBase>(
			this, 
			&RenderWindowBase::RenderScene);
	}
}

void Core::Widgets::RenderWindowBase::SaveImage3DWindow( const std::string& completeFileName )
{
	
}

void Core::Widgets::RenderWindowBase::CenterToPoint( double p[3] )
{

}

void Core::Widgets::RenderWindowBase::SetEvtHandlerEnabled( bool enable )
{

}

void Core::Widgets::RenderWindowBase::OnDataSelected( Core::DataEntity::Pointer dataEntity )
{

}

void Core::Widgets::RenderWindowBase::GetProperties( blTagMap::Pointer tagMap )
{

}

void Core::Widgets::RenderWindowBase::SetProperties( blTagMap::Pointer tagMap )
{

}

void Core::Widgets::RenderWindowBase::Init( Core::DataEntityHolder::Pointer selectedDataEntity, bool enablePlanes /*= true*/ )
{

}
bool Core::Widgets::RenderWindowBase::GetEnableTimeUpdates() const
{
	return m_EnableTimeUpdates;
}

void Core::Widgets::RenderWindowBase::SetEnableTimeUpdates( bool val )
{
	m_EnableTimeUpdates = val;
}

void Core::Widgets::RenderWindowBase::SetCamera3DHolder( Camera3DHolderType::Pointer val )
{
	if ( m_Camera3DHolder.IsNotNull() )
	{
		m_Camera3DHolder->RemoveObserver1<RenderWindowBase>( this, &RenderWindowBase::UpdateCamera3D );
	}

	m_Camera3DHolder = val;

	if ( m_Camera3DHolder.IsNotNull() )
	{
		m_Camera3DHolder->AddObserver1<RenderWindowBase>( this, &RenderWindowBase::UpdateCamera3D );
	}
}

Core::Camera3DHolderType::Pointer 
Core::Widgets::RenderWindowBase::GetCamera3DHolder() const
{
	return m_Camera3DHolder;
}

void Core::Widgets::RenderWindowBase::UpdateCamera3D( Camera3D::Pointer cam )
{
	
}

void Core::Widgets::RenderWindowBase::Lock3DCamera( bool lock /*= true */ )
{
	m_Lock3DCamera = lock;
}

bool Core::Widgets::RenderWindowBase::GetLock3DCamera() const
{
	return m_Lock3DCamera;
}

Core::SlicePlaneHolderType::Pointer 
Core::Widgets::RenderWindowBase::GetSlicePlaneHolder() const
{
	return m_SlicePlaneHolder;
}

void Core::Widgets::RenderWindowBase::SetSlicePlaneHolder( 
	SlicePlaneHolderType::Pointer val )
{
	if ( m_SlicePlaneHolder.IsNotNull() )
	{
		m_SlicePlaneHolder->RemoveObserver1<RenderWindowBase>( this, &RenderWindowBase::UpdateSlicePlane );
	}

	m_SlicePlaneHolder = val;

	if ( m_SlicePlaneHolder.IsNotNull() )
	{
		m_SlicePlaneHolder->AddObserver1<RenderWindowBase>( this, &RenderWindowBase::UpdateSlicePlane );
	}
}

bool Core::Widgets::RenderWindowBase::GetLockSlicePlanes() const
{
	return m_LockSlicePlanes;
}

void Core::Widgets::RenderWindowBase::SetLockSlicePlanes( bool val )
{
	m_LockSlicePlanes = val;
}

void Core::Widgets::RenderWindowBase::UpdateSlicePlane( 
	SlicePlane::Pointer slicePlane )
{

}

void Core::Widgets::RenderWindowBase::InitDefaultLayout()
{

}

void Core::Widgets::RenderWindowBase::GetSlicePlanesPositions( 
	std::list<SlicePlane::Pointer> &slicePlanesPositions )
{

}

int Core::Widgets::RenderWindowBase::SecToTimeStep( double time )
{
	std::vector<DataEntity::Pointer> all;
	all = GetAllDataEntitiesWithTime( );
	if ( all.empty() )
	{
		return -1;
	}

	Core::DataEntity::Pointer first = all.front();
	size_t timeStep = 0;
	while ( timeStep < first->GetNumberOfTimeSteps() &&
		    time > first->GetTimeAtTimeStep( timeStep ) )
	{
		timeStep++;
	}

	return int( timeStep );
}

double Core::Widgets::RenderWindowBase::TimeStepToSec( int timeStep )
{
	std::vector<DataEntity::Pointer> all;
	all = GetAllDataEntitiesWithTime( );
	if ( all.empty() )
	{
		return -1;
	}

	Core::DataEntity::Pointer first = all.front();
	timeStep = int( std::min( first->GetNumberOfTimeSteps() - 1, size_t( timeStep ) ) );
	return first->GetTimeAtTimeStep( timeStep );
}

std::vector<Core::DataEntity::Pointer> Core::Widgets::RenderWindowBase::GetAllDataEntitiesWithTime()
{
	std::vector<DataEntity::Pointer> all;
	all = GetPrivateRenderingTree()->GetAllDataEntities();

	std::vector<DataEntity::Pointer> selected;
	std::vector<DataEntity::Pointer>::iterator it;
	for ( it = all.begin() ; it != all.end() ; it++ )
	{
		if ( (*it)->GetNumberOfTimeSteps() && 
			 (*it)->GetTimeAtTimeStep( 0 ) != Core::DataEntityImpl::CONSTANT_IN_TIME )
		{
			selected.push_back( *it );
		}
	}

	return selected;
}

Core::Display2DHolderType::Pointer Core::Widgets::RenderWindowBase::GetDisplay2DHolder() const
{
	return m_Display2DHolder;
}

void Core::Widgets::RenderWindowBase::SetDisplay2DHolder( Display2DHolderType::Pointer val )
{
	if ( m_Display2DHolder.IsNotNull() )
	{
		m_Display2DHolder->RemoveObserver1<RenderWindowBase>( this, &RenderWindowBase::UpdateDisplay2D );
	}

	m_Display2DHolder = val;

	if ( m_Display2DHolder.IsNotNull() )
	{
		m_Display2DHolder->AddObserver1<RenderWindowBase>( this, &RenderWindowBase::UpdateDisplay2D );
	}
}

void Core::Widgets::RenderWindowBase::UpdateDisplay2D( Display2D::Pointer display2D )
{

}

void Core::Widgets::RenderWindowBase::EnableInteractors( bool enable )
{
}