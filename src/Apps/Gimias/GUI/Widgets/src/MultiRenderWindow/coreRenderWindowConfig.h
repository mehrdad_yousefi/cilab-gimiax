/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreRenderWindowConfig_H
#define _coreRenderWindowConfig_H

#include "gmWidgetsWin32Header.h"
#include "coreRenderWindowConfigUI.h"
#include "coreBaseWindow.h"
#include "coreRenderWindowConfigBase.h"

namespace Core
{
namespace Widgets
{

class RenderWindowContainer;

/** 
\brief GIMIAS specific implementation of wxMitkMultiRenderWindowConfig.

\ingroup gmWidgets
\author Xavi Planes
\date 3 Mar 2010
*/
class GMWIDGETS_EXPORT RenderWindowConfig :
	public coreRenderWindowConfigUI,
	public BaseWindow
{
public:
	//!
	coreDefineBaseWindowFactory( Core::Widgets::RenderWindowConfig );

	//!
	RenderWindowConfig(
		wxWindow* parent, 
		int id , const wxPoint&  pos = wxDefaultPosition, const wxSize&  size = wxDefaultSize, long style = 0);

	//!
	void SetMultiRenderWindow( RenderWindowBase* multiRenderWindow );

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

	//!
	void OnInit( );

private:
	void OnAddCol(wxCommandEvent &event);
	void OnAddRow(wxCommandEvent &event);
	void OnRemove(wxCommandEvent &event);

	//!
	RenderWindowContainer* GetWorkingArea( );

	//!
	void UpdateWidget( );

	//!
	void InitComboBox( );

	//!
	RenderWindowBase* CreateView( RenderWindowContainer* workingArea );

	//!
	void UpdateRegisteredWindows( );

	//! Overwride
	Core::BaseWindow* CreateChild(const std::string &factoryName );

	//! Overwride
	void DestroyChild( BaseWindow* win );

private:
	//!
	std::vector<std::string> m_ViewList;
	//!
	RenderWindowConfigBase* m_MultiRenderWindowConfigBase;
};

}
}

#endif // _coreRenderWindowConfig_H
