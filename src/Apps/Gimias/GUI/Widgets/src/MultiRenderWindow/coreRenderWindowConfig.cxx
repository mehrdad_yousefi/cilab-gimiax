/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreRenderWindowConfig.h"
#include "corePluginTab.h"
#include "coreRenderWindowContainer.h"
#include "coreWorkingAreaManagerWidget.h"
#include "coreBaseWindowFactories.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreBaseWindowFactorySearch.h"

Core::Widgets::RenderWindowConfig::RenderWindowConfig( 
	wxWindow* parent, int id, const wxPoint& pos /*= wxDefaultPosition*/, 
	const wxSize& size /*= wxDefaultSize*/, long style /*= 0*/)
	: coreRenderWindowConfigUI( parent, id, pos, size, style )
{
	InitComboBox( );

	m_MultiRenderWindowConfigBase = NULL;

	SetIsContainerWindow( true );
	SetChildWindowType( WIDGET_TYPE_RENDER_WINDOW_CONFIG );
}

void Core::Widgets::RenderWindowConfig::OnInit( )
{
	UpdateRegisteredWindows( );
}

void Core::Widgets::RenderWindowConfig::SetMultiRenderWindow( 
	Core::Widgets::RenderWindowBase* multiRenderWindow )
{
	BaseWindow::SetMultiRenderWindow( multiRenderWindow );

	UpdateWidget();
}

Core::BaseProcessor::Pointer Core::Widgets::RenderWindowConfig::GetProcessor()
{
	return NULL;
}


void Core::Widgets::RenderWindowConfig::OnAddCol( wxCommandEvent &event )
{
	RenderWindowContainer* workingArea = GetWorkingArea( );
	if ( !GetWorkingArea( ) )
	{
		return;
	}

	RenderWindowBase *multiRenderWindow = CreateView( workingArea );

	// Add to the container
	workingArea->Add( multiRenderWindow );
	workingArea->SetActiveWindow( multiRenderWindow );

	UpdateWidget();
}

void Core::Widgets::RenderWindowConfig::OnAddRow( wxCommandEvent &event )
{
	RenderWindowContainer* workingArea = GetWorkingArea( );
	if ( !GetWorkingArea( ) )
	{
		return;
	}

	RenderWindowBase *multiRenderWindow = CreateView( workingArea );

	// Add to the container
	RenderWindowBase* window = workingArea->GetActiveWindow( );
	int layer = workingArea->GetMaxLayer();
	if ( window )
	{
		layer = workingArea->GetAuiPaneInfo( window ).dock_layer;
	}
	workingArea->Add( multiRenderWindow, layer );
	workingArea->SetActiveWindow( multiRenderWindow );

	UpdateWidget();
}

void Core::Widgets::RenderWindowConfig::OnRemove( wxCommandEvent &event )
{
	RenderWindowContainer* workingArea = GetWorkingArea( );
	if ( !GetWorkingArea( ) )
	{
		return;
	}

	// Add to the container
	RenderWindowBase* multiRenderWindow = workingArea->GetActiveWindow( );
	workingArea->Destroy( multiRenderWindow );

	UpdateWidget();
}

Core::Widgets::RenderWindowContainer* Core::Widgets::RenderWindowConfig::GetWorkingArea()
{
	if ( !GetPluginTab() )
	{
		return NULL;
	}

	BaseWindow* baseWindow = GetPluginTab()->GetCurrentWorkingArea();
	if ( baseWindow == NULL )
	{
		return NULL;
	}

	RenderWindowContainer* workingArea = dynamic_cast<RenderWindowContainer*> ( baseWindow );
	if ( workingArea == NULL )
	{
		return NULL;
	}

	return workingArea;
}

void Core::Widgets::RenderWindowConfig::UpdateWidget()
{
	RenderWindowContainer* workingArea = GetWorkingArea( );
	RenderWindowBase* window = NULL;
	if ( workingArea )
	{
		window = workingArea->GetActiveWindow( );
	}

	m_btnAddColView->Enable( workingArea );
	m_btnAddRowView->Enable( workingArea );
	m_btnRemoveView->Enable( workingArea && window );

}

void Core::Widgets::RenderWindowConfig::InitComboBox()
{

	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	BaseWindowFactorySearch::Pointer search = BaseWindowFactorySearch::New( );
	search->SetType( WIDGET_TYPE_RENDER_WINDOW );
	search->Update( );
	std::list<std::string> factoryList = search->GetFactoriesNames();

	if ( factoryList.empty() || factoryList.size() == m_ViewList.size( ) )
	{
		return;
	}

	m_ViewList.clear( );
	m_CmbNewView->Clear( );
	std::list<std::string>::iterator it;
	for ( it = factoryList.begin(); it != factoryList.end() ; it++ )
	{
		m_ViewList.push_back( *it );
		WindowConfig config;
		baseWindowFactory->GetWindowConfig( *it, config );
		m_CmbNewView->AppendString( config.GetCaption() );
	}

	m_CmbNewView->SetSelection( 0 );
}

Core::Widgets::RenderWindowBase* 
Core::Widgets::RenderWindowConfig::CreateView( RenderWindowContainer* workingArea )
{
	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );
	std::string factory = m_ViewList[ m_CmbNewView->GetSelection() ];

	BaseWindow* baseWindow = baseWindowFactory->CreateBaseWindow( factory, workingArea );
	RenderWindowBase *multiRenderWindow;
	multiRenderWindow = dynamic_cast<RenderWindowBase*> ( baseWindow );

	GetPluginTab()->InitBaseWindow( baseWindow );

	multiRenderWindow->InitDefaultLayout( );

	wxWindow* win = dynamic_cast<wxWindow*> ( multiRenderWindow );
	std::ostringstream stream;
	stream << "View" << workingArea->GetNumberOfWindows();
	win->SetName( stream.str() );

	Core::DataEntityList::Pointer dataEntityList;
	dataEntityList = Core::Runtime::Kernel::GetDataContainer( )->GetDataEntityList( );
	Core::DataEntityHolder::Pointer selectedDataEntityHolder;
	selectedDataEntityHolder = dataEntityList->GetSelectedDataEntityHolder();
	multiRenderWindow->Init( selectedDataEntityHolder );

	return multiRenderWindow;
}

Core::BaseWindow* Core::Widgets::RenderWindowConfig::CreateChild( const std::string &factoryName )
{
	if ( m_MultiRenderWindowConfigBase != NULL )
	{
		return NULL;
	}

	// Get factory
	Core::BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	// Create window
	BaseWindow *baseWin = baseWindowFactory->CreateBaseWindow( factoryName, this );
	
	wxWindow* win = dynamic_cast<wxWindow*> ( baseWin );
	wxStaticBox *staticbox = new wxStaticBox(this, -1, wxT("Current View configuration"));
	wxStaticBoxSizer* sizer = new wxStaticBoxSizer(staticbox, wxVERTICAL);
	sizer->Add( win, wxSizerFlags( ).Proportion( 1 ).Expand() );
	GetSizer( )->Add( sizer, wxSizerFlags( ).Proportion( 1 ).Expand().Border() );

	m_MultiRenderWindowConfigBase = dynamic_cast<RenderWindowConfigBase*> ( baseWin );

	// OnInit can change the processor observers
	GetPluginTab( )->InitBaseWindow( baseWin );

	return baseWin;
}

void Core::Widgets::RenderWindowConfig::DestroyChild( Core::BaseWindow* win )
{
	wxWindow* wxWin = dynamic_cast<wxWindow*> ( win );

	wxWin->Destroy( );
	
	m_MultiRenderWindowConfigBase = NULL;
}

void Core::Widgets::RenderWindowConfig::UpdateRegisteredWindows()
{
	InitComboBox();

	BaseWindow::UpdateRegisteredWindows( );
}
