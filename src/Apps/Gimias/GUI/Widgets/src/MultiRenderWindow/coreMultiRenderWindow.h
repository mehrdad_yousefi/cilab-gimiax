/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreMultiRenderWindow_H
#define coreMultiRenderWindow_H

#include "coreRenderWindowBase.h"

namespace Core
{
namespace Widgets
{
//! Backwards compatibility
typedef RenderWindowBase MultiRenderWindow;

}
}

#endif // coreMultiRenderWindow_H
