/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreDisplay2D_H
#define coreDisplay2D_H

#include "gmWidgetsWin32Header.h"
#include "coreObject.h"

namespace Core
{

/** 
2D Display information to pass to other windows
\ingroup gmWidgets
\author Xavi Planes
\date Oct 2011
*/
class Display2D : public Core::SmartPointerObject
{
public:
	coreDeclareSmartPointerClassMacro(Core::Display2D, Core::SmartPointerObject);

protected:
	Display2D( ) 
	{
		m_scaleFactor = 0;
		m_OriginInMMX = 0;
		m_OriginInMMY = 0;
		m_Id = -1;
	}
	~Display2D( ) {}

public:
	double m_scaleFactor;
	double m_OriginInMMX;
	double m_OriginInMMY;
	//! Identifier of the display 2D
	int m_Id;
};

typedef Core::DataHolder<Display2D::Pointer> Display2DHolderType;

}

#endif // coreDisplay2D_H
