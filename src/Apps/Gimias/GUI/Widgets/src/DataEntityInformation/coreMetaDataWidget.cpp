/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/


#include "coreMetadataWidget.h"
#include "coreDataEntityMetadata.h"
#include "coreDataEntityInfoHelper.h"
#include "coreMetadataInformationWidget.h"

#include "wxUnicode.h"

Core::Widgets::MetadataWidget::MetadataWidget(wxWindow* parent, 
								int id, 
								const wxPoint& pos, 
								const wxSize& size, 
								long style):
    wxPanel(parent, id, pos, size, wxTAB_TRAVERSAL)
{
	m_dataEntityHolder = Core::DataEntityHolder::New();

	m_label_NrOfTimeStep = new wxStaticText(this, wxID_ANY, wxT("Number of Time Steps"));
	m_txt_numberTimeSteps = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_CENTRE|wxNO_BORDER);
	m_label_mod = new wxStaticText(this, wxID_ANY, wxT("Image modality"));
	m_comboBox_modality = new wxComboBox(this, wxID_ANY, wxT("Image modality"), wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_DROPDOWN|wxCB_SIMPLE|wxCB_READONLY);

	m_lblType = new wxStaticText(this, wxID_ANY, wxT("Type"));
	m_cmbType = new wxComboBox(this, wxID_ANY, wxT("Type"), wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_DROPDOWN|wxCB_SIMPLE|wxCB_READONLY);

	m_MetadataInformationWidget = new MetadataInformationWidget( this, wxID_ANY );
	m_MetadataInformationWidget->SetInputHolder( m_dataEntityHolder );

	// definition of variables
	m_changeInWidgetObserver.SetSlotFunction(this, &MetadataWidget::UpdateData);
	m_changeInWidgetObserver.Observe(m_comboBox_modality);
	m_changeInWidgetObserver.Observe(m_cmbType);
		
	//update modality combo box
	Core::DataEntityInfoHelper::ModalityList list;
	list = Core::DataEntityInfoHelper::GetAvailableModalityTypes();
	for(Core::DataEntityInfoHelper::ModalityList::iterator it = list.begin();
		it != list.end();
		++it)
	{
		std::string modalityStr = Core::DataEntityInfoHelper::GetModalityTypeAsString(*it);
		m_comboBox_modality->Append(_U(modalityStr.c_str()));
	}

	// Update DataEntity type combo box
	Core::DataEntityInfoHelper::EntityList typesList;
	typesList = Core::DataEntityInfoHelper::GetAvailableEntityTypes();
	for(Core::DataEntityInfoHelper::EntityList::iterator it = typesList.begin();
		it != typesList.end();
		++it)
	{
		std::string typeStr = Core::DataEntityInfoHelper::GetEntityTypeAsString(*it);
		m_cmbType->Append(_U(typeStr.c_str()));
	}
	

	set_properties();
	do_layout();

	m_DisableDataEntityUpdate = false;
}

void Core::Widgets::MetadataWidget::UpdateData()
{
	if(m_dataEntity.IsNull())
		 return;
	
	m_DisableDataEntityUpdate = true;

	Core::DataEntityMetadata::Pointer meta = m_dataEntity->GetMetadata();

	size_t nTimeSteps=m_dataEntity->GetNumberOfTimeSteps();

	//get modality from combo box
	std::string modStr = _U(m_comboBox_modality->GetValue());
	ModalityType mod = DataEntityInfoHelper::GetModalityType (modStr);
	meta->SetModality (mod);

	std::string typeStr = _U(m_cmbType->GetValue());
	m_dataEntity->SetType( DataEntityInfoHelper::GetDataEntityType( typeStr ) );

	m_DisableDataEntityUpdate = false;

	// Update rendering node based on modality
	// RenderingTreeMITK::OnModifiedDataEntity( )
	m_dataEntity->Modified();

}

void Core::Widgets::MetadataWidget::UpdateWidget(bool bResetModality /*=true*/)
{		
	if ( m_dataEntity.IsNull() || m_DisableDataEntityUpdate )
		return;

	Core::DataEntityMetadata::Pointer meta = m_dataEntity->GetMetadata();
	size_t numTimeSteps = m_dataEntity->GetNumberOfTimeSteps();

	if (numTimeSteps > 1)
	{
		//show widget controls for time steps number, ED,ES flags and values
		m_txt_numberTimeSteps->Show();
		m_label_NrOfTimeStep->Show();

		m_txt_numberTimeSteps->SetValue( wxString::Format(wxT("%d"),
			(int)m_dataEntity->GetNumberOfTimeSteps()));
	}
	else
	{
		//hide widget controls for time steps number, ED,ES flags and values, when 
		m_txt_numberTimeSteps->Hide();
		m_label_NrOfTimeStep->Hide();
	}

	std::string typeString = DataEntityInfoHelper::GetEntityTypeAsString( m_dataEntity->GetType() );
	m_cmbType->SetValue( typeString );

	if(!bResetModality)
		return;

	//set modality on combo box
	Core::ModalityType mod = m_dataEntity->GetMetadata()->GetModality();
	std::string modString = DataEntityInfoHelper::GetModalityTypeAsString (mod);
	m_comboBox_modality->SetValue (modString);
}

void Core::Widgets::MetadataWidget::SetDataEntity( Core::DataEntity::Pointer data)
{
	m_dataEntity = data;
	this->m_dataEntityHolder->SetSubject(m_dataEntity);
}

void Core::Widgets::MetadataWidget::set_properties()
{
    
}


void Core::Widgets::MetadataWidget::do_layout()
{
	// layout them
	wxBoxSizer* vlayout = new wxBoxSizer(wxVERTICAL);
	
	// First part
	wxBoxSizer* sizer_1 = new wxBoxSizer(wxVERTICAL);
	
	//Sizer for image modality
	wxBoxSizer* sizer_9 = new wxBoxSizer(wxHORIZONTAL);
	sizer_9->Add (m_label_mod, 1, wxEXPAND, 0);
	sizer_9->Add (m_comboBox_modality, 1, wxEXPAND, 0);
	sizer_1->Add (sizer_9, 0, wxEXPAND, 0);
	m_comboBox_modality->SetInitialSize( m_comboBox_modality->GetSize() ); // due to bug in wxWidgets 3.0.2, set the initial size or dropbox will not display correctly

	// DataEntity type
	wxBoxSizer* sizer_10 = new wxBoxSizer(wxHORIZONTAL);
	sizer_10->Add (m_lblType, 1, wxEXPAND, 0);
	sizer_10->Add (m_cmbType, 1, wxEXPAND, 0);
	sizer_1->Add (sizer_10, 0, wxEXPAND, 0);
	m_cmbType->SetInitialSize( m_cmbType->GetSize() ); // due to bug in wxWidgets 3.0.2, set the initial size or dropbox will not display correctly

	// Second part
	wxBoxSizer* sizer_2 = new wxBoxSizer(wxVERTICAL);
	
	//Sizers for timesteps 
	wxBoxSizer* sizer_6 = new wxBoxSizer(wxHORIZONTAL);
	sizer_6->Add(m_label_NrOfTimeStep, 1, wxEXPAND, 0);
	sizer_6->Add(m_txt_numberTimeSteps, 1, wxEXPAND, 0);
	sizer_2->Add(sizer_6, 0, wxEXPAND, 0);

	sizer_2->Add( m_MetadataInformationWidget, 1, wxEXPAND | wxALL, 4 );
	

	vlayout->Add(sizer_1, 0, wxEXPAND | wxALL, 4);
	vlayout->Add(sizer_2, 1, wxEXPAND | wxALL, 4);
	SetSizer(vlayout);
	vlayout->Fit(this);
}

