/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include "wx/wxprec.h"

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include <wx/notebook.h>

// Core
#include "coreDataEntityInformation.h"
#include "coreReportExceptionMacros.h"
#include "coreDataEntity.h"
#include "coreInputControl.h"
#include "coreDataEntityHelper.h"
#include "coreDataContainer.h"
#include "coreKernel.h"
#include "coreDataEntityMetadata.h"
#include "coreBaseWindowFactories.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreBaseWindowFactorySearch.h"

#include "Information.xpm"

using namespace Core::Widgets;

//!
DataEntityInformation::DataEntityInformation(wxWindow* parent, 
											wxWindowID id, 
											const wxPoint& pos, 
											const wxSize& size, 
											long style, 
											const wxString& name)
: wxScrolledWindow(parent, id, pos, size, style, name)
{
	SetBitmap( information_xpm );

	m_AcquireInput =  new InputControl( this, wxID_ANY,	true);

	m_Notebook = new wxNotebook(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxNB_TOP);

	// Create the widgets
	wxPanel* notebook_Pane = new wxPanel(m_Notebook, wxID_ANY);
	m_StackControl = new wxWidgetStackControl(notebook_Pane, wxID_ANY);
	notebook_Pane->SetSizer( new wxBoxSizer(wxVERTICAL) );
	notebook_Pane->GetSizer()->Add( m_StackControl, 0, wxEXPAND | wxALL, 4 );
	m_Notebook->AddPage(notebook_Pane,wxT("Processing data"));
	m_MetadataWidget = new Core::Widgets::MetadataWidget(m_Notebook,wxID_ANY);
	m_Notebook->AddPage(m_MetadataWidget,wxT("Meta data"));

	m_NumericDataWidget = new Core::Widgets::NumericDataWidget(m_StackControl,wxID_ANY);
	m_StackControl->Add(m_NumericDataWidget);

	//m_AcquireInput->Hide();
	m_EmptyLabel = new wxStaticText(
		m_StackControl, 
		wxID_ANY, 
		wxT("\n\nThere are not available informations \nfor the current input data entity"), 
		wxDefaultPosition, 
		wxDefaultSize );

	// Add the widgets to the stackControl
	m_StackControl->Add(m_EmptyLabel);


	m_lblDataEntityID = new wxStaticText(this, wxID_ANY, wxT("ID"));
	m_txtDataEntityID = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_CENTRE|wxNO_BORDER);
	m_SizerDataEntityID = new wxBoxSizer(wxHORIZONTAL);
	m_SizerDataEntityID->Add(m_lblDataEntityID, 1, wxEXPAND, 0);
	m_SizerDataEntityID->Add(m_txtDataEntityID, 1, wxEXPAND, 0);

	// Init holders
	SetInputHolder(Core::DataEntityHolder::New());
	m_AcquireInput->SetAcquiredInputDataHolder( 
		m_InputHolder,
		Core::UnknownTypeId );
	
	set_properties( );
	do_layout( );

	m_StackControl->Raise(m_EmptyLabel);

	SetIsContainerWindow( true );
	SetChildWindowType( WIDGET_TYPE_DATA_INFORMATION );
}

void DataEntityInformation::OnInit( )
{
	UpdateRegisteredWindows( );
}

void DataEntityInformation::set_properties()
{
	SetScrollRate(10, 10);
}

void DataEntityInformation::do_layout()
{
	// layout them
	wxBoxSizer* vlayout = new wxBoxSizer(wxVERTICAL);
	vlayout->Add(m_AcquireInput, 0, wxEXPAND | wxALL, 4);
	vlayout->Add(m_SizerDataEntityID, 0, wxEXPAND | wxALL, 4);
	vlayout->Add(m_Notebook, 1, wxEXPAND | wxALL, 4);

	//wxStaticBox* processingStaticBox = new wxStaticBox(this, -1, wxT("Processing data"));
	//wxStaticBoxSizer* sizer1 = new wxStaticBoxSizer(processingStaticBox, wxVERTICAL);
	//sizer1->Add(m_StackControl, 0, wxEXPAND | wxALL, 4);
	//vlayout->Add(sizer1, 0, wxEXPAND | wxALL, 4);

	//wxStaticBox* metadataStaticBox = new wxStaticBox(this, -1, wxT("Meta data"));
	//wxStaticBoxSizer* sizer2 = new wxStaticBoxSizer(metadataStaticBox, wxVERTICAL);
	//sizer2->Add(m_MetadataWidget, 1, wxEXPAND | wxALL, 4);
	//vlayout->Add(sizer2, 1, wxEXPAND | wxALL, 4);

	SetSizer(vlayout);
	vlayout->Fit(this);

}
//!
DataEntityInformation::~DataEntityInformation(void)
{
	m_InputHolder->SetSubject( NULL );
	if ( m_InputHolder.IsNotNull() )
	{
		m_InputHolder->RemoveObserver(
			this, 
			&DataEntityInformation::OnInputHolderChanged, 
			Core::DH_SUBJECT_MODIFIED_OR_NEW_SUBJECT);
	}
}

//!
void DataEntityInformation::SetListBrowser(DataEntityListBrowser* listBrowser)
{
	BaseWindow::SetListBrowser( listBrowser );
	m_AcquireInput->SetDataEntityListBrowser( listBrowser ); 
}

//!
void DataEntityInformation::OnInputHolderChanged(void)
{
	Core::DataEntity::Pointer dataEntity = m_InputHolder->GetSubject();

	wxWindowUpdateLocker lock( this );

	// Clear old data
	ClearOldData();
		
	m_MetadataWidget->SetDataEntity(dataEntity);

	if(dataEntity.IsNotNull())
	{
		m_txtDataEntityID->SetValue( wxString::Format( "%d", (int)dataEntity->GetId() ) );

		dataEntity->GetMetadata()->AddObserverOnChangedTag<DataEntityInformation>(
			this, 
			&DataEntityInformation::OnChangedTag);

		bool found = false;
		for ( int i = 0 ; i < m_StackControl->GetNumberOfWidgets( ) ; i++ )
		{
			wxWindow* win = m_StackControl->GetWidgetByIndex( i );
			DataInformationWidgetBase* info = dynamic_cast<DataInformationWidgetBase*> ( win );
			if ( info && info->IsValidData( dataEntity ) )
			{
				info->SetInputDataEntity( dataEntity );
				m_StackControl->Raise( win );
				found = true;
			}
		}

		if ( dataEntity->IsNumericData() )
		{
			m_StackControl->Raise(m_NumericDataWidget);
			blTagMap::Pointer tagMap;
			dataEntity->GetProcessingData(tagMap);
			m_NumericDataWidget->UpdateWidget(tagMap);
			found = true;
		}

		if ( !found )
		{
			m_StackControl->Raise(m_EmptyLabel);
		}

		// Metadata 
		m_MetadataWidget->UpdateWidget();

	}
	else
	{
		m_StackControl->Raise(m_EmptyLabel);
		m_MetadataWidget->UpdateWidget();
	}

	FitInside( );

	wxSizeEvent resEvent(m_Notebook->GetPage( 0 )->GetBestSize(), m_Notebook->GetPage( 0 )->GetId());
	resEvent.SetEventObject(m_Notebook->GetPage( 0 ));
	m_Notebook->GetPage( 0 )->GetEventHandler()->ProcessEvent(resEvent);

}

void Core::Widgets::DataEntityInformation::SetInputHolder( Core::DataEntityHolder::Pointer inputHolder )
{
	if ( m_InputHolder.IsNotNull() )
	{
		m_InputHolder->RemoveObserver(
			this, 
			&DataEntityInformation::OnInputHolderChanged, 
			Core::DH_SUBJECT_MODIFIED_OR_NEW_SUBJECT);
	}
	m_InputHolder = inputHolder;
	m_InputHolder->SetName( _U( GetName() ) + ": Input" );

	m_InputHolder->AddObserver(
		this, 
		&DataEntityInformation::OnInputHolderChanged, 
		Core::DH_SUBJECT_MODIFIED_OR_NEW_SUBJECT);
}


bool Core::Widgets::DataEntityInformation::Show( bool show /* = true */ )
{
	return wxPanel::Show( show );
}

void Core::Widgets::DataEntityInformation::OnChangedTag(
	blTagMap* renamedDataEntity,
	const std::string &name)
{
	m_MetadataWidget->UpdateWidget(false); //do not reset modality name..otherwise changes to modality will be invalidated
}

void Core::Widgets::DataEntityInformation::ClearOldData()
{
	for ( int i = 0 ; i < m_StackControl->GetNumberOfWidgets( ) ; i++ )
	{
		wxWindow* win = m_StackControl->GetWidgetByIndex( i );

		DataInformationWidgetBase* info;
		info = dynamic_cast<DataInformationWidgetBase*> ( win );

		if ( info )
		{
			info->Clear();
		}
	}
}

Core::BaseProcessor::Pointer Core::Widgets::DataEntityInformation::GetProcessor()
{
	return NULL;
}

Core::BaseWindow* Core::Widgets::DataEntityInformation::CreateChild( const std::string &factoryName )
{
	// Get factory
	Core::BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	// Create window
	BaseWindow *baseWin = baseWindowFactory->CreateBaseWindow( factoryName, m_StackControl );

	// Check type
	DataInformationWidgetBase* widget = dynamic_cast<DataInformationWidgetBase*> ( baseWin );
	if ( widget == NULL )
	{
		throw Core::Exceptions::Exception( 
			"DataEntityInformation::CreateChild",
			"A Data Information window should derive from DataInformationWidgetBase" );
	}

	// Add to stack
	wxWindow* win = dynamic_cast<wxWindow*> ( baseWin );
	m_StackControl->Add( win );

	return baseWin;
}

void Core::Widgets::DataEntityInformation::DestroyChild( Core::BaseWindow* win )
{
	wxWindow* wxWin = dynamic_cast<wxWindow*> ( win );

	m_StackControl->Remove( wxWin );
	
	wxWin->Destroy( );
}

wxWidgetStackControl* Core::Widgets::DataEntityInformation::GetStackControl() const
{
	return m_StackControl;
}

/*
//! TODO if you want to take into account the changes of the metadata and to put it in the 
data information widget
you should also add the observer to changed tag like in the data entity list browser

dataEntity->GetMetadata( )->AddObserverOnChangedTag<DataEntityInformation>(
			this, 
			&DataEntityInformation::OnChangedTag);
void Core::Widgets::DataEntityInformation::OnChangedTag(
	blTagMap* tagMap,
	unsigned long tagId )
{
	if ( tagId != Core::DE_TAG_ED ||
		 tagId != Core::DE_TAG_ES)
	{
		return;
	}
	Core::DataEntityMetadata* metaData = dynamic_cast<Core::DataEntityMetadata*> (tagMap);
	if ( metaData == NULL )
	{
		return;
	}
}*/
