/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreDataInformationWidgetBase_H
#define coreDataInformationWidgetBase_H

#include "coreDataEntity.h"

namespace Core{
namespace Widgets{

/** 
\brief Base interface for all DataInformationWidget
\ingroup gmWidgets
\author Xavi Planes
\date Nov 2010
*/
class DataInformationWidgetBase : public BaseWindow
{
public:
	//!
	virtual bool IsValidData( Core::DataEntity::Pointer dataEntity ) = 0;
	//!
	virtual void SetInputDataEntity( Core::DataEntity::Pointer dataEntity ) = 0;
	//!
	virtual void Clear( ) = 0;
};

} // Widgets
} // Core

#endif // coreDataInformationWidgetBase_H
