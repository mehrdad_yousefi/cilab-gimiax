/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "corePreferencesDialog.h"
#include "corePreferencesPage.h"
#include "coreBaseWindowFactorySearch.h"

using namespace Core::Widgets;

Core::Widgets::PreferencesDialog::PreferencesDialog( 
	wxWindow* parent, int id, const wxString& title, 
	const wxPoint& pos/*=wxDefaultPosition*/, const wxSize& size/*=wxDefaultSize*/, 
	long style/*=wxDEFAULT_DIALOG_STYLE*/ )
	: corePreferencesDialogUI(parent, id, title, pos, size, wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER|wxMAXIMIZE_BOX|wxMINIMIZE_BOX)
{
	m_PreferencesWindow = NULL;

	// Add observer to registered windows
	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );
	baseWindowFactory->GetFactoriesHolder()->AddObserver(
		this,
		&PreferencesDialog::UpdateTreebook );
}

void Core::Widgets::PreferencesDialog::UpdateTreebook()
{
	if ( m_PreferencesWindow == NULL )
	{
		return;
	}

	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = Core::Runtime::Kernel::GetGraphicalInterface()->GetBaseWindowFactory( );

	// Create all preferences windows
	BaseWindowFactorySearch::Pointer search = BaseWindowFactorySearch::New( );
	search->SetType( WIDGET_TYPE_PREFERENCES );
	search->Update( );
	std::list<std::string> windowsList = search->GetFactoriesNames();

	std::list<std::string>::iterator it;
	for ( it = windowsList.begin( ) ; it != windowsList.end() ; it++ )
	{
		std::string factoryName = *it;

		WindowConfig config;
		baseWindowFactory->GetWindowConfig( factoryName, config );
		
		if ( !wxWindow::FindWindowByName( config.GetCaption(), m_PreferencesWindow ) )
		{
			BaseWindow* baseWindow = baseWindowFactory->CreateBaseWindow( factoryName, m_PreferencesWindow );
			m_PreferencesWindow->AddPage( 
				dynamic_cast<wxWindow*> (baseWindow),
				config.GetCaption() );

		}
	}

	// Delete all unregistered windows 
	std::list<wxWindow*> winList;
	Core::Widgets::PreferencesPage* page;
	for ( size_t i = 0 ; i < m_PreferencesWindow->GetPageCount( ) ; i++ )
	{
		page = dynamic_cast<Core::Widgets::PreferencesPage*> ( m_PreferencesWindow->GetPage( i ) );
		if ( !page )
		{
			std::stringstream stream;
			stream << "Preference page " << m_PreferencesWindow->GetPage( i ) << " is not derived from Core::Widgets::PreferencesPage";
			throw Core::Exceptions::Exception( "PreferencesDialog::UpdateTreebook", stream.str().c_str( ) );
		}

		if ( baseWindowFactory->FindFactory( page->GetFactoryName() ).IsNull() )
		{
			winList.push_back( dynamic_cast<wxWindow*> (page) );
		}
	}

	std::list<wxWindow*>::iterator itWin;
	for ( itWin = winList.begin() ; itWin != winList.end() ; itWin++ )
	{
		for ( size_t i = 0 ; i < m_PreferencesWindow->GetPageCount( ) ; i++ )
		{
			if ( m_PreferencesWindow->GetPage( i ) == *itWin )
			{
				m_PreferencesWindow->RemovePage( i );
				(*itWin)->Destroy( );
				break;
			}
		}
	}
}

void Core::Widgets::PreferencesDialog::OnOK( wxCommandEvent &event )
{
	Core::Widgets::PreferencesPage* page;
	for ( size_t i = 0 ; i < m_PreferencesWindow->GetPageCount( ) ; i++ )
	{
		page = dynamic_cast<Core::Widgets::PreferencesPage*> ( m_PreferencesWindow->GetPage( i ) );
		if ( page )
		{
			page->UpdateData();
		}
	}

	event.Skip();
}

void Core::Widgets::PreferencesDialog::OnCancel( wxCommandEvent &event )
{
	event.Skip();
}

bool Core::Widgets::PreferencesDialog::Show( bool show /*= true */ )
{
	if ( show )
	{
		CreatePreferencesWindow( );

		// Update widgets
		Core::Widgets::PreferencesPage* page;
		for ( size_t i = 0 ; i < m_PreferencesWindow->GetPageCount( ) ; i++ )
		{
			page = dynamic_cast<Core::Widgets::PreferencesPage*> ( m_PreferencesWindow->GetPage( i ) );
			if ( page )
			{
				page->UpdateWidget();
			}
		}
	}

	return corePreferencesDialogUI::Show( show );
}

void Core::Widgets::PreferencesDialog::OnApply( wxCommandEvent &event )
{
	Core::Widgets::PreferencesPage* page;
	for ( size_t i = 0 ; i < m_PreferencesWindow->GetPageCount( ) ; i++ )
	{
		page = dynamic_cast<Core::Widgets::PreferencesPage*> ( m_PreferencesWindow->GetPage( i ) );
		if ( page )
		{
			page->UpdateData();
		}
	}

	event.Skip();
}

wxTreebook* Core::Widgets::PreferencesDialog::GetPreferencesWindow() const
{
	return m_PreferencesWindow;
}

PreferencesDialog::~PreferencesDialog(void)
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	if ( graphicalIface.IsNull( ) )
	{
		return;
	}

	BaseWindowFactories::Pointer baseWindowFactory;
	baseWindowFactory = graphicalIface->GetBaseWindowFactory( );
	if ( baseWindowFactory.IsNotNull( ) )
	{
		baseWindowFactory->GetFactoriesHolder()->RemoveObserver(
			this,
			&PreferencesDialog::UpdateTreebook );
	}
}

void Core::Widgets::PreferencesDialog::CreatePreferencesWindow( )
{
	// Create wxTreebook
	if ( m_PreferencesWindow == NULL )
	{
		m_PreferencesWindow = new wxTreebook( this, wxID( "PreferencesDialogTreebook" ) );

		UpdateTreebook( );

		GetSizer()->Insert( 0, m_PreferencesWindow, 
			wxSizerFlags( ).Proportion( 1 ).Expand().Border(wxALL, 5) );

		// Invokes the constraint-based layout algorithm or the sizer-based algorithm for this window.
		Layout();
		// Sizes the window so that it fits around its subwindows
	}
}
