/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _corePreferencesDialog_H
#define _corePreferencesDialog_H

#include "gmWidgetsWin32Header.h"
#include "coreObject.h"
#include <wx/treebook.h>
#include "corePreferencesDialogUI.h"

namespace Core
{
namespace Widgets
{
/** 
\brief Select the plugins to load.

\ingroup gmWidgets
\author Xavi Planes
\date Nov 2010
*/
class GMWIDGETS_EXPORT PreferencesDialog : public corePreferencesDialogUI
{
public:
	//!
	PreferencesDialog(
		wxWindow* parent, int id, const wxString& title, 
		const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, 
		long style=wxDEFAULT_DIALOG_STYLE);

	//!
	~PreferencesDialog(void);

	//! 
	bool Show(bool show = true );

	//!
	void CreatePreferencesWindow( );

	//!
	wxTreebook* GetPreferencesWindow() const;

protected:

	//!
	void UpdateTreebook( );

	//!
	void OnOK(wxCommandEvent &event);
	void OnCancel(wxCommandEvent &event);
	void OnApply(wxCommandEvent &event);

private:
	//!
	wxTreebook* m_PreferencesWindow;
};
}
}

#endif // _corePreferencesDialog_H
