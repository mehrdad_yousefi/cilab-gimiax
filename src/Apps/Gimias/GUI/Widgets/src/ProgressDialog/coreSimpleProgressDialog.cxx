/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/
// For compilers that don't support precompilation, include "wx/wx.h"
#include "wx/wxprec.h"

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include "coreSimpleProgressDialog.h"
#include "coreKernel.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreAssert.h"
#include "coreReportExceptionMacros.h"
#include <wx/listctrl.h>

#define wxID_ListControl	(1 + wxID_HIGHEST)

using namespace Core::Widgets;

//!
SimpleProgressDialog::SimpleProgressDialog(
	wxWindow* parent, int id, const wxString& title, const wxPoint& pos, const wxSize& size, long style)
: coreSimpleProgressFrameUI(parent, id, title, pos, size, style)
{
}

//!
SimpleProgressDialog::~SimpleProgressDialog(void)
{

}

