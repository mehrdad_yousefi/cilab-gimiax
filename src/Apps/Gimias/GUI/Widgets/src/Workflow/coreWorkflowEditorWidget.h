/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreWorkflowEditorWidget_H
#define _coreWorkflowEditorWidget_H

#include "coreWorkflowEditorWidgetUI.h"
#include "coreWorkflow.h"

namespace Core
{

namespace Widgets
{

/** 
\brief Widget to edit a Workflow
\ingroup gmWidgets
\author Xavi Planes
\date 1 June 2010
*/
class WorkflowEditorWidget: public coreWorkflowEditorWidgetUI
{
public:
	//!
	WorkflowEditorWidget(wxWindow* parent, int id);

	//!
	void SetWorkflow( Core::Workflow::Pointer workflow );

	//!
	bool Show(bool show = true );

private:

    wxDECLARE_EVENT_TABLE();

	//!
	void OnBeginDragTreeRegisteredWindows(wxTreeEvent& event);

	//!
	void OnBeginDragTreeWorkflow(wxTreeEvent& event);

	//!
	void UpdateRegisteredWindows( );

	//!
	void OnOk(wxCommandEvent &event);

	//!
	void OnCancel(wxCommandEvent& event);

	//!
    void OnSelectExtraPlugins(wxCommandEvent &event);

	//!
	void UpdateWidget( );

	//!
	std::string GetList( const Core::Workflow::PluginNamesListType &list );

	//!
	Core::Workflow::PluginNamesListType GetList( const std::string &list );
protected:

};

} // namespace Core
} // namespace Widgets

#endif // _coreWorkflowEditorWidget_H
