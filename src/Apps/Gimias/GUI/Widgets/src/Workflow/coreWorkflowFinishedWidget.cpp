/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreWorkflowFinishedWidget.h"


DEFINE_EVENT_TYPE(wxEVT_WORKFLOW_FINISHED);


Core::Widgets::WorkflowFinishedWidget::WorkflowFinishedWidget(wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    coreWorkflowFinishedWidgetUI(parent, id, pos, size, style)
{
	m_Processor = Processor::New();
}

Core::Widgets::WorkflowFinishedWidget::~WorkflowFinishedWidget()
{

}

void Core::Widgets::WorkflowFinishedWidget::OnOK( wxCommandEvent &event )
{
	wxCommandEvent evt( wxEVT_WORKFLOW_FINISHED, wxID_ANY );
  wxPostEvent(wxTheApp->GetTopWindow()->GetEventHandler(), evt);
}

void Core::Widgets::WorkflowFinishedWidget::ConnectProcessor( Core::BaseWindow* baseWindow )
{
	if ( baseWindow == NULL || baseWindow->GetProcessor().IsNull() )
	{
		return;
	}

	Core::DataContainer::Pointer dataContainer = Core::Runtime::Kernel::GetDataContainer();
	Core::DataEntityList::Pointer list = dataContainer->GetDataEntityList();

	m_Processor->SetNumberOfOutputs( baseWindow->GetProcessor()->GetNumberOfOutputs() );

	std::list<Core::DataEntity::Pointer> discardedList;
	for (size_t i = 0 ; i < m_Processor->GetNumberOfOutputs() ; i++ )
	{
		Core::DataEntity::Pointer dataEntity;
		dataEntity = baseWindow->GetProcessor()->GetOutputDataEntity( i );

		// If DataEntity is in the DataEntityList, use it
		if ( dataEntity.IsNotNull( ) && list->Find( dataEntity->GetId() ) != list->End( ) )
		{
			m_Processor->GetOutputPort( i )->SetDataEntity( dataEntity );
		}
		else
		{
			m_Processor->GetOutputPort( i )->SetDataEntity( NULL );
		}
	}
}

Core::BaseProcessor::Pointer Core::Widgets::WorkflowFinishedWidget::GetProcessor()
{
	return m_Processor.GetPointer( );
}


