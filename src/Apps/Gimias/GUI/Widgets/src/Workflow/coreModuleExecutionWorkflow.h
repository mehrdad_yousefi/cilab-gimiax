/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _ModuleExecutionWorkflow_H
#define _ModuleExecutionWorkflow_H

#include "gmProcessorsWin32Header.h"
#include "dynModuleExecutionImpl.h"
#include "ModuleDescription.h"
#include "coreCreateExceptionMacros.h"

namespace Core
{
	class WxGenericEvtHandler;
	class WxWaitEvtHandler;
}

//!
coreCreateRuntimeExceptionClassMacro( ModuleExecutionWorkflowAbortExcetion, "Process aborted", GMWIDGETS_EXPORT );


/**
Executes a ModuleDescription using a Processor

\ingroup gmKernel
\author Xavi Planes
\date Oct 2011
*/
class GMWIDGETS_EXPORT ModuleExecutionWorkflow : public dynModuleExecutionImpl
{
public:
	typedef ModuleExecutionWorkflow Self;
	typedef blSmartPointer<Self> Pointer;
	blNewMacro(Self);
	defineModuleFactory( ModuleExecutionWorkflow );
	virtual dynModuleExecutionImpl::Pointer CreateAnother(void) const
	{
		dynModuleExecutionImpl::Pointer smartPtr;
		smartPtr = New().GetPointer();
		return smartPtr;
	}

	//!
	virtual void Update( );

protected:
	//! Set a parameter value to m_Filter
	virtual void SetParameterValue( ModuleParameter *param );

	//! Get a parameter value from m_Filter
	virtual void GetParameterValue( ModuleParameter *param );

	//!
	ModuleExecutionWorkflow( );

	//!
	virtual ~ModuleExecutionWorkflow( );

	//! Activate workflow, wait for finished and deactivate workflow
	void UpdateInternal();

	/** Execute a function sending an event to the main GUI thread
	\param [in] checkAbort if it's true, check abort value. Throw an exception
	of type ModuleExecutionWorkflowAbortExcetion if process is aborted
	*/
	void ExecuteFunction( boost::function0<void> val, bool checkAbort );

	//!
	void ActivateWorkflow( );

	//! Set parameters of the processors of the workflow
	void SetWorkflowParameters( );

	//!
	void PublishInputs();

	//!
	void DeActivateWorkflow( );

	//!
	void RetrieveOutputDataEntities( );

	//!
	void WaitWorkflowFinishedEvent( );

	//! We need to process pending events before pop is called
	void PopEventHandlers( );

	//! Get processor of current workflow navigation step
	Core::BaseProcessor::Pointer GetCurrentWorkflowProcessor( );

private:
	//!
	Core::WxGenericEvtHandler* m_WxGenericEvtHandler;
	//!
	Core::WxWaitEvtHandler* m_WxWaitEvtHandler;

};

#endif // _ModuleExecutionWorkflow_H

