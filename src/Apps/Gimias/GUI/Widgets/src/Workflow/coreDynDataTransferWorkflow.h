/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreDynDataTransferWorkflow_H
#define _coreDynDataTransferWorkflow_H

// CoreLib
#include "gmProcessorsWin32Header.h"
#include "coreDynDataTransferBase.h"
#include "dynModuleExecution.h"
#include "coreDynDataTransferProcessor.h"

namespace Core{

/**
\brief Transfer data for Processor execution.

The inputs/outputs passed to the Processor through DataEntity.

\ingroup gmKernel
\author Xavi Planes
\date 14 07 2010
*/
class GMWIDGETS_EXPORT DynDataTransferWorkflow : public Core::DynDataTransferProcessor
{
public:
	//!
	coreDeclareSmartPointerClassMacro(Core::DynDataTransferWorkflow, Core::DynDataTransferBase);
	coreDefineFactoryClass( DynDataTransferWorkflow )
	coreDefineFactoryTagsBegin( )
	coreDefineFactoryAddTag( "ModuleType", std::string( "WorkflowModule" ) )
	coreDefineFactoryTagsEnd( )
	coreDefineFactoryClassEnd( )

protected:
	//!
	DynDataTransferWorkflow(void);

	//!
	virtual ~DynDataTransferWorkflow(void);

private:
	//!
	DynDataTransferWorkflow( const Self& );

	//!
	void operator=(const Self&);

protected:
};

} // namespace Core{


#endif // _coreDynDataTransferBaseCLP_H


