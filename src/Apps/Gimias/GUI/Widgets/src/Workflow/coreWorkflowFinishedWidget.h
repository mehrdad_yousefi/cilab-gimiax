/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreWorkflowFinishedWidgetUI_H
#define _coreWorkflowFinishedWidgetUI_H

#include "coreWorkflowFinishedWidgetUI.h"

BEGIN_DECLARE_EVENT_TYPES()
	DECLARE_EXPORTED_EVENT_TYPE(GMWIDGETS_EXPORT,wxEVT_WORKFLOW_FINISHED, -1)
END_DECLARE_EVENT_TYPES()

namespace Core{
namespace Widgets{

/** 
\brief Widget that appears at the end of a workflow
\ingroup gmWidgets
\author Xavi Planes
\date Oct 2011
*/
class GMWIDGETS_EXPORT WorkflowFinishedWidget : 
	public coreWorkflowFinishedWidgetUI, 
	public Core::BaseWindow {

	/**
	Empty processor to manager the inputs easily
	*/
	class Processor : public BaseProcessor
	{
	public:
		coreDeclareSmartPointerClassMacro(Processor, Core::BaseProcessor);
		Processor( ){}
	};

public:
	//!
	coreDefineBaseWindowFactory( Core::Widgets::WorkflowFinishedWidget );

	//!
    WorkflowFinishedWidget(wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

	//!
	~WorkflowFinishedWidget( );

	//! Connect outputs to outputs
	virtual void ConnectProcessor( Core::BaseWindow* baseWindow );

	//!
	Core::BaseProcessor::Pointer GetProcessor( );

private:
	virtual void OnOK(wxCommandEvent &event);

protected:
	//!
	Processor::Pointer m_Processor;
};

} // Widgets
} // Core

#endif // _coreWorkflowFinishedWidgetUI_H
