/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreModuleExecutionWorkflow.h"
#include "dynModuleExecution.h"

#include "coreFactoryManager.h"
#include "coreBaseProcessor.h"
#include "coreModuleExecutionProcessor.h"
#include "coreWorkflowManager.h"
#include "coreSettings.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreWxMitkCoreMainWindow.h"
#include "coreMainMenu.h"
#include "coreWxGenericEvtHandler.h"
#include "corePluginProviderManager.h"
#include "coreDataTreeHelper.h"
#include "corePluginTab.h"
#include "coreWorkflowNavigationWidget.h"
#include "coreWorkflowFinishedWidget.h"
#include "coreWxWaitEvtHandler.h"

#include "dynModuleHelper.h"
#include "wxEventHandlerHelper.h"


//! \todo Move this funcion as member function of the class
bool CheckInputData( )
{
    Core::Widgets::BaseMainWindow* baseMain = Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow( );
    Core::Widgets::wxMitkCoreMainWindow* main = dynamic_cast<Core::Widgets::wxMitkCoreMainWindow*> ( baseMain );

    Core::RenderingTree::Pointer renderingTree;
    renderingTree = main->GetCurrentPluginTab( )->GetRenderingTreeManager()->GetActiveTree( );

    return !renderingTree->GetAllDataEntities( ).empty( );
}

void PushEventHandlers( wxEvtHandler* handler  )
{
	Core::Widgets::BaseMainWindow* baseMain = Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow( );
	Core::Widgets::wxMitkCoreMainWindow* main = dynamic_cast<Core::Widgets::wxMitkCoreMainWindow*> ( baseMain );

	wxPushBackEventHandler( main, handler );
}

ModuleExecutionWorkflow::ModuleExecutionWorkflow( )
{
	m_WxGenericEvtHandler = NULL;
	m_WxWaitEvtHandler = new Core::WxWaitEvtHandler( );

	// Send event handler to main GUI
	ExecuteFunction( boost::bind( &PushEventHandlers, m_WxWaitEvtHandler ), false );
}

ModuleExecutionWorkflow::~ModuleExecutionWorkflow()
{
	// Process pending events before destroying the handler
	ExecuteFunction( boost::bind( &ModuleExecutionWorkflow::PopEventHandlers, this ), false );

	delete m_WxWaitEvtHandler;
	m_WxWaitEvtHandler = NULL;
}

void ModuleExecutionWorkflow::Update()
{

	try
	{
		UpdateInternal( );
	}
	catch( Core::Exceptions::ModuleExecutionWorkflowAbortExcetion )
	{
		// Deactivate workflow
		ExecuteFunction( boost::bind( &ModuleExecutionWorkflow::DeActivateWorkflow, this ), false );
	}
	catch( ... )
	{
		// Deactivate workflow
		ExecuteFunction( boost::bind( &ModuleExecutionWorkflow::DeActivateWorkflow, this ), false );

		throw;
	}

}

void ModuleExecutionWorkflow::UpdateInternal()
{
	Core::Workflow::Pointer workflow;
	workflow = Core::Runtime::Kernel::GetWorkflowManager()->GetWorkflow( GetModule()->GetTitle() );
	if ( workflow.IsNull() )
	{
		std::stringstream stream;
		stream << "Cannot find Workflow: " << GetModule( )->GetTitle();
		throw Core::Exceptions::Exception("ModuleExecutionWorkflow::Update", 
			stream.str().c_str() );
	}

	ExecuteFunction( boost::bind( &ModuleExecutionWorkflow::ActivateWorkflow, this ), true );

	// Set workflow parameters
	ExecuteFunction( boost::bind( &ModuleExecutionWorkflow::SetWorkflowParameters, this ), true );

	// Send event to publish inputs
	int retries = 0;
	while ( !CheckInputData( ) && retries < 2 )
	{
		ExecuteFunction( boost::bind( &ModuleExecutionWorkflow::PublishInputs, this ), true );

		// If input is not loaded, wait 100 msec because on Linux, the render window
		// can take some time to be realized
		if ( !CheckInputData( ) )
		{
			wxThread::Sleep( 100 );
		}

		retries++;
	}

	// wait until user finishes workflow
	WaitWorkflowFinishedEvent( );

	// Get output data entity
	RetrieveOutputDataEntities();

	// Deactivate workflow
	ExecuteFunction( boost::bind( &ModuleExecutionWorkflow::DeActivateWorkflow, this ), true );
}

void ModuleExecutionWorkflow::ExecuteFunction( boost::function0<void> val, bool checkAbort )
{
	Core::WxGenericEvent event;
	event.SetFunction( val );
  // XXX mkarmona look for this event if posted inside main thread execution thread
  wxPostEvent(wxTheApp->GetTopWindow()->GetEventHandler(), event);

	if ( checkAbort )
	{
		while ( event.WaitTimeout( 1000 ) == wxSEMA_TIMEOUT )
		{
			// Retrieve information about abort
			GetUpdateCallback()->Modified();

			if (GetModule()->GetProcessInformation()->Abort)
			{
				GetModule()->GetProcessInformation()->Progress = 0;
				GetModule()->GetProcessInformation()->StageProgress =0;
				GetUpdateCallback()->Modified();
				throw Core::Exceptions::ModuleExecutionWorkflowAbortExcetion( "ModuleExecutionWorkflow::ExecuteFunction" );
			}
		}
	}
	else
	{
		event.Wait( );
	}
}
	
void ModuleExecutionWorkflow::SetParameterValue( ModuleParameter *param )
{

}

void ModuleExecutionWorkflow::GetParameterValue( ModuleParameter *param )
{

}

void ModuleExecutionWorkflow::ActivateWorkflow()
{
	// Set active workflow
	Core::Runtime::Kernel::GetWorkflowManager()->SetActiveWorkflow( GetModule()->GetTitle() );

	// Add web services plugin temporally
	Core::Workflow::Pointer workflow;
	workflow = Core::Runtime::Kernel::GetWorkflowManager()->GetWorkflow( GetModule()->GetTitle() );
	Core::Workflow::PluginNamesListType pluginsNames = workflow->GetPluginNamesList( );
	pluginsNames.push_back( "WebServicesPlugin" );
	workflow->SetPluginNamesList( pluginsNames );

	// Add last step of the workflow
	Core::WorkflowSubStep::Pointer subStep;
	subStep = Core::WorkflowSubStep::New( Core::Widgets::WorkflowFinishedWidget::Factory::GetNameClass() );
	(*workflow->GetStepVector().rbegin())->AddSubStep( subStep );

	// Change perspective
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();
	settings->SetPerspective( Core::Runtime::PERSPECTIVE_WORKFLOW );

	// Send event to Update UI
	Core::Widgets::BaseMainWindow* baseMain = Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow( );
	Core::Widgets::wxMitkCoreMainWindow* main = dynamic_cast<Core::Widgets::wxMitkCoreMainWindow*> ( baseMain );
	main->UpdatePluginConfiguration();
}

void ModuleExecutionWorkflow::DeActivateWorkflow()
{
	// Unload all data
	Core::Runtime::Kernel::GetDataContainer()->GetDataEntityList( )->RemoveAll( );

	// Remove web services plugin 
	Core::Workflow::Pointer workflow;
	workflow = Core::Runtime::Kernel::GetWorkflowManager()->GetWorkflow( GetModule()->GetTitle() );
	Core::Workflow::PluginNamesListType pluginsNames = workflow->GetPluginNamesList( );
	pluginsNames.remove( "WebServicesPlugin" );
	workflow->SetPluginNamesList( pluginsNames );

	// Remove last step
	Core::WorkflowStep::SubStepListType& subStepVector = (*workflow->GetStepVector().rbegin())->GetSubStepVector( );
	subStepVector.erase( --subStepVector.rbegin().base() );

	// Change perspective
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();
	settings->SetPerspective( Core::Runtime::PERSPECTIVE_PLUGIN );

	// Restore selected plugins from configuration
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	graphicalIface->GetPluginProviderManager()->LoadConfiguration( true );

	// Send event to Update UI
	Core::Widgets::BaseMainWindow* baseMain = Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow( );
	Core::Widgets::wxMitkCoreMainWindow* main = dynamic_cast<Core::Widgets::wxMitkCoreMainWindow*> ( baseMain );
	main->UpdatePluginConfiguration();
}

void ModuleExecutionWorkflow::SetWorkflowParameters( )
{
	try{
		Core::BaseProcessor::Pointer processor = GetCurrentWorkflowProcessor();

		// Set parameters
		blTagMap::Pointer parameters = ModuleExecutionProcessor::ModuleToTagMap( *GetModule() );
		processor->SetParameters( parameters );
	}
	catch(...)
	{
	}
}

void ModuleExecutionWorkflow::PublishInputs()
{
	// Retrieve inputs and add to DataEntityList sending an event to UI
	for ( int i = 0 ; i < dynModuleHelper::GetNumberOfInputs( GetModule() ) ; i++ )
	{
		ModuleParameter* param = dynModuleHelper::GetInput( GetModule(), i );

		// Retrieve input data entity
		int timeStep = 0;
		Core::DataEntity::Pointer dataEntity = ModuleExecutionProcessor::GetDataEntity(param->GetDefault(), timeStep);
		if (dataEntity.IsNull())
		{
			// not a valid pointer
			continue;
		}

		Core::Widgets::BaseMainWindow* baseMain = Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow( );
		Core::Widgets::wxMitkCoreMainWindow* main = dynamic_cast<Core::Widgets::wxMitkCoreMainWindow*> ( baseMain );

		Core::RenderingTree::Pointer renderingTree;
		renderingTree = main->GetCurrentPluginTab( )->GetRenderingTreeManager()->GetActiveTree( );

		Core::DataEntityHolder::Pointer holder = Core::DataEntityHolder::New( );
		holder->SetSubject( dataEntity );

		// If exception is thrown, remove the DataEntity and return
		try{
			Core::DataTreeHelper::PublishOutput( holder, renderingTree, true, true );
		}
		catch(...)
		{
			renderingTree->Remove( dataEntity, true );
			return;
		}
	}
}

Core::BaseProcessor::Pointer ModuleExecutionWorkflow::GetCurrentWorkflowProcessor( )
{
	Core::Widgets::BaseMainWindow* baseMain = Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow( );
	Core::Widgets::wxMitkCoreMainWindow* main = dynamic_cast<Core::Widgets::wxMitkCoreMainWindow*> ( baseMain );
	Core::BasePluginTab* basePluginTab = main->GetCurrentPluginTab();
	Core::Widgets::PluginTab* pluginTab = dynamic_cast<Core::Widgets::PluginTab*> ( basePluginTab );
	wxWindow* win = pluginTab->GetWindow( "Workflow Navigation" );
	Core::Widgets::WorkflowNavigationWidget* navigation;
	navigation = dynamic_cast<Core::Widgets::WorkflowNavigationWidget*> ( win );
	if ( !navigation || 
		 navigation->GetProcessingWidget() == NULL || 
		 navigation->GetProcessingWidget()->GetProcessor().IsNull() )
	{
		std::stringstream stream;
		stream << "Cannot find Processor for last step of the workflow";
		throw Core::Exceptions::Exception("ModuleExecutionWorkflow::RetrieveOutputDataEntities", 
			stream.str().c_str() );
	}

	Core::BaseProcessor::Pointer processor = navigation->GetProcessingWidget()->GetProcessor();
	return processor;
}

void ModuleExecutionWorkflow::RetrieveOutputDataEntities()
{
	Core::BaseProcessor::Pointer processor = GetCurrentWorkflowProcessor();

	// Update outputs
	int outputCount = 0;
	for ( size_t i = 0 ; i < dynModuleHelper::GetNumberOfOutputs( GetModule() ) ; i++ )
	{
		ModuleParameter* param = dynModuleHelper::GetOutput( GetModule(), i );
		if ( param->GetDefault() == "false" )
		{
			continue;
		}

		// Find the first output that is not NULL
		Core::DataEntity::Pointer outDataEntity;
		while ( outputCount < processor->GetNumberOfOutputs() &&
			    outDataEntity.IsNull() )
		{
			outDataEntity = processor->GetOutputDataEntity( outputCount );
			outputCount++;
		}

		if ( outDataEntity.IsNull( ) )
		{
			std::stringstream stream;
			stream << "Cannot find output data for last step of the workflow";
			throw Core::Exceptions::Exception("ModuleExecutionWorkflow::RetrieveOutputDataEntities", 
				stream.str().c_str() );
		}

		// Retrieve output data entity
		int timeStep = 0;
		Core::DataEntity::Pointer dataEntity = ModuleExecutionProcessor::GetDataEntity(param->GetDefault(), timeStep);
		if ( dataEntity.IsNull() )
		{
			// not a valid pointer
			continue;
		}

		dataEntity->Copy( outDataEntity );
	}
}

void ModuleExecutionWorkflow::WaitWorkflowFinishedEvent()
{
	m_WxWaitEvtHandler->SetEventType( wxEVT_WORKFLOW_FINISHED );
	m_WxWaitEvtHandler->Connect();
	while ( m_WxWaitEvtHandler->WaitTimeout( 1000 ) == wxSEMA_TIMEOUT )
	{
		// Retrieve information about abort
		GetUpdateCallback()->Modified();

		if (GetModule()->GetProcessInformation()->Abort)
		{
			GetModule()->GetProcessInformation()->Progress = 0;
			GetModule()->GetProcessInformation()->StageProgress =0;
			GetUpdateCallback()->Modified();
			throw Core::Exceptions::ModuleExecutionWorkflowAbortExcetion( "ModuleExecutionWorkflow::ExecuteFunction" );
		}
	}
}

void ModuleExecutionWorkflow::PopEventHandlers( )
{
	Core::Widgets::BaseMainWindow* baseMain = Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow( );
	Core::Widgets::wxMitkCoreMainWindow* main = dynamic_cast<Core::Widgets::wxMitkCoreMainWindow*> ( baseMain );

	wxPopEventHandler( main, m_WxWaitEvtHandler );
}

