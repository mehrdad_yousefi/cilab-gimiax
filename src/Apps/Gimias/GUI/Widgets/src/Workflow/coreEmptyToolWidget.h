/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreEmptyTool_H
#define _coreEmptyTool_H


#include "coreProcessingWidget.h"
#include "coreEmptyToolWidgetUI.h"

namespace Core
{
namespace Widgets
{

/** 
\brief Empty tool to create visualization workflows.

\ingroup gmWidgets
\author Xavi Planes
\date May 2012
*/
class EmptyToolWidget: 
	public coreEmptyToolWidgetUI,
	public Core::Widgets::ProcessingWidget
{
public:
	//!
	coreDefineBaseWindowFactory( Core::Widgets::EmptyToolWidget );

	//!
    EmptyToolWidget(wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

	//!
	~EmptyToolWidget( );

private:

protected:
};


}
}

#endif // _coreEmptyTool_H
