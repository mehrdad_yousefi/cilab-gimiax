/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreWorkflowEditorWidget.h"
#include "coreBaseWindowFactories.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreWorkflowManager.h"
#include "coreWorkflowTreeItemData.h"
#include "corePluginTree.h"
#include "blTextUtils.h"

BEGIN_EVENT_TABLE(Core::Widgets::WorkflowEditorWidget, coreWorkflowEditorWidgetUI)
    EVT_TREE_BEGIN_DRAG( wxID_TREE_REGISTERED_WINDOWS, Core::Widgets::WorkflowEditorWidget::OnBeginDragTreeRegisteredWindows)
	EVT_TREE_BEGIN_DRAG( wxID_TREE_WORKFLOW, Core::Widgets::WorkflowEditorWidget::OnBeginDragTreeWorkflow)
END_EVENT_TABLE()

class PluginSelectorTree : public Core::Widgets::PluginTree
{
public:
	//!
	PluginSelectorTree(
		wxWindow* parent, 
		int id  = wxID_ANY, 
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=0)
		: Core::Widgets::PluginTree( parent, id, pos, size, style )
	{
	}

	//!
	~PluginSelectorTree(void)
	{
	}

	//! Check if a plugin is selected and refresh tree item state
	virtual bool CheckPluginSelected( 
		Core::Runtime::PluginProvider::Pointer provider, 
		blTagMap::Pointer plugins, const std::string &caption )
	{
		std::string pluginName = provider->GetPluginName( caption );
		Core::Workflow::PluginNamesListType::iterator it;
		it = std::find( m_PluginsList.begin( ), m_PluginsList.end( ), pluginName );
		return it != m_PluginsList.end( );
	}

	//! Refresh data with value of tree item
	virtual void SetPluginSelected( 
		Core::Runtime::PluginProvider::Pointer provider, 
		blTagMap::Pointer plugins, const std::string &caption, bool val )
	{
		bool found = CheckPluginSelected( provider, plugins, caption );
		if ( val == found )
			return;

		std::string pluginName = provider->GetPluginName( caption );

		if ( val )
		{
			m_PluginsList.push_back( pluginName );
		}
		else 
		{
			m_PluginsList.remove( pluginName );
		}
	}

	//!
	Core::Workflow::PluginNamesListType GetPluginsList(  )
	{
		return m_PluginsList;
	}


	//!
	void SetPluginsList( Core::Workflow::PluginNamesListType &list )
	{
		m_PluginsList = list;
	}
public:
	//! Plugins names
	Core::Workflow::PluginNamesListType m_PluginsList;
};

class PluginSelectorDialog : public wxDialog
{
	public:
	//!
	PluginSelectorDialog(
		wxWindow* parent, 
		int id  = wxID_ANY, 
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=wxDEFAULT_DIALOG_STYLE)
		: wxDialog( parent, id, "", pos, size, style )
	{
		m_Tree  = new PluginSelectorTree(this,  
			wxID_ANY, 
			wxDefaultPosition, 
			wxSize(200, 300), 
			wxTR_HAS_BUTTONS|wxTR_LINES_AT_ROOT|wxTR_DEFAULT_STYLE|wxSUNKEN_BORDER
			);
		m_Tree->UpdateWidget( );
		m_btnOK = new wxButton(this, wxID_OK, wxT("OK"));
		m_btnCancel = new wxButton(this, wxID_CANCEL, wxT("Cancel"));

		wxBoxSizer* sizer_1 = new wxBoxSizer(wxVERTICAL);
	    wxBoxSizer* sizer_2 = new wxBoxSizer(wxHORIZONTAL);
		sizer_2->Add(m_btnOK, 0, wxALL, 5);
		sizer_2->Add(m_btnCancel, 0, wxALL, 5);
		sizer_1->Add(m_Tree, 1, wxALL|wxEXPAND, 5);
	    sizer_1->Add(sizer_2, 0, wxALL|wxALIGN_RIGHT, 5);
		SetSizer(sizer_1);
		Layout();

	}

	//!
	~PluginSelectorDialog(void)
	{
	}

	//!
	PluginSelectorTree* GetTree( )
	{
		return m_Tree;
	}

private:
	PluginSelectorTree* m_Tree;
    wxButton* m_btnOK;
    wxButton* m_btnCancel;
};

Core::Widgets::WorkflowEditorWidget::WorkflowEditorWidget( wxWindow* parent, int id )
	: coreWorkflowEditorWidgetUI( parent, id, "Workflow Editor" )
{
	UpdateRegisteredWindows( );
}

void Core::Widgets::WorkflowEditorWidget::SetWorkflow( Core::Workflow::Pointer workflow )
{
	m_TreeWorkflow->SetWorkflow( workflow );
	UpdateWidget( );
}

void Core::Widgets::WorkflowEditorWidget::UpdateRegisteredWindows()
{
	m_TreeRegisteredWindows->UpdateRegisteredWindows();
}

void Core::Widgets::WorkflowEditorWidget::OnBeginDragTreeRegisteredWindows( wxTreeEvent& event )
{
	try
	{

		// need to explicitly allow drag
		wxTextDataObject data( m_TreeRegisteredWindows->GetItemText( event.GetItem() ) );
		wxDropSource dragSource( this );
		dragSource.SetData( data );
		wxDragResult result = dragSource.DoDragDrop( wxDrag_DefaultMove );

		if ( result == wxDragMove )
		{
			wxArrayTreeItemIds selection;
			m_TreeRegisteredWindows->GetSelections( selection );
			std::list<std::string> items;
			for ( size_t i = 0 ; i < selection.size() ; i++ )
			{
				items.push_back( m_TreeRegisteredWindows->GetItemFactoryName( selection[ i ] ) );
			}

			m_TreeWorkflow->AddItems( items );

			m_TreeWorkflow->UpdateWorkflow();

			UpdateWidget( );
		}

	}
	coreCatchExceptionsReportAndNoThrowMacro( WorkflowEditorWidget::OnBeginDragTreeRegisteredWindows )
}

void Core::Widgets::WorkflowEditorWidget::OnBeginDragTreeWorkflow( wxTreeEvent& event )
{
	try
	{

		// need to explicitly allow drag
		if ( event.GetItem() == m_TreeWorkflow->GetRootItem() )
		{
			return;
		}

		wxArrayTreeItemIds selection;
		m_TreeWorkflow->GetSelections( selection );
		m_TreeWorkflow->SetRestrictedSelection( true );


		wxTextDataObject textData( m_TreeWorkflow->GetItemFactoryName( event.GetItem() ) );
		wxDropSource dragSource( this );
		dragSource.SetData( textData );
		wxDragResult result = dragSource.DoDragDrop( wxDrag_DefaultMove );

		if ( result == wxDragNone )
		{
			m_TreeWorkflow->DeleteItems( selection );
		}
		else 
		{
			wxArrayTreeItemIds newSelection;
			m_TreeWorkflow->GetSelections( newSelection );
			m_TreeWorkflow->MoveItems( selection, newSelection );
		}

		m_TreeWorkflow->SetRestrictedSelection( false );

	}
	coreCatchExceptionsReportAndNoThrowMacro( WorkflowEditorWidget::OnBeginDragTreeWorkflow )

}

void Core::Widgets::WorkflowEditorWidget::OnOk( wxCommandEvent &event )
{
	try
	{

		m_TreeWorkflow->UpdateWorkflow();

		// Save all workflows
		Core::Runtime::Kernel::GetWorkflowManager()->SaveAll( );

		if ( GetParent() != NULL )
		{
			GetParent()->Show( );
		}

		// Update list of extra pluins
		m_TreeWorkflow->GetWorkflow( )->SetExtraPluginNamesList( 
			GetList( m_txtExtraPlugins->GetValue( ).ToStdString() ) );

		event.Skip();

	}
	coreCatchExceptionsReportAndNoThrowMacro( WorkflowEditorWidget::OnApply )
}

bool Core::Widgets::WorkflowEditorWidget::Show( bool show /*= true */ )
{
	if ( show )
	{
		UpdateRegisteredWindows();
	}

	return coreWorkflowEditorWidgetUI::Show( show );
}

//!
void Core::Widgets::WorkflowEditorWidget::OnCancel(wxCommandEvent& event)
{
	try
	{

		if ( GetParent() != NULL )
		{
			GetParent()->Show( );
		}

		event.Skip();

	}
	coreCatchExceptionsReportAndNoThrowMacro( WorkflowEditorWidget::OnCancel )

}

void Core::Widgets::WorkflowEditorWidget::UpdateWidget( )
{
	if ( m_TreeWorkflow->GetWorkflow( ).IsNull( ) )
	{
		return;
	}

	// Check that all plugins are loaded
	Core::Workflow::PluginNamesListType requiredPlugins;
	Core::Workflow::PluginNamesListType notLoadedPlugins;
	requiredPlugins = m_TreeWorkflow->GetWorkflow( )->GetPluginNamesList( );
	notLoadedPlugins = requiredPlugins;

	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	Core::Runtime::PluginProviderManager::PluginProvidersType pluginProviers;
	pluginProviers = graphicalIface->GetPluginProviderManager()->GetPluginProviders( );

	// Iterate over all plugin providers
	Core::Workflow::PluginNamesListType::iterator itPlugin;
	for ( itPlugin = requiredPlugins.begin( ) ; itPlugin != requiredPlugins.end( ) ; itPlugin++ )
	{
		Core::Runtime::PluginProviderManager::PluginProvidersType::iterator it;
		for ( it = pluginProviers.begin( ) ; it != pluginProviers.end( ) ; it++ )
		{
			// Get names of loaded plugins
			std::list<std::string> loadedPlugins = (*it)->GetLoadedPlugins( );
			std::list<std::string>::iterator itFound;
			itFound = std::find( loadedPlugins.begin( ), loadedPlugins.end( ), *itPlugin );
			if ( itFound != loadedPlugins.end( ) )
			{
				notLoadedPlugins.remove( *itPlugin );
			}
		}
	}

	// If it's not empty, throw exception
	if ( !notLoadedPlugins.empty( ) )
	{
		std::stringstream sstream;
		sstream << "You cannot edit this workflow because you need to load the required plugins: " << std::endl;
		Core::Workflow::PluginNamesListType::iterator itPlugin;
		for ( itPlugin = notLoadedPlugins.begin( ) ; itPlugin != notLoadedPlugins.end( ) ; itPlugin++ )
		{
			sstream << *itPlugin << std::endl;
		}
		throw Core::Exceptions::Exception( "WorkflowEditorWidget::UpdateWidget", sstream.str( ).c_str( ) );
	}

	// Update list of required plugins
	m_txtRequiredPlugins->SetValue( GetList( m_TreeWorkflow->GetWorkflow( )->GetPluginNamesList( ) ) );

	// Update list of extra pluins
	m_txtExtraPlugins->SetValue( GetList( m_TreeWorkflow->GetWorkflow( )->GetExtraPluginNamesList( ) ) );
}

std::string Core::Widgets::WorkflowEditorWidget::GetList( 
	const Core::Workflow::PluginNamesListType &list )
{
	Core::Workflow::PluginNamesListType::const_iterator it = list.begin( );
	std::stringstream stream;
	while ( it != list.end( ) )
	{
		stream << *it;
		it++;
		if ( it != list.end( ) )
		{
			 stream << ",";
		}
	}

	return stream.str( );
}

Core::Workflow::PluginNamesListType 
Core::Widgets::WorkflowEditorWidget::GetList( const std::string &list )
{
	Core::Workflow::PluginNamesListType plugins;
	blTextUtils::ParseLine( list, ',', plugins );
	return plugins;
}

void Core::Widgets::WorkflowEditorWidget::OnSelectExtraPlugins(wxCommandEvent &event)
{
	PluginSelectorDialog dlg( this, wxID_ANY );
	dlg.SetSize( 400, 400 );

	// Convert plugin names to plugin captions
	Core::Workflow::PluginNamesListType pluginsList = GetList( m_txtExtraPlugins->GetValue( ).ToStdString() );
	dlg.GetTree( )->SetPluginsList( pluginsList );
	dlg.GetTree( )->UpdateWidget( );
	if ( dlg.ShowModal( ) == wxID_OK )
	{
		// Convert plugin captions to plugin names
		m_txtExtraPlugins->SetValue( GetList( dlg.GetTree( )->GetPluginsList( ) ) );
	}
}
