/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreWidgetCollective.h"

#include "wxID.h"

#include "coreWxMitkCoreMainWindow.h"
#include "coreKernel.h"
#include "corePluginTabFactory.h"
#include "coreRenderingTree.h"
#include "coreProcessorWidgetsBuilder.h"
#include "coreWxMitkGraphicalInterface.h"

Core::WidgetCollective::WidgetCollective( )
{
}

Core::Widgets::RenderWindowBase *Core::WidgetCollective::GetActiveMultiRenderWindow()
{
	if ( GetPluginTab() == NULL )
	{
		return NULL;
	}

	Core::Widgets::RenderWindowBase *multiRenderWindow;
	multiRenderWindow = GetPluginTab( )->GetWorkingAreaManager()->GetActiveMultiRenderWindow( );
	return multiRenderWindow;
}

Core::Widgets::PluginTab* Core::WidgetCollective::GetPluginTab() const
{
	BasePluginTab* basePluginTab = Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow()->GetLastPluginTab( );
	return dynamic_cast<Core::Widgets::PluginTab*> ( basePluginTab );
}

