/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreWidgetCollective_H
#define coreWidgetCollective_H

#include "gmWidgetsWin32Header.h"
#include "coreObject.h"
#include "coreBaseWindow.h"

namespace Core {

/**
Accessibility functions

\ingroup gmWidget
\author Xavi Planes
\date 16 july 2009
*/

class GMWIDGETS_EXPORT WidgetCollective : public Core::SmartPointerObject
{
public:
	//!
	coreDeclareSmartPointerClassMacro(
		Core::WidgetCollective, 
		Core::SmartPointerObject );

protected:
	WidgetCollective( );

	//!
	Core::Widgets::RenderWindowBase *GetActiveMultiRenderWindow( );

	//!
	Core::Widgets::PluginTab* GetPluginTab() const;


protected:
};

} // Core

#endif //coreWidgetCollective_H
