/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "corePluginTree.h"

#include "coreStringHelper.h"

using namespace Core::Widgets;

BEGIN_EVENT_TABLE(PluginTree, blwxTreeCtrl)
	EVT_TREE_STATE_IMAGE_CLICK( wxID_ANY, PluginTree::OnCheckBoxItemToggle)
END_EVENT_TABLE()

class PluginData : public wxTreeItemData
{
public:
	PluginData( const std::string &pluginName )
	{
		m_PluginName = pluginName;
	}
	std::string m_PluginName;
};

Core::Widgets::PluginTree::PluginTree( 
	wxWindow* parent, 
	int id /*= wxID_ANY*/, 
	const wxPoint& pos/*=wxDefaultPosition*/, 
	const wxSize& size/*=wxDefaultSize*/, 
	long style/*=0*/ ) : blwxTreeCtrl( parent, id, pos, size, style )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	m_PluginProviderManager = graphicalIface->GetPluginProviderManager();

	m_Modified = false;
}

//!
PluginTree::~PluginTree(void)
{
}

void PluginTree::SetFilter( const std::string &val )
{
	m_Filter = val;
}

bool PluginTree::IsModified( )
{
	return m_Modified;
}

void PluginTree::SetModified( bool val )
{
	m_Modified = val;
}

void PluginTree::UpdateWidget(void)
{
	try
	{
		Core::Runtime::Settings::Pointer settings;
		settings = Core::Runtime::Kernel::GetApplicationSettings();
		
		wxWindowUpdateLocker lock( this );

		DeleteAllItems( );
		AddRoot( "Plugins" );
		SetItemImage(GetRootItem(), 1);

		// Create a checkbox with the text of each available profile type
		wxTreeItemId parentItem;
		
		Core::Runtime::PluginProviderManager::PluginProvidersType pluginProviers;
		pluginProviers = m_PluginProviderManager->GetPluginProviders( );

		Core::Runtime::PluginProviderManager::PluginProvidersType::iterator itProvider;
		for ( itProvider = pluginProviers.begin( ) ; itProvider != pluginProviers.end( ) ; itProvider++ )
		{
			parentItem = AppendItem(GetRootItem( ), _U( (*itProvider)->GetName( ) ), -1, -1, 
				new PluginData( (*itProvider)->GetName( ), (*itProvider)->GetName( ), false ) );

			blTagMap::Pointer plugins;
			plugins = (*itProvider)->GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );

			bool selected = UpdateTree( *itProvider, parentItem, plugins, false );

			SetItemImage(parentItem, selected + 1 );

			Expand( parentItem );
		}

		Expand( GetRootItem( ) );

		if ( !m_Filter.empty( ) )
		{
			ExpandAll( );
		}

		// Count number of leafs
		int totalPlugins = 0;
		std::list<wxTreeItemId> pendingItems;
		pendingItems.push_back( GetRootItem( ) );
		while( !pendingItems.empty( ) )
		{
			wxTreeItemId currentItemId = pendingItems.front();
			pendingItems.pop_front( );

			// count item if it's a leaf without children
			if ( GetChildrenCount( currentItemId ) == 0 )
			{
				totalPlugins++;
			}

			// Iterate over all direct children items of current item and add them to the list
			wxTreeItemIdValue cookie;
			wxTreeItemId itemId = GetFirstChild( currentItemId, cookie );
			while ( itemId.IsOk() )
			{
				pendingItems.push_back( itemId );
				itemId = GetNextChild( currentItemId, cookie);
			}
		}
		SetItemText( GetRootItem( ), wxString::Format( "Plugins (%d)", totalPlugins ) );

		// Update scroll position
		SetScrollPos(wxVERTICAL,0); 

	}
	coreCatchExceptionsAddTraceAndThrowMacro(PluginTree::SetupProfileCheckboxes)
}

//!
void PluginTree::OnCheckBoxItemToggle(wxTreeEvent& event)
{
	try
	{
		wxTreeItemId clickedItemId = event.GetItem();

		int state = GetItemImage( clickedItemId );

		// If the item is a virtual plugin, update parent item
		PluginData* pluginData = dynamic_cast<PluginData*> ( GetItemData( clickedItemId ) );
		if ( pluginData->m_IsVirtualPlugin )
		{
			clickedItemId = GetItemParent( clickedItemId );
			SetItemImage( clickedItemId, state );
		}

		// If it's a category item -> Apply to all children
		std::list<wxTreeItemId> pendingItems;
		pendingItems.push_back( clickedItemId );

		// manage a list of pending items
		while( !pendingItems.empty( ) )
		{
			wxTreeItemId currentItemId = pendingItems.front();
			pendingItems.pop_front( );

			if ( currentItemId.IsOk() )
			{
				// Set state for all children of current item and add pending 
				// items to all children of these children
				wxTreeItemIdValue cookie;
				wxTreeItemId itemId = GetFirstChild( currentItemId, cookie );
				while ( itemId.IsOk() )
				{
					SetItemImage( itemId, state );
					pendingItems.push_back( itemId );
					itemId = GetNextChild( currentItemId, cookie);
				}
			}
			
		}

		// Update plugin provider configuration
		UpdateProvider( m_PluginProviderManager->GetPluginProvider( pluginData->m_ProviderName ), clickedItemId );

		m_Modified = true;

		event.Skip();
	}
	coreCatchExceptionsReportAndNoThrowMacro( PluginTree::OnCheckBoxItemToggle )

}

/**
 */
bool Core::Widgets::PluginTree::UpdateTree(
	Core::Runtime::PluginProvider::Pointer provider, 
	wxTreeItemId parentItem,
	blTagMap::Pointer plugins,
	bool isVirtualPlugin )
{
	bool allChildrenSelected = true;
	blTagMap::Iterator it;
	for( it = plugins->GetIteratorBegin(); it != plugins->GetIteratorEnd(); ++it)
	{
		std::string caption = it->second->GetName( );
		std::string pluginName = isVirtualPlugin ? caption : provider->GetPluginName( caption );

		// Check the filter
		bool filtered = m_Filter.empty( );
		std::string nameLowercase = Core::StringHelper::ToLowerCase( caption );
		std::string filterLowercase = Core::StringHelper::ToLowerCase( m_Filter.c_str( ) );
		size_t pos = nameLowercase.find( filterLowercase );
		if ( pos != std::string::npos )
		{
			filtered = true;
		}

		// Get selected and iterate recursivelly
		bool selected = false;
		wxTreeItemId  item;
		if ( it->second->GetValue().type() == typeid( blTagMap::Pointer ) )
		{
			item = AppendItem( parentItem, _U(caption), -1, -1, 
				new PluginData( pluginName, provider->GetName( ), isVirtualPlugin ) );
			selected = UpdateTree( provider, item, it->second->GetValueCasted<blTagMap::Pointer>( ), isVirtualPlugin );
			if ( GetChildrenCount( item ) == 0 && !filtered )
			{
				Delete( item );
			}
		}
		else 
		{
			item = AppendItem( parentItem, _U(caption), -1, -1, 
				new PluginData( pluginName, provider->GetName( ), isVirtualPlugin ));
			selected = CheckPluginSelected( provider, plugins, it->first );

			// Add virtual child plugins information
			if ( !isVirtualPlugin )
			{
				blTagMap::Pointer virtualPlugins = provider->GetVirtualChildPlugins( pluginName );
				if ( virtualPlugins->GetLength( ) > 0 )
					UpdateTree( provider, item, virtualPlugins, true );
			}

			// If no child item is filtered, and neither this one, delete item
			if ( GetChildrenCount( item ) == 0 && !filtered )
			{
				Delete( item );
			}
		}

		// Set checked state
		if ( item.IsOk( ) )
		{
			SetItemImage(item, selected + 1 );
		}

		// Return if all children are selected
		allChildrenSelected &= selected;
	}

	return allChildrenSelected;
}

void Core::Widgets::PluginTree::UpdateProvider( 
	Runtime::PluginProvider::Pointer provider, 
	wxTreeItemId item )
{
	blTagMap::Pointer plugins;
	plugins = provider->GetProperties()->GetTagValue<blTagMap::Pointer>( "Plugins" );

	// Get state of current item
	std::string itemTxt = _U(GetItemText(item));
	int state = GetItemImage( item );

	// Update tag value
	blTag::Pointer tag = plugins->FindTagByName( itemTxt, true );
	if ( tag.IsNotNull( ) && tag->GetValue().type() != typeid( blTagMap::Pointer ) )
	{
		bool checked = state == 2;
		SetPluginSelected( provider, plugins, itemTxt, checked );
	}

	// Set state for all children of current item 
	wxTreeItemIdValue cookie;
	wxTreeItemId childItemId = GetFirstChild( item, cookie );
	while ( childItemId.IsOk() )
	{
		UpdateProvider( provider, childItemId );
		childItemId = GetNextChild( item, cookie);
	}
}

bool Core::Widgets::PluginTree::CheckPluginSelected( 
	Core::Runtime::PluginProvider::Pointer provider, 
	blTagMap::Pointer plugins, const std::string &caption )
{
	blTag::Pointer tag = plugins->FindTagByName( caption, true );
	return tag->GetValueCasted<bool>( );
}

void Core::Widgets::PluginTree::SetPluginSelected( 
	Core::Runtime::PluginProvider::Pointer provider, 
	blTagMap::Pointer plugins, const std::string &caption, bool val )
{
	blTag::Pointer tag = plugins->FindTagByName( caption, true );
	if ( tag.IsNotNull( ) )
		tag->SetValue( val );
}
