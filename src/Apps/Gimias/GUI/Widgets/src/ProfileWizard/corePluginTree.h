/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef corePluginTree_H
#define corePluginTree_H

#include "gmWidgetsWin32Header.h"
#include "coreObject.h"
#include "blwxTreeCtrl.h"
#include "corePluginProviderManager.h"

namespace Core
{
namespace Widgets
{
/** 
\brief Tree of plugins

\ingroup gmWidgets
\author Xavi Planes
\date Nov 2012
*/
class GMWIDGETS_EXPORT PluginTree : public blwxTreeCtrl
{
public:

	//!
	PluginTree(
		wxWindow* parent, 
		int id  = wxID_ANY, 
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=0);

	//!
	~PluginTree(void);

	//!
	void SetFilter( const std::string &val );

	//! 
	void UpdateWidget( );

	//!
	bool IsModified( );

	//!
	void SetModified( bool val );

protected:
	//!
	void OnCheckBoxItemToggle(wxTreeEvent& event);

	//! Update tree state
	bool UpdateTree( 
		Core::Runtime::PluginProvider::Pointer provider, 
		wxTreeItemId parentItem, 
		blTagMap::Pointer plugins,
		bool isVirtualPlugin );

	/** Update profile state
	\param [in] unloadResources Unload registered resources
	*/
	void UpdateProvider( 
		Core::Runtime::PluginProvider::Pointer provider, 
		wxTreeItemId item );

	//! Check if a plugin is selected using the caption and refresh tree item state
	virtual bool CheckPluginSelected( 
		Core::Runtime::PluginProvider::Pointer provider, 
		blTagMap::Pointer plugins, const std::string &caption );

	//! Refresh data with value of tree item using the caption
	virtual void SetPluginSelected( 
		Core::Runtime::PluginProvider::Pointer provider, 
		blTagMap::Pointer plugins, const std::string &caption, bool val );

private:
    wxDECLARE_EVENT_TABLE();


private:
	//! Filter elements to be displayed
	std::string m_Filter;
	//!
	Runtime::PluginProviderManager::Pointer m_PluginProviderManager;
	//!
	bool m_Modified;
};

/** 
\brief Plugin Item data

\ingroup gmWidgets
\author Xavi Planes
\date Nov 2012
*/
class PluginData : public wxTreeItemData
{
public:
	PluginData( const std::string &pluginName, const std::string &providerName, bool isVirtualPlugin )
	{
		m_PluginName = pluginName;
		m_ProviderName = providerName;
		m_IsVirtualPlugin = isVirtualPlugin;
	}
	std::string m_PluginName;
	std::string m_ProviderName;
	bool m_IsVirtualPlugin;
};

}
}

#endif // corePluginTree_H
