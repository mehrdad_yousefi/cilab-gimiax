/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include <wx/wupdlock.h>

#include <blMitkUnicode.h>

#include "corePluginSelectorWidget.h"
#include "coreAssert.h"
#include "coreKernel.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreReportExceptionMacros.h"
#include "corePropertiesConfigurationDialog.h"
#include "coreFrontEndPluginProvider.h"
#include "coreBaseWindowFactorySearch.h"
#include "coreProcessorManager.h"
#include "coreWxGenericEvent.h"
#include "coreCompatibility.h"

#include "blwxTreeCtrl.h"
#include "blTextUtils.h"

using namespace Core::Widgets;

#define wxID_chkList wxID( "wxID_chkList" )
#define wxID_IMPORT_NEW_PLUGINS wxID("wxID_IMPORT_NEW_PLUGINS")
#define wxID_SCAN_PLUGINS wxID("wxID_SCAN_PLUGINS")
#define wxID_REMOVE_PROVIDER wxID( "wxID_REMOVE_PROVIDER" )
#define wxID_ADD_DEFAULT wxID("wxID_ADD_DEFAULT")
#define wxID_FILTER_TXT wxID("wxID_FILTER_TXT")
#define wxID_DESCRIPTION_TXT wxID("wxID_DESCRIPTION_TXT")

BEGIN_EVENT_TABLE(PluginSelectorWidget, wxPanel)
	EVT_TREE_SEL_CHANGED( wxID_chkList, PluginSelectorWidget::OnSelectionChanged)
	EVT_TREE_ITEM_RIGHT_CLICK( wxID_chkList, PluginSelectorWidget::OnRightMouseButtonClick)
	EVT_BUTTON(wxID_IMPORT_NEW_PLUGINS, PluginSelectorWidget::OnImportNewPlugins)
	EVT_MENU	(wxID_REMOVE_PROVIDER, PluginSelectorWidget::OnRemoveProvider)	
	EVT_MENU	(wxID_SCAN_PLUGINS, PluginSelectorWidget::OnScanPlugins)	
	EVT_MENU	(wxID_ADD_DEFAULT, PluginSelectorWidget::OnAddDefault)	
	EVT_MENU	(wxID_ANY, PluginSelectorWidget::OnMenu)	
	EVT_TEXT	(wxID_FILTER_TXT, PluginSelectorWidget::OnFilter)	
END_EVENT_TABLE()

Core::Widgets::PluginSelectorWidget::PluginSelectorWidget( 
	wxWindow* parent, 
	int id /*= wxID_ANY*/, 
	const wxPoint& pos/*=wxDefaultPosition*/, 
	const wxSize& size/*=wxDefaultSize*/, 
	long style/*=0*/ ) : wxPanel( parent, id, pos, size, style )
{
	m_ChkTree  = new PluginTree(this,  
		wxID_chkList, 
		wxDefaultPosition, 
		wxSize(200, 300), 
		wxTR_HAS_BUTTONS|wxTR_LINES_AT_ROOT|wxTR_DEFAULT_STYLE|wxSUNKEN_BORDER
		);
	m_btnImport = new wxButton(this, wxID_IMPORT_NEW_PLUGINS, wxT("Import new plugins"));
	m_lblFilter = new wxStaticText(this, wxID_ANY, wxT("Search"));
	m_txtFilter = new wxTextCtrl(this, wxID_FILTER_TXT );
	m_txtDescription = new wxTextCtrl(this, wxID_DESCRIPTION_TXT, "",
		wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_READONLY );
	m_OnlineDocumentation = new wxHyperlinkCtrl( this, wxID_ANY, 
			wxT( "Online Documentation" ), "" );
	m_OnlineDocumentation->Enable( false );
	m_bmpSnapshot = new wxStaticBitmap(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxDOUBLE_BORDER);
	m_bmpSnapshot->SetMinSize(wxSize(60, 60));

	m_EnableCallbackMessages = false;

	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	m_PluginProviderManager = graphicalIface->GetPluginProviderManager();
	m_PluginProviderManager->AddObserverOnModified( this, &PluginSelectorWidget::OnModifiedPluginProviderManager );
	OnModifiedPluginProviderManager( );

	m_PluginProviderManager->GetProvidersUpdater()->GetStatusHolder( )->AddObserver( 
		this, &PluginSelectorWidget::OnModifiedProvidersStatus );
	do_layout( );

}

PluginSelectorWidget::~PluginSelectorWidget(void)
{
	RemovePluginProviderObservers( );
}

void PluginSelectorWidget::do_layout(void)
{
	// layout
	wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer* sizer_5 = new wxBoxSizer(wxHORIZONTAL);
	sizer_5->Add(m_lblFilter, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5);
	sizer_5->Add(m_txtFilter, wxSizerFlags( ).Proportion(1).Expand() );
	sizer->Add( sizer_5, 0, wxEXPAND|wxALL, 5);
	sizer->Add(m_ChkTree, wxSizerFlags( ).Proportion(5).Expand().Border(wxALL,5) );
	
	wxBoxSizer* sizer_6 = new wxBoxSizer(wxHORIZONTAL);
	sizer_6->Add(m_bmpSnapshot, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5);
	sizer_6->Add(m_txtDescription, 1, wxEXPAND );
	sizer->Add( sizer_6, 0, wxEXPAND|wxALL, 5);
	
	sizer->Add(m_OnlineDocumentation, wxSizerFlags( ).Right() );
	wxBoxSizer* sizer_3 = new wxBoxSizer(wxHORIZONTAL);
	sizer_3->Add(m_btnImport, 0, wxALL|wxALIGN_RIGHT, 5);
	sizer->Add(sizer_3, 0, wxALIGN_RIGHT, 0);
	SetSizer(sizer);
}

void Core::Widgets::PluginSelectorWidget::UpdateData()
{
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();

	if ( m_ChkTree->IsModified( ) || settings->IsFirstTimeStart() )
	{

		// It was not canceled, so assign the profile to the current user profile
		m_PluginProviderManager->SaveConfiguration( );

		settings->SaveSettings();
		m_ChkTree->SetModified( false );

		settings->SetPerspective( Core::Runtime::PERSPECTIVE_PLUGIN );

		// Update new configuration
		Core::Runtime::Kernel::GetGraphicalInterface()->GetMainWindow( )->UpdatePluginConfiguration( );
	}
}

void Core::Widgets::PluginSelectorWidget::UpdateWidget()
{
	m_ChkTree->SetFilter( m_txtFilter->GetValue( ).ToStdString() );
	// Fills the page with one checkbox for each available profile, and checks them for user selected profiles
	m_ChkTree->UpdateWidget();
}

void Core::Widgets::PluginSelectorWidget::OnImportNewPlugins( wxCommandEvent &event )
{
	try{

		m_ProvidersMenu = new wxMenu( );

		Core::FactoryManager::FactoryListType factories;
		factories = FactoryManager::FindAll( Runtime::PluginProvider::GetNameClass() );
		Core::FactoryManager::FactoryListType::iterator it;
		for ( it = factories.begin() ; it != factories.end() ; it++ )
		{
			m_ProvidersMenu->Append(wxNewId(), (*it)->GetInstanceClassName() );
		}

		PopupMenu( m_ProvidersMenu );

		delete m_ProvidersMenu;
		m_ProvidersMenu = NULL;
	}
	coreCatchExceptionsReportAndNoThrowMacro( PluginSelectorWidget::OnImportNewPlugins )
}

void Core::Widgets::PluginSelectorWidget::OnMenu( wxCommandEvent& event )
{
	try
	{
		wxMenuItem* item = m_ProvidersMenu->FindItem( event.GetId() );

		BaseFactory::Pointer baseFactory;
		baseFactory = FactoryManager::FindByInstanceClassName( item->GetLabel().ToStdString() );
		SmartPointerObject::Pointer object;
		object = baseFactory->CreateInstance();
		Runtime::PluginProvider::Pointer provider = Runtime::PluginProvider::SafeDownCast( object );

		Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
		graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

		PropertiesConfigurationDialog *dialog;

		BaseWindowFactorySearch::Pointer search = BaseWindowFactorySearch::New( );
		search->SetCaption( provider->GetNameOfClass() );
		search->Update( );
		std::list<std::string> windowList = search->GetFactoriesNames();

		if ( windowList.empty() )
		{
			dialog = new PropertiesConfigurationDialog( this, wxID_ANY, "Properties" );
		}
		else
		{
			BaseWindow* baseWindow;
			baseWindow = graphicalIface->GetBaseWindowFactory()->CreateBaseWindow( *windowList.begin(), this );
			dialog = static_cast<PropertiesConfigurationDialog*> ( baseWindow );
		}

		blTagMap::Pointer configuration;
		configuration = provider->GetProperties()->GetTagValue<blTagMap::Pointer>( "Configuration" );
		dialog->SetProperties( configuration );

		dialog->SetSize( wxSize(600, dialog->GetSize(  ).y) );
		if ( dialog->ShowModal() == wxID_OK )
		{
			Runtime::Kernel::GetApplicationRuntime()->SetAppState( Runtime::APP_STATE_PROCESSING );

			m_PluginProviderManager->Add( provider );

			m_PluginProviderManager->UpdateProviders( provider->GetName(), false );

			m_ChkTree->SetModified( true );

			m_ChkTree->UpdateWidget();
		}

		dialog->Destroy();
	}
	coreCatchExceptionsReportAndNoThrowMacro( PluginSelectorWidget::OnMenu )

	Runtime::Kernel::GetApplicationRuntime()->SetAppState( Runtime::APP_STATE_IDLE );
}

void Core::Widgets::PluginSelectorWidget::OnRightMouseButtonClick( wxTreeEvent& event )
{
	try
	{

		Core::Runtime::Settings::Pointer settings;
		settings = Core::Runtime::Kernel::GetApplicationSettings();

		m_TreeItemMenu = new wxMenu( );

		wxTreeItemId itemID = event.GetItem( );
		m_ChkTree->SelectItem( itemID );

		if ( m_ChkTree->GetItemParent( itemID ) == m_ChkTree->GetRootItem( ) &&
			settings->GetPerspective( ) == Runtime::PERSPECTIVE_PLUGIN )
		{
			m_TreeItemMenu->Append(wxID_REMOVE_PROVIDER, "Remove" );
			m_TreeItemMenu->Append(wxID_SCAN_PLUGINS, "Scan" );

			PopupMenu( m_TreeItemMenu );
		}
		else if ( itemID == m_ChkTree->GetRootItem( ) )
		{
			m_TreeItemMenu->Append(wxID_SCAN_PLUGINS, "Scan" );
			m_TreeItemMenu->Append(wxID_ADD_DEFAULT, "Add default providers" );

			PopupMenu( m_TreeItemMenu );
		}
		else
		{
		}

		delete m_TreeItemMenu;
		m_TreeItemMenu = NULL;
	}
	coreCatchExceptionsReportAndNoThrowMacro( PluginSelectorWidget::OnRightMouseButtonClick )
}

void Core::Widgets::PluginSelectorWidget::OnScanPlugins( wxCommandEvent &event )
{
	Runtime::Kernel::GetApplicationRuntime()->SetAppState( Runtime::APP_STATE_PROCESSING );

	try
	{

		wxTreeItemId itemID = m_ChkTree->GetSelection( );

		std::string caption = std::string(m_ChkTree->GetItemText( itemID ).mb_str());

		if ( m_ChkTree->GetItemParent( itemID ) == m_ChkTree->GetRootItem( ) )
		{
			m_PluginProviderManager->ScanPlugins( caption, false );
		}
		else if ( itemID == m_ChkTree->GetRootItem( ) )
		{
			m_PluginProviderManager->ScanPlugins( "", false );
		}

		m_ChkTree->UpdateWidget();
	}
	coreCatchExceptionsReportAndNoThrowMacro( PluginSelectorWidget::OnScanPlugins )

	Runtime::Kernel::GetApplicationRuntime()->SetAppState( Runtime::APP_STATE_IDLE );
}

void Core::Widgets::PluginSelectorWidget::OnRemoveProvider( wxCommandEvent& event )
{
	try
	{
		wxTreeItemId itemID = m_ChkTree->GetSelection( );

		std::string caption = std::string(m_ChkTree->GetItemText( itemID ).mb_str());

		Runtime::PluginProvider::Pointer provider = m_PluginProviderManager->GetPluginProvider( caption );
		if ( provider->GetNameOfClass() == Runtime::FrontEndPluginProvider::GetNameClass() )
		{
			wxMessageDialog dialog( NULL, "Are you sure you want to remove this provider?", "Warning", wxYES_NO );
			if ( dialog.ShowModal() == wxID_NO )
			{
				return;
			}
		}

		m_PluginProviderManager->Remove( provider );

		m_ChkTree->UpdateWidget();

		m_ChkTree->SetModified( true );
	}
	coreCatchExceptionsReportAndNoThrowMacro( PluginSelectorWidget::OnRemoveProvider )
}

void Core::Widgets::PluginSelectorWidget::OnAddDefault( wxCommandEvent& event )
{
	Runtime::Kernel::GetApplicationRuntime()->SetAppState( Runtime::APP_STATE_PROCESSING );

	try
	{
		m_PluginProviderManager->AddDefaultProviders( );

		m_ChkTree->UpdateWidget();
	}
	coreCatchExceptionsReportAndNoThrowMacro( PluginSelectorWidget::OnAddDefault )

	Runtime::Kernel::GetApplicationRuntime()->SetAppState( Runtime::APP_STATE_IDLE );
}

void Core::Widgets::PluginSelectorWidget::OnFilter( wxCommandEvent& event )
{
	UpdateWidget();
}

void Core::Widgets::PluginSelectorWidget::OnModifiedProvidersStatus( )
{
	Core::WxGenericEvent event;

	switch ( m_PluginProviderManager->GetProvidersUpdater()->GetStatusHolder( )->GetSubject( ) )
	{
	case PluginProvidersUpdater::STATUS_IDLE:break;
	case PluginProvidersUpdater::STATUS_PROCESSING:
		event.SetFunction( boost::bind( &PluginSelectorWidget::OnStartLoading, this ) );
		break;
	case PluginProvidersUpdater::STATUS_FINISHED:
		event.SetFunction( boost::bind( &PluginSelectorWidget::OnStopLoading, this ) );
		break;
	}

	if ( wxIsMainThread( ) )
	{
		wxTheApp->GetTopWindow( )->GetEventHandler( )->ProcessEvent( event );
	}
	else
	{
    wxQueueEvent(wxTheApp->GetTopWindow(),event.Clone());
	}

}

void Core::Widgets::PluginSelectorWidget::OnStartLoading( )
{
	if ( !m_PluginProviderManager->GetProvidersUpdater()->GetEnableLoadPlugins( ) ||
		 !m_EnableCallbackMessages )
		return;

	m_Clock.Start( );

	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	//Runtime::Kernel::GetApplicationRuntime()->SetAppState( Runtime::APP_STATE_PROCESSING );

	// Change state
	m_PluginProviderManager->GetStateHolder( )->SetSubject( 
		Core::Runtime::PluginProviderManager::STATE_LOADING_PLUGINS );

	// Lock main GUI
	if ( graphicalIface.IsNotNull( ) && graphicalIface->GetMainWindow() )
		graphicalIface->GetMainWindow()->Lock( true );

	// Initialize perspective
	if ( graphicalIface->GetMainWindow() )
	{
		graphicalIface->GetMainWindow()->InitializePerspective( );
	}

	// 
	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();
	switch ( settings->GetPerspective() )
	{
	case Core::Runtime::PERSPECTIVE_PLUGIN :
		break;
	case Core::Runtime::PERSPECTIVE_WORKFLOW :
		{
			// Update selected plugins from workflow plugin's list
			Core::Workflow::Pointer workflow;
			workflow = Core::Runtime::Kernel::GetWorkflowManager()->GetActiveWorkflow( );
			if ( workflow.IsNotNull() )
			{
				// Get plugin list
				Workflow::PluginNamesListType pluginsList = workflow->GetPluginNamesList();
				Workflow::PluginNamesListType extra = workflow->GetExtraPluginNamesList();
				pluginsList.insert(  pluginsList.end(), extra.begin(), extra.end( ) );

				// Check if the plugin is in the list and set it to true, else set it to false
				Core::Runtime::PluginProviderManager::PluginProvidersType providers;
				providers = m_PluginProviderManager->GetPluginProviders( );
				Core::Runtime::PluginProviderManager::PluginProvidersType::iterator itProvider;
				for ( itProvider = providers.begin() ; itProvider != providers.end() ; itProvider++ )
				{
					(*itProvider)->EnablePluginsByName( pluginsList, NULL );
				}	

				std::cout << "Loading workflow " << workflow->GetName( ) << "..." << std::endl;
			}
		}
		break;
	}


}

void Core::Widgets::PluginSelectorWidget::OnStopLoading( )
{
	m_ChkTree->UpdateWidget();

	if ( !m_PluginProviderManager->GetProvidersUpdater()->GetEnableLoadPlugins( ) ||
		 !m_EnableCallbackMessages )
		return;

	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	Core::Runtime::Settings::Pointer settings;
	settings = Core::Runtime::Kernel::GetApplicationSettings();

	//Runtime::Kernel::GetApplicationRuntime()->SetAppState( Runtime::APP_STATE_IDLE );

	switch ( settings->GetPerspective() )
	{
	case Core::Runtime::PERSPECTIVE_PLUGIN :
		{
			// Configure plugin tab layout after adding all windows by all plugins
			if ( graphicalIface->GetMainWindow() )
			{
				graphicalIface->GetMainWindow()->UpdateLayout( );
			}
		}
		break;
	case Core::Runtime::PERSPECTIVE_WORKFLOW :
		{
			Core::Workflow::Pointer workflow;
			workflow = Core::Runtime::Kernel::GetWorkflowManager()->GetActiveWorkflow( );
			if ( graphicalIface->GetMainWindow() )
			{
				graphicalIface->GetMainWindow()->AttachWorkflow( workflow );
			}
		}
		break;
	}

	// Unlock main window
	if ( graphicalIface.IsNotNull( ) && graphicalIface->GetMainWindow() )
		graphicalIface->GetMainWindow()->Lock( false );

	// change plugin provider state
	m_PluginProviderManager->GetStateHolder( )->SetSubject( 
		Core::Runtime::PluginProviderManager::STATE_IDLE );

	// Hide splash screen
	graphicalIface->GetMainWindow()->ShowSplashScreen( false );

	// show main window
	graphicalIface->GetMainWindow()->Show( );

	std::cout << "Loading plugins: " << m_Clock.Finish( ) << " sec" << std::endl;
}

void Core::Widgets::PluginSelectorWidget::OnModifiedPluginProviderManager( )
{
	if ( !m_EnableCallbackMessages )
		return;

	// Update observer to callback
	Core::Runtime::PluginProviderManager::PluginProvidersType providers;
	providers = m_PluginProviderManager->GetPluginProviders( );
	
	Core::Runtime::PluginProviderManager::PluginProvidersType::iterator itProvider;
	for ( itProvider = providers.begin() ; itProvider != providers.end() ; itProvider++ )
	{
		// Remove previous observer (if any)
		(*itProvider)->GetUpdateCallback( )->RemoveObserver1( this, &PluginSelectorWidget::OnUpdateCallback );
		(*itProvider)->GetUpdateCallback( )->AddObserver1( this, &PluginSelectorWidget::OnUpdateCallback );
	}	

}

void Core::Widgets::PluginSelectorWidget::OnUpdateCallback( SmartPointerObject* object )
{
	// If observer is removed, return
	if ( object == NULL || !m_EnableCallbackMessages )
		return;

	Core::WxGenericEvent event;
	event.SetFunction( boost::bind( &PluginSelectorWidget::UpdateLogMessages, this ) );
	if ( wxIsMainThread( ) )
	{
		wxTheApp->GetTopWindow( )->GetEventHandler( )->ProcessEvent( event );
	}
	else
	{
    wxPostEvent(wxTheApp->GetTopWindow()->GetEventHandler(),event);
	}
}

void Core::Widgets::PluginSelectorWidget::UpdateLogMessages( )
{
	Core::Runtime::PluginProviderManager::PluginProvidersType providers;
	providers = m_PluginProviderManager->GetPluginProviders( );
	
	Core::Runtime::PluginProviderManager::PluginProvidersType::iterator itProvider;
	for ( itProvider = providers.begin() ; itProvider != providers.end() ; itProvider++ )
	{

		// Get information messages
		std::string information = (*itProvider)->GetUpdateCallback( )->GetInformationMessage( );
		std::string newMessages = information.substr(m_CallbackMessages[ (*itProvider)->GetUpdateCallback( ) ].size( ), information.size( ) );

		std::list<std::string> messages;
		blTextUtils::ParseLine( newMessages, '\n', messages );
		if ( !messages.empty( ) )
		{

			std::list<std::string>::iterator itMessage;
			for ( itMessage = messages.begin( ) ; itMessage != messages.end( ) ; itMessage++ )
			{
				if ( (*itMessage).find( "in cache" ) == std::string::npos )
				{
					Core::Runtime::Kernel::GetGraphicalInterface()->LogMessage((*itMessage));
					Core::Runtime::Kernel::GetLogManager()->LogMessage((*itMessage));
					std::cout << (*itMessage) << std::endl;
				}
			}

			m_CallbackMessages[ (*itProvider)->GetUpdateCallback( ) ] = information;
		}

		// Get exception messages
		std::string exception = (*itProvider)->GetUpdateCallback( )->GetExceptionMessage( );
		std::string newException = exception.substr(m_CallbackExceptions[ (*itProvider)->GetUpdateCallback( ) ].size( ), exception.size( ) );
		if ( !newException.empty( ) )
		{
			m_CallbackExceptions[ (*itProvider)->GetUpdateCallback( ) ] = exception;
			Core::Exceptions::Exception e( "", newException.c_str( ) );
			Core::Runtime::Kernel::GetGraphicalInterface()->LogException(e);
			Core::Runtime::Kernel::GetLogManager()->LogException(e);
		}
	}	
}

void Core::Widgets::PluginSelectorWidget::EnableCallbackMessages( bool enable )
{
	m_EnableCallbackMessages = enable;
	if ( m_EnableCallbackMessages )
	{
		OnModifiedPluginProviderManager( );
	}
	else
	{
		RemovePluginProviderObservers( );
	}
}

void Core::Widgets::PluginSelectorWidget::RemovePluginProviderObservers( )
{
	Core::Runtime::PluginProviderManager::PluginProvidersType providers;
	providers = m_PluginProviderManager->GetPluginProviders( );
	
	Core::Runtime::PluginProviderManager::PluginProvidersType::iterator itProvider;
	for ( itProvider = providers.begin() ; itProvider != providers.end() ; itProvider++ )
	{
		(*itProvider)->GetUpdateCallback( )->RemoveObserver1( this, &PluginSelectorWidget::OnUpdateCallback );
	}	
}

void Core::Widgets::PluginSelectorWidget::OnSelectionChanged(wxTreeEvent& event)
{
	UpdatePluginDescription( );
}

void Core::Widgets::PluginSelectorWidget::UpdatePluginDescription( )
{
	// Get plugin provider
	wxTreeItemId itemId = m_ChkTree->GetSelection( );
	if ( !itemId.IsOk( ) )
	{
		return;
	}

	ModuleDescription module = GetModuleDescription( itemId );

	m_txtDescription->SetValue( module.GetDescription( ) );
	m_OnlineDocumentation->SetURL( module.GetDocumentationURL( ) );

	// Change properties of hyperlink
	if ( module.GetDocumentationURL( ).empty( ) )
	{
		m_OnlineDocumentation->Enable( false );
		m_OnlineDocumentation->SetNormalColour( wxColour( 128, 128, 128 ) );
	}
	else
	{
		m_OnlineDocumentation->Enable( true );
		m_OnlineDocumentation->SetNormalColour( wxColour( 0, 0, 255 ) );
	}

	// Get preview image
	wxImage image = GetPreviewImage( itemId );

	// Get parent preview image
	wxTreeItemId parentItemId = m_ChkTree->GetItemParent( itemId );
	if ( !image.IsOk( ) && parentItemId.IsOk( ) )
	{
		// Get parent plugin image
		image = GetPreviewImage( parentItemId );
	}

	// If image cannot be loaded, load GIMIAS icon
	if ( !image.IsOk( ) )
	{
		std::string location = Core::Runtime::Kernel::GetApplicationSettings( )->GetCoreResourceForFile( "GIMIAS_Logo.ico" );
		if ( itksys::SystemTools::FileExists( location.c_str( ) ) )
		{
			image.LoadFile( location );
		}
	}

	// Set icon to control
	if ( !image.IsOk( ) )
	{
		m_bmpSnapshot->SetIcon( wxNullIcon );
	}
	else
	{
		wxIcon icon;
		icon.CopyFromBitmap( image.Scale( 60, 60, wxIMAGE_QUALITY_HIGH ) );
		m_bmpSnapshot->SetIcon( icon );
	}
}

ModuleDescription Core::Widgets::PluginSelectorWidget::GetModuleDescription( wxTreeItemId itemId )
{
	PluginData* pluginData = dynamic_cast<PluginData*> ( m_ChkTree->GetItemData( itemId ) );
	Core::Runtime::PluginProvider::Pointer provider;
	ModuleDescription module;
	if ( pluginData != NULL )
	{
		provider = m_PluginProviderManager->GetPluginProvider( pluginData->m_ProviderName );

		// Get module description
		if ( provider.IsNotNull( ) && provider->GetModuleFactory( ) != NULL )
		{
			module = provider->GetModuleFactory( )->GetModuleDescription( pluginData->m_PluginName );
		}
	}

	return module;
}

wxImage Core::Widgets::PluginSelectorWidget::GetPreviewImage( wxTreeItemId itemId )
{
	ModuleDescription module = GetModuleDescription( itemId );

	// Get preview image
	std::string location = module.GetLocation( );
	blTextUtils::StrSub( location, ".xml", ".png" );
	wxImage image;
	if ( itksys::SystemTools::FileExists( location.c_str( ) ) )
	{
		image.LoadFile( location );
	}

	return image;
}

