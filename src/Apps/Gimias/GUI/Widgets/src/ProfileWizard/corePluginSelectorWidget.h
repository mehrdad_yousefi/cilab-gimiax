/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef corePluginSelectorWidget_H
#define corePluginSelectorWidget_H

#include "gmWidgetsWin32Header.h"
#include "coreObject.h"
#include "corePreferencesPage.h"
#include "corePluginProviderManager.h"
#include "corePluginTree.h"
#include "blClock.h"
#include "corePluginProvidersUpdater.h"
#include <wx/hyperlink.h>

class wxWizardEvent;

namespace Core
{
namespace Widgets
{
/** 
\brief Select the plugins to load.

\ingroup gmWidgets
\author Xavi Planes
\date Nov 2010
*/
class GMWIDGETS_EXPORT PluginSelectorWidget : 
	public wxPanel
	,public Core::Widgets::PreferencesPage
{
public:
	coreDefineBaseWindowFactory( Core::Widgets::PluginSelectorWidget )

	//!
	PluginSelectorWidget(
		wxWindow* parent, 
		int id  = wxID_ANY, 
		const wxPoint& pos=wxDefaultPosition, 
		const wxSize& size=wxDefaultSize, 
		long style=0);

	//!
	~PluginSelectorWidget(void);

	//! Redefined
	void UpdateData( );

	//! Redefined
	void UpdateWidget( );

	//!
	void EnableCallbackMessages( bool enable );

protected:

	//! Update tree state
	bool UpdateTree( wxTreeItemId parentItem, blTagMap::Pointer plugins );

	/** Update profile state
	\param [in] unloadResources Unload registered resources
	*/
	void UpdateProvider( 
		Runtime::PluginProvider::Pointer provider, 
		wxTreeItemId item );

	//!
	void do_layout();

	//!
	void OnStartLoading( );

	//!
	void OnStopLoading( );

	//!
	void OnModifiedProvidersStatus( );

	//! Update observers to providers for callback
	void OnModifiedPluginProviderManager( );

	//!
	void RemovePluginProviderObservers( );

	/** Update messages (called when AddInformationMessage() is called in the provider)
	\note Secondary thread
	*/
	void OnUpdateCallback( SmartPointerObject* object );

	//!
	void UpdateLogMessages( );

	//!
	void UpdatePluginDescription( );

	//!
	ModuleDescription GetModuleDescription( wxTreeItemId itemId );

	//!
	wxImage GetPreviewImage( wxTreeItemId itemId );

private:

	//!
	void OnRightMouseButtonClick(wxTreeEvent& event);

	//!
	void OnImportNewPlugins(wxCommandEvent &event);

	//!
	void OnScanPlugins(wxCommandEvent &event);

	/** To show a configuration dialog, searches for a registered dialog with
	the caption that matches the NameOfClass of the plugin provider. If no
	factory is found, it will create a generic PropertiesConfigurationDialog.
	*/
	void OnMenu(wxCommandEvent& event);

	//!
	void OnRemoveProvider(wxCommandEvent& event);

	//!
	void OnAddDefault(wxCommandEvent& event);

	//!
	void OnFilter( wxCommandEvent& event );

	//!
	void OnSelectionChanged(wxTreeEvent& event);

private:
	//!
	PluginTree* m_ChkTree;

	//!
	wxButton* m_btnImport;

	//!
	wxMenu* m_ProvidersMenu;

	//!
	wxMenu* m_TreeItemMenu;

	//!
	wxStaticText* m_lblFilter;

	//!
	wxTextCtrl* m_txtFilter;

	//!
	wxTextCtrl* m_txtDescription;

	//!
	wxHyperlinkCtrl* m_OnlineDocumentation;

	//!
	Runtime::PluginProviderManager::Pointer m_PluginProviderManager;

	//! Keep track of callback messages already processed
	std::map<Core::UpdateCallback::Pointer,std::string> m_CallbackMessages;

	//! Keep track of callback messages already processed
	std::map<Core::UpdateCallback::Pointer,std::string> m_CallbackExceptions;

	//!
	blClock m_Clock;

	//!
	wxStaticBitmap* m_bmpSnapshot;

	//! Enable plugin provider observers
	bool m_EnableCallbackMessages;

    wxDECLARE_EVENT_TABLE();
};
}
}

#endif // corePluginSelectorWidget_H
