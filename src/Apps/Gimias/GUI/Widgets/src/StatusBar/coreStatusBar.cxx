/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include "wx/wxprec.h"

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include <wx/animate.h>
#include <wx/wupdlock.h>

#include "coreStatusBar.h"
#include "coreProcessorManager.h"

#include "wxMemoryUsageIndicator.h"
#include "wxID.h"
#include "resource/cancel_processing.xpm"
#include "resource/cancel_processing_disabled.xpm"
#include "resource/cancel_processing_green.xpm"
#include "resource/cancel_processing_orange.xpm"
#include "resource/log_files.xpm"
#include "resource/log_files_red.xpm"

#define wxID_MemoryUsageWidget		wxID("wxID_MemoryUsageWidget")
#define wxID_CancelProcessing		wxID("wxID_CancelProcessing")
#define wxID_LogFiles				wxID("wxID_LogFiles")

wxBEGIN_EVENT_TABLE(Core::Widgets::StatusBar, wxStatusBar)
	EVT_SIZE(Core::Widgets::StatusBar::OnSize)
	EVT_MOVE(Core::Widgets::StatusBar::OnMove)
	EVT_UPDATE_CALLBACK_EVENT( StatusBar::OnUpdateCallback )
	EVT_BUTTON(wxID_CancelProcessing, StatusBar::OnCancelBtn)
wxEND_EVENT_TABLE()

enum
{
	Field_Text,
	Field_Processing,
	Field_Processing_Cancel,
	Field_Processing_Log,
	Field_Processing_Text,
	Field_Processing_Progress,
	Field_MemoryIndicator,
	Field_Max
};

Core::Widgets::StatusBar::StatusBar(wxWindow *parent)
	: wxStatusBar(parent, wxID_ANY, 0)
{
	m_ActiveProcessorThreadUID = -1;
	m_CancelPressed = false;

	m_ProcessorLogDialog = new ProcessorLogDialog(
		wxTheApp->GetTopWindow(), wxID_ANY, "Log" );
	m_ProcessorLogDialog->Hide();

	// Create wxMemoryUsageIndicator
	m_MemoryUsageWidget = new wxMemoryUsageIndicator( this, wxID_MemoryUsageWidget );

	// Create wxAnimationCtrl
	m_ProcessingCtrl = new wxAnimationCtrl( this, wxID_ANY );
	std::string resourcePath;
	resourcePath = Core::Runtime::Kernel::GetApplicationSettings()->GetResourcePath();
	std::string filename = resourcePath + "/icon_processing_min.gif";
	m_ProcessingCtrl->LoadFile( filename.c_str() );

	// Create wxGauge
	m_Gauge = new wxGauge( this, wxID_ANY, 100,
		wxDefaultPosition,
		wxDefaultSize,
		wxGA_SMOOTH | wxGA_HORIZONTAL );
	m_Gauge->SetSize( 100, 20 );

	// Cancel button
	m_btnCancel = new wxBitmapButton( this, wxID_CancelProcessing,
		wxBitmap( cancel_processing_xpm ) );
	m_btnCancel->SetBitmapDisabled( wxBitmap( cancel_processing_disabled_xpm ) );
	m_btnCancel->Enable( false );

	// Error msg
	m_btnLog = new awxCheckButton( this, wxID_LogFiles, wxDefaultPosition, wxDefaultSize,
		log_files_xpm, NULL, NULL, NULL );
  this->GetEventHandler()->Bind(wxEVT_COMMAND_MENU_SELECTED, &StatusBar::OnLogFiles, this, wxID_LogFiles);

	// Widths
	int widths[ Field_Max ];
	widths[ Field_Text ] = -1;
	widths[ Field_Processing ] = m_ProcessingCtrl->GetAnimation().GetSize().GetX();
	widths[ Field_Processing_Cancel ] = 20;
	widths[ Field_Processing_Log ] = m_btnLog->GetSize().GetX();
	widths[ Field_Processing_Text ] = 200;
	widths[ Field_Processing_Progress ] = m_Gauge->GetSize().GetX();
	widths[ Field_MemoryIndicator ] = m_MemoryUsageWidget->GetMinSize().GetX();
	SetFieldsCount( Field_Max );
	SetStatusWidths( Field_Max, widths);

	// Doesn't work
	int sizeY = m_ProcessingCtrl->GetAnimation().GetSize().GetY();
	SetMinHeight( sizeY );

}

Core::Widgets::StatusBar::~StatusBar()
{
}

void Core::Widgets::StatusBar::OnSize(wxSizeEvent& event)
{
	wxRect rect;
	wxSize size;

	// Move the widget
	GetFieldRect( Field_Processing, rect );
	size = m_ProcessingCtrl->GetSize();
	m_ProcessingCtrl->Move( rect.x, rect.y );

	// Move the widget
	GetFieldRect( Field_Processing_Cancel, rect );
	size = m_btnCancel->GetSize();
	m_btnCancel->Move( rect.x, rect.y );

	// Move the widget
	GetFieldRect( Field_Processing_Log, rect );
	size = m_btnLog->GetSize();
	m_btnLog->Move( rect.x, rect.y );

	// Move the widget
	GetFieldRect( Field_Processing_Progress, rect );
	size = m_Gauge->GetSize();
	m_Gauge->Move( rect.x, rect.y );

	// Move the widget
	GetFieldRect( Field_MemoryIndicator, rect );
	size = m_MemoryUsageWidget->GetSize();
	m_MemoryUsageWidget->Move( rect.x, rect.y );

	// Processor dialog
	wxPoint screenPos = ClientToScreen( m_btnLog->GetPosition( ) );
	long sizeX = GetSize( ).GetWidth( ) - m_btnLog->GetPosition().x;
	m_ProcessorLogDialog->SetSize( sizeX, 200 );
	screenPos.y -= m_ProcessorLogDialog->GetSize().GetY();
	m_ProcessorLogDialog->SetPosition( screenPos );

    event.Skip();
}

void Core::Widgets::StatusBar::OnMove(wxMoveEvent& event)
{
	// Processor dialog
	wxPoint screenPos = ClientToScreen( m_btnLog->GetPosition( ) );
	screenPos.y -= m_ProcessorLogDialog->GetSize().GetY();
	m_ProcessorLogDialog->SetPosition( screenPos );

	event.Skip( );
}

void Core::Widgets::StatusBar::OnUpdateCallback( WxUpdateCallbackEvent& event )
{
	if ( m_ActiveProcessorThreadUID == -1 )
	{

		ProcessorThread::Pointer processorThread;
		processorThread = Runtime::Kernel::GetProcessorManager()->GetProcessorThread(
			-1, ProcessorThread::STATE_ACTIVE );
		if ( processorThread.IsNotNull()  )
		{
			// Start playing the icon
			m_ProcessingCtrl->Play();
			m_ProcessingCtrl->Show( );
			m_Gauge->Show();
			m_Gauge->SetValue( 0 );
			SetStatusText( wxString::Format( "Processing %s...",
				processorThread->GetProcessorName().c_str() ), Field_Processing_Text );

			// Restore initial cancel button
			m_btnCancel->SetBitmapLabel( wxBitmap( cancel_processing_xpm ) );

			m_ActiveProcessorThreadUID = processorThread->GetUID();
		}
	}
	// Only update the active processor
	else if ( event.GetUpdateCallback()->GetProcessorThreadID() == m_ActiveProcessorThreadUID )
	{

		// User canceled
		if ( m_CancelPressed )
		{
			event.GetUpdateCallback()->SetAbortProcessing( true );
			m_btnCancel->SetBitmapLabel( wxBitmap( cancel_processing_green_xpm ) );
		}

		// Progress
		m_Gauge->SetValue( event.GetUpdateCallback()->GetProgress() * 100 );

		// Error message
		if ( !event.GetUpdateCallback()->GetExceptionMessage().empty() )
		{
			m_btnLog->SetIcon( awxButton::State_ButtonUp, log_files_red_xpm );
		}

		m_ProcessorLogDialog->SetPendingUpdate( true );

		// Processor finished?
		Core::ProcessorThread::Pointer processorThread;
		processorThread = Runtime::Kernel::GetProcessorManager()->GetProcessorThread( m_ActiveProcessorThreadUID );
		if ( processorThread->GetState() == ProcessorThread::STATE_FINISHED )
		{
			m_ProcessingCtrl->Stop( );
			m_ProcessingCtrl->Hide();
			m_Gauge->Hide();
			SetStatusText( "", Field_Processing_Text );
			m_btnCancel->Enable( false );
			m_ActiveProcessorThreadUID = -1;
			m_CancelPressed = false;
		}
		else if ( !m_CancelPressed )
		{
			// For readers and writers, this property can change during execution
			Core::BaseProcessor::Pointer processor = processorThread->GetProcessor();
			m_btnCancel->SetBitmapLabel( wxBitmap( cancel_processing_xpm ) );
			// Enable it if progress has been changed
			m_btnCancel->Enable( processor.IsNotNull( ) && event.GetUpdateCallback()->GetProgress( ) != 0 );
		}
	}
}


void Core::Widgets::StatusBar::OnCancelBtn(wxCommandEvent &event)
{
	if ( m_ActiveProcessorThreadUID == -1 )
	{
		return;
	}

	m_CancelPressed = true;
	m_btnCancel->SetBitmapLabel( wxBitmap( cancel_processing_orange_xpm ) );
}

void Core::Widgets::StatusBar::OnLogFiles( wxCommandEvent& event )
{
	if ( event.GetInt() )
	{
		m_btnLog->SetIcon( awxButton::State_ButtonUp, log_files_xpm );

		m_ProcessorLogDialog->UpdateWidget( );
		m_ProcessorLogDialog->Show();
	}
	else
	{
		m_ProcessorLogDialog->Hide();
	}

	event.Skip();
}
