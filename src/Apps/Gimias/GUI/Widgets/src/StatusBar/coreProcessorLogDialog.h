/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreProcessorLogDialog_H
#define _coreProcessorLogDialog_H


#include "coreProcessorLogDialogUI.h"

namespace Core
{

namespace Widgets{

/**
Modal dialog to show all executed, pending and active processors of
ProcessorManager. 

The user can double click on an item to show full message text.

\ingroup gmWidgets
\author Xavi Planes
\date Jan 2011
*/
class ProcessorLogDialog : public coreProcessorLogDialogUI 
{
public:
	enum MODE
	{
		//! Show log of all processors
		MODE_LOG,
		//! Show information of a selected processor
		MODE_INFORMATION
	};
	//!
    ProcessorLogDialog(wxWindow* parent, int id, const wxString& title, 
		const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, 
		long style=wxDEFAULT_DIALOG_STYLE);

	//!
	~ProcessorLogDialog();
	//!
	void SetPendingUpdate( bool val );
	//!
	void UpdateWidget( );

private:
	//!
    wxDECLARE_EVENT_TABLE();
	//!
	void OnItemActivated( wxListEvent &event );

	//!
	void OnListRightClick(wxListEvent &event);

	//!
	void OnMenuItemRemoveTask( wxCommandEvent& event );

	//!
	void OnOK(wxCommandEvent &event);

	//!
	void SetMode( MODE mode );

	//! Updated widget if needed
	void OnTimer(wxTimerEvent& event);

protected:

	//!
	MODE m_Mode;
	//!
	long m_SelectedItem;
	//! For updateing state
	wxTimer* m_Timer;
	//! Set pending update for next timer event
	bool m_PendingUpdate;
};

} // Widgets
} // Core

#endif // _coreProcessorLogDialog_H
