/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreStatusBar_H
#define coreStatusBar_H

#include "gmWidgetsWin32Header.h"
#include "coreWxUpdateCallbackEvent.h"
#include "coreProcessorLogDialog.h"

#include "wx/statusbr.h"
#include "awx-0.8/button.h"

class wxMemoryUsageIndicator;
class wxAnimationCtrl;

namespace Core
{
namespace Widgets
{

/** 
\brief Custom status bar

\ingroup gmWidgets
\author Xavi Planes
\date 23 Dec 2008
*/
class GMWIDGETS_EXPORT StatusBar : public wxStatusBar
{
public:
	StatusBar(wxWindow *parent);
	virtual ~StatusBar();

	//!
	void OnSize(wxSizeEvent& event);

protected:

	//!
	void OnMove(wxMoveEvent& event);

	//!
	void OnUpdateCallback( WxUpdateCallbackEvent& event );

	//!
	void OnCancelBtn(wxCommandEvent &event);

	//!
	void OnLogFiles( wxCommandEvent& event );

    wxDECLARE_EVENT_TABLE();

protected:
	//!
	wxMemoryUsageIndicator *m_MemoryUsageWidget;
	//!
	wxAnimationCtrl* m_ProcessingCtrl;
	//!
	wxBitmapButton* m_btnCancel;
	//!
	awxCheckButton* m_btnLog;
	//!
	wxGauge* m_Gauge;
	//!
	ProcessorLogDialog* m_ProcessorLogDialog;

	//!
	bool m_CancelPressed;
	//!
	long m_ActiveProcessorThreadUID;
};

} // namespace Widgets
} // namespace Core

#endif // coreStatusBar_H
