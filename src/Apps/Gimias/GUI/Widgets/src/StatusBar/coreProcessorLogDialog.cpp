/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreProcessorLogDialog.h"
#include "coreProcessorManager.h"
#include "wx/wupdlock.h"

#define wxID_INFORMATION_CTRL wxID( "wxID_INFORMATION_CTRL" )
#define wxID_REMOVE_TASK wxID( "wxID_REMOVE_TASK" )

BEGIN_EVENT_TABLE(Core::Widgets::ProcessorLogDialog, coreProcessorLogDialogUI)
	EVT_LIST_ITEM_ACTIVATED(wxID_ANY, ProcessorLogDialog::OnItemActivated)
	EVT_LIST_ITEM_RIGHT_CLICK(wxID_ANY, ProcessorLogDialog::OnListRightClick)
	EVT_MENU	(wxID_REMOVE_TASK,ProcessorLogDialog::OnMenuItemRemoveTask)
	EVT_TIMER		(wxID_ANY, ProcessorLogDialog::OnTimer)
END_EVENT_TABLE();

Core::Widgets::ProcessorLogDialog::ProcessorLogDialog(
	wxWindow* parent, int id, const wxString& title, const wxPoint& pos, 
	const wxSize& size, long style)
		: coreProcessorLogDialogUI(parent, id, title, pos, size, style)
{
	m_listCtrl->InsertColumn(0,_T("Id"), wxLIST_FORMAT_LEFT, 50 );
	m_listCtrl->InsertColumn(1,_T("Duration (s)"), wxLIST_FORMAT_LEFT, 50 );
	m_listCtrl->InsertColumn(2,_T("Processor"), wxLIST_FORMAT_LEFT, 100 );
	m_listCtrl->InsertColumn(3,_T("Message"), wxLIST_FORMAT_LEFT, 230 );

	SetMode( MODE_LOG );

	m_Timer = new wxTimer(this, wxID_ANY);
	m_Timer->Start(1000, wxTIMER_CONTINUOUS); // milli seconds
	m_PendingUpdate = false;
}

Core::Widgets::ProcessorLogDialog::~ProcessorLogDialog()
{
	delete m_Timer;
}

void Core::Widgets::ProcessorLogDialog::UpdateWidget( )
{
	switch( m_Mode )
	{
	case MODE_LOG: 
		{
			bool entryAdded = false;

			// Fill m_ProcessorListCtrl
			Core::ProcessorManager::ProcessorThreadListType processorThreads;
			processorThreads = Runtime::Kernel::GetProcessorManager()->GetProcessorThreadList( );
			Core::ProcessorManager::ProcessorThreadListType::iterator it;
			for ( it = processorThreads.begin() ; it != processorThreads.end() ; it++)
			{
				wxString uid = wxString::Format( "%d", static_cast<unsigned int>((*it)->GetUID( ) ));
				long itemId = m_listCtrl->FindItem( -1, uid );
				if ( itemId == -1 )
				{
					itemId = m_listCtrl->InsertItem( m_listCtrl->GetItemCount(), uid );
					m_listCtrl->SetItem( itemId, 1, (*it)->GetDurationAsString() );
					m_listCtrl->SetItem( itemId, 2, (*it)->GetProcessorName() );
					m_listCtrl->SetItem( itemId, 3, (*it)->GetStatusMessage( ) );
					entryAdded = true;
				}
				else
				{
					wxListItem info;
					info.SetId( itemId );
					info.SetColumn( 1 );
					m_listCtrl->GetItem( info );
					if ( info.GetText( ) != (*it)->GetDurationAsString() )
					{
						info.SetText( (*it)->GetDurationAsString() );
						m_listCtrl->SetItem( info );
					}

					info.SetColumn( 2 );
					m_listCtrl->GetItem( info );
					if ( info.GetText( ) != (*it)->GetProcessorName() )
					{
						info.SetText( (*it)->GetProcessorName() );
						m_listCtrl->SetItem( info );
					}

					info.SetColumn( 3 );
					m_listCtrl->GetItem( info );
					if ( info.GetText( ) != (*it)->GetStatusMessage() )
					{
						info.SetText( (*it)->GetStatusMessage() );
						m_listCtrl->SetItem( info );
					}
				}
			}

			if ( m_listCtrl->GetItemCount() && entryAdded )
			{
				m_listCtrl->EnsureVisible(m_listCtrl->GetItemCount() - 1);
			}
		}
		break;
	case MODE_INFORMATION: 
		{
			// Get UID of selected item
			wxString uidStr = m_listCtrl->GetItemText( m_SelectedItem );
			long uid;
			if ( uidStr.ToLong( &uid ) )
			{
				Core::ProcessorThread::Pointer processorThread;
				processorThread = Runtime::Kernel::GetProcessorManager()->GetProcessorThread( uid );
				if ( processorThread.IsNotNull() && processorThread->GetUpdateCallback().IsNotNull( ) )
				{
					// Backup scroll position
					// int scrollPosY = GetScrollPos( wxVERTICAL );

					std::stringstream stream;
					if ( !processorThread->GetUpdateCallback()->GetExceptionMessage( ).empty( ) )
					{
						stream << "Exception:" << std::endl << processorThread->GetUpdateCallback()->GetExceptionMessage( );
						stream << std::endl;
						stream << "________________________________" << std::endl;
					}
					if ( !processorThread->GetUpdateCallback( )->GetInformationMessage().empty( ) )
					{
						stream << "Information: " << std::endl << processorThread->GetUpdateCallback( )->GetInformationMessage();
					}
					if ( stream.str().empty( ) )
					{
						stream << processorThread->GetStatusMessage( );
					}
					m_txtInformation->SetValue( stream.str().c_str( ) );

					//SetScrollPos( wxVERTICAL, scrollPosY );
				}
			}
		}
		break;
	}

}

void Core::Widgets::ProcessorLogDialog::OnItemActivated( wxListEvent &event )
{
	m_SelectedItem = event.GetIndex( );

	SetMode( MODE_INFORMATION );
}

void Core::Widgets::ProcessorLogDialog::SetMode( MODE mode )
{
	wxWindowUpdateLocker lock(this);

	m_Mode = mode;
	switch( mode)
	{
	case MODE_LOG: 
		m_txtInformation->Show( false );
		m_btnOK->Show( false );
		m_listCtrl->Show( true );
		break;
	case MODE_INFORMATION: 
		GetSizer()->Show( m_txtInformation, true );
		GetSizer()->Show( m_btnOK, true );
		GetSizer()->Show( m_listCtrl, false );
		break;
	}

	Layout( );

	UpdateWidget( );
}

void Core::Widgets::ProcessorLogDialog::OnOK( wxCommandEvent &event )
{
	SetMode( MODE_LOG );
}

void Core::Widgets::ProcessorLogDialog::OnListRightClick( wxListEvent &event )
{
	long item = -1;
	item = m_listCtrl->GetNextItem(item,
		wxLIST_NEXT_ALL,
		wxLIST_STATE_SELECTED);
	if ( item != -1 )
	{
		wxString uidStr = m_listCtrl->GetItemText( item );
		long uid;
		if ( uidStr.ToLong( &uid ) )
		{
			Core::ProcessorThread::Pointer processorThread;
			processorThread = Runtime::Kernel::GetProcessorManager()->GetProcessorThread( uid );
			if ( processorThread.IsNotNull() && 
				processorThread->GetState() != ProcessorThread::STATE_FINISHED )
			{
				wxMenu menu;
				menu.Append( wxID_REMOVE_TASK, "Cancel" );
				PopupMenu( &menu );
			}
		}
	}
}

void Core::Widgets::ProcessorLogDialog::OnMenuItemRemoveTask( wxCommandEvent& event )
{
	long item = -1;
	item = m_listCtrl->GetNextItem(item,
		wxLIST_NEXT_ALL,
		wxLIST_STATE_SELECTED);
	while ( item != -1 )
	{
		wxString uidStr = m_listCtrl->GetItemText( item );
		long uid;
		if ( uidStr.ToLong( &uid ) )
		{
			Core::ProcessorThread::Pointer processorThread;
			processorThread = Runtime::Kernel::GetProcessorManager()->GetProcessorThread( uid );
			if ( processorThread.IsNotNull() && processorThread->GetProcessor().IsNotNull() )
			{
				processorThread->GetProcessor()->Abort();
			}
		}

		item = m_listCtrl->GetNextItem(item,
			wxLIST_NEXT_ALL,
			wxLIST_STATE_SELECTED);
	}


}

void Core::Widgets::ProcessorLogDialog::OnTimer( wxTimerEvent& event )
{
	if ( m_PendingUpdate || IsShown( ) )
	{
		UpdateWidget();
		m_PendingUpdate = false;
	}
}

void Core::Widgets::ProcessorLogDialog::SetPendingUpdate( bool val )
{
	m_PendingUpdate = val;
}
