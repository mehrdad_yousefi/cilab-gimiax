// -*- C++ -*- generated by wxGlade 0.6.3 on Thu Jan 26 16:28:49 2012

#include <wx/wx.h>
#include <wx/image.h>

#ifndef CORECUSTOMAPPLICATIONMANAGERWIDGETUI_H
#define CORECUSTOMAPPLICATIONMANAGERWIDGETUI_H

// begin wxGlade: ::dependencies
#include <wx/treectrl.h>
// end wxGlade

// begin wxGlade: ::extracode
#include "wxID.h"

#define wxID_BTN_NEW wxID( "wxID_BTN_NEW" )
#define wxID_BTN_DELETE wxID( "wxID_BTN_DELETE" )
#define wxID_BTN_EDIT wxID( "wxID_BTN_EDIT" )
#define wxID_BTN_IMPORT wxID( "wxID_BTN_IMPORT" )
#define wxID_BTN_RENAME wxID( "wxID_BTN_RENAME" )
#define wxID_BTN_COPY wxID( "wxID_BTN_COPY" )


// end wxGlade


class coreCustomApplicationManagerWidgetUI: public wxDialog {
public:
    // begin wxGlade: coreCustomApplicationManagerWidgetUI::ids
    // end wxGlade

    coreCustomApplicationManagerWidgetUI(wxWindow* parent, int id, const wxString& title, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=wxDEFAULT_DIALOG_STYLE);

private:
    // begin wxGlade: coreCustomApplicationManagerWidgetUI::methods
    void set_properties();
    void do_layout();
    // end wxGlade

protected:
    // begin wxGlade: coreCustomApplicationManagerWidgetUI::attributes
    wxTreeCtrl* m_Tree;
    wxButton* m_btnNew;
    wxButton* m_btnEdit;
    wxButton* m_btnRename;
    wxButton* m_btnCopy;
    wxButton* m_btnDelete;
    wxButton* m_btnImport;
    // end wxGlade

    wxDECLARE_EVENT_TABLE();

public:
    virtual void OnItemActivated(wxTreeEvent &event); // wxGlade: <event_handler>
    virtual void OnBtnNew(wxCommandEvent &event); // wxGlade: <event_handler>
    virtual void OnBtnEdit(wxCommandEvent &event); // wxGlade: <event_handler>
    virtual void OnBtnRename(wxCommandEvent &event); // wxGlade: <event_handler>
    virtual void OnBtnCopy(wxCommandEvent &event); // wxGlade: <event_handler>
    virtual void OnBtnDelete(wxCommandEvent &event); // wxGlade: <event_handler>
    virtual void OnBtnImport(wxCommandEvent &event); // wxGlade: <event_handler>
}; // wxGlade: end class


#endif // CORECUSTOMAPPLICATIONMANAGERWIDGETUI_H
