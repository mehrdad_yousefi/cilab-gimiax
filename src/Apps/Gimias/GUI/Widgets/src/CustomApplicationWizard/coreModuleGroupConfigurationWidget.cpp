/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreModuleGroupConfigurationWidget.h"

#include "wxUnicode.h"

IMPLEMENT_CLASS( Core::Widgets::ModuleGroupConfigurationWidget, coreModuleGroupConfigurationWidgetUI )

Core::Widgets::ModuleGroupConfigurationWidget::ModuleGroupConfigurationWidget
	(ModuleParameterGroup *group, wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    coreModuleGroupConfigurationWidgetUI(parent, id, pos, size, style)
{
	m_Group = group;
	UpdateWidget( );
}

void Core::Widgets::ModuleGroupConfigurationWidget::UpdateData( )
{
	if ( m_txtLabel->IsEmpty( ) )
	{
		throw Core::Exceptions::Exception( "ModuleGroupConfigurationWidget::UpdateData", "Label field is required for a group" );
	}

	m_Group->SetLabel( m_txtLabel->GetValue().ToStdString() );
	m_Group->SetDescription( m_txtDescription->GetValue().ToStdString() );
	m_Group->SetAdvanced( m_chkAdvanced->GetValue( ) ? "true" : "false" );
}

void Core::Widgets::ModuleGroupConfigurationWidget::UpdateWidget( )
{
	m_txtLabel->SetValue( _U( m_Group->GetLabel( ) ) );
	m_txtDescription->SetValue( _U( m_Group->GetDescription( ) ) );
	m_chkAdvanced->SetValue( m_Group->GetAdvanced( ) == "true"? true : false );
}
