/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreModuleParamConfigurationWidget_H
#define _coreModuleParamConfigurationWidget_H

#include "coreModuleParamConfigurationWidgetUI.h"
#include "coreCustomApplicationWizardPage.h"

namespace Core
{
namespace Widgets
{

class ModuleParamConfigurationWidget : 
	public coreModuleParamConfigurationWidgetUI,
	public CustomApplicationWizardPage
{
public:
	DECLARE_CLASS( ModuleParamConfigurationWidget )
	//!
    ModuleParamConfigurationWidget(ModuleParameter *parameter, wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

	//! Redefined
	virtual void UpdateData( );

	//! Redefined
	virtual void UpdateWidget( );

private:

protected:
	//!
	ModuleParameter *m_Parameter;
};

}
}

#endif // _coreModuleParamConfigurationWidget_H
