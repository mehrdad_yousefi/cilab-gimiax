/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreModuleConfigurationWidget.h"

#include "wxUnicode.h"

IMPLEMENT_CLASS( Core::Widgets::ModuleConfigurationWidget, coreModuleConfigurationWidgetUI )

Core::Widgets::ModuleConfigurationWidget::ModuleConfigurationWidget
	(ModuleDescription *module, wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    coreModuleConfigurationWidgetUI(parent, id, pos, size, style)
{
	m_Module = module;
	UpdateWidget( );
}

void Core::Widgets::ModuleConfigurationWidget::UpdateData( )
{
	if ( m_txtCategory->IsEmpty( ) )
	{
		throw Core::Exceptions::Exception( "ModuleConfigurationWidget::UpdateData", "Category field is required" );
	}

	if ( m_txtTitle->IsEmpty( ) )
	{
		throw Core::Exceptions::Exception( "ModuleConfigurationWidget::UpdateData", "Title field is required" );
	}

	if ( m_txtExecutable->IsEmpty( ) )
	{
		throw Core::Exceptions::Exception( "ModuleConfigurationWidget::UpdateData", "Executable field is required" );
	}

	m_Module->SetTitle( m_txtTitle->GetValue().ToStdString() );
	m_Module->SetCategory( m_txtCategory->GetValue().ToStdString() );
	m_Module->SetDescription( m_txtDescription->GetValue().ToStdString() );
	m_Module->SetContributor( m_txtContributor->GetValue().ToStdString() );
	m_Module->SetExecutableApp( m_txtExecutable->GetValue().ToStdString() );
	m_Module->SetWorkingDirectory( m_txtWorkingDirectory->GetValue().ToStdString() );
	m_Module->SetVersion( m_txtVersion->GetValue().ToStdString() );
	m_Module->SetLicense( m_txtLicense->GetValue().ToStdString() );
	m_Module->SetDocumentationURL( m_txtDocumentationURL->GetValue().ToStdString() );
	m_Module->SetAcknowledgements( m_txtAcknowledgements->GetValue().ToStdString() );
}

void Core::Widgets::ModuleConfigurationWidget::UpdateWidget( )
{
	m_txtTitle->SetValue( _U( m_Module->GetTitle( ) ) );
	m_txtCategory->SetValue( _U( m_Module->GetCategory( ) ) );
	m_txtDescription->SetValue( _U( m_Module->GetDescription( ) ) );
	m_txtContributor->SetValue( _U( m_Module->GetContributor( ) ) );
	m_txtExecutable->SetValue( _U( m_Module->GetExecutableApp( ) ) );
	m_txtWorkingDirectory->SetValue( _U( m_Module->GetWorkingDirectory( ) ) );
	m_txtVersion->SetValue( _U( m_Module->GetVersion( ) ) );
	m_txtLicense->SetValue( _U( m_Module->GetLicense( ) ) );
	m_txtDocumentationURL->SetValue( _U( m_Module->GetDocumentationURL( ) ) );
	m_txtAcknowledgements->SetValue( _U( m_Module->GetAcknowledgements(  ) ) );
}

void Core::Widgets::ModuleConfigurationWidget::OnBrowseExecutable(wxCommandEvent &event)
{
	wxFileDialog openFileDialog(this, wxT("Open executable"), wxT(""), wxT(""), wxT("All files (*.*)|*.*;*.;*"), 
		wxFD_OPEN | wxFD_FILE_MUST_EXIST );

	if ( !m_txtExecutable->IsEmpty( ) )
	{
		openFileDialog.SetFilename( m_txtExecutable->GetValue( ) );
	}

	if(openFileDialog.ShowModal() == wxID_OK)
	{
		m_txtExecutable->SetValue( openFileDialog.GetPath() );
	}
}


void Core::Widgets::ModuleConfigurationWidget::OnBrowseWorkingDirectory(wxCommandEvent &event)
{
	wxDirDialog openDirectoryDialog( this, wxT("Directory"), wxT(""));

	if ( !m_txtWorkingDirectory->IsEmpty( ) )
	{
		openDirectoryDialog.SetPath( m_txtWorkingDirectory->GetValue( ) );
	}

	if(openDirectoryDialog.ShowModal() == wxID_OK)
	{
		m_txtWorkingDirectory->SetValue( openDirectoryDialog.GetPath() );
	}

}
