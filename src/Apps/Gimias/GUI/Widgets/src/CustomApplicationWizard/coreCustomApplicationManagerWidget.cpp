/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreCustomApplicationManagerWidget.h"
#include "coreWxMitkGraphicalInterface.h"
#include "coreModuleDescriptionSearch.h"

#include "dynModuleXMLWriter.h"

BEGIN_EVENT_TABLE(Core::Widgets::CustomApplicationManagerWidget, coreCustomApplicationManagerWidgetUI)
END_EVENT_TABLE()


Core::Widgets::CustomApplicationManagerWidget::CustomApplicationManagerWidget(wxWindow* parent, int id):
    coreCustomApplicationManagerWidgetUI(parent, id, "Custom Application Manager")
{
	m_EditorWidget = new CustomApplicationWizard( this , wxID_ANY );

	// Add observer to plugin provider holder to update list of available plugins
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
	Runtime::PluginProviderManager::Pointer pluginProviderManager;
	pluginProviderManager = graphicalIface->GetPluginProviderManager();
	pluginProviderManager->GetStateHolder()->AddObserver1( this, &CustomApplicationManagerWidget::OnPluginManagerModified );
}

void Core::Widgets::CustomApplicationManagerWidget::UpdateWidget()
{
	m_Tree->DeleteAllItems();
	m_Tree->AddRoot(wxT( "Applications" ), -1, -1);

	// Search ext app modules
	ModuleDescriptionSearch::Pointer search = ModuleDescriptionSearch::New( );
	search->ScanOnlyEnabled( false );
	search->AddType( "ExternalApp" );
	search->Update( );
	std::list<ModuleDescription> moduleList = search->GetFoundModuleList();

	// Populate the tree
	std::list<ModuleDescription>::iterator itModule;
	for ( itModule = moduleList.begin() ; itModule != moduleList.end() ; itModule++ )
	{
		std::string moduleTitle = itModule->GetTitle();

		wxTreeItemId item;
		item = m_Tree->AppendItem( m_Tree->GetRootItem(), moduleTitle );
	}

	m_Tree->ExpandAll();
}

void Core::Widgets::CustomApplicationManagerWidget::OnBtnNew(wxCommandEvent &event)
{
	try
	{
		m_TemporalModule = ModuleDescription( );
		m_TemporalModule.SetTitle( "" );
		m_TemporalModule.SetType( "ExternalApp" );

		m_EditorWidget->SetModuleDescription( &m_TemporalModule );
		std::string moduleName = m_EditorWidget->CheckRenameModule( );
		if ( moduleName.empty( ) )
		{
			return;
		}

		m_TemporalModule.SetTitle( moduleName );
		m_EditorWidget->Center( );
		m_EditorWidget->Show();

	}
	coreCatchExceptionsReportAndNoThrowMacro( CustomApplicationManagerWidget::OnBtnNew )

}

void Core::Widgets::CustomApplicationManagerWidget::OnBtnEdit(wxCommandEvent &event)
{
	try
	{
		m_TemporalModule = GetSelectedModule( );
		if ( m_TemporalModule.GetTitle( ) == "Unknown" )
		{
			return;
		}
		m_EditorWidget->SetModuleDescription( &m_TemporalModule );
		m_EditorWidget->Center( );
		m_EditorWidget->Show();
	}
	coreCatchExceptionsReportAndNoThrowMacro( CustomApplicationManagerWidget::OnBtnEdit )
}

void Core::Widgets::CustomApplicationManagerWidget::OnBtnRename(wxCommandEvent &event)
{
	try
	{
		Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();

		m_TemporalModule = GetSelectedModule( );
		if ( m_TemporalModule.GetTitle( ) == "Unknown" )
		{
			return;
		}

		// Get previous name and rename it
		m_EditorWidget->SetModuleDescription( &m_TemporalModule );
		std::string newModuleName = m_EditorWidget->CheckRenameModule( );
		if ( newModuleName.empty( ) )
		{
			return;
		}
		
		// Remove old file from disk
		std::string oldFilename = m_EditorWidget->GetModuleLocation( );
		itksys::SystemTools::RemoveFile( oldFilename.c_str( ) );

		// Remove module if loaded
		m_EditorWidget->RemoveLoadedModule( );
		
		// Write to disk with the new name and empty location
		m_TemporalModule.SetTitle( newModuleName );
		m_TemporalModule.SetLocation( "" );
		std::string filename = m_EditorWidget->GetModuleLocation( );
		dynModuleXMLWriter::Pointer writer = dynModuleXMLWriter::New( );
		writer->SetModule( &m_TemporalModule );
		writer->SetFileName( filename );
		writer->Update( );

		// Reload from disk
		m_EditorWidget->ReLoadModuleDescription( );
	}
	coreCatchExceptionsReportAndNoThrowMacro( CustomApplicationManagerWidget::OnBtnEdit )
}

void Core::Widgets::CustomApplicationManagerWidget::OnBtnDelete(wxCommandEvent &event)
{
	try
	{
		m_TemporalModule = GetSelectedModule( );
		if ( m_TemporalModule.GetTitle( ) == "Unknown" )
		{
			return;
		}

		m_EditorWidget->SetModuleDescription( &m_TemporalModule );
		m_EditorWidget->RemoveLoadedModule( );

		// Remove file
		std::string filename = m_EditorWidget->GetModuleLocation( );
		itksys::SystemTools::RemoveFile( filename.c_str( ) );

		// Reload plugins
		Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
		graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
		Runtime::PluginProviderManager::Pointer pluginProviderManager;
		pluginProviderManager = graphicalIface->GetPluginProviderManager();
		pluginProviderManager->ScanPlugins( );

		UpdateWidget( );
	}
	coreCatchExceptionsReportAndNoThrowMacro( CustomApplicationManagerWidget::OnBtnDelete )

}

bool Core::Widgets::CustomApplicationManagerWidget::Show( bool show /*= true */ )
{
	if ( show )
	{
		UpdateWidget();
	}
	return coreCustomApplicationManagerWidgetUI::Show( show );
}

ModuleDescription Core::Widgets::CustomApplicationManagerWidget::GetSelectedModule()
{
	wxTreeItemId item = m_Tree->GetSelection();
	if ( !item.IsOk( ) )
	{
		return ModuleDescription();
	}

	ModuleDescriptionSearch::Pointer search = ModuleDescriptionSearch::New( );
	search->ScanOnlyEnabled( false );
	search->AddType( "ExternalApp" );
	search->SetModuleTitle( m_Tree->GetItemText( item ).ToStdString() );
	search->Update( );
	std::list<ModuleDescription> moduleList = search->GetFoundModuleList();
	if ( moduleList.empty() )
	{
		return ModuleDescription();
	}

	return *moduleList.begin( );
}

void Core::Widgets::CustomApplicationManagerWidget::OnBtnImport( wxCommandEvent &event )
{
	try
	{
		wxFileDialog* openFileDialog = 
			new wxFileDialog(this, wxT("Import a workflow file"), wxT(""), wxT(""), wxT("*.xml"), 
			wxFD_OPEN | wxFD_FILE_MUST_EXIST );
		
		if(openFileDialog->ShowModal() == wxID_OK)
		{
			// Source and destination filenames
			std::string srcFileName = std::string( openFileDialog->GetPath().c_str() );
			std::string filename = itksys::SystemTools::GetFilenameWithoutExtension( srcFileName );
			Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
			std::string dstFilename = std::string( "$(AppData)/Descriptions" ) + "/" + filename + ".xml";
			settings->ReplaceGimiasPath( dstFilename );

			// Check if destination exists
			if ( itksys::SystemTools::FileExists( dstFilename.c_str( ) ) )
			{
				wxMessageDialog overwriteExistingFolderDialog(
					this, wxT("Overwrite existing file?"), wxT("Ovewrite"), wxYES_NO /*| wxCANCEL*/);
				if ( overwriteExistingFolderDialog.ShowModal() == wxID_NO )
				{
					return ;
				}
			}

			// Copy the file
			itksys::SystemTools::CopyAFile( srcFileName.c_str( ), dstFilename.c_str( ) );

			// Scan all plugins
			Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
			graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();
			Runtime::PluginProviderManager::Pointer pluginProviderManager;
			pluginProviderManager = graphicalIface->GetPluginProviderManager();
			pluginProviderManager->ScanPlugins( );

		}
	}
	coreCatchExceptionsReportAndNoThrowMacro( CustomApplicationManagerWidget::OnEndLabelEdit )
}

void Core::Widgets::CustomApplicationManagerWidget::OnBtnCopy(wxCommandEvent &event)
{
	try
	{
		Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();

		m_TemporalModule = GetSelectedModule( );
		if ( m_TemporalModule.GetTitle( ) == "Unknown" )
		{
			return;
		}

		// Get previous name and rename it
		m_EditorWidget->SetModuleDescription( &m_TemporalModule );
		
		// Check new title
		std::string newModuleName = m_TemporalModule.GetTitle( );
		ModuleDescriptionSearch::Pointer search = ModuleDescriptionSearch::New( );
		search->ScanOnlyEnabled( false );
		search->AddType( "ExternalApp" );
		search->SetModuleTitle( newModuleName );
		search->Update( );
		std::list<ModuleDescription> moduleList = search->GetFoundModuleList();
		while ( !moduleList.empty( ) )
		{
			newModuleName = newModuleName + " Copy";
			search->SetModuleTitle( newModuleName );
			search->Update( );
			moduleList = search->GetFoundModuleList();
		}

		// Write XML to disk
		m_TemporalModule.SetTitle( newModuleName );
		m_TemporalModule.SetLocation( "" );
		std::string filename = m_EditorWidget->GetModuleLocation( );
		dynModuleXMLWriter::Pointer writer = dynModuleXMLWriter::New( );
		writer->SetModule( &m_TemporalModule );
		writer->SetFileName( filename );
		writer->Update( );

		// Reload from disk
		m_EditorWidget->ReLoadModuleDescription( );
	}
	coreCatchExceptionsReportAndNoThrowMacro( CustomApplicationManagerWidget::OnBtnEdit )
}

void Core::Widgets::CustomApplicationManagerWidget::OnPluginManagerModified( 
	Core::Runtime::PluginProviderManager::STATE state )
{
	if ( state == Core::Runtime::PluginProviderManager::STATE_IDLE )
	{
		UpdateWidget( );
	}
}

void Core::Widgets::CustomApplicationManagerWidget::OnItemActivated(wxTreeEvent &event)
{
	try
	{
	    wxCommandEvent commandEvent = wxCommandEvent( );
		OnBtnEdit( commandEvent );
		event.Skip();
	}
	coreCatchExceptionsReportAndNoThrowMacro( CustomApplicationManagerWidget::OnEndLabelEdit )
}
