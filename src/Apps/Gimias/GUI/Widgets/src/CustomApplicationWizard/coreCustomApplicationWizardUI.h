// -*- C++ -*- generated by wxGlade 0.6.3 on Thu Jan 26 17:35:35 2012

#include <wx/wx.h>
#include <wx/image.h>

#ifndef CORECUSTOMAPPLICATIONWIZARDUI_H
#define CORECUSTOMAPPLICATIONWIZARDUI_H

// begin wxGlade: ::dependencies
// end wxGlade

// begin wxGlade: ::extracode
#include "wxID.h"
#include <wx/treebook.h>

#define wxID_ADD wxID( "wxID_ADD" )
#define wxID_REMOVE wxID( "wxID_REMOVE" )
// end wxGlade


class coreCustomApplicationWizardUI: public wxDialog {
public:
    // begin wxGlade: coreCustomApplicationWizardUI::ids
    // end wxGlade

    coreCustomApplicationWizardUI(wxWindow* parent, int id, const wxString& title, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=wxDEFAULT_DIALOG_STYLE);

private:
    // begin wxGlade: coreCustomApplicationWizardUI::methods
    void set_properties();
    void do_layout();
    // end wxGlade

protected:
    // begin wxGlade: coreCustomApplicationWizardUI::attributes
    wxStaticBox* sizer_4_staticbox;
    wxBitmapButton* m_btnAdd;
    wxBitmapButton* m_btnRemove;
    wxTreebook* m_ConfigurationTree;
    wxTextCtrl* m_txtCommandLine;
    wxButton* m_btnOK;
    wxButton* m_btnApply;
    wxButton* m_btnCancel;
    // end wxGlade

    wxDECLARE_EVENT_TABLE();

public:
    virtual void OnAdd(wxCommandEvent &event); // wxGlade: <event_handler>
    virtual void OnRemove(wxCommandEvent &event); // wxGlade: <event_handler>
    virtual void OnOK(wxCommandEvent &event); // wxGlade: <event_handler>
    virtual void OnApply(wxCommandEvent &event); // wxGlade: <event_handler>
    virtual void OnCancel(wxCommandEvent &event); // wxGlade: <event_handler>
}; // wxGlade: end class


#endif // CORECUSTOMAPPLICATIONWIZARDUI_H
