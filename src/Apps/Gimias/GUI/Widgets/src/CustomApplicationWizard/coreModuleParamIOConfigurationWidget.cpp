/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreModuleParamIOConfigurationWidget.h"

#include "wxUnicode.h"

IMPLEMENT_CLASS( Core::Widgets::ModuleParamIOConfigurationWidget, coreModuleParamIOConfigurationWidgetUI )

Core::Widgets::ModuleParamIOConfigurationWidget::ModuleParamIOConfigurationWidget
	(ModuleParameter *parameter, wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    coreModuleParamIOConfigurationWidgetUI(parent, id, pos, size, style)
{
	m_Parameter = parameter;
	UpdateWidget( );
}

void Core::Widgets::ModuleParamIOConfigurationWidget::UpdateData( )
{
	if ( m_cmbType->GetValue( ).IsEmpty( ) )
	{
		throw Core::Exceptions::Exception( "ModuleParamIOConfigurationWidget::UpdateData", "Type field is required for a IO parameter" );
	}

	if ( m_cmbChannel->GetValue( ).IsEmpty( ) )
	{
		throw Core::Exceptions::Exception( "ModuleParamIOConfigurationWidget::UpdateData", "Channel field is required for a IO parameter" );
	}

	if ( m_txtName->IsEmpty( ) )
	{
		throw Core::Exceptions::Exception( "ModuleParamIOConfigurationWidget::UpdateData", "Name field is required for a IO parameter" );
	}

	if ( m_cmbFormat->/*IsEnabled()*/IsThisEnabled() && m_cmbFormat->GetValue( ).IsEmpty( ) )
	{
		throw Core::Exceptions::Exception( "ModuleParamIOConfigurationWidget::UpdateData", "Format field is required for a IO parameter" );
	}

	if ( m_txtIndex->/*IsEnabled()*/IsThisEnabled() && m_txtIndex->IsEmpty( ) )
	{
		throw Core::Exceptions::Exception( "ModuleParamIOConfigurationWidget::UpdateData", "Index field is required for a IO parameter" );
	}

	m_Parameter->SetTag( m_cmbType->GetValue().ToStdString() );
	m_Parameter->SetChannel( m_cmbChannel->GetValue().ToStdString() );
	m_Parameter->SetName( m_txtName->GetValue().ToStdString() );
	m_Parameter->SetFileExtensionsAsString( m_txtFileExtensions->GetValue().ToStdString() );
	m_Parameter->SetLabel( m_txtLabel->GetValue().ToStdString() );
	m_Parameter->SetDescription( m_txtDescription->GetValue().ToStdString() );

	if ( m_cmbFormat->/*IsEnabled()*/IsThisEnabled() )
	{
		m_Parameter->SetFormat( m_cmbFormat->GetValue().ToStdString() );
		m_Parameter->SetIndex( "" );
	}
	if ( m_txtIndex->/*IsEnabled()*/IsThisEnabled() )
	{
		m_Parameter->SetIndex( m_txtIndex->GetValue().ToStdString() );
		m_Parameter->SetFormat( "" );
	}
}

void Core::Widgets::ModuleParamIOConfigurationWidget::UpdateWidget( )
{
	m_cmbType->SetValue( _U( m_Parameter->GetTag( ) ) );
	m_cmbChannel->SetValue( _U( m_Parameter->GetChannel( ) ) );
	m_txtName->SetValue( _U( m_Parameter->GetName( ) ) );
	m_cmbFormat->SetValue( _U( m_Parameter->GetFormat( ) ) );
	m_txtFileExtensions->SetValue( _U( m_Parameter->GetFileExtensionsAsString( ) ) );
	m_txtLabel->SetValue( _U( m_Parameter->GetLabel( ) ) );
	m_txtIndex->SetValue( _U( m_Parameter->GetIndex( ) ) );
	m_txtDescription->SetValue( _U( m_Parameter->GetDescription( ) ) );

	m_rbtnFormat->SetValue( m_txtIndex->GetValue( ).IsEmpty( ) );
	m_rbtnIndex->SetValue( !m_txtIndex->GetValue( ).IsEmpty( ) );

	UpdateRadioState( );
}

void Core::Widgets::ModuleParamIOConfigurationWidget::OnRadioButton(wxCommandEvent &event)
{
	UpdateRadioState( );
}

void Core::Widgets::ModuleParamIOConfigurationWidget::UpdateRadioState( )
{
	m_cmbFormat->Enable( m_rbtnFormat->GetValue( ) );
	m_txtIndex->Enable( m_rbtnIndex->GetValue( ) );
}
