/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreModuleGroupConfigurationWidget_H
#define _coreModuleGroupConfigurationWidget_H

#include "coreModuleGroupConfigurationWidgetUI.h"
#include "coreCustomApplicationWizardPage.h"

namespace Core
{
namespace Widgets
{

class ModuleGroupConfigurationWidget : 
	public coreModuleGroupConfigurationWidgetUI,
	public CustomApplicationWizardPage
{
public:
	DECLARE_CLASS( ModuleGroupConfigurationWidget )
	//!
	ModuleGroupConfigurationWidget(ModuleParameterGroup *group, wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

	//! Redefined
	virtual void UpdateData( );

	//! Redefined
	virtual void UpdateWidget( );

private:

protected:
	//!
	ModuleParameterGroup *m_Group;
};

}
}

#endif // _coreModuleGroupConfigurationWidget_H
