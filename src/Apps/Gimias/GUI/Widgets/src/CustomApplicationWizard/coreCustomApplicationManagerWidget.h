/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreCustomApplicationManagerWidget_H
#define _coreCustomApplicationManagerWidget_H

#include "coreCustomApplicationManagerWidgetUI.h"
#include "coreCustomApplicationWizard.h"
#include "ModuleDescription.h"
#include "corePluginProviderManager.h"

namespace Core{
namespace Widgets{

/** 
\brief Manager for all available custom applications
\ingroup gmWidgets
\author Xavi Planes
\date Jan 2012
*/
class CustomApplicationManagerWidget : public coreCustomApplicationManagerWidgetUI {
public:

	//!
    CustomApplicationManagerWidget(wxWindow* parent, int id);

	//!
	bool Show(bool show = true );

protected:
    wxDECLARE_EVENT_TABLE();

    void OnBtnNew(wxCommandEvent &event);
    void OnBtnEdit(wxCommandEvent &event);
	void OnBtnDelete(wxCommandEvent &event);
	void OnBtnImport(wxCommandEvent &event);
    void OnBtnRename(wxCommandEvent &event);
    void OnItemActivated(wxTreeEvent &event);
    void OnBtnCopy(wxCommandEvent &event);

	//!
	ModuleDescription GetSelectedModule();

	//!
	void UpdateWidget( );
	
	//! Update list of available plugins
	void OnPluginManagerModified( Core::Runtime::PluginProviderManager::STATE state );

private:
	//!
	CustomApplicationWizard* m_EditorWidget;
	//! When new button is pressed
	ModuleDescription m_TemporalModule;
};


} // Widgets
} // Core


#endif // _coreCustomApplicationManagerWidget_H
