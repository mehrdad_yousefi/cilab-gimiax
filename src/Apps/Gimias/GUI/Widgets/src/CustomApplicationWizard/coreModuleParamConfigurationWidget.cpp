/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreModuleParamConfigurationWidget.h"

#include "wxUnicode.h"

IMPLEMENT_CLASS( Core::Widgets::ModuleParamConfigurationWidget, coreModuleParamConfigurationWidgetUI )

Core::Widgets::ModuleParamConfigurationWidget::ModuleParamConfigurationWidget
	(ModuleParameter *parameter, wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    coreModuleParamConfigurationWidgetUI(parent, id, pos, size, style)
{
	m_Parameter = parameter;
	UpdateWidget( );
}

void Core::Widgets::ModuleParamConfigurationWidget::UpdateData( )
{
	if ( m_cmbType->GetValue( ).IsEmpty( ) )
	{
		throw Core::Exceptions::Exception( "ModuleParamConfigurationWidget::UpdateData", "Type field is required for a parameter" );
	}

	if ( m_txtName->IsEmpty( ) )
	{
		throw Core::Exceptions::Exception( "ModuleParamConfigurationWidget::UpdateData", "Name field is required for a parameter" );
	}

	if ( m_cmbFormat->GetValue( ).IsEmpty( ) )
	{
		throw Core::Exceptions::Exception( "ModuleParamConfigurationWidget::UpdateData", "Format field is required for a parameter" );
	}

	m_Parameter->SetTag( m_cmbType->GetValue().ToStdString() );
	m_Parameter->SetName( m_txtName->GetValue().ToStdString() );
	m_Parameter->SetFormat( m_cmbFormat->GetValue().ToStdString() );
	m_Parameter->SetLabel( m_txtLabel->GetValue().ToStdString() );
	m_Parameter->SetDefault( m_txtDefault->GetValue().ToStdString() );
	m_Parameter->SetDescription( m_txtDescription->GetValue().ToStdString() );
}

void Core::Widgets::ModuleParamConfigurationWidget::UpdateWidget( )
{
	m_cmbType->SetValue( _U( m_Parameter->GetTag( ) ) );
	m_txtName->SetValue( _U( m_Parameter->GetName( ) ) );
	m_cmbFormat->SetValue( _U( m_Parameter->GetFormat( ) ) );
	m_txtLabel->SetValue( _U( m_Parameter->GetLabel( ) ) );
	m_txtDefault->SetValue( _U( m_Parameter->GetDefault( ) ) );
	m_txtDescription->SetValue( _U( m_Parameter->GetDescription( ) ) );
}
