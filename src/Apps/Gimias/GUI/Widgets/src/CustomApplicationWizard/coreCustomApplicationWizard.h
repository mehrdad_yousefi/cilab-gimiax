/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreCustomApplicationWizard_H
#define _coreCustomApplicationWizard_H

#include "gmWidgetsWin32Header.h"
#include "coreObject.h"
#include "coreCustomApplicationWizardUI.h"
#include "corePluginProvider.h"

namespace Core
{
namespace Widgets
{
/** 
\brief Select the plugins to load.

\ingroup gmWidgets
\author Xavi Planes
\date Nov 2010
*/
class GMWIDGETS_EXPORT CustomApplicationWizard : public coreCustomApplicationWizardUI
{
public:
	//!
	CustomApplicationWizard(
		wxWindow* parent, int id, const wxString& title = "Application Wizard", 
		const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, 
		long style=wxDEFAULT_DIALOG_STYLE);

	//!
	~CustomApplicationWizard(void);

	//! 
	bool Show(bool show = true );

	//!
	wxTreebook* GetPreferencesWindow() const;

	//!
	ModuleDescription *GetModuleDescription( ) const;

	//!
	void SetModuleDescription( ModuleDescription *module );

	//! Remove module m_Module from the provider and load plugins from disk
	void ReLoadModuleDescription( );

	//! Remove module m_Module from the provider and load plugins from disk
	void RemoveLoadedModule( );

	/** Check for new module name and if file exists
	\return new module name
	*/
	std::string CheckRenameModule( );

	//! Use location or title to find the module location
	std::string GetModuleLocation( );

	//! Get plugin provider for a concrete module
	Core::Runtime::PluginProvider::Pointer GetPluginProvider( const std::string &moduleTitle );

	//!
	void UpdateWidget( );

	//!
	void UpdateData( );

	//!
	void CheckParsingErrors( );

protected:

	//!
	void UpdateCommandLineExample( );

	//!
	void OnOK(wxCommandEvent &event);
	void OnCancel(wxCommandEvent &event);
	void OnApply(wxCommandEvent &event);
    void OnAdd(wxCommandEvent &event);
    void OnRemove(wxCommandEvent &event);

private:
	//!
	ModuleDescription *m_Module;
};
}
}

#endif // _coreCustomApplicationWizard_H
