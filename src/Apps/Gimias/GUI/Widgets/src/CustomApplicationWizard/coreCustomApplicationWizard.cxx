/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

// For compilers that don't support precompilation, include "wx/wx.h"
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "wx/wupdlock.h"

#include "coreCustomApplicationWizard.h"
#include "coreCustomApplicationWizardPage.h"
#include "coreModuleConfigurationWidget.h"
#include "coreModuleGroupConfigurationWidget.h"
#include "coreModuleParamConfigurationWidget.h"
#include "coreModuleParamIOConfigurationWidget.h"

#include "coreDynProcessor.h"
#include "coreModuleDescriptionSearch.h"
#include "corePluginProviderManager.h"

#include "add.xpm"
#include "remove.xpm"

#include "dynModuleHelper.h"
#include "dynModuleXMLWriter.h"

#include "ModuleDescriptionParser.h"

#include "blTextUtils.h"

#include <fstream>

using namespace Core::Widgets;

Core::Widgets::CustomApplicationWizard::CustomApplicationWizard( 
	wxWindow* parent, int id, const wxString& title, 
	const wxPoint& pos/*=wxDefaultPosition*/, const wxSize& size/*=wxDefaultSize*/, 
	long style/*=wxDEFAULT_DIALOG_STYLE*/ )
	: coreCustomApplicationWizardUI(parent, id, title, pos, size, wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER|wxMAXIMIZE_BOX|wxMINIMIZE_BOX)
{
	m_btnAdd->SetBitmapLabel( wxBitmap( add_xpm ) );
	m_btnRemove->SetBitmapLabel( wxBitmap( remove_xpm ) );
}

CustomApplicationWizard::~CustomApplicationWizard(void)
{

}

ModuleDescription *Core::Widgets::CustomApplicationWizard::GetModuleDescription( ) const
{
	return m_Module;
}

void Core::Widgets::CustomApplicationWizard::SetModuleDescription( 
	ModuleDescription *module )
{
	m_Module = module;
}

void Core::Widgets::CustomApplicationWizard::UpdateWidget()
{
	wxWindowUpdateLocker locker( this );


	int selection = m_ConfigurationTree->GetSelection( );

	// Module configuration
	m_ConfigurationTree->DeleteAllPages( );
	m_ConfigurationTree->AddPage( new ModuleConfigurationWidget( m_Module, m_ConfigurationTree, wxID_ANY ), wxT( "Root" ), true );

	std::vector<ModuleParameterGroup>::iterator pgIt;
	for ( pgIt = m_Module->GetParameterGroups().begin();
		pgIt != m_Module->GetParameterGroups().end(); ++pgIt)
	{
		wxWindow *page = new ModuleGroupConfigurationWidget( &(*pgIt), m_ConfigurationTree, wxID_ANY );
		m_ConfigurationTree->AddSubPage( page, pgIt->GetLabel( ), true );
		int groupPagePos = m_ConfigurationTree->GetSelection();

		bool isIOGroup = pgIt->GetLabel( ) == "IO" ? true : false;

		std::vector<ModuleParameter>::iterator pBeginIt = pgIt->GetParameters().begin();
		std::vector<ModuleParameter>::iterator pEndIt = pgIt->GetParameters().end();
		for (std::vector<ModuleParameter>::iterator pIt = pBeginIt; pIt != pEndIt; ++pIt)
		{
			wxWindow* subPage;
			if ( isIOGroup )
			{
				subPage = new ModuleParamIOConfigurationWidget( &(*pIt), m_ConfigurationTree, wxID_ANY );
			}
			else
			{
				subPage = new ModuleParamConfigurationWidget( &(*pIt), m_ConfigurationTree, wxID_ANY );
			}
			m_ConfigurationTree->InsertSubPage( groupPagePos, subPage, pIt->GetName( ) , true );
		}
	}

	if ( selection > 0 && selection < m_ConfigurationTree->GetPageCount( ) )
	{
		m_ConfigurationTree->SetSelection( selection );
	}

	// Send a resize event to update the size of m_EmbededWindow
	wxSizeEvent resEvent(m_ConfigurationTree->GetBestSize(), m_ConfigurationTree->GetId());
	resEvent.SetEventObject(m_ConfigurationTree);
	m_ConfigurationTree->GetEventHandler()->ProcessEvent(resEvent);
}

void Core::Widgets::CustomApplicationWizard::OnOK( wxCommandEvent &event )
{
	UpdateData( );

	event.Skip();
}

void Core::Widgets::CustomApplicationWizard::OnCancel( wxCommandEvent &event )
{
	event.Skip();
}

bool Core::Widgets::CustomApplicationWizard::Show( bool show /*= true */ )
{
	if ( show )
	{
		UpdateWidget( );

		// Update widgets
		Core::Widgets::CustomApplicationWizardPage* page;
		for ( size_t i = 0 ; i < m_ConfigurationTree->GetPageCount( ) ; i++ )
		{
			page = dynamic_cast<Core::Widgets::CustomApplicationWizardPage*> ( m_ConfigurationTree->GetPage( i ) );
			if ( page )
			{
				page->UpdateWidget();
			}
		}

		UpdateCommandLineExample( );
	}

	return coreCustomApplicationWizardUI::Show( show );
}

void Core::Widgets::CustomApplicationWizard::OnApply( wxCommandEvent &event )
{
	UpdateData( );

	event.Skip();
}

void Core::Widgets::CustomApplicationWizard::OnAdd( wxCommandEvent &event )
{
	int selection = m_ConfigurationTree->GetSelection( );
	if ( selection == -1 )
	{
		return;
	}

	wxWindow* window = m_ConfigurationTree->GetPage( selection );
	std::string pageName = std::string( m_ConfigurationTree->GetPageText( selection ).c_str() );
	if ( window->IsKindOf( CLASSINFO(ModuleConfigurationWidget) ) )
	{
		// Create a new group
		ModuleParameterGroup group;
		std::string groupName = "Group";
		int index = 0;
		while ( dynModuleHelper::FindParameterGroup( m_Module, groupName ) )
		{
			std::stringstream stream;
			stream << "Group" << index;
			groupName = stream.str( );
			index++;
		}
		group.SetLabel( groupName );
		m_Module->AddParameterGroup( group );
		UpdateWidget();
	}
	else if ( window->IsKindOf( CLASSINFO(ModuleGroupConfigurationWidget) ) ||
			  window->IsKindOf( CLASSINFO(ModuleParamConfigurationWidget) ) ||
			  window->IsKindOf( CLASSINFO(ModuleParamIOConfigurationWidget) ) )
	{
		// Get selected group
		std::vector<ModuleParameterGroup>::iterator pgIt;
		for ( pgIt = m_Module->GetParameterGroups().begin();
			pgIt != m_Module->GetParameterGroups().end(); ++pgIt)
		{
			if ( pgIt->GetLabel( ) == pageName )
			{
				// Create a new parameter
				ModuleParameter parameter;
				std::string name = "Parameter";
				int index = 0;
				while ( dynModuleHelper::FindParameter( m_Module, "", 0, name, "" ) != NULL )
				{
					std::stringstream stream;
					stream << "Parameter" << index;
					name = stream.str( );
					index++;
				}
				parameter.SetName( name );
				if ( window->IsKindOf( CLASSINFO(ModuleParamIOConfigurationWidget) ) ||
					 pageName == "IO" )
				{
					parameter.SetTag( "image" );
					parameter.SetChannel( "input" );
					parameter.SetIndex( "0" );
				}
				else
				{
					parameter.SetTag( "integer" );
					parameter.SetFormat( "--flag $(value)" );
				}

				pgIt->AddParameter( parameter );
			}
		}

		UpdateWidget();
	}
}

void Core::Widgets::CustomApplicationWizard::OnRemove( wxCommandEvent &event )
{
	int selection = m_ConfigurationTree->GetSelection( );
	if ( selection == -1 )
	{
		return;
	}

	int pageParentSelection = m_ConfigurationTree->GetPageParent( selection );
	if ( pageParentSelection == -1 )
	{
		return;
	}

	wxWindow* window = m_ConfigurationTree->GetPage( selection );
	std::string pageName = std::string( m_ConfigurationTree->GetPageText( selection ).c_str() );
	std::string pageParentName = std::string( m_ConfigurationTree->GetPageText( pageParentSelection ).c_str() );
	
	if ( window->IsKindOf( CLASSINFO(ModuleGroupConfigurationWidget) ) )
	{
		// Erase parameter group
		dynModuleHelper::RemoveParameterGroup( m_Module, pageName );
		UpdateWidget( );
	}
	else if ( window->IsKindOf( CLASSINFO(ModuleParamConfigurationWidget) ) ||
			  window->IsKindOf( CLASSINFO(ModuleParamIOConfigurationWidget) ) )
	{

		dynModuleHelper::RemoveParameter( m_Module, pageParentName, pageName );
		UpdateWidget();
	}
}

void Core::Widgets::CustomApplicationWizard::UpdateData( )
{
	try
	{
		// Update data of ModuleDescription
		Core::Widgets::CustomApplicationWizardPage* page;
		for ( size_t i = 0 ; i < m_ConfigurationTree->GetPageCount( ) ; i++ )
		{
			page = dynamic_cast<Core::Widgets::CustomApplicationWizardPage*> ( m_ConfigurationTree->GetPage( i ) );
			if ( page )
			{
				page->UpdateData();
			}
		}

		// Check parsing errors
		CheckParsingErrors( );

		// Update control
		UpdateCommandLineExample( );

		// Create directory if doesn't exist
		std::string path = itksys::SystemTools::GetFilenamePath( GetModuleLocation( ) );
		itksys::SystemTools::MakeDirectory( path.c_str( ) );

		// Write XML file
		dynModuleXMLWriter::Pointer writer = dynModuleXMLWriter::New( );
		writer->SetModule( m_Module );
		writer->SetFileName( GetModuleLocation( ) );
		writer->Update( );

		// Update orignal ModuleDescription
		ReLoadModuleDescription( );

		// Update group/parameter name
		UpdateWidget( );
	}
	coreCatchExceptionsReportAndNoThrowMacro(CustomApplicationWizard::OnApply)
}

void Core::Widgets::CustomApplicationWizard::CheckParsingErrors( )
{
	// Write a temporary file and check for errors when parsing the XML
	Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
	std::string filename = std::string( "$(AppData)/temp.xml" );
	settings->ReplaceGimiasPath( filename );
	dynModuleXMLWriter::Pointer writer = dynModuleXMLWriter::New( );
	writer->SetModule( m_Module );
	writer->SetFileName( filename );
	writer->Update( );

	// Parse XML file and check for errors
	std::ifstream iFile( filename.c_str( ) ,std::ios::in);
	if (!iFile.fail())
	{
		// Read XML file
		std::stringstream buffer;
		buffer << iFile.rdbuf();
		std::string xml(buffer.str());
		ModuleDescriptionParser parser;

		// Redirect standard error
		std::streambuf* origcerrrdbuf = std::cerr.rdbuf();
		std::ostringstream cerrstringstream;
		std::cerr.rdbuf( cerrstringstream.rdbuf() );
		ModuleDescription module;
		int res = parser.Parse(xml, module);
		std::cerr.rdbuf( origcerrrdbuf );
		
		if ( res == 1 )
		{
			std::list<std::string> lines;
			blTextUtils::ParseLine( cerrstringstream.str(), '\r', lines );
			std::string xmlError = *lines.begin( );
			throw Core::Exceptions::Exception( 
				"CustomApplicationWizard::UpdateData",
				xmlError.c_str( ) );
		}
	}
}

void Core::Widgets::CustomApplicationWizard::ReLoadModuleDescription( )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	// Get ModuleDescription from factory of CLP
	Runtime::PluginProviderManager::Pointer pluginProviderManager;
	pluginProviderManager = graphicalIface->GetPluginProviderManager();
	Core::Runtime::PluginProvider::Pointer provider;
	provider = GetPluginProvider( m_Module->GetTitle( ) );
	if ( provider.IsNull( ) ) 
	{
		// It could be possible that this module has not been loaded yet
		pluginProviderManager->ScanPlugins( );
		provider = GetPluginProvider( m_Module->GetTitle( ) );

		if ( provider.IsNull( ) )
		{
			throw Core::Exceptions::Exception( 
				"CustomApplicationWizard::UpdateOriginalModuleDescription",
				"Cannot find plugin provider" );
		}
	}

	// Remove loaded module
	RemoveLoadedModule( );

	// ReScan all plugins
	pluginProviderManager->UpdateProviders( provider->GetName( ), false );
}

void Core::Widgets::CustomApplicationWizard::RemoveLoadedModule( )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	// Get ModuleDescription from factory of CLP
	Core::Runtime::PluginProvider::Pointer provider;
	provider = GetPluginProvider( m_Module->GetTitle( ) );
	if ( provider.IsNull( ) ) 
	{
		// Module has not been loaded
		return;
	}

	// Remove loaded module
	std::string factoryName = graphicalIface->GetMainWindow()->FindFactoryNameByModule( m_Module );
	if ( !factoryName.empty() )
	{
		Core::BaseWindowFactory::Pointer factory;
		factory = graphicalIface->GetBaseWindowFactory( )->FindFactory( factoryName );
		factory->RemoveModule( provider->GetName() );

		// If it's empty, unregister it
		if ( factory->GetModules( ).empty( ) )
		{
			graphicalIface->GetBaseWindowFactory( )->UnRegisterFactory( factoryName );
		}

		// Update observers that it has been modified
		graphicalIface->GetBaseWindowFactory( )->GetFactoriesHolder()->NotifyObservers( );
	}
}

Core::Runtime::PluginProvider::Pointer Core::Widgets::CustomApplicationWizard::GetPluginProvider( 
	const std::string &moduleTitle )
{
	Core::Runtime::wxMitkGraphicalInterface::Pointer graphicalIface;
	graphicalIface = Core::Runtime::Kernel::GetGraphicalInterface();

	Runtime::PluginProviderManager::Pointer pluginProviderManager;
	pluginProviderManager = graphicalIface->GetPluginProviderManager();

	ModuleDescriptionSearch::Pointer search = ModuleDescriptionSearch::New( );
	std::string pluginProviderName = search->FindProvider( m_Module->GetTitle( ) );
	Core::Runtime::PluginProvider::Pointer provider;
	provider = pluginProviderManager->GetPluginProvider( pluginProviderName );
	return provider;
}

wxTreebook* Core::Widgets::CustomApplicationWizard::GetPreferencesWindow() const
{
	return m_ConfigurationTree;
}

void Core::Widgets::CustomApplicationWizard::UpdateCommandLineExample( )
{
	wxWindowUpdateLocker locker( m_txtCommandLine );

	if ( m_Module->GetType( ) == "Unknown" )
	{
		m_txtCommandLine->Clear( );
		return;
	}

	// Create a copy of the module description to avoid writing default values of I/O
	ModuleDescription module = *m_Module;
	Core::DynProcessor::Pointer processor = Core::DynProcessor::New( );
	processor->SetModule( &module );
	processor->GetModuleExecution( )->SetSaveScript( true );

	// Use simplified temporary and working directory
	processor->SetTemporaryDirectory( "" );

	Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
	processor->SetWorkingDirectory( settings->GetProjectHomePath() );
	
	// Set use case folder
	std::string dataFolder = "/data";
	int count = 0;
	while ( itksys::SystemTools::FileExists( dataFolder.c_str( ) ) )
	{
		std::stringstream sstream;
		sstream << "/data" << count++;
		dataFolder = sstream.str( );
	}
	processor->SetUseCase( dataFolder );

	// Update it
	processor->Update( );

	// Get command line string
	std::string scriptFilePath = processor->GetModuleExecution( )->GetUseCaseDirectory( ) + "/script.cmd";
	std::ifstream scripFile( scriptFilePath.c_str() );
	if ( scripFile.is_open( ) )
	{
		std::string commandlineExample;
		getline( scripFile, commandlineExample );
		m_txtCommandLine->SetValue( commandlineExample );
	}

	// Remove use case folder
	itksys::SystemTools::RemoveADirectory( 
		processor->GetModuleExecution( )->GetUseCaseDirectory( ).c_str() );
}


std::string Core::Widgets::CustomApplicationWizard::CheckRenameModule( )
{
	// Ask for the title to check the availability of the filename
	std::string currentName = m_Module->GetTitle( );
	wxString title = ::wxGetTextFromUser(
		"Introduce the application title", "Application title", currentName, this );
	if ( title.IsEmpty( ) || title == "Unknown" )
	{
		return "";
	}

	// If file exists, ask to overwrite
	Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
	std::string filename = m_Module->GetLocation( );
	
	// If it's not empy, use the same location
	if ( !filename.empty( ) )
	{
		return title.ToStdString();
	}
	else
	{
		// Create a new filename
		filename = std::string( "$(AppData)/Descriptions" ) + "/" + title + ".xml";
		settings->ReplaceGimiasPath( filename );

		if ( itksys::SystemTools::FileExists( filename.c_str( ) ) )
		{
			wxMessageDialog overwriteExistingFolderDialog(
				this, wxT("Overwrite existing file?"), wxT("Ovewrite"), wxYES_NO /*| wxCANCEL*/);
			if ( overwriteExistingFolderDialog.ShowModal() == wxID_NO )
			{
				return "";
			}
		}
	}

	return title.ToStdString();
}

std::string Core::Widgets::CustomApplicationWizard::GetModuleLocation( )
{
	Core::Runtime::Settings::Pointer settings = Core::Runtime::Kernel::GetApplicationSettings();
	std::string filename = m_Module->GetLocation( );
	if ( filename.empty( ) )
	{
		filename = std::string( "$(AppData)/Descriptions" ) + "/" + m_Module->GetTitle( ) + ".xml";
		settings->ReplaceGimiasPath( filename );
	}

	return filename;
}

