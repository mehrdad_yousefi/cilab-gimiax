/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreModuleConfigurationWidget_H
#define _coreModuleConfigurationWidget_H

#include "coreModuleConfigurationWidgetUI.h"
#include "coreCustomApplicationWizardPage.h"

namespace Core
{
namespace Widgets
{

class ModuleConfigurationWidget : 
	public coreModuleConfigurationWidgetUI,
	public CustomApplicationWizardPage
{
public:
	DECLARE_CLASS( ModuleConfigurationWidget )

	//!
    ModuleConfigurationWidget(ModuleDescription *module, wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

	//! Redefined
	virtual void UpdateData( );

	//! Redefined
	virtual void UpdateWidget( );

private:
	//!
	void OnBrowseExecutable(wxCommandEvent &event);

	//!
	void OnBrowseWorkingDirectory(wxCommandEvent &event);

protected:
	//!
	ModuleDescription *m_Module;
};

}
}

#endif // _coreModuleConfigurationWidget_H
