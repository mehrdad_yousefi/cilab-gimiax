/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef _coreModuleParamIOConfigurationWidget_H
#define _coreModuleParamIOConfigurationWidget_H

#include "coreModuleParamIOConfigurationWidgetUI.h"
#include "coreCustomApplicationWizardPage.h"

namespace Core
{
namespace Widgets
{

class ModuleParamIOConfigurationWidget : 
	public coreModuleParamIOConfigurationWidgetUI,
	public CustomApplicationWizardPage
{
public:
	DECLARE_CLASS( ModuleParamIOConfigurationWidget )
	//!
    ModuleParamIOConfigurationWidget(ModuleParameter *parameter, wxWindow* parent, int id, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=0);

	//! Redefined
	virtual void UpdateData( );

	//! Redefined
	virtual void UpdateWidget( );

	//!
	void UpdateRadioState( );

private:
    virtual void OnRadioButton(wxCommandEvent &event);

protected:
	//!
	ModuleParameter *m_Parameter;
};

}
}

#endif // _coreModuleParamIOConfigurationWidget_H
