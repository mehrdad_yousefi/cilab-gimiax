// -*- C++ -*- generated by wxGlade 0.6.3 on Tue Jan 24 14:27:38 2012

#include "coreModuleParamConfigurationWidgetUI.h"

// begin wxGlade: ::extracode

// end wxGlade


coreModuleParamConfigurationWidgetUI::coreModuleParamConfigurationWidgetUI(wxWindow* parent, int id, const wxPoint& pos, const wxSize& size, long style):
    wxPanel(parent, id, pos, size, wxTAB_TRAVERSAL)
{
    // begin wxGlade: coreModuleParamConfigurationWidgetUI::coreModuleParamConfigurationWidgetUI
    label_1 = new wxStaticText(this, wxID_ANY, wxT("Type*"));
    const wxString m_cmbType_choices[] = {
        wxT("double"),
        wxT("integer"),
        wxT("float"),
        wxT("boolean"),
        wxT("string"),
        wxT("file"),
        wxT("directory"),
        wxT("integer-vector"),
        wxT("float-vector"),
        wxT("double-vector"),
        wxT("string-vector"),
        wxT("integer-enumeration"),
        wxT("float-enumeration"),
        wxT("double-enumeration"),
        wxT("string-enumeration")
    };
    m_cmbType = new wxComboBox(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 15, m_cmbType_choices, wxCB_DROPDOWN|wxCB_READONLY);
    label_1_copy = new wxStaticText(this, wxID_ANY, wxT("Name*"));
    m_txtName = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
    label_1_copy_copy = new wxStaticText(this, wxID_ANY, wxT("Format*"));
    const wxString m_cmbFormat_choices[] = {
        wxT("--flag $(value)"),
        wxT("--flag=$(value)"),
        wxT("-flag $(value)"),
        wxT("-flag=$(value)")
    };
    m_cmbFormat = new wxComboBox(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 4, m_cmbFormat_choices, wxCB_DROPDOWN);
    label_1_copy_copy_copy = new wxStaticText(this, wxID_ANY, wxT("Label"));
    m_txtLabel = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
    label_1_copy_copy_copy_copy = new wxStaticText(this, wxID_ANY, wxT("Default"));
    m_txtDefault = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
    label_1_copy_copy_copy_copy_copy = new wxStaticText(this, wxID_ANY, wxT("Description"));
    m_txtDescription = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE);

    set_properties();
    do_layout();
    // end wxGlade
}


void coreModuleParamConfigurationWidgetUI::set_properties()
{
    // begin wxGlade: coreModuleParamConfigurationWidgetUI::set_properties
    label_1->SetToolTip(wxT("The type of the parameter"));
    m_cmbType->SetSelection(-1);
    label_1_copy->SetToolTip(wxT("The name of the argument"));
    label_1_copy_copy->SetToolTip(wxT("Format used to specify this parameter. You can use the variables $(value) or $(name)"));
    m_cmbFormat->SetSelection(-1);
    label_1_copy_copy_copy->SetToolTip(wxT("A short string that summarizes the parameter used for automatic GUI generation"));
    label_1_copy_copy_copy_copy->SetToolTip(wxT("A default value for the parameter. "));
    label_1_copy_copy_copy_copy_copy->SetToolTip(wxT("A short description of the parameter"));
    // end wxGlade
}


void coreModuleParamConfigurationWidgetUI::do_layout()
{
    // begin wxGlade: coreModuleParamConfigurationWidgetUI::do_layout
    wxBoxSizer* GlobalSizer = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* sizer_1_copy_copy_copy_copy_copy = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* sizer_1_copy_copy_copy_copy = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* sizer_1_copy_copy_copy = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* sizer_1_copy_copy = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* sizer_1_copy = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* sizer_1 = new wxBoxSizer(wxHORIZONTAL);
    sizer_1->Add(label_1, 1, wxALIGN_CENTER_VERTICAL, 0);
    sizer_1->Add(m_cmbType, 1, 0, 0);
    GlobalSizer->Add(sizer_1, 0, wxALL|wxEXPAND, 5);
    sizer_1_copy->Add(label_1_copy, 1, wxALIGN_CENTER_VERTICAL, 0);
    sizer_1_copy->Add(m_txtName, 1, wxEXPAND, 0);
    GlobalSizer->Add(sizer_1_copy, 0, wxALL|wxEXPAND, 5);
    sizer_1_copy_copy->Add(label_1_copy_copy, 1, wxALIGN_CENTER_VERTICAL, 0);
    sizer_1_copy_copy->Add(m_cmbFormat, 1, 0, 0);
    GlobalSizer->Add(sizer_1_copy_copy, 0, wxALL|wxEXPAND, 5);
    sizer_1_copy_copy_copy->Add(label_1_copy_copy_copy, 1, wxALIGN_CENTER_VERTICAL, 0);
    sizer_1_copy_copy_copy->Add(m_txtLabel, 1, wxEXPAND, 0);
    GlobalSizer->Add(sizer_1_copy_copy_copy, 0, wxALL|wxEXPAND, 5);
    sizer_1_copy_copy_copy_copy->Add(label_1_copy_copy_copy_copy, 1, wxALIGN_CENTER_VERTICAL, 0);
    sizer_1_copy_copy_copy_copy->Add(m_txtDefault, 1, wxEXPAND, 0);
    GlobalSizer->Add(sizer_1_copy_copy_copy_copy, 0, wxALL|wxEXPAND, 5);
    sizer_1_copy_copy_copy_copy_copy->Add(label_1_copy_copy_copy_copy_copy, 1, wxALIGN_CENTER_VERTICAL, 0);
    sizer_1_copy_copy_copy_copy_copy->Add(m_txtDescription, 1, wxEXPAND, 0);
    GlobalSizer->Add(sizer_1_copy_copy_copy_copy_copy, 1, wxALL|wxEXPAND, 5);
    GlobalSizer->Add(20, 20, 1, wxEXPAND, 0);
    SetSizer(GlobalSizer);
    GlobalSizer->Fit(this);
    // end wxGlade
}

