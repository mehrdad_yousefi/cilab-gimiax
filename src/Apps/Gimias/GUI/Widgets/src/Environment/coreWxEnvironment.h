/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#ifndef coreWxEnvironment_H
#define coreWxEnvironment_H

#include "gmWidgetsWin32Header.h"
#include "coreEnvironment.h"

namespace Core 
{
namespace Runtime 
{

/** 
Interface for event filter

\ingroup gmKernel
\author Xavi Planes
\date Oct 2011
*/
class wxBaseEventFilter
{
public:
	wxBaseEventFilter( )
	{

	}

	~wxBaseEventFilter( )
	{

	}

	virtual int eventFilter( wxEvent& event ) = 0;
};

/** 

\sa Core::Runtime::Kernel
\ingroup gmKernel
\author Xavi Planes
\date Apr 2011
*/
class GMWIDGETS_EXPORT WxEnvironment : public Core::Runtime::Environment
{
public:
	coreDeclareSmartPointerClassMacro(Core::Runtime::WxEnvironment, Core::Runtime::Environment);

	//!
	bool IsAppRunning(void) const;

	//! Filter an event calling all registered event filters
	int FilterEvent( wxEvent& event );

	//!
	void RegisterEventFilter( wxBaseEventFilter* filter );

	//!
	void UnRegisterEventFilter( wxBaseEventFilter* filter );

protected:
	//! Redefined
	void Exec(void);

	//! Redefined
	void ExitApplication(void);

	//!
	WxEnvironment(void);

	//!
	virtual ~WxEnvironment(void);

private:
	coreDeclareNoCopyConstructors(WxEnvironment);

	//!
	void RestartIfNeeded( );

protected:

	//!
	std::list<wxBaseEventFilter*> m_EventFilterList;

};

} // namespace Runtime
} // namespace Core

#endif
