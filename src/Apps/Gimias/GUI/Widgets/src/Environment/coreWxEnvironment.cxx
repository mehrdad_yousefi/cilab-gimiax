/*
* Copyright (c) 2014,
* Computational Image and Simulation Technologies in Biomedicine (CISTIB),
* The University Of Sheffield, South Yorkshire, UK. All rights reserved. 2012-2014
* Universitat Pompeu Fabra (UPF), Barcelona, Spain. All rights reserved. 2009-2012
* See license.txt file for details.
*/

#include "coreDirectory.h"
#include "coreException.h"
#include "coreReportExceptionMacros.h"
#include "coreWxEnvironment.h"
#include "coreAssert.h"
#include "coreLogger.h"
#include "coreKernel.h"
#include <itksys/Process.h>
#include "wx/filename.h"

using namespace Core::Runtime;

#if defined(WIN32) || defined(_WINDOWS_)
	#include <process.h>
#else
	#include <unistd.h>
#endif

//!
WxEnvironment::WxEnvironment()
{
}

//!
WxEnvironment::~WxEnvironment(void)
{
}


bool WxEnvironment::IsAppRunning(void) const
{
	return wxTheApp != NULL && wxTheApp->IsMainLoopRunning();
}

void WxEnvironment::ExitApplication(void)
{
	coreAssertMacro(wxTheApp != NULL);

	// If we're on exit, do not try exiting again
	if(this->OnAppExit())
		return;
	this->exit = true;

	// by default, the program will exit the
	// main loop (and so, usually, terminate) when the last top-level
	// program window is deleted
	wxTheApp->SetExitOnFrameDelete(true);

	// Destroy the main window (wxMitkCoreMainWindow) and the child windows
	// will be destroyed automatically
	wxWindow* mainWindow = wxTheApp->GetTopWindow();
	if(mainWindow != NULL)
	{
		mainWindow->Show( false );
		mainWindow->Destroy();
	}
}

void WxEnvironment::Exec(void)
{
	coreAssertMacro(wxTheApp != NULL);
	try
	{
		// Run the main event loop
		while(!this->OnAppExit() && !wxTheApp->IsMainLoopRunning())
		{
			try
			{
				wxTheApp->MainLoop();
			}
			coreCatchOnlyRuntimeExceptionsReportAndNoThrowMacro(WxEnvironment::Exec)
		}

		// Dispatch destroy event for main window when only the profile window is shown
		wxTheApp->ProcessIdle( );
	}
	coreCatchExceptionsAddTraceAndThrowMacro(WxEnvironment::Exec);
}

void WxEnvironment::RestartIfNeeded(void)
{
	if ( Core::Runtime::Environment::GetMustRestartOnExit( ) && 
		!Core::Runtime::Environment::GetArgv( ).empty( ) )
	{
		size_t argc = Core::Runtime::Environment::GetArgv( ).size();
		// Restart again GIMIAS
		if ( !Core::Runtime::Environment::GetArgv( )[ 0 ].empty( ) )
		{
			// Split argv in filename, directory, parameters
			std::string operation;
			std::string file;
			std::string directory;
			std::string parameters;

			// File and directory
			wxFileName filename( _U( Core::Runtime::Environment::GetArgv( )[ 0 ] ) );
			file = _U( filename.GetFullPath() );
			directory = _U( filename.GetPath() );

			// Parameters
			for ( size_t i = 1 ; i < argc ; i++ )
			{
				// Parameter cannot contain spaces!!! In Windows this will fail
				parameters.append( _U( Core::Runtime::Environment::GetArgv( )[ i ] ) );
				parameters.append( " " );
			}

			// Run as administrator in Windows
			if ( Core::Runtime::Environment::GetRestartWithAdministratorPrivileges() )
			{
				operation = "runas";
			}

#ifdef WIN32
			// Use ShellExecute Function to give administrator privileges when needed
			// This must be done for WebUpdate in order to install files in Program files folder
			SHELLEXECUTEINFO info = {0};
			info.cbSize=sizeof(SHELLEXECUTEINFO);
			info.hwnd = NULL;

			// The SEE_MASK_NOASYNC flag must be specified if the thread 
			// calling ShellExecuteEx does not have a message loop or if 
			// the thread or process will terminate soon after ShellExecuteEx returns
			info.fMask = SEE_MASK_NOASYNC;
			info.lpVerb = std::wstring(operation.begin(), operation.end()).c_str(); 
			info.lpFile = std::wstring(file.begin(), file.end()).c_str();  
			info.lpDirectory = std::wstring(directory.begin(), directory.end()).c_str(); 
			info.lpParameters = std::wstring(parameters.begin(), parameters.end()).c_str(); 
			info.nShow = SW_SHOWNORMAL;
			::ShellExecuteEx( &info );

#else
			//To avoid wxWidgets unicode vs non-unicode problems,:
			// chose to call wxExecute in the form wxExecute(wxString &); i.e. define argv as a wxstring containing
			//	commandline strings separated by " "
			wxString argv_str = _U( _U(filename.GetFullPath()) + " " + parameters );
			// Set current directory to the GIMIAS.exe folder
			wxSetWorkingDirectory( _U( directory ) );
			wxExecute( argv_str );
#endif
		}
	}
}

void Core::Runtime::WxEnvironment::RegisterEventFilter( wxBaseEventFilter* filter )
{
	m_EventFilterList.push_back( filter );
}

void Core::Runtime::WxEnvironment::UnRegisterEventFilter( wxBaseEventFilter* filter )
{
	m_EventFilterList.remove( filter );
}

int Core::Runtime::WxEnvironment::FilterEvent( wxEvent& event )
{
	int ret = -1;

	std::list<wxBaseEventFilter*>::iterator it;
	for ( it = m_EventFilterList.begin() ; it != m_EventFilterList.end() ; it++ )
	{
		int val = (*it)->eventFilter( event );
		ret = ( val == -1 ) ? ret : val;
	}

	return ret;
}
