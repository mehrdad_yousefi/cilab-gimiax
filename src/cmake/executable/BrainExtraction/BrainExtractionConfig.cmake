# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( BrainExtraction_FOUND TRUE )
SET( BrainExtraction_USE_FILE "${GIMIAS_BINARY_PATH}/executable/BrainExtraction/UseBrainExtraction.cmake" )
SET( BrainExtraction_INCLUDE_DIRS  )
SET( BrainExtraction_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
