# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( GrayscaleGrindPeakImageFilter_FOUND TRUE )
SET( GrayscaleGrindPeakImageFilter_USE_FILE "${GIMIAS_BINARY_PATH}/executable/GrayscaleGrindPeakImageFilter/UseGrayscaleGrindPeakImageFilter.cmake" )
SET( GrayscaleGrindPeakImageFilter_INCLUDE_DIRS  )
SET( GrayscaleGrindPeakImageFilter_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
