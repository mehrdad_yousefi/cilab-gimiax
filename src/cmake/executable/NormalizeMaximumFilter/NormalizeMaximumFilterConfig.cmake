# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( NormalizeMaximumFilter_FOUND TRUE )
SET( NormalizeMaximumFilter_USE_FILE "${GIMIAS_BINARY_PATH}/executable/NormalizeMaximumFilter/UseNormalizeMaximumFilter.cmake" )
SET( NormalizeMaximumFilter_INCLUDE_DIRS  )
SET( NormalizeMaximumFilter_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
