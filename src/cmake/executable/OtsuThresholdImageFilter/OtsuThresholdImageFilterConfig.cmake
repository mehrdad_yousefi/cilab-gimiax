# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( OtsuThresholdImageFilter_FOUND TRUE )
SET( OtsuThresholdImageFilter_USE_FILE "${GIMIAS_BINARY_PATH}/executable/OtsuThresholdImageFilter/UseOtsuThresholdImageFilter.cmake" )
SET( OtsuThresholdImageFilter_INCLUDE_DIRS  )
SET( OtsuThresholdImageFilter_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
