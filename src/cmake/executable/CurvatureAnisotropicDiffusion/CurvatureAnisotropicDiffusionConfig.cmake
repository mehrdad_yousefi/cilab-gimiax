# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( CurvatureAnisotropicDiffusion_FOUND TRUE )
SET( CurvatureAnisotropicDiffusion_USE_FILE "${GIMIAS_BINARY_PATH}/executable/CurvatureAnisotropicDiffusion/UseCurvatureAnisotropicDiffusion.cmake" )
SET( CurvatureAnisotropicDiffusion_INCLUDE_DIRS  )
SET( CurvatureAnisotropicDiffusion_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
