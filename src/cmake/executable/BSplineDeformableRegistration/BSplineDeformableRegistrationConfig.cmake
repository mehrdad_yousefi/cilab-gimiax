# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( BSplineDeformableRegistration_FOUND TRUE )
SET( BSplineDeformableRegistration_USE_FILE "${GIMIAS_BINARY_PATH}/executable/BSplineDeformableRegistration/UseBSplineDeformableRegistration.cmake" )
SET( BSplineDeformableRegistration_INCLUDE_DIRS  )
SET( BSplineDeformableRegistration_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
