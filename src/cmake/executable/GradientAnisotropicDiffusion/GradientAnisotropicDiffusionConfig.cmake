# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( GradientAnisotropicDiffusion_FOUND TRUE )
SET( GradientAnisotropicDiffusion_USE_FILE "${GIMIAS_BINARY_PATH}/executable/GradientAnisotropicDiffusion/UseGradientAnisotropicDiffusion.cmake" )
SET( GradientAnisotropicDiffusion_INCLUDE_DIRS  )
SET( GradientAnisotropicDiffusion_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
