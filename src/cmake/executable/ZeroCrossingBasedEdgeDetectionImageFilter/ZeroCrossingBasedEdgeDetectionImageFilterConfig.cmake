# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ZeroCrossingBasedEdgeDetectionImageFilter_FOUND TRUE )
SET( ZeroCrossingBasedEdgeDetectionImageFilter_USE_FILE "${GIMIAS_BINARY_PATH}/executable/ZeroCrossingBasedEdgeDetectionImageFilter/UseZeroCrossingBasedEdgeDetectionImageFilter.cmake" )
SET( ZeroCrossingBasedEdgeDetectionImageFilter_INCLUDE_DIRS  )
SET( ZeroCrossingBasedEdgeDetectionImageFilter_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
