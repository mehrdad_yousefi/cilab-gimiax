# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( GaussianBlurImageFilter_FOUND TRUE )
SET( GaussianBlurImageFilter_USE_FILE "${GIMIAS_BINARY_PATH}/executable/GaussianBlurImageFilter/UseGaussianBlurImageFilter.cmake" )
SET( GaussianBlurImageFilter_INCLUDE_DIRS  )
SET( GaussianBlurImageFilter_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
