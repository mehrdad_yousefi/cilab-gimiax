# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( AffineTransform_FOUND TRUE )
SET( AffineTransform_USE_FILE "${GIMIAS_BINARY_PATH}/executable/AffineTransform/UseAffineTransform.cmake" )
SET( AffineTransform_INCLUDE_DIRS  )
SET( AffineTransform_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
