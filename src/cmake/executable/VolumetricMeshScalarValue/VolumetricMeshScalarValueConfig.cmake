# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( VolumetricMeshScalarValue_FOUND TRUE )
SET( VolumetricMeshScalarValue_USE_FILE "${GIMIAS_BINARY_PATH}/executable/VolumetricMeshScalarValue/UseVolumetricMeshScalarValue.cmake" )
SET( VolumetricMeshScalarValue_INCLUDE_DIRS  )
SET( VolumetricMeshScalarValue_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
