# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( BinaryThresholdImageFilter_FOUND TRUE )
SET( BinaryThresholdImageFilter_USE_FILE "${GIMIAS_BINARY_PATH}/executable/BinaryThresholdImageFilter/UseBinaryThresholdImageFilter.cmake" )
SET( BinaryThresholdImageFilter_INCLUDE_DIRS  )
SET( BinaryThresholdImageFilter_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
