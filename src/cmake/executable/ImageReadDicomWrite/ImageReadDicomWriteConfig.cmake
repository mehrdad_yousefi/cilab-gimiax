# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ImageReadDicomWrite_FOUND TRUE )
SET( ImageReadDicomWrite_USE_FILE "${GIMIAS_BINARY_PATH}/executable/ImageReadDicomWrite/UseImageReadDicomWrite.cmake" )
SET( ImageReadDicomWrite_INCLUDE_DIRS  )
SET( ImageReadDicomWrite_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
