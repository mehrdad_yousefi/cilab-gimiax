# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( OtsuThresholdSegmentation_FOUND TRUE )
SET( OtsuThresholdSegmentation_USE_FILE "${GIMIAS_BINARY_PATH}/executable/OtsuThresholdSegmentation/UseOtsuThresholdSegmentation.cmake" )
SET( OtsuThresholdSegmentation_INCLUDE_DIRS  )
SET( OtsuThresholdSegmentation_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
