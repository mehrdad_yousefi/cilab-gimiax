# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( Gimias_FOUND TRUE )
SET( Gimias_USE_FILE "${GIMIAS_BINARY_PATH}/executable/Gimias/UseGimias.cmake" )
SET( Gimias_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/executable/Gimias"  )
SET( Gimias_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
