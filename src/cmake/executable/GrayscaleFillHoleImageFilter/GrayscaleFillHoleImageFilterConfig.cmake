# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( GrayscaleFillHoleImageFilter_FOUND TRUE )
SET( GrayscaleFillHoleImageFilter_USE_FILE "${GIMIAS_BINARY_PATH}/executable/GrayscaleFillHoleImageFilter/UseGrayscaleFillHoleImageFilter.cmake" )
SET( GrayscaleFillHoleImageFilter_INCLUDE_DIRS  )
SET( GrayscaleFillHoleImageFilter_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
