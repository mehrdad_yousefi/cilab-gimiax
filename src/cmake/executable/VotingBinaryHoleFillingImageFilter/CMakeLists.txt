# CMakeLists.txt generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

PROJECT(VotingBinaryHoleFillingImageFilter)
SET( GIMIAS_BINARY_PATH "GIMIAS/bin" CACHE PATH "Path to GIMIAS binaries folder")
SET( GIMIAS_SRC_PATH "GIMIAS/src" CACHE PATH "Path to GIMIAS source code folder")
SET( GIMIAS_THIRD_PARTY_SCR_PATH "GIMIAS/thirdParty" CACHE PATH "Path to GIMIAS thirdparty libraries source code")
SET( GIMIAS_THIRD_PARTY_BINARIES_PATH "GIMIAS/bin/thirdParty" CACHE PATH "Path to GIMIAS thirdparty binaries")
SET( GIMIAS_EXECUTABLE_PATH "GIMIAS/bin/bin" CACHE PATH "Path to GIMIAS executable folder")
MESSAGE( STATUS "Processing VotingBinaryHoleFillingImageFilter" )

# All binary outputs are written to the same folder.
SET( CMAKE_SUPPRESS_REGENERATION TRUE )
SET( EXECUTABLE_OUTPUT_PATH "${GIMIAS_EXECUTABLE_PATH}/${CMAKE_CFG_INTDIR}/commandLinePlugins")
SET( LIBRARY_OUTPUT_PATH "${GIMIAS_EXECUTABLE_PATH}/${CMAKE_CFG_INTDIR}/commandLinePlugins")
cmake_minimum_required(VERSION 2.4.6)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)



INCLUDE("${GIMIAS_SRC_PATH}/cmake/executable/VotingBinaryHoleFillingImageFilter/VotingBinaryHoleFillingImageFilterConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/executable/VotingBinaryHoleFillingImageFilter/UseVotingBinaryHoleFillingImageFilter.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/VotingBinaryHoleFillingImageFilterLib/VotingBinaryHoleFillingImageFilterLibConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/VotingBinaryHoleFillingImageFilterLib/UseVotingBinaryHoleFillingImageFilterLib.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/ITK-3.20Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/UseITK-3.20.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/VTK-5.10.1Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/UseVTK-5.10.1.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/HDF5Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/UseHDF5.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/ZLIBConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/UseZLIB.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/SLICERConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/UseSLICER.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/Slicer3/GenerateCLP/GenerateCLPConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/Slicer3/GenerateCLP/UseGenerateCLP.cmake")

# Add target
ADD_EXECUTABLE(VotingBinaryHoleFillingImageFilter    "${GIMIAS_THIRD_PARTY_SCR_PATH}/SLICER/Slicer3/Applications/CLI/Templates/CommandLineSharedLibraryWrapper.cxx" )
TARGET_LINK_LIBRARIES(VotingBinaryHoleFillingImageFilter  VotingBinaryHoleFillingImageFilterLib ${VotingBinaryHoleFillingImageFilter_LIBRARIES} )
ADD_DEFINITIONS( )

#Adding specific windows macros
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PlatformDependent.cmake" )
INCREASE_MSVC_HEAP_LIMIT( 1000 )
SUPPRESS_VC8_DEPRECATED_WARNINGS( )
SUPPRESS_LINKER_WARNING_4089( VotingBinaryHoleFillingImageFilter )
SUPPRESS_COMPILER_WARNING_DLL_EXPORT( VotingBinaryHoleFillingImageFilter )

#Adding properties

ADD_DEPENDENCIES(VotingBinaryHoleFillingImageFilter VotingBinaryHoleFillingImageFilterLib)
