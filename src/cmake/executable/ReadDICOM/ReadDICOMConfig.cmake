# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ReadDICOM_FOUND TRUE )
SET( ReadDICOM_USE_FILE "${GIMIAS_BINARY_PATH}/executable/ReadDICOM/UseReadDICOM.cmake" )
SET( ReadDICOM_INCLUDE_DIRS  )
SET( ReadDICOM_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
