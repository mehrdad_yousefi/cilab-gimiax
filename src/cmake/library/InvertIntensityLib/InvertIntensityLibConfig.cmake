# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( InvertIntensityLib_FOUND TRUE )
SET( InvertIntensityLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/InvertIntensityLib/UseInvertIntensityLib.cmake" )
SET( InvertIntensityLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/InvertIntensityLib"  )
SET( InvertIntensityLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( InvertIntensityLib_LIBRARIES ${InvertIntensityLib_LIBRARIES} "InvertIntensityLib"  )
