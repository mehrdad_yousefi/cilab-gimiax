# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( IsolatedConnectedLib_FOUND TRUE )
SET( IsolatedConnectedLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/IsolatedConnectedLib/UseIsolatedConnectedLib.cmake" )
SET( IsolatedConnectedLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/IsolatedConnectedLib"  )
SET( IsolatedConnectedLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( IsolatedConnectedLib_LIBRARIES ${IsolatedConnectedLib_LIBRARIES} "IsolatedConnectedLib"  )
