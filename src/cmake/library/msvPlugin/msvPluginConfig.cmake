# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( msvPlugin_FOUND TRUE )
SET( msvPlugin_USE_FILE "${GIMIAS_BINARY_PATH}/library/msvPlugin/UsemsvPlugin.cmake" )
SET( msvPlugin_INCLUDE_DIRS "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/MSVPlugin" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/MSVPlugin/widgets/msvVTKBtnPanelWidget" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/MSVPlugin/widgets/msvVTKBtnPanelWidget" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/MSVPlugin/widgets/msvRenderingTree" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/MSVPlugin/widgets/msvRenderingTree" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/MSVPlugin/processors" "${GIMIAS_BINARY_PATH}/library/msvPlugin"  )
SET( msvPlugin_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/plugins/msvPlugin/lib"  )
SET( msvPlugin_LIBRARIES ${msvPlugin_LIBRARIES} "msvPlugin"  )
