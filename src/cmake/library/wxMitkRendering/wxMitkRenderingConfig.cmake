# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( wxMitkRendering_FOUND TRUE )
SET( wxMitkRendering_USE_FILE "${GIMIAS_BINARY_PATH}/library/wxMitkRendering/UsewxMitkRendering.cmake" )
SET( wxMitkRendering_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include" "${GIMIAS_BINARY_PATH}/library/wxMitkRendering"  )
SET( wxMitkRendering_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( wxMitkRendering_LIBRARIES ${wxMitkRendering_LIBRARIES} "wxMitkRendering"  )
