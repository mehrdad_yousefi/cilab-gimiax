# CMakeLists.txt generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

PROJECT(wxMitkRendering)
SET( GIMIAS_BINARY_PATH "GIMIAS/bin" CACHE PATH "Path to GIMIAS binaries folder")
SET( GIMIAS_SRC_PATH "GIMIAS/src" CACHE PATH "Path to GIMIAS source code folder")
SET( GIMIAS_THIRD_PARTY_SCR_PATH "GIMIAS/thirdParty" CACHE PATH "Path to GIMIAS thirdparty libraries source code")
SET( GIMIAS_THIRD_PARTY_BINARIES_PATH "GIMIAS/bin/thirdParty" CACHE PATH "Path to GIMIAS thirdparty binaries")
SET( GIMIAS_EXECUTABLE_PATH "GIMIAS/bin/bin" CACHE PATH "Path to GIMIAS executable folder")
MESSAGE( STATUS "Processing wxMitkRendering" )

# All binary outputs are written to the same folder.
SET( CMAKE_SUPPRESS_REGENERATION TRUE )
SET( EXECUTABLE_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
SET( LIBRARY_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
cmake_minimum_required(VERSION 2.4.6)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)


ADD_SUBDIRECTORY("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibMITK" "${GIMIAS_BINARY_PATH}/library/TpExtLibMITK")
ADD_SUBDIRECTORY("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibITK" "${GIMIAS_BINARY_PATH}/library/TpExtLibITK")

INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/wxMitkRendering/wxMitkRenderingConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/wxMitkRendering/UsewxMitkRendering.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibWxWidgets/TpExtLibWxWidgetsConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibWxWidgets/UseTpExtLibWxWidgets.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxWebUpdate/tpExtLibWxWebUpdateConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxWebUpdate/UsetpExtLibWxWebUpdate.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxhttpEngine/tpExtLibWxhttpEngineConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxhttpEngine/UsetpExtLibWxhttpEngine.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/WXWIDGETS-3.0.2/WXWIDGETS-3.0.2Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/WXWIDGETS-3.0.2/UseWXWIDGETS-3.0.2.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibITK/TpExtLibITKConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibITK/UseTpExtLibITK.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibMITK/TpExtLibMITKConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibMITK/UseTpExtLibMITK.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibSignal/BaseLibSignalConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibSignal/UseBaseLibSignal.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibVTK/BaseLibVTKConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibVTK/UseBaseLibVTK.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CISTIBToolkit/CISTIBToolkitConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CISTIBToolkit/UseCISTIBToolkit.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/CGNS/CGNSConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/CGNS/UseCGNS.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibITK/BaseLibITKConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibITK/UseBaseLibITK.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/CLAPACK-3.2.1/CLAPACK-3.2.1Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/CLAPACK-3.2.1/UseCLAPACK-3.2.1.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/BaseLibConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/UseBaseLib.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/LOG4CPLUSConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/UseLOG4CPLUS.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/BOOST-1.45.0Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/UseBOOST-1.45.0.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/CILabMacrosConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/UseCILabMacros.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/MITK_SVN2/MITK_SVN2Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/MITK_SVN2/UseMITK_SVN2.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/ITK-3.20Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/UseITK-3.20.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ANN/ANNConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ANN/UseANN.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/VTK-5.10.1Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/UseVTK-5.10.1.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/HDF5Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/UseHDF5.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/ZLIBConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/UseZLIB.cmake")

if(UNIX)
    find_package(GTK2 2.10 REQUIRED gtk)
    INCLUDE_DIRECTORIES(${GTK2_INCLUDE_DIRS})
    LINK_LIBRARIES(${GTK2_LIBRARIES})
endif()

#Adding CMake PrecompiledHeader Pre
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PCHSupport_26.cmake" )
GET_NATIVE_PRECOMPILED_HEADER("wxMitkRendering" "${GIMIAS_SRC_PATH}/Modules/wxMitk/wxMitkPCH.h")

 # Create PCH Files (header) group 
IF (WIN32)
  SOURCE_GROUP("PCH Files (header)" FILES "${GIMIAS_SRC_PATH}/Modules/wxMitk/wxMitkPCH.h" )
ENDIF(WIN32)


 # Create PCH Files group 
IF (WIN32)
  SOURCE_GROUP("PCH Files" FILES "${GIMIAS_BINARY_PATH}/library/wxMitkRendering/wxMitkRendering_pch.cxx" )
ENDIF(WIN32)

#Configure header file and move it to binary folder
CONFIGURE_FILE(${GIMIAS_SRC_PATH}/cmake/library/wxMitkRendering/wxMitkRenderingWin32Header.h.in ${GIMIAS_BINARY_PATH}/library/wxMitkRendering/wxMitkRenderingWin32Header.h)

# Add target
ADD_LIBRARY(wxMitkRendering SHARED    "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src/wxMitkAbortEventFilter.cxx" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src/wxMitkApp.cxx" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src/wxMitkApplicationCursorImplementation.cxx" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src/wxMitkColorSpaceHelper.cxx" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src/wxMitkHistogramHelper.cxx" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src/wxMitkLevelWindowHelper.cxx" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src/wxMitkMouseOverHistogramEvent.cxx" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src/wxMitkRenderingManager.cxx" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src/wxMitkRenderingManagerFactory.cxx" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src/wxMitkRenderingRequestEvent.cxx" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src/wxMitkRenderWindow.cxx" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src/wxMitkVTKRenderWindowInteractor.cxx" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src/wxMitkVTKWindow.cxx" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src/wxVTKRenderWindowInteractor.cxx" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/src/wxMitkHistogramHelper.txx" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include/wxMitkAbortEventFilter.h" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include/wxMitkApp.h" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include/wxMitkApplicationCursorImplementation.h" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include/wxMitkColorSpaceHelper.h" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include/wxMitkHistogramHelper.h" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include/wxMitkLevelWindowHelper.h" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include/wxMitkMouseOverHistogramEvent.h" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include/wxMitkRenderingManager.h" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include/wxMitkRenderingManagerFactory.h" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include/wxMitkRenderingRequestEvent.h" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include/wxMitkRenderWindow.h" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include/wxMitkVTKRenderWindowInteractor.h" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include/wxMitkVTKWindow.h" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include/wxMitkWindowHandle.h" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkRendering/include/wxVTKRenderWindowInteractor.h" "${GIMIAS_SRC_PATH}/Modules/wxMitk/wxMitkPCH.h" "${GIMIAS_BINARY_PATH}/library/wxMitkRendering/wxMitkRendering_pch.cxx" )
TARGET_LINK_LIBRARIES(wxMitkRendering  TpExtLibWxWidgets  tpExtLibWxWebUpdate  tpExtLibWxhttpEngine  TpExtLibITK  TpExtLibMITK  BaseLibSignal  BaseLibVTK  CISTIBToolkit  BaseLibITK  BaseLib  CILabMacros ${wxMitkRendering_LIBRARIES} )

IF(WIN32)
 IF( MSVC) ## NOT SURE "UNICODE" IS NEEDED FOR OTHER THAN MSVC
  ADD_DEFINITIONS(
  /DUNICODE
  )
 ENDIF(MSVC)
ELSE(WIN32)
 ## NOT SURE "UNICODE" IS NEEDED FOR LINUX
ENDIF(WIN32)

#Adding specific windows macros
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PlatformDependent.cmake" )
INCREASE_MSVC_HEAP_LIMIT( 1000 )
SUPPRESS_VC8_DEPRECATED_WARNINGS( )
SUPPRESS_LINKER_WARNING_4089( wxMitkRendering )
SUPPRESS_COMPILER_WARNING_DLL_EXPORT( wxMitkRendering )

#Adding properties

#Adding CMake PrecompiledHeader Post
ADD_NATIVE_PRECOMPILED_HEADER("wxMitkRendering" "${GIMIAS_SRC_PATH}/Modules/wxMitk/wxMitkPCH.h")

ADD_DEPENDENCIES(wxMitkRendering CILabMacros)
ADD_DEPENDENCIES(wxMitkRendering BaseLib)
ADD_DEPENDENCIES(wxMitkRendering BaseLibITK)
ADD_DEPENDENCIES(wxMitkRendering CISTIBToolkit)
ADD_DEPENDENCIES(wxMitkRendering BaseLibVTK)
ADD_DEPENDENCIES(wxMitkRendering BaseLibSignal)
ADD_DEPENDENCIES(wxMitkRendering TpExtLibMITK)
ADD_DEPENDENCIES(wxMitkRendering TpExtLibITK)
ADD_DEPENDENCIES(wxMitkRendering tpExtLibWxhttpEngine)
ADD_DEPENDENCIES(wxMitkRendering tpExtLibWxWebUpdate)
ADD_DEPENDENCIES(wxMitkRendering TpExtLibWxWidgets)
