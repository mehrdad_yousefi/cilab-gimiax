# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( SshAPI_FOUND TRUE )
SET( SshAPI_USE_FILE "${GIMIAS_BINARY_PATH}/library/SshAPI/UseSshAPI.cmake" )
SET( SshAPI_INCLUDE_DIRS "${GIMIAS_EXTENSIONS_SRC_PATH}/modules/SshAPI/libmodules/sshAPI/src" "${GIMIAS_BINARY_PATH}/library/SshAPI"  )
SET( SshAPI_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( SshAPI_LIBRARIES ${SshAPI_LIBRARIES} "SshAPI"  )
