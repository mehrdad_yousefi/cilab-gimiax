# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( DilateLib_FOUND TRUE )
SET( DilateLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/DilateLib/UseDilateLib.cmake" )
SET( DilateLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/DilateLib"  )
SET( DilateLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( DilateLib_LIBRARIES ${DilateLib_LIBRARIES} "DilateLib"  )
