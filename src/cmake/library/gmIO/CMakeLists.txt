# CMakeLists.txt generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

PROJECT(gmIO)
SET( GIMIAS_BINARY_PATH "GIMIAS/bin" CACHE PATH "Path to GIMIAS binaries folder")
SET( GIMIAS_SRC_PATH "GIMIAS/src" CACHE PATH "Path to GIMIAS source code folder")
SET( GIMIAS_THIRD_PARTY_SCR_PATH "GIMIAS/thirdParty" CACHE PATH "Path to GIMIAS thirdparty libraries source code")
SET( GIMIAS_THIRD_PARTY_BINARIES_PATH "GIMIAS/bin/thirdParty" CACHE PATH "Path to GIMIAS thirdparty binaries")
SET( GIMIAS_EXECUTABLE_PATH "GIMIAS/bin/bin" CACHE PATH "Path to GIMIAS executable folder")
MESSAGE( STATUS "Processing gmIO" )

# All binary outputs are written to the same folder.
SET( CMAKE_SUPPRESS_REGENERATION TRUE )
SET( EXECUTABLE_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
SET( LIBRARY_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
cmake_minimum_required(VERSION 2.4.6)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)


ADD_SUBDIRECTORY("${GIMIAS_SRC_PATH}/cmake/library/gmFiltering" "${GIMIAS_BINARY_PATH}/library/gmFiltering")
ADD_SUBDIRECTORY("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibUtf8" "${GIMIAS_BINARY_PATH}/library/TpExtLibUtf8")

INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmIO/gmIOConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmIO/UsegmIO.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibUtf8/TpExtLibUtf8Config.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibUtf8/UseTpExtLibUtf8.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmFiltering/gmFilteringConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmFiltering/UsegmFiltering.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/SLICERConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/UseSLICER.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmDataHandling/gmDataHandlingConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmDataHandling/UsegmDataHandling.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmCommonObjects/gmCommonObjectsConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmCommonObjects/UsegmCommonObjects.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/ITK-3.20Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/UseITK-3.20.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/VTK-5.10.1Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/UseVTK-5.10.1.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/HDF5Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/UseHDF5.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/ZLIBConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/UseZLIB.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibNumericData/BaseLibNumericDataConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibNumericData/UseBaseLibNumericData.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TinyXml/TinyXmlConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TinyXml/UseTinyXml.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/BaseLibConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/UseBaseLib.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/LOG4CPLUSConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/UseLOG4CPLUS.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/BOOST-1.45.0Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/UseBOOST-1.45.0.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/CILabMacrosConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/UseCILabMacros.cmake")

#Adding CMake PrecompiledHeader Pre
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PCHSupport_26.cmake" )
GET_NATIVE_PRECOMPILED_HEADER("gmIO" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/gmIOPCH.h")

 # Create PCH Files (header) group 
IF (WIN32)
  SOURCE_GROUP("PCH Files (header)" FILES "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/gmIOPCH.h" )
ENDIF(WIN32)


 # Create PCH Files group 
IF (WIN32)
  SOURCE_GROUP("PCH Files" FILES "${GIMIAS_BINARY_PATH}/library/gmIO/gmIO_pch.cxx" )
ENDIF(WIN32)

#Configure header file and move it to binary folder
CONFIGURE_FILE(${GIMIAS_SRC_PATH}/cmake/library/gmIO/gmIOWin32Header.h.in ${GIMIAS_BINARY_PATH}/library/gmIO/gmIOWin32Header.h)

# Add target
ADD_LIBRARY(gmIO SHARED    "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/gmIOPCH.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreBaseDataEntityReader.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreBaseDataEntityWriter.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreBaseIO.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreConfigVars.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreDataEntityIORegistration.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreDataEntityReader.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreDataEntityWriter.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreDirectory.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreDirEntryFilter.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreFile.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreScanFolder.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreSettingsIO.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreBaseDataEntityReader.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreBaseDataEntityWriter.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreBaseIO.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreConfigVars.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreDataEntityIORegistration.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreDataEntityReader.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreDataEntityWriter.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreDirectory.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreDirEntryFilter.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreFile.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreScanFolder.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/coreSettingsIO.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/DataFormatObjects/NumericDataFile/coreNumericDataReader.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/DataFormatObjects/NumericDataFile/coreNumericDataWriter.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/DataFormatObjects/NumericDataFile/coreNumericDataReader.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/DataFormatObjects/NumericDataFile/coreNumericDataWriter.h" "${GIMIAS_BINARY_PATH}/library/gmIO/gmIO_pch.cxx" )
TARGET_LINK_LIBRARIES(gmIO  TpExtLibUtf8  gmFiltering  gmDataHandling  gmCommonObjects  BaseLibNumericData  TinyXml  BaseLib  CILabMacros ${gmIO_LIBRARIES} )

IF(WIN32)
    ADD_DEFINITIONS( -D_CRT_SECURE_NO_DEPRECATE -D_SCL_SECURE_NO_WARNINGS  )
ELSE(WIN32)
    ADD_DEFINITIONS( -D_CRT_SECURE_NO_DEPRECATE -D_SCL_SECURE_NO_WARNINGS )
ENDIF()


#Adding specific windows macros
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PlatformDependent.cmake" )
INCREASE_MSVC_HEAP_LIMIT( 1000 )
SUPPRESS_VC8_DEPRECATED_WARNINGS( )
SUPPRESS_LINKER_WARNING_4089( gmIO )
SUPPRESS_COMPILER_WARNING_DLL_EXPORT( gmIO )

#Adding properties

#Adding CMake PrecompiledHeader Post
ADD_NATIVE_PRECOMPILED_HEADER("gmIO" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/gmIOPCH.h")

ADD_DEPENDENCIES(gmIO CILabMacros)
ADD_DEPENDENCIES(gmIO BaseLib)
ADD_DEPENDENCIES(gmIO TinyXml)
ADD_DEPENDENCIES(gmIO BaseLibNumericData)
ADD_DEPENDENCIES(gmIO gmCommonObjects)
ADD_DEPENDENCIES(gmIO gmDataHandling)
ADD_DEPENDENCIES(gmIO gmFiltering)
ADD_DEPENDENCIES(gmIO TpExtLibUtf8)
