# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( gmIO_FOUND TRUE )
SET( gmIO_USE_FILE "${GIMIAS_BINARY_PATH}/library/gmIO/UsegmIO.cmake" )
SET( gmIO_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/IO/src/DataFormatObjects/NumericDataFile" "${GIMIAS_BINARY_PATH}/library/gmIO"  )
SET( gmIO_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( gmIO_LIBRARIES ${gmIO_LIBRARIES} "gmIO"  )
