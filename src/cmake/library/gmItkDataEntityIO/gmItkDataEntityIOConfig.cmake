# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( gmItkDataEntityIO_FOUND TRUE )
SET( gmItkDataEntityIO_USE_FILE "${GIMIAS_BINARY_PATH}/library/gmItkDataEntityIO/UsegmItkDataEntityIO.cmake" )
SET( gmItkDataEntityIO_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Apps/Plugins/MITKPlugin/itkDataEntityIO" "${GIMIAS_BINARY_PATH}/library/gmItkDataEntityIO"  )
SET( gmItkDataEntityIO_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( gmItkDataEntityIO_LIBRARIES ${gmItkDataEntityIO_LIBRARIES} "gmItkDataEntityIO"  )
