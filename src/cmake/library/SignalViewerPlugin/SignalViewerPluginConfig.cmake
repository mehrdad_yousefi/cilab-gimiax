# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( SignalViewerPlugin_FOUND TRUE )
SET( SignalViewerPlugin_USE_FILE "${GIMIAS_BINARY_PATH}/library/SignalViewerPlugin/UseSignalViewerPlugin.cmake" )
SET( SignalViewerPlugin_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Apps/Plugins/SignalViewerPlugin" "${GIMIAS_SRC_PATH}/Apps/Plugins/SignalViewerPlugin/processors" "${GIMIAS_SRC_PATH}/Apps/Plugins/SignalViewerPlugin/widgets/PlotWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/SignalViewerPlugin/widgets/PlotWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/SignalViewerPlugin/widgets/SignalTimePropagationPanelWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/SignalViewerPlugin/widgets/SignalTimePropagationPanelWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/SignalViewerPlugin/widgets/SignalReaderWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/SignalViewerPlugin/widgets/SignalReaderWidget" "${GIMIAS_BINARY_PATH}/library/SignalViewerPlugin"  )
SET( SignalViewerPlugin_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/plugins/SignalViewerPlugin/lib"  )
SET( SignalViewerPlugin_LIBRARIES ${SignalViewerPlugin_LIBRARIES} "SignalViewerPlugin"  )
