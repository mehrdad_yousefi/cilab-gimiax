# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( AppendBodyParts3DLib_FOUND TRUE )
SET( AppendBodyParts3DLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/AppendBodyParts3DLib/UseAppendBodyParts3DLib.cmake" )
SET( AppendBodyParts3DLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/AppendBodyParts3DLib"  )
SET( AppendBodyParts3DLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( AppendBodyParts3DLib_LIBRARIES ${AppendBodyParts3DLib_LIBRARIES} "AppendBodyParts3DLib"  )
