# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ImageLabelCombineLib_FOUND TRUE )
SET( ImageLabelCombineLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/ImageLabelCombineLib/UseImageLabelCombineLib.cmake" )
SET( ImageLabelCombineLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/ImageLabelCombineLib"  )
SET( ImageLabelCombineLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( ImageLabelCombineLib_LIBRARIES ${ImageLabelCombineLib_LIBRARIES} "ImageLabelCombineLib"  )
