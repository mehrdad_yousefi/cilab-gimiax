# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( dcmAPIData_FOUND TRUE )
SET( dcmAPIData_USE_FILE "${GIMIAS_BINARY_PATH}/library/dcmAPIData/UsedcmAPIData.cmake" )
SET( dcmAPIData_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/DcmAPI/libmodules/dcmData/src" "${GIMIAS_SRC_PATH}/Modules/DcmAPI/libmodules/dcmData/include" "${GIMIAS_BINARY_PATH}/library/dcmAPIData"  )
SET( dcmAPIData_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( dcmAPIData_LIBRARIES ${dcmAPIData_LIBRARIES} "dcmAPIData"  )
