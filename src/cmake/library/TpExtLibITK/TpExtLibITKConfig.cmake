# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( TpExtLibITK_FOUND TRUE )
SET( TpExtLibITK_USE_FILE "${GIMIAS_BINARY_PATH}/library/TpExtLibITK/UseTpExtLibITK.cmake" )
SET( TpExtLibITK_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtITK/src" "${GIMIAS_BINARY_PATH}/library/TpExtLibITK"  )
SET( TpExtLibITK_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( TpExtLibITK_LIBRARIES ${TpExtLibITK_LIBRARIES} "TpExtLibITK"  )
