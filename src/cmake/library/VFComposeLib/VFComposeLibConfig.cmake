# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( VFComposeLib_FOUND TRUE )
SET( VFComposeLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/VFComposeLib/UseVFComposeLib.cmake" )
SET( VFComposeLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/VFComposeLib"  )
SET( VFComposeLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( VFComposeLib_LIBRARIES ${VFComposeLib_LIBRARIES} "VFComposeLib"  )
