# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( OtsuThresholdImageFilterLib_FOUND TRUE )
SET( OtsuThresholdImageFilterLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/OtsuThresholdImageFilterLib/UseOtsuThresholdImageFilterLib.cmake" )
SET( OtsuThresholdImageFilterLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/OtsuThresholdImageFilterLib"  )
SET( OtsuThresholdImageFilterLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( OtsuThresholdImageFilterLib_LIBRARIES ${OtsuThresholdImageFilterLib_LIBRARIES} "OtsuThresholdImageFilterLib"  )
