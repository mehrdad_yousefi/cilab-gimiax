# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( BaseLibITK_FOUND TRUE )
SET( BaseLibITK_USE_FILE "${GIMIAS_BINARY_PATH}/library/BaseLibITK/UseBaseLibITK.cmake" )
SET( BaseLibITK_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blPDShape/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blImage/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blImageProperties/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blImageUtilities/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blMath/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blPDAlignment/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blUtilitiesITK/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blPDShape/include" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blImage/include" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blImageProperties/include" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blImageUtilities/include" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blMath/include" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blPDAlignment/include" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blUtilitiesITK/include" "${GIMIAS_BINARY_PATH}/library/BaseLibITK"  )
SET( BaseLibITK_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( BaseLibITK_LIBRARIES ${BaseLibITK_LIBRARIES} "BaseLibITK"  )
