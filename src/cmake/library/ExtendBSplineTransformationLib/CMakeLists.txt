# CMakeLists.txt generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

PROJECT(ExtendBSplineTransformationLib)
SET( GIMIAS_BINARY_PATH "GIMIAS/bin" CACHE PATH "Path to GIMIAS binaries folder")
SET( GIMIAS_SRC_PATH "GIMIAS/src" CACHE PATH "Path to GIMIAS source code folder")
SET( GIMIAS_THIRD_PARTY_SCR_PATH "GIMIAS/thirdParty" CACHE PATH "Path to GIMIAS thirdparty libraries source code")
SET( GIMIAS_THIRD_PARTY_BINARIES_PATH "GIMIAS/bin/thirdParty" CACHE PATH "Path to GIMIAS thirdparty binaries")
SET( GIMIAS_EXECUTABLE_PATH "GIMIAS/bin/bin" CACHE PATH "Path to GIMIAS executable folder")
MESSAGE( STATUS "Processing ExtendBSplineTransformationLib" )

# All binary outputs are written to the same folder.
SET( CMAKE_SUPPRESS_REGENERATION TRUE )
SET( EXECUTABLE_OUTPUT_PATH "${GIMIAS_EXECUTABLE_PATH}/${CMAKE_CFG_INTDIR}/commandLinePlugins")
SET( LIBRARY_OUTPUT_PATH "${GIMIAS_EXECUTABLE_PATH}/${CMAKE_CFG_INTDIR}/commandLinePlugins")
cmake_minimum_required(VERSION 2.4.6)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)


ADD_SUBDIRECTORY("${GIMIAS_SRC_PATH}/cmake/executable/ExtendBSplineTransformation" "${GIMIAS_BINARY_PATH}/executable/ExtendBSplineTransformation")

INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/ExtendBSplineTransformationLib/ExtendBSplineTransformationLibConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/ExtendBSplineTransformationLib/UseExtendBSplineTransformationLib.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibITK/TpExtLibITKConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibITK/UseTpExtLibITK.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/ITK-3.20Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/UseITK-3.20.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/VTK-5.10.1Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/UseVTK-5.10.1.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/HDF5Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/UseHDF5.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/ZLIBConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/UseZLIB.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/SLICERConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/UseSLICER.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/Slicer3/GenerateCLP/GenerateCLPConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/Slicer3/GenerateCLP/UseGenerateCLP.cmake")

# Start code from callback function 'CreateCMakeCLPPre'
SET( CLP ${PROJECT_NAME}CLP )
SET( ${CLP}_SOURCE "${GIMIAS_SRC_PATH}/Modules/TpExtLib/Applications/ExtendBSplineTransformation/ExtendBSplineTransformation.cxx" )
GET_FILENAME_COMPONENT( TMP_FILENAME ${${CLP}_SOURCE} NAME_WE )
SET( ${CLP}_INCLUDE_FILE ${CMAKE_CURRENT_BINARY_DIR}/${TMP_FILENAME}CLP.h )

# End code from callback function 'CreateCMakeCLPPre'

#Configure header file and move it to binary folder
CONFIGURE_FILE(${GIMIAS_SRC_PATH}/cmake/library/ExtendBSplineTransformationLib/ExtendBSplineTransformationLibWin32Header.h.in ${GIMIAS_BINARY_PATH}/library/ExtendBSplineTransformationLib/ExtendBSplineTransformationLibWin32Header.h)

# Add target
ADD_LIBRARY(ExtendBSplineTransformationLib SHARED    "${GIMIAS_SRC_PATH}/Modules/TpExtLib/Applications/ExtendBSplineTransformation/ExtendBSplineTransformation.cxx" "${${CLP}_INCLUDE_FILE}" )
TARGET_LINK_LIBRARIES(ExtendBSplineTransformationLib  TpExtLibITK ${ExtendBSplineTransformationLib_LIBRARIES} )
ADD_DEFINITIONS( -Dmain=ModuleEntryPoint  /D_SCL_SECURE_NO_WARNINGS )

#Adding specific windows macros
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PlatformDependent.cmake" )
INCREASE_MSVC_HEAP_LIMIT( 1000 )
SUPPRESS_VC8_DEPRECATED_WARNINGS( )
SUPPRESS_LINKER_WARNING_4089( ExtendBSplineTransformationLib )
SUPPRESS_COMPILER_WARNING_DLL_EXPORT( ExtendBSplineTransformationLib )

#Adding properties

# Start code from callback function 'CreateCMakeCLPPost'
GENERATECLP( ${CLP}_SOURCE "${GIMIAS_SRC_PATH}/Modules/TpExtLib/Applications/ExtendBSplineTransformation/ExtendBSplineTransformation.xml" )

# End code from callback function 'CreateCMakeCLPPost'


ADD_DEPENDENCIES(ExtendBSplineTransformationLib TpExtLibITK)
