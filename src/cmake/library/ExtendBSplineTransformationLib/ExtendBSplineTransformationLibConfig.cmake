# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ExtendBSplineTransformationLib_FOUND TRUE )
SET( ExtendBSplineTransformationLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/ExtendBSplineTransformationLib/UseExtendBSplineTransformationLib.cmake" )
SET( ExtendBSplineTransformationLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/ExtendBSplineTransformationLib"  )
SET( ExtendBSplineTransformationLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( ExtendBSplineTransformationLib_LIBRARIES ${ExtendBSplineTransformationLib_LIBRARIES} "ExtendBSplineTransformationLib"  )
