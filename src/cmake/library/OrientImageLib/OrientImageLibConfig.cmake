# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( OrientImageLib_FOUND TRUE )
SET( OrientImageLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/OrientImageLib/UseOrientImageLib.cmake" )
SET( OrientImageLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/OrientImageLib"  )
SET( OrientImageLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( OrientImageLib_LIBRARIES ${OrientImageLib_LIBRARIES} "OrientImageLib"  )
