# CMakeLists.txt generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

PROJECT(DcmAPI)
SET( GIMIAS_BINARY_PATH "GIMIAS/bin" CACHE PATH "Path to GIMIAS binaries folder")
SET( GIMIAS_SRC_PATH "GIMIAS/src" CACHE PATH "Path to GIMIAS source code folder")
SET( GIMIAS_THIRD_PARTY_SCR_PATH "GIMIAS/thirdParty" CACHE PATH "Path to GIMIAS thirdparty libraries source code")
SET( GIMIAS_THIRD_PARTY_BINARIES_PATH "GIMIAS/bin/thirdParty" CACHE PATH "Path to GIMIAS thirdparty binaries")
SET( GIMIAS_EXECUTABLE_PATH "GIMIAS/bin/bin" CACHE PATH "Path to GIMIAS executable folder")
MESSAGE( STATUS "Processing DcmAPI" )

# All binary outputs are written to the same folder.
SET( CMAKE_SUPPRESS_REGENERATION TRUE )
SET( EXECUTABLE_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
SET( LIBRARY_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
cmake_minimum_required(VERSION 2.4.6)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)

ADD_SUBDIRECTORY("${GIMIAS_SRC_PATH}/cmake/library/dcmAPIIO" "${GIMIAS_BINARY_PATH}/library/dcmAPIIO")

IF(BUILD_TESTING)
  # ... CMake code to create tests ...
  # ADD_SUBDIRECTORY("${GIMIAS_SRC_PATH}/cmake/executable/DcmAPITests" "${GIMIAS_BINARY_PATH}/executable/DcmAPITests") # Test hangs. Disabled until fixed. 
ENDIF(BUILD_TESTING)

INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DcmAPI/DcmAPIConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DcmAPI/UseDcmAPI.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/dcmAPIIO/dcmAPIIOConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/dcmAPIIO/UsedcmAPIIO.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibNumericData/BaseLibNumericDataConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibNumericData/UseBaseLibNumericData.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TinyXml/TinyXmlConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TinyXml/UseTinyXml.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibSignal/BaseLibSignalConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibSignal/UseBaseLibSignal.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibVTK/BaseLibVTKConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibVTK/UseBaseLibVTK.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CISTIBToolkit/CISTIBToolkitConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CISTIBToolkit/UseCISTIBToolkit.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/CGNS/CGNSConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/CGNS/UseCGNS.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibITK/BaseLibITKConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibITK/UseBaseLibITK.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/CLAPACK-3.2.1/CLAPACK-3.2.1Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/CLAPACK-3.2.1/UseCLAPACK-3.2.1.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/ITK-3.20Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/UseITK-3.20.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/VTK-5.10.1Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/UseVTK-5.10.1.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/HDF5Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/UseHDF5.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/ZLIBConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/UseZLIB.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/dcmAPIData/dcmAPIDataConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/dcmAPIData/UsedcmAPIData.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/BaseLibConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/UseBaseLib.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/LOG4CPLUSConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/UseLOG4CPLUS.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/CILabMacrosConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/UseCILabMacros.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/BOOST-1.45.0Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/UseBOOST-1.45.0.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/DCMTK-3.6.0/DCMTK-3.6.0Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/DCMTK-3.6.0/UseDCMTK-3.6.0.cmake")

#Configure header file and move it to binary folder
CONFIGURE_FILE(${GIMIAS_SRC_PATH}/cmake/library/DcmAPI/DcmAPIWin32Header.h.in ${GIMIAS_BINARY_PATH}/library/DcmAPI/DcmAPIWin32Header.h)

# Add target

set(build_type_library SHARED)
if(WIN32)
  set(build_type_library STATIC)
endif(WIN32)

ADD_LIBRARY(DcmAPI ${build_type_library} "${GIMIAS_SRC_PATH}/csnake_dummy.cpp" )
ADD_DEFINITIONS( )

#Adding specific windows macros
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PlatformDependent.cmake" )
INCREASE_MSVC_HEAP_LIMIT( 1000 )
SUPPRESS_VC8_DEPRECATED_WARNINGS( )
SUPPRESS_LINKER_WARNING_4089( DcmAPI )
SUPPRESS_COMPILER_WARNING_DLL_EXPORT( DcmAPI )

#Adding properties

