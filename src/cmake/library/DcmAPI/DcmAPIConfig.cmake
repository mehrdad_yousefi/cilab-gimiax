# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( DcmAPI_FOUND TRUE )
SET( DcmAPI_USE_FILE "${GIMIAS_BINARY_PATH}/library/DcmAPI/UseDcmAPI.cmake" )
SET( DcmAPI_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/DcmAPI"  )
SET( DcmAPI_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
