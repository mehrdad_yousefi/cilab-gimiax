# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( TavernaPlugin_FOUND TRUE )
SET( TavernaPlugin_USE_FILE "${GIMIAS_BINARY_PATH}/library/TavernaPlugin/UseTavernaPlugin.cmake" )
SET( TavernaPlugin_INCLUDE_DIRS "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/TavernaPlugin" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/TavernaPlugin/processors" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/TavernaPlugin/widgets/TavernaPluginWorkflowPanelWidget" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/TavernaPlugin/widgets/TavernaPluginWorkflowPanelWidget" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/TavernaPlugin/widgets/TavernaPluginWorkingArea" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/TavernaPlugin/widgets/TavernaPluginWorkingArea" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/TavernaPlugin/widgets/TavernaPluginConfigurationPanelWidget" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/TavernaPlugin/widgets/TavernaPluginConfigurationPanelWidget" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/TavernaPlugin/widgets/TavernaPluginToolbar" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/TavernaPlugin/widgets/TavernaPluginToolbar" "${GIMIAS_BINARY_PATH}/library/TavernaPlugin"  )
SET( TavernaPlugin_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/plugins/TavernaPlugin/lib"  )
SET( TavernaPlugin_LIBRARIES ${TavernaPlugin_LIBRARIES} "TavernaPlugin"  )
