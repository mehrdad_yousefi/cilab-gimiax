# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( MaskLib_FOUND TRUE )
SET( MaskLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/MaskLib/UseMaskLib.cmake" )
SET( MaskLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/MaskLib"  )
SET( MaskLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( MaskLib_LIBRARIES ${MaskLib_LIBRARIES} "MaskLib"  )
