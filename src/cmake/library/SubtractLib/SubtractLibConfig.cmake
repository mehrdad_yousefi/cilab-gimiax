# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( SubtractLib_FOUND TRUE )
SET( SubtractLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/SubtractLib/UseSubtractLib.cmake" )
SET( SubtractLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/SubtractLib"  )
SET( SubtractLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( SubtractLib_LIBRARIES ${SubtractLib_LIBRARIES} "SubtractLib"  )
