# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( XNAT_FOUND TRUE )
SET( XNAT_USE_FILE "${GIMIAS_BINARY_PATH}/library/XNAT/UseXNAT.cmake" )
SET( XNAT_INCLUDE_DIRS "${GIMIAS_EXTENSIONS_SRC_PATH}/modules/XNAT/libmodules/xnat/src" "${GIMIAS_BINARY_PATH}/library/XNAT"  )
SET( XNAT_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( XNAT_LIBRARIES ${XNAT_LIBRARIES} "XNAT"  )
