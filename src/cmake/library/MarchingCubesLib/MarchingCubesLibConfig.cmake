# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( MarchingCubesLib_FOUND TRUE )
SET( MarchingCubesLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/MarchingCubesLib/UseMarchingCubesLib.cmake" )
SET( MarchingCubesLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/MarchingCubesLib"  )
SET( MarchingCubesLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( MarchingCubesLib_LIBRARIES ${MarchingCubesLib_LIBRARIES} "MarchingCubesLib"  )
