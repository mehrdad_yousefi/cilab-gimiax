# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( SecurityLib_FOUND TRUE )
SET( SecurityLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/SecurityLib/UseSecurityLib.cmake" )
SET( SecurityLib_INCLUDE_DIRS "${GIMIAS_EXTENSIONS_SRC_PATH}/modules/SecurityLib/libmodules/SecurityLib/src" "${GIMIAS_BINARY_PATH}/library/SecurityLib"  )
SET( SecurityLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( SecurityLib_LIBRARIES ${SecurityLib_LIBRARIES} "SecurityLib"  )
