# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( BaseLib_FOUND TRUE )
SET( BaseLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/BaseLib/UseBaseLib.cmake" )
SET( BaseLib_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blCommon/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blUtilities/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blCommon/include" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blUtilities/include" "${GIMIAS_BINARY_PATH}/library/BaseLib"  )
SET( BaseLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )

if(WIN32)
  SET( BaseLib_LIBRARIES ${BaseLib_LIBRARIES} "psapi" )
endif(WIN32)

SET( BaseLib_LIBRARIES ${BaseLib_LIBRARIES} "BaseLib"  )
