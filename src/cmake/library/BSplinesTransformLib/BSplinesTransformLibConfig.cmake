# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( BSplinesTransformLib_FOUND TRUE )
SET( BSplinesTransformLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/BSplinesTransformLib/UseBSplinesTransformLib.cmake" )
SET( BSplinesTransformLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/BSplinesTransformLib"  )
SET( BSplinesTransformLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( BSplinesTransformLib_LIBRARIES ${BSplinesTransformLib_LIBRARIES} "BSplinesTransformLib"  )
