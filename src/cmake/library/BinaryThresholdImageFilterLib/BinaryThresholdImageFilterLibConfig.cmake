# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( BinaryThresholdImageFilterLib_FOUND TRUE )
SET( BinaryThresholdImageFilterLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/BinaryThresholdImageFilterLib/UseBinaryThresholdImageFilterLib.cmake" )
SET( BinaryThresholdImageFilterLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/BinaryThresholdImageFilterLib"  )
SET( BinaryThresholdImageFilterLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( BinaryThresholdImageFilterLib_LIBRARIES ${BinaryThresholdImageFilterLib_LIBRARIES} "BinaryThresholdImageFilterLib"  )
