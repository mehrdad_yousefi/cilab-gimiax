# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( AffineTransformLib_FOUND TRUE )
SET( AffineTransformLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/AffineTransformLib/UseAffineTransformLib.cmake" )
SET( AffineTransformLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/AffineTransformLib"  )
SET( AffineTransformLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( AffineTransformLib_LIBRARIES ${AffineTransformLib_LIBRARIES} "AffineTransformLib"  )
