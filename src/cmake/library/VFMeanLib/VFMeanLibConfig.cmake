# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( VFMeanLib_FOUND TRUE )
SET( VFMeanLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/VFMeanLib/UseVFMeanLib.cmake" )
SET( VFMeanLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/VFMeanLib"  )
SET( VFMeanLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( VFMeanLib_LIBRARIES ${VFMeanLib_LIBRARIES} "VFMeanLib"  )
