# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( CxxTest_FOUND TRUE )
SET( CxxTest_USE_FILE "${GIMIAS_BINARY_PATH}/library/CxxTest/UseCxxTest.cmake" )
SET( CxxTest_INCLUDE_DIRS "${GIMIAS_THIRD_PARTY_SCR_PATH}/CXXTEST/CxxTest" "${GIMIAS_BINARY_PATH}/library/CxxTest"  )
SET( CxxTest_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( CxxTest_LIBRARIES ${CxxTest_LIBRARIES} "CxxTest"  )
