# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( TileImagesLib_FOUND TRUE )
SET( TileImagesLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/TileImagesLib/UseTileImagesLib.cmake" )
SET( TileImagesLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/TileImagesLib"  )
SET( TileImagesLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( TileImagesLib_LIBRARIES ${TileImagesLib_LIBRARIES} "TileImagesLib"  )
