# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ManualSegmentationPlugin_FOUND TRUE )
SET( ManualSegmentationPlugin_USE_FILE "${GIMIAS_BINARY_PATH}/library/ManualSegmentationPlugin/UseManualSegmentationPlugin.cmake" )
SET( ManualSegmentationPlugin_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Apps/Plugins/ManualSegmentationPlugin" "${GIMIAS_SRC_PATH}/Apps/Plugins/ManualSegmentationPlugin/processors" "${GIMIAS_SRC_PATH}/Apps/Plugins/ManualSegmentationPlugin/widgets/ManualSegmentationWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/ManualSegmentationPlugin/widgets/ManualSegmentationWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/ManualSegmentationPlugin/widgets/MultiLevelROIPanelWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/ManualSegmentationPlugin/widgets/MultiLevelROIPanelWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/ManualSegmentationPlugin/widgets/InteractiveSegmPanelWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/ManualSegmentationPlugin/widgets/InteractiveSegmPanelWidget" "${GIMIAS_BINARY_PATH}/library/ManualSegmentationPlugin"  )
SET( ManualSegmentationPlugin_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/plugins/ManualSegmentationPlugin/lib"  )
SET( ManualSegmentationPlugin_LIBRARIES ${ManualSegmentationPlugin_LIBRARIES} "ManualSegmentationPlugin"  )
