# CMakeLists.txt generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

PROJECT(gmWidgets)
SET( GIMIAS_BINARY_PATH "GIMIAS/bin" CACHE PATH "Path to GIMIAS binaries folder")
SET( GIMIAS_SRC_PATH "GIMIAS/src" CACHE PATH "Path to GIMIAS source code folder")
SET( GIMIAS_THIRD_PARTY_SCR_PATH "GIMIAS/thirdParty" CACHE PATH "Path to GIMIAS thirdparty libraries source code")
SET( GIMIAS_THIRD_PARTY_BINARIES_PATH "GIMIAS/bin/thirdParty" CACHE PATH "Path to GIMIAS thirdparty binaries")
SET( GIMIAS_EXECUTABLE_PATH "GIMIAS/bin/bin" CACHE PATH "Path to GIMIAS executable folder")
MESSAGE( STATUS "Processing gmWidgets" )

# All binary outputs are written to the same folder.
SET( CMAKE_SUPPRESS_REGENERATION TRUE )
SET( EXECUTABLE_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
SET( LIBRARY_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
cmake_minimum_required(VERSION 2.4.6)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)


ADD_SUBDIRECTORY("${GIMIAS_SRC_PATH}/cmake/library/gmWxEvents" "${GIMIAS_BINARY_PATH}/library/gmWxEvents")
ADD_SUBDIRECTORY("${GIMIAS_SRC_PATH}/cmake/library/DynWxAGUILib" "${GIMIAS_BINARY_PATH}/library/DynWxAGUILib")

INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWidgets/gmWidgetsConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWidgets/UsegmWidgets.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DynWxAGUILib/DynWxAGUILibConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DynWxAGUILib/UseDynWxAGUILib.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/GuiBridgeLibWxWidgets/GuiBridgeLibWxWidgetsConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/GuiBridgeLibWxWidgets/UseGuiBridgeLibWxWidgets.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibWxWidgets/TpExtLibWxWidgetsConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibWxWidgets/UseTpExtLibWxWidgets.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxWebUpdate/tpExtLibWxWebUpdateConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxWebUpdate/UsetpExtLibWxWebUpdate.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxhttpEngine/tpExtLibWxhttpEngineConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxhttpEngine/UsetpExtLibWxhttpEngine.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/GuiBridgeLib/GuiBridgeLibConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/GuiBridgeLib/UseGuiBridgeLib.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWxEvents/gmWxEventsConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWxEvents/UsegmWxEvents.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/WXWIDGETS-3.0.2/WXWIDGETS-3.0.2Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/WXWIDGETS-3.0.2/UseWXWIDGETS-3.0.2.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmProcessors/gmProcessorsConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmProcessors/UsegmProcessors.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmKernel/gmKernelConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmKernel/UsegmKernel.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibBoost/TpExtLibBoostConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibBoost/UseTpExtLibBoost.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DynLib/DynLibConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DynLib/UseDynLib.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LIBELF/LIBELFConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LIBELF/UseLIBELF.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/ExPat/ExPatConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/ExPat/UseExPat.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWorkflow/gmWorkflowConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWorkflow/UsegmWorkflow.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmIO/gmIOConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmIO/UsegmIO.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibUtf8/TpExtLibUtf8Config.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibUtf8/UseTpExtLibUtf8.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmFiltering/gmFilteringConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmFiltering/UsegmFiltering.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/SLICERConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/UseSLICER.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmDataHandling/gmDataHandlingConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmDataHandling/UsegmDataHandling.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmCommonObjects/gmCommonObjectsConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmCommonObjects/UsegmCommonObjects.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/ITK-3.20Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/UseITK-3.20.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/VTK-5.10.1Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/UseVTK-5.10.1.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/HDF5Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/UseHDF5.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/ZLIBConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/UseZLIB.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibNumericData/BaseLibNumericDataConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibNumericData/UseBaseLibNumericData.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TinyXml/TinyXmlConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TinyXml/UseTinyXml.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/BaseLibConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/UseBaseLib.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/LOG4CPLUSConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/UseLOG4CPLUS.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/BOOST-1.45.0Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/UseBOOST-1.45.0.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/CILabMacrosConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/UseCILabMacros.cmake")

#Adding CMake PrecompiledHeader Pre
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PCHSupport_26.cmake" )
GET_NATIVE_PRECOMPILED_HEADER("gmWidgets" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/gmWidgetsPCH.h")

 # Create PCH Files (header) group 
IF (WIN32)
  SOURCE_GROUP("PCH Files (header)" FILES "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/gmWidgetsPCH.h" )
ENDIF(WIN32)


 # Create PCH Files group 
IF (WIN32)
  SOURCE_GROUP("PCH Files" FILES "${GIMIAS_BINARY_PATH}/library/gmWidgets/gmWidgets_pch.cxx" )
ENDIF(WIN32)

#Configure header file and move it to binary folder
CONFIGURE_FILE(${GIMIAS_SRC_PATH}/cmake/library/gmWidgets/gmWidgetsWin32Header.h.in ${GIMIAS_BINARY_PATH}/library/gmWidgets/gmWidgetsWin32Header.h)

# Add target
ADD_LIBRARY(gmWidgets SHARED    "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/gmWidgetsPCH.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CGNSReaderWidget/CGNSFileReaderWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CGNSReaderWidget/CGNSFileReaderWidgetUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CGNSReaderWidget/CGNSFileReaderWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CGNSReaderWidget/CGNSFileReaderWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CommandPanel/coreCommandPanel.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CommandPanel/coreCommandPanel.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreCustomApplicationManagerWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreCustomApplicationManagerWidgetUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreCustomApplicationWizardUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleConfigurationWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleConfigurationWidgetUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleGroupConfigurationWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleGroupConfigurationWidgetUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleParamConfigurationWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleParamConfigurationWidgetUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleParamIOConfigurationWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleParamIOConfigurationWidgetUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreCustomApplicationWizard.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreCustomApplicationManagerWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreCustomApplicationManagerWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreCustomApplicationWizard.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreCustomApplicationWizardPage.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreCustomApplicationWizardUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleConfigurationWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleConfigurationWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleGroupConfigurationWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleGroupConfigurationWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleParamConfigurationWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleParamConfigurationWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleParamIOConfigurationWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/CustomApplicationWizard/coreModuleParamIOConfigurationWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/DataEntityInformation/coreMetaDataWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/DataEntityInformation/coreDataEntityInformation.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/DataEntityInformation/coreNumericDataWidget.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/DataEntityInformation/coreDataEntityInformation.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/DataEntityInformation/coreDataInformationWidgetBase.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/DataEntityInformation/coreMetadataWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/DataEntityInformation/coreNumericDataWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/DataEntityListBrowser/coreDataEntityListBrowser.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/DataEntityListBrowser/coreDataEntityTreeCtrl.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/DataEntityListBrowser/coreDataEntityListBrowser.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/DataEntityListBrowser/coreDataEntityTreeCtrl.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Environment/coreWxEnvironment.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Environment/coreWxEnvironment.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/GlobalPreferences/coreGlobalPreferencesUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/GlobalPreferences/coreGlobalPreferencesWidget.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/GlobalPreferences/coreGlobalPreferencesUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/GlobalPreferences/coreGlobalPreferencesWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Helper/coreUserHelperWidget.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Helper/coreUserHelperWidgetUI.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Helper/coreUserHelperWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Helper/coreUserHelperWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/InputControl/coreBaseInputControl.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/InputControl/coreInputControl.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/InputControl/coreProcessorInputWidget.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/InputControl/coreSelectionComboBox.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/InputControl/coreBaseInputControl.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/InputControl/coreInputControl.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/InputControl/coreProcessorInputWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/InputControl/coreSelectionComboBox.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/LogFileViewer/coreLogFileViewer.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/LogFileViewer/coreLogFileViewer.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MacroPannelWidget/coreMacroPannelWidget.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MacroPannelWidget/coreMacroPannelWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MetadataInformation/coreMetadataInformationWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MetadataInformation/EditTagDialog.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MetadataInformation/EditTagDialogUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MetadataInformation/MetadataInformationWidgetUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MetadataInformation/coreMetadataInformationWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MetadataInformation/EditTagDialog.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MetadataInformation/EditTagDialogUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MetadataInformation/MetadataInformationWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MetadataInformation/TagItemTreeData.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MissingDataEntityFieldsWizard/coreMissingDataEntityFieldsWizard.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MissingDataEntityFieldsWizard/coreMissingDataEntityFieldsWizard.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MovieToolbar/coreMovieToolbarUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MovieToolbar/coreMovieToolbar.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MovieToolbar/coreMovieToolbar.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MovieToolbar/coreMovieToolbarUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreRenderWindowConfigUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreWorkingAreaManagerWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreWorkingAreaManagerWidgetUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreDataTreeHelper.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreRenderWindowBase.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreRenderWindowConfig.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreRenderWindowContainer.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreRenderWindowLinker.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreWorkingAreaConfig.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreWorkingAreaStorage.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreDataEntityUtilities.txx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreCamera3D.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreDataEntityUtilities.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreDataTreeHelper.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreDisplay2D.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreMultiRenderWindow.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreRenderWindowBase.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreRenderWindowConfig.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreRenderWindowConfigBase.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreRenderWindowConfigUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreRenderWindowContainer.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreRenderWindowLinker.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreSlicePlane.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreWorkingAreaConfig.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreWorkingAreaManagerWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreWorkingAreaManagerWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/MultiRenderWindow/coreWorkingAreaStorage.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/PluginTab/corePluginTab.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/PluginTab/corePluginTabFactory.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/PluginTab/coreWorkingAreaManager.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/PluginTab/corePluginTab.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/PluginTab/corePluginTabFactory.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/PluginTab/coreWorkingAreaManager.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Preferences/corePreferencesDialogUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Preferences/corePreferencesDialog.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Preferences/corePreferencesDialog.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Preferences/corePreferencesDialogUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Preferences/corePreferencesPage.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessingToolbox/coreDynProcessingWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessingToolbox/coreDynProcessingWidgetFactory.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessingToolbox/coreProcessingWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessingToolbox/coreSimpleProcessingWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessingToolbox/coreSimpleProcessingWidgetUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessingToolbox/coreBaseToolboxWidget.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessingToolbox/coreProcessingToolboxWidget.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessingToolbox/coreSimpleProcessingWidget.txx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessingToolbox/coreBaseToolboxWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessingToolbox/coreDynProcessingWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessingToolbox/coreDynProcessingWidgetFactory.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessingToolbox/coreProcessingToolboxWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessingToolbox/coreProcessingWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessingToolbox/coreSimpleProcessingWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessingToolbox/coreSimpleProcessingWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessorWidgetsBuilder/coreProcessorWidgetsBuilder.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProcessorWidgetsBuilder/coreProcessorWidgetsBuilder.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProfileWizard/coreImportConfigurationWizard.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProfileWizard/corePluginSelectorWidget.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProfileWizard/corePluginTree.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProfileWizard/coreProfileWizard.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProfileWizard/coreImportConfigurationWizard.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProfileWizard/corePluginSelectorWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProfileWizard/corePluginTree.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProfileWizard/coreProfileWizard.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProgressDialog/coreSimpleProgressDialog.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProgressDialog/coreSimpleProgressFrameUI.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProgressDialog/coreSimpleProgressDialog.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ProgressDialog/coreSimpleProgressFrameUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/PropertiesConfigurationDialog/corePropertiesConfigurationDialog.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/PropertiesConfigurationDialog/corePropertiesConfigurationDialog.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/SelectionToolbox/coreSelectionToolWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/SelectionToolbox/coreSelectionToolboxWidget.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/SelectionToolbox/coreSelectionToolboxWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/SelectionToolbox/coreSelectionToolWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/StatusBar/coreProcessorLogDialog.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/StatusBar/coreProcessorLogDialogUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/StatusBar/coreStatusBar.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/StatusBar/coreProcessorLogDialog.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/StatusBar/coreProcessorLogDialogUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/StatusBar/coreStatusBar.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ThumbnailWidget/coreThumbnailWidgetUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ThumbnailWidget/coreThumbnailWidget.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ThumbnailWidget/coreThumbnailWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/ThumbnailWidget/coreThumbnailWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Toolbar/coreToolbarBase.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Toolbar/coreToolbarIO.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Toolbar/coreToolbarPluginTab.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Toolbar/coreToolbarRegisteredWindows.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Toolbar/coreToolbarWindows.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Toolbar/coreToolbarWorkingArea.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Toolbar/coreToolbarBase.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Toolbar/coreToolbarIO.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Toolbar/coreToolbarPluginTab.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Toolbar/coreToolbarRegisteredWindows.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Toolbar/coreToolbarSelectionTools.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Toolbar/coreToolbarWindows.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Toolbar/coreToolbarWorkingArea.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Toolbox/coreToolboxWidget.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Toolbox/coreToolboxWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/UserRegistration/coreUserRegistrationDialog.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/UserRegistration/coreUserRegistrationDialogUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/UserRegistration/coreUserRegistrationDialog.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/UserRegistration/coreUserRegistrationDialogUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/VisualProperties/coreVisualProperties.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/VisualProperties/coreVisualProperties.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/VisualProperties/coreVisualPropertiesBase.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/WebUpdate/coreWebUpdateDialog.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/WebUpdate/coreWebUpdatePreferencesUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/WebUpdate/coreWebUpdatePreferencesWidget.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/WebUpdate/coreWebUpdateDialog.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/WebUpdate/coreWebUpdatePreferencesUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/WebUpdate/coreWebUpdatePreferencesWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/WidgetCollective/coreWidgetCollective.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/WidgetCollective/coreWidgetCollective.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreBaseTreeWorkflow.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreEmptyToolWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreEmptyToolWidgetUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreTreeRegisteredWindows.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreTreeWorkflow.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowEditorWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowEditorWidgetUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowFinishedWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowFinishedWidgetUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowManagerWidget.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowManagerWidgetUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowNavigationWidgetUI.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreDynDataTransferWorkflow.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreModuleExecutionWorkflow.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowGUIBuilder.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowNavigationWidget.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreBaseTreeWorkflow.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreDynDataTransferWorkflow.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreEmptyToolWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreEmptyToolWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreModuleExecutionWorkflow.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreTreeRegisteredWindows.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreTreeWorkflow.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowEditorWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowEditorWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowFinishedWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowFinishedWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowGUIBuilder.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowManagerWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowManagerWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowNavigationWidget.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowNavigationWidgetUI.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/Workflow/coreWorkflowTreeItemData.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/wxMitkCoreMainWindow/coreMainContextMenu.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/wxMitkCoreMainWindow/coreMainMenu.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/wxMitkCoreMainWindow/coreSplashScreen.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/wxMitkCoreMainWindow/coreStyleManager.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/wxMitkCoreMainWindow/coreWxDataObjectDataEntity.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/wxMitkCoreMainWindow/coreWxMitkCoreMainWindow.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/wxMitkCoreMainWindow/coreMainContextMenu.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/wxMitkCoreMainWindow/coreMainMenu.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/wxMitkCoreMainWindow/coreSplashScreen.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/wxMitkCoreMainWindow/coreStyleManager.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/wxMitkCoreMainWindow/coreWorkingAreaFactory.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/wxMitkCoreMainWindow/coreWxDataObjectDataEntity.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/src/wxMitkCoreMainWindow/coreWxMitkCoreMainWindow.h" "${GIMIAS_BINARY_PATH}/library/gmWidgets/gmWidgets_pch.cxx" )
TARGET_LINK_LIBRARIES(gmWidgets  DynWxAGUILib  GuiBridgeLibWxWidgets  TpExtLibWxWidgets  tpExtLibWxWebUpdate  tpExtLibWxhttpEngine  GuiBridgeLib  gmWxEvents  gmProcessors  gmKernel  TpExtLibBoost  DynLib  ExPat  gmWorkflow  gmIO  TpExtLibUtf8  gmFiltering  gmDataHandling  gmCommonObjects  BaseLibNumericData  TinyXml  BaseLib  CILabMacros ${gmWidgets_LIBRARIES} )

IF(WIN32)
 IF( MSVC) ## NOT SURE "UNICODE" IS NEEDED FOR OTHER THAN MSVC
  ADD_DEFINITIONS(
  /DUNICODE
  )
 ENDIF(MSVC)
ELSE(WIN32)
 ## NOT SURE "UNICODE" IS NEEDED FOR LINUX
ENDIF(WIN32)

#Adding specific windows macros
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PlatformDependent.cmake" )
INCREASE_MSVC_HEAP_LIMIT( 1000 )
SUPPRESS_VC8_DEPRECATED_WARNINGS( )
SUPPRESS_LINKER_WARNING_4089( gmWidgets )
SUPPRESS_COMPILER_WARNING_DLL_EXPORT( gmWidgets )

#Adding properties

#Adding CMake PrecompiledHeader Post
ADD_NATIVE_PRECOMPILED_HEADER("gmWidgets" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/Widgets/gmWidgetsPCH.h")

ADD_DEPENDENCIES(gmWidgets CILabMacros)
ADD_DEPENDENCIES(gmWidgets BaseLib)
ADD_DEPENDENCIES(gmWidgets TinyXml)
ADD_DEPENDENCIES(gmWidgets BaseLibNumericData)
ADD_DEPENDENCIES(gmWidgets gmCommonObjects)
ADD_DEPENDENCIES(gmWidgets gmDataHandling)
ADD_DEPENDENCIES(gmWidgets gmFiltering)
ADD_DEPENDENCIES(gmWidgets TpExtLibUtf8)
ADD_DEPENDENCIES(gmWidgets gmIO)
ADD_DEPENDENCIES(gmWidgets gmWorkflow)
ADD_DEPENDENCIES(gmWidgets ExPat)
ADD_DEPENDENCIES(gmWidgets DynLib)
ADD_DEPENDENCIES(gmWidgets TpExtLibBoost)
ADD_DEPENDENCIES(gmWidgets gmKernel)
ADD_DEPENDENCIES(gmWidgets gmProcessors)
ADD_DEPENDENCIES(gmWidgets gmWxEvents)
ADD_DEPENDENCIES(gmWidgets GuiBridgeLib)
ADD_DEPENDENCIES(gmWidgets tpExtLibWxhttpEngine)
ADD_DEPENDENCIES(gmWidgets tpExtLibWxWebUpdate)
ADD_DEPENDENCIES(gmWidgets TpExtLibWxWidgets)
ADD_DEPENDENCIES(gmWidgets GuiBridgeLibWxWidgets)
ADD_DEPENDENCIES(gmWidgets DynWxAGUILib)
