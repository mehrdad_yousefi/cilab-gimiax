# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( WflLib_FOUND TRUE )
SET( WflLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/WflLib/UseWflLib.cmake" )
SET( WflLib_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/WflLib/libmodules/wflBase/src" "${GIMIAS_SRC_PATH}/Modules/WflLib/libmodules/wflIO/src" "${GIMIAS_BINARY_PATH}/library/WflLib"  )
SET( WflLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( WflLib_LIBRARIES ${WflLib_LIBRARIES} "WflLib"  )
