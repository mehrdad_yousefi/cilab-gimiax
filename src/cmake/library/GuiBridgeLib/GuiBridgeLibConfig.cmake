# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( GuiBridgeLib_FOUND TRUE )
SET( GuiBridgeLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/GuiBridgeLib/UseGuiBridgeLib.cmake" )
SET( GuiBridgeLib_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/GuiBridgeLib/libmodules/guiBridgeLib/src" "${GIMIAS_BINARY_PATH}/library/GuiBridgeLib"  )
SET( GuiBridgeLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( GuiBridgeLib_LIBRARIES ${GuiBridgeLib_LIBRARIES} "GuiBridgeLib"  )
