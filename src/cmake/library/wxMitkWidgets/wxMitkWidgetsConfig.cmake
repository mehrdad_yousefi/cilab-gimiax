# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( wxMitkWidgets_FOUND TRUE )
SET( wxMitkWidgets_USE_FILE "${GIMIAS_BINARY_PATH}/library/wxMitkWidgets/UsewxMitkWidgets.cmake" )
SET( wxMitkWidgets_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkWidgets/src" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkWidgets/include" "${GIMIAS_SRC_PATH}/Modules/wxMitk/libmodules/wxMitkWidgets/resource" "${GIMIAS_BINARY_PATH}/library/wxMitkWidgets"  )
SET( wxMitkWidgets_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( wxMitkWidgets_LIBRARIES ${wxMitkWidgets_LIBRARIES} "wxMitkWidgets"  )
