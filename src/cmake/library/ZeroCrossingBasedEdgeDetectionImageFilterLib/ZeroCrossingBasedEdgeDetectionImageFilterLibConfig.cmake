# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ZeroCrossingBasedEdgeDetectionImageFilterLib_FOUND TRUE )
SET( ZeroCrossingBasedEdgeDetectionImageFilterLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/ZeroCrossingBasedEdgeDetectionImageFilterLib/UseZeroCrossingBasedEdgeDetectionImageFilterLib.cmake" )
SET( ZeroCrossingBasedEdgeDetectionImageFilterLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/ZeroCrossingBasedEdgeDetectionImageFilterLib"  )
SET( ZeroCrossingBasedEdgeDetectionImageFilterLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( ZeroCrossingBasedEdgeDetectionImageFilterLib_LIBRARIES ${ZeroCrossingBasedEdgeDetectionImageFilterLib_LIBRARIES} "ZeroCrossingBasedEdgeDetectionImageFilterLib"  )
