# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( BaseLibNumericData_FOUND TRUE )
SET( BaseLibNumericData_USE_FILE "${GIMIAS_BINARY_PATH}/library/BaseLibNumericData/UseBaseLibNumericData.cmake" )
SET( BaseLibNumericData_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blNumericData/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blNumericData/include" "${GIMIAS_BINARY_PATH}/library/BaseLibNumericData"  )
SET( BaseLibNumericData_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( BaseLibNumericData_LIBRARIES ${BaseLibNumericData_LIBRARIES} "BaseLibNumericData"  )
