# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( LabelMapSmoothingLib_FOUND TRUE )
SET( LabelMapSmoothingLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/LabelMapSmoothingLib/UseLabelMapSmoothingLib.cmake" )
SET( LabelMapSmoothingLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/LabelMapSmoothingLib"  )
SET( LabelMapSmoothingLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( LabelMapSmoothingLib_LIBRARIES ${LabelMapSmoothingLib_LIBRARIES} "LabelMapSmoothingLib"  )
