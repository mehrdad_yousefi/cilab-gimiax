# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( CurvatureAnisotropicDiffusionLib_FOUND TRUE )
SET( CurvatureAnisotropicDiffusionLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/CurvatureAnisotropicDiffusionLib/UseCurvatureAnisotropicDiffusionLib.cmake" )
SET( CurvatureAnisotropicDiffusionLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/CurvatureAnisotropicDiffusionLib"  )
SET( CurvatureAnisotropicDiffusionLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( CurvatureAnisotropicDiffusionLib_LIBRARIES ${CurvatureAnisotropicDiffusionLib_LIBRARIES} "CurvatureAnisotropicDiffusionLib"  )
