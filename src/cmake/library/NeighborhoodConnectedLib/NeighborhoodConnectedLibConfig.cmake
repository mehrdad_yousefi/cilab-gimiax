# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( NeighborhoodConnectedLib_FOUND TRUE )
SET( NeighborhoodConnectedLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/NeighborhoodConnectedLib/UseNeighborhoodConnectedLib.cmake" )
SET( NeighborhoodConnectedLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/NeighborhoodConnectedLib"  )
SET( NeighborhoodConnectedLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( NeighborhoodConnectedLib_LIBRARIES ${NeighborhoodConnectedLib_LIBRARIES} "NeighborhoodConnectedLib"  )
