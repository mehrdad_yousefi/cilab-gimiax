# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ConnectedThresholdLib_FOUND TRUE )
SET( ConnectedThresholdLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/ConnectedThresholdLib/UseConnectedThresholdLib.cmake" )
SET( ConnectedThresholdLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/ConnectedThresholdLib"  )
SET( ConnectedThresholdLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( ConnectedThresholdLib_LIBRARIES ${ConnectedThresholdLib_LIBRARIES} "ConnectedThresholdLib"  )
