# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( DynLib_FOUND TRUE )
SET( DynLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/DynLib/UseDynLib.cmake" )
SET( DynLib_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/DynLib/libmodules/dynBase/src" "${GIMIAS_BINARY_PATH}/library/DynLib"  )
SET( DynLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
if(WIN32)
    SET( DynLib_LIBRARIES ${DynLib_LIBRARIES} "Dbghelp.lib" )
endif(WIN32)

SET( DynLib_LIBRARIES ${DynLib_LIBRARIES} "DynLib"  )
