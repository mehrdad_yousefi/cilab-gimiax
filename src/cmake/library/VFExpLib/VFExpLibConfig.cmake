# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( VFExpLib_FOUND TRUE )
SET( VFExpLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/VFExpLib/UseVFExpLib.cmake" )
SET( VFExpLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/VFExpLib"  )
SET( VFExpLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( VFExpLib_LIBRARIES ${VFExpLib_LIBRARIES} "VFExpLib"  )
