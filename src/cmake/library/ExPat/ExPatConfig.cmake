# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ExPat_FOUND TRUE )
SET( ExPat_USE_FILE "${GIMIAS_BINARY_PATH}/library/ExPat/UseExPat.cmake" )
SET( ExPat_INCLUDE_DIRS "${GIMIAS_THIRD_PARTY_SCR_PATH}/EXPAT/Expat-2.0.1/Source/lib" "${GIMIAS_BINARY_PATH}/library/ExPat"  )
SET( ExPat_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( ExPat_LIBRARIES ${ExPat_LIBRARIES} "ExPat"  )
