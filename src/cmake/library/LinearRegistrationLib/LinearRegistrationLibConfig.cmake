# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( LinearRegistrationLib_FOUND TRUE )
SET( LinearRegistrationLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/LinearRegistrationLib/UseLinearRegistrationLib.cmake" )
SET( LinearRegistrationLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/LinearRegistrationLib"  )
SET( LinearRegistrationLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( LinearRegistrationLib_LIBRARIES ${LinearRegistrationLib_LIBRARIES} "LinearRegistrationLib"  )
