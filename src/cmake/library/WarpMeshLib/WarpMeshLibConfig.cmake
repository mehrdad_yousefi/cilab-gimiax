# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( WarpMeshLib_FOUND TRUE )
SET( WarpMeshLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/WarpMeshLib/UseWarpMeshLib.cmake" )
SET( WarpMeshLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/WarpMeshLib"  )
SET( WarpMeshLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( WarpMeshLib_LIBRARIES ${WarpMeshLib_LIBRARIES} "WarpMeshLib"  )
