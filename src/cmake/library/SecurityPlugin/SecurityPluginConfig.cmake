# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( SecurityPlugin_FOUND TRUE )
SET( SecurityPlugin_USE_FILE "${GIMIAS_BINARY_PATH}/library/SecurityPlugin/UseSecurityPlugin.cmake" )
SET( SecurityPlugin_INCLUDE_DIRS "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/SecurityPlugin" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/SecurityPlugin/processors" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/SecurityPlugin/widgets/SecurityPluginUserManager" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/SecurityPlugin/widgets/SecurityPluginUserManager" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/SecurityPlugin/widgets/PluginSecurityWidget" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/SecurityPlugin/widgets/PluginSecurityWidget" "${GIMIAS_BINARY_PATH}/library/SecurityPlugin"  )
SET( SecurityPlugin_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/plugins/SecurityPlugin/lib"  )
SET( SecurityPlugin_LIBRARIES ${SecurityPlugin_LIBRARIES} "SecurityPlugin"  )
