# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( WebServicesPlugin_FOUND TRUE )
SET( WebServicesPlugin_USE_FILE "${GIMIAS_BINARY_PATH}/library/WebServicesPlugin/UseWebServicesPlugin.cmake" )
SET( WebServicesPlugin_INCLUDE_DIRS "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/WebServicesPlugin" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/WebServicesPlugin/processors" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/WebServicesPlugin/widgets/WebServicesPreferencesPanelWidget" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/WebServicesPlugin/widgets/WebServicesPreferencesPanelWidget" "${GIMIAS_BINARY_PATH}/library/WebServicesPlugin"  )
SET( WebServicesPlugin_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/plugins/WebServicesPlugin/lib"  )
SET( WebServicesPlugin_LIBRARIES ${WebServicesPlugin_LIBRARIES} "WebServicesPlugin"  )
