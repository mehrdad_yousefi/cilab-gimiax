# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( SkullStripperLib_FOUND TRUE )
SET( SkullStripperLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/SkullStripperLib/UseSkullStripperLib.cmake" )
SET( SkullStripperLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/SkullStripperLib"  )
SET( SkullStripperLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( SkullStripperLib_LIBRARIES ${SkullStripperLib_LIBRARIES} "SkullStripperLib"  )
