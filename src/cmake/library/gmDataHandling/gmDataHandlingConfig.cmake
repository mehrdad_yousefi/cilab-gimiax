# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( gmDataHandling_FOUND TRUE )
SET( gmDataHandling_USE_FILE "${GIMIAS_BINARY_PATH}/library/gmDataHandling/UsegmDataHandling.cmake" )
SET( gmDataHandling_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/DataHandling/src" "${GIMIAS_BINARY_PATH}/library/gmDataHandling"  )
SET( gmDataHandling_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( gmDataHandling_LIBRARIES ${gmDataHandling_LIBRARIES} "gmDataHandling"  )
