# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( BaseLibSignal_FOUND TRUE )
SET( BaseLibSignal_USE_FILE "${GIMIAS_BINARY_PATH}/library/BaseLibSignal/UseBaseLibSignal.cmake" )
SET( BaseLibSignal_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blSignal/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blSignal/include" "${GIMIAS_BINARY_PATH}/library/BaseLibSignal"  )
SET( BaseLibSignal_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( BaseLibSignal_LIBRARIES ${BaseLibSignal_LIBRARIES} "BaseLibSignal"  )
