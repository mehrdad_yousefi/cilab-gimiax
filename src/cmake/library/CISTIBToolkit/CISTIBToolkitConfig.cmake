# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( CISTIBToolkit_FOUND TRUE )
SET( CISTIBToolkit_USE_FILE "${GIMIAS_BINARY_PATH}/library/CISTIBToolkit/UseCISTIBToolkit.cmake" )
SET( CISTIBToolkit_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/CISTIBToolkit"  )
SET( CISTIBToolkit_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
