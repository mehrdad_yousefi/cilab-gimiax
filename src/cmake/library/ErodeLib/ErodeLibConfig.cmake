# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ErodeLib_FOUND TRUE )
SET( ErodeLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/ErodeLib/UseErodeLib.cmake" )
SET( ErodeLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/ErodeLib"  )
SET( ErodeLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( ErodeLib_LIBRARIES ${ErodeLib_LIBRARIES} "ErodeLib"  )
