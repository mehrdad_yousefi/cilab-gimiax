# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ResampleVolumeLib_FOUND TRUE )
SET( ResampleVolumeLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/ResampleVolumeLib/UseResampleVolumeLib.cmake" )
SET( ResampleVolumeLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/ResampleVolumeLib"  )
SET( ResampleVolumeLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( ResampleVolumeLib_LIBRARIES ${ResampleVolumeLib_LIBRARIES} "ResampleVolumeLib"  )
