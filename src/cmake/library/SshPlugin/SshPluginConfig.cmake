# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( SshPlugin_FOUND TRUE )
SET( SshPlugin_USE_FILE "${GIMIAS_BINARY_PATH}/library/SshPlugin/UseSshPlugin.cmake" )
SET( SshPlugin_INCLUDE_DIRS "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/SshPlugin" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/SshPlugin/processors" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/SshPlugin/widgets/SshPluginConfigurationPanelWidget" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/SshPlugin/widgets/SshPluginConfigurationPanelWidget" "${GIMIAS_BINARY_PATH}/library/SshPlugin"  )
SET( SshPlugin_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/plugins/SshPlugin/lib"  )
SET( SshPlugin_LIBRARIES ${SshPlugin_LIBRARIES} "SshPlugin"  )
