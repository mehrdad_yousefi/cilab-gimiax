# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ConfidenceConnectedLib_FOUND TRUE )
SET( ConfidenceConnectedLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/ConfidenceConnectedLib/UseConfidenceConnectedLib.cmake" )
SET( ConfidenceConnectedLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/ConfidenceConnectedLib"  )
SET( ConfidenceConnectedLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( ConfidenceConnectedLib_LIBRARIES ${ConfidenceConnectedLib_LIBRARIES} "ConfidenceConnectedLib"  )
