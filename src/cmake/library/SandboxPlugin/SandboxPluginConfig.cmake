# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( SandboxPlugin_FOUND TRUE )
SET( SandboxPlugin_USE_FILE "${GIMIAS_BINARY_PATH}/library/SandboxPlugin/UseSandboxPlugin.cmake" )
SET( SandboxPlugin_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Apps/Plugins/SandboxPlugin" "${GIMIAS_SRC_PATH}/Apps/Plugins/SandboxPlugin/processors" "${GIMIAS_SRC_PATH}/Apps/Plugins/SandboxPlugin/widgets/SandboxPluginShapeScalePanelWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/SandboxPlugin/widgets/SandboxPluginShapeScalePanelWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/SandboxPlugin/widgets/SandboxPluginSubtractPanelWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/SandboxPlugin/widgets/SandboxPluginSubtractPanelWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/SandboxPlugin/widgets/SandboxPluginResamplePanelWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/SandboxPlugin/widgets/SandboxPluginResamplePanelWidget" "${GIMIAS_BINARY_PATH}/library/SandboxPlugin"  )
SET( SandboxPlugin_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/plugins/SandboxPlugin/lib"  )
SET( SandboxPlugin_LIBRARIES ${SandboxPlugin_LIBRARIES} "SandboxPlugin"  )
