# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( TpExtLibMITK_FOUND TRUE )
SET( TpExtLibMITK_USE_FILE "${GIMIAS_BINARY_PATH}/library/TpExtLibMITK/UseTpExtLibMITK.cmake" )
SET( TpExtLibMITK_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtMITK/src" "${GIMIAS_BINARY_PATH}/library/TpExtLibMITK"  )
SET( TpExtLibMITK_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( TpExtLibMITK_LIBRARIES ${TpExtLibMITK_LIBRARIES} "TpExtLibMITK"  )
