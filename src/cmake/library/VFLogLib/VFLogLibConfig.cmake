# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( VFLogLib_FOUND TRUE )
SET( VFLogLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/VFLogLib/UseVFLogLib.cmake" )
SET( VFLogLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/VFLogLib"  )
SET( VFLogLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( VFLogLib_LIBRARIES ${VFLogLib_LIBRARIES} "VFLogLib"  )
