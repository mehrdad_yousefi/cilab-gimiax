# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( gmWxEvents_FOUND TRUE )
SET( gmWxEvents_USE_FILE "${GIMIAS_BINARY_PATH}/library/gmWxEvents/UsegmWxEvents.cmake" )
SET( gmWxEvents_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src" "${GIMIAS_BINARY_PATH}/library/gmWxEvents"  )
SET( gmWxEvents_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( gmWxEvents_LIBRARIES ${gmWxEvents_LIBRARIES} "gmWxEvents"  )
