# CMakeLists.txt generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

PROJECT(gmWxEvents)
SET( GIMIAS_BINARY_PATH "GIMIAS/bin" CACHE PATH "Path to GIMIAS binaries folder")
SET( GIMIAS_SRC_PATH "GIMIAS/src" CACHE PATH "Path to GIMIAS source code folder")
SET( GIMIAS_THIRD_PARTY_SCR_PATH "GIMIAS/thirdParty" CACHE PATH "Path to GIMIAS thirdparty libraries source code")
SET( GIMIAS_THIRD_PARTY_BINARIES_PATH "GIMIAS/bin/thirdParty" CACHE PATH "Path to GIMIAS thirdparty binaries")
SET( GIMIAS_EXECUTABLE_PATH "GIMIAS/bin/bin" CACHE PATH "Path to GIMIAS executable folder")
MESSAGE( STATUS "Processing gmWxEvents" )

# All binary outputs are written to the same folder.
SET( CMAKE_SUPPRESS_REGENERATION TRUE )
SET( EXECUTABLE_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
SET( LIBRARY_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
cmake_minimum_required(VERSION 2.4.6)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)


ADD_SUBDIRECTORY("${GIMIAS_SRC_PATH}/cmake/library/gmProcessors" "${GIMIAS_BINARY_PATH}/library/gmProcessors")

INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWxEvents/gmWxEventsConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWxEvents/UsegmWxEvents.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/WXWIDGETS-3.0.2/WXWIDGETS-3.0.2Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/WXWIDGETS-3.0.2/UseWXWIDGETS-3.0.2.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmProcessors/gmProcessorsConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmProcessors/UsegmProcessors.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmKernel/gmKernelConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmKernel/UsegmKernel.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibBoost/TpExtLibBoostConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibBoost/UseTpExtLibBoost.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DynLib/DynLibConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DynLib/UseDynLib.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LIBELF/LIBELFConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LIBELF/UseLIBELF.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/ExPat/ExPatConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/ExPat/UseExPat.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWorkflow/gmWorkflowConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWorkflow/UsegmWorkflow.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmIO/gmIOConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmIO/UsegmIO.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibUtf8/TpExtLibUtf8Config.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibUtf8/UseTpExtLibUtf8.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmFiltering/gmFilteringConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmFiltering/UsegmFiltering.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/SLICERConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/UseSLICER.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmDataHandling/gmDataHandlingConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmDataHandling/UsegmDataHandling.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmCommonObjects/gmCommonObjectsConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmCommonObjects/UsegmCommonObjects.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/ITK-3.20Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/UseITK-3.20.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/VTK-5.10.1Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/UseVTK-5.10.1.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/HDF5Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/UseHDF5.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/ZLIBConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/UseZLIB.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibNumericData/BaseLibNumericDataConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibNumericData/UseBaseLibNumericData.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TinyXml/TinyXmlConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TinyXml/UseTinyXml.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/BaseLibConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/UseBaseLib.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/LOG4CPLUSConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/UseLOG4CPLUS.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/BOOST-1.45.0Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/UseBOOST-1.45.0.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/CILabMacrosConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/UseCILabMacros.cmake")

#Adding CMake PrecompiledHeader Pre
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PCHSupport_26.cmake" )
GET_NATIVE_PRECOMPILED_HEADER("gmWxEvents" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/gmWxEventsPCH.h")

 # Create PCH Files (header) group 
IF (WIN32)
  SOURCE_GROUP("PCH Files (header)" FILES "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/gmWxEventsPCH.h" )
ENDIF(WIN32)


 # Create PCH Files group 
IF (WIN32)
  SOURCE_GROUP("PCH Files" FILES "${GIMIAS_BINARY_PATH}/library/gmWxEvents/gmWxEvents_pch.cxx" )
ENDIF(WIN32)

#Configure header file and move it to binary folder
CONFIGURE_FILE(${GIMIAS_SRC_PATH}/cmake/library/gmWxEvents/gmWxEventsWin32Header.h.in ${GIMIAS_BINARY_PATH}/library/gmWxEvents/gmWxEventsWin32Header.h)


# Add target
ADD_LIBRARY(gmWxEvents SHARED    "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/gmWxEventsPCH.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxCallbackObserver.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxGenericEvent.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxGenericEvtHandler.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxPortInvocation.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxProcessorEvtHandler.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxUpdateCallbackEvent.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxUpdatePortEvent.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxWaitEvtHandler.cpp" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxEventsFactoriesRegistration.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxCallbackObserver.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxEventsFactoriesRegistration.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxGenericEvent.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxGenericEvtHandler.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxPortInvocation.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxProcessorEvtHandler.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxUpdateCallbackEvent.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxUpdatePortEvent.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/src/coreWxWaitEvtHandler.h" "${GIMIAS_BINARY_PATH}/library/gmWxEvents/gmWxEvents_pch.cxx" )
TARGET_LINK_LIBRARIES(gmWxEvents  gmProcessors  gmKernel  TpExtLibBoost  DynLib  ExPat  gmWorkflow  gmIO  TpExtLibUtf8  gmFiltering  gmDataHandling  gmCommonObjects  BaseLibNumericData  TinyXml  BaseLib  CILabMacros ${gmWxEvents_LIBRARIES} )

IF(WIN32)
 IF( MSVC) ## NOT SURE "UNICODE" IS NEEDED FOR OTHER THAN MSVC
  ADD_DEFINITIONS(
  /DUNICODE
  )
 ENDIF(MSVC)
ELSE(WIN32)
 ## NOT SURE "UNICODE" IS NEEDED FOR LINUX
ENDIF(WIN32)

#Adding specific windows macros
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PlatformDependent.cmake" )
INCREASE_MSVC_HEAP_LIMIT( 1000 )
SUPPRESS_VC8_DEPRECATED_WARNINGS( )
SUPPRESS_LINKER_WARNING_4089( gmWxEvents )
SUPPRESS_COMPILER_WARNING_DLL_EXPORT( gmWxEvents )

#Adding properties

#Adding CMake PrecompiledHeader Post
ADD_NATIVE_PRECOMPILED_HEADER("gmWxEvents" "${GIMIAS_SRC_PATH}/Apps/Gimias/GUI/WxEvents/gmWxEventsPCH.h")

ADD_DEPENDENCIES(gmWxEvents CILabMacros)
ADD_DEPENDENCIES(gmWxEvents BaseLib)
ADD_DEPENDENCIES(gmWxEvents TinyXml)
ADD_DEPENDENCIES(gmWxEvents BaseLibNumericData)
ADD_DEPENDENCIES(gmWxEvents gmCommonObjects)
ADD_DEPENDENCIES(gmWxEvents gmDataHandling)
ADD_DEPENDENCIES(gmWxEvents gmFiltering)
ADD_DEPENDENCIES(gmWxEvents TpExtLibUtf8)
ADD_DEPENDENCIES(gmWxEvents gmIO)
ADD_DEPENDENCIES(gmWxEvents gmWorkflow)
ADD_DEPENDENCIES(gmWxEvents ExPat)
ADD_DEPENDENCIES(gmWxEvents DynLib)
ADD_DEPENDENCIES(gmWxEvents TpExtLibBoost)
ADD_DEPENDENCIES(gmWxEvents gmKernel)
ADD_DEPENDENCIES(gmWxEvents gmProcessors)
