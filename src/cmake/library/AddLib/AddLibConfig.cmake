# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( AddLib_FOUND TRUE )
SET( AddLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/AddLib/UseAddLib.cmake" )
SET( AddLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/AddLib"  )
SET( AddLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( AddLib_LIBRARIES ${AddLib_LIBRARIES} "AddLib"  )
