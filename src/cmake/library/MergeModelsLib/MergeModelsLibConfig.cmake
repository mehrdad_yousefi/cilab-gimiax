# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( MergeModelsLib_FOUND TRUE )
SET( MergeModelsLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/MergeModelsLib/UseMergeModelsLib.cmake" )
SET( MergeModelsLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/MergeModelsLib"  )
SET( MergeModelsLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( MergeModelsLib_LIBRARIES ${MergeModelsLib_LIBRARIES} "MergeModelsLib"  )
