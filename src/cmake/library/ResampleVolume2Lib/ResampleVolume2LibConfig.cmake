# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ResampleVolume2Lib_FOUND TRUE )
SET( ResampleVolume2Lib_USE_FILE "${GIMIAS_BINARY_PATH}/library/ResampleVolume2Lib/UseResampleVolume2Lib.cmake" )
SET( ResampleVolume2Lib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/ResampleVolume2Lib"  )
SET( ResampleVolume2Lib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( ResampleVolume2Lib_LIBRARIES ${ResampleVolume2Lib_LIBRARIES} "ResampleVolume2Lib"  )
