# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( CILabMacros_FOUND TRUE )
SET( CILabMacros_USE_FILE "${GIMIAS_BINARY_PATH}/library/CILabMacros/UseCILabMacros.cmake" )
SET( CILabMacros_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/CILabMacros/libmodules/CILabMacros/include" "${GIMIAS_SRC_PATH}/Modules/CILabMacros/libmodules/Loki/include" "${GIMIAS_BINARY_PATH}/library/CILabMacros"  )
SET( CILabMacros_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( CILabMacros_LIBRARIES ${CILabMacros_LIBRARIES} "CILabMacros"  )
