# CMakeLists.txt generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

PROJECT(TpExtLibBoost)
SET( GIMIAS_BINARY_PATH "GIMIAS/bin" CACHE PATH "Path to GIMIAS binaries folder")
SET( GIMIAS_SRC_PATH "GIMIAS/src" CACHE PATH "Path to GIMIAS source code folder")
SET( GIMIAS_THIRD_PARTY_SCR_PATH "GIMIAS/thirdParty" CACHE PATH "Path to GIMIAS thirdparty libraries source code")
SET( GIMIAS_THIRD_PARTY_BINARIES_PATH "GIMIAS/bin/thirdParty" CACHE PATH "Path to GIMIAS thirdparty binaries")
SET( GIMIAS_EXECUTABLE_PATH "GIMIAS/bin/bin" CACHE PATH "Path to GIMIAS executable folder")
MESSAGE( STATUS "Processing TpExtLibBoost" )

# All binary outputs are written to the same folder.
SET( CMAKE_SUPPRESS_REGENERATION TRUE )
SET( EXECUTABLE_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
SET( LIBRARY_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
cmake_minimum_required(VERSION 2.4.6)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)



INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibBoost/TpExtLibBoostConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibBoost/UseTpExtLibBoost.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/BOOST-1.45.0Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/UseBOOST-1.45.0.cmake")

#Configure header file and move it to binary folder
CONFIGURE_FILE(${GIMIAS_SRC_PATH}/cmake/library/TpExtLibBoost/TpExtLibBoostWin32Header.h.in ${GIMIAS_BINARY_PATH}/library/TpExtLibBoost/TpExtLibBoostWin32Header.h)


set(build_type_library SHARED)
if(WIN32)
  set(build_type_library STATIC)
endif(WIN32)

# Add target
ADD_LIBRARY(TpExtLibBoost ${build_type_library} "${GIMIAS_SRC_PATH}/csnake_dummy.cpp" )
ADD_DEFINITIONS( )

#Adding specific windows macros
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PlatformDependent.cmake" )
INCREASE_MSVC_HEAP_LIMIT( 1000 )
SUPPRESS_VC8_DEPRECATED_WARNINGS( )
SUPPRESS_LINKER_WARNING_4089( TpExtLibBoost )
SUPPRESS_COMPILER_WARNING_DLL_EXPORT( TpExtLibBoost )

#Adding properties

