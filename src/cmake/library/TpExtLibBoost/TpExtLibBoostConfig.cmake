# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( TpExtLibBoost_FOUND TRUE )
SET( TpExtLibBoost_USE_FILE "${GIMIAS_BINARY_PATH}/library/TpExtLibBoost/UseTpExtLibBoost.cmake" )
SET( TpExtLibBoost_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtBoost/src" "${GIMIAS_BINARY_PATH}/library/TpExtLibBoost"  )
SET( TpExtLibBoost_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( TpExtLibBoost_LIBRARIES ${TpExtLibBoost_LIBRARIES} "TpExtLibBoost"  )
