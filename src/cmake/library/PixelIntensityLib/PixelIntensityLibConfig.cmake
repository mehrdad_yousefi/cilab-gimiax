# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( PixelIntensityLib_FOUND TRUE )
SET( PixelIntensityLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/PixelIntensityLib/UsePixelIntensityLib.cmake" )
SET( PixelIntensityLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/PixelIntensityLib"  )
SET( PixelIntensityLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( PixelIntensityLib_LIBRARIES ${PixelIntensityLib_LIBRARIES} "PixelIntensityLib"  )
