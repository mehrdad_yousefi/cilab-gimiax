# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( RigidRegistrationLib_FOUND TRUE )
SET( RigidRegistrationLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/RigidRegistrationLib/UseRigidRegistrationLib.cmake" )
SET( RigidRegistrationLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/RigidRegistrationLib"  )
SET( RigidRegistrationLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( RigidRegistrationLib_LIBRARIES ${RigidRegistrationLib_LIBRARIES} "RigidRegistrationLib"  )
