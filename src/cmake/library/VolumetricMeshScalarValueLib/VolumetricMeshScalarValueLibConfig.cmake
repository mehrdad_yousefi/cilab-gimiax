# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( VolumetricMeshScalarValueLib_FOUND TRUE )
SET( VolumetricMeshScalarValueLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/VolumetricMeshScalarValueLib/UseVolumetricMeshScalarValueLib.cmake" )
SET( VolumetricMeshScalarValueLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/VolumetricMeshScalarValueLib"  )
SET( VolumetricMeshScalarValueLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( VolumetricMeshScalarValueLib_LIBRARIES ${VolumetricMeshScalarValueLib_LIBRARIES} "VolumetricMeshScalarValueLib"  )
