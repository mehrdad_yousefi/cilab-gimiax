# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( BaseLibTetgen_FOUND TRUE )
SET( BaseLibTetgen_USE_FILE "${GIMIAS_BINARY_PATH}/library/BaseLibTetgen/UseBaseLibTetgen.cmake" )
SET( BaseLibTetgen_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blMeshIO/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blTetgen/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blMeshIO/include" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blTetgen/include" "${GIMIAS_BINARY_PATH}/library/BaseLibTetgen"  )
SET( BaseLibTetgen_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( BaseLibTetgen_LIBRARIES ${BaseLibTetgen_LIBRARIES} "BaseLibTetgen"  )
