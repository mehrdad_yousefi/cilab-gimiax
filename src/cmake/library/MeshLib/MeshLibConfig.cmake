# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( MeshLib_FOUND TRUE )
SET( MeshLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/MeshLib/UseMeshLib.cmake" )
SET( MeshLib_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/MeshLib/libmodules/meMesh/src" "${GIMIAS_SRC_PATH}/Modules/MeshLib/libmodules/meTools/src" "${GIMIAS_SRC_PATH}/Modules/MeshLib/libmodules/mePolyline/src" "${GIMIAS_SRC_PATH}/Modules/MeshLib/libmodules/meMeshFilters/src" "${GIMIAS_SRC_PATH}/Modules/MeshLib/libmodules/meBoolean/src" "${GIMIAS_SRC_PATH}/Modules/MeshLib/libmodules/meMesh/include" "${GIMIAS_SRC_PATH}/Modules/MeshLib/libmodules/meTools/include" "${GIMIAS_SRC_PATH}/Modules/MeshLib/libmodules/mePolyline/include" "${GIMIAS_SRC_PATH}/Modules/MeshLib/libmodules/meMeshFilters/include" "${GIMIAS_SRC_PATH}/Modules/MeshLib/libmodules/meBoolean/include" "${GIMIAS_BINARY_PATH}/library/MeshLib"  )
SET( MeshLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( MeshLib_LIBRARIES ${MeshLib_LIBRARIES} "MeshLib"  )
