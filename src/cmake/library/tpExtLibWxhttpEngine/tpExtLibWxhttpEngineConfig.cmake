# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( tpExtLibWxhttpEngine_FOUND TRUE )
SET( tpExtLibWxhttpEngine_USE_FILE "${GIMIAS_BINARY_PATH}/library/tpExtLibWxhttpEngine/UsetpExtLibWxhttpEngine.cmake" )
SET( tpExtLibWxhttpEngine_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxhttpEngine/src" "${GIMIAS_BINARY_PATH}/library/tpExtLibWxhttpEngine"  )
SET( tpExtLibWxhttpEngine_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( tpExtLibWxhttpEngine_LIBRARIES ${tpExtLibWxhttpEngine_LIBRARIES} "tpExtLibWxhttpEngine"  )
