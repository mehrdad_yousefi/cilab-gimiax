# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( VFDownSampleLib_FOUND TRUE )
SET( VFDownSampleLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/VFDownSampleLib/UseVFDownSampleLib.cmake" )
SET( VFDownSampleLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/VFDownSampleLib"  )
SET( VFDownSampleLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( VFDownSampleLib_LIBRARIES ${VFDownSampleLib_LIBRARIES} "VFDownSampleLib"  )
