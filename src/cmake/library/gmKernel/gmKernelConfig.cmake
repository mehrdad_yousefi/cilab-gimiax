# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( gmKernel_FOUND TRUE )
SET( gmKernel_USE_FILE "${GIMIAS_BINARY_PATH}/library/gmKernel/UsegmKernel.cmake" )
SET( gmKernel_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Kernel/src" "${GIMIAS_BINARY_PATH}/library/gmKernel"  )
SET( gmKernel_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( gmKernel_LIBRARIES ${gmKernel_LIBRARIES} "gmKernel"  )
