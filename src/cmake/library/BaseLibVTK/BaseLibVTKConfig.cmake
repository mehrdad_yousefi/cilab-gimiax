# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( BaseLibVTK_FOUND TRUE )
SET( BaseLibVTK_USE_FILE "${GIMIAS_BINARY_PATH}/library/BaseLibVTK/UseBaseLibVTK.cmake" )
SET( BaseLibVTK_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blUtilitiesVTK/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blIO/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blCardioModel/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blSimplexDeformableModels/src" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blUtilitiesVTK/include" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blIO/include" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blCardioModel/include" "${GIMIAS_SRC_PATH}/Modules/BaseLib/libmodules/blSimplexDeformableModels/include" "${GIMIAS_BINARY_PATH}/library/BaseLibVTK"  )
SET( BaseLibVTK_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( BaseLibVTK_LIBRARIES ${BaseLibVTK_LIBRARIES} "BaseLibVTK"  )
