# CMakeLists.txt generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

PROJECT(TpExtLibWxWidgets)
SET( GIMIAS_BINARY_PATH "GIMIAS/bin" CACHE PATH "Path to GIMIAS binaries folder")
SET( GIMIAS_SRC_PATH "GIMIAS/src" CACHE PATH "Path to GIMIAS source code folder")
SET( GIMIAS_THIRD_PARTY_SCR_PATH "GIMIAS/thirdParty" CACHE PATH "Path to GIMIAS thirdparty libraries source code")
SET( GIMIAS_THIRD_PARTY_BINARIES_PATH "GIMIAS/bin/thirdParty" CACHE PATH "Path to GIMIAS thirdparty binaries")
SET( GIMIAS_EXECUTABLE_PATH "GIMIAS/bin/bin" CACHE PATH "Path to GIMIAS executable folder")
MESSAGE( STATUS "Processing TpExtLibWxWidgets" )

# All binary outputs are written to the same folder.
SET( CMAKE_SUPPRESS_REGENERATION TRUE )
SET( EXECUTABLE_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
SET( LIBRARY_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
cmake_minimum_required(VERSION 2.4.6)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)


ADD_SUBDIRECTORY("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxWebUpdate" "${GIMIAS_BINARY_PATH}/library/tpExtLibWxWebUpdate")

INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibWxWidgets/TpExtLibWxWidgetsConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibWxWidgets/UseTpExtLibWxWidgets.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxWebUpdate/tpExtLibWxWebUpdateConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxWebUpdate/UsetpExtLibWxWebUpdate.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxhttpEngine/tpExtLibWxhttpEngineConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxhttpEngine/UsetpExtLibWxhttpEngine.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/WXWIDGETS-3.0.2/WXWIDGETS-3.0.2Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/WXWIDGETS-3.0.2/UseWXWIDGETS-3.0.2.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/BaseLibConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/UseBaseLib.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/LOG4CPLUSConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/UseLOG4CPLUS.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/BOOST-1.45.0Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/UseBOOST-1.45.0.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/CILabMacrosConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/UseCILabMacros.cmake")

#Adding CMake PrecompiledHeader Pre
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PCHSupport_26.cmake" )
GET_NATIVE_PRECOMPILED_HEADER("TpExtLibWxWidgets" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/tpExtLibWxWidgetsPCH.h")

 # Create PCH Files (header) group 
IF (WIN32)
  SOURCE_GROUP("PCH Files (header)" FILES "${GIMIAS_SRC_PATH}/Modules/TpExtLib/tpExtLibWxWidgetsPCH.h" )
ENDIF(WIN32)


 # Create PCH Files group 
IF (WIN32)
  SOURCE_GROUP("PCH Files" FILES "${GIMIAS_BINARY_PATH}/library/TpExtLibWxWidgets/TpExtLibWxWidgets_pch.cxx" )
ENDIF(WIN32)

#Configure header file and move it to binary folder
CONFIGURE_FILE(${GIMIAS_SRC_PATH}/cmake/library/TpExtLibWxWidgets/TpExtLibWxWidgetsWin32Header.h.in ${GIMIAS_BINARY_PATH}/library/TpExtLibWxWidgets/TpExtLibWxWidgetsWin32Header.h)

# Add target
ADD_LIBRARY(TpExtLibWxWidgets SHARED    "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxAutoCompletionTextCtrl.cxx" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxEmbeddedAppWindow.cxx" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxFloatSlider.cxx" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxMemoryUsageIndicator.cxx" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxSliderWithSpinCtrl.cxx" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxSliderWithTextCtrl.cxx" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxToolBoxControl.cxx" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxToolBoxItem.cxx" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxToolBoxItemSelectedEvent.cxx" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxUnicode.cxx" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxWidgetStackControl.cxx" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/blwxRangeValidator.cpp" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/blwxTreeCtrl.cpp" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/button.cpp" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/thumbnailctrl.cpp" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/toolbar.cpp" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/treelistctrl.cpp" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxCaptureWindow.cpp" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxCheckableControl.cpp" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxCxxTestFrame.cpp" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxEventHandlerHelper.cpp" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxID.cpp" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/blMitkUnicode.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/blwxRangeValidator.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/blwxTreeCtrl.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/msgqueue.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/thumbnailctrl.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxAutoCompletionTextCtrl.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxCaptureWindow.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxCheckableControl.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxCxxTestFrame.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxEmbeddedAppWindow.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxEventHandlerHelper.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxFloatSlider.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxID.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxMemoryUsageIndicator.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxSliderWithSpinCtrl.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxSliderWithTextCtrl.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxToolBoxControl.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxToolBoxItem.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxToolBoxItemSelectedEvent.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxUnicode.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/tpExtWxWidgets/src/wxWidgetStackControl.h" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/tpExtLibWxWidgetsPCH.h" "${GIMIAS_BINARY_PATH}/library/TpExtLibWxWidgets/TpExtLibWxWidgets_pch.cxx" )
TARGET_LINK_LIBRARIES(TpExtLibWxWidgets  tpExtLibWxWebUpdate  tpExtLibWxhttpEngine  BaseLib  CILabMacros ${TpExtLibWxWidgets_LIBRARIES} )
ADD_DEFINITIONS( )

#Adding specific windows macros
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PlatformDependent.cmake" )
INCREASE_MSVC_HEAP_LIMIT( 1000 )
SUPPRESS_VC8_DEPRECATED_WARNINGS( )
SUPPRESS_LINKER_WARNING_4089( TpExtLibWxWidgets )
SUPPRESS_COMPILER_WARNING_DLL_EXPORT( TpExtLibWxWidgets )

#Adding properties

#Adding CMake PrecompiledHeader Post
ADD_NATIVE_PRECOMPILED_HEADER("TpExtLibWxWidgets" "${GIMIAS_SRC_PATH}/Modules/TpExtLib/tpExtLibWxWidgetsPCH.h")

ADD_DEPENDENCIES(TpExtLibWxWidgets CILabMacros)
ADD_DEPENDENCIES(TpExtLibWxWidgets BaseLib)
ADD_DEPENDENCIES(TpExtLibWxWidgets tpExtLibWxhttpEngine)
ADD_DEPENDENCIES(TpExtLibWxWidgets tpExtLibWxWebUpdate)
