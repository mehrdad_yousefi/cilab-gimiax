# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ImageReadDicomWriteLib_FOUND TRUE )
SET( ImageReadDicomWriteLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/ImageReadDicomWriteLib/UseImageReadDicomWriteLib.cmake" )
SET( ImageReadDicomWriteLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/ImageReadDicomWriteLib"  )
SET( ImageReadDicomWriteLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( ImageReadDicomWriteLib_LIBRARIES ${ImageReadDicomWriteLib_LIBRARIES} "ImageReadDicomWriteLib"  )
