# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( VotingBinaryHoleFillingImageFilterLib_FOUND TRUE )
SET( VotingBinaryHoleFillingImageFilterLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/VotingBinaryHoleFillingImageFilterLib/UseVotingBinaryHoleFillingImageFilterLib.cmake" )
SET( VotingBinaryHoleFillingImageFilterLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/VotingBinaryHoleFillingImageFilterLib"  )
SET( VotingBinaryHoleFillingImageFilterLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( VotingBinaryHoleFillingImageFilterLib_LIBRARIES ${VotingBinaryHoleFillingImageFilterLib_LIBRARIES} "VotingBinaryHoleFillingImageFilterLib"  )
