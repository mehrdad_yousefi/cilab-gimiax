# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( NormalizeMaximumFilterLib_FOUND TRUE )
SET( NormalizeMaximumFilterLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/NormalizeMaximumFilterLib/UseNormalizeMaximumFilterLib.cmake" )
SET( NormalizeMaximumFilterLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/NormalizeMaximumFilterLib"  )
SET( NormalizeMaximumFilterLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( NormalizeMaximumFilterLib_LIBRARIES ${NormalizeMaximumFilterLib_LIBRARIES} "NormalizeMaximumFilterLib"  )
