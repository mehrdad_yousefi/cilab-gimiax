# CMakeLists.txt generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

PROJECT(DynWxAGUILib)
SET( GIMIAS_BINARY_PATH "GIMIAS/bin" CACHE PATH "Path to GIMIAS binaries folder")
SET( GIMIAS_SRC_PATH "GIMIAS/src" CACHE PATH "Path to GIMIAS source code folder")
SET( GIMIAS_THIRD_PARTY_SCR_PATH "GIMIAS/thirdParty" CACHE PATH "Path to GIMIAS thirdparty libraries source code")
SET( GIMIAS_THIRD_PARTY_BINARIES_PATH "GIMIAS/bin/thirdParty" CACHE PATH "Path to GIMIAS thirdparty binaries")
SET( GIMIAS_EXECUTABLE_PATH "GIMIAS/bin/bin" CACHE PATH "Path to GIMIAS executable folder")
MESSAGE( STATUS "Processing DynWxAGUILib" )

# All binary outputs are written to the same folder.
SET( CMAKE_SUPPRESS_REGENERATION TRUE )
SET( EXECUTABLE_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
SET( LIBRARY_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
cmake_minimum_required(VERSION 2.4.6)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)


ADD_SUBDIRECTORY("${GIMIAS_SRC_PATH}/cmake/library/GuiBridgeLibWxWidgets" "${GIMIAS_BINARY_PATH}/library/GuiBridgeLibWxWidgets")

INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DynWxAGUILib/DynWxAGUILibConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DynWxAGUILib/UseDynWxAGUILib.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/GuiBridgeLibWxWidgets/GuiBridgeLibWxWidgetsConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/GuiBridgeLibWxWidgets/UseGuiBridgeLibWxWidgets.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibWxWidgets/TpExtLibWxWidgetsConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibWxWidgets/UseTpExtLibWxWidgets.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxWebUpdate/tpExtLibWxWebUpdateConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxWebUpdate/UsetpExtLibWxWebUpdate.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxhttpEngine/tpExtLibWxhttpEngineConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxhttpEngine/UsetpExtLibWxhttpEngine.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/WXWIDGETS-3.0.2/WXWIDGETS-3.0.2Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/WXWIDGETS-3.0.2/UseWXWIDGETS-3.0.2.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/GuiBridgeLib/GuiBridgeLibConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/GuiBridgeLib/UseGuiBridgeLib.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DynLib/DynLibConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DynLib/UseDynLib.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TinyXml/TinyXmlConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TinyXml/UseTinyXml.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LIBELF/LIBELFConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LIBELF/UseLIBELF.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/BaseLibConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/UseBaseLib.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/LOG4CPLUSConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/UseLOG4CPLUS.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/BOOST-1.45.0Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/UseBOOST-1.45.0.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/CILabMacrosConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/UseCILabMacros.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/SLICERConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/UseSLICER.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/ITK-3.20Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/UseITK-3.20.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/VTK-5.10.1Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/UseVTK-5.10.1.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/HDF5Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/UseHDF5.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/ZLIBConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/UseZLIB.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/ExPat/ExPatConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/ExPat/UseExPat.cmake")

#Adding CMake PrecompiledHeader Pre
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PCHSupport_26.cmake" )
GET_NATIVE_PRECOMPILED_HEADER("DynWxAGUILib" "${GIMIAS_SRC_PATH}/Modules/DynLib/dynWxAGUILibPCH.h")

 # Create PCH Files (header) group 
IF (WIN32)
  SOURCE_GROUP("PCH Files (header)" FILES "${GIMIAS_SRC_PATH}/Modules/DynLib/dynWxAGUILibPCH.h" )
ENDIF(WIN32)


 # Create PCH Files group 
IF (WIN32)
  SOURCE_GROUP("PCH Files" FILES "${GIMIAS_BINARY_PATH}/library/DynWxAGUILib/DynWxAGUILib_pch.cxx" )
ENDIF(WIN32)

#Configure header file and move it to binary folder
CONFIGURE_FILE(${GIMIAS_SRC_PATH}/cmake/library/DynWxAGUILib/DynWxAGUILibWin32Header.h.in ${GIMIAS_BINARY_PATH}/library/DynWxAGUILib/DynWxAGUILibWin32Header.h)

# Add target
ADD_LIBRARY(DynWxAGUILib SHARED    "${GIMIAS_SRC_PATH}/Modules/DynLib/libmodules/dynWxAGUI/src/dynBasePanel.cpp" "${GIMIAS_SRC_PATH}/Modules/DynLib/libmodules/dynWxAGUI/src/dynBasePanelUI.cpp" "${GIMIAS_SRC_PATH}/Modules/DynLib/libmodules/dynWxAGUI/src/dynWxAGUIBuilder.cpp" "${GIMIAS_SRC_PATH}/Modules/DynLib/libmodules/dynWxAGUI/src/dynWxControlFactory.cpp" "${GIMIAS_SRC_PATH}/Modules/DynLib/libmodules/dynWxAGUI/src/dynWxGUIUpdater.cpp" "${GIMIAS_SRC_PATH}/Modules/DynLib/libmodules/dynWxAGUI/src/dynBasePanel.h" "${GIMIAS_SRC_PATH}/Modules/DynLib/libmodules/dynWxAGUI/src/dynBasePanelUI.h" "${GIMIAS_SRC_PATH}/Modules/DynLib/libmodules/dynWxAGUI/src/dynWxAGUIBuilder.h" "${GIMIAS_SRC_PATH}/Modules/DynLib/libmodules/dynWxAGUI/src/dynWxControlFactory.h" "${GIMIAS_SRC_PATH}/Modules/DynLib/libmodules/dynWxAGUI/src/dynWxControlFactoryBase.h" "${GIMIAS_SRC_PATH}/Modules/DynLib/libmodules/dynWxAGUI/src/dynWxGUIUpdater.h" "${GIMIAS_SRC_PATH}/Modules/DynLib/dynWxAGUILibPCH.h" "${GIMIAS_BINARY_PATH}/library/DynWxAGUILib/DynWxAGUILib_pch.cxx" )
TARGET_LINK_LIBRARIES(DynWxAGUILib  GuiBridgeLibWxWidgets  TpExtLibWxWidgets  tpExtLibWxWebUpdate  tpExtLibWxhttpEngine  GuiBridgeLib  DynLib  TinyXml  BaseLib  CILabMacros  ExPat ${DynWxAGUILib_LIBRARIES} )
ADD_DEFINITIONS( )

#Adding specific windows macros
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PlatformDependent.cmake" )
INCREASE_MSVC_HEAP_LIMIT( 1000 )
SUPPRESS_VC8_DEPRECATED_WARNINGS( )
SUPPRESS_LINKER_WARNING_4089( DynWxAGUILib )
SUPPRESS_COMPILER_WARNING_DLL_EXPORT( DynWxAGUILib )

#Adding properties

#Adding CMake PrecompiledHeader Post
ADD_NATIVE_PRECOMPILED_HEADER("DynWxAGUILib" "${GIMIAS_SRC_PATH}/Modules/DynLib/dynWxAGUILibPCH.h")

ADD_DEPENDENCIES(DynWxAGUILib ExPat)
ADD_DEPENDENCIES(DynWxAGUILib CILabMacros)
ADD_DEPENDENCIES(DynWxAGUILib BaseLib)
ADD_DEPENDENCIES(DynWxAGUILib TinyXml)
ADD_DEPENDENCIES(DynWxAGUILib DynLib)
ADD_DEPENDENCIES(DynWxAGUILib GuiBridgeLib)
ADD_DEPENDENCIES(DynWxAGUILib tpExtLibWxhttpEngine)
ADD_DEPENDENCIES(DynWxAGUILib tpExtLibWxWebUpdate)
ADD_DEPENDENCIES(DynWxAGUILib TpExtLibWxWidgets)
ADD_DEPENDENCIES(DynWxAGUILib GuiBridgeLibWxWidgets)
