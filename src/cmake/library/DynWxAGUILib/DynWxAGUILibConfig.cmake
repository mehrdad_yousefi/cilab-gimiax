# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( DynWxAGUILib_FOUND TRUE )
SET( DynWxAGUILib_USE_FILE "${GIMIAS_BINARY_PATH}/library/DynWxAGUILib/UseDynWxAGUILib.cmake" )
SET( DynWxAGUILib_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/DynLib/libmodules/dynWxAGUI/src" "${GIMIAS_BINARY_PATH}/library/DynWxAGUILib"  )
SET( DynWxAGUILib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( DynWxAGUILib_LIBRARIES ${DynWxAGUILib_LIBRARIES} "DynWxAGUILib"  )
