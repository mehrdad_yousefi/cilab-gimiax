# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( MedianImageFilterLib_FOUND TRUE )
SET( MedianImageFilterLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/MedianImageFilterLib/UseMedianImageFilterLib.cmake" )
SET( MedianImageFilterLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/MedianImageFilterLib"  )
SET( MedianImageFilterLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( MedianImageFilterLib_LIBRARIES ${MedianImageFilterLib_LIBRARIES} "MedianImageFilterLib"  )
