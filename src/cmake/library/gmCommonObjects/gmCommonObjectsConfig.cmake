# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( gmCommonObjects_FOUND TRUE )
SET( gmCommonObjects_USE_FILE "${GIMIAS_BINARY_PATH}/library/gmCommonObjects/UsegmCommonObjects.cmake" )
SET( gmCommonObjects_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/CommonObjects/src" "${GIMIAS_BINARY_PATH}/library/gmCommonObjects"  )
SET( gmCommonObjects_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( gmCommonObjects_LIBRARIES ${gmCommonObjects_LIBRARIES} "gmCommonObjects"  )
