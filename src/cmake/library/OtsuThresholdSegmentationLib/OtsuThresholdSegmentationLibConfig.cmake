# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( OtsuThresholdSegmentationLib_FOUND TRUE )
SET( OtsuThresholdSegmentationLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/OtsuThresholdSegmentationLib/UseOtsuThresholdSegmentationLib.cmake" )
SET( OtsuThresholdSegmentationLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/OtsuThresholdSegmentationLib"  )
SET( OtsuThresholdSegmentationLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( OtsuThresholdSegmentationLib_LIBRARIES ${OtsuThresholdSegmentationLib_LIBRARIES} "OtsuThresholdSegmentationLib"  )
