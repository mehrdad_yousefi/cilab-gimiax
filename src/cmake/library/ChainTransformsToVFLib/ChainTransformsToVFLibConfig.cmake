# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ChainTransformsToVFLib_FOUND TRUE )
SET( ChainTransformsToVFLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/ChainTransformsToVFLib/UseChainTransformsToVFLib.cmake" )
SET( ChainTransformsToVFLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/ChainTransformsToVFLib"  )
SET( ChainTransformsToVFLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( ChainTransformsToVFLib_LIBRARIES ${ChainTransformsToVFLib_LIBRARIES} "ChainTransformsToVFLib"  )
