# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( GrayscaleFillHoleImageFilterLib_FOUND TRUE )
SET( GrayscaleFillHoleImageFilterLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/GrayscaleFillHoleImageFilterLib/UseGrayscaleFillHoleImageFilterLib.cmake" )
SET( GrayscaleFillHoleImageFilterLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/GrayscaleFillHoleImageFilterLib"  )
SET( GrayscaleFillHoleImageFilterLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( GrayscaleFillHoleImageFilterLib_LIBRARIES ${GrayscaleFillHoleImageFilterLib_LIBRARIES} "GrayscaleFillHoleImageFilterLib"  )
