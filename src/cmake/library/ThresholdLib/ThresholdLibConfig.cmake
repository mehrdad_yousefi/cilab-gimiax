# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ThresholdLib_FOUND TRUE )
SET( ThresholdLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/ThresholdLib/UseThresholdLib.cmake" )
SET( ThresholdLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/ThresholdLib"  )
SET( ThresholdLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( ThresholdLib_LIBRARIES ${ThresholdLib_LIBRARIES} "ThresholdLib"  )
