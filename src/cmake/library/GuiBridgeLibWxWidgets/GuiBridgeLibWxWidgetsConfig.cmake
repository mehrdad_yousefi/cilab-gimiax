# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( GuiBridgeLibWxWidgets_FOUND TRUE )
SET( GuiBridgeLibWxWidgets_USE_FILE "${GIMIAS_BINARY_PATH}/library/GuiBridgeLibWxWidgets/UseGuiBridgeLibWxWidgets.cmake" )
SET( GuiBridgeLibWxWidgets_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/GuiBridgeLib/libmodules/guiBridgeLibWxWidgets/src" "${GIMIAS_BINARY_PATH}/library/GuiBridgeLibWxWidgets"  )
SET( GuiBridgeLibWxWidgets_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( GuiBridgeLibWxWidgets_LIBRARIES ${GuiBridgeLibWxWidgets_LIBRARIES} "GuiBridgeLibWxWidgets"  )
