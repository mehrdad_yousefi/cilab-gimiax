# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( WarpImageLib_FOUND TRUE )
SET( WarpImageLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/WarpImageLib/UseWarpImageLib.cmake" )
SET( WarpImageLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/WarpImageLib"  )
SET( WarpImageLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( WarpImageLib_LIBRARIES ${WarpImageLib_LIBRARIES} "WarpImageLib"  )
