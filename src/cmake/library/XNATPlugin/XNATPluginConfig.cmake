# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( XNATPlugin_FOUND TRUE )
SET( XNATPlugin_USE_FILE "${GIMIAS_BINARY_PATH}/library/XNATPlugin/UseXNATPlugin.cmake" )
SET( XNATPlugin_INCLUDE_DIRS "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/XNATPlugin" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/XNATPlugin/processors" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/XNATPlugin/widgets/XNATToolbar" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/XNATPlugin/widgets/XNATToolbar" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/XNATPlugin/widgets/XNATWorkingArea" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/XNATPlugin/widgets/XNATWorkingArea" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/XNATPlugin/widgets/XNATTree" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/XNATPlugin/widgets/XNATTree" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/XNATPlugin/widgets/XNATPluginConnectionPanelWidget" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/XNATPlugin/widgets/XNATPluginConnectionPanelWidget" "${GIMIAS_BINARY_PATH}/library/XNATPlugin"  )
SET( XNATPlugin_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/plugins/XNATPlugin/lib"  )
SET( XNATPlugin_LIBRARIES ${XNATPlugin_LIBRARIES} "XNATPlugin"  )
