# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( GaussianBlurImageFilterLib_FOUND TRUE )
SET( GaussianBlurImageFilterLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/GaussianBlurImageFilterLib/UseGaussianBlurImageFilterLib.cmake" )
SET( GaussianBlurImageFilterLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/GaussianBlurImageFilterLib"  )
SET( GaussianBlurImageFilterLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( GaussianBlurImageFilterLib_LIBRARIES ${GaussianBlurImageFilterLib_LIBRARIES} "GaussianBlurImageFilterLib"  )
