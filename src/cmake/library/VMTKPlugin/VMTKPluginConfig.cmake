# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( VMTKPlugin_FOUND TRUE )
SET( VMTKPlugin_USE_FILE "${GIMIAS_BINARY_PATH}/library/VMTKPlugin/UseVMTKPlugin.cmake" )
SET( VMTKPlugin_INCLUDE_DIRS "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/VMTKPlugin" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/VMTKPlugin/processors" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/VMTKPlugin/widgets/VesselCenterlineWidget" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/VMTKPlugin/widgets/VesselCenterlineWidget" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/VMTKPlugin/widgets/VesselBifurcationsWidget" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/VMTKPlugin/widgets/VesselBifurcationsWidget" "${GIMIAS_BINARY_PATH}/library/VMTKPlugin"  )
SET( VMTKPlugin_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/plugins/VMTKPlugin/lib"  )
SET( VMTKPlugin_LIBRARIES ${VMTKPlugin_LIBRARIES} "VMTKPlugin"  )
