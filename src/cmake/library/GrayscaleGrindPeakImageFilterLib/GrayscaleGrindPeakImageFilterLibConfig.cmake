# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( GrayscaleGrindPeakImageFilterLib_FOUND TRUE )
SET( GrayscaleGrindPeakImageFilterLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/GrayscaleGrindPeakImageFilterLib/UseGrayscaleGrindPeakImageFilterLib.cmake" )
SET( GrayscaleGrindPeakImageFilterLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/GrayscaleGrindPeakImageFilterLib"  )
SET( GrayscaleGrindPeakImageFilterLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( GrayscaleGrindPeakImageFilterLib_LIBRARIES ${GrayscaleGrindPeakImageFilterLib_LIBRARIES} "GrayscaleGrindPeakImageFilterLib"  )
