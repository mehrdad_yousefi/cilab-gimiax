# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( wsGimias_FOUND TRUE )
SET( wsGimias_USE_FILE "${GIMIAS_BINARY_PATH}/library/wsGimias/UsewsGimias.cmake" )
SET( wsGimias_INCLUDE_DIRS "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/WebServicesPlugin/webService" "${GIMIAS_BINARY_PATH}/library/wsGimias"  )
SET( wsGimias_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( wsGimias_LIBRARIES ${wsGimias_LIBRARIES} "wsGimias"  )
