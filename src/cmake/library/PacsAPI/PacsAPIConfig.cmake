# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( PacsAPI_FOUND TRUE )
SET( PacsAPI_USE_FILE "${GIMIAS_BINARY_PATH}/library/PacsAPI/UsePacsAPI.cmake" )
SET( PacsAPI_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/PacsAPI/libmodules/pacsAPI/src" "${GIMIAS_SRC_PATH}/Modules/PacsAPI/libmodules/pacsAPI/include" "${GIMIAS_BINARY_PATH}/library/PacsAPI"  )
SET( PacsAPI_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( PacsAPI_LIBRARIES ${PacsAPI_LIBRARIES} "PacsAPI"  )
