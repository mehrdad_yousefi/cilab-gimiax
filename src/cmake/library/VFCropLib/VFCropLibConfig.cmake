# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( VFCropLib_FOUND TRUE )
SET( VFCropLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/VFCropLib/UseVFCropLib.cmake" )
SET( VFCropLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/VFCropLib"  )
SET( VFCropLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( VFCropLib_LIBRARIES ${VFCropLib_LIBRARIES} "VFCropLib"  )
