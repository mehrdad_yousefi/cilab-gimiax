# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( HistogramMatchingLib_FOUND TRUE )
SET( HistogramMatchingLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/HistogramMatchingLib/UseHistogramMatchingLib.cmake" )
SET( HistogramMatchingLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/HistogramMatchingLib"  )
SET( HistogramMatchingLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( HistogramMatchingLib_LIBRARIES ${HistogramMatchingLib_LIBRARIES} "HistogramMatchingLib"  )
