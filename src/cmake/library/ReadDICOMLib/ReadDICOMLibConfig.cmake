# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( ReadDICOMLib_FOUND TRUE )
SET( ReadDICOMLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/ReadDICOMLib/UseReadDICOMLib.cmake" )
SET( ReadDICOMLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/ReadDICOMLib"  )
SET( ReadDICOMLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( ReadDICOMLib_LIBRARIES ${ReadDICOMLib_LIBRARIES} "ReadDICOMLib"  )
