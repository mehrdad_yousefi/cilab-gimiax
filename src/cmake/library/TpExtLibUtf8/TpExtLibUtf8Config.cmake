# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( TpExtLibUtf8_FOUND TRUE )
SET( TpExtLibUtf8_USE_FILE "${GIMIAS_BINARY_PATH}/library/TpExtLibUtf8/UseTpExtLibUtf8.cmake" )
SET( TpExtLibUtf8_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/TpExtLib/libmodules/utf8_v2_3_1/src" "${GIMIAS_BINARY_PATH}/library/TpExtLibUtf8"  )
SET( TpExtLibUtf8_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( TpExtLibUtf8_LIBRARIES ${TpExtLibUtf8_LIBRARIES} "TpExtLibUtf8"  )
