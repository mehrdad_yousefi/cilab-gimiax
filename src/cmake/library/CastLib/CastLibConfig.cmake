# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( CastLib_FOUND TRUE )
SET( CastLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/CastLib/UseCastLib.cmake" )
SET( CastLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/CastLib"  )
SET( CastLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( CastLib_LIBRARIES ${CastLib_LIBRARIES} "CastLib"  )
