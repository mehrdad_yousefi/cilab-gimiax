# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( GradientAnisotropicDiffusionLib_FOUND TRUE )
SET( GradientAnisotropicDiffusionLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/GradientAnisotropicDiffusionLib/UseGradientAnisotropicDiffusionLib.cmake" )
SET( GradientAnisotropicDiffusionLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/GradientAnisotropicDiffusionLib"  )
SET( GradientAnisotropicDiffusionLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( GradientAnisotropicDiffusionLib_LIBRARIES ${GradientAnisotropicDiffusionLib_LIBRARIES} "GradientAnisotropicDiffusionLib"  )
