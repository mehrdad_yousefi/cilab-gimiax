# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( leftAtriumSegmentationLib_FOUND TRUE )
SET( leftAtriumSegmentationLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/leftAtriumSegmentationLib/UseleftAtriumSegmentationLib.cmake" )
SET( leftAtriumSegmentationLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/leftAtriumSegmentationLib"  )
SET( leftAtriumSegmentationLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( leftAtriumSegmentationLib_LIBRARIES ${leftAtriumSegmentationLib_LIBRARIES} "leftAtriumSegmentationLib"  )
