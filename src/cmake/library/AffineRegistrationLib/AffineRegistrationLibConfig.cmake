# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( AffineRegistrationLib_FOUND TRUE )
SET( AffineRegistrationLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/AffineRegistrationLib/UseAffineRegistrationLib.cmake" )
SET( AffineRegistrationLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/AffineRegistrationLib"  )
SET( AffineRegistrationLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( AffineRegistrationLib_LIBRARIES ${AffineRegistrationLib_LIBRARIES} "AffineRegistrationLib"  )
