# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( VFInverseLib_FOUND TRUE )
SET( VFInverseLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/VFInverseLib/UseVFInverseLib.cmake" )
SET( VFInverseLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/VFInverseLib"  )
SET( VFInverseLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( VFInverseLib_LIBRARIES ${VFInverseLib_LIBRARIES} "VFInverseLib"  )
