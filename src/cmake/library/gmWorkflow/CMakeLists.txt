# CMakeLists.txt generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

PROJECT(gmWorkflow)
SET( GIMIAS_BINARY_PATH "GIMIAS/bin" CACHE PATH "Path to GIMIAS binaries folder")
SET( GIMIAS_SRC_PATH "GIMIAS/src" CACHE PATH "Path to GIMIAS source code folder")
SET( GIMIAS_THIRD_PARTY_SCR_PATH "GIMIAS/thirdParty" CACHE PATH "Path to GIMIAS thirdparty libraries source code")
SET( GIMIAS_THIRD_PARTY_BINARIES_PATH "GIMIAS/bin/thirdParty" CACHE PATH "Path to GIMIAS thirdparty binaries")
SET( GIMIAS_EXECUTABLE_PATH "GIMIAS/bin/bin" CACHE PATH "Path to GIMIAS executable folder")
MESSAGE( STATUS "Processing gmWorkflow" )

# All binary outputs are written to the same folder.
SET( CMAKE_SUPPRESS_REGENERATION TRUE )
SET( EXECUTABLE_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
SET( LIBRARY_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin")
cmake_minimum_required(VERSION 2.4.6)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)



INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWorkflow/gmWorkflowConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWorkflow/UsegmWorkflow.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibNumericData/BaseLibNumericDataConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibNumericData/UseBaseLibNumericData.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TinyXml/TinyXmlConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TinyXml/UseTinyXml.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmCommonObjects/gmCommonObjectsConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmCommonObjects/UsegmCommonObjects.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/BaseLibConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/UseBaseLib.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/LOG4CPLUSConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/UseLOG4CPLUS.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/CILabMacrosConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/UseCILabMacros.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/BOOST-1.45.0Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/UseBOOST-1.45.0.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/ITK-3.20Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/UseITK-3.20.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/VTK-5.10.1Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/UseVTK-5.10.1.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/HDF5Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/UseHDF5.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/ZLIBConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/UseZLIB.cmake")

#Adding CMake PrecompiledHeader Pre
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PCHSupport_26.cmake" )
GET_NATIVE_PRECOMPILED_HEADER("gmWorkflow" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/gmWorkflowPCH.h")

 # Create PCH Files (header) group 
IF (WIN32)
  SOURCE_GROUP("PCH Files (header)" FILES "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/gmWorkflowPCH.h" )
ENDIF(WIN32)


 # Create PCH Files group 
IF (WIN32)
  SOURCE_GROUP("PCH Files" FILES "${GIMIAS_BINARY_PATH}/library/gmWorkflow/gmWorkflow_pch.cxx" )
ENDIF(WIN32)

#Configure header file and move it to binary folder
CONFIGURE_FILE(${GIMIAS_SRC_PATH}/cmake/library/gmWorkflow/gmWorkflowWin32Header.h.in ${GIMIAS_BINARY_PATH}/library/gmWorkflow/gmWorkflowWin32Header.h)

# Add target
ADD_LIBRARY(gmWorkflow SHARED    "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/gmWorkflowPCH.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreBaseWorkflowIO.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreBoostWorkflowReader.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreWorkflow.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreWorkflowReader.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreWorkflowSerialize.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreWorkflowStep.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreWorkflowSubStep.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreWorkflowWriter.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreXMLWorkflowReader.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreXMLWorkflowWriter.cxx" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreBaseWorkflowIO.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreBoostWorkflowReader.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreWorkflow.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreWorkflowReader.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreWorkflowSerialize.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreWorkflowStep.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreWorkflowSubStep.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreWorkflowWriter.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreXMLWorkflowReader.h" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src/coreXMLWorkflowWriter.h" "${GIMIAS_BINARY_PATH}/library/gmWorkflow/gmWorkflow_pch.cxx" )
TARGET_LINK_LIBRARIES(gmWorkflow  BaseLibNumericData  TinyXml  gmCommonObjects  BaseLib  CILabMacros ${gmWorkflow_LIBRARIES} )
ADD_DEFINITIONS( )

#Adding specific windows macros
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PlatformDependent.cmake" )
INCREASE_MSVC_HEAP_LIMIT( 1000 )
SUPPRESS_VC8_DEPRECATED_WARNINGS( )
SUPPRESS_LINKER_WARNING_4089( gmWorkflow )
SUPPRESS_COMPILER_WARNING_DLL_EXPORT( gmWorkflow )

#Adding properties

#Adding CMake PrecompiledHeader Post
ADD_NATIVE_PRECOMPILED_HEADER("gmWorkflow" "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/gmWorkflowPCH.h")

ADD_DEPENDENCIES(gmWorkflow CILabMacros)
ADD_DEPENDENCIES(gmWorkflow BaseLib)
ADD_DEPENDENCIES(gmWorkflow gmCommonObjects)
ADD_DEPENDENCIES(gmWorkflow TinyXml)
ADD_DEPENDENCIES(gmWorkflow BaseLibNumericData)
