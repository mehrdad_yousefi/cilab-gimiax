# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( gmWorkflow_FOUND TRUE )
SET( gmWorkflow_USE_FILE "${GIMIAS_BINARY_PATH}/library/gmWorkflow/UsegmWorkflow.cmake" )
SET( gmWorkflow_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Workflow/src" "${GIMIAS_BINARY_PATH}/library/gmWorkflow"  )
SET( gmWorkflow_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( gmWorkflow_LIBRARIES ${gmWorkflow_LIBRARIES} "gmWorkflow"  )
