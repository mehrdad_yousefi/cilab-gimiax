# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( BrainExtractionLib_FOUND TRUE )
SET( BrainExtractionLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/BrainExtractionLib/UseBrainExtractionLib.cmake" )
SET( BrainExtractionLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/BrainExtractionLib"  )
SET( BrainExtractionLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/commandLinePlugins"  )
SET( BrainExtractionLib_LIBRARIES ${BrainExtractionLib_LIBRARIES} "BrainExtractionLib"  )
