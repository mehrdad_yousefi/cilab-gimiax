# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( TinyXml_FOUND TRUE )
SET( TinyXml_USE_FILE "${GIMIAS_BINARY_PATH}/library/TinyXml/UseTinyXml.cmake" )
SET( TinyXml_INCLUDE_DIRS "${GIMIAS_THIRD_PARTY_SCR_PATH}/TinyXml/src" "${GIMIAS_BINARY_PATH}/library/TinyXml"  )
SET( TinyXml_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( TinyXml_LIBRARIES ${TinyXml_LIBRARIES} "TinyXml"  )
