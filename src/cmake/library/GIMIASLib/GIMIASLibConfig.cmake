# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( GIMIASLib_FOUND TRUE )
SET( GIMIASLib_USE_FILE "${GIMIAS_BINARY_PATH}/library/GIMIASLib/UseGIMIASLib.cmake" )
SET( GIMIASLib_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/library/GIMIASLib"  )
SET( GIMIASLib_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
