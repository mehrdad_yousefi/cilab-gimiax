# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( dcmAPIIO_FOUND TRUE )
SET( dcmAPIIO_USE_FILE "${GIMIAS_BINARY_PATH}/library/dcmAPIIO/UsedcmAPIIO.cmake" )
SET( dcmAPIIO_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Modules/DcmAPI/libmodules/dcmIO/src" "${GIMIAS_BINARY_PATH}/library/dcmAPIIO"  )
SET( dcmAPIIO_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( dcmAPIIO_LIBRARIES ${dcmAPIIO_LIBRARIES} "dcmAPIIO"  )
