# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( SceneViewPlugin_FOUND TRUE )
SET( SceneViewPlugin_USE_FILE "${GIMIAS_BINARY_PATH}/library/SceneViewPlugin/UseSceneViewPlugin.cmake" )
SET( SceneViewPlugin_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Apps/Plugins/SceneViewPlugin" "${GIMIAS_SRC_PATH}/Apps/Plugins/SceneViewPlugin/processors" "${GIMIAS_SRC_PATH}/Apps/Plugins/SceneViewPlugin/widgets/ExecuteCommandWidget" "${GIMIAS_SRC_PATH}/Apps/Plugins/SceneViewPlugin/widgets/ExecuteCommandWidget" "${GIMIAS_BINARY_PATH}/library/SceneViewPlugin"  )
SET( SceneViewPlugin_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/plugins/SceneViewPlugin/lib"  )
SET( SceneViewPlugin_LIBRARIES ${SceneViewPlugin_LIBRARIES} "SceneViewPlugin"  )
