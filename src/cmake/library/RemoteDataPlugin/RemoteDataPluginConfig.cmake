# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( RemoteDataPlugin_FOUND TRUE )
SET( RemoteDataPlugin_USE_FILE "${GIMIAS_BINARY_PATH}/library/RemoteDataPlugin/UseRemoteDataPlugin.cmake" )
SET( RemoteDataPlugin_INCLUDE_DIRS "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/processors" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/widgets/RemoteDataMenuHandler" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/widgets/RemoteDataMenuHandler" "${GIMIAS_BINARY_PATH}/library/RemoteDataPlugin"  )
SET( RemoteDataPlugin_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/plugins/RemoteDataPlugin/lib"  )
SET( RemoteDataPlugin_LIBRARIES ${RemoteDataPlugin_LIBRARIES} "RemoteDataPlugin"  )
