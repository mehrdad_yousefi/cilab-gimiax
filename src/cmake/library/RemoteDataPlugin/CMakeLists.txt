# CMakeLists.txt generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

PROJECT(RemoteDataPlugin)
SET( GIMIAS_BINARY_PATH "GIMIAS/bin" CACHE PATH "Path to GIMIAS binaries folder")
SET( GIMIAS_SRC_PATH "GIMIAS/src" CACHE PATH "Path to GIMIAS source code folder")
SET( GIMIAS_EXTENSIONS_SRC_PATH "GIMIAS/src_extensions" CACHE PATH "Path to GIMIAS extensions source code folder")
SET( GIMIAS_THIRD_PARTY_SCR_PATH "GIMIAS/thirdParty" CACHE PATH "Path to GIMIAS thirdparty libraries source code")
SET( GIMIAS_THIRD_PARTY_BINARIES_PATH "GIMIAS/bin/thirdParty" CACHE PATH "Path to GIMIAS thirdparty binaries")
SET( GIMIAS_EXTENSIONS_THIRD_PARTY_SCR_PATH "GIMIAS/thirdParty_extensions" CACHE PATH "Path to GIMIAS extensions thirdparty libraries source code")
SET( GIMIAS_EXTENSIONS_THIRD_PARTY_BINARIES_PATH "GIMIAS/bin/thirdParty_extensions" CACHE PATH "Path to GIMIAS extensions thirdparty binaries")
SET( GIMIAS_EXECUTABLE_PATH "GIMIAS/bin/bin" CACHE PATH "Path to GIMIAS executable folder")
MESSAGE( STATUS "Processing RemoteDataPlugin" )

# All binary outputs are written to the same folder.
SET( CMAKE_SUPPRESS_REGENERATION TRUE )
SET( EXECUTABLE_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/plugins/RemoteDataPlugin/lib")
SET( LIBRARY_OUTPUT_PATH "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}/plugins/RemoteDataPlugin/lib")
cmake_minimum_required(VERSION 2.4.6)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
  cmake_policy(SET CMP0011 NEW)
endif(COMMAND cmake_policy)



INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/RemoteDataPlugin/RemoteDataPluginConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/RemoteDataPlugin/UseRemoteDataPlugin.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/SshAPI/SshAPIConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/SshAPI/UseSshAPI.cmake")
INCLUDE("${GIMIAS_EXTENSIONS_THIRD_PARTY_BINARIES_PATH}/Libssh/LibsshConfig.cmake")
INCLUDE("${GIMIAS_EXTENSIONS_THIRD_PARTY_BINARIES_PATH}/Libssh/UseLibssh.cmake")
INCLUDE("${GIMIAS_EXTENSIONS_THIRD_PARTY_BINARIES_PATH}/Poco/PocoConfig.cmake")
INCLUDE("${GIMIAS_EXTENSIONS_THIRD_PARTY_BINARIES_PATH}/Poco/UsePoco.cmake")
INCLUDE("${GIMIAS_EXTENSIONS_THIRD_PARTY_BINARIES_PATH}/Libsendspaceapi/LibsendspaceapiConfig.cmake")
INCLUDE("${GIMIAS_EXTENSIONS_THIRD_PARTY_BINARIES_PATH}/Libsendspaceapi/UseLibsendspaceapi.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LIBXML2/LIBXML2Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LIBXML2/UseLIBXML2.cmake")
INCLUDE("${GIMIAS_EXTENSIONS_THIRD_PARTY_BINARIES_PATH}/Curl/CurlConfig.cmake")
INCLUDE("${GIMIAS_EXTENSIONS_THIRD_PARTY_BINARIES_PATH}/Curl/UseCurl.cmake")
INCLUDE("${GIMIAS_EXTENSIONS_THIRD_PARTY_BINARIES_PATH}/OpenSSL/OpenSSLConfig.cmake")
INCLUDE("${GIMIAS_EXTENSIONS_THIRD_PARTY_BINARIES_PATH}/OpenSSL/UseOpenSSL.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmCoreLight/gmCoreLightConfig.cmake.private")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmCoreLight/UsegmCoreLight.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWidgets/gmWidgetsConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWidgets/UsegmWidgets.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DynWxAGUILib/DynWxAGUILibConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DynWxAGUILib/UseDynWxAGUILib.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/GuiBridgeLibWxWidgets/GuiBridgeLibWxWidgetsConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/GuiBridgeLibWxWidgets/UseGuiBridgeLibWxWidgets.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibWxWidgets/TpExtLibWxWidgetsConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibWxWidgets/UseTpExtLibWxWidgets.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxWebUpdate/tpExtLibWxWebUpdateConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxWebUpdate/UsetpExtLibWxWebUpdate.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxhttpEngine/tpExtLibWxhttpEngineConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/tpExtLibWxhttpEngine/UsetpExtLibWxhttpEngine.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/GuiBridgeLib/GuiBridgeLibConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/GuiBridgeLib/UseGuiBridgeLib.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWxEvents/gmWxEventsConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWxEvents/UsegmWxEvents.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/WXWIDGETS-3.0.2/WXWIDGETS-3.0.2Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/WXWIDGETS-3.0.2/UseWXWIDGETS-3.0.2.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmProcessors/gmProcessorsConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmProcessors/UsegmProcessors.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmKernel/gmKernelConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmKernel/UsegmKernel.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibBoost/TpExtLibBoostConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibBoost/UseTpExtLibBoost.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DynLib/DynLibConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/DynLib/UseDynLib.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LIBELF/LIBELFConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LIBELF/UseLIBELF.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/ExPat/ExPatConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/ExPat/UseExPat.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWorkflow/gmWorkflowConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmWorkflow/UsegmWorkflow.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmIO/gmIOConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmIO/UsegmIO.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibUtf8/TpExtLibUtf8Config.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TpExtLibUtf8/UseTpExtLibUtf8.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmFiltering/gmFilteringConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmFiltering/UsegmFiltering.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/SLICERConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/SLICER/UseSLICER.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmDataHandling/gmDataHandlingConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmDataHandling/UsegmDataHandling.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibNumericData/BaseLibNumericDataConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLibNumericData/UseBaseLibNumericData.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TinyXml/TinyXmlConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/TinyXml/UseTinyXml.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmCommonObjects/gmCommonObjectsConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/gmCommonObjects/UsegmCommonObjects.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/BaseLibConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/BaseLib/UseBaseLib.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/LOG4CPLUSConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/LOG4CPLUS/UseLOG4CPLUS.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/CILabMacrosConfig.cmake")
INCLUDE("${GIMIAS_SRC_PATH}/cmake/library/CILabMacros/UseCILabMacros.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/BOOST-1.45.0Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/BOOST-1.45.0/UseBOOST-1.45.0.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/ITK-3.20Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ITK-3.20/UseITK-3.20.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/VTK-5.10.1Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/VTK-5.10.1/UseVTK-5.10.1.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/HDF5Config.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/HDF5/UseHDF5.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/ZLIBConfig.cmake")
INCLUDE("${GIMIAS_THIRD_PARTY_BINARIES_PATH}/ZLIB/UseZLIB.cmake")

#Adding CMake PrecompiledHeader Pre
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PCHSupport_26.cmake" )
GET_NATIVE_PRECOMPILED_HEADER("RemoteDataPlugin" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/RemoteDataPluginPCH.h")

 # Create Widgets group 
IF (WIN32)
  SOURCE_GROUP("Widgets" FILES "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/widgets/RemoteDataMenuHandler/RemoteDataMenuHandler.cpp" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/widgets/RemoteDataMenuHandler/RemoteDataMenuHandler.h" )
ENDIF(WIN32)


 # Create PCH Files group 
IF (WIN32)
  SOURCE_GROUP("PCH Files" FILES "${GIMIAS_BINARY_PATH}/library/RemoteDataPlugin/RemoteDataPlugin_pch.cxx" )
ENDIF(WIN32)

#Configure header file and move it to binary folder
CONFIGURE_FILE(${GIMIAS_SRC_PATH}/cmake/library/RemoteDataPlugin/RemoteDataPluginWin32Header.h.in ${GIMIAS_BINARY_PATH}/library/RemoteDataPlugin/RemoteDataPluginWin32Header.h)

# Add target
ADD_LIBRARY(RemoteDataPlugin SHARED    "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/RemoteDataPlugin.cxx" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/RemoteDataPluginProcessorCollective.cxx" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/RemoteDataPluginWidgetCollective.cxx" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/RemoteDataPlugin.h" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/RemoteDataPluginPCH.h" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/RemoteDataPluginProcessorCollective.h" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/RemoteDataPluginWidgetCollective.h" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/processors/coreCurlReader.cxx" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/processors/coreCurlWriter.cxx" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/processors/coreZipReader.cxx" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/processors/EncodeString.cxx" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/processors/SendSpaceBase.cxx" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/processors/SendSpaceReader.cxx" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/processors/SendSpaceWriter.cxx" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/processors/coreCurlReader.h" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/processors/coreCurlWriter.h" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/processors/coreZipReader.h" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/processors/EncodeString.h" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/processors/SendSpaceBase.h" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/processors/SendSpaceReader.h" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/processors/SendSpaceWriter.h" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/widgets/RemoteDataMenuHandler/RemoteDataMenuHandler.cpp" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/widgets/RemoteDataMenuHandler/RemoteDataMenuHandler.h" "${GIMIAS_BINARY_PATH}/library/RemoteDataPlugin/RemoteDataPlugin_pch.cxx" )
TARGET_LINK_LIBRARIES(RemoteDataPlugin  SshAPI  gmCoreLight  gmWidgets  DynWxAGUILib  GuiBridgeLibWxWidgets  TpExtLibWxWidgets  tpExtLibWxWebUpdate  tpExtLibWxhttpEngine  GuiBridgeLib  gmWxEvents  gmProcessors  gmKernel  TpExtLibBoost  DynLib  ExPat  gmWorkflow  gmIO  TpExtLibUtf8  gmFiltering  gmDataHandling  BaseLibNumericData  TinyXml  gmCommonObjects  BaseLib  CILabMacros ${RemoteDataPlugin_LIBRARIES} )
ADD_DEFINITIONS( )

#Adding specific windows macros
INCLUDE( "${GIMIAS_THIRD_PARTY_SCR_PATH}/cmakeMacros/PlatformDependent.cmake" )
INCREASE_MSVC_HEAP_LIMIT( 1000 )
SUPPRESS_VC8_DEPRECATED_WARNINGS( )
SUPPRESS_LINKER_WARNING_4089( RemoteDataPlugin )
SUPPRESS_COMPILER_WARNING_DLL_EXPORT( RemoteDataPlugin )

#Adding properties

#Adding CMake PrecompiledHeader Post
ADD_NATIVE_PRECOMPILED_HEADER("RemoteDataPlugin" "${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/RemoteDataPluginPCH.h")

ADD_DEPENDENCIES(RemoteDataPlugin CILabMacros)
ADD_DEPENDENCIES(RemoteDataPlugin BaseLib)
ADD_DEPENDENCIES(RemoteDataPlugin gmCommonObjects)
ADD_DEPENDENCIES(RemoteDataPlugin TinyXml)
ADD_DEPENDENCIES(RemoteDataPlugin BaseLibNumericData)
ADD_DEPENDENCIES(RemoteDataPlugin gmDataHandling)
ADD_DEPENDENCIES(RemoteDataPlugin gmFiltering)
ADD_DEPENDENCIES(RemoteDataPlugin TpExtLibUtf8)
ADD_DEPENDENCIES(RemoteDataPlugin gmIO)
ADD_DEPENDENCIES(RemoteDataPlugin gmWorkflow)
ADD_DEPENDENCIES(RemoteDataPlugin ExPat)
ADD_DEPENDENCIES(RemoteDataPlugin DynLib)
ADD_DEPENDENCIES(RemoteDataPlugin TpExtLibBoost)
ADD_DEPENDENCIES(RemoteDataPlugin gmKernel)
ADD_DEPENDENCIES(RemoteDataPlugin gmProcessors)
ADD_DEPENDENCIES(RemoteDataPlugin gmWxEvents)
ADD_DEPENDENCIES(RemoteDataPlugin GuiBridgeLib)
ADD_DEPENDENCIES(RemoteDataPlugin tpExtLibWxhttpEngine)
ADD_DEPENDENCIES(RemoteDataPlugin tpExtLibWxWebUpdate)
ADD_DEPENDENCIES(RemoteDataPlugin TpExtLibWxWidgets)
ADD_DEPENDENCIES(RemoteDataPlugin GuiBridgeLibWxWidgets)
ADD_DEPENDENCIES(RemoteDataPlugin DynWxAGUILib)
ADD_DEPENDENCIES(RemoteDataPlugin gmWidgets)
ADD_DEPENDENCIES(RemoteDataPlugin gmCoreLight)
ADD_DEPENDENCIES(RemoteDataPlugin SshAPI)

INSTALL(FILES ${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/plugin.xml DESTINATION ${GIMIAS_EXECUTABLE_PATH}/\${}/plugins/RemoteDataPlugin/lib/\${})
INSTALL(FILES ${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/plugin.xml DESTINATION ${GIMIAS_EXECUTABLE_PATH}/\${}/plugins/RemoteDataPlugin/lib/\${})
INSTALL(FILES ${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/plugin.xml DESTINATION ${GIMIAS_EXECUTABLE_PATH}/\${}/plugins/RemoteDataPlugin/lib/\${})
INSTALL(FILES ${GIMIAS_EXTENSIONS_SRC_PATH}/plugins/RemoteDataPlugin/plugin.xml DESTINATION ${GIMIAS_EXECUTABLE_PATH}/\${}/plugins/RemoteDataPlugin/lib/\${})
