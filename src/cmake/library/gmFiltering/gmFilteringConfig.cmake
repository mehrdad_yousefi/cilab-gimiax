# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( gmFiltering_FOUND TRUE )
SET( gmFiltering_USE_FILE "${GIMIAS_BINARY_PATH}/library/gmFiltering/UsegmFiltering.cmake" )
SET( gmFiltering_INCLUDE_DIRS "${GIMIAS_SRC_PATH}/Apps/Gimias/Core/Filtering/src" "${GIMIAS_BINARY_PATH}/library/gmFiltering"  )
SET( gmFiltering_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
SET( gmFiltering_LIBRARIES ${gmFiltering_LIBRARIES} "gmFiltering"  )
