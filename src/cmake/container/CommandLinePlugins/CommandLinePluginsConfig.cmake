# File generated automatically by the CSnake generator.
# DO NOT EDIT (changes will be lost)

SET( CommandLinePlugins_FOUND TRUE )
SET( CommandLinePlugins_USE_FILE "${GIMIAS_BINARY_PATH}/container/CommandLinePlugins/UseCommandLinePlugins.cmake" )
SET( CommandLinePlugins_INCLUDE_DIRS "${GIMIAS_BINARY_PATH}/container/CommandLinePlugins"  )
SET( CommandLinePlugins_LIBRARY_DIRS "${GIMIAS_BINARY_PATH}/bin/${CMAKE_CFG_INTDIR}"  )
