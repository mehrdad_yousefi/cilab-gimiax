# Installation Process

This project is an on going project in **alpha** state. Some things are fixed in order to compile it successfully but they will be fixed. Those things are:

- Update to a proper cmake usage
- Improve project folder structure
- 3party binaries when possible
- OSX and GNU/Linux first class citizens

## Vagrant GNU/Linux Debian Jessie VM

If you know what [Vagrant][va] is then you could find [here a vagrant recipe][recipe] for Linux.

[va]:https://www.vagrantup.com/
[recipe]: https://bitbucket.org/CISTIB/cilab-gimiax-vagrant-linux64/overview

## Linux Installation Process

These are the steps you have to follow. You have to install these packages:

```bash
sudo apt-get install gcc g++ make autoconf libtool gdb \
    cmake git libosmesa6-dev libxt-dev libgtk2.0-dev \
    libgl1-mesa-dev pkg-config uuid-dev cmake-*-gui \
    libglu1-mesa-dev libtiff-dev wx3.0-headers wx3.0-i18n \
    libelfg0-dev build-essential vim libwxgtk-media3.0-dev \
    libwxgtk-webview3.0-dev libboost1.55-dev
```

After packages have been installed you have to create a dir in which you can
build the code `mkdir build`.

There is another option that's using valgrant; [this repository][1] has all information about it.

[1]: https://bitbucket.org/CISTIB/cilab-gimiax-vagrant-linux64

## Windows Installation Process

In order to compile Gimias in Windows Platform using Visual Studio 10 SP1 for x86-64bit you have to download and decompress this 3party package from [cistib repository][2] and decompress in `C:\`.

[2]: https://bitbucket.org/CISTIB/cilab-gimiax-3party/downloads/cistib.7z

## OSX Installation Process (WIP)

You have to install these packages using `brew`:

```bash
# TODO brew recipes
brew install ...
```
